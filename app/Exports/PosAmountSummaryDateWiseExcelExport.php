<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PosAmountSummaryDateWiseExcelExport implements FromView ,ShouldAutoSize
{

	 use Exportable;

	protected $outlets,$start_date,$end_date ;

	public function __construct($outlets,$start_date,$end_date){

		$this->outlets = $outlets;
      $this->start_date = $start_date;
      $this->end_date = $end_date;
	}


    

    public function view(): View
    {   
         $outlets = $this->outlets;
         $start_date = $this->start_date;
         $end_date = $this->end_date;

    	 return view('possalessummaryDateWiseexcel',[
                        'outlets'=>$outlets,
                        'start_date'=>$start_date,
                        'end_date'=>$end_date,
                     ]);

        // return $this->viewFile ;
    }
}
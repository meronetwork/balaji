<?php namespace App\Helpers;


use \Illuminate\Support\Facades\DB;
use App\Models\Reservation;

class ReservationHelper {



	public static function room_occupied(){

		$date = date('Y-m-d');

		return Reservation::where('reservation_status', '3');
	}

	public static function hotel_arrival(){


		$date = date('Y-m-d');

		return \App\Models\Reservation::where('book_in', $date)->where('reservation_status','2')->where('parent_res', null);

		
	}

	public static function void(){


		$date = date('Y-m-d');

		return \App\Models\Reservation::where('book_in', $date)->where('reservation_status','9')->where('parent_res', null);



	}

	public static function noShow(){


		$date = date('Y-m-d');

		return \App\Models\Reservation::where('book_in', $date)->where('reservation_status','10')->where('parent_res', null);



	}
	public static function cancelled(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('book_in', $date)->where('reservation_status','11')->where('parent_res', null);

	}


	public static function hotel_departure(){


		$date = date('Y-m-d');
		return \App\Models\Reservation::where('book_out', $date)
						->where('reservation_status','3')
                        ->where('parent_res', null);
		
	}


	public static function due_out(){


		$date = date('Y-m-d');

		return \App\Models\Reservation::where('reservation_status', '4')->where('parent_res', null)
                    ->where('book_out', $date);
		
	}

	public static function stay_over(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('book_out', '<', $date)
				->where('reservation_status','3')
				->where('parent_res', null);


		
	}

	public static function arrival_confirm(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('reservation_type', '1')->where('check_in', $date)->where('parent_res', null);

		
	}

	public static function arrival_unconfirm(){

		$date = date('Y-m-d');
		return \App\Models\Reservation::where('reservation_type', '5')->where('check_in', $date)->where('parent_res', null);
		
	}
	public static function arrived(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('reservation_status', '3')->where('check_in', $date)->where('parent_res', null);

		
	}public static function walk_in(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('check_in', $date)
                    ->where('reservation_status','3')
                    ->where('parent_res', null);

		
	}public static function due_in(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('reservation_status', '2')->where('parent_res', null)
                    ->where('book_in', $date);

		
	}public static function check_out(){

		$date = date('Y-m-d');
		return \App\Models\Reservation::where('reservation_status', '6')->where('parent_res', null)
                    ->where('check_out', $date);

		
	}
	public static function day_use(){

		$date = date('Y-m-d');

		return \App\Models\Reservation::where('book_in', $date)
                    ->where('reservation_status','3')
                    ->where('book_out', $date)
                    ->where('parent_res', null);
		
	}



    public static function findMonthStartAndEnd($date){

        $nepaliCalendar = new \App\Helpers\NepaliCalendar();

        $date_arr = explode('-',$date);

        $nepali_date = $nepaliCalendar->eng_to_nep($date_arr[0],$date_arr[1],$date_arr[2]);

        $nepStartDate = [$nepali_date['year'],$nepali_date['month'],'1'];

        $engStartDate = $nepaliCalendar->nep_to_eng($nepStartDate[0],$nepStartDate[1],$nepStartDate[2]);

        $engStartDate = $engStartDate['year'].'-'.$engStartDate['month'].'-'.$engStartDate['date'];

        $last_day_of_month = $nepaliCalendar->getNepliEndMonthDay($nepali_date['year'],$nepali_date['month']);

        $nepEndDate = [$nepali_date['year'],$nepali_date['month'],$last_day_of_month];

        $engEndDate = $nepaliCalendar->nep_to_eng($nepEndDate[0],$nepEndDate[1],$nepEndDate[2]);

        $engEndDate = $engEndDate['year'].'-'.$engEndDate['month'].'-'.$engEndDate['date'];

        $nepStartEnd = [
            'start'=>$nepStartDate,
            'end'=>$nepEndDate,
        ];

        $engEndDate= [
            'start'=>$engStartDate,
            'end'=>$engEndDate,

        ];

        return ['nep_start_end'=>$nepStartEnd, 'eng_start_end'=>$engEndDate];


    }

    public static function findYearStartAndEndFromNepali($date){
        //date should be in nepali year

        $nepaliCalendar = new \App\Helpers\NepaliCalendar();

        $date_arr = explode('-',$date);

        $engStartDate = $nepaliCalendar->nep_to_eng($date_arr[0],$date_arr[1],$date_arr[2]);
  
        $engStartDate = $engStartDate['year'].'-'.$engStartDate['month'].'-'.$engStartDate['date'];


        $last_day_of_month = $nepaliCalendar->getNepliEndMonthDay($date_arr[0],$date_arr[1]);

        $nepEndDate = [$date_arr[0],$date_arr[1],$last_day_of_month];

        $engEndDate = $nepaliCalendar->nep_to_eng($nepEndDate[0],$nepEndDate[1],$nepEndDate[2]);

        $engEndDate = $engEndDate['year'].'-'.$engEndDate['month'].'-'.$engEndDate['date'];


        $nepStartEnd = [
            'start'=>$date_arr,
            'end'=>$nepEndDate,
        ];

        $engEndDate= [
            'start'=>$engStartDate,
            'end'=>$engEndDate,

        ];

        return ['nep_start_end'=>$nepStartEnd, 'eng_start_end'=>$engEndDate];
    }


    public static function findNepaliDate($date){


        $nepaliCalendar = new \App\Helpers\NepaliCalendar();
        
        $monthStartEnd = self::findMonthStartAndEnd($date);
      
        //find start end end of month
        $currentNepaliYear = $monthStartEnd['nep_start_end']['start'][0];

        $baishakStartDate =  $currentNepaliYear.'-'.'1'.'-'.'1';

        $bishakStartEnd = self::findYearStartAndEndFromNepali($baishakStartDate);

        $chaitraStartDate = $currentNepaliYear.'-'.'12'.'-'.'1';

        $chaitraStartEnd = self::findYearStartAndEndFromNepali($chaitraStartDate);



        $month = [
            'start' => $monthStartEnd['eng_start_end']['start'],
            'end' => $monthStartEnd['eng_start_end']['end'],
        ];

        $year = [
            'start' => $bishakStartEnd['eng_start_end']['start'],
            'end' => $chaitraStartEnd['eng_start_end']['end'],
        ];
        

        $date_arr = explode('-',$date);

        $today_date = $nepaliCalendar->eng_to_nep($date_arr[0],$date_arr[1],$date_arr[2]);  //today_date means given date in nepali date
        
        return [ 'month'=>$month,'year'=>$year,'today_date'=>$today_date ];


    }



    public static function updateOccupancy($resid){

    	$reservation = \App\Models\Reservation::find($resid);
  		$toInsert = [];
  		$today = date('Y-m-d');
  		$toInsert [] = [

    		'resv_id'=>$reservation->id,
    		'occupancy_child'=>$reservation->occupancy_child,
    		'occupancy_adult'=>$reservation->occupancy,
    		'guest_type'=>$reservation->guest_type,
    		'date'=>date('Y-m-d'),

    	];

    	$parentRes = \App\Models\Reservation::where('parent_res',$resid)->get();

    	foreach ($parentRes as $key => $value) {
    		
	    	$toInsert []  = [

	    		'resv_id'=>$value->id,
	    		'occupancy_child'=>$value->occupancy_child,
	    		'occupancy_adult'=>$value->occupancy,
	    		'guest_type'=>$value->guest_type,
	    		'date'=>$today,

	    	];
    	}

    	return \App\Models\ReservationOccupancy::insert($toInsert);
    }



    public static function paidByArr($paidBy){


        $lists = $paidBy->unique('paid_by')->pluck('paid_by')->toArray();

        return implode('<br> & ', $lists);


    }



    public static function calculateRestroAmount(array $orderInfo,$service_charge = 0){  

        //orderInfo must contain product Id=>product_id and qty=>qty

        $subtotal = 0;
        $discount_percent = 0;
        $discount_amount = 0;
        // $service_charge = 0;
        // $amount_with_service = 0;
        foreach ($orderInfo as $key => $value) {
            $price = \App\Models\Product::find($value['product_id'])->price;
            $subtotal += $price* (float) $value['qty'];
        }

        if($service_charge){
            $service_charge = env('SERVICE_CHARGE') / 100 *$subtotal; 
        }
    
        $amount_with_service = $subtotal + $service_charge;
 
        $taxable_amount = $amount_with_service;
        $tax_amount = (13/100)*$taxable_amount;
        $total_amount = $taxable_amount + $tax_amount;


        return [
            'subtotal'=>$subtotal,
            'discount_percent'=>$discount_percent,
            'service_charge'=>$service_charge,
            'amount_with_service'=>$amount_with_service,
            'tax_amount'=>$tax_amount,
            'total_amount'=>$total_amount,
            'taxable_amount'=>$taxable_amount,
        ];
    }






    public static function adjustFolioAmount($folio_id){

        $folioDetails = \App\Models\FolioDetail::where('folio_id',$folio_id)->get();
        $attributes = [];
        $subtotal = $folioDetails->sum('total');
        $discount_percent = 0; //to adjust
        $service_charge =    env('SERVICE_CHARGE',10) / 100 * $subtotal;
        $amount_with_service = $subtotal + $service_charge;
        $taxable_amount = $amount_with_service;
        $tax_amount = 13 / 100 * $taxable_amount;
        $total_amount = $amount_with_service + $tax_amount;
        
        return [
          'subtotal'=>$subtotal,
          'discount_percent'=>$discount_percent,
          'service_charge'=>$service_charge,
          'amount_with_service'=>$amount_with_service,
          'taxable_amount'=>$taxable_amount,
          'tax_amount'=>$tax_amount,
          'total_amount'=>$total_amount,
        ];


    }
    public static function findStartDate($date){


        $nepaliCalendar = new \App\Helpers\NepaliCalendar();

        $date_arr = explode('-',$date);

        $nepali_date = $nepaliCalendar->eng_to_nep($date_arr[0],$date_arr[1],$date_arr[2]);

        $nepMonthStart=$nepali_date['year'].'-'.$nepali_date['month'].'-'.'1';
        $nepMonthStartExplode=explode('-',$nepMonthStart);
        $engMonthStart=$nepaliCalendar->nep_to_eng($nepMonthStartExplode[0],$nepMonthStartExplode[1],$nepMonthStartExplode[2]);
        $engMonthStart=$engMonthStart['year'].'-'.$engMonthStart['month'].'-'.$engMonthStart['date'];

        $fullNepStartDate = ($nepali_date['month']>3?$nepali_date['year']:($nepali_date['year']-1)).'-'.'04'.'-'.'1';

        $nepStartDate=explode('-',$fullNepStartDate);
        $engStartDate = $nepaliCalendar->nep_to_eng($nepStartDate[0],$nepStartDate[1],$nepStartDate[2]);

        $engStartDate = $engStartDate['year'].'-'.$engStartDate['month'].'-'.$engStartDate['date'];
        $new_date=explode('-',date('Y-m-d'));
        $todayNepDate= $nepaliCalendar->eng_to_nep($new_date[0],$new_date[1],$new_date[2]);

        return [ 'month'=>$engMonthStart,'year'=>$engStartDate,'today_date'=>$todayNepDate ];


    }
}

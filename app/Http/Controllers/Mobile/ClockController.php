<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use App\Models\Clock;
use Flash;
use Illuminate\Http\Request;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER.
 */
class ClockController extends Controller
{
    public function clockin(Request $request)
    {
        $uid = $request->user_id;
        $inloc = $request->latitude.','.$request->longitude;

        $chk_attendance = Attendance::where('user_id', $uid)->where('date_in', date('Y-m-d'))->first();
        if ($chk_attendance) {
            Attendance::where('attendance_id', $chk_attendance->attendance_id)->update(['date_out' => null, 'clocking_status' => 1]);
            $clock = Clock::where('attendance_id', $chk_attendance->attendance_id)->first();
            Clock::where('clock_id', $clock->clock_id)->update(['clockout_time' => null, 'clocking_status' => 1]);
        } else {
            $attendance = new Attendance();
            $attendance->user_id = $uid;
            $attendance->leave_category_id = 0;
            $attendance->date_in = date('Y-m-d');
            $attendance->attendance_status = 1;
            $attendance->clocking_status = 1;
            $attendance->inLoc = $inloc;
            $attendance->save();

            $clock = new Clock();
            $clock->attendance_id = $attendance->id;
            $clock->clockin_time = date('H:i:s');
            $clock->clocking_status = 1;
            $clock->ip_address = Request::getClientIp();
            $clock->save();
        }

        return ['result'=>true];
    }

    public function clockout(Request $request)
    {
        $uid = $request->user_id;
        $outloc = $request->latitude.','.$request->longitude;
        $chk_attendance = Attendance::where('user_id', $uid)->where('clocking_status', '1')->first();
        if ($chk_attendance) {
            Attendance::where('attendance_id', $chk_attendance->attendance_id)->update(['date_out' => date('Y-m-d'), 'clocking_status' => 0, 'outLoc'=>$outloc]);

            $clock = Clock::where('attendance_id', $chk_attendance->attendance_id)->first();
            Clock::where('clock_id', $clock->clock_id)->update(['clockout_time' => date('H:i:s'), 'clocking_status' => 0]);

            $res = true;
        } else {
            $res = false;
        }

        return ['result'=>true];
    }

    public function clocking_status(Request $request)
    {
        $uid = $request->user_id;
        $check = Attendance::where('user_id', $uid)->where('clocking_status', '1')->first();
        if ($check) {
            $time = \Carbon\Carbon::createFromTimeStamp(strtotime($check->created_at))->diffForHumans();

            return ['result'=>true, 'time'=>$time];
        } else {
            return ['result'=>false];
        }
    }
}

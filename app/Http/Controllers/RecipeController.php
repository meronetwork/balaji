<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $recipe;
    public function __construct(Recipe $recipe)
    {
        $this->recipe=$recipe;
    }

    public function index()
    {
        $page_title = "Admin | Recipes";
        $isindex = true;
        $recipes = $this->recipe->orderBy('id','desc')
            ->paginate(30);
        // dd($recipes);
        return view('admin.recipes.index',compact('recipes','isindex','page_title'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = trans('admin/cases/general.page.create.title'); // "Admin | Client | Create";
        $page_description = trans('admin/cases/general.page.create.description'); // "Creating a new client";
        $final_products =  \App\Models\Product::where('is_raw_material',0)
            ->where('enabled',1)->get();
        $products =  \App\Models\Product::where('is_raw_material',1)->where('enabled',1)->get();

//        $customers = \App\Models\Client::where('org_id', Auth::user()->org_id)->get();
//        $users = \App\User::where('enabled', '1')->pluck('first_name', 'id');;

        return view('admin.recipes.create', compact(  'page_title', 'page_description','final_products','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        // ]);
        $attributes = $request->all();
        $recipe =  $this->recipe->create($attributes);


        $recipe_id = $recipe->id;
        $product_id = $request->rproduct_id;
        $quantity = $request->rquantity;


        foreach ($product_id as $key => $value) {
            if ($value != '') {
                $detail = \App\Models\RecipeDetail::create([
                    "recipe_id" => $recipe_id,
                    "raw_prod_id" => $value,
                    "qty" => $quantity[$key],
                ]);
            }
        }
        Flash::success('Recipe created Successfully.');
        return redirect('/admin/recipe');


        Flash::success('Recipe created Successfully.');

        return redirect('/admin/recipe');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Admin | Recipe | Create";

        $page_description = "Creating a new Recipe";

        $recipe=$this->recipe->find($id);
        $final_products =  \App\Models\Product::where('is_raw_material',0)->where('enabled',1)->get();
        $products =  \App\Models\Product::where('is_raw_material',1)->where('enabled',1)->get();

        $orderDetails = \App\Models\RecipeDetail::where('recipe_id',$recipe->id)->get();

        // dd($final_products[0]);

        return view('admin.recipes.edit', compact(  'page_title', 'page_description','recipe','final_products','products','orderDetails'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        // ]);
        $attributes = $request->all();
        $this->recipe->find($id)->update($attributes);

        $product_id = $request->rproduct_id;
        $quantity = $request->rquantity;

        \App\Models\RecipeDetail::where('recipe_id',$id)->delete();
        // dd('asdf');
        foreach ($product_id as $key => $value) {
            if ($value != '') {
                $detail = \App\Models\RecipeDetail::create([
                    "recipe_id" => $id,
                    "raw_prod_id" => $value,
                    "qty" => $quantity[$key],
                ]);
            }
        }

        Flash::success('Recipe Updated Successfully.');

        return redirect('/admin/recipe');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recipe = $this->recipe->find($id);
        $RecipeDetail = \App\Models\RecipeDetail::where('recipe_id',$id)->delete();
//        \TaskHelper::authorizeOrg($supportJob);
        if (! $recipe->isdeletable()) {
            abort(403);
        }

        $recipe->delete();

//        MasterComments::where('type', 'orders')->where('master_id', $id)->delete();

        Flash::success('Recipe deleted.');

//        if (\Request::get('type')) {
//            return redirect('/admin/orders?type='.\Request::get('type'));
//        }

        return redirect('/admin/recipe');
    }

    public function deleteModal($id)
    {
        $error = null;

        $recipe = $this->recipe->find($id);

        if (! $recipe->isdeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Recipe';

        $recipe = $this->recipe->find($id);
        if (\Request::get('type')) {
            $modal_route = route('admin.recipe.delete', ['id' => $recipe->id]).'?type='.\Request::get('type');
        } else {
            $modal_route = route('admin.recipe.delete', ['id' => $recipe->id]);
        }

        $modal_body = 'Are you sure you want to delete this Recipe?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function Search(Request $request){
        $key = $request->search;
        $page_title = "Admin | show";
//        $recipe = Recipe::select('recipes.id','recipes.name','recipes.description','fact_batches.product_id')->leftjoin('products','fact_batches.product_id','=','products.id')->where('fact_batches.id','LIKE','%'.$key.'%')->orWhere('products.name','LIKE','%'.$key.'%')->orWhere('fact_batches.max_qty','LIKE','%'.$key.'%')->orWhere('fact_batches.batch_no','LIKE','%'.$key.'%')->orderBy('fact_batches.id','desc')->paginate(30);
        $recipes = Recipe::select('recipes.id','recipes.name','recipes.description')->where('recipes.id','LIKE','%'.$key.'%')->orWhere('recipes.name','LIKE','%'.$key.'%')->orWhere('recipes.description','LIKE','%'.$key.'%')->orderBy('recipes.id','desc')->paginate(30);
        return view('admin.factoryBatch.index',compact('page_title','recipes'));
    }

}

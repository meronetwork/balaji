<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationRoom;
use App\Models\ReservationGuest;
use App\Models\ReservationDoc;
use App\Models\FolioDetail;
use App\Models\Audit as Audit;
use Flash;
use Auth;
use DB;

class FolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {

        $folios = \App\Models\Folio::orderBy('id', 'desc')
            ->where('reservation_id', $id)
            ->get();

        $reservation = \App\Models\Reservation::find($id);

        $locations = \App\Models\ProductLocation::where('enabled', '1')
            ->where('org_id', \Auth::user()->org_id)
            ->pluck('location_name', 'id')->all();

        $leads = \App\Models\Lead::pluck('name', 'id')->all();

        $fiscalyears = \App\Models\Fiscalyear::orderBy('fiscal_year', 'desc')->pluck('fiscal_year', 'id')->all();

        $page_title = 'Folio of ' . ($reservation->guest->first_name ??'') . '' . ($reservation->guest->last_name??'') . '';

        $page_description = 'Room No ' . $reservation->room_num . ' Check In Date ' . $reservation->check_in . ' Check Out Date ' . $reservation->check_out . '';

        $orders = \App\Models\Orders::orderBy('id', 'desc')
            ->where('org_id', \Auth::user()->org_id)
            ->where('reservation_id', $id)
            ->paginate(20);



        // dd($orders);

        return view('admin.hotel.folio.index', compact('folios', 'page_title', 'page_description', 'locations', 'leads', 'fiscalyears', 'id', 'orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {

        $reservation = \App\Models\Reservation::find($id);
        $page_title = 'Folio Create of ' . $reservation->guest->first_name . '' . $reservation->guest->last_name . '';
        $page_description = 'Room No ' . $reservation->room_num . '';

        $folios = \App\Models\Folio::where('reservation_id', $id)->get();

        $products = \App\Models\Product::where('category_id', '1')->orderBy('id', 'desc')->get();

        $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first();

        $bill_no = \App\Models\Orders::select('bill_no')
            ->where('fiscal_year', $fiscal_year->fiscal_year)
            ->where('outlet_id', env('HOTEL_OUTLET_ID'))
            ->orderBy('bill_no', 'desc')
            ->first();

        $bill_no = $bill_no->bill_no + 1;

        return view('admin.hotel.folio.create', compact('products', 'page_title', 'page_description', 'reservation', 'folios', 'bill_no'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $attributes = $request->all();
        $attributes['org_id'] = \Auth::user()->org_id;
        $attributes['user_id'] = \Auth::user()->id;
        $attributes['reservation_id'] = $id;
        $attributes['total_amount'] = $request->final_total;
        $attributes['tax_amount'] = $request->taxable_tax;
        $attributes['bill_date'] = \Carbon\Carbon::now();


        $ckfiscalyear = \App\Models\Fiscalyear::where('current_year', '1')
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->first();

        if (!$ckfiscalyear)
            return \Redirect::back()->withErrors(['Please update fiscal year <a href="/admin/fiscalyear/create">Click Here</a>!']);

        $attributes['fiscal_year'] = \App\Models\Fiscalyear::where('current_year', 1)->first()->fiscal_year;

        $folio = \App\Models\Folio::create($attributes);

        Audit::log(Auth::user()->id, "Hotel Invoice", "Hotel Folio Bill Created : ID-" . $folio->id . "");

        $product_id = $request->product_id;
        $date = $request->date;
        $price = $request->price;
        $quantity = $request->quantity;
        $tax = $request->tax;
        $tax_amount = $request->tax_amount;
        $total = $request->total;
        $flag = $request->flag;

        foreach ($product_id as $key => $value) {
            if ($value != '') {
                $detail = new FolioDetail();
                $detail->folio_id = $folio->id;
                $temp_product = \App\Models\Article::where('description', $product_id[$key])->first();
                $detail->product_id = $temp_product->id;
                $detail->price = $price[$key];
                $detail->quantity = $quantity[$key];
                $detail->tax = $tax[$key];
                $detail->tax = $tax[$key];
                $detail->tax_amount = $tax_amount[$key];
                $detail->total = $total[$key];
                $detail->date = $date[$key];
                $detail->is_inventory = 1;
                $detail->flag = $flag[$key];
                $detail->save();
            }
        }

        $tax_id_custom = $request->custom_tax_amount;
        $custom_items_date = $request->custom_items_date;
        $custom_items_name = $request->custom_items_name;
        $custom_items_rate = $request->custom_items_rate;
        $custom_items_qty  = $request->custom_items_qty;
        $custom_items_price = $request->custom_items_price;
        $custom_tax_amount = $request->custom_tax_amount;
        $custom_total = $request->custom_total;

        foreach ($custom_items_name as $key => $value) {
            if ($value != '') {

                $detail = new FolioDetail();
                $detail->folio_id = $folio->id;
                $detail->description = $custom_items_name[$key];
                $detail->price = $custom_items_price[$key];
                $detail->quantity = $custom_items_qty[$key];
                $detail->tax = $tax_id_custom[$key];
                $detail->tax_amount = $custom_tax_amount[$key];
                $detail->total = $custom_total[$key];
                $detail->date = $custom_items_date[$key];
                $detail->is_inventory = 0;
                $detail->save();
            }
        }

        Flash::success('Folio Created sucessfully ');

        return redirect('/admin/reservation/folio/' . $id . '/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $folio_id)
    {

        $reservation = \App\Models\Reservation::find($id);

        $page_title = 'Folio Edit of ' . ($reservation->guest->first_name ??'') . '' . ($reservation->guest->last_name??'') . '';
        $page_description = 'Room No ' . $reservation->room_num . '';
        $folio = \App\Models\Folio::where('id', $folio_id)->first();
        $folioDetails = \App\Models\FolioDetail::where('folio_id', $folio->id)->get();
        $products = \App\Models\Product::where('category_id', '1')->orderBy('id', 'desc')->get();
        //dd($folioDetails);


        $mergedIn = $folioDetails->where('merged_from','!=',$folio->id)->where('merged_from','!=',null);

        $mergedOut =  \App\Models\FolioDetail::where('merged_from', $folio->id)->get();


        return view('admin.hotel.folio.edit', compact('reservation', 'page_title', 'page_description', 'folio', 'folioDetails', 'products','mergedOut','mergedIn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validateArticles($products){

        foreach ($products ?? [] as $key => $value) {

            $check =  \App\Models\Article::where('description', $value)->exists();

            if(!$check){

                return false;

            }
        }


        return true;
    }


    public function update(Request $request, $id, $folio_id)
    {

        $folio = \App\Models\Folio::find($folio_id);

        if ($folio->isEditable()) {
            $attributes = $request->all();

            //dd($attributes);

            $attributes['org_id'] = \Auth::user()->org_id;
            $attributes['user_id'] = \Auth::user()->id;
            $attributes['reservation_id'] = $id;
            $attributes['total_amount'] = $request->final_total;
            $attributes['tax_amount'] = $request->taxable_tax;
            if(!$this->validateArticles($request->product_id)){

                Flash::error("Please Insert Articles Properly");
                return redirect()->back();

            }
            if(!$this->validateArticles($request->product_id_new)){

                Flash::error("Please Insert Articles Properly");
                return redirect()->back();

            }

            Audit::log(Auth::user()->id, "Hotel Invoice", "Hotel Folio Bill Updated : ID-" . $folio->id . "");

            \App\Models\FolioDetail::where('folio_id', $folio->id)->delete();


            $product_id = $request->product_id;
            $posted_to_ledger = $request->posted_to_ledger;
            $is_posted = $request->is_posted;
            $date = $request->date;
            $price = $request->price;
            $quantity = $request->quantity;
            $tax = $request->tax;
            $tax_amount = $request->tax_amount;
            $total = $request->total;
            $flag = $request->flag;




            foreach ($product_id ?? [] as $key => $value) {
                if ($value != '') {
                    $detail = new \App\Models\FolioDetail();
                    $detail->folio_id = $folio->id;
                    $temp_product = \App\Models\Article::where('description', $product_id[$key])->first();
                    $detail->product_id = $temp_product->id;
                    $detail->price = $price[$key];
                    $detail->quantity = $quantity[$key];
                    $detail->tax = $tax[$key];
                    $detail->tax_amount = $tax_amount[$key];
                    $detail->total = $total[$key];
                    $detail->posted_to_ledger = $posted_to_ledger[$key];
                    $detail->is_posted = $is_posted[$key];
                    $detail->res_id = $folio->reservation_id;
                    $detail->is_inventory = 1;
                    $detail->date = $date[$key];
                    $detail->flag = $flag[$key];
                    // dd($detail);
                    $detail->save();
                }
            }

            $description_custom = $request->description_custom;
            $posted_to_ledger_custom = $request->posted_to_ledger_custom;
            $is_posted_custom = $request->is_posted_custom;
            $date_custom = $request->date_custom;
            $price_custom = $request->price_custom;
            $quantity_custom = $request->quantity_custom;
            $tax_custom = $request->tax_custom;
            $tax_amount_custom = $request->tax_amount_custom;
            $total_custom = $request->total_custom;
            $flag_custom = $request->flag_custom;

            foreach ($description_custom as $key => $value) {
                if ($value != '') {
                    $detail = new \App\Models\FolioDetail();
                    $detail->folio_id = $folio->id;
                    $detail->description = $description_custom[$key];
                    $detail->price = $price_custom[$key];
                    $detail->quantity = $quantity_custom[$key];
                    $detail->tax = $tax_custom[$key] ?? null;
                    $detail->tax_amount = $tax_amount_custom[$key] ?? null;
                    $detail->total = $total_custom[$key];
                    $detail->posted_to_ledger = $posted_to_ledger_custom[$key];
                    $detail->is_posted = $is_posted_custom[$key];
                    $detail->is_inventory = 0;
                    $detail->date = $date_custom[$key];
                    $detail->flag = $flag_custom[$key];
                    $detail->res_id = $folio->reservation_id;
                    //dd($detail);
                    $detail->save();
                }
            }



            if ($request->product_id_new != null) {
                $product_id_new = $request->product_id_new;

                $date_new = $request->date_new;
                $ticket_new = $request->ticket_new;
                $price_new = $request->price_new;
                $quantity_new = $request->quantity_new;
                $flight_date_new = $request->flight_date_new;
                $tax_new = $request->tax_new;
                $tax_amount_new = $request->tax_amount_new;
                $total_new = $request->total_new;
                $flag_new = $request->flag_new;
                $is_posted_new = $request->is_posted_new;

                foreach ($product_id_new as $key => $value) {

                    $detail = new \App\Models\FolioDetail();
                    $detail->folio_id = $folio->id;
                    $temp_product_new = \App\Models\Article::where('description', $product_id_new[$key])->first();
                    $detail->product_id = $temp_product_new->id;
                    $detail->price = $price_new[$key];
                    $detail->quantity = $quantity_new[$key] ?? null;
                    $detail->tax = $tax_new[$key] ?? null;
                    $detail->tax_amount = $tax_amount_new[$key] ??  null;
                    $detail->total = $total_new[$key];
                    $detail->is_inventory = 1;
                    $detail->is_posted = $is_posted_new[$key];
                    $detail->date = $date_new[$key];
                    $detail->flag = $flag_new[$key];
                    $detail->save();
                }
            }


            // Custom items Start
            $custom_items_name_new = $request->custom_items_name_new;
            $custom_items_date_new = $request->custom_items_date_new;
            $custom_ticket_new = $request->custom_ticket_new;
            $custom_items_price_new = $request->custom_items_price_new;
            $custom_items_qty_new = $request->custom_items_qty_new;
            $custom_flight_date_new = $request->custom_flight_date_new;
            $tax_id_custom_new = $request->tax_id_custom_new;
            $custom_tax_amount_new = $request->custom_tax_amount_new;
            $custom_total_new = $request->custom_total_new;

            if (!empty($custom_items_name_new)) {
                foreach ($custom_items_name_new as $key => $value) {

                    $detail = new \App\Models\FolioDetail();
                    $detail->folio_id = $folio->id;
                    $detail->description = $custom_items_name_new[$key];
                    $detail->price = $custom_items_price_new[$key];
                    $detail->quantity = $custom_items_qty_new[$key];
                    $detail->tax = $tax_id_custom_new[$key];
                    $detail->tax_amount = $custom_tax_amount_new[$key];
                    $detail->total = $custom_total_new[$key];
                    $detail->is_inventory = 0;
                    $detail->date = $custom_items_date_new[$key];
                    $detail->save();
                }
            }

            $folio->update($attributes);

            Flash::success('Order updated Successfully.');

            return redirect()->back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {

        $folio = \App\Models\Folio::find($id);

        //dd($folio);

        if (!$folio->isdeletable()) {
            abort(403);
        }

        $folio->delete($id);

        \App\Models\FolioDetail::where('folio_id', $id)->delete($id);

        Flash::success('Folio successfully deleted.');


        return redirect()->back();
    }

    /**
     * Delete Confirm
     *
     * @param   int   $id
     * @return  View
     */
    public function getModalDelete($id)
    {

        $folio = \App\Models\Folio::find($id);

        if (!$folio->isdeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Folio';

        $folio = \App\Models\Folio::find($id);

        $modal_route = route('admin.hotel.folio.orders.delete', array('id' => $folio->id));

        $modal_body = 'Are you sure you want to delete this folio?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function getroomRate($id)
    {
        $room = \App\Models\Room::select('room_types.room_rate')->leftjoin('room_types', 'room.roomtype_id', '=', 'room_types.roomtype_id')->where('room.room_id', $id)->get();
        return response()->json($room);
    }


    public function printInvoice($id)
    {

        $ord = \App\Models\Folio::find($id);

        $orderDetails = \App\Models\FolioDetail::where('folio_id', $ord->id)->get();

        $imagepath = \Auth::user()->organization->logo;

        $print_no = \App\Models\FolioPrint::where('folio_id', $id)->count();

        $attributes = new \App\Models\FolioPrint();
        $attributes->folio_id = $id;
        $attributes->printed_date = \Carbon\Carbon::now();
        $attributes->printed_by = \Auth::user()->id;
        $attributes->save();

        $ord->update(['is_bill_printed' => 1]);

        return view('admin.hotel.folio.print', compact('ord', 'imagepath', 'orderDetails', 'print_no'));
    }

    public function printTaxInvoice($id)
    {

        $ord = \App\Models\Folio::find($id);
        $orderDetails = \App\Models\FolioDetail::where('folio_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;
        return view('admin.hotel.folio.taxprint', compact('ord', 'imagepath', 'orderDetails'));
    }

    public function generatePDF($id)
    {
        $ord = \App\Models\Folio::find($id);
        $orderDetails = \App\Models\FolioDetail::where('folio_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;

        $pdf = \PDF::loadView('admin.hotel.folio.generateInvoicePDF', compact('ord', 'imagepath', 'orderDetails'));
        $file = $id . '_' . $ord->name . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';

        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }
        return $pdf->download($file);
    }


    public function allFolioSales(Request $request)
    {

        $page_title = "Admin | salesBook";
        $op = \Request::get('op');
        $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first()->fiscal_year;
        if (\Request::has('filter')) {
            if ($request->filter == 'nep') {                 //for nepali
                $startdate = $request->nepstartdate;
                $enddate = $request->nependdate;
                $cal = new \App\Helpers\NepaliCalendar();
                $startdate = $cal->nep_to_eng_digit_conversion($startdate);
                $date = $cal->nep_to_eng($startdate[0], $startdate[1], $startdate[2]);
                $startdate = $date['year'] . '-' . $date['month'] . '-' . $date['date'];
                $enddate = $cal->nep_to_eng_digit_conversion($enddate);
                $date = $cal->nep_to_eng($enddate[0], $enddate[1], $enddate[2]);
                $enddate = $date['year'] . '-' . $date['month'] . '-' . $date['date'];
            } else {
                $startdate = $request->engstartdate;
                $enddate = $request->engenddate;
            }
            if ($op == 'pdf') {
                $sales_book = \App\Models\Invoice::where('bill_date', '>=', $startdate)
                    ->where('bill_date', '<=', $enddate)
                    ->get();
                $pdf = \PDF::loadView('pdf.filteredsales', compact('sales_book', 'fiscal_year', 'startdate', 'enddate'));
                $file = 'Report_salebook_filtered' . date('_Y_m_d') . '.pdf';
                if (\File::exists('reports/' . $file)) {
                    \File::Delete('reports/' . $file);
                }
                return $pdf->download($file);
            }
            if ($op == 'excel') {
                $data = DB::select("select invoice.id as SN , invoice.bill_no as 'Bill Num', invoice.customer_name as 'Customers Name',invoice.customer_pan as 'Customers PAN No','' as 'Total Sales','' as 'Non Tax Sales','' as 'Export Sales','' as Discount ,invoice.taxable_amount as Amount,invoice.tax_amount as 'Tax(Rs)' from invoice where invoice.bill_date >= '$startdate' AND invoice.bill_date <= '$enddate' ");
                $data = json_decode(json_encode($data), true);
                return \Excel::create('invoice', function ($excel) use ($data) {
                    $excel->sheet('mySheet', function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('csv');
            }
            $sales_book = \App\Models\Invoice::where('bill_date', '>=', $startdate)
                ->where('bill_date', '<=', $enddate)
                ->paginate(50);
        } else {

            if ($op == 'pdf') {
                $sales_book = \App\Models\Invoice::get();
                $pdf = \PDF::loadView('pdf.filteredsales', compact('sales_book', 'fiscal_year'));
                $file = 'Report_salebook_filtered' . date('_Y_m_d') . '.pdf';
                if (\File::exists('reports/' . $file)) {
                    \File::Delete('reports/' . $file);
                }
                return $pdf->download($file);
            }
            if ($op == 'excel') {
                $data = DB::select("select invoice.id as SN , invoice.bill_no as 'Bill Num', invoice.customer_name as 'Customers Name',invoice.customer_pan as 'Customers PAN No','' as 'Total Sales','' as 'Non Tax Sales','' as 'Export Sales','' as Discount ,invoice.taxable_amount as Amount,invoice.tax_amount as 'Tax(Rs)' from invoice ");
                $data = json_decode(json_encode($data), true);
                return \Excel::create('invoice', function ($excel) use ($data) {
                    $excel->sheet('mySheet', function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('csv');
            }

            $sales_book = \App\Models\Invoice::paginate(50);
        }
        // return  response()->json($purchase_book);


        return view('admin.hotel.folio.allfoliosales', compact('sales_book', 'page_title', 'page_description', 'locations', 'leads', 'fiscalyears', 'id', 'orders', 'fiscal_year'));
    }

    public function SalesBookByMonths($month)
    {

        $page_title = "Admin | purchasebook";
        $fiscal_year = (\App\Models\Fiscalyear::where('current_year', '1')->first())->fiscal_year;
        $fiscal_y = substr($fiscal_year, 0, 4);
        $cal = new \App\Helpers\NepaliCalendar();
        $days_list = $cal->bs;
        if ($month < 4)
            $year = $fiscal_y + 1;
        else
            $year = $fiscal_y;
        $year_array = $days_list[$year - 2000];
        $start = $cal->nep_to_eng($year, $month, 1);
        $end = $cal->nep_to_eng($year, $month, $year_array[(int)$month]);
        $startdate = $start['year'] . '-' . $start['month'] . '-' . $start['date'];
        $enddate = $end['year'] . '-' . $end['month'] . '-' . $end['date'];
        if (\Request::has('op')) {
            $op = \Request::get('op');
            $months = (int) $month;
            if ($op == 'pdf') {
                $sales_book = \App\Models\Invoice::where('bill_date', '>=', $startdate)
                    ->where('bill_date', '<=', $enddate)
                    ->get();
                $pdf = \PDF::loadView('pdf.filteredsales', compact('sales_book', 'fiscal_year', 'startdate', 'enddate'));
                $file = 'Report_salebook_filtered' . date('_Y_m_d') . '.pdf';
                if (\File::exists('reports/' . $file)) {
                    \File::Delete('reports/' . $file);
                }
                return $pdf->download($file);
            }
            if ($op == 'excel') {
                $data = DB::select("select invoice.id as SN , invoice.bill_no as 'Bill Num', invoice.customer_name as 'Customers Name',invoice.customer_pan as 'Customers PAN No','' as 'Total Sales','' as 'Non Tax Sales','' as 'Export Sales','' as Discount ,invoice.taxable_amount as Amount,folio.tax_amount as 'Tax(Rs)' from invoice where invoice.bill_date >= '$startdate' AND invoice.bill_date <= '$enddate' ");
                $data = json_decode(json_encode($data), true);
                return \Excel::create('folio', function ($excel) use ($data) {
                    $excel->sheet('mySheet', function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('csv');
            }
        }
        $sales_book = \App\Models\Invoice::where('bill_date', '>=', $startdate)
            ->where('bill_date', '<=', $enddate)
            ->paginate(50);
        return view('admin.hotel.folio.allfoliosales', compact('sales_book', 'page_title', 'fiscal_year'));
    }


    public function returnInvoiceBook(Request $request)
    {

        $page_title = "Return Hotel Sales Book";
        $page_description = 'Return Hotel Sales Book';
        $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();
        Audit::log(Auth::user()->id, "Hotel Invoice", "Acessed Hotel Bill Return Page");

        return view('admin.hotel.folio.returninvoice', compact('page_title', 'users', 'page_description'));
    }

    public function returnInvoiceBookList(Request $request)
    {

        $page_title = 'Admin | Hotel | Sales Return Book';
        $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();

        $invoice = \App\Models\Invoice::select('invoice.*', 'invoice_meta.*')
            ->leftjoin('invoice_meta', 'invoice_meta.invoice_id', '=', 'invoice.id')
            ->where('invoice.bill_date', '>=', $request->start_date)
            ->where('invoice.bill_date', '<=', $request->end_date)
            ->where('invoice.org_id', \Auth::user()->org_id)
            ->where('invoice_meta.is_bill_active', 0)
            ->where(function ($query) use ($request) {
                if ($request->user_id) {
                    return $query->where('invoice.user_id', $request->user_id);
                }
            })
            ->get();

        $request = $request->all();

        Audit::log(Auth::user()->id, "Hotel Invoice", "Acessed Data of Hotel Bill Return Page");



        return view('admin.hotel.folio.returninvoice', compact('page_title', 'users', 'page_description', 'invoice', 'request'));
    }


    public function voidfromedit(Request $request, $id)
    {

        $reason = $request->reason;
        $folio = \App\Models\Folio::find($id);

        $makevoid = ['void_reason' => $reason, 'is_bill_active' => 0];

        \App\Models\Folio::find($id)->update($makevoid);


        Flash::success('Folio sucessfully marked as void');

        return redirect()->back();
    }

    public function voidConfirm($id)
    {

        $error = null;

        $res = \App\Models\Folio::find($id);

        $modal_title = "Void This Folio";

        $modal_route = route('admin.folio.makevoid', $res->id);

        return view('modal_void_reason', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function InvoiceTypeModal($id)
    {

        $error = null;
        $modal_title = "Select Invoice Type";


        $modal_route = route('admin.invoice.type.select.post', $id);

        return view('modal_invoice_type', compact('error', 'modal_route', 'modal_title','id'));
    }

    public function  InvoiceTypePost(Request $request, $id)
    {

        $invoice_type = $request->invoice_type;
        //dd($invoice_type);

        $folio_id = $request->folio_id;

        //  dd($folio_id);

        if ($folio_id != $id) {

            Flash::error('Id Missmatched');
            return Redirect::back();
        }

        if ($invoice_type == "summarized") {

            $ord = \App\Models\Orders::where('id', $id)->groupBy('id')->first();

            $flag_details = \App\Models\OrderDetail::select('flag', 'date')->where('order_id', $ord->id)->distinct('flag')
                ->groupBy('flag')
                ->get();

            $orderDetails = array();

            foreach ($flag_details as $fd) {

                $folio_details_flags = \App\Models\OrderDetail::where('order_id', $ord->id)->where('flag', $fd->flag)->get();

                $folio_detail_total = \App\Models\OrderDetail::where('order_id', $ord->id)->where('flag', $fd->flag)->sum('total');

                array_push(
                    $orderDetails,
                    [

                        'description' => $fd->flag,
                        'price' => $folio_detail_total,
                        'total' => $folio_detail_total,
                        'date' => $fd->date

                    ]
                );
            }


            $print_no = \App\Models\POSPrint::where('order_id', $id)->count();

            $attributes = new \App\Models\POSPrint();
            $attributes->order_id = $id;
            $attributes->printed_date = \Carbon\Carbon::now();
            $attributes->printed_by = \Auth::user()->id;
            $attributes->save();


            return view('admin.hotel.folio.invoiceTypeSummarized', compact('ord', 'orderDetails', 'print_no'));
        } else {

            $ord = \App\Models\Orders::where('id', $id)->groupBy('id')->first();

            $orderDetails = \App\Models\OrderDetail::where('order_id', $ord->id)->get();

            //  dd($orderDetails);

            $print_no = \App\Models\POSPrint::where('order_id', $id)->count();

            // dd($print_no);

            $attributes = new \App\Models\POSPrint();
            $attributes->order_id = $id;
            $attributes->printed_date = \Carbon\Carbon::now();
            $attributes->printed_by = \Auth::user()->id;
            $attributes->save();

            // dd($orderDetails);
            return view('admin.hotel.folio.invoiceTypeDescribed', compact('ord', 'orderDetails', 'print_no'));
        }
    }

    public function estimatePrint($reservation_id){

        $ord = \App\Models\Folio::where('reservation_id', $reservation_id)->first();

        $orderDetails = \App\Models\FolioDetail::where('folio_id', $ord->id)->get();

        $print_no = 1;

        $reservation = $ord->reservation;

        return view('admin.hotel.reservation.estimate_print', compact('ord', 'orderDetails', 'print_no','reservation'));
    }

    public function hotelcreditnote($id)
    {
        $ord = \App\Models\Invoice::find($id);
        $page_title = 'Credit Note';
        $page_description = 'View Order';
        $orderDetails =  \App\Models\InvoiceDetail::where('invoice_id', $ord->id)->get();
        //dd($orderDetails);
        $imagepath = \Auth::user()->organization->logo;
        // dd($imagepath);

        return view('admin.hotel.folio.hotel_credit_notepdf', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
    }


    public function merge($resid){

        $reservation = \App\Models\Reservation::where('parent_res',null)
                ->where('id','!=',$resid)
                ->where('reservation_status','3')->get();


        return view('admin.hotel.folio.merge',compact('resid','reservation'));
    }

    public function split($folioId){


        $folio = \App\Models\Folio::find($folioId);

        $order_details = \App\Models\FolioDetail::with('article')
                        ->where('folio_id',$folioId)
                        ->get();

        $id = $folioId;

        return view('admin.hotel.folio.split',compact('order_details','folio','id'));

    }



    public function splitPost(Request $request,$folioId){

        $attributes = $request->all();

        $fromFolio = \App\Models\FolioDetail::whereIn('id',$request->from_product)
                    ->get()
                    ->toArray();

        $toFolio = \App\Models\FolioDetail::whereIn('id',$request->to_product)->get()->toArray();

        $toInsertfromFolio = [];

        foreach ($fromFolio as $key => $value) {
            unset($value['id']);
            $toInsertfromFolio[] = $value;
        }

        $folio = \App\Models\Folio::find($folioId);
        //remove all folio details form current folio
        \App\Models\FolioDetail::where('folio_id',$folioId)->delete();
        \App\Models\FolioDetail::insert($toInsertfromFolio);
        $folio->update(\ReservationHelper::adjustFolioAmount($folio->id));
        //for splted folio

        $reservation = $folio->reservation;
        //create new folio where we need to split
        $folio =   app('\App\Http\Controllers\Hotel\NightAuditController')->createFolio($reservation,'split');

        $toInserttoFolio = [];
        foreach ($toFolio as $key => $value) {
            unset($value['id']);
            $value['folio_id'] = $folio->id;
            $toInserttoFolio [] = $value;
        }
        \App\Models\FolioDetail::insert($toInserttoFolio);
        //adustfolioamount
        $folio->update(\ReservationHelper::adjustFolioAmount($folio->id));

        Flash::success("FOLIO Successfully SPLITTED");
        return redirect()->back();
    }

    public function mergePost(Request $request,$resid){

        $fromFolio =  \App\Models\Folio::where('reservation_id',$resid)->first();

        $toFolio = \App\Models\Folio::where('reservation_id',$request->res_id)->first();

        if(!$toFolio || !$toFolio){

            Flash::error("USSS NO FOLIO SETTLEd");

            return redirect()->back();

        }
        \App\Models\FolioDetail::where('folio_id',$fromFolio->id)->update(['folio_id'=>$toFolio->id,'merged_from'=>$fromFolio->id]);

        app('\App\Http\Controllers\Hotel\NightAuditController')->adjustFolioAmount($fromFolio->reservation_id);

        app('\App\Http\Controllers\Hotel\NightAuditController')->adjustFolioAmount($toFolio->reservation_id);

        Flash::success("FOLIO Successfully Merged");

        return redirect()->back();

    }



}

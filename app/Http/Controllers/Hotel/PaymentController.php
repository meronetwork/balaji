<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use App\Models\PaymentType;
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexpaymentType()
    {
        $page_title = 'Admin | payment';
        $payment_type = PaymentType::paginate(30);
        return view('admin.hotel.payment.payment-type-index',compact('payment_type','page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createpaymentType()
    {   
        $page_title = 'Admin | payment';
        $description = "Create a new payment";
        return view('admin.hotel.payment.payment-type-create',compact('description','page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storepaymentType(Request $request)
    {
        $payment_type = $request->all();
        PaymentType::create($payment_type);
        Flash::success("PaymentType sucessfully created");
        return redirect('/admin/hotel/payment-type-index/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editpaymentType($id)
    {
       $page_title = 'Admin | payment';
       $description = "Edit a new payment";
       $edit = PaymentType::where('payment_type_id',$id)->first();
       return view('admin.hotel.payment.payment-type-create',compact('edit','page_title','description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatepaymentType(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);

        PaymentType::where('payment_type_id',$id)->update($update);
        Flash::success("PaymentType sucessfully updated");
        return redirect('/admin/hotel/payment-type-index/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPaymentType($id)
    {
        PaymentType::where('payment_type_id',$id)->delete();
        Flash::success("PaymentType sucessfully deleted");
        return redirect('/admin/hotel/payment-type-index/');
    }
        public function getModalDeletePaymentType($id)
    {
        $error = null;
        $payment_type = PaymentType::where('payment_type_id',$id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete payment type";
        $modal_body ="Are you sure that you want to delete payment type id ".$payment_type->payment_type_id ." with the name ".$payment_type->payment_name."? This operation is irreversible";
        $modal_route = route('admin.hotel.payment-type-delete',$payment_type->payment_type_id);
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;
use Flash;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use DB;
use App\Models\Audit as Audit;

class PosReportController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function __construct(){

      $this->pay_method = [
        'cash'=>'Cash',
        'check'=>'Check',
        'travel-agent'=>'Travel Agent',
        'city-ledger'=>'City Ledger',
        'complementry'=>'Complementry',
        'staff'=>'Staff',
        'credit-cards'=>'Credit  Cards',
        'room'    =>'Room',
        'e-sewa'=>'e-sewa'
    ];

  }
  public function daily()
  {

    $page_title = 'Admin | Pos | Sales';
    $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();
    $outlets = \App\Models\PosOutlets::where('enabled', '1')->get();
    return view('admin.hotel.posreport.sales', compact('page_title', 'users', 'outlets'));
  }

  public function dailyreport(Request $request)
  {

    $page_title = 'Admin | Pos | Sales';
    $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();
    $outlets = \App\Models\PosOutlets::where('enabled', '1')->get();
    $sales = \App\Models\Orders::where('bill_date', '>=', $request->start_date)->where('bill_date', '<=', $request->end_date)
      ->where('outlet_id', $request->outlet_id)

      ->where(function ($query) use ($request) {
        if ($request->user_id) {
          return $query->where('user_id', $request->user_id);
        }
      })
      ->get();

    $request = $request->all();
    return view('admin.hotel.posreport.sales', compact('page_title', 'users', 'outlets', 'sales', 'request'));
  }

  public function products()
  {

    $page_title = 'Admin | Pos | Sales By Prod';
    $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();
    $products_cat =   \App\Models\ProductTypeMaster::where('enabled', '1')->get();
    return view('admin.hotel.posreport.sale_product', compact('page_title', 'users', 'products_cat'));
  }

  public function productsreport(Request $request)
  {

    $page_title = 'Admin | Pos | ales By Prod';
    $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();
    $products_cat = \App\Models\ProductTypeMaster::where('enabled', '1')->get();


    $select_cat =  \App\Models\ProductTypeMaster::find($request->product_type_id);

    $sales  = \App\Models\OrderDetail::select('fin_orders.bill_date', 'fin_order_detail.total', 'product_categories.id', 'products.name', 'product_categories.name as pro_cat', 'products.product_type_id', 'fin_order_detail.quantity')
      ->leftjoin('products', 'products.id', '=', 'fin_order_detail.product_id')
      ->leftjoin('product_categories', 'products.category_id', '=', 'product_categories.id')
      ->leftjoin('fin_orders', 'fin_orders.id', '=', 'fin_order_detail.order_id')
      ->where('products.product_type_id', $request->product_type_id)
      ->where('fin_orders.bill_date', '>=', $request->start_date)
      ->where('fin_orders.bill_date', '<=', $request->end_date)
      ->where(function ($query) use ($request) {
        if ($request->user_id) {
          return $query->where('fin_orders.user_id');
        }
      })
      ->get()->groupBy('pro_cat');

    $request = $request->all();
    return view('admin.hotel.posreport.sale_product', compact('page_title', 'users', 'outlets', 'sales', 'products_cat', 'select_cat', 'request'));
  }


  private function convertdate($date)
  {
    $date = explode('-', $date);
    $cal = new \App\Helpers\NepaliCalendar();
    $converted = $cal->eng_to_nep($date[0], $date[1], $date[2]);
    $nepdate =  $converted['year'] . '.' . $converted['nmonth'] . '.' . $converted['date'];
    return $nepdate;
  }

  public function possalesreturn()
  {

    $page_title = "Admin | Sales | Return";
    $fiscalyear = \App\Models\Fiscalyear::orderBy('id', 'desc')->get();

    if (\Auth::user()->hasRole('admins')) {
      $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
        ->where('enabled', 1)
        ->get();
    } else {
      $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
      $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
        ->orderBy('id', 'DESC')
        ->where('enabled', 1)
        ->get();
    }


//    $credit_note_no =  (\App\Models\OrderMeta::orderBy('credit_note_no', 'desc')
//        ->where('credit_note_no', '!=', 'null')->first()->credit_note_no ?? 0) + 1;


    Audit::log(Auth::user()->id, "POS Billing", "Accessed Pos Bill Return Page");

    return view('admin.hotel.posreport.possalesreturn', compact('page_title', 'fiscalyear', 'outlets'));
  }

  public function possalesreturnpost(Request $request)
  {

      DB::beginTransaction();
      if (\Request::get('bill_type') == 'abbreviated') {
          $orders = \App\Models\AbbrOrders::orderBy('id', 'desc')
              ->where('fiscal_year', $request->fiscal_year)
              ->where('outlet_id', $request->outlet_id)
              ->where('bill_no', $request->bill_no)->first();

          // dd($orders);
          $ordermeta = \App\Models\AbbrOrderMeta::orderBy('id', 'desc')->where('order_id', $orders->id)->first();

          if (!$ordermeta->settlement) {

              Flash::error("Bill not settled");

              return redirect()->back();
          }


          if (!$ordermeta->settlement) {

              Flash::error("Please Settle Bill first");

              return redirect()->back();
          }


          if ($orders) {

              $entriesitem = \App\Models\Entryitem::where('entry_id', $ordermeta->settle_entry_id)->where('dc', 'D')->first();

              if ($entriesitem) {
                  $entry_type_id = \FinanceHelper::get_entry_type_id('creditnote');
                  $attributes_order['entrytype_id'] = $entry_type_id; //crdeitnotes
                  $attributes_order['tag_id'] = 3; //crdeitnotes
                  $attributes_order['user_id'] = \Auth::user()->id;
                  $attributes_order['org_id'] = \Auth::user()->org_id;
                  $attributes_order['number'] = $entriesitem->number;
                  $attributes_order['resv_id'] = $orders->reservation_id;
                  $attributes_order['order_id'] = $orders->id;
                  $attributes_order['source'] = 'Sales_Return';
                  $attributes_order['date'] = \Carbon\Carbon::today();
                  $attributes_order['notes'] = "Credit Return: " . $orders->id . "";
                  $attributes_order['bill_no'] = 'AI-' . $orders->outlet->shortname . $orders->bill_no;

                  $attributes_order['dr_total'] = $orders->total_amount;
                  $attributes_order['cr_total'] = $orders->total_amount;
                  $type = \App\Models\Entrytype::find($entry_type_id);
                  $attributes_order['number'] = \TaskHelper::generateId($type);
                  $entry = \App\Models\Entry::create($attributes_order);

                  $cash_amount = new \App\Models\Entryitem();
                  $cash_amount->entry_id = $entry->id;
                  $cash_amount->dc = 'C';
                  $cash_amount->ledger_id = $entriesitem->ledger_id;
                  $cash_amount->amount = $orders->total_amount;
                  $cash_amount->user_id = \Auth::user()->id;
                  $cash_amount->org_id = \Auth::user()->org_id;
                  $cash_amount->narration = 'being sales return made';
                  $cash_amount->save();

                  $cash_amount = new \App\Models\Entryitem();
                  $cash_amount->entry_id = $entry->id;
                  $cash_amount->dc = 'D';
                  $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_SALES_RETURN');
                  $cash_amount->amount = $orders->total_amount;
                  $cash_amount->user_id = \Auth::user()->id;
                  $cash_amount->org_id = \Auth::user()->org_id;
                  $cash_amount->narration = 'being sales return made';
                  $cash_amount->save();
              }


              //UPDATING THE ORDERS TABLE
              $ordermeta->update(['is_bill_active' => 0, 'void_reason' => $request->void_reason, 'cancel_date' => $request->cancel_date, 'credit_note_no' => $request->credit_note_no, 'is_realtime' => 1, 'credit_user_id' => \Auth::user()->id]);

              DB::commit();
              Flash::success('Bill Returned Successfully.');

              return redirect()->back();
          }
           else {
              DB::commit();
              Audit::log(Auth::user()->id, "POS Billing", "Bill Is Not Found: ID-" . $orders->id . "");
              \Flash::warning('No Bill Found Of this Number');
              return redirect()->back();
          }
      }



          $orders = \App\Models\Orders::orderBy('id', 'desc')
        ->where('fiscal_year', $request->fiscal_year)
        ->where('outlet_id', $request->outlet_id)
        ->where('bill_no', $request->bill_no)->first();

    // dd($orders);
    $ordermeta = \App\Models\OrderMeta::orderBy('id', 'desc')->where('order_id', $orders->id)->first();

    if(!$ordermeta->settlement){

        Flash::error("Bill not settled");

        return redirect()->back();
    }



    if(!$ordermeta->settlement){

      Flash::error("Please Settle Bill first");

      return redirect()->back();
    }


    if ($orders) {

      if ($orders->pos_customer_id) {
        $guest_name  = $orders->client->name;
        $guest_pan  = $orders->client->vat;
      } elseif ($orders->reservation_id) {
        if ($orders->reservation->company_id) {
          $guest_name = $orders->reservation->client->name;
          $guest_pan  = $orders->reservation->client->vat;
        } else {
          $guest_name = $orders->reservation->guest_name;
          $guest_pan  = $orders->client->vat;
        }
      } else {

        if ($orders->folio->reservation->company_id) {
          $guest_name = $orders->folio->reservation->client->name;
          $guest_pan  = $orders->folio->reservation->client->vat;
        } else {
          $guest_name = $orders->folio->reservation->guest_name;
          $guest_pan  = $orders->folio->client->vat;
        }
      }


      $bill_date_nepali = $this->convertdate($orders->bill_date);
      $cancel_date = $this->convertdate($request->cancel_date);

      $bill_today_date_nep = $this->convertdate(date('Y-m-d'));

        $invoice_no='CN-TI-'.$orders->outlet->short_name . $orders->bill_no;


        //POSTING DATA TO IRD
      $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'), "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $guest_pan, "fiscal_year" => $orders->fiscal_year, "buyer_name" => $guest_name, "ref_invoice_number" => $invoice_no, "credit_note_date" => $cancel_date, "credit_note_number" => $request->credit_note_no, "reason_for_return" => $request->void_reason, "total_sales" => $orders->total_amount, "taxable_sales_vat" => $orders->taxable_amount, "vat" => $orders->tax_amount, "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0, "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0, "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);

      $irdsync = new \App\Models\NepalIRDSync();
      $response = $irdsync->returnbill($data);

      if ($response == 200 || $response == 201  ) {

        $entriesitem  = \App\Models\Entryitem::where('entry_id', $ordermeta->settle_entry_id)->where('dc', 'D')->first();

        if ($entriesitem) {
            $entry_type_id=\FinanceHelper::get_entry_type_id('creditnote');
          $attributes_order['entrytype_id'] = $entry_type_id; //crdeitnotes
          $attributes_order['tag_id'] = 3; //crdeitnotes
          $attributes_order['user_id'] = \Auth::user()->id;
          $attributes_order['org_id'] = \Auth::user()->org_id;
          $attributes_order['number'] = $entriesitem->number;
          $attributes_order['resv_id'] = $orders->reservation_id;
          $attributes_order['order_id'] = $orders->id;
          $attributes_order['source'] = 'Sales_Return';
          $attributes_order['date'] = \Carbon\Carbon::today();
          $attributes_order['notes'] = "Credit Return: " . $orders->id . "";

          $attributes_order['dr_total'] = $orders->total_amount;
          $attributes_order['cr_total'] = $orders->total_amount;
            $type=\App\Models\Entrytype::find($entry_type_id);
            $attributes_order['number'] = \TaskHelper::generateId($type);
          $entry = \App\Models\Entry::create($attributes_order);

          $cash_amount = new \App\Models\Entryitem();
          $cash_amount->entry_id = $entry->id;
          $cash_amount->dc = 'C';
          $cash_amount->ledger_id = $entriesitem->ledger_id;
          $cash_amount->amount = $orders->total_amount;
            $cash_amount->user_id = \Auth::user()->id;
            $cash_amount->org_id = \Auth::user()->org_id;
          $cash_amount->narration = 'being sales return made';
          $cash_amount->save();

          $cash_amount = new \App\Models\Entryitem();
          $cash_amount->entry_id = $entry->id;
          $cash_amount->dc = 'D';
          $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_SALES_RETURN');
          $cash_amount->amount = $orders->total_amount;
            $cash_amount->user_id = \Auth::user()->id;
            $cash_amount->org_id = \Auth::user()->org_id;
          $cash_amount->narration = 'being sales return made';
          $cash_amount->save();
        }


        //UPDATING THE ORDERS TABLE
        $ordermeta->update(['is_bill_active' => 0, 'void_reason' => $request->void_reason, 'cancel_date' => $request->cancel_date, 'credit_note_no' => $request->credit_note_no, 'is_realtime' => 1, 'credit_user_id' => \Auth::user()->id]);

        //UPDATE AUDIT LOG
        if($response == 200){
           Flash::success('Successfully Returned from IRD. Code: ' . $response . '');
          Audit::log(Auth::user()->id, "POS Billing", "Bill Is Returned To IRD: ID-" . $orders->id . " Response :" . $response . "");
        }else{
           Flash::success('Bill Returned Without IRD');
          Audit::log(Auth::user()->id,"POS Billing", "Bill is returned without IRD CONNECTION");


        }
        DB::commit();

        return redirect()->back();
      } else {

        if ($response == 101) {
          $ordermeta->update(['is_bill_active' => 0, 'is_realtime' => 1]);
        } else {
          $ordermeta->update(['is_realtime' => 1]);
        }

        DB::commit();
        Audit::log(Auth::user()->id, "POS Billing", "Bill Is Returned To IRD: ID-" . $orders->outlet->short_name . '' . $orders->bill_no . " Response :" . $response . "");
        Flash::error('Return Cannot Due to Response Code: ' . $response . '');
        return redirect()->back();
      }
    } else {
      DB::commit();
      Audit::log(Auth::user()->id, "POS Billing", "Bill Is Not Found: ID-" . $orders->id . "");
      \Flash::warning('No Bill Found Of this Number');
      return redirect()->back();
    }
  }


  public function posIrdIndex(Request $request)
  {

    $page_title = "Admin | POS | SYNC WITH IRD";

    $fiscal_year = \App\Models\Fiscalyear::where('current_year', 1)->first()->fiscal_year;

    $posbills = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.*')
      ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
      ->where('fin_orders.fiscal_year', $fiscal_year)
      ->where('fin_orders_meta.sync_with_ird', 0)
      ->groupBy('fin_orders.id')
      ->get();

    //dd($posbills);

    return view('admin.hotel.posreport.possyncwithird', compact('posbills', 'page_title'));
  }

  public function materializeview()
  {

    $page_title = 'Admin | Pos | Sales | Materialize | Search';

    $users = \App\User::where('enabled', '1')->pluck('username', 'username as id')->all();


    if (\Auth::user()->hasRole('admins')) {
      $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
        ->where('enabled', 1)
        ->get();
    } else {
      $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
      $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
        ->orderBy('id', 'DESC')
        ->where('enabled', 1)
        ->get();
    }


    return view('admin.hotel.posreport.sales_materalize', compact('page_title', 'outlets', 'users'));
  }

  public function materializeviewresult(Request $request)
  {

    $page_title = 'Admin | Pos | Sales | Materialize | Results';

    $users = \App\User::where('enabled', '1')->pluck('username', 'username as id')->all();

    $outlet_name = \App\Models\PosOutlets::find($request->outlet_id)->name ?? '';

    if (\Auth::user()->hasRole('admins')) {
      $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
        ->where('enabled', 1)
        ->get();
    } else {
      $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
      $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
        ->orderBy('id', 'DESC')
        ->where('enabled', 1)
        ->get();
    }

    $op = \Request::get('op');

    $startdate = $request->start_date;
    $enddate = $request->end_date;


    $sales = \DB::table('fin_order_materialize_view')->where('bill_date', '>=', $request->start_date)
      ->where('bill_date', '<=', $request->end_date)
      ->where(function ($query) use ($request) {
        if ($request->outlet_id) {
          return $query->where('outlet_id', $request->outlet_id);
        }
      })->where(function ($query) use ($request) {
        if ($request->user_id) {
          return $query->where('entered_by', $request->user_id);
        }
      })->paginate(50);


    if ($op == 'print') {

      $sales_print = \DB::table('fin_order_materialize_view')->where('bill_date', '>=', $request->start_date)
        ->where('bill_date', '<=', $request->end_date)
        ->where(function ($query) use ($request) {
          if ($request->outlet_id) {
            return $query->where('outlet_id', $request->outlet_id);
          }
        })->where(function ($query) use ($request) {
          if ($request->user_id) {
            return $query->where('entered_by', $request->user_id);
          }
        })->get();
      return view('print.materializebook', compact('sales_print', 'startdate', 'enddate', 'outlet_name'));
    } elseif ($op == 'pdf') {

      $sales_pdf = \DB::table('fin_order_materialize_view')->where('bill_date', '>=', $request->start_date)
        ->where('bill_date', '<=', $request->end_date)
        ->where(function ($query) use ($request) {
          if ($request->outlet_id) {
            return $query->where('outlet_id', $request->outlet_id);
          }
        })->where(function ($query) use ($request) {
          if ($request->user_id) {
            return $query->where('entered_by', $request->user_id);
          }
        })->get();

      $pdf = \PDF::loadView('pdf.materializebook', compact('sales_pdf', 'fiscal_year', 'startdate', 'enddate', 'outlet_name'))->setPaper('a4', 'landscape');
      $file = 'Report_materialize_filtered' . date('_Y_m_d') . '.pdf';
      if (File::exists('reports/' . $file)) {
        File::Delete('reports/' . $file);
      }

      return $pdf->download($file);
    }
    elseif($op == 'excel'){
      $allData = [];
      $tTaxAmount = 0;
      $tdiscount_amount = 0;
      $ttax_amount = 0;
      $ttotal_amount = 0;
      $sales_pdf = \DB::table('fin_order_materialize_view')->where('bill_date', '>=', $request->start_date)
        ->where('bill_date', '<=', $request->end_date)
        ->where(function ($query) use ($request) {
          if ($request->outlet_id) {
            return $query->where('outlet_id', $request->outlet_id);
          }
        })->where(function ($query) use ($request) {
          if ($request->user_id) {
            return $query->where('entered_by', $request->user_id);
          }
        })->get();
        foreach($sales_pdf as $s){
            $data = [
                'Fiscal Year'=>$s->fiscal_year,
                'Bill No.'=>'TI-'.$s->outlet_code.$s->bill_no,
                'Customer Name'=>$s->customer_name,
                'Customer Pan'=>$s->customer_pan,
                'Bill Date'=>$s->bill_date,
                'Tax Amount'=>$s->tax_amount,
                'Discount Amount'=>$s->discount,
                'Taxable Amount'=>$s->total_amount,
                'Total Amount'=>$s->total_amount,
                'Sync With IRD'=> $s->sync_with_ird == '1' ? "Yes": "No",
                'Is Bill Printed'=>$s->is_bill_printed == '1' ? 'Yes': 'No',
                'Is Bill Active'=>$s->is_bill_active  == '1' ? 'Yes': 'No',
                'Print Time'=>$s->printed_time,
                'Entered By'=>$s->entered_by,
                'Printed By'=>$s->printed_by,
                'Is Real Time'=>$s->is_realtime == '1' ? "Yes": "No"
            ];
            $allData[] = $data;
            $tTaxAmount += $s->tax_amount;
            $tdiscount_amount += $s->discount;
            $ttax_amount += $s->taxable_amount;
            $ttotal_amount += $s->total_amount;

            if(!$s->is_bill_active){


                $data = [
                    'Fiscal Year'=>$s->fiscal_year,
                    'Bill No.'=>'Ref of TI-'.$s->outlet_code.$s->bill_no.' '.'CN-TI-'.$s->outlet_code.$s->bill_no,
                    'Customer Name'=>$s->customer_name,
                    'Customer Pan'=>$s->customer_pan,
                    'Bill Date'=>$s->bill_date,
                    'Tax Amount'=>-$s->tax_amount,
                    'Discount Amount'=>-$s->discount,
                    'Taxable Amount'=>-$s->total_amount,
                    'Total Amount'=>-$s->total_amount,
                    'Sync With IRD'=>$s->sync_with_ird == '1' ? "Yes": "No",
                    'Is Bill Printed'=>$s->is_bill_printed == '1' ? 'Yes': 'No',
                    'Is Bill Active'=>$s->is_bill_active  == '1' ? 'Yes': 'No',
                    'Print Time'=>$s->printed_time,
                    'Entered By'=>$s->entered_by,
                    'Printed By'=>$s->printed_by,
                    'Is Real Time'=>$s->is_realtime == '1' ? "Yes": "No"
                ];

                $allData[] = $data;

                $tTaxAmount -= $s->tax_amount;
                $tdiscount_amount -= $s->discount;
                $ttax_amount -= $s->taxable_amount;
                $ttotal_amount -= $s->total_amount;

            }





        }

   return \Excel::download(new \App\Exports\ExcelExport($allData, true), 'sales_materalize.csv');




    }




    $request = $request->all();

    return view('admin.hotel.posreport.sales_materalize', compact('page_title', 'outlets', 'users', 'sales', 'request'));
  }



  public function hotelIrdIndex()
  {



    $clients = \App\Models\Client::select('id', 'name')->orderBy('id', 'DESC')->pluck('name','id');


    $hotelbills = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.settlement','fin_orders_meta.posting_entry_id')
            ->where(function($query){
                $start_date = \Request::get('start_date');
                $end_date = \Request::get('end_date');
                if($start_date && $end_date){
                  return $query->where('fin_orders.bill_date','>=',$start_date )
                            ->where('fin_orders.bill_date','<=',$end_date );
                }
            })->where(function($query){
                $paid_by = \Request::get('paid_by');
                if($paid_by){
                  return $query->where('payments.sale_id',$paid_by);
                }
            })
            ->where(function($query){
                $bill_no = \Request::get('bill_no');
                if($bill_no){
                  return $query->where('fin_orders.bill_no',$bill_no);
                }
            })->where(function($query){
                $customer_id= \Request::get('clients_id');
                if($customer_id){
                  return $query->where('pos_customer_id',$customer_id);
                }

            })
            ->where('outlet_id', env('HOTEL_OUTLET_ID'))
            ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
            ->leftjoin('payments','payments.sale_id','=','fin_orders.id')
            ->groupBy('fin_orders.id')
            ->orderBy('fin_orders.id', 'desc')
            ->paginate(20);

    $pay_method = $this->pay_method;

    $page_title = "Admin | Hotel | SYNC WITH IRD | Invoice List";



    //dd($hotelbills);

    return view('admin.hotel.posreport.hotelsyncwithird', compact('hotelbills', 'page_title','clients','pay_method'));
  }


  public function hotelsalesreturn()
  {


    $page_title = "Admin | Hotel | Sales | Return";
    $fiscalyear = \App\Models\Fiscalyear::orderBy('id', 'desc')->get();
    $outlets = \App\Models\PosOutlets::orderBy('id', 'desc')->get();

    $credit_note_no =  \App\Models\InvoiceMeta::orderBy('id', 'desc')->where('credit_note_no', '!=', 'null')->first()->credit_note_no + 1;


    return view('admin.hotel.posreport.hotelsalesreturn', compact('page_title', 'fiscalyear', 'outlets', 'credit_note_no'));
  }

  public function hotelsalesreturnpost(Request $request)
  {

    $invoice = \App\Models\Invoice::where('fiscal_year', $request->fiscal_year)->where('bill_no', $request->bill_no)->first();

    $invoicemeta = \App\Models\InvoiceMeta::where('invoice_id', $invoice->id)->first();

    if (count($invoice) == 1) {
      if ($invoice->reservation->client) {
        $guest_name  = $invoice->reservation->client->name;
        $buyer_pan   = $invoice->reservation->client->vat;
      } else {
        $guest_name  = $invoice->reservation->guest_name;
        $buyer_pan   = 0;
      }

      $bill_date_nepali = $this->convertdate($invoice->bill_date);
      $cancel_date = $this->convertdate($request->cancel_date);

      $bill_today_date_nep = $this->convertdate(date('Y-m-d'));


      $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'), "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $buyer_pan, "fiscal_year" => $invoice->fiscal_year, "buyer_name" => $guest_name, "ref_invoice_number" => env('HOTEL_BILL_PREFIX') . $invoice->bill_no, "credit_note_date" => $cancel_date, "credit_note_number" => $request->credit_note_no, "reason_for_return" => $request->void_reason, "total_sales" => $invoice->total_amount, "taxable_sales_vat" => $invoice->taxable_amount, "vat" => $invoice->tax_amount, "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0, "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0, "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);


      $irdsync = new \App\Models\NepalIRDSync();
      $response = $irdsync->returnbill($data);

      if ($response == 200) {

        $attributes_order['entrytype_id'] = 7; //crdeitnotes
        $attributes_order['tag_id'] = 3; //crdeitnotes
        $attributes_order['user_id'] = \Auth::user()->id;
        $attributes_order['org_id'] = \Auth::user()->org_id;
        $attributes_order['number'] = $id;
        $attributes_order['resv_id'] = $invoice->reservation_id;
        $attributes_order['source'] = 'Sales_Return';
        $attributes_order['date'] = \Carbon\Carbon::today();
        $attributes_order['notes'] = "Credit Return Hotel: " . $invoice->id . "";

        $attributes_order['dr_total'] = $invoice->total_amount;
        $attributes_order['cr_total'] = $invoice->total_amount;

        $entry = \App\Models\Entry::create($attributes_order);

        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->dc = 'C';
        $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
        $cash_amount->amount = $invoice->total_amount;
        $cash_amount->narration = 'being sales made ';
        $cash_amount->save();

        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->dc = 'D';
        $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('HOTEL_SALES_RETURN');
        $cash_amount->amount = $invoice->total_amount;
        $cash_amount->narration = 'being payment made';
        $cash_amount->save();


        $invoicemeta->update(['is_bill_active' => 0, 'void_reason' => $request->void_reason, 'cancel_date' => $request->cancel_date, 'credit_note_no' => $request->credit_note_no, 'is_realtime' => 1, 'credit_user_id' => \Auth::user()->id]);

        Audit::log(Auth::user()->id, "Hotel Invoice", "Bill Is Returned To IRD: ID-" . $invoice->id . " Response :" . $response . "");

        Flash::success('Successfully Returned from IRD. Code: ' . $response . '');
        return redirect()->back();
      } else {

        $invoicemeta->update(['is_realtime' => 1 ]);

        Audit::log(Auth::user()->id, "Hotel Invoice", "Bill Is Returned To IRD: ID-" . env('HOTEL_BILL_PREFIX') . $invoice->bill_no . " Response :" . $response . "");

        Flash::error('Return Cannot Due to Response Code: ' . $response . '');
        return redirect()->back();
      }
    } else {

      Audit::log(Auth::user()->id, "Hotel Invoice", "Bill Is Not Found: ID-" . $invoice->id . "");

      \Flash::warning('No Bill Found Of this Number');
      return redirect()->back();
    }
  }


  public function hotelmaterializeview()
  {

    $page_title = 'Admin | Pos | Sales';

    $users = \App\User::where('enabled', '1')->pluck('username', 'username as id')->all();

    return view('admin.hotel.posreport.invoice_materalize', compact('page_title', 'users'));
  }

  public function hotelmaterializeviewresult(Request $request)
  {

    $page_title = 'Admin | Pos | Sales';

    $users = \App\User::where('enabled', '1')->pluck('username', 'username as id')->all();

    $sales = \DB::table('invoice_materialize_view')->where('bill_date', '>=', $request->start_date)

      ->where('bill_date', '<=', $request->end_date)

      ->where(function ($query) use ($request) {

        if ($request->user_id) {

          return $query->where('entered_by', $request->user_id);
        }
      })->get();


    if (count($sales) == 0) {

      \Flash::warning("No any record Found");
    }
    $request = $request->all();

    return view('admin.hotel.posreport.invoice_materalize', compact('page_title', 'users', 'sales', 'request'));
  }
}

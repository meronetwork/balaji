<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationRoom;
use App\Models\ReservationGuest;
use App\Models\ReservationDoc;
use App\Models\Audit as Audit;
use App\Models\Attachment;
use Flash;
use Auth;
use DB;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $reservation = Reservation::where('parent_res', null)
            ->where(function ($query) {
                if (\Request::get('check_in_date') && \Request::get('check_in_date') != '')
                    return $query->where('check_in', \Request::get('check_in_date'))->orWhere('book_in',\Request::get('check_in_date'));;
            })
            ->where(function ($query) {
                if (\Request::get('check_out_date') && \Request::get('check_out_date') != '')
                    return $query->where('check_out', \Request::get('check_out_date'))->orWhere('book_out',\Request::get('check_out_date'));;
            })
            ->where(function ($query) {
                if (\Request::get('user_id') && \Request::get('user_id') != '')
                    return $query->where('user_id', \Request::get('user_id'));
            })
            ->where(function ($query) {
                if (\Request::get('agent_id') && \Request::get('agent_id') != '')
                    return $query->where('agent_id', \Request::get('agent_id'));
            })
            ->where(function ($query) {
                if (\Request::get('guest_type') && \Request::get('guest_type') != '')
                    return $query->where('guest_type', \Request::get('guest_type'));
            })->orderBy('id', 'desc')
            ->paginate('30');

        $agents = \App\Models\Client::where('relation_type', 'agent')->pluck('name', 'id')->all();

        //dd($agents);

        $users = \App\User::pluck('username', 'id')->all();
        $page_title = 'Reservation Index';
        $guest_type = [
            'TOU' => ['Tourist', 'btn bg-green  margin'],
            'BUS' => ['Business Travellers', 'btn bg-red  margin'],
            'FAM' => ['Families', 'btn bg-navy  margin'],
            'STA' => ['Hotel Staffs', 'btn bg-red  margin'],
            'DEl' => ['Delegates', 'btn bg-olive  margin'],
            'VIP' => ['VIP', 'btn bg-maroon margin'],
            'COR' => ['Corporate', 'btn bg-purple margin'],
            'GOV' => ['Government', 'btn bg-blue margin'],
            'FIT' => ["FIT", 'btn bg-orange margin'],
            'DIP' => ['Diplomats', 'btn btn-success margin'],
            'COM' => ["Complementry", 'btn bg-green margin']
        ];

        return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
    }


    public function guesthistory()
    {

        $reservation = Reservation::orderBy('id', 'desc')->where('parent_res', null)
            ->where(function ($query) {
                if (\Request::get('check_in_date') && \Request::get('check_in_date') != '')
                    return $query->where('check_in', \Request::get('check_in_date'))->orWhere('book_in',\Request::get('check_in_date'));
            })
            ->where(function ($query) {
                if (\Request::get('check_out_date') && \Request::get('check_out_date') != '')
                    return $query->where('check_out', \Request::get('check_out_date'))->orWhere('book_out',\Request::get('check_out_date'));
            })
            ->where(function ($query) {
                if (\Request::get('user_id') && \Request::get('user_id') != '')
                    return $query->where('user_id', \Request::get('user_id'));
            })
            ->where(function ($query) {
                if (\Request::get('agent_id') && \Request::get('agent_id') != '')
                    return $query->where('agent_id', \Request::get('agent_id'));
            })
            ->where(function ($query) {
                if (\Request::get('guest_type') && \Request::get('guest_type') != '')
                    return $query->where('guest_type', \Request::get('guest_type'));
            })
            ->paginate('30');


        $agents =  \App\Models\Client::where('relation_type', 'agent')->pluck('name', 'id')->all();
        $users = \App\User::pluck('username', 'id')->all();

        $page_title = 'Reservation Index';

        $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'GOV' => 'Government', 'FIT' => "FIT", 'DIP' => "Diplomats", 'COM' => "Complementry"];

        return view('admin.hotel.reservation.guesthistory', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = date('Y-m-d');
        $room = DB::select("select room.room_number,room_types.room_name FROM room LEFT JOIN room_types ON room.roomtype_id = room_types.roomtype_id WHERE room.room_number NOT In(select room_num FROM reservations WHERE  reservations.reservation_status IN (3) )");

        $room = json_decode(json_encode($room), true);

        $guest = \App\Models\Contact::all();
        $agent = \App\Models\Client::select('id', 'name')->where('relation_type', 'agent')->get();
        $client = \App\Models\Client::select('id', 'name')->where('relation_type', 'hotelcustomer')->get();
        $rstatus = \App\Models\ReservationStatus::all();
        $rtype = \App\Models\ReservationType::all();
        $rates = \App\Models\Rateplan::all();
        $sources = \App\Models\Communication::all();

        $paymenttype = \App\Models\PaymentType::all();
        $page_title = 'Create Reservation';

        return view('admin.hotel.reservation.create', compact('room', 'guest', 'agent', 'rstatus', 'client', 'rtype', 'rates', 'page_title', 'sources', 'paymenttype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // $check_checkout=Reservation::where('check_in','<=',$request->check_out)->where('room_num',$request->room_num)->where('check_in','>',$request->check_in)->first();

        // if($check_checkout){
        // $error = "Room number ".$request->room_num." is already booked from ".$check_checkout->check_in." to ".$check_checkout->check_out;
        // return \Redirect::back()->withErrors([$error]);
        // }


        $reservation = $request->all();

        //dd($reservation);
        $reservation['guest_name'] = strtoupper($request->guest_name);

        // dd($reservation);
        $rateplan = json_decode($reservation['rate_id'], true);
        $reservation['rate_id'] = $rateplan['id'];
        // dd($reservation);

        if ($request->submit_method == 'check_in') {
            $status = \App\Models\ReservationStatus::where('status_name', 'Checked In')->first();
            $reservation['reservation_status'] = $status->id;
        }

        if ($request->submit_method == 'reserv') {
            $status = \App\Models\ReservationStatus::where('status_name', 'Due In')->first();
            $reservation['reservation_status'] = $status->id;
        }

        if ($request->company_id) {

            $reservation['ledger_id'] = \App\Models\Client::find($request->company_id)->ledger_id;
        }

        //dd($reservation);

        $reservation['user_id'] = \Auth::user()->id;
        $reservation = Reservation::create($reservation);
        foreach ($request->extra_room_num ?? [] as $key => $room_num) {
            $extra_reservation = $reservation->replicate();
            $extra_reservation->room_num = $room_num;
            $extra_reservation->rate_id = json_decode(($request->extra_rate_plan)[$key], true)['id'];
            $extra_reservation->room_rate = ($request->extra_room_rate)[$key];
            $extra_reservation->occupancy = ($request->extra_no_of_adult)[$key];
            $extra_reservation->occupancy_child = ($request->extra_no_of_child)[$key];
            $extra_reservation->guest_name = ($request->extra_guest_name)[$key];
            $extra_reservation->guest_id = ($request->extra_guest_id)[$key];
            $extra_reservation->doc_type_extra = ($request->extra_doc_type)[$key];
            $extra_reservation->doc_num_extra = ($request->extra_doc_num)[$key];
            $extra_reservation->parent_res = $reservation->id;
            $extra_reservation->save();
        }


        \App\Models\Room::where('room_number', $reservation->room_num)->update(['status_id' => '0']);
        foreach ($request->room_no1 ??[] as $moreroom) {
            $more = ['room_num' => $moreroom, 'res_id' => $reservation->id];
            ReservationRoom::create($more);
            if ($update_room) {
                \App\Models\Room::where('room_number', $moreroom)->update($update_room);
            }
        }

        foreach ($request->full_name ?? [] as $key => $fullname) {
            $moreguest = ['full_name' => $fullname, 'res_id' => $reservation->id, 'doc_type' => $request->doc_type[$key], 'doc_num' => $request->doc_num[$key], 'guests_type' => $request->guests_type[$key]];
            ReservationGuest::create($moreguest);
        }

        $files = $request->file('doc');
        foreach ($files ??[] as $doc_) {
            $doc_name = time() . "" . $doc_->getClientOriginalName();
            $destinationPath = public_path('/reservation/');
            $doc_->move($destinationPath, $doc_name);
            $doc_name = "/reservation/" . $doc_name;
            $reservation_doc = ['res_id' => $reservation->id, 'doc' => $doc_name, 'user_id' => \Auth::user()->id];
            ReservationDoc::create($reservation_doc);
        }




        $reservation = \App\Models\Reservation::find($reservation->id);
        $reservation_child = \App\Models\Reservation::where('parent_res', $reservation->id)->get();

        $mail_from =  env('APP_EMAIL');
        $mail_to   =  $reservation->email;
        $subject   =  "Reservation Confirmed";


        $pdf = \PDF::loadView('admin.hotel.reservation.emailsample', compact('reservation', 'reservation_child'));

        $file = str_replace(' ', '_', trim($reservation->guest_name)) . '_CRM_' . $reservation->id . '.pdf';

        //dd($file);

        //dd('Done2');
        if (\File::exists('uploads/' . $file)) {
            \File::Delete('uploads/' . $file);
        }
        $pdf->save('uploads/' . $file);
        $stamp = time();
        // $fields = [
        //     'file'  =>  $stamp . $filename,
        // ];
         $fields = [
            'file'  => null,
        ];
        try {
            //send email
            $mail = \Mail::send('emails.email-reservation-confirmation', [], function ($message) use ($subject, $mail_from, $mail_to, $file, $Lead, $request, $fields, $reservation) {
                //$mail = \Mail::send('emails.email-send', [], function ($message) use ($attributes, $Lead, $request, $fields) {
                $message->subject($subject);
                $message->from($mail_from, env('APP_COMPANY'));
                $message->to($mail_to, '');
                $message->attach('uploads/' . $file);
                if ($request->file('attachment')) {
                    $message->attach('sent_attachments/' . $fields['file']);
                }
            });
        } catch (\Exception $e) {
        }



        Flash::success('Reservation sucessfully created');
        return redirect('/admin/hotel/reservation-index/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Reservation::find($id);
        // $guest = \App\Models\Contact::find($edit->guest_id);
        $agent = \App\Models\Client::select('id', 'name')->where('relation_type', 'agent')->get();
        $client = \App\Models\Client::select('id', 'name')->where('relation_type', 'hotelcustomer')->get();
        $moreroom = ReservationRoom::where('res_id', $id)->get();
        $moreguest = ReservationGuest::where('res_id', $id)->get();
        $moredoc = ReservationDoc::where('res_id', $id)->get();
        $rstatus = \App\Models\ReservationStatus::all();
        $check_clock =  \App\Models\ReservationStatus::select('id')->where('status_name', 'Checked In')->first();

        $rtype = \App\Models\ReservationType::find($edit->reservation_type);

        $rateplan = \App\Models\Rateplan::orderBy('id', 'desc')->where('roomtype_id', $edit->room->roomtype_id)->get();

        $sources = \App\Models\Communication::all();

        $paymenttype = \App\Models\PaymentType::all();

        $page_title = 'Res # ' . $edit->id;

        $guest = \App\Models\Contact::all();

        $room = DB::select("select room.room_number,room_types.room_name FROM room LEFT JOIN room_types ON room.roomtype_id = room_types.roomtype_id WHERE room.room_number NOT In(select room_num FROM reservations WHERE  reservations.reservation_status IN (3) )");

        $room = json_decode(json_encode($room), true);

        $extra_reservation = Reservation::where('parent_res', $id)->get();

        return view('admin.hotel.reservation.edit', compact('edit', 'room', 'guest', 'agent', 'rstatus', 'client', 'moreguest', 'moredoc', 'moreroom', 'rtype', 'check_clock', 'page_title', 'sources', 'rateplan', 'paymenttype', 'extra_reservation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateRes = $request->all();

        if ($request->company_id) {
            $updateRes['ledger_id'] = \App\Models\Client::find($request->company_id)->ledger_id;
        }
        $reservation = Reservation::find($id)->update($updateRes);
        \App\Models\ReservationUpdate::create(['res_id' => $id, 'user_id' => \Auth::user()->id]);
        foreach ($request->child_res_id ??[] as $key => $child_id) {
            $extra_reservation = Reservation::find($child_id);
            $extra_reservation->room_num = ($request->extra_room_num)[$key];
            $extra_reservation->rate_id = ($request->extra_rate_plan)[$key];
            $extra_reservation->room_rate = ($request->extra_room_rate)[$key];
            $extra_reservation->occupancy = ($request->extra_no_of_adult)[$key];
            $extra_reservation->occupancy_child = ($request->extra_no_of_child)[$key];
            $extra_reservation->guest_name = ($request->extra_guest_name)[$key];
            $extra_reservation->guest_id = ($request->extra_guest_id)[$key];
            $extra_reservation->doc_type_extra = ($request->extra_doc_type)[$key];
            $extra_reservation->doc_num_extra = ($request->extra_doc_num)[$key];
            $extra_reservation->update();
        }
        $reservation = Reservation::find($id);

        foreach ($request->extra_room_num_new ?? [] as $key => $room_num) {

            $extra_reservation = $reservation->replicate();
            $extra_reservation->room_num = $room_num;

            $extra_reservation->rate_id = json_decode(($request->extra_rate_plan_new)[$key], true)['id'];
            $extra_reservation->room_rate = ($request->extra_room_rate_new)[$key];
            $extra_reservation->occupancy = ($request->extra_no_of_adult_new)[$key];
            $extra_reservation->occupancy_child = ($request->extra_no_of_child_new)[$key];
            $extra_reservation->guest_name = ($request->extra_guest_name_new)[$key];
            $extra_reservation->guest_id = ($request->extra_guest_id_new)[$key];
            $extra_reservation->doc_type_extra = ($request->extra_doc_type_new)[$key];
            $extra_reservation->doc_num_extra = ($request->extra_doc_num_new)[$key];
            $extra_reservation->parent_res = $reservation->id;
            $extra_reservation->save();
        }

        // \App\Models\Room::whereIn('room_number',$request->currentroom)->update(['status_id'=>'1']);
        // \App\Models\Room::where('room_number',$reservation->room_num)->update(['status_id'=>'0']);
        Flash::success('Reservation sucessfully updated');
        return redirect()->back();
    }

    public function voidfromedit(Request $request, $id)
    {

        //   dd($request->reason);

        $reason = $request->reason;

        $reservation = Reservation::find($id);

        $makevoid = ['check_in' => null, 'check_out' => null, 'room_num' => "", 'rate_id' => "", 'reservation_status' => '9', 'void_reason' => $reason, 'book_in' => $reservation->check_in, 'book_out' => $reservation->check_out];

        Reservation::find($id)->update($makevoid);

        Reservation::where('parent_res', $id)->update($makevoid);


        $folio = \App\Models\Folio::where('reservation_id',$id)->first();
        if($folio){
            \App\Models\FolioDetail::where('folio_id',$folio->id)->delete();
            \App\Models\Folio::where('reservation_id',$id)->delete();


        }




        Flash::success('Reservation sucessfully marked as void');
        return redirect('/admin/hotel/reservation-edit/' . $id);
    }

    public function voidConfirm($id)
    {

        $error = null;
        $res = Reservation::find($id);

        $modal_title = "Void This Reservation";
        $modal_route = route('admin.reservation.voidfromedit', $res->id);

        return view('modal_void_reason', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Reservation::find($id)->delete();
        Reservation::where('parent_res', $id)->delete();
        Flash::success('Reservation has been sucessfully deleted');
        return redirect('/admin/hotel/reservation-index/');
    }

    public function getroomPlan($id)
    {

        $room_type_id = \App\Models\Room::select('roomtype_id')->where('room_number', $id)->first();
        $rateplan = \App\Models\Rateplan::where('roomtype_id', $room_type_id->roomtype_id)->get();
        return response()->json($rateplan);
    }

    public function checkRoomForDate($date)
    {

        $room = DB::select("select room.room_number,room_types.room_name FROM room LEFT JOIN room_types ON room.roomtype_id = room_types.roomtype_id WHERE room.room_number NOT In(select room_num FROM reservations WHERE '$date' >= check_in AND '$date' < check_out )");
        return response()->json($room);
    }



    public function checkins()
    {

        Audit::log(Auth::user()->id, 'checkins', 'show', ['name' => 'checkin']);
        $status = \App\Models\ReservationStatus::where('status_name', 'Checked In')->first();
        $page_title =  "Admin | Checkins | Show";
        $page_description =  "Displaying Todays Checkins";
        $check_in = date('Y-m-d');

        $reservation = Reservation::where('check_in', $check_in)->where('reservation_status', $status->id)->get();

        return view('admin.hotel.reservation.checkins', compact('page_title', 'page_description', 'reservation'));
    }

    public function arrivals()
    {

        Audit::log(Auth::user()->id, 'checkins', 'show', ['name' => 'checkin']);
        $status = \App\Models\ReservationStatus::where('status_name', 'Checked In')->first();
        $page_title =  "Admin | Arrivals | Show";
        $page_description =  "Displaying Todays Arrivals";
        $check_in = date('Y-m-d');

        $reservation = Reservation::where('book_in', $check_in)->where('reservation_status', $status->id)->get();

        return view('admin.hotel.reservation.checkins', compact('page_title', 'page_description', 'reservation'));
    }



    public function checkouts()
    {

        Audit::log(Auth::user()->id, 'checkouts', 'show', ['name' => 'checkin']);
        $status = \App\Models\ReservationStatus::where('status_name', 'Checked out')->first();
        $page_title =  "Admin | Checkouts | Show";
        $page_description =  "Displaying Todays Checkouts";
        $check_out = date('Y-m-d');

        $reservation = Reservation::where('check_out', $check_out)->where('reservation_status', $status->id)->get();



        return view('admin.hotel.reservation.checkouts', compact('page_title', 'page_description', 'reservation'));
    }

    public function departures()
    {

        Audit::log(Auth::user()->id, 'checkouts', 'show', ['name' => 'checkin']);
        $status = \App\Models\ReservationStatus::where('status_name', 'Checked out')->first();
        $page_title =  "Admin | Departures | Show";
        $page_description =  "Displaying Todays Departures";
        $check_out = date('Y-m-d');

        $reservation = Reservation::where('book_out', $check_out)->where('reservation_status', $status->id)->get();



        return view('admin.hotel.reservation.checkouts', compact('page_title', 'page_description', 'reservation'));
    }


    public function confirmDeleteFile($id)
    {
        $error = null;
        $docs = ReservationDoc::find($id);
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete Reservation Doc";
        $modal_body = "Are you sure that you want to delete  reservation docs id " . $docs->id . "? This operation is irreversible";
        $modal_route = route('admin.hotel.reservation-delete-file', $docs->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function DeleteFile($id)
    {
        ReservationDoc::find($id)->delete();
        return \Redirect::bacK();
    }

    public function AddFile(Request $request, $id)
    {

        $doc_ = $request->file('moredocuments');
        $doc_name = time() . "" . $doc_->getClientOriginalName();
        $destinationPath = public_path('/reservation/');
        $doc_->move($destinationPath, $doc_name);
        $doc_name = "/reservation/" . $doc_name;
        $reservation_doc = ['res_id' => $id, 'doc' => $doc_name, 'user_id' => \Auth::user()->id];
        ReservationDoc::create($reservation_doc);
        return \Redirect::back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    private function getNextCodeLedgers($id)
    {
        $group_data = \App\Models\COAgroups::find($id);

        $group_code = $group_data->code??'-';

        $q = \App\Models\COALedgers::where('group_id', $id)->get();

        if (count($q) > 0) {
            $last = $q->last();
            $last = $last->code;
            $l_array = explode('-', $last);
            $new_index = end($l_array);
            $new_index += 1;
            $new_index = sprintf("%04d", $new_index);
            return $group_code . "-" . $new_index;
        } else {
            return $group_code . "-0001";
        }
    }


    public function PostLedgers($name, $id)
    {

        $detail = new \App\Models\COALedgers();
        $staff_or_company_id = \App\Models\COAgroups::find($id);
        $detail->group_id = $id;

        $detail->org_id = \Auth::user()->org_id;
        $detail->user_id = \Auth::user()->id;

        $detail->code = $this->getNextCodeLedgers($id);


        $detail->name = $name;
        $detail->op_balance_dc = 'D';
        $detail->op_balance = 0.00;
        $detail->notes = $name;

        //dd($detail);

            $detail->type = 0;


            $detail->reconciliation = 0;

        $detail->save();
        return $detail->id;
    }


    public function makeCheckin($request){

        $_check =  \App\Models\ReservationStatus::select('id')
                    ->where('status_name', 'Checked In')->first();

        $res_id =  $request->res_id;

        $reservation = \App\Models\Reservation::find($res_id);

        if (!($reservation->client->ledger_id??null)) {

            $reservation_full_name = '' . ucwords($reservation->guest_name) . ' ' . env('RES_CODE') . '' . $res_id . '';
            $_ledgers = $this->PostLedgers($reservation_full_name, env('Guest_Ledger_Group'));
        } else {

            $_ledgers = $reservation->client->ledger_id;
        }



        $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first();

        $bill_no = \App\Models\Orders::select('bill_no')
            ->where('fiscal_year', $fiscal_year->fiscal_year)
            ->where('outlet_id', env('HOTEL_OUTLET_ID'))
            ->orderBy('bill_no', 'desc')
            ->first();

        $bill_no = ($bill_no->bill_no??0) + 1;

        $ckfiscalyear = \App\Models\Fiscalyear::where('current_year', '1')
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->first();

        if (!$ckfiscalyear)
            return \Redirect::back()->withErrors(['Please update fiscal year <a href="/admin/fiscalyear/create">Click Here</a>!']);

        $fiscal_year = \App\Models\Fiscalyear::where('current_year', 1)->first()->fiscal_year;


        $room_num = $reservation->room_num;
        $total_days = 1; // adjust messey code

        $room_detail = \App\Models\Room::where('room_number', $room_num)->first();
        $room_type = $room_detail->room_type->room_name;
        // $room_price = $reservation->rateplan->rate;
        $room_price = $reservation->room_rate;
        $breakfast_price = $reservation->rateplan->breakfast_rate;
        $lunch_price = $reservation->rateplan->lunch_rate;
        $dinner_price = $reservation->rateplan->dinner_rate;

        // $adjustmentprice = ($reservation->room_rate) - ($room_price + $breakfast_price + $lunch_price + $dinner_price);
        $adjustmentprice = 0;



        $total_room_price = $total_days * $room_price;
        $total_breakfast_price = $total_days * $breakfast_price;
        $total_lunch_price = $total_days * $lunch_price;
        $total_dinner_price = $total_days * $dinner_price;



        $all_total_amount = $total_room_price + $total_breakfast_price + $total_lunch_price + $total_dinner_price + $adjustmentprice;
        $service_charge = $all_total_amount * env('SERVICE_CHARGE') / 100;
        $service_total_amount =  $all_total_amount * env('SERVICE_CHARGE') / 100 + $all_total_amount;
        $tax_amount = $service_total_amount * 13 / 100;
        $room_description = '' . $room_num . ' (' . $room_type . ')';


        // dd($room_description);
        if ($reservation->guest_type != 'COM') {

            $attributes = new \App\Models\Folio();
            $attributes->user_id = \Auth::user()->id;
            $attributes->folio_type = 'master';
            $attributes->reservation_id = $reservation->id;
            $attributes->org_id = \Auth::user()->org_id;
            $attributes->bill_date = \Carbon\Carbon::now();
            $attributes->subtotal = $all_total_amount;
            $attributes->service_charge = $service_charge;
            $attributes->amount_with_service = $service_total_amount;
            $attributes->discount_percent = 0;
            $attributes->taxable_amount = $service_total_amount;
            $attributes->tax_amount = $tax_amount;
            $attributes->total_amount = $service_total_amount + $tax_amount;
            $attributes->vat_type = 'yes';
            $attributes->bill_no = $bill_no;
            $attributes->fiscal_year = $fiscal_year;


            $attributes->service_type = 'yes';
            $attributes->save();

            $detail_attributes = new \App\Models\FolioDetail();
            $detail_attributes->folio_id = $attributes->id;
            $detail_attributes->description = $room_description;
            $detail_attributes->price = $room_price;
            $detail_attributes->quantity = $total_days;
            $detail_attributes->total = $total_room_price;
            $detail_attributes->date = \Carbon\Carbon::now();
            $detail_attributes->is_inventory = 0;
            $detail_attributes->res_id = $reservation->id;
            $detail_attributes->flag = 'accommodation';
            $detail_attributes->save();

            if ($breakfast_price != 0) {

                $detail_attributes = new \App\Models\FolioDetail();
                $detail_attributes->folio_id = $attributes->id;
                $detail_attributes->description = 'Break Fast';
                $detail_attributes->price = $breakfast_price;
                $detail_attributes->quantity = $total_days;
                $detail_attributes->total = $total_breakfast_price;
                $detail_attributes->date = \Carbon\Carbon::now();
                $detail_attributes->is_inventory = 0;
                 $detail_attributes->res_id = $reservation->id;
                $detail_attributes->flag = 'accommodation';
                $detail_attributes->save();
            }

            if ($lunch_price != 0) {

                $detail_attributes = new \App\Models\FolioDetail();
                $detail_attributes->folio_id = $attributes->id;
                $detail_attributes->description = 'Lunch';
                $detail_attributes->price = $lunch_price;
                $detail_attributes->quantity = $total_days;
                $detail_attributes->total = $total_lunch_price;
                $detail_attributes->date = \Carbon\Carbon::now();
                $detail_attributes->is_inventory = 0;
                 $detail_attributes->res_id = $reservation->id;
                $detail_attributes->flag = 'restaurant';
                $detail_attributes->save();
            }

            if ($dinner_price != 0) {

                $detail_attributes = new \App\Models\FolioDetail();
                $detail_attributes->folio_id = $attributes->id;
                $detail_attributes->description = 'Dinner';
                $detail_attributes->price = $dinner_price;
                $detail_attributes->quantity = $total_days;
                $detail_attributes->total = $total_dinner_price;
                $detail_attributes->date = \Carbon\Carbon::now();
                $detail_attributes->is_inventory = 0;
                 $detail_attributes->res_id = $reservation->id;
                $detail_attributes->flag = 'restaurant';
                $detail_attributes->save();
            }

            if ($adjustmentprice != 0) {

                $detail_attributes = new \App\Models\FolioDetail();
                $detail_attributes->folio_id = $attributes->id;
                $detail_attributes->description = 'Discount on Reservation';
                $detail_attributes->price = $adjustmentprice;
                $detail_attributes->quantity = $total_days;
                $detail_attributes->total = $adjustmentprice;
                $detail_attributes->date = \Carbon\Carbon::now();
                $detail_attributes->is_inventory = 0;
                 $detail_attributes->res_id = $reservation->id;
                $detail_attributes->flag = 'discount';
                $detail_attributes->save();
            }
        }

        $total_subtotal = 0;
        $total_service_charge = 0;
        $total_amount_with_service = 0;
        $total_taxable_amount = 0;
        $total_tax_amount = 0;
        $total_total_amount = 0;


        $folio  = \App\Models\Folio::orderBy('id', 'asc')->where('reservation_id', $reservation->id)->first();

        $total_subtotal = $folio->subtotal??0;
        $total_service_charge = $folio->service_charge??0;
        $total_amount_with_service = $folio->amount_with_service??0;
        $total_taxable_amount = $folio->taxable_amount??0;
        $total_tax_amount = $folio->tax_amount??0;
        $total_total_amount = $folio->total_amount??0;



        $child_res = \App\Models\Reservation::where('parent_res', $res_id)->get();
        if (count($child_res) > 0) {
            foreach ($child_res as $cr) {


                $room_num = $cr->room_num;

                $total_days = 1;
                $room_detail = \App\Models\Room::where('room_number', $room_num)->first();
                $room_type = $room_detail->room_type->room_name;
                // $room_price = $cr->rateplan->rate;
                $room_price = $cr->room_rate;
                $breakfast_price = $cr->rateplan->breakfast_rate;
                $lunch_price = $cr->rateplan->lunch_rate;
                $dinner_price = $cr->rateplan->dinner_rate;

                // $adjustmentprice = ($cr->room_rate) - ($room_price + $breakfast_price + $lunch_price + $dinner_price);

                $adjustmentprice = 0;

                $total_room_price = $total_days * $room_price;
                $total_breakfast_price = $total_days * $breakfast_price;
                $total_lunch_price = $total_days * $lunch_price;
                $total_dinner_price = $total_days * $dinner_price;

                $all_total_amount = $total_room_price + $total_breakfast_price + $total_lunch_price + $total_dinner_price + $adjustmentprice;
                $service_charge = $all_total_amount * env('SERVICE_CHARGE') / 100;
                $service_total_amount =  $all_total_amount * env('SERVICE_CHARGE') / 100 + $all_total_amount;
                $tax_amount = $service_total_amount * 13 / 100;
                $room_description = '' . $room_num . ' (' . $room_type . ')';

                if ($cr->guest_type != 'COM') {


                    $total_subtotal = $total_subtotal + $all_total_amount;
                    $total_service_charge = $total_service_charge + $service_charge;
                    $total_amount_with_service = $total_amount_with_service + $service_total_amount;
                    $total_taxable_amount = $total_taxable_amount + $service_total_amount;
                    $total_tax_amount = $total_tax_amount + $tax_amount;
                    $total_total_amount = $total_total_amount + $service_total_amount + $tax_amount;

                    $attributes = [];
                    $attributes['subtotal'] = $total_subtotal;
                    $attributes['service_charge'] = $total_service_charge;
                    $attributes['amount_with_service'] = $total_amount_with_service;
                    $attributes['discount_percent'] = 0;
                    $attributes['taxable_amount'] = $total_taxable_amount;
                    $attributes['tax_amount'] = $total_tax_amount;
                    $attributes['total_amount'] = $total_total_amount;

                    \App\Models\Folio::find($folio->id)->update($attributes);

                    $detail_attributes = new \App\Models\FolioDetail();
                    $detail_attributes->folio_id = $folio->id;
                    $detail_attributes->description = $room_description;
                    $detail_attributes->price = $room_price;
                    $detail_attributes->quantity = $total_days;
                    $detail_attributes->total = $total_room_price;
                    $detail_attributes->date = \Carbon\Carbon::now();
                    $detail_attributes->is_inventory = 0;
                    $detail_attributes->res_id = $cr->id;
                    $detail_attributes->flag = 'accommodation';
                    $detail_attributes->res_id = $cr->id;
                    $detail_attributes->save();


                    if ($breakfast_price != 0) {
                        $detail_attributes = new \App\Models\FolioDetail();
                        $detail_attributes->folio_id = $folio->id;
                        $detail_attributes->description = 'Break Fast';
                        $detail_attributes->price = $breakfast_price;
                        $detail_attributes->quantity = $total_days;
                        $detail_attributes->total = $total_breakfast_price;
                        $detail_attributes->date = \Carbon\Carbon::now();
                        $detail_attributes->is_inventory = 0;
                        $detail_attributes->flag = 'restaurant';
                        $detail_attributes->res_id = $cr->id;
                        $detail_attributes->save();
                    }

                    if ($lunch_price != 0) {
                        $detail_attributes = new \App\Models\FolioDetail();
                        $detail_attributes->folio_id = $folio->id;
                        $detail_attributes->description = 'Lunch';
                        $detail_attributes->price = $lunch_price;
                        $detail_attributes->quantity = $total_days;
                        $detail_attributes->total = $total_lunch_price;
                        $detail_attributes->date = \Carbon\Carbon::now();
                        $detail_attributes->is_inventory = 0;
                        $detail_attributes->flag = 'restaurant';
                        $detail_attributes->res_id = $cr->id;
                        $detail_attributes->save();
                    }

                    if ($dinner_price != 0) {

                        $detail_attributes = new \App\Models\FolioDetail();
                        $detail_attributes->folio_id = $folio->id;
                        $detail_attributes->description = 'Dinner';
                        $detail_attributes->price = $dinner_price;
                        $detail_attributes->quantity = $total_days;
                        $detail_attributes->total = $total_dinner_price;
                        $detail_attributes->date = \Carbon\Carbon::now();
                        $detail_attributes->is_inventory = 0;
                        $detail_attributes->flag = 'restaurant';
                        $detail_attributes->res_id = $cr->id;
                        $detail_attributes->save();
                    }

                    if ($adjustmentprice != 0) {

                        $detail_attributes = new \App\Models\FolioDetail();
                        $detail_attributes->folio_id = $folio->id;
                        $detail_attributes->description = 'Discount on Reservation';
                        $detail_attributes->price = $adjustmentprice;
                        $detail_attributes->quantity = $total_days;
                        $detail_attributes->total = $adjustmentprice;
                        $detail_attributes->date = \Carbon\Carbon::now();
                        $detail_attributes->is_inventory = 0;
                        $detail_attributes->flag = 'discount';
                        $detail_attributes->res_id = $cr->id;
                        $detail_attributes->save();
                    }
                }
            }
        }

        return ['_check'=>$_check,'res_id'=>$res_id,'_ledgers'=>$_ledgers];
    }


    public function checkinfromedit(Request $request)
    {
        DB::beginTransaction();

        $checkFolio = \App\Models\Folio::where('reservation_id',$request->res_id)->exists();

        $reservation = Reservation::find($request->res_id);

        if(!$reservation->room_num || !$reservation->rate_id){


            Flash::error("Please Update Room Number and rate first !!!");

            return redirect()->back();
        }
        $checkOtherRes = Reservation::where('reservation_status','3')
                ->where('room_num',$reservation->room_num)->first();

        if($checkOtherRes){


            Flash::error("Room num ".$reservation->room_num.' Already been booked by res #'.$checkOtherRes->id );

            return redirect()->back();

        }


        if($checkFolio){

            Flash::error("Folio already settled");

            return redirect()->back();
        }


        $checkInInfo = $this->makeCheckin($request);

        extract($checkInInfo);

        //  dd(4);
        $check_in = date('Y-m-d');
        $check_in_time = date('H:i');
        Reservation::find($res_id)->update(['reservation_status' => $_check->id, 'check_in' => $check_in, 'check_in_time' => $check_in_time, 'ledger_id' => $_ledgers]);

        Reservation::where('parent_res', $res_id)->update(['reservation_status' => $_check->id, 'check_in' => $check_in, 'check_in_time' => $check_in_time, 'ledger_id' => $_ledgers]);

        \ReservationHelper::updateOccupancy($request->res_id);
        Flash::success("Successfully Checked In");

        DB::commit();
        return \Redirect::back();
    }

    //today
    public function checkoutfromedit($res_id)
    {



        $_check =  \App\Models\ReservationStatus::select('id')->where('status_name', 'Checked out')->first();
        $check_out = date('Y-m-d');
        $check_out_time = date('H:i');
        $user = \Auth::user()->id;
        Reservation::find($res_id)->update(['reservation_status' => $_check->id, 'check_out' => $check_out, 'check_in_time' => $check_out_time, 'check_out_by' => $user]);

        Reservation::where('parent_res', $res_id)->update(['reservation_status' => $_check->id, 'check_out' => $check_out, 'check_in_time' => $check_out_time, 'check_out_by' => $user]);



        $error = null;
        $res = Reservation::find($res_id);

        $modal_title = "Change House Status";
        $modal_route = route('admin.hotel.reservation.checkout.house-status', $res_id);

        return view('modal_house_status', compact('error', 'modal_route', 'modal_title'));
    }

    public function checkoutfromeditModel($id)
    {

        $res = Reservation::find($id);

        $total_foilo_amount = \App\Models\Folio::where('reservation_id', $id)->sum('total_amount');
        $total_pos_amount =  \App\Models\Orders::where('reservation_id', $id)->sum('total_amount');
        $total_folio_paid_amount = (int) \App\Models\FolioPayment::where('reservation_id', $id)->sum('amount');

        $orders_ids = \App\Models\Orders::where('reservation_id', $id)->pluck('id')->toArray();

        $pos_payments = \App\Models\Payment::whereIn('sale_id', $orders_ids)->sum('amount');

        $total_amount = $total_foilo_amount + $total_pos_amount;

        $total_paid_amount = $total_folio_paid_amount + $pos_payments;

        if ($total_paid_amount >= $total_amount) {

            $modal_title = "Checkouts";
            $modal_body = "Are you sure that you want to check out reservation id #" . $res->id . " This operation is irreversible";
            $modal_route = route('admin.hotel.checkoutfromedit', $res->id);
        } else {

            $modal_title = "Sorry Cannot CheckOut";
            $modal_body = "The Amount Paid is NPR" . $total_paid_amount . ".Total Amount To be Paid Is NPR" . $total_amount . " Which is Incomplete Payment";
            $modal_route = route('admin.hotel.checkouterror', $res->id);
        }

        return view('modal_confirmation', compact('modal_route', 'modal_title', 'modal_body'));
    }

    public function checkouterrorModal($id)
    {


        $modal_title = "If you must change the pricing do it before checkout in Folio";
        $modal_body = "Are you sure that you want to check out reservation id #" . $id . " This operation is irreversible";
        $modal_route = route('admin.hotel.checkouterror', $id);

        return view('modal_confirmation', compact('modal_route', 'modal_title', 'modal_body'));
    }

    public function checkouterror($id)
    {

        $folios = \App\Models\Folio::orderBy('id', 'desc')->where('reservation_id', $id)->get();
        $orders = \App\Models\Orders::where('reservation_id', $id)->get();
        $folio_payments = \App\Models\FolioPayment::where('reservation_id', $id)->get();
        $orders_ids = \App\Models\Orders::where('reservation_id', $id)->pluck('id')->toArray();
        $payments = \App\Models\Payment::whereIn('sale_id', $orders_ids)->get();

        $invoices =  \App\Models\Orders::select('fin_orders.*')
            ->leftjoin('folio', 'folio.id', '=', 'fin_orders.folio_id')
            ->where('fin_orders.org_id', \Auth::user()->org_id)
            ->where('folio.reservation_id', $id)
            ->groupBy('fin_orders.id')
            ->paginate(20);

        $reservation_id = $id;
        return view('admin.hotel.reservation.checkoutdetail', compact('folios', 'orders', 'folio_payments', 'payments', 'invoices','reservation_id'));
    }







    //endtoday

    public function moveroom($res_id)
    {
        $res = Reservation::find($res_id);
        $history =  \App\Models\ReservationRoomMoves::where('reservation_id', $res_id)->get();
        $room_num = $res->room_num;
        $check_out = $res->check_out;
        $check_in = $res->check_in;
        $date = date('Y-m-d');
        //$room = json_decode(json_encode($room), true);
        return view('admin.hotel.reservation.room_move', compact('res_id', 'room_num', 'history', 'check_out', 'check_in'));
    }
    public function moveroomstore(Request $request)
    {

        $res_id = $request->reservation_id;
        $res = Reservation::find($res_id);
        $move_room = $request->all();
        $move_room['user_id'] = \Auth::user()->id;
        $move_room['from_room'] = $res->room_num;
        \App\Models\ReservationRoomMoves::create($move_room);
        Reservation::find($res_id)->update(['room_num' => $move_room['to_room']]);
        Flash::success('Room sucessfully moved');
        return redirect('/admin/reservation/' . $res_id . '/move_room/');
    }

    public function getModalDelete($id)
    {
        $error = null;
        $res = Reservation::find($id);
        $modal_title = "Delete Reservation ? Cannot be undone";
        $modal_body = "Are you sure that you want to delete reservation id #" . $res->id . " This operation is irreversible";
        $modal_route = route('admin.hotel.reservation.delete', $res->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function extendstayshow($res_id)
    {

        $page_title = 'Admin|extendstay';
        $res = Reservation::find($res_id);
        $history =  \App\Models\ReservationStay::where('reservation_id', $res_id)->get();
        $check_out = $res->check_out;
        $check_out1 = date('Y-m-d', strtotime($check_out . ' + 1 days'));
        return view('admin.hotel.reservation.extend_stay', compact('check_out', 'res_id', 'check_out1', 'history', 'page_title'));
    }
    public function extendstaystore(Request $request, $res_id)
    {
        $res = Reservation::find($res_id);
        $stay = $request->all();
        $stay['reservation_id'] = $res_id;
        $stay['previous_checkout_date'] = $res->check_out;
        $stay['user_id'] = \Auth::user()->id;
        \App\Models\ReservationStay::create($stay);
        $res->update(['check_out' => $stay['extend_checkout_date']]);
        Flash::success('Reservation sucessfully extended');
        return redirect('/admin/hotel/extend-stay/' . $res_id . '/');
    }
    public function extendpackageshow($res_id)
    {

        $res = Reservation::find($res_id);
        $history =  \App\Models\ReservationPackage::where('reservation_id', $res_id)->get();
        $check_out = $res->check_out;
        $page_title = "Admin|extend package";

        return view('admin.hotel.reservation.extend_package', compact('page_title', 'res_id', 'check_out', 'history'));
    }
    public function extendpackagestore(Request $request, $res_id)
    {

        $package = $request->all();


        $reservation = \App\Models\Reservation::find($res_id);

        if ($request->package != 1) {

            $attraction_id = $request->package;
            $attraction_detail = \App\Models\Attraction::where('attraction_id', $attraction_id)->first();

            $attraction_description = $attraction_detail->attraction_name;
            $attraction_price = $attraction_detail->price;
            $attraction_quantity = $request->number_of_person;
            $total_attraction_price = $attraction_price * $attraction_quantity;

            $attraction_service_charge = $total_attraction_price * env('SERVICE_CHARGE') / 100 + $total_attraction_price;

            $attraction_tax = $attraction_service_charge * 13 / 100;

            // dd($attraction_tax);

            $attract_attributes = new \App\Models\Folio();
            $attract_attributes->user_id = \Auth::user()->id;
            $attract_attributes->reservation_id = $reservation->id;
            $attract_attributes->org_id = \Auth::user()->id;
            $attract_attributes->bill_date = \Carbon\Carbon::now();
            $attract_attributes->subtotal = $total_attraction_price;
            $attract_attributes->amount_with_service = $attraction_service_charge;
            $attract_attributes->discount_percent = 0;
            $attract_attributes->taxable_amount = $attraction_service_charge;
            $attract_attributes->tax_amount = $attraction_tax;
            $attract_attributes->total_amount = $attraction_service_charge + $attraction_tax;
            $attract_attributes->vat_type = 'yes';
            $attract_attributes->service_type = 'yes';
            $attract_attributes->save();

            $attract_detail_attributes = new \App\Models\FolioDetail();
            $attract_detail_attributes->folio_id = $attract_attributes->id;
            $attract_detail_attributes->description = $attraction_description;
            $attract_detail_attributes->price = $attraction_price;
            $attract_detail_attributes->quantity = $attraction_quantity;
            $attract_detail_attributes->total = $attraction_price * $attraction_quantity;
            $attract_detail_attributes->date = \Carbon\Carbon::now();
            $attract_detail_attributes->is_inventory = 0;
            $attract_detail_attributes->save();
        }

        //dd('notdone');

        $package['reservation_id'] = $res_id;
        $package['user_id'] = \Auth::user()->id;
        \App\Models\ReservationPackage::create($package);



        Flash::success("Package successfully added");
        return redirect('/admin/hotel/extend-package/' . $res_id);
    }
    public function extendguestshow($res_id)
    {

        $history =   \App\Models\ReservationGuest::where('res_id', $res_id)->get();
        return view('admin.hotel.reservation.extend_guest', compact('res_id', 'history'));
    }
    public function extendgueststore(Request $request, $res_id)
    {

        $guest = $request->all();
        $guest['res_id'] = $res_id;
        \App\Models\ReservationGuest::create($guest);
        Flash::success("Guest type sucessfully added");
        return redirect('/admin/hotel/extend-guest/' . $res_id);
    }

    public function extendbedshow($res_id)
    {

        $history =   \App\Models\ReservationBed::where('res_id', $res_id)->orderBy('id', 'desc')->get();
        $res = Reservation::find($res_id);
        $check_out = $res->check_out;
        return view('admin.hotel.reservation.extend_bed', compact('check_out', 'res_id', 'history'));
    }

    public function extendbedstore(Request $request, $res_id)
    {

        $beds = $request->all();

        $beds['res_id'] = $res_id;
        $beds['user_id'] = \Auth::user()->id;
        \App\Models\ReservationBed::create($beds);
        Flash::success("Beds  sucessfully added");
        return redirect('/admin/hotel/extend-bed/' . $res_id);
    }
    public function extendroomshow($res_id)
    {

        $history =   \App\Models\ReservationRoom::where('res_id', $res_id)->orderBy('id', 'desc')->get();
        $main_res = \App\Models\Reservation::find($res_id);

        //dd($main_res);

        $res = Reservation::find($res_id);
        $check_in = $res->check_in;
        $room = DB::select("select room.room_number,room_types.room_name FROM room LEFT JOIN room_types ON room.roomtype_id = room_types.roomtype_id WHERE room.room_number NOT In(select room_num FROM reservations WHERE '$check_in' >= check_in AND '$check_in' < check_out )");
        $rooms = [];
        foreach ($room as $rm) {
            array_push($rooms, (array)$rm);
        }

        return view('admin.hotel.reservation.extend_room', compact('res_id', 'history', 'rooms', 'main_res'));
    }

    public function extendroomstore(Request $request, $res_id)
    {
        $room = $request->all();
        $res = Reservation::find($res_id)->toArray();



        $room['res_id'] = $res_id;
        $room['user_id'] = \Auth::user()->id;
        $addedroom = \App\Models\ReservationRoom::create($room);
        $res['room_num'] = $request->room_num;
        $res['parent_res'] = $res_id;
        $res['linked_to'] = $addedroom->id;
        Reservation::create($res);
        Flash::success("Room  sucessfully added");
        return redirect('/admin/hotel/extend-room/' . $res_id);
    }



    public function extendroomconfirmdelete($linked_id)
    {
        $error = null;
        $res_room = \App\Models\ReservationRoom::find($linked_id);
        $modal_title = "Delete Reservation room? Cannot be undone";
        $modal_body = "Are you sure that you want to delete reservation room with #" . $res_room->id . " This operation is irreversible";
        $modal_route = route('admin.hotel.extend-room-del', $res_room->id);
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function extendroomdelete($linked_id)
    {
        $res_room = \App\Models\ReservationRoom::find($linked_id);
        $res_id = $res_room->res_id;
        $res_room->delete();
        if ($linked_id)
            Reservation::where('linked_to', $linked_id)->delete();
        Flash::success("Reservation room sucessfully deleted");
        return redirect('/admin/hotel/extend-room/' . $res_id);
    }



    public function roomguest($res_id, $room_num)
    {

        $guests = \App\Models\ReservationRoomGuest::where('res_id', $res_id)->where('room_num', $room_num)->get();

        $page_title = 'Add Guest';

        return view('admin.hotel.reservation.add_room_guests', compact('guests', 'res_id', 'room_num', 'page_title'));
    }

    public function storeroomguest(Request $request, $res_id, $room_num)
    {

        $guests = $request->all();

        $guests['res_id'] = $res_id;
        $guests['room_num'] = $room_num;

        $guests = \App\Models\ReservationRoomGuest::create($guests);

        return redirect('/admin/hotel/room-guest/' . $res_id . '/' . $room_num);
    }



    public function roomguestconfirmdelete($id)
    {
        $error = null;
        $room_guest = \App\Models\ReservationRoomGuest::find($id);

        //dd($room_guest);
        $modal_title = "Delete Room Guest? Cannot be undone";
        $modal_body = "Are you sure that you want to delete  Room Guest? This operation is irreversible";
        $modal_route = route('admin.hotel.room.guest.del', $room_guest->id);
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function roomguestdelete($id)
    {

        $room_guest = \App\Models\ReservationRoomGuest::find($id);
        $res_id = $room_guest->res_id;
        $room_num = $room_guest->room_num;
        $room_guest->delete();

        Flash::success("Room Guest sucessfully deleted");
        return redirect('/admin/hotel/room-guest/' . $res_id . '/' . $room_num);
    }

    public function guestlist(Request $request)
    {

        if (\Request::has('specific_date'))
            $date = $request->specific_date;
        else
            $date = date('Y-m-d');

        $res_id = \App\Models\Reservation::where('check_in', '<=', $date)->where('check_out', '>=', $date)->where('reservation_status', 3)->pluck('id');
        $guests = \App\Models\ReservationRoomGuest::whereIn('res_id', $res_id)->get();

        $page_title = 'Guest List';

        return view('admin.hotel.reservation.guestlist', compact('guests', 'page_title'));
    }


    public function generateGRCardPDF($id)
    {

        $ord = Reservation::find($id);

        $imagepath = \Auth::user()->organization->logo;


        $grc = \PDF::loadView('admin.hotel.reservation.grcard', compact('ord', 'imagepath'));
        $file = $id . '_' . $grc->id . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';

        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }

        return $grc->download($file);
    }

    public function generateConfirmPDF($id)
    {

        $ord = Reservation::find($id);

        $imagepath = \Auth::user()->organization->logo;


        $grc = \PDF::loadView('admin.hotel.reservation.confirmation', compact('ord', 'imagepath'));
        $file = $id . '_' . $grc->id . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';

        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }

        return $grc->download($file);
    }

    public function generateGRCardprint($id)
    {
        $ord = Reservation::find($id);
        $imagepath = \Auth::user()->organization->logo;
        return view('admin.hotel.reservation.grcardprint', compact('ord', 'imagepath'));
    }

    public function unsettledFolio()
    {

        $reservation = \App\Models\Reservation::all();
        $unsetted_res = [];
        foreach ($reservation as $res) {
            $total_folio_amount = \App\Models\Folio::where('reservation_id', $res->id)->sum('total_amount');
            $total_paid_amount = \App\Models\FolioPayment::where('reservation_id', $res->id)->sum('amount');
            $total = $total_folio_amount  - $total_paid_amount;
            if ($total > 0) {
                array_push($unsetted_res, $res->id);
            }
        }
        $reservation = \App\Models\Reservation::whereIn('id', $unsetted_res)->get();
        return view('admin.hotel.reservation.unsettled-folio', compact('reservation'));
    }

    public function unsettledPos()
    {

        $reservation = \App\Models\Reservation::all();
        $unsetted_pos = [];
        foreach ($reservation as $res) {
            $total_pos_amount = \App\Models\Orders::where('reservation_id', $res->id)->sum('total_amount');
            $orders_ids = \App\Models\Orders::where('reservation_id', $res->id)->pluck('id')->toArray();
            $payments = \App\Models\Payment::whereIn('sale_id', $orders_ids)->sum('amount');
            $total = $total_pos_amount  - $payments;
            if ($total > 0) {
                array_push($unsetted_pos, $res->id);
            }
        }
        $orders = \App\Models\Orders::whereIn('reservation_id', $unsetted_pos)->get();
        return view('admin.hotel.reservation.unsettled-pos', compact('orders'));
    }


    public function earlycheckout($id)
    {

        $reservation = Reservation::find($id);

        $earlycheckout = ['check_in' => null, 'check_out' => \Carbon\Carbon::today(), 'reservation_status' => '6', 'book_in' => $reservation->check_in, 'book_out' => $reservation->check_out];

        Reservation::find($id)->update($earlycheckout);

        Flash::success('Early Checked Out');
        return redirect('/admin/hotel/reservation-edit/' . $id);
    }


    public function get_resv(Request $request)
    {

        //  dd($request->room_num);

        $reservation = \App\Models\Reservation::orderBy('id', 'desc')->where('room_num', $request->room_num)->where('reservation_status', 3)->first();

        if(!$reservation){

             return ['success' => 0];

        }


        $data = '<div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label col-sm-4">Guest Name</label>
                      <div class="input-group ">
                          <input type="text" name="guest_name" placeholder="Guest Name" id="guest_name" value="' . $reservation->guest_name . '" class="form-control" >
                          <div class="input-group-addon">
                              <a href="#"><i class="fa fa-credit-card"></i></a>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                     <label class="control-label col-sm-4">Resv NO</label>
                      <div class="input-group ">
                          <input type="text" name="room_reservation_id" placeholder="Reservation" id="reservation_id" value="' . $reservation->id . '" class="form-control"  required>
                          <div class="input-group-addon">
                              <a href="#"><i class="fa fa-credit-card"></i></a>
                          </div>
                      </div>
                    </div>
                </div>';



        return ['success' => 1, 'data' => $data];
    }

    public function search()
    {
        $term = trim(\Request::get('search'));


        $reservation = \App\Models\Reservation::where('guest_name', 'LIKE', '%' . $term . '%')
            ->orwhere('room_num', 'LIKE', '%' . $term . '%')
            ->orWhere('email', 'LIKE', '%' . $term . '%')
            ->orWhere('phone', 'LIKE', '%' . $term . '%')
            ->orWhere('mobile', 'LIKE', '%' . $term . '%')
            ->orWhere('id', 'LIKE', '%' . $term . '%')
            ->orderBy('guest_name', 'asc')->paginate(30);


        $page_title = "Admin | Rerservation | Search";
        $page_description = "List of Reservation by Keyword: " . $term;

        return view('admin.hotel.reservation.search', compact('reservation', 'page_title', 'page_description'));
    }


    public function SendEmail($id)
    {

        $reservation = \App\Models\Reservation::find($id);

        $reservation_child = \App\Models\Reservation::where('parent_res', $id)->get();


        return view('admin.hotel.reservation.emailsample', compact('reservation', 'reservation_child'));
    }

    public function cancelReservation($id)
    {


        // dd($id);

        $reservation = Reservation::where('id', $id)->update(['reservation_status' => 11]);
        $reservation_parent = Reservation::where('parent_res', $id)->update(['reservation_status' => 11]);

        $subject = "Reservation Cancelled";
        $mail_to = $reservation->email;
        $mail_from = env('APP_EMAIL');

        try{

        $mail = \Mail::send('emails.email-reservation-cancelled', [], function ($message) use ($subject, $mail_from, $mail_to, $file, $Lead, $request, $fields, $reservation) {
            //$mail = \Mail::send('emails.email-send', [], function ($message) use ($attributes, $Lead, $request, $fields) {
            $message->subject($subject);
            $message->from($mail_from, env('APP_COMPANY'));
            $message->to($mail_to, '');

            if ($request->file('attachment')) {
                $message->attach('sent_attachments/' . $fields['file']);
            }
        });

        }catch(\Exception $e){



        }




        return redirect()->back();
    }

    public function ReservationByStatus($id)
    {
        //dd($id);

        $reservation = \App\Models\Reservation::orderBy('id', 'desc')->where('parent_res', null)->where('reservation_status', $id)->paginate(30);

        $status_name = \App\Models\ReservationStatus::find($id)->status_name;

        //dd($status_name);

        $page_title = 'Admin | Reservation | Status | ' . $status_name . '';
        $page_description = 'List of Reservation by: ' . $status_name . '';

        // dd($reservation);

        return view('admin.hotel.reservation.reservationbystatus', compact('reservation', 'reservation_child', 'page_title', 'page_description'));
    }

}

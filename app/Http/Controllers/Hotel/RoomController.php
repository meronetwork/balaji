<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use App\Models\RoomType;
use App\Models\RoomStatus;
use App\Models\Room;
use App\Models\Audit as Audit;
use Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRoomType()
    {
        $page_title = 'Room Types';
        $roomtype = RoomType::all();
        return view('admin.hotel.room.room-type-index', compact('roomtype', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRoomType()
    {
        $page_title = 'Create Room Type';
        return view('admin.hotel.room.room-type-create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRoomType(Request $request)
    {
        $roomtype = $request->all();
        RoomType::create($roomtype);
        Flash::success('Room type sucessfully created');
        return redirect('/admin/hotel/room-type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showRoomType($id)
    {
        $page_title = 'Admin | hotel';
        $show = RoomType::where('roomtype_id', $id)->first();
        return view('admin.hotel.room.room-type-show', compact('show', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editRoomType($id)
    {
        $page_title = 'Admin | Room Type';
        $edit = RoomType::where('roomtype_id', $id)->first();
        return view('admin.hotel.room.room-type-edit', compact('edit', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRoomType(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);
        RoomType::where('roomtype_id', $id)->update($update);

        Flash::success('Room type sucessfully updated');
        return redirect('/admin/hotel/room-type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyRoomType($id)
    {
        RoomType::where('roomtype_id', $id)->delete();
        Flash::success('Room type sucessfully deleted');
        return redirect('/admin/hotel/room-type');
    }
    public function getModalDeleteRoomType($id)
    {
        $error = null;
        $roomtype = RoomType::where('roomtype_id', $id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete Room Type";
        $modal_body = "Are you sure that you want to delete room type id " . $roomtype->roomtype_id . " with the name " . $roomtype->room_name . "? This operation is irreversible";
        $modal_route = route('admin.hotel.room-type-delete', $roomtype->roomtype_id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function indexRoom()
    {

        // $room = Room::select('room.room_number','room.room_id','room.floor_number','room.status_id','room_types.room_name,room_status.status_color')
        //                 ->leftjoin('room_status','room_status.status_id','=','room.status_id')
        //                 ->leftjoin('room_types','room_types.roomtype_id','=','room.roomtype_id')
        //                 ->orderBy('room.room_id','DESC')->paginate(30);

        $room = \App\Models\Room::orderBy('room_id', 'desc')->paginate(30);

        $page_title = "Admin | Room";
        return view('admin.hotel.room.room-index', compact('room', 'page_title'));
    }

    public function createRoom()
    {

        $roomtype = RoomType::all();
        $roomstatus = RoomStatus::all();
        $page_title = "Admin|room";
        return view('admin.hotel.room.room-create', compact('roomstatus', 'roomtype', 'page_title'));
    }
    public function storeRoom(Request $request)
    {
        $this->validate($request, array(
            'room_number'    => 'required|unique:room',
        ));

        $room = $request->all();
        $room = Room::create($room);
        Flash::success('Room  sucessfully added');
        return redirect('/admin/hotel/room-index');
    }
    public function editRoom($id)
    {
        $roomtype = RoomType::all();
        $roomstatus = RoomStatus::all();
        $edit = Room::where('room_id', $id)->first();
        return view('admin.hotel.room.room-edit', compact('edit', 'roomtype', 'roomstatus'));
    }
    public function updateRoom(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);
        Room::where('room_id', $id)->update($update);
        Flash::success('Room  sucessfully updated');
        return redirect('/admin/hotel/room-index');
    }


    public function getModalDeleteRoom($id)
    {
        $error = null;
        $room = Room::where('room_id', $id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete room";
        $modal_body = "Are you sure that you want to delete room id " . $room->room_id . " with the number " . $room->room_number . "? This operation is irreversible";
        $modal_route = route('admin.hotel.room-delete', $room->room_id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function destroyRoom($id)
    {
        Room::where('room_id', $id)->delete();
        Flash::success('Room  sucessfully deleted');
        return redirect('/admin/hotel/room-index');
    }

    public function indexRoomStatus()
    {
        $page_title = 'Admin | hotel';
        $status1 = RoomStatus::all();
        return view('admin.hotel.room.room-status-index', compact('status1', 'page_title'));
    }
    public function createRoomStatus()
    {
        $page_title = 'Admin | hotel';
        $description = "Create a new room status";
        return view('admin.hotel.room.room-status-create', compact('description', 'page_title'));
    }
    public function storeRoomStatus(Request $request)
    {
        $status = $request->all();
        RoomStatus::create($status);
        Flash::success("Room status sucessfully created");
        return redirect('/admin/hotel/room-status-index');
    }
    public function editRoomStatus($id)
    {
        $page_title = 'Admin | hotel';
        $edit = RoomStatus::where('status_id', $id)->first();
        return view('admin.hotel.room.room-status-create', compact('edit', 'page_title'));
    }
    public function updateRoomStatus(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);
        RoomStatus::where('status_id', $id)->update($update);
        Flash::success("Room status sucessfully Updated");
        return redirect('/admin/hotel/room-status-index');
    }
    public function getModalDeleteRoomStatus($id)
    {
        $error = null;
        $roomstatus = RoomStatus::where('status_id', $id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete RoomStatus";
        $modal_body = "Are you sure that you want to delete hotel id " . $roomstatus->status_id . " with the name " . $roomstatus->status_name . "? This operation is irreversible";
        $modal_route = route('admin.hotel.status-delete', $roomstatus->status_id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function destroyRoomStatus($id)
    {
        RoomStatus::where('status_id', $id)->delete();
        Room::where('status_id', $id)->delete();
        Flash::success('Room status sucessfully deleted');
        return redirect('/admin/hotel/room-status-index/');
    }

    public function rooms_overview(Request $request)
    {
        
        Audit::log(Auth::user()->id, 'rooms_overview', 'show', ['name' => 'overview']);
        if (\Request::has('specific_date'))
            $date = $request->specific_date;
        else
            $date = date('Y-m-d');

        //dd($date);

        $page_title =  "Admin | Overview | Show";
        $page_description =  "Displaying Todays Checkouts";
        $room_status = '';
        $rooms = Room::select('room.room_number', 'room.room_id', 'room.floor_number', 'room.status_id', 'room_types.room_name', 'room_status.status_color')
            ->leftjoin('room_status', 'room_status.status_id', '=', 'room.status_id')
            ->leftjoin('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')
            ->orderBy('room.room_number', 'asc')->get();

        //  dd($rooms);

        $room_status = [];
        foreach ($rooms as $room) {
            $guest = \App\Models\Reservation::where('room_num', $room->room_number)
                ->where('reservation_status','3')->first();

            //dd($guest);
            if ($guest) {
                if ($guest->guest_id) {
                    array_push($room_status, [$guest->guest->full_name, $guest->reservation_state->status_color]);

                    //  dd($guest);
                } else {
                    array_push($room_status, [$guest->guest_name, $guest->reservation_state->status_color]);
                }
            } else {
                array_push($room_status, ['&nbsp;', $guest->reservation_state->status_color??'']);
            }
        }

        //  dd($room_status);

        $res_state = \App\Models\ReservationStatus::all();

        return view('admin.hotel.reservation.overview', compact('page_title', 'page_description', 'rooms', 'room_status', 'res_state'));
    }


    public function get_rooms()
    {


        $term = strtolower(\Request::get('term'));

        //dd($term);
        $rooms = \App\Models\Room::select('room_id', 'room_number')->where('room_number', 'LIKE', '%' . $term . '%')->groupBy('room_number')->take(5)->get();

        //dd($rooms);

        //  dd($ledgers);
        $return_array = array();

        foreach ($rooms as $v) {

            $return_array[] = array('value' => $v->room_number, 'id' => $v->room_id);
        }
        return \Response::json($return_array);
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;


class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){ 

        $page_title = 'Admin | Hotel | Articles';

        $page_description = 'List of hotel Articles';

        $articles = \App\Models\Article::orderBy('id','desc')->get();
        
        return view('admin.hotel.articles.index',compact('articles','page_title','page_description'));
    }


    public function create(){
      
        $page_title = 'Create | Hotel | Articles';
        $page_description = '';
        $transactioncodes = \App\Models\TransactionCodes::orderBy('id','desc')->get();

        return view('admin.hotel.articles.create',compact('roomtype','page_title','page_description','transactioncodes'));
    }


    public function store(Request $request){

        $this->validate($request, array(
            'code'      => 'required',
            'description'=>'required',
           
        ));

        $attributes = $request->all();
       
        //dd($attributes);

        $transactioncodes = \App\Models\Article::create($attributes);
        Flash::success('Article  sucessfully added');
        return redirect('/admin/hotel/articles');

    }


    public function edit($id){

        $articles = \App\Models\Article::find($id);

        $page_title = 'Edit Articles';
        $page_description = '';

        $transactioncodes = \App\Models\TransactionCodes::orderBy('id','desc')->get();

        return view ('admin.hotel.articles.edit',compact('transactioncodes','page_title','page_description','articles'));
    }


    public function update(Request $request , $id){

         $this->validate($request, array(
            'code'      => 'required',
            'description'=>'required',
           
        ));
      
        $attributes = $request->all();

        

        \App\Models\Article::find($id)->update($attributes);

        Flash::success('Articles sucessfully updated');
        return redirect('/admin/hotel/articles');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $articles = \App\Models\Article::find($id);
       
        $modal_title = "Delete Articles ";
        $modal_body ="Are you sure that you want to delete articles id ".$articles->id ." with the code ".$articles->code."? This operation is irreversible";

        $modal_route = route('admin.hotel.articles.delete',$articles->id);  
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }


    public function destroy($id){
         $articles = \App\Models\Article::find($id)->delete();
        Flash::success('Articles  sucessfully deleted');
        return redirect('/admin/hotel/articles');
    }


     public function getArticleDetailAjax($productId)
    {

       // dd($productId);

        $product = \App\Models\Article::select('id', 'code', 'price', 'description')->where('description', $productId)->first();


        return ['data' => json_encode($product)];
    }  

    

}

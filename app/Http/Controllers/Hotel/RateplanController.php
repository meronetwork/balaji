<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use App\Models\RoomType;
use App\Models\Rateplan;
class RateplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        
        $plan = Rateplan::select('room_types.room_name','roomtype_rate_plans.package_name','roomtype_rate_plans.rate','roomtype_rate_plans.breakfast_rate','roomtype_rate_plans.lunch_rate','roomtype_rate_plans.dinner_rate','roomtype_rate_plans.order_num','roomtype_rate_plans.enabled','roomtype_rate_plans.id')
        ->join('room_types','room_types.roomtype_id','=','roomtype_rate_plans.roomtype_id')
        ->orderBy('roomtype_rate_plans.id','DESC')
                      ->paginate(30);

        $page_title = 'Rate Plans';

        return view('admin.hotel.rateplan.index',compact('plan','page_title'));
    }


    public function create(){
        $roomtype = RoomType::all();
        $page_title = 'Create Rate Plans';

        return view('admin.hotel.rateplan.create',compact('roomtype','page_title'));
    }


    public function store(Request $request){
        $room = $request->all();
        $room = Rateplan::create($room);
        Flash::success('Plan  sucessfully added');
        return redirect('/admin/hotel/rate-plans');
    }


    public function edit($id){
        $roomtype = RoomType::all();
        $edit = Rateplan::where('id',$id)->first();
        $page_title = 'Edit Rate Plans';

        return view ('admin.hotel.rateplan.edit',compact('edit','roomtype','page_title'));
    }


    public function update(Request $request , $id){
      
        $update = $request->all();
        unset($update['_token']);


        Rateplan::where('id',$id)->update($update);

        Flash::success('Plan  sucessfully updated');
        return redirect('/admin/hotel/rate-plans');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $rate = Rateplan::where('id',$id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete Rate Plan";
        $modal_body ="Are you sure that you want to delete rate id ".$rate->id ." with the number ".$rate->package_name."? This operation is irreversible";
        $modal_route = route('admin.hotel.rate-delete',$rate->id);
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }


    public function destroy($id){
        Rateplan::where('id',$id)->delete();
        Flash::success('Plan  sucessfully deleted');
        return redirect('/admin/hotel/rate-plans');
    }

    

}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;


class TransactionCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){ 

        $page_title = 'Admin | Hotel | Transaction | Codes';

        $page_description = 'List of Transaction Groups';

        $transactioncodes = \App\Models\TransactionCodes::orderBy('id','desc')->get();
        
        return view('admin.hotel.transactioncodes.index',compact('transactioncodes','page_title','page_description'));
    }


    public function create(){
      
        $page_title = 'Create | Hotel | Transactions';
        $page_description = '';
        $transactiongroups = \App\Models\TransactionGroups::orderBy('id','desc')->get();

        return view('admin.hotel.transactioncodes.create',compact('roomtype','page_title','page_description','transactiongroups'));
    }


    public function store(Request $request){

        $this->validate($request, array(
            'code'      => 'required',
           
        ));

        $attributes = $request->all();
        if(!isset($attributes['enabled']))
            $attributes['enabled'] = 0;
        //dd($attributes);

        $transactioncodes = \App\Models\TransactionCodes::create($attributes);
        Flash::success('Transaction Codes  sucessfully added');
        return redirect('/admin/hotel/transaction/codes');

    }


    public function edit($id){

        $transactioncodes = \App\Models\TransactionCodes::find($id);

        $page_title = 'Edit Transaction Groups';
        $page_description = '';

        $transactiongroups = \App\Models\TransactionGroups::orderBy('id','desc')->get();

        return view ('admin.hotel.transactioncodes.edit',compact('transactioncodes','page_title','page_description','transactiongroups'));
    }


    public function update(Request $request , $id){

        $this->validate($request, array(
            'code'      => 'required',
           
        ));
      
        $attributes = $request->all();

        if(!isset($attributes['enabled']))
            $attributes['enabled'] = 0;

        \App\Models\TransactionCodes::find($id)->update($attributes);

        Flash::success('Transaction Codes sucessfully updated');
        return redirect('/admin/hotel/transaction/codes');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $transactioncodes = \App\Models\TransactionCodes::find($id);
       
        $modal_title = "Delete Transaction Codes";
        $modal_body ="Are you sure that you want to delete transaction codes id ".$transactioncodes->id ." with the number ".$transactioncodes->code."? This operation is irreversible";

        $modal_route = route('admin.hotel.transaction.codes.delete',$transactioncodes->id);
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }


    public function destroy($id){
         $transactioncodes = \App\Models\TransactionCodes::find($id)->delete();
        Flash::success('Transaction Codes  sucessfully deleted');
        return redirect('/admin/hotel/transaction/codes');
    }

    

}

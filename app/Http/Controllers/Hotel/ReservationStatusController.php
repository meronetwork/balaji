<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
class ReservationStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Admin | reservation';
     $status1 = \App\Models\ReservationStatus::all();
      return view('admin.hotel.reservation-status.index',compact('status1','page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $description="Create a reservation status";
        $page_title = 'Admin | reservation ';
        return view('admin.hotel.reservation-status.create',compact('page_title','description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = $request->all();
        \App\Models\ReservationStatus::create($status);
        Flash::success("Reservation status sucessfully created");
        return redirect('/admin/hotel/res-status-index/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { $description="Create a reservation status";
         $page_title = 'Admin | reservation ';
        $edit = \App\Models\ReservationStatus::find($id);
        return view('admin.hotel.reservation-status.create',compact('edit','page_title','description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = $request->all();
        \App\Models\ReservationStatus::find($id)->update($update);
        Flash::success("Reservation status sucessfully updated");
        return redirect('/admin/hotel/res-status-index/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Models\ReservationStatus::find($id)->delete();
         Flash::success("Reservation status sucessfully deleted");
        return redirect('/admin/hotel/res-status-index/');

    }

     public function getModalDelete($id)
    {
        $error = null;
        $res_status =  \App\Models\ReservationStatus::find($id);
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete reservation status";
        $modal_body ="Are you sure that you want to delete reservation status id ".$res_status->id ." with the name ".$res_status->status_name."? This operation is irreversible";
        $modal_route = route('admin.hotel.res-status-delete',$res_status->id);
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }

}

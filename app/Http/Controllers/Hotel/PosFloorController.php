<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;


class PosFloorController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $page_title = 'Admin | POS | Floor';

        $page_description = 'List of pos floors';

        $posfloor = \App\Models\PosFloor::orderBy('id', 'desc')->get();

        return view('admin.hotel.posfloors.index', compact('posfloor', 'page_title', 'page_description'));
    }


    public function create()
    {

        $page_title = 'Create | POS | Floor';
        $page_description = '';

        $posoutlets = \App\Models\PosOutlets::where('enabled', '1')->pluck('name', 'id')->all();

        return view('admin.hotel.posfloors.create', compact('page_title', 'page_description', 'posoutlets'));
    }


    public function store(Request $request)
    {

        $this->validate($request, array(
            'name'      => 'required',
        ));

        $attributes = $request->all();

        $posfloors = \App\Models\PosFloor::create($attributes);
        Flash::success('POS Floor Groups  sucessfully added');
        return redirect('/admin/hotel/posfloors');
    }


    public function edit($id)
    {

        $posfloor = \App\Models\PosFloor::find($id);

        $posoutlets = \App\Models\PosOutlets::where('enabled', '1')->pluck('name', 'id')->all();

        $page_title = 'Edit Pos Floors';
        $page_description = '';

        return view('admin.hotel.posfloors.edit', compact('posoutlets', 'page_title', 'page_description', 'posfloor'));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, array(
            'name'      => 'required',
        ));

        $attributes = $request->all();

        \App\Models\PosFloor::find($id)->update($attributes);

        Flash::success('Pos Floor sucessfully updated');
        return redirect('/admin/hotel/posfloors');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $posfloor = \App\Models\PosFloor::find($id);

        $modal_title = "Delete Pos Floor";
        $modal_body = "Are you sure that you want to delete pos floors id " . $posfloor->id . " with the name: " . $posfloor->name . "? This operation is irreversible";

        $modal_route = route('admin.hotel.posfloors.delete', $posfloor->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }


    public function destroy($id)
    {

        $posfloor = \App\Models\PosFloor::find($id)->delete();
        Flash::success('POS Floor sucessfully deleted');
        return redirect('/admin/hotel/posfloors');
    }
}

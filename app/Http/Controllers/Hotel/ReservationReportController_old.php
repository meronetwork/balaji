<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use App\Models\Attraction;
use App\Models\Hotel;
class ReservationReportControllerOld extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {  
        $page_title = 'Admin | Reservation | Reports | Index';
        $page_description = 'Lists of Reports Available.'; 
       
        return view('admin.hotel.reservationreports.index',compact('page_title','page_description'));
    }

    public function ManagerFlashReport(){

        $now = \Carbon\Carbon::now();
        $last = \Carbon\Carbon::now()->subYear(1);

        $this_year = $now->year;
        $last_year = $last->year; 

        $total_days_this_year = 365 + $now->format('L');
        $total_days_last_year = 365 + $last->format('L');

       // dd($total_days_last_year);

        //Total Rooms In Hotel

        $total_rooms_today = \App\Models\Room::count();
        $total_days_this_month = $total_rooms_today * 30;
        $total_room_this_year = $total_rooms_today * $total_days_this_year;

        $total_room_last_day = \App\Models\Room::count();
        $total_rooms_last_year_month = $total_room_last_day * 30;
        $total_rooms_last_year = $total_room_last_day * $total_days_last_year;

        //Rooms Occupied

        $today_date = \Carbon\Carbon::today();
        $this_month = \Carbon\Carbon::today()->subDays(30);
        $this_year_start = \Carbon\Carbon::today()->startOfYear();
        $this_year_end = \Carbon\Carbon::today()->endOfYear()->format('Y-m-d');

        $last_year_today= \Carbon\Carbon::today()->subYear(1);
        $last_this_month = \Carbon\Carbon::today()->subYear(1)->subDays(30);
        $last_year_start = \Carbon\Carbon::today()->subYear(1)->startOfYear();
        $last_year_end = \Carbon\Carbon::today()->subYear(1)->endOfYear()->format('Y-m-d');

        //dd($last_year_end);

        //dd($this_month);

       // Rooms Occupied

        $today_rooms_occupied = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','3')->count();


        $this_month_rooms_occupied = 0;
        $this_month_rooms_occupied_get = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->whereIn('reservation_status',['3','6'])->get();

         foreach($this_month_rooms_occupied_get as $tmcg){
             
             $this_month_rooms_occupied = $this_month_rooms_occupied + $tmcg->number_of_days;
            
          }


        $this_year_rooms_occupied = 0;
        $this_year_rooms_occupied_get = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->whereIn('reservation_status',['3','6'])->get();

         foreach($this_year_rooms_occupied_get as $tmcg){
             
             $this_year_rooms_occupied = $this_year_rooms_occupied + $tmcg->number_of_days;
            
          }

        $last_year_today_rooms_occupied = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->whereIn('reservation_status',['3','6'])->count();


        $last_this_month_rooms_occupied = 0;
        $last_this_month_rooms_occupied_get = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_this_month_rooms_occupied_get as $tmcg){
             $last_this_month_rooms_occupied = $last_this_month_rooms_occupied + $tmcg->number_of_days;
            
          }


        $last_year_rooms_occupied = 0;
        $last_year_rooms_occupied_get = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_year_rooms_occupied_get as $tmcg){
             $last_year_rooms_occupied = $last_year_rooms_occupied + $tmcg->number_of_days; 
          }


  ///   Complementry Rooms


        $today_complementry = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('guest_type','COM')->count();

        $this_month_complementry = 0;

        $this_month_complementry_get = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->whereIn('reservation_status',['3','6'])->where('guest_type','COM')->get();

          foreach($this_month_complementry_get as $tmcg){
             
             $this_month_complementry = $this_month_complementry + $tmcg->number_of_days;
            
          }


        $this_year_complementry = 0;
        $this_year_complementry_get = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_out','<=',$this_year_end)->whereIn('reservation_status',['3','6'])->where('guest_type','COM')->get();

         foreach($this_year_complementry_get as $tmcg){

            $this_year_complementry = $this_year_complementry + $tmcg->number_of_days; 
            
          }

        $last_year_today_complementry = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->whereIn('reservation_status',['3','6'])->where('guest_type','COM')->count();


        $last_this_month_complementry = 0;
        $last_this_month_complementry_get = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_out','<=',$last_year_today)->whereIn('reservation_status',['3','6'])->where('guest_type','COM')->get();

        foreach($last_this_month_complementry_get as $tmcg){
               $last_this_month_complementry = $last_this_month_complementry + $tmcg->number_of_days;

        }
       
        $last_year_complementry = 0;
        $last_year_complementry_get = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_out','<=',$last_year_end)->whereIn('reservation_status',['3','6'])->where('guest_type','COM')->get(); 

         foreach($last_year_complementry_get as $tmcg){
               $last_year_complementry = $last_year_complementry + $tmcg->number_of_days;

        }

      // In House Adults
         $today_house_adults = 0;
         $today_house_adults_get = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','3')->get();
          foreach($today_house_adults_get as $tmcg){
             $today_house_adults = $today_house_adults + $tmcg->occupancy;
          }


        $this_month_adults_occupied = 0;
        $this_month_adults_occupied_get = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->whereIn('reservation_status',['3','6'])->get();

         foreach($this_month_adults_occupied_get as $tmcg){
             $this_month_adults_occupied = $this_month_adults_occupied + $tmcg->occupancy;
          }


        $this_year_adults_occupied = 0;
        $this_year_adults_occupied_get = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->whereIn('reservation_status',['3','6'])->get();

         foreach($this_year_adults_occupied_get as $tmcg){
             $this_year_adults_occupied = $this_year_adults_occupied + $tmcg->occupancy;
          }   
        
        $last_year_today_adults_occupied = 0;
        $last_year_today_adults_occupied_get = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_year_today_adults_occupied_get as $tmcg){
             $last_year_today_adults_occupied = $last_year_today_adults_occupied + $tmcg->occupancy;
          }



        $last_this_month_adults_occupied = 0;
        $last_this_month_adults_occupied_get = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_this_month_adults_occupied_get as $tmcg){
             $last_this_month_adults_occupied = $last_this_month_adults_occupied + $tmcg->occupancy;
            
          }


        $last_year_adults_occupied = 0;
        $last_year_adults_occupied_get = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_year_adults_occupied_get as $tmcg){
             $last_year_adults_occupied = $last_year_adults_occupied + $tmcg->occupancy; 
          }


           // In House Child
         $today_house_childs = 0;
         $today_house_childs_get = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','3')->get();
          foreach($today_house_childs_get as $tmcg){
             $today_house_childs = $today_house_childs + $tmcg->occupancy_child;
          }


        $this_month_childs_occupied = 0;
        $this_month_childs_occupied_get = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->whereIn('reservation_status',['3','6'])->get();

         foreach($this_month_childs_occupied_get as $tmcg){
             $this_month_childs_occupied = $this_month_childs_occupied + $tmcg->occupancy_child;
          }


        $this_year_childs_occupied = 0;
        $this_year_childs_occupied_get = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->whereIn('reservation_status',['3','6'])->get();

         foreach($this_year_childs_occupied_get as $tmcg){
             $this_year_childs_occupied = $this_year_childs_occupied + $tmcg->occupancy_child;
          }   
        
        $last_year_today_childs_occupied = 0;
        $last_year_today_childs_occupied_get = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_year_today_childs_occupied_get as $tmcg){
             $last_year_today_childs_occupied = $last_year_today_childs_occupied + $tmcg->occupancy_child;
          }



        $last_this_month_childs_occupied = 0;
        $last_this_month_childs_occupied_get = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_this_month_childs_occupied_get as $tmcg){
             $last_this_month_childs_occupied = $last_this_month_childs_occupied + $tmcg->occupancy_child;
            
          }


        $last_year_childs_occupied = 0;
        $last_year_childs_occupied_get = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->whereIn('reservation_status',['3','6'])->get();

         foreach($last_year_childs_occupied_get as $tmcg){
             $last_year_childs_occupied = $last_year_childs_occupied + $tmcg->occupancy_child; 
          }

        // Individual Person 

       
        $today_house_individual = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','3')->where('occupancy','1')->count();
          
        $this_month_individual = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->whereIn('reservation_status',['3','6'])->where('occupancy','1')->count();

        $this_year_individual = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->whereIn('reservation_status',['3','6'])->where('occupancy','1')->count();
        
        $last_year_today_individual = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->whereIn('reservation_status',['3','6'])->where('occupancy','1')->count();


        $last_this_month_individual = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->whereIn('reservation_status',['3','6'])->where('occupancy','1')->count();

        $last_year_individual = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->whereIn('reservation_status',['3','6'])->where('occupancy','1')->count();


        // VIP Persons  

        $today_house_vip = 0;
        $today_house_vip_get = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','3')->where('guest_type','VIP')->get();

         foreach($today_house_vip_get as $tmcg){
             $today_house_vip = $today_house_vip + $tmcg->occupancy;
          }
          
        $this_month_vip = 0;
        $this_month_vip_get = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->whereIn('reservation_status',['3','6'])->where('guest_type','VIP')->get();

         foreach($this_month_vip_get as $tmcg){
             $this_month_vip = $this_month_vip + $tmcg->occupancy;
          }

        $this_year_vip = 0;
        $this_year_vip_get = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->whereIn('reservation_status',['3','6'])->where('guest_type','VIP')->get();

         foreach($this_year_vip_get as $tmcg){
             $this_year_vip = $this_year_vip + $tmcg->occupancy;
          }

        $last_year_today_vip = 0;
        
        $last_year_today_vip_get = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->whereIn('reservation_status',['3','6'])->where('guest_type','VIP')->get();

         foreach($last_year_today_vip_get as $tmcg){
             $last_year_today_vip = $last_year_today_vip + $tmcg->occupancy;
          }

        $last_this_month_vip = 0;
        $last_this_month_vip_get = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->whereIn('reservation_status',['3','6'])->where('guest_type','VIP')->get();

         foreach($last_this_month_vip_get as $tmcg){
             $last_this_month_vip = $last_this_month_vip + $tmcg->occupancy;
          }


        $last_year_vip = 0;
        $last_year_vip_get = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->whereIn('reservation_status',['3','6'])->where('guest_type','VIP')->get();

         foreach($last_year_vip_get as $tmcg){
             $last_year_vip = $last_year_vip + $tmcg->occupancy;
          }

        //No Show Rooms

        $today_no_show_rooms = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','10')->count();
        $this_month_no_show_rooms = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->where('reservation_status','10')->count();
        $this_year_no_show_rooms = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->where('reservation_status','10')->count();
        $last_year_today_no_show_rooms = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->where('reservation_status','10')->count();
        $last_this_month_no_show_rooms= \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->where('reservation_status','10')->count();
        $last_year_no_show_rooms = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->where('reservation_status','10')->count();


        //No Shows Person
        
        $today_no_show_persons=0;
        $today_no_show_persons_get = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','10')->get();

        foreach($today_no_show_persons_get as $tmcg){
             $today_no_show_persons = $today_no_show_persons + $tmcg->occupancy;
          }

        $this_month_no_show_persons = 0;
        $this_month_no_show_persons_get = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->where('reservation_status','10')->get();

        foreach($this_month_no_show_persons_get as $tmcg){
             $this_month_no_show_persons = $this_month_no_show_persons + $tmcg->occupancy;
          }

        $this_year_no_show_persons=0;
        $this_year_no_show_persons_get = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->where('reservation_status','10')->get();

        foreach($this_year_no_show_persons_get as $tmcg){
             $this_year_no_show_persons = $this_year_no_show_persons + $tmcg->occupancy;
          }

        $last_year_today_no_show_persons = 0;
        $last_year_today_no_show_persons_get = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->where('reservation_status','10')->get();

        foreach($last_year_today_no_show_persons_get as $tmcg){
             $last_year_today_no_show_persons = $last_year_today_no_show_persons + $tmcg->occupancy;
          }

        $last_this_month_no_show_persons = 0;
        $last_this_month_no_show_persons_get = \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->where('reservation_status','10')->get();

        foreach($last_this_month_no_show_persons_get as $tmcg){
             $last_this_month_no_show_persons = $last_this_month_no_show_persons + $tmcg->occupancy;
          }

        $last_year_no_show_persons = 0;
        $last_year_no_show_persons_get = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->where('reservation_status','10')->get();

        foreach($last_year_no_show_persons_get as $tmcg){
             $last_year_no_show_persons = $last_year_no_show_persons + $tmcg->occupancy;
          }


        // Cancelled

        $today_cancelled = \App\Models\Reservation::where('check_in','<=',$today_date)->where('check_out','>=',$today_date)->where('reservation_status','11')->count();
        $this_month_cancelled = \App\Models\Reservation::where('check_in','>=',$this_month)->where('check_in','<=',$today_date)->where('reservation_status','11')->count();
        $this_year_cancelled = \App\Models\Reservation::where('check_in','>=',$this_year_start)->where('check_in','<=',$this_year_end)->where('reservation_status','11')->count();
        $last_year_today_cancelled = \App\Models\Reservation::where('check_in','<=',$last_year_today)->where('check_out','>=',$last_year_today)->where('reservation_status','11')->count();
        $last_this_month_cancelled= \App\Models\Reservation::where('check_in','>=',$last_this_month)->where('check_in','<=',$last_year_today)->where('reservation_status','11')->count();
        $last_year_cancelled = \App\Models\Reservation::where('check_in','>=',$last_year_start)->where('check_in','<=',$last_year_end)->where('reservation_status','11')->count();

        //Reservation Made Today

        $today_reservation = \App\Models\Reservation::where('created_at','<=',\Carbon\Carbon::tomorrow())->where('created_at','>=',$today_date)->count();

        $this_month_reservation = \App\Models\Reservation::where('created_at','>=',$this_month)->where('created_at','<=',$today_date)->count();

        $this_year_reservation = \App\Models\Reservation::where('created_at','>=',$this_year_start)->where('created_at','<=',$this_year_end)->count();

        $last_year_today_reservation = \App\Models\Reservation::where('created_at','<=',$last_year_today)->where('created_at','>=',$last_year_today)->count();

        $last_this_month_reservation = \App\Models\Reservation::where('created_at','>=',$last_this_month)->where('created_at','<=',$last_year_today)->count();

        $last_year_reservation = \App\Models\Reservation::where('created_at','>=',$last_year_start)->where('created_at','<=',$last_year_end)->count();

        // Arrival Tomorrow

        $tomorrow_arrival = \App\Models\Reservation::where('check_in',\Carbon\Carbon::tomorrow()->format('Y-m-d'))->where('reservation_status','2')->count();

        $last_year_tomorrow_arrival = \App\Models\Reservation::where('check_in',\Carbon\Carbon::tomorrow()->subYear(1)->format('Y-m-d'))->count();

       // Arrival Persons Tomorrow
        $tomorrow_arrival_persons = 0;
        $tomorrow_arrival_persons_get = \App\Models\Reservation::where('check_in',\Carbon\Carbon::tomorrow()->format('Y-m-d'))->where('reservation_status','2')->get();

          foreach($tomorrow_arrival_persons_get as $tmcg){
             $tomorrow_arrival_persons = $tomorrow_arrival_persons + $tmcg->occupancy;
          }

        $last_year_tomorrow_arrival_persons = 0;
        $last_year_tomorrow_arrival_persons_get = \App\Models\Reservation::where('check_in',\Carbon\Carbon::tomorrow()->subYear(1)->format('Y-m-d'))->get();

          foreach($last_year_tomorrow_arrival_persons_get as $tmcg){
             $last_year_tomorrow_arrival_persons = $last_year_tomorrow_arrival_persons + $tmcg->occupancy;
          }

        //  Depature Rooms For Tomorrow

        $tomorrow_depature = \App\Models\Reservation::where('check_out',\Carbon\Carbon::tomorrow()->format('Y-m-d'))->where('reservation_status','2')->count();

        $last_year_tomorrow_depature = \App\Models\Reservation::where('check_out',\Carbon\Carbon::tomorrow()->subYear(1))->count();

        //Depature Persons for Tomorrow

        $tomorrow_depature_persons = 0;
        $tomorrow_depature_persons_get = \App\Models\Reservation::where('check_out',\Carbon\Carbon::tomorrow()->format('Y-m-d'))->where('reservation_status','2')->get();
        foreach($tomorrow_depature_persons_get as $tmcg){
             $tomorrow_depature_persons = $tomorrow_depature_persons + $tmcg->occupancy;
          }

        $last_year_tomorrow_depature_persons = 0;
        $last_year_tomorrow_depature_persons_get = \App\Models\Reservation::where('check_out',\Carbon\Carbon::tomorrow()->subYear(1))->get();
         foreach($last_year_tomorrow_depature_persons_get as $tmcg){
             $last_year_tomorrow_depature_persons = $last_year_tomorrow_depature_persons + $tmcg->occupancy;
          }




        $stamp = time();

        $pdf = \PDF::loadView('admin.hotel.reservationreports.managerflashreport',compact('this_year','last_year','total_rooms_today','total_days_this_month','total_room_this_year','total_room_last_day','total_rooms_last_year_month','total_rooms_last_year','today_rooms_occupied','this_month_rooms_occupied','this_year_rooms_occupied','last_year_today_rooms_occupied','last_this_month_rooms_occupied','last_year_rooms_occupied','today_complementry','this_month_complementry','this_year_complementry','last_year_today_complementry','last_this_month_complementry','last_year_complementry','today_house_adults','this_month_adults_occupied','this_year_adults_occupied','last_year_today_adults_occupied','last_this_month_adults_occupied','last_year_adults_occupied','today_house_childs','this_month_childs_occupied','this_year_childs_occupied','last_year_today_childs_occupied','last_this_month_childs_occupied','last_year_childs_occupied','today_house_individual','this_month_individual','this_year_individual','last_year_today_individual','last_this_month_individual','last_year_individual','today_house_vip','this_month_vip','this_year_vip','last_year_today_vip','last_this_month_vip','last_year_vip','today_no_show_rooms','this_month_no_show_rooms','this_year_no_show_rooms','last_year_today_no_show_rooms','last_this_month_no_show_rooms','last_year_no_show_rooms','today_cancelled','this_month_cancelled','this_year_cancelled','last_year_today_cancelled','last_this_month_cancelled','last_year_cancelled','today_no_show_persons','this_month_no_show_persons','this_year_no_show_persons','last_year_today_no_show_persons','last_this_month_no_show_persons','last_year_no_show_persons','today_reservation','this_month_reservation','this_year_reservation','last_year_today_reservation','last_this_month_reservation','last_year_reservation','tomorrow_arrival','last_year_tomorrow_arrival','tomorrow_arrival_persons','last_year_tomorrow_arrival_persons','tomorrow_depature','last_year_tomorrow_depature','tomorrow_depature_persons','last_year_tomorrow_depature_persons'));

        $file =''.$stamp.'-managerflashreport.pdf';
        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);  

    }

    public function GuestLedger(){

        $reservations = \App\Models\Reservation::orderBy('id','desc')->where('reservation_status','3')->get();

        //dd($reservations);

        $stamp = time();

        $pdf = \PDF::loadView('admin.hotel.reservationreports.guestledger', compact('reservations')); 
        $file =''.$stamp.'-guestledgers.pdf';

        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
    }

    public function RevenueReport(){

        $sources = \App\Models\Communication::orderBy('id','desc')->get();

        $stamp = time(); 
        $pdf = \PDF::loadView('admin.hotel.reservationreports.revenuereports',compact('sources'));
        $file =''.$stamp.'-revenuereports.pdf';

        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
    }

    public function ResturantSales(){

        $page_title = 'Admin | Reservation | Reports | Resturant Sales';  

        $date_now = date('Y-m-d');

        $date_today = date('Y-m-d');

        $date_week_after = date('Y-m-d', strtotime($date_now . ' -7 day'));

       // dd($date_week_after);
        $dates = array();

         for($i=1;$i<=7;$i++){

              array_push($dates,$date_now);
              $date_now = date('Y-m-d', strtotime($date_now . ' -1 day'));
        }

        //dd($dates);

        $producttypemasters = \App\Models\ProductTypeMaster::orderBy('id','desc')->get();

        //dd($producttypemasters);

        return view('admin.hotel.reservationreports.resturantsales',compact('dates','producttypemasters')); 


        // $stamp = time(); 
        // $pdf = \PDF::loadView('admin.hotel.reservationreports.resturantsales',compact('sources'));
        // $file =''.$stamp.'-revenuereports.pdf';

        // if (\File::exists('reports/'.$file)) 
        // {
        //     \File::Delete('reports/'.$file);
        // }
        // return $pdf->download($file);
    }

   
}

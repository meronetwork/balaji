<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;


class TransactionGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){ 

        $page_title = 'Admin | Hotel | Transaction | Groups';

        $page_description = 'List of Transaction Groups';

        $transactiongroups = \App\Models\TransactionGroups::orderBy('id','desc')->get();
        
        return view('admin.hotel.transactiongroups.index',compact('transactiongroups','page_title','page_description'));
    }


    public function create(){
      
        $page_title = 'Create | Hotel | Transactions';
        $page_description = '';

        return view('admin.hotel.transactiongroups.create',compact('page_title','page_description'));
    }


    public function store(Request $request){

        $this->validate($request, array(
            'group_code'      => 'required',
           
        ));

        $attributes = $request->all();
        if(!isset($attributes['enabled']))
            $attributes['enabled'] = 0;
        //dd($attributes);

        $transactiongroups = \App\Models\TransactionGroups::create($attributes);
        Flash::success('Transaction Groups  sucessfully added');
        return redirect('/admin/hotel/transaction/groups');

    }


    public function edit($id){

        $transactiongroups = \App\Models\TransactionGroups::find($id);

        $page_title = 'Edit Transaction Groups';
        $page_description = '';

        return view ('admin.hotel.transactiongroups.edit',compact('transactiongroups','page_title','page_description'));
    }


    public function update(Request $request , $id){

        $this->validate($request, array(
            'group_code'      => 'required',
           
        ));
      
        $attributes = $request->all();

        if(!isset($attributes['enabled']))
            $attributes['enabled'] = 0;

        \App\Models\TransactionGroups::find($id)->update($attributes);

        Flash::success('Transaction Groups sucessfully updated');
        return redirect('/admin/hotel/transaction/groups');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $trasnactiongroups = \App\Models\TransactionGroups::find($id);
       
        $modal_title = "Delete Transaction groups";
        $modal_body ="Are you sure that you want to delete transaction groups id ".$trasnactiongroups->id ." with the number ".$trasnactiongroups->group_code."? This operation is irreversible";

        $modal_route = route('admin.hotel.transaction.groups.delete',$trasnactiongroups->id);
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }


    public function destroy($id){
         $trasnactiongroups = \App\Models\TransactionGroups::find($id)->delete();
        Flash::success('Transaction Groups  sucessfully deleted');
        return redirect('/admin/hotel/transaction/groups');
    }

    

}

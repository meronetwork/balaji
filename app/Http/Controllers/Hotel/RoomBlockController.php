<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\RoomBlock;
use Flash;
class RoomBlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blocked = RoomBlock::all();
        return view('admin.hotel.room-block.index',compact('blocked'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $room = \App\Models\Room::all();
        return view('admin.hotel.room-block.create',compact('room'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $block = $request->all();
        $block['user_id'] = \Auth::user()->id;
        Flash::success('Room successfully blocked');
        RoomBlock::create($block);
        return redirect('/admin/hotel/room-block-index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Block|edit';
        $edit = RoomBlock::find($id);
         $room = \App\Models\Room::all();
       return view('admin.hotel.room-block.edit',compact('room','edit','page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);
        RoomBlock::find($id)->update($update);
        Flash::success('Room  blocked record successfully updated');
        return redirect('/admin/hotel/room-block-index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RoomBlock::find($id)->delete();
        Flash::success('Room  blocked record successfully deleted');
        return redirect('/admin/hotel/room-block-index');

    }
    public function confirmDelete($id){
        $error = null;
        $blocked = RoomBlock::find($id);
        $room = \App\Models\Room::where('room_id',$blocked->room_id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Unblocked";
        $modal_body ="Are you sure that you want to unblock room ".$room->room_number." with id ".$blocked->room_id ." blocked id ".$blocked->id."? This operation is irreversible";
        $modal_route = route('admin.hotel.room-block-delete',$blocked->id);

   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function blockselected($room_id){
        dd("DONE");
    }
}

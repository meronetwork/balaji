<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationRoom;
use App\Models\ReservationGuest;
use App\Models\ReservationDoc;
use App\Models\FolioDetail;
use Flash;
use Auth;
use DB;

class CashieringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guestsin()
    {

        $reservation = Reservation::orderBy('id', 'desc')->where('parent_res', null)
            ->whereIn('reservation_status', ['3', '4', '8'])
            ->where(function ($query) {
                if (\Request::get('check_in_date') && \Request::get('check_in_date') != '')
                    return $query->where('check_in', \Request::get('check_in_date'));
            })
            ->where(function ($query) {
                if (\Request::get('check_out_date') && \Request::get('check_out_date') != '')
                    return $query->where('check_out', \Request::get('check_in_date'));
            })
            ->where(function ($query) {
                if (\Request::get('user_id') && \Request::get('user_id') != '')
                    return $query->where('user_id', \Request::get('user_id'));
            })
            ->where(function ($query) {
                if (\Request::get('agent_id') && \Request::get('agent_id') != '')
                    return $query->where('agent_id', \Request::get('agent_id'));
            })
            ->where(function ($query) {
                if (\Request::get('guest_type') && \Request::get('guest_type') != '')
                    return $query->where('guest_type', \Request::get('guest_type'));
            })
            ->paginate('30');

        $agents = \App\Models\Lead::pluck('name', 'id')->all();
        $users = \App\User::pluck('username', 'id')->all();


        $page_title = 'Reservation Index';
        $guest_type = [
            'TOU' => ['Tourist', 'btn bg-green  margin'],
            'BUS' => ['Business Travellers', 'btn bg-red  margin'],
            'FAM' => ['Families', 'btn bg-navy  margin'],
            'STA' => ['Hotel Staffs', 'btn bg-red  margin'],
            'DEl' => ['Delegates', 'btn bg-olive  margin'],
            'VIP' => ['VIP', 'btn bg-maroon margin'],
            'COR' => ['Corporate', 'btn bg-purple margin'],
            'GOV' => ['Government', 'btn bg-blue margin'],
            'FIT' => ["FIT", 'btn bg-orange margin'],
            'COM' => ["Complementry", 'btn bg-green margin']
        ];

        return view('admin.hotel.cashiering.billing.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
    }

    public function billingdetail($id)
    {

        $folios = \App\Models\Folio::where('reservation_id', $id)->get();
        $orders = \App\Models\Orders::where('reservation_id', $id)->get();
        $folio_payments = \App\Models\FolioPayment::where('reservation_id', $id)->get();
        $orders_ids = \App\Models\Orders::where('reservation_id', $id)->pluck('id')->toArray();
        $payments = \App\Models\Payment::whereIn('sale_id', $orders_ids)->get();

        return view('admin.hotel.cashiering.billing.billingdetail', compact('folios', 'orders', 'folio_payments', 'payments'));
    }
}

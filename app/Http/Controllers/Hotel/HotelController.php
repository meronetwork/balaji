<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use App\Models\Attraction;
use App\Models\Hotel;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Admin | hotel';
        $hotel = Hotel::all();
        return view('admin.hotel.hotel.index', compact('hotel', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Admin | hotel';
        return view('admin.hotel.hotel.create', compact('attraction', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel = $request->all();
        Hotel::create($hotel);
        Flash::success('Hotel sucessfully created');
        return redirect('/admin/hotel/hotel-index/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page_title = 'Admin | hotel';
        $show =  Hotel::leftjoin('attractions', 'attractions.attraction_id', '=', 'hotel.attraction_id')->where('hotel_id', $id)->first();
        return view('admin.hotel.hotel.show', compact('show', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Admin | hotel';

        $edit = Hotel::where('hotel_id', $id)->first();
        return view('admin.hotel.hotel.edit', compact('attraction', 'edit', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);
        Hotel::where('hotel_id', $id)->update($update);
        Flash::success('Hotel has been sucessfully updated');
        return redirect('/admin/hotel/hotel-index/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Hotel::where('hotel_id', $id)->delete();
        Flash::success('Hotel has been sucessfully deleted');
        return redirect('/admin/hotel/hotel-index/');
    }
    public function getModalDelete($id)
    {
        $error = null;
        $hotel = Hotel::where('hotel_id', $id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete hotel";
        $modal_body = "Are you sure that you want to delete hotel id " . $hotel->hotel_id . " with the name " . $hotel->hotel_name . "? This operation is irreversible";
        $modal_route = route('admin.hotel.hotel-delete', $hotel->hotel_id);


        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationRoom;
use App\Models\ReservationGuest;
use App\Models\ReservationDoc;
use App\Models\Audit as Audit;
use Flash;
use Auth;
use App\Models\Room;
use App\Models\Housestatus;

class HouseKeepingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = "Admin | Housekeeping";
        $rooms = Room::select(
            'room.room_number',
            'room_types.room_name',
            'room.status_id',
            'room.house_status_id',
            'room.hk_remarks',
            'room.housekeeper_id',
            'room.room_id',
            'room_types.type_code'
        )
            ->join('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')
            ->orderBy('room_number', 'desc')
            ->paginate('50');

        $house_status = \App\Models\Housestatus::orderby('id')->pluck('status_name', 'id');

        //room_number,room_type,house_keeping_status,availablity(housekeeping),Remakrs,Housekeeper(user list)
        return view('admin.hotel.house-keeping.index', compact('rooms', 'house_status', 'page_title'));
    }
    public function create()
    {
        return view('admin.hotel.house-keeping.create');
    }
    public function edit($id)
    {
        $room = \App\Models\Room::where('room_id', $id)->first();
        $housestatus = \App\Models\Housestatus::all();
        $housekeeper = \App\User::all();
        return view('admin.hotel.house-keeping.edit', compact('room', 'housestatus', 'housekeeper'));
    }
    public function update(Request $request, $id)
    {
        $update = $request->all();
        unset($update['_token']);
        \App\Models\Room::where('room_id', $id)->update($update);
        Flash::success("successfully update");
        return redirect('/admin/hotel/house-keep-index/');
    }

    public function destroy($id)
    {

        \App\Models\RoomBlock::find($id)->delete();
        Flash::success('Room  blocked record successfully deleted');
        return redirect('/admin/hotel/house-keep-index/');
    }
    public function confirmDelete($id)
    {
        $error = null;
        $blocked = \App\Models\RoomBlock::find($id);
        $room = \App\Models\Room::where('room_id', $blocked->room_id)->first();
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Unblocked";
        $modal_body = "Are you sure that you want to unblock room " . $room->room_number . " with id " . $blocked->room_id . " blocked id " . $blocked->id . "? This operation is irreversible";
        $modal_route = route('admin.hotel.room-block-unblock', $blocked->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function blockroom($id)
    {
        $selected_room = \App\Models\Room::where('room_id', $id)->first();
        return view('admin.hotel.room-block.create', compact('selected_room'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ChangeHouseStatus(Request $request, $res_id)
    {

        $status_id = $request->house_status_id;

        $reservation = Reservation::find($res_id);

        //dd($reservation);

        $reservation_parent = Reservation::where('parent_res', $res_id)->get();

        $room = \App\Models\Room::where('room_number', $reservation->room_num)->update(['house_status_id' => $status_id]);

        //dd($room);

        if (count($reservation_parent) > 0) {
            foreach ($reservation_parent as $rp) {

                \App\Models\Room::where('room_number', $rp->room_num)->update(['house_status_id' => $status_id]);
            }
        }

        $ord = \App\Models\Folio::where('reservation_id', $reservation->id)->first();

        $orderDetails = \App\Models\FolioDetail::where('folio_id', $ord->id)->get();

        $mail_from =  env('APP_EMAIL');
        $mail_to   =  $reservation->email;
        $subject   =  "Reservation Check Out";

        if ($mail_to) {


            $pdf = \PDF::loadView('admin.hotel.folio.generateInvoicePDF', compact('reservation', 'ord', 'orderDetails'));

            $file = str_replace(' ', '_', trim($reservation->guest_name)) . '_CRM_' . $reservation->id . '.pdf';

            //dd($file);
            if (\File::exists('uploads/' . $file)) {
                \File::Delete('uploads/' . $file);
            }
            $pdf->save('uploads/' . $file);

            // $fields = [
            //     'file'  =>  $stamp . $filename,
            // ];
            $file = str_replace('/', '-', $file);
           try{


            //send email
            $mail = \Mail::send('emails.email-checkout', [], function ($message) use ($subject, $mail_from, $mail_to, $file, $Lead, $request, $fields, $reservation) {
                //$mail = \Mail::send('emails.email-send', [], function ($message) use ($attributes, $Lead, $request, $fields) {
                $message->subject($subject);
                $message->from($mail_from, env('APP_COMPANY'));
                $message->to($mail_to, '');
                $message->attach('uploads/' . $file);
                if ($request->file('attachment')) {
                    $message->attach('sent_attachments/' . $fields['file']);
                }
            });

        }catch(\Exception $e){


        }
        }



        Flash::success("Sucessfully checked out");

        return redirect('/admin/hotel/reservation-edit/' . $res_id);
    }
    public function ajaxRoomUpdate(Request $request)
    {
        $type = $request->type;
        if($type == 'status'){
            $attributes['house_status_id'] = $request->update_value;
            $room = Room::where('room_id',$request->id)->update($attributes);
            return ['status'=>1];
        }

    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Attraction;
use Flash;

class PosTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $costcenters = \App\Models\PosTable::orderBy('id', 'desc')->get();

        $page_title = 'Admin | Hotel | POS Tables';

        return view('admin.hotel.postables.index', compact('page_title', 'costcenters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page_title = 'Admin | Hotel | Pos Tables';

        $description = 'Create a new Tables';

        $outlets = \App\Models\PosOutlets::pluck('name', 'id')->all();

        $table_area = \App\Models\TableArea::select('name', 'id', 'floor_id')->get();

        return view('admin.hotel.postables.create', compact('page_title', 'description', 'outlets', 'table_area'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tables = $request->all();


        if ($request->file('image')) {
            $stamp = time();
            $file = $request->file('image');
            //dd($file);
            $destinationPath = public_path() . '/tables/';

            $filename = $file->getClientOriginalName();
            $request->file('image')->move($destinationPath, $stamp . '_' . $filename);

            $tables['image'] = $stamp . '_' . $filename;
        }


        \App\Models\PosTable::create($tables);
        Flash::success('POS Table added');

        return redirect('/admin/hotel/pos-tables/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

        $description = 'Edit an Pos Outlets ';

        $page_title = 'Admin | Hotel | Outlets';

        $edit =  \App\Models\PosTable::find($id);

        $outlets = \App\Models\PosOutlets::pluck('name', 'id')->all();

        $table_area = \App\Models\TableArea::select('name', 'id', 'floor_id')->get();

        return view('admin.hotel.postables.edit', compact('edit', 'description', 'page_title', 'outlets', 'table_area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $request->all();
        $pos_tables =  \App\Models\PosTable::find($id);

        if ($request->file('image')) {
            $stamp = time();
            $destinationPath = public_path() . '/tables/';

            $file = \Request::file('image');
            $filename = $file->getClientOriginalName();
            \Request::file('image')->move($destinationPath, $stamp . $filename);

            $image = \Image::make($destinationPath . $stamp . $filename)
                ->save($destinationPath . $stamp . $filename);

            $attributes['image'] = $stamp . $filename;
        }




        $pos_tables->update($attributes);
        Flash::success('POS Table Updated');

        return redirect('/admin/hotel/pos-tables/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kitchen = \App\Models\PosTable::find($id)->delete();
        Flash::success('Pos Table deleted');
        return redirect('/admin/hotel/pos-tables/index');
    }

    public function getModalDelete($id)
    {

        $error = null;
        $outlets =  \App\Models\PosTable::find($id);
        $modal_title = "Delete Pos Table";
        $modal_body = "Are you sure that you want to delete POS Table ID " . $outlets->id . ". This operation is irreversible";
        $modal_route = route('admin.hotel.pos-tables.delete', $outlets->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use App\Models\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationRoom;
use App\Models\ReservationGuest;
use App\Models\ReservationDoc;
use App\Models\FolioDetail;
use App\Models\Audit as Audit;
use Flash;
use Auth;
use DB;

class PaymentFolioController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */


  public function depositList(){


    $deposit = \App\Models\FolioPayment::where(function($query){

      if(\Request::get('start_date') &&  \Request::get('end_date')) {

        $start_date = \Request::get('start_date');
        $end_date = \Request::get('end_date');
        $query->whereBetween('created_at', [$start_date." 00:00:00", $end_date." 23:59:59"])->get();


      }


    })->where('is_deposit','1')
    ->orderBy('id','desc')
        ->paginate(30);



    $page_title = 'Deposit List';


    return view('admin.hotel.cashiering.depositlist',compact('deposit','page_title'));



  }

  public function index(Request $request, $id)
  {

    $folio = \App\Models\Folio::find($id);
    $reservation = \App\Models\Reservation::find($folio->reservation_id);

    //dd($reservation);

    $page_title = 'Settlement of ' . $reservation->guest->first_name . '' . $reservation->guest->last_name . '';
    $page_description = 'Room No ' . $reservation->room_num . ' Check In Date ' . $reservation->check_in . ' Check Out Date ' . $reservation->check_out . '';
    $paymentlist = \App\Models\FolioPayment::where('folio_id', $id)->get();

    return view('admin.hotel.paymentfolio.index', compact('folios', 'page_title', 'fiscalyears', 'id', 'page_description', 'paymentlist'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request, $id)
  {

    $folio = \App\Models\Folio::find($id);
    $reservation = \App\Models\Reservation::find($folio->reservation_id);

    $page_title = 'Settlement for ' . ($reservation->guest->first_name ?? null) . '' . ($reservation->guest->last_name ?? null) . '';
    $page_description = 'Room No ' . $reservation->room_num . '';

    $folios = \App\Models\Folio::where('id', $id)->get();
    $products = \App\Models\Product::orderBy('id', 'desc')->get();
    $payment_method = \App\Models\PaymentType::orderby('payment_type_id')->pluck('payment_name', 'payment_type_id');

    $folio_amount = \App\Models\Folio::where('id', $id)->sum('total_amount');
    $paid_amount =  \App\Models\FolioPayment::where('folio_id', $id)->sum('amount');


    if($folio->folio_type == 'master'){
      $deposit_amount = \App\Models\FolioPayment::where('reservation_id',$folio->reservation_id)
                        ->where('is_deposit',1)
                        ->sum('amount');
      $folio_amount -= $deposit_amount;
    }
    // $payment_remain = \TaskHelper::ReservationDueAmount($reservation->id);
    $payment_remain = $folio_amount;
    $housestatus = \App\Models\Housestatus::select('status_name','id')->get();

    if ($request->ajax()) {
      return view('admin.hotel.paymentfolio.modal.create', compact('products', 'page_title', 'page_description', 'reservation', 'folios', 'payment_method', 'payment_remain', 'id','housestatus'));
    }
    return view('admin.hotel.paymentfolio.create', compact('products', 'page_title', 'page_description', 'reservation', 'folios', 'payment_method', 'payment_remain', 'id','housestatus'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, $id)
  {
    DB::beginTransaction();
    $attributes = $request->all();

    $attributes['org_id'] = \Auth::user()->org_id;
    $attributes['user_id'] = \Auth::user()->id;
    $attributes['folio_id'] = $id;
    $folio = \App\Models\Folio::find($id);

    $foliopayment = \App\Models\FolioPayment::create($attributes);



    $foliodetails = \App\Models\FolioDetail::where('folio_id', $id)->where('posted_to_ledger', '0')->get();

    $sub_total_amount = 0;

    foreach ($foliodetails as $fd) {
      $sub_total_amount = $sub_total_amount + $fd->total;
    }

    if ($folio->service_charge) {

      $total_service_charge =  env('SERVICE_CHARGE',10) / 100 * $sub_total_amount;
      $total_amount_with_service_charge = $total_service_charge + $sub_total_amount;
      $total_taxable_amount = $total_amount_with_service_charge;
      $total_tax_amount = env('SALES_TAX',13) / 100 * $total_taxable_amount;
      $total_amount = $total_tax_amount + $total_taxable_amount;
    } else {

      $total_service_charge =  0;
      $total_amount_with_service_charge = $total_service_charge + $sub_total_amount;
      $total_taxable_amount = $total_amount_with_service_charge;
      $total_tax_amount = env('SALES_TAX',13) / 100 * $total_taxable_amount;
      $total_amount = $total_tax_amount + $total_taxable_amount;
    }

    $attributes_folio = $request->all();

    $current_fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;

    $type = \App\Models\Entrytype::find(13);

    $attributes_folio['number'] = \TaskHelper::generateId($type);
    $attributes_folio['entrytype_id'] = 13; //Receipt
    $attributes_folio['tag_id'] = 25; // Hotel Invoice Payment
    $attributes_folio['user_id'] = \Auth::user()->id;
    $attributes_folio['org_id'] = \Auth::user()->org_id;
    $attributes_folio['resv_id'] = $folio->reservation_id;
    $attributes_folio['source'] = 'AUTO_CHKOUT';
    $attributes_folio['date'] = \Carbon\Carbon::today();

    $attributes_folio['notes'] = $request->note;

    $attributes_folio['fiscal_year_id'] = $current_fiscal_year;

    if ($request->paid_by == 'complementry') {

      $attributes_folio['dr_total'] = 0;
      $attributes_folio['cr_total'] = 0;
    } else {

      $attributes_folio['dr_total'] = $total_amount;
      $attributes_folio['cr_total'] = $total_amount;
    }

    $entry = \App\Models\Entry::create($attributes_folio);


    if ($request->paid_by == 'cash') {

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id =  \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
      $cash_amount->amount = $total_amount;
      $cash_amount->narration = 'being sales made';
      $cash_amount->save();
    } elseif ($request->paid_by == 'check') {

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->cheque_no = $request->cheque_no;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();



      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id =  \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
      $cash_amount->amount = $total_amount;
      $cash_amount->narration = 'being sales made';
      $cash_amount->save();
    } elseif ($request->paid_by == 'travel-agent') {

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();


      $temp_ledger = \App\Models\COALedgers::where('name', $request['travel_agent_ledger'])->first();

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id = $temp_ledger->id;
      $cash_amount->amount = $total_amount;
      $cash_amount->narration = 'being sales made';
      $cash_amount->save();
    } elseif ($request->paid_by == 'city-ledger') {

      // Romm Sales ledger
      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // Credit account

      $temp_city_ledger = \App\Models\COALedgers::where('name', $request['city_ledger'])->first();

      $credit_amount = new \App\Models\Entryitem();
      $credit_amount->entry_id = $entry->id;
      $credit_amount->dc = 'D';
      $credit_amount->ledger_id = $temp_city_ledger->id;
      $credit_amount->amount = $total_amount;
      $credit_amount->narration = 'being sales made';
      $credit_amount->save();
    } elseif ($request->paid_by == 'complementry') {

      // Romm Sales ledger

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // Credit account
      $credit_amount = new \App\Models\Entryitem();
      $credit_amount->entry_id = $entry->id;
      $credit_amount->dc = 'D';
      $credit_amount->ledger_id =  \FinanceHelper::get_ledger_id('COMPLEMENTRY_LEDGER_ID');
      $credit_amount->amount = $total_amount;
      $credit_amount->narration = 'being sales made';
      $credit_amount->save();
    } elseif ($request->paid_by == 'staff') {

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made of ' . $request->staff_name . '';
      $sub_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id = 122;
      $cash_amount->amount = $total_amount;
      $cash_amount->narration = 'being sales made of ' . $request->staff_name . '';
      $cash_amount->save();
    } elseif ($request->paid_by == 'credit-cards') {

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // cash account
      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id =  \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
      $cash_amount->amount = $total_amount;
      $cash_amount->narration = 'being sales made';
      $cash_amount->save();
    } elseif ($request->paid_by == 'e-sewa') {

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
      $sub_amount->amount = $total_amount;
      $sub_amount->narration = 'being sales made E-sewa id ' . $request->e_sewa_id . '';
      $sub_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id =  \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
      $cash_amount->amount = $total_amount;
      $cash_amount->narration = 'being sales made E-sewa Id ' . $request->e_sewa_id . '';
      $cash_amount->save();
    }

    // \App\Models\Folio::find($id)->update(['settlement'=>1]);
    $foliodetails = \App\Models\FolioDetail::where('folio_id', $id)->update(['posted_to_ledger' => 1]);


      $foliopayment->update(['entry_id'=>$entry->id]);
      $folio->update(['settle_entry_id'=>$entry->id]);


    DB::commit();
    Flash::success('Folio Payment Created sucessfully ');

    // app('\App\Http\Controllers\Hotel\ReservationController')
    // ->checkoutfromedit($request->reservation_id);


    // app('\App\Http\Controllers\Hotel\HouseKeepingController')
    // ->ChangeHouseStatus($request,$request->reservation_id);

    // return redirect('/admin/hotel/reservation-edit/' . $request->reservation_id);

   // $orderid =   app('\App\Http\Controllers\OrdersController')
   //  ->ConvertFoliotoFinalInvoice($folio->id,true);


    return redirect("/admin/hotel/reservation/folio/{$folio->id}/generate/final/invoice?house_status_id=".request()->house_status_id."&checkout=".request()->checkout);


  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id, $payment_id)
  {

    $reservation = \App\Models\Reservation::find($id);

    $reservation_id = $id;

    $page_title = 'Settlement editing ' . $reservation->guest->first_name . '' . $reservation->guest->last_name . '';

    $page_description = 'Room No ' . $reservation->room_num . '';

    $foliopayment = \App\Models\FolioPayment::where('id', $payment_id)->first();

    $payment_method = \App\Models\PaymentType::orderby('payment_type_id')->pluck('payment_name', 'payment_type_id');

    return view('admin.hotel.paymentfolio.edit', compact('reservation_id', 'page_title', 'page_description', 'folio', 'folioDetails', 'foliopayment', 'payment_method'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id, $payment_id)
  {

    $payment = \App\Models\FolioPayment::find($payment_id);

    if ($payment->isEditable()) {
      $attributes = $request->all();

      $payment->update($attributes);

      Flash::success('Folio Payment updated Successfully.');

      return redirect('/admin/payment/folio/' . $id . '/index');
    }
  }

  /**
   * @param $id
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function destroy($id)
  {
    $foliopayment = \App\Models\FolioPayment::find($id);

    if (!$foliopayment->isdeletable()) {
      abort(403);
    }

    $foliopayment->delete($id);

    Flash::success('Folio Payment successfully deleted.');

    return redirect()->back();
  }

  /**
   * Delete Confirm
   *
   * @param   int   $id
   * @return  View
   */
  public function getModalDelete($id)
  {

    $foliopayment = \App\Models\FolioPayment::find($id);

    if (!$foliopayment->isdeletable()) {
      abort(403);
    }

    $modal_title = 'Delete Folio Payment';

    $foliopayment = \App\Models\FolioPayment::find($id);

    $modal_route = route('admin.hotel.payment.folio.delete', array('id' => $foliopayment->id));

    $modal_body = 'Are you sure you want to delete this folio payment?';

    return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
  }


  public function reservationList($id)
  {

    $paymentfoliolist = \App\Models\FolioPayment::orderBy('id', 'asc')->where('reservation_id', $id)->get();

    $page_title = 'List Of Reservation Settlement';

    $paymentposlist = \App\Models\Payment::orderBy('id', 'asc')->where('room_reservation_id', $id)->get();

    return view('admin.hotel.paymentfolio.paymentreservation', compact('paymentfoliolist', 'page_title', 'paymentposlist'));
  }



  public function depositCreate($id)
  {


    $page_title = 'Reservation Deposit';

    $page_description = 'Reservation Deposit';

    $dipositHistory = \App\Models\FolioPayment::where('reservation_id',$id)->where('is_deposit','1')->get();


    return view('admin.hotel.paymentfolio.depositcreate', compact('id', 'page_title', 'page_description','dipositHistory'));
  }


  public function printReceipt($id){



  $payment =   \App\Models\FolioPayment::find($id);
  $reservation = \App\Models\Reservation::find($payment->reservation_id);

  return view('admin.hotel.paymentfolio.receipt',compact('payment','reservation'));




  }


  public function depositStore(Request $request, $id)
  {

    DB::beginTransaction();

    $fromFrontCash = false;
    if($id == 'from-front'){
      $fromFrontCash = true;

      $request->request->add(['reservation_id'=>$request->room_num]);
      $id = $request->room_num;
    }
    $attributes = $request->all();

    $attributes['is_deposit'] = 1;

    $reservation = \App\Models\Reservation::find($request->reservation_id);

    // dd($attributes);

    $attributes['amount'] = $request->amount;
    $attributes['user_id'] = \Auth::user()->id;
      $type = \App\Models\Entrytype::find(13);

      $attributes_deposit['number'] = \TaskHelper::generateId($type);
    $attributes_deposit['entrytype_id'] = 13;
    $attributes_deposit['tag_id'] = 8; //REVENUE
    $attributes_deposit['user_id'] = \Auth::user()->id;
    $attributes_deposit['org_id'] = \Auth::user()->org_id;
    $attributes_deposit['resv_id'] = $request->reservation_id;
    $attributes_deposit['source'] = 'AUTO_DEPOSIT';
    $attributes_deposit['date'] = \Carbon\Carbon::today();

    $attributes_deposit['dr_total'] = $request->amount;
    $attributes_deposit['cr_total'] = $request->amount;

    $entry = \App\Models\Entry::create($attributes_deposit);

    if ($request->paid_by == 'cash') {

      $discount_amount = new \App\Models\Entryitem();
      $discount_amount->entry_id = $entry->id;
      $discount_amount->dc = 'C';
      $discount_amount->ledger_id = $reservation->ledger_id;
      $discount_amount->amount = $request->amount;
      $discount_amount->narration = 'Deposit Amount of ' . env('RES_CODE') . '' . $request->reservation_id . '';
      $discount_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
      $cash_amount->amount = $request->amount;
      $cash_amount->narration = 'Deposit Amount  By Cash ';
      $cash_amount->save();
    }elseif ($request->paid_by == 'credit-cards') {

        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->dc = 'C';
        $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
        $cash_amount->amount = $request->amount;
        $cash_amount->narration = 'Deposit amount by credit';
        $cash_amount->save();


        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->dc = 'D';
        $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CREDIT_CARD');
        $cash_amount->amount = $request->amount;
        $cash_amount->narration = 'Deposit Amount  By Credit';
        $cash_amount->save();
      }
      else {


      $discount_amount = new \App\Models\Entryitem();
      $discount_amount->entry_id = $entry->id;
      $discount_amount->dc = 'C';
      $discount_amount->ledger_id = $reservation->ledger_id;
      $discount_amount->amount = $request->amount;
      $discount_amount->narration = 'Deposit Amount of ' . env('RES_CODE') . '' . $request->reservation_id . '';
      $discount_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id =\FinanceHelper::get_ledger_id('CHEQUE_AMOUNT_LEDGER_ID');
      $cash_amount->amount = $request->amount;
      $cash_amount->narration = 'Deposit Amount  By Cheque Bank Name:' . $request->bank_name . ' Cheque No: ' . $request->cheque_no . ' Cheque Date: ' . $request->cheque_date . '';
      $cash_amount->save();
    }


      $attributes['entry_id']=$entry->id;
    \App\Models\FolioPayment::create($attributes);
    DB::commit();
    if($fromFrontCash){


      return redirect()->back();
    }
    return redirect('/admin/hotel/reservation-edit/' . $request->reservation_id );
  }

    public function allFolioPayment()
    {

        $payment_list = \App\Models\FolioPayment::select('folio_payments.*',
            'fin_orders_meta.is_bill_active')
            ->where(function($query){

                if(\Request::get('start_date') && \Request::get('end_date')){

                    $start_date = \Request::get('start_date');

                    $end_date = \Request::get('end_date');
                }else{

                    $start_date = date('Y-m-d');

                    $end_date = date('Y-m-d');
                }

                return $query->whereDate('folio_payments.date','>=',$start_date)
                    ->whereDate('folio_payments.date','<=',$end_date);

            })
            ->where(function($query){
                $outlet_id = \Request::get('outlet_id');
                if($outlet_id){
                    return $query->where('fin_orders.outlet_id',$outlet_id);
                }
            })->where(function($query){
                $paid_by = \Request::get('paid_by');
                if($paid_by){
                    return $query->where('folio_payments.paid_by',$paid_by);
                }
            })->where(function($query){

                $bill_no = \Request::get('bill_no');
                if($bill_no){

                    return $query->where('fin_orders.bill_no',$bill_no);

                }

            })->where(function($query){



                $customer_id= \Request::get('client_id');

                if($customer_id){

                    return $query->where('fin_orders.pos_customer_id',$customer_id);
                }

            })
            ->leftjoin('fin_orders','fin_orders.reservation_id','=','folio_payments.reservation_id')
            ->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
            ->orderby('folio_payments.id', 'desc')
            ->groupBy('folio_payments.id')
            ->paginate(50);
        $pay_method = [
            'cash' => 'Cash',
            'check' => 'Check',
            'travel-agent' => 'Travel Agent',
            'city-ledger' => 'City Ledger',
            'complementry' => 'Complementry',
            'staff' => 'Staff',
            'credit-cards' => 'Credit  Cards',
            'room' => 'Room',
            'e-sewa' => 'e-sewa'
        ];

        $page_title = 'All Receipts';

        $page_description  = 'Hotel Receipts List';
        $outlets = \App\Models\PosOutlets::pluck('name','id');
        $pay_method = $this->pay_method;
        $clients = Client::pluck('name','id');

        return view('admin.hotel.paymentfolio.allfoliopayments', compact('payment_list', 'page_title', 'page_description','outlets','pay_method','clients'));
    }

}

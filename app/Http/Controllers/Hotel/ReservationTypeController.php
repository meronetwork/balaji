<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
class ReservationTypeController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Admin | reservation';
     $type1 = \App\Models\ReservationType::all();
      return view('admin.hotel.reservation-type.index',compact('type1','page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $description="Create a reservation type";
        $page_title = 'Admin | reservation ';
        return view('admin.hotel.reservation-type.create',compact('page_title','description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = $request->all();
        \App\Models\ReservationType::create($type);
        Flash::success("Reservation type sucessfully created");
        return redirect('/admin/hotel/res-type-index/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { $description="Create a reservation type";
         $page_title = 'Admin | reservation ';
        $edit = \App\Models\ReservationType::find($id);
        return view('admin.hotel.reservation-type.create',compact('edit','page_title','description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = $request->all();

        \App\Models\ReservationType::find($id)->update($update);
        Flash::success("Reservation type sucessfully updated");
        return redirect('/admin/hotel/res-type-index/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Models\ReservationType::find($id)->delete();
         Flash::success("Reservation type sucessfully deleted");
        return redirect('/admin/hotel/res-type-index/');

    }

     public function getModalDelete($id)
    {
        $error = null;
        $res_type =  \App\Models\ReservationType::find($id);
        // if (!$attraction->isdeletable())
        // {
        //     abort(403);
        // }
        $modal_title = "Delete reservation type";
        $modal_body ="Are you sure that you want to delete reservation type id ".$res_type->id ." with the name ".$res_type->type_name."? This operation is irreversible";
        $modal_route = route('admin.hotel.res-type-delete',$res_type->id);
   
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }
}

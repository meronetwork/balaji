<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;


class GroupReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function GroupVoidFolioConfirm($id)
    {

        $error = null;
        //$res = \App\Models\Folio::find($id);

        $modal_title = "Void Group Folio Of This Reservation";
        $modal_route = route('admin.folio.makevoid.groups', $id);

        return view('modal_void_reason', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function GroupVoidFolio(Request $request, $id)
    {


        $reason = $request->reason;

        \App\Models\Folio::where('reservation_id', $id)->update(['is_void' => 1, 'void_reason' => $reason, 'is_bill_active' => 0]);


        Flash::success('Folio sucessfully marked as void');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TableBooking;
use Flash;


class TableBookingController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $page_title = 'Admin | Table | Bookings';

        $page_description = 'List of Table Bookings';

        $tablebookings = TableBooking::orderBy('id', 'desc')->get();

        return view('admin.hotel.tablebooking.index', compact('tablebookings', 'page_title', 'page_description'));
    }


    public function create()
    {
        $page_title = 'Create | Table | Bookings';
        $page_description = '';

        return view('admin.hotel.tablebooking.create', compact('page_title', 'page_description'));
    }

    public function show($id)
    {
        $tablebookings = TableBooking::find($id);

        $page_title = 'Show Table Bookings';
        $page_description = '';

        return view('admin.hotel.tablebooking.show', compact('page_title', 'page_description', 'tablebookings'));
    }


    public function store(Request $request)
    {

        $this->validate($request, array(
            'first_name'      => 'required',
        ));

        $attributes = $request->all();
        $attributes['user_id'] = \Auth::user()->id;
        $attributes['org_id'] = \Auth::user()->org_id;

        //  dd($attributes);

        $tablebookings = TableBooking::create($attributes);
        Flash::success('Table sucessfully added');
        return redirect('/admin/hotel/table/bookings');
    }


    public function edit($id)
    {
        $tablebookings = TableBooking::find($id);

        $page_title = 'Edit Table Bookings';
        $page_description = '';

        return view('admin.hotel.tablebooking.edit', compact('page_title', 'page_description', 'tablebookings'));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, array(
            'first_name'      => 'required',
        ));

        $attributes = $request->all();
        $attributes['user_id'] = \Auth::user()->id;
        $attributes['org_id'] = \Auth::user()->org_id;

        TableBooking::find($id)->update($attributes);

        Flash::success('Table Booking sucessfully updated');
        return redirect('/admin/hotel/table/bookings');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $tablebookings = \App\Models\TableBooking::find($id);

        $modal_title = "Delete Table Booking";
        $modal_body = "Are you sure that you want to delete table Booking id " . $tablebookings->id . " with the First name: " . $tablebookings->first_name . "? This operation is irreversible";

        $modal_route = route('admin.hotel.table.bookings.delete', $tablebookings->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }


    public function destroy($id)
    {
        $tablearea = \App\Models\TableBooking::find($id)->delete();
        Flash::success('Table Bookings sucessfully deleted');
        return redirect('/admin/hotel/table/bookings');
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Attraction;
use Flash;
use Session;

class POSMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $posmenu = \App\Models\PosMenu::orderBy('id', 'desc')->get();

        //dd($posmenu);
        $page_title = 'Admin | Hotel | POS Menu';

        return view('admin.hotel.posmenu.index', compact('page_title', 'posmenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page_title = 'Admin | Hotel | Pos Outlets';

        $description = 'Create a new Outlets';

        $outlets = \App\Models\PosOutlets::pluck('name', 'id')->all();

        return view('admin.hotel.posmenu.create', compact('page_title', 'description', 'outlets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $menus = $request->all();
        //dd($menus);
        \App\Models\PosMenu::create($menus);
        Flash::success('POS Menu added');

        return redirect('/admin/hotel/pos-menu/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $description = 'Edit an Pos Menu';
        $page_title = 'Admin | Hotel | Menu';

        $edit =  \App\Models\PosMenu::find($id);

        $outlets = \App\Models\PosOutlets::pluck('name', 'id')->all();

        return view('admin.hotel.posmenu.edit', compact('edit', 'description', 'page_title', 'outlets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $attributes = $request->all();

        // dd($attributes);

        $pos_menu =  \App\Models\PosMenu::find($id);
        $pos_menu->update($attributes);

        Flash::success('POS Menu updated');

        return redirect('/admin/hotel/pos-menu/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $menus = \App\Models\PosMenu::find($id)->delete();

        Flash::success('Pos Menu deleted');

        return redirect('/admin/hotel/pos-menu/index');
    }

    public function getModalDelete($id)
    {

        $error = null;

        $menus =  \App\Models\PosMenu::find($id);

        // dd($menus);

        $modal_title = "Delete Pos Menu";

        $modal_body = "Are you sure that you want to delete POS Menu ID " . $menus->id . ". This operation is irreversible";

        $modal_route = route('admin.hotel.pos-menu.delete', $menus->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;


class TableAreaController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $page_title = 'Admin | Table | Area';

        $page_description = 'List of table Area';

        $tablearea = \App\Models\TableArea::orderBy('id', 'desc')->get();

        return view('admin.hotel.tablearea.index', compact('tablearea', 'page_title', 'page_description'));
    }


    public function create()
    {

        $page_title = 'Create | Table | Area';
        $page_description = '';

        $posfloors = \App\Models\PosFloor::select('name', 'id', 'outlet_id')->get();

        return view('admin.hotel.tablearea.create', compact('page_title', 'page_description', 'posfloors'));
    }


    public function store(Request $request)
    {

        $this->validate($request, array(
            'name'      => 'required',
        ));

        $attributes = $request->all();

        $tablearea = \App\Models\TableArea::create($attributes);
        Flash::success('Table Area   sucessfully added');
        return redirect('/admin/hotel/tablearea');
    }


    public function edit($id)
    {

        $tablearea = \App\Models\TableArea::find($id);
        $posfloors = \App\Models\PosFloor::select('name', 'id', 'outlet_id')->get();

        $page_title = 'Edit Table area';
        $page_description = '';

        return view('admin.hotel.tablearea.edit', compact('posfloors', 'page_title', 'page_description', 'tablearea'));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, array(
            'name'      => 'required',
        ));

        $attributes = $request->all();

        \App\Models\TableArea::find($id)->update($attributes);

        Flash::success('Table Area sucessfully updated');
        return redirect('/admin/hotel/tablearea');
    }


    public function getModalDelete($id)
    {
        $error = null;
        $tablearea = \App\Models\TableArea::find($id);

        $modal_title = "Delete Table Area";
        $modal_body = "Are you sure that you want to delete table Area id " . $tablearea->id . " with the name: " . $tablearea->name . "? This operation is irreversible";

        $modal_route = route('admin.hotel.tablearea.delete', $tablearea->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }


    public function destroy($id)
    {

        $tablearea = \App\Models\TableArea::find($id)->delete();
        Flash::success('Table Area sucessfully deleted');
        return redirect('/admin/hotel/tablearea');
    }
}

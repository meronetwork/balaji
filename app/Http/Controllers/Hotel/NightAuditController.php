<?php

namespace App\Http\Controllers\Hotel;

use App\Models\AbbrOrders;
use App\Models\BusinessDate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\NightAuditLog;
use App\Models\Room;
use App\Models\Folio;
use App\Models\Orders;
use Flash;
use DB;
use Auth;

use App\Models\Audit as Audit;


class NightAuditController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function pendingres()
  {
    $exclude = NightAuditLog::where('operation', 'pending')->pluck('res_id')->toArray();

    //dd($exclude);
    $pending = Reservation::where('reservation_status', '<>', '3')
        ->where('reservation_status', '<>', '6')
        ->where('reservation_status', '<>', '9')
        ->where('book_in','<=',date('Y-m-d'))
        ->whereNotIn('id', $exclude)->get();

    //dd($pending);


    $res_status = \App\Models\ReservationStatus::all();
    return view('admin.hotel.night-audit.pending-res', compact('pending', 'res_status'));
  }

  public function pendingresupdate(Request $request)
  {

    $res_id = $request->id;
    $status_id = $request->status_id;
    if ($status_id == '9') {
      $makevoid = ['check_in' => null, 'check_out' => null, 'room_num' => "", 'rate_id' => "", 'reservation_status' => '9'];
      Reservation::find($res_id)->update($makevoid);
    } else
      Reservation::find($res_id)->update(['reservation_status' => $status_id]);
    $auditlog = ['user_id' => \Auth::user()->id, 'res_id' => $res_id, 'operation' => 'pending'];
    NightAuditLog::create($auditlog);
    return ['status' => 1];
  }

  public function releaseres()
  {

    $exclude = NightAuditLog::where('operation', 'release')->pluck('res_id')->toArray();
    $release = Reservation::where('reservation_status', '<>', '6')->where('book_out', date('Y-m-d'))->whereNotIn('id', $exclude)->get();
    $res_status = \App\Models\ReservationStatus::all();
    return view('admin.hotel.night-audit.release-res', compact('release', 'res_status'));
  }

  public function releaseresupdate(Request $request)
  {

    $res_id = $request->id;
    $status_id = $request->status_id;
    Reservation::find($res_id)->update(['reservation_status' => $status_id]);
    $auditlog = ['user_id' => \Auth::user()->id, 'res_id' => $res_id, 'operation' => 'release'];
    NightAuditLog::create($auditlog);
    return ['status' => 1];
  }

  public function roomStatus()
  {

    $page_title = "House Keeping Status";
    $page_description = "lists of House kepping Status";

    $rooms = Room::select(
      'room.room_number',
      'room_types.room_name',
      'room.status_id',
      'room.house_status_id',
      'room.hk_remarks',
      'room.housekeeper_id',
      'room.room_id',
      'room_types.type_code'
    )
      ->join('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')
      ->where('room.house_status_id', '1')
      ->orderBy('room_number', 'desc')
      ->paginate('50');

    return view('admin.hotel.night-audit.roomstatus', compact('rooms', 'page_title', 'page_description'));
  }

  public function unsettledFolios()
  {

    $folio_details = \App\Models\FolioDetail::where('posted_to_ledger', 0)->distinct('folio_id')->pluck('folio_id');
    $folios = \App\Models\Folio::select('folio.*')->whereIn('folio.id', $folio_details)
          ->leftjoin('reservations','reservations.id','=','folio.reservation_id')
          ->where('folio.is_bill_active', 1)
          ->whereIn('reservations.reservation_status',['3','6'])
          ->get();
    $page_title = 'Payment and Settlements';
    $page_description = 'Unsettled Folios';

    return view('admin.hotel.night-audit.unsettledfolios', compact('folios', 'page_title', 'page_description'));
  }

  public function unsettledPOSbills()
  {

    $posbills = Orders::orderBy('id', 'desc')->where('settlement', '0')->where('is_bill_active', 1)->get();

    $page_title = 'Night Audit';
    $page_description = 'Unsettled POS Bills';

    return view('admin.hotel.night-audit.unsettledposbills', compact('posbills', 'page_title', 'page_description'));
  }

  public function settleFolio($id)
  {

    $page_title = 'Settle Folio';
    $page_description = 'Displaying Folio To Settle';
    $folio = \App\Models\Folio::find($id);
    $tags = \App\Models\Tag::pluck('title', 'id')->all();


    return view('admin.hotel.night-audit.settlefolio', compact('page_title', 'page_description', 'id', 'folio', 'tags'));
  }

  public function settlePOS($id)
  {

    $page_title = 'Settle POS';
    $page_description = 'Displaying POS To Settle';

    $order = \App\Models\Orders::find($id);

    $tags = \App\Models\Tag::pluck('title', 'id')->all();

    return view('admin.hotel.night-audit.settlepos', compact('page_title', 'page_description', 'tags', 'order'));
  }

  public function settlePostFolio(Request $request, $id)
  {


    $this->validate($request, array(

      'settle_type' => 'required'

    ));

    $folio = \App\Models\Folio::find($id);

    $attributes = $request->all();

    //dd($attributes);

    $attributes['entrytype_id'] = 4;
    $attributes['tag_id'] = $request->tag_id;
    $attributes['user_id'] = \Auth::user()->id;
    $attributes['org_id'] = \Auth::user()->org_id;
    $attributes['number'] = $id;
    $attributes['resv_id'] = $folio->reservation_id;
    $attributes['date'] = \Carbon\Carbon::today();
    $attributes['dr_total'] = $folio->total_amount;
    $attributes['cr_total'] = $folio->total_amount;

    $entry = \App\Models\Entry::create($attributes);


    if ($attributes->settle_type == 'Cash') {

      /// Romm Sales ledger

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id = 39;
      $sub_amount->amount = $folio->subtotal;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // Service Charge

      if ($folio->service_charge) {

        $service_charge = new \App\Models\Entryitem();
        $service_charge->entry_id = $entry->id;
        $service_charge->dc = 'C';
        $service_charge->ledger_id = 116;
        $service_charge->amount = $folio->service_charge;
        $service_charge->narration = 'service charge';
        $service_charge->save();
      }

      // Vat Amount

      $vat_amount = new \App\Models\Entryitem();
      $vat_amount->entry_id = $entry->id;
      $vat_amount->dc = 'C';
      $vat_amount->ledger_id = 31;
      $vat_amount->amount = $folio->tax_amount;
      $vat_amount->narration = 'being payble vat booked';
      $vat_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id = 5;
      $cash_amount->amount = $folio->total_amount;
      $cash_amount->narration = 'being sales made';
      $cash_amount->save();
    } else {

      // Romm Sales ledger

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id = 39;
      $sub_amount->amount = $folio->subtotal;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // Service Charge

      if ($folio->service_charge) {

        $service_charge = new \App\Models\Entryitem();
        $service_charge->entry_id = $entry->id;
        $service_charge->dc = 'C';
        $service_charge->ledger_id = 116;
        $service_charge->amount = $folio->service_charge;
        $service_charge->narration = 'service charge';
        $service_charge->save();
      }
      // Vat Amount

      $vat_amount = new \App\Models\Entryitem();
      $vat_amount->entry_id = $entry->id;
      $vat_amount->dc = 'C';
      $vat_amount->ledger_id = 31;
      $vat_amount->amount = $folio->tax_amount;
      $vat_amount->narration = 'being payble vat booked';
      $vat_amount->save();

      // Credit account

      $credit_amount = new \App\Models\Entryitem();
      $credit_amount->entry_id = $entry->id;
      $credit_amount->dc = 'D';
      $credit_amount->ledger_id = $request->party_ledger_id;
      $credit_amount->amount = $folio->total_amount;
      $credit_amount->narration = 'being sales made';
      $credit_amount->save();
    }


    \App\Models\Folio::find($id)->update(['settlement' => 1]);

    Flash::success("Folio Successfully setteled");

    return redirect('/admin/hotel/nightaudit-unsettledfolios');
  }

  public function settlePostPOS(Request $request, $id)
  {

    $this->validate($request, array(

      'settle_type' => 'required'

    ));

    $order = \App\Models\Orders::find($id);
    $attributes = $request->all();


    // dd($attributes);

    $outlet_ledgers = $order->outlet->ledger_id;

    // dd($attributes);

    $attributes['entrytype_id'] = 4;
    $attributes['tag_id'] = $request->tag_id;
    $attributes['user_id'] = \Auth::user()->id;
    $attributes['org_id'] = \Auth::user()->org_id;
    $attributes['number'] = $id;
    $attributes['resv_id'] = $order->reservation_id;
    $attributes['date'] = \Carbon\Carbon::today();
    $attributes['dr_total'] = $order->total_amount;
    $attributes['cr_total'] = $order->total_amount;

    dd($attributes);

    $entry = \App\Models\Entry::create($attributes);


    if ($attributes->settle_type == 'Cash') {


      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id = $outlet_ledgers;
      $sub_amount->amount = $order->subtotal;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // Service Charge

      if ($order->service_charge) {

        $service_charge = new \App\Models\Entryitem();
        $service_charge->entry_id = $entry->id;
        $service_charge->dc = 'C';
        $service_charge->ledger_id = 116;
        $service_charge->amount = $order->service_charge;
        $service_charge->narration = 'service charge';
        $service_charge->save();
      }

      // Vat Amount

      $vat_amount = new \App\Models\Entryitem();
      $vat_amount->entry_id = $entry->id;
      $vat_amount->dc = 'C';
      $vat_amount->ledger_id = 31;
      $vat_amount->amount = $order->tax_amount;
      $vat_amount->narration = 'being payble vat booked';
      $vat_amount->save();

      // cash account

      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id = 5;
      $cash_amount->amount = $order->total_amount;
      $cash_amount->narration = 'being sales made';
      $cash_amount->save();
    } else {

      // Romm Sales ledger

      $sub_amount = new \App\Models\Entryitem();
      $sub_amount->entry_id = $entry->id;
      $sub_amount->dc = 'C';
      $sub_amount->ledger_id = $outlet_ledgers;
      $sub_amount->amount = $order->subtotal;
      $sub_amount->narration = 'being sales made';
      $sub_amount->save();

      // Service Charge

      if ($order->service_charge) {

        $service_charge = new \App\Models\Entryitem();
        $service_charge->entry_id = $entry->id;
        $service_charge->dc = 'C';
        $service_charge->ledger_id = 116;
        $service_charge->amount = $order->service_charge;
        $service_charge->narration = 'service charge';
        $service_charge->save();
      }

      // Vat Amount

      $vat_amount = new \App\Models\Entryitem();
      $vat_amount->entry_id = $entry->id;
      $vat_amount->dc = 'C';
      $vat_amount->ledger_id = 31;
      $vat_amount->amount = $order->tax_amount;
      $vat_amount->narration = 'being payble vat booked';
      $vat_amount->save();

      // Credit account

      $credit_amount = new \App\Models\Entryitem();
      $credit_amount->entry_id = $entry->id;
      $credit_amount->dc = 'D';
      $credit_amount->ledger_id = $request->party_ledger_id;
      $credit_amount->amount = $order->total_amount;
      $credit_amount->narration = 'being sales made';
      $credit_amount->save();
    }


    \App\Models\Orders::find($id)->update(['settlement' => 1]);

    Flash::success("POS Successfully setteled");

    return redirect('/admin/hotel/nightaudit-unsettledposbills');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


  public function updateFolio($reservation){

    $total_subtotal = 0;
    $total_service_charge = 0;
    $total_amount_with_service = 0;
    $total_taxable_amount = 0;
    $total_tax_amount = 0;
    $total_total_amount = 0;

    $folio = \App\Models\Folio::orderBy('id', asc)->where('reservation_id', $reservation->id)->first();


    $room_num = $reservation->room_num;
    $total_days = 1;

    $room_detail = \App\Models\Room::where('room_number', $room_num)->first();
    $room_type = $room_detail->room_type->room_name;
    $room_price = $reservation->room_rate;
    $breakfast_price = $reservation->rateplan->breakfast_rate;
    $lunch_price = $reservation->rateplan->lunch_rate;
    $dinner_price = $reservation->rateplan->dinner_rate;

    // $adjustmentprice = ($reservation->room_rate) - ($room_price + $breakfast_price + $lunch_price + $dinner_price);
    $adjustmentprice = 0;
    $total_room_price = $total_days * $room_price;
    $total_breakfast_price = $total_days * $breakfast_price;
    $total_lunch_price = $total_days * $lunch_price;
    $total_dinner_price = $total_days * $dinner_price;



    $all_total_amount = $total_room_price + $total_breakfast_price + $total_lunch_price + $total_dinner_price + $adjustmentprice;
    $service_charge = $all_total_amount * env('SERVICE_CHARGE') / 100;
    $service_total_amount =  $all_total_amount * env('SERVICE_CHARGE') / 100 + $all_total_amount;
    $tax_amount = $service_total_amount * 13 / 100;
    $room_description = '' . $room_num . ' (' . $room_type . ')';

    // dd($room_description);
    if ($reservation->guest_type != 'COM') {

      $total_subtotal = $folio->subtotal + $all_total_amount;
      $total_service_charge = $folio->service_charge + $service_charge;
      $total_amount_with_service = $folio->amount_with_service + $service_total_amount;
      $total_taxable_amount = $folio->taxable_amount + $service_total_amount;
      $total_tax_amount = $folio->tax_amount + $tax_amount;
      $total_total_amount = $folio->total_amount + $service_total_amount + $tax_amount;


      $attributes = [];
      $attributes['subtotal'] = $total_subtotal;
      $attributes['service_charge'] = $total_service_charge;
      $attributes['amount_with_service'] = $total_amount_with_service;
      $attributes['discount_percent'] = 0;
      $attributes['taxable_amount'] = $total_taxable_amount;
      $attributes['tax_amount'] = $total_tax_amount;
      $attributes['total_amount'] = $total_total_amount;

      \App\Models\Folio::find($folio->id)->update($attributes);

      $detail_attributes = new \App\Models\FolioDetail();
      $detail_attributes->folio_id = $folio->id;
      $detail_attributes->description = $room_description;
      $detail_attributes->price = $room_price;
      $detail_attributes->quantity = $total_days;
      $detail_attributes->total = $total_room_price;
      $detail_attributes->date = \Carbon\Carbon::now();
      $detail_attributes->is_inventory = 0;
      $detail_attributes->flag = 'accommodation';
      $detail_attributes->res_id = $reservation->id;
      $detail_attributes->save();

      if ($breakfast_price != 0) {

        $detail_attributes_breakfast = new \App\Models\FolioDetail();
        $detail_attributes_breakfast->folio_id = $folio->id;
        $detail_attributes_breakfast->description = 'Break Fast';
        $detail_attributes_breakfast->price = $breakfast_price;
        $detail_attributes_breakfast->quantity = $total_days;
        $detail_attributes_breakfast->total = $total_breakfast_price;
        $detail_attributes_breakfast->date = \Carbon\Carbon::now();
        $detail_attributes_breakfast->is_inventory = 0;
        $detail_attributes->flag = 'restaurant';
        $detail_attributes->res_id = $reservation->id;
        $detail_attributes_breakfast->save();
      }

      if ($lunch_price != 0) {

        $detail_attributes_lunch = new \App\Models\FolioDetail();
        $detail_attributes_lunch->folio_id = $folio->id;
        $detail_attributes_lunch->description = 'Lunch';
        $detail_attributes_lunch->price = $lunch_price;
        $detail_attributes_lunch->quantity = $total_days;
        $detail_attributes_lunch->total = $total_lunch_price;
        $detail_attributes_lunch->date = \Carbon\Carbon::now();
        $detail_attributes_lunch->is_inventory = 0;
        $detail_attributes->flag = 'restaurant';
        $detail_attributes->res_id = $reservation->id;
        $detail_attributes_lunch->save();
      }

      if ($dinner_price != 0) {

        $detail_attributes_dinner = new \App\Models\FolioDetail();
        $detail_attributes_dinner->folio_id = $folio->id;
        $detail_attributes_dinner->description = 'Dinner';
        $detail_attributes_dinner->price = $dinner_price;
        $detail_attributes_dinner->quantity = $total_days;
        $detail_attributes_dinner->total = $total_dinner_price;
        $detail_attributes_dinner->date = \Carbon\Carbon::now();
        $detail_attributes_dinner->is_inventory = 0;
        $detail_attributes->flag = 'restaurant';
        $detail_attributes->res_id = $reservation->id;
        $detail_attributes_dinner->save();
      }
      if ($adjustmentprice != 0) {

        $detail_attributes = new \App\Models\FolioDetail();
        $detail_attributes->folio_id = $folio->id;
        $detail_attributes->description = 'Discount on Reservation';
        $detail_attributes->price = $adjustmentprice;
        $detail_attributes->quantity = $total_days;
        $detail_attributes->total = $adjustmentprice;
        $detail_attributes->date = \Carbon\Carbon::now();
        $detail_attributes->is_inventory = 0;
        $detail_attributes->flag = 'discount';
        $detail_attributes->res_id = $reservation->id;
        $detail_attributes->save();
      }
    }

    $child_res = \App\Models\Reservation::where('parent_res', $reservation->id)->get();
    if (count($child_res) > 0) {
      foreach ($child_res as $cr) {

        $room_num = $cr->room_num;

        $total_days = 1;
        $room_detail = \App\Models\Room::where('room_number', $room_num)->first();
        $room_type = $room_detail->room_type->room_name;
        $room_price = $cr->room_rate;
        $breakfast_price = $cr->rateplan->breakfast_rate;
        $lunch_price = $cr->rateplan->lunch_rate;
        $dinner_price = $cr->rateplan->dinner_rate;
        // $adjustmentprice = ($cr->room_rate) - ($room_price + $breakfast_price + $lunch_price + $dinner_price);
        $adjustmentprice = 0;

        $total_room_price = $total_days * $room_price;
        $total_breakfast_price = $total_days * $breakfast_price;
        $total_lunch_price = $total_days * $lunch_price;
        $total_dinner_price = $total_days * $dinner_price;

        $all_total_amount = $total_room_price + $total_breakfast_price + $total_lunch_price + $total_dinner_price + $adjustmentprice;
        $service_charge = $all_total_amount * env('SERVICE_CHARGE') / 100;
        $service_total_amount =  $all_total_amount * env('SERVICE_CHARGE') / 100 + $all_total_amount;
        $tax_amount = $service_total_amount * 13 / 100;
        $room_description = '' . $room_num . ' (' . $room_type . ')';

        if ($cr->guest_type != 'COM') {

          $total_subtotal = $total_subtotal + $all_total_amount;
          $total_service_charge = $total_service_charge + $service_charge;
          $total_amount_with_service = $total_amount_with_service + $service_total_amount;
          $total_taxable_amount = $total_taxable_amount + $service_total_amount;
          $total_tax_amount = $total_tax_amount + $tax_amount;
          $total_total_amount = $total_total_amount + $service_total_amount + $tax_amount;

          $attributes = [];
          $attributes['subtotal'] = $total_subtotal;
          $attributes['service_charge'] = $total_service_charge;
          $attributes['amount_with_service'] = $total_amount_with_service;
          $attributes['discount_percent'] = 0;
          $attributes['taxable_amount'] = $total_taxable_amount;
          $attributes['tax_amount'] = $total_tax_amount;
          $attributes['total_amount'] = $total_total_amount;

          \App\Models\Folio::find($folio->id)->update($attributes);

          $detail_attributes = new \App\Models\FolioDetail();
          $detail_attributes->folio_id = $folio->id;
          $detail_attributes->description = $room_description;
          $detail_attributes->price = $room_price;
          $detail_attributes->quantity = $total_days;
          $detail_attributes->total = $total_room_price;
          $detail_attributes->date = \Carbon\Carbon::now();
          $detail_attributes->is_inventory = 0;
          $detail_attributes->res_id = $cr->id;
          $detail_attributes->flag = 'accommodation';
          $detail_attributes->save();

          if ($breakfast_price != 0) {

            $detail_attributes = new \App\Models\FolioDetail();
            $detail_attributes->folio_id = $folio->id;
            $detail_attributes->description = 'Break Fast';
            $detail_attributes->price = $breakfast_price;
            $detail_attributes->quantity = $total_days;
            $detail_attributes->total = $total_breakfast_price;
            $detail_attributes->date = \Carbon\Carbon::now();
            $detail_attributes->is_inventory = 0;
            $detail_attributes->res_id = $cr->id;
            $detail_attributes->flag = 'restaurant';
            $detail_attributes->save();
          }

          if ($lunch_price != 0) {

            $detail_attributes = new \App\Models\FolioDetail();
            $detail_attributes->folio_id = $folio->id;
            $detail_attributes->description = 'Lunch';
            $detail_attributes->price = $lunch_price;
            $detail_attributes->quantity = $total_days;
            $detail_attributes->total = $total_lunch_price;
            $detail_attributes->date = \Carbon\Carbon::now();
            $detail_attributes->is_inventory = 0;
            $detail_attributes->res_id = $cr->id;
            $detail_attributes->flag = 'restaurant';
            $detail_attributes->save();
          }

          if ($dinner_price != 0) {
            $detail_attributes = new \App\Models\FolioDetail();
            $detail_attributes->folio_id = $folio->id;
            $detail_attributes->description = 'Dinner';
            $detail_attributes->price = $dinner_price;
            $detail_attributes->quantity = $total_days;
            $detail_attributes->total = $total_dinner_price;
            $detail_attributes->date = \Carbon\Carbon::now();
            $detail_attributes->is_inventory = 0;
            $detail_attributes->res_id = $cr->id;
            $detail_attributes->flag = 'restaurant';
            $detail_attributes->save();
          }

          if ($adjustmentprice != 0) {

            $detail_attributes = new \App\Models\FolioDetail();
            $detail_attributes->folio_id = $folio->id;
            $detail_attributes->description = 'Discount on Reservation';
            $detail_attributes->price = $adjustmentprice;
            $detail_attributes->quantity = $total_days;
            $detail_attributes->total = $adjustmentprice;
            $detail_attributes->date = \Carbon\Carbon::now();
            $detail_attributes->is_inventory = 0;
            $detail_attributes->res_id = $cr->id;
            $detail_attributes->flag = 'discount';
            $detail_attributes->save();
          }
        }
      }
    }


    return 1;

  }


  public function FolioTomorrow(Request $request)
  {


    $tomorrow = \Carbon\Carbon::tomorrow();
    // dd($tomorrow);


    $reservations = \App\Models\Reservation::where('reservation_status', 3)->where('check_out', '!=', $tomorrow)->where('check_out', '>=', $tomorrow)
      ->where('parent_res', null)->get();



    foreach ($reservations as $reservation) {

        $this->updateFolio($reservation);

    }


    return redirect('/home');
  }


  public function getNextBillNumber($outlet_id){

    $order_no = \App\Models\Orders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)
                ->first();

    $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first();

    $bill_no = \App\Models\Orders::select('bill_no')
        ->where('fiscal_year', $fiscal_year->fiscal_year)
        ->orderBy('bill_no', 'desc')
        ->where('outlet_id', $outlet_id)
        ->first();

    $bill_no = $bill_no->bill_no + 1;

    return $bill_no;

  }
  public function getNextAbbrBillNumber($outlet_id){

    $order_no = \App\Models\AbbrOrders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)
                ->first();

    $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first();

    $bill_no = \App\Models\AbbrOrders::select('bill_no')
        ->where('fiscal_year', $fiscal_year->fiscal_year)
        ->orderBy('bill_no', 'desc')
        ->where('outlet_id', $outlet_id)
        ->first();

      $bill_no = $bill_no->bill_no + 1;

    return $bill_no;

  }

  public function createOrderMeta($order){

    $ordermeta = new \App\Models\OrderMeta();
    $ordermeta->order_id = $order->id;
    $ordermeta->sync_with_ird = 0;
    $ordermeta->is_bill_active = 1;
    $ordermeta->save();

    return 0;
  }
  public function createAbbrOrderMeta($order){

    $ordermeta = new \App\Models\AbbrOrderMeta();
    $ordermeta->order_id = $order->id;
    $ordermeta->sync_with_ird = 0;
    $ordermeta->is_bill_active = 1;
    $ordermeta->save();

    return 0;
  }

  public function moveProxyToOrder($id){

    $finOrderProxy = \App\Models\OrderProxy::find($id);
    if($finOrderProxy && $finOrderProxy->is_final_invoice){

      return false;

    }elseif (!$finOrderProxy) {
      return false;
    }

    $finOrderProxyArr = $finOrderProxy->toArray();

    unset($finOrderProxyArr['id']);

    $finOrderProxyArr['bill_no'] = $this->getNextBillNumber($finOrderProxyArr['outlet_id']);

    //dd($finOrderProxyArr);
    $finOrderProxyArr ['bill_date'] = BusinessDate::first()->business_date;
//    $finOrderProxyArr ['bill_type'] = $finOrderProxyArr['total_amount']>10000?'non_abbreviated':'abbreviated';

    $finOrder = \App\Models\Orders::create($finOrderProxyArr);

    $this->createOrderMeta($finOrder);

    $finOrderProxy->update(['reference_order'=>$finOrder->id,'ready_status'=>'checkedout','is_final_invoice'=>'1']); //to check

    $finOrderProxyDetails = \App\Models\OrderDetailProxy::where('order_id',$id)->get()
    ->toArray();


    $finOrdersDetails = [];

    foreach ($finOrderProxyDetails as $key => $value) {

      unset($value['id']);

      $value['order_id'] = $finOrder->id;

      $finOrdersDetails [] = $value;
    }

    \App\Models\OrderDetail::insert($finOrdersDetails);

    \App\Models\OrderDetailProxy::where('order_id',$id)->delete();

    $mergedTable =  \App\Models\OrderProxy::where('merged_to',$id)->pluck('id')->toArray();

    \App\Models\OrderProxy::whereIn('id',$mergedTable)->delete();

    \App\Models\OrderDetailProxy::whereIn('order_id',$mergedTable)->delete();
    if(!$finOrderProxy->is_edm){

      $finOrderProxy->delete();
    }

    return $finOrder->id;



  }

  public function moveProxyToAbbrOrder($id){

    $finOrderProxy = \App\Models\OrderProxy::find($id);
    if($finOrderProxy && $finOrderProxy->is_final_invoice){

      return false;

    }elseif (!$finOrderProxy) {
      return false;
    }

    $finOrderProxyArr = $finOrderProxy->toArray();

    unset($finOrderProxyArr['id']);

    $finOrderProxyArr['bill_no'] = $this->getNextAbbrBillNumber($finOrderProxyArr['outlet_id']);

    //dd($finOrderProxyArr);
    $finOrderProxyArr ['bill_date'] = date('Y-m-d');

    $finOrder = \App\Models\AbbrOrders::create($finOrderProxyArr);

    $this->createAbbrOrderMeta($finOrder);

    $finOrderProxy->update(['reference_order'=>$finOrder->id,'ready_status'=>'checkedout','is_final_invoice'=>'1']); //to check

    $finOrderProxyDetails = \App\Models\OrderDetailProxy::where('order_id',$id)->get()
    ->toArray();


    $finOrdersDetails = [];

    foreach ($finOrderProxyDetails as $key => $value) {

      unset($value['id']);

      $value['order_id'] = $finOrder->id;

      $finOrdersDetails [] = $value;
    }

    \App\Models\AbbrOrderDetail::insert($finOrdersDetails);

    \App\Models\OrderDetailProxy::where('order_id',$id)->delete();

    $mergedTable =  \App\Models\OrderProxy::where('merged_to',$id)->pluck('id')->toArray();

    \App\Models\OrderProxy::whereIn('id',$mergedTable)->delete();

    \App\Models\OrderDetailProxy::whereIn('order_id',$mergedTable)->delete();
    if(!$finOrderProxy->is_edm){

      $finOrderProxy->delete();
    }

    return $finOrder->id;



  }




  public function OutletPostBills($proxyid)
  {
    DB::beginTransaction();
      $order_proxy=\App\Models\OrderProxy::find($proxyid);
      if ($order_proxy->invoice_type=='abbr'){

          $id = $this->moveProxyToAbbrOrder($proxyid);
          if(!$id){

              Flash::warning("Order Already Posted Please Dont Move back and post order Now :D");

              return redirect('/admin/pos/dashboard');
          }

          $order = \App\Models\AbbrOrders::find($id);

          $order_product_type  = \App\Models\AbbrOrderDetail::where('order_id', $order->id)->select('product_type_id')->distinct('product_type_id')->get();

          $fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', '1')->first();


          $bill_no = \App\Models\AbbrOrders::select('bill_no')
              ->where('fiscal_year', $fiscal_year->fiscal_year)
              ->where('outlet_id', $order->outlet_id)
              ->orderBy('bill_no', 'desc')
              ->first();



          $bill_no = $bill_no->bill_no + 1;



          $ckfiscalyear = \App\Models\Fiscalyear::where('current_year', '1')
              ->where('start_date', '<=', date('Y-m-d'))
              ->where('end_date', '>=', date('Y-m-d'))
              ->first();

          if (!$ckfiscalyear)
              return \Redirect::back()->withErrors(['Please update fiscal year <a href="/admin/fiscalyear/create">Click Here</a>!']);

          $fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->fiscal_year;



          if ($order->reservation->guest_type != 'COM') {

              $outlet_ledgers = $order->outlet->ledger_id;

              $current_ledger_id = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;

              $attributes_order['entrytype_id'] = 11;
              $attributes_order['tag_id'] = 6; //sales invoice
              $attributes_order['user_id'] = \Auth::user()->id;
              $attributes_order['org_id'] = \Auth::user()->org_id;
              $type=\App\Models\Entrytype::find(11);
              $attributes_order['number'] = \TaskHelper::generateId($type);
              $attributes_order['bill_no'] = 'AI'.$order->bill_no;
              $attributes_order['resv_id'] = $order->reservation_id ?? 0;
              $attributes_order['source'] = 'AUTO_POS';
              $attributes_order['date'] = \Carbon\Carbon::today();
              $attributes_order['fiscal_year_id'] = $current_ledger_id;

              $attributes_order['notes'] = 'Entry Of Abbreviated POS Id#' . $order->id . '';

              $attributes_order['dr_total'] = $order->total_amount+$order->discount_amount;
              $attributes_order['cr_total'] = $order->total_amount+$order->discount_amount;

              $attributes_order['order_id'] = $order->id;

              if ($order->reservation && $order->reservation->guest_type == 'COM') {

                  $attributes_order['dr_total'] = 0;
                  $attributes_order['cr_total'] = 0;
              } else {

                  $attributes_order['dr_total'] = $order->total_amount+$order->discount_amount;
                  $attributes_order['cr_total'] = $order->total_amount+$order->discount_amount;
              }

              $entry = \App\Models\Entry::create($attributes_order);

              foreach ($order_product_type as $opt) {
                  if ($opt->product_type_id) {

                      $ledger_id = \App\Models\ProductTypeMaster::find($opt->product_type_id)->ledger_id;
                      $product_type_total_amount = \App\Models\AbbrOrderDetail::where('order_id', $order->id)->where('product_type_id', $opt->product_type_id)->sum(\DB::raw('total - tax_amount'));
                      $sub_amount = new \App\Models\Entryitem();
                      $sub_amount->entry_id = $entry->id;
                      $sub_amount->dc = 'C';
                      $sub_amount->ledger_id = $ledger_id;
                      $sub_amount->amount = $product_type_total_amount;
                      $sub_amount->narration = 'being sales made';
                      $sub_amount->save();
                  } else {

                      $ledger_id = \FinanceHelper::get_ledger_id('OTHERS_INCOME_LEDGER_ID');
                      $product_type_total_amount = \App\Models\AbbrOrderDetail::where('order_id', $order->id)->where('product_type_id', $opt->product_type_id)->sum(\DB::raw('total - tax_amount'));
                      $sub_amount = new \App\Models\Entryitem();
                      $sub_amount->entry_id = $entry->id;
                      $sub_amount->dc = 'C';
                      $sub_amount->ledger_id = $ledger_id;
                      $sub_amount->amount = $product_type_total_amount;
                      $sub_amount->narration = 'being sales made';
                      $sub_amount->save();
                  }
              }

//      if ($order->service_charge) {
//
//        $service_charge = new \App\Models\Entryitem();
//        $service_charge->entry_id = $entry->id;
//        $service_charge->dc = 'C';
//        $service_charge->ledger_id = \FinanceHelper::get_ledger_id('SERVICE_CHARGE_LEDGER_ID');
//        $service_charge->amount = $order->service_charge;
//        $service_charge->narration = 'being sales made';
//        $service_charge->save();
//      }

              // Vat Amount
              $vat_amount = new \App\Models\Entryitem();
              $vat_amount->entry_id = $entry->id;
              $vat_amount->dc = 'C';
              $vat_amount->ledger_id = \FinanceHelper::get_ledger_id('VAT_LEDGER_ID');
              $vat_amount->amount = $order->tax_amount;
              $vat_amount->narration = 'being sales made ';
              $vat_amount->save();

              // cash account
              if ($order->discount_amount>0) {
                  $discount = new \App\Models\Entryitem();
                  $discount->entry_id = $entry->id;
                  $discount->dc = 'D';
                  $discount->ledger_id = 1534;//1534 (for live:[POS Guest Discount])
                  $discount->amount = $order->discount_amount;
                  $discount->narration = 'being sales made';
                  $discount->save();
              }
              $cash_amount = new \App\Models\Entryitem();
              $cash_amount->entry_id = $entry->id;
              $cash_amount->dc = 'D';
              $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
              $cash_amount->amount = $order->total_amount;
              $cash_amount->narration = 'being sales made ';
              $cash_amount->save();
          }

          \App\Models\AbbrOrderMeta::where('order_id', $id)->first()->update(['is_posted' => 1, 'posting_entry_id' => $entry->id ?? '']);


          Audit::log(Auth::user()->id, "POS Billing", "Bill Is Posted to ledger: ID-" . $id . "");

          Flash::success('Orders posted Successfully');
          DB::commit();
          return redirect("/admin/payment/orders/{$order->id}/create?invoice_type=abbr")->with( [ 'printbill' => true ] );

      }



    $id = $this->moveProxyToOrder($proxyid);
    if(!$id){

      Flash::warning("Order Already Posted Please Dont Move back and post order Now :D");

      return redirect('/admin/pos/dashboard');
    }

    $order = \App\Models\Orders::find($id);
    $order_product_type  = \App\Models\OrderDetail::where('order_id', $order->id)->select('product_type_id')->distinct('product_type_id')->get();

    $fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', '1')->first();


    $bill_no = \App\Models\Orders::select('bill_no')
      ->where('fiscal_year', $fiscal_year->fiscal_year)
      ->where('outlet_id', $order->outlet_id)
      ->orderBy('bill_no', 'desc')
      ->first();



    $bill_no = $bill_no->bill_no + 1;



    $ckfiscalyear = \App\Models\Fiscalyear::where('current_year', '1')
      ->where('start_date', '<=', date('Y-m-d'))
      ->where('end_date', '>=', date('Y-m-d'))
      ->first();

    if (!$ckfiscalyear)
      return \Redirect::back()->withErrors(['Please update fiscal year <a href="/admin/fiscalyear/create">Click Here</a>!']);

    $fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->fiscal_year;



    if ($order->reservation->guest_type != 'COM') {

      $outlet_ledgers = $order->outlet->ledger_id;

      $current_ledger_id = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;

      $attributes_order['entrytype_id'] = 11;
      $attributes_order['tag_id'] = 6; //sales invoice
      $attributes_order['user_id'] = \Auth::user()->id;
      $attributes_order['org_id'] = \Auth::user()->org_id;
        $type=\App\Models\Entrytype::find(11);
        $attributes_order['number'] = \TaskHelper::generateId($type);
        $attributes_order['bill_no'] = 'TI'.$order->bill_no;
      $attributes_order['resv_id'] = $order->reservation_id ?? 0;
      $attributes_order['source'] = 'AUTO_POS';
      $attributes_order['date'] = \Carbon\Carbon::today();
      $attributes_order['fiscal_year_id'] = $current_ledger_id;

      $attributes_order['notes'] = 'Entry Of POS Id#' . $order->id . '';

      $attributes_order['dr_total'] = $order->total_amount+$order->discount_amount;
      $attributes_order['cr_total'] = $order->total_amount+$order->discount_amount;

      $attributes_order['order_id'] = $order->id;

      if ($order->reservation && $order->reservation->guest_type == 'COM') {

        $attributes_order['dr_total'] = 0;
        $attributes_order['cr_total'] = 0;
      } else {

        $attributes_order['dr_total'] = $order->total_amount+$order->discount_amount;
        $attributes_order['cr_total'] = $order->total_amount+$order->discount_amount;
      }

      $entry = \App\Models\Entry::create($attributes_order);

      foreach ($order_product_type as $opt) {
        if ($opt->product_type_id) {

          $ledger_id = \App\Models\ProductTypeMaster::find($opt->product_type_id)->ledger_id;
          $product_type_total_amount = \App\Models\OrderDetail::where('order_id', $order->id)->where('product_type_id', $opt->product_type_id)->sum(\DB::raw('total - tax_amount'));
          $sub_amount = new \App\Models\Entryitem();
          $sub_amount->entry_id = $entry->id;
          $sub_amount->dc = 'C';
          $sub_amount->ledger_id = $ledger_id;
          $sub_amount->amount = $product_type_total_amount;
          $sub_amount->narration = 'being sales made';
          $sub_amount->save();
        } else {

          $ledger_id = \FinanceHelper::get_ledger_id('OTHERS_INCOME_LEDGER_ID');
          $product_type_total_amount = \App\Models\OrderDetail::where('order_id', $order->id)->where('product_type_id', $opt->product_type_id)->sum(\DB::raw('total - tax_amount'));
          $sub_amount = new \App\Models\Entryitem();
          $sub_amount->entry_id = $entry->id;
          $sub_amount->dc = 'C';
          $sub_amount->ledger_id = $ledger_id;
          $sub_amount->amount = $product_type_total_amount;
          $sub_amount->narration = 'being sales made';
          $sub_amount->save();
        }
      }

//      if ($order->service_charge) {
//
//        $service_charge = new \App\Models\Entryitem();
//        $service_charge->entry_id = $entry->id;
//        $service_charge->dc = 'C';
//        $service_charge->ledger_id = \FinanceHelper::get_ledger_id('SERVICE_CHARGE_LEDGER_ID');
//        $service_charge->amount = $order->service_charge;
//        $service_charge->narration = 'being sales made';
//        $service_charge->save();
//      }

      // Vat Amount
      $vat_amount = new \App\Models\Entryitem();
      $vat_amount->entry_id = $entry->id;
      $vat_amount->dc = 'C';
      $vat_amount->ledger_id = \FinanceHelper::get_ledger_id('VAT_LEDGER_ID');
      $vat_amount->amount = $order->tax_amount;
      $vat_amount->narration = 'being sales made ';
      $vat_amount->save();

      // cash account
        if ($order->discount_amount>0) {
            $discount = new \App\Models\Entryitem();
            $discount->entry_id = $entry->id;
            $discount->dc = 'D';
            $discount->ledger_id = 1534;//1534 (for live:[POS Guest Discount])
            $discount->amount = $order->discount_amount;
            $discount->narration = 'being sales made';
            $discount->save();
        }
      $cash_amount = new \App\Models\Entryitem();
      $cash_amount->entry_id = $entry->id;
      $cash_amount->dc = 'D';
      $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
      $cash_amount->amount = $order->total_amount;
      $cash_amount->narration = 'being sales made ';
      $cash_amount->save();
    }

    \App\Models\OrderMeta::where('order_id', $id)->first()->update(['is_posted' => 1, 'posting_entry_id' => $entry->id ?? '']);


    Audit::log(Auth::user()->id, "POS Billing", "Bill Is Posted to ledger: ID-" . $id . "");

    Flash::success('Orders posted Successfully');
    DB::commit();
    return redirect("/admin/payment/orders/{$order->id}/create")->with( [ 'printbill' => true ] );
  }



  public function OutletPostBillsToday()
  {

    $posbills = Orders::select('fin_orders.*')
      ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
      ->orderBy('fin_orders.id', 'desc')
      ->where('fin_orders_meta.is_posted', '0')
      ->where('fin_orders_meta.is_bill_active', 1)
      ->get();
//      $posbills = Orders::orderBy('id', 'desc')
//          ->select('fin_orders.*')
//          ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
//          ->where('fin_orders_meta.settlement', '0')
//          ->where('fin_orders_meta.is_bill_active', 1)
//          ->get();
//    dd($posbills);

    if (count($posbills) > 0) {

      foreach ($posbills as $ord) {

        $order = \App\Models\Orders::find($ord->id);

        $order_product_type  = \App\Models\OrderDetail::where('order_id', $ord->id)->select('product_type_id')->distinct('product_type_id')->get();


        if ($order->reservation->guest_type != 'COM') {

          // $attributes_order = $request->all();

          $current_ledger_id = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;



          $outlet_ledgers = $order->outlet->ledger_id;

          $attributes_order['entrytype_id'] = 11;
          $attributes_order['tag_id'] = 6; //sales invoice
          $attributes_order['user_id'] = \Auth::user()->id;
          $attributes_order['org_id'] = \Auth::user()->org_id;
          $attributes_order['number'] = $order->reservation_id;
          $attributes_order['resv_id'] = $order->reservation_id;
          $attributes_order['source'] = 'AUTO_POS';
          $attributes_order['date'] = \Carbon\Carbon::today();
          $attributes_order['notes'] = 'Entry Of POS Id#' . $order->id . '';
          $attributes_order['dr_total'] = $order->total_amount+$order->discount_amount;
          $attributes_order['cr_total'] = $order->total_amount+$order->discount_amount;
          $attributes_order['fiscal_year_id'] = $current_ledger_id;

          if ($order->reservation->guest_type == 'COM') {

            $attributes_order['dr_total'] = 0;
            $attributes_order['cr_total'] = 0;
          } else {

            $attributes_order['dr_total'] = $order->total_amount+$order->discount_amount;
            $attributes_order['cr_total'] = $order->total_amount+$order->discount_amount;
          }

          $entry = \App\Models\Entry::create($attributes_order);

          foreach ($order_product_type as $opt) {
            if ($opt->product_type_id) {

              $ledger_id = \App\Models\ProductTypeMaster::find($opt->product_type_id)->ledger_id;
              $product_type_total_amount = \App\Models\OrderDetail::where('order_id', $order->id)->where('product_type_id', $opt->product_type_id)->sum('total');
              $sub_amount = new \App\Models\Entryitem();
              $sub_amount->entry_id = $entry->id;
              $sub_amount->dc = 'C';
              $sub_amount->ledger_id = $ledger_id;
              $sub_amount->amount = $product_type_total_amount;
              $sub_amount->narration = 'being sales made';
              $sub_amount->save();
            } else {

              $ledger_id =  \FinanceHelper::get_ledger_id('OTHERS_INCOME_LEDGER_ID');
              $product_type_total_amount = \App\Models\OrderDetail::where('order_id', $order->id)->where('product_type_id', $opt->product_type_id)->sum('total');
              $sub_amount = new \App\Models\Entryitem();
              $sub_amount->entry_id = $entry->id;
              $sub_amount->dc = 'C';
              $sub_amount->ledger_id = $ledger_id;
              $sub_amount->amount = $product_type_total_amount;
              $sub_amount->narration = 'being sales made';
              $sub_amount->save();
            }
          }
          if ($order->service_charge) {

            $service_charge = new \App\Models\Entryitem();
            $service_charge->entry_id = $entry->id;
            $service_charge->dc = 'C';
            $service_charge->ledger_id =  \FinanceHelper::get_ledger_id('SERVICE_CHARGE_LEDGER_ID');
            $service_charge->amount = $order->service_charge;
            $service_charge->narration = 'being sales made';
            $service_charge->save();
          }
          // Vat Amount
          $vat_amount = new \App\Models\Entryitem();
          $vat_amount->entry_id = $entry->id;
          $vat_amount->dc = 'C';
          $vat_amount->ledger_id = \FinanceHelper::get_ledger_id('VAT_LEDGER_ID');
          $vat_amount->amount = $order->tax_amount;
          $vat_amount->narration = 'being sales made ';
          $vat_amount->save();

            if ($order->discount_amount>0) {
                $discount = new \App\Models\Entryitem();
                $discount->entry_id = $entry->id;
                $discount->dc = 'D';
                $discount->ledger_id = 1534;//1534 (for live:[POS Guest Discount])
                $discount->amount = $order->discount_amount;
                $discount->narration = 'being sales made';
                $discount->save();
            }
          // cash account

          $cash_amount = new \App\Models\Entryitem();
          $cash_amount->entry_id = $entry->id;
          $cash_amount->dc = 'D';
          $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
          $cash_amount->amount = $order->total_amount;
          $cash_amount->narration = 'being sales made ';
          $cash_amount->save();
        }

        \App\Models\OrderMeta::where('order_id', $ord->id)->first()->update(['is_posted' => 1, 'posting_entry_id' => $entry->id]);



        Audit::log(Auth::user()->id, "POS Billing", "Bill Is Posted: ID-" . $ord->id . "");
      }
    } else {

      Flash::error('Already Posted');
      return redirect()->back();
    }

    Flash::success("All POS Successfully Posted");

    return redirect()->back();
  }

  public function FolioPostBills($id)
  {
    DB::beginTransaction();

    $folio  = \App\Models\Folio::find($id);
   // dd($folio);
    $foliodetails = \App\Models\FolioDetail::where('folio_id', $folio->id)->where('is_posted', '0')->get();

    if (count($foliodetails) > 0) {

      if ($folio->reservation->guest_type != 'COM') {

        $sub_total_amount = 0;

        foreach ($foliodetails as $fd) {
          $sub_total_amount = $sub_total_amount + $fd->total;
        }

        // if ($folio->service_charge) {

        $total_service_charge =  $folio->service_charge;
        $total_amount_with_service_charge = $total_service_charge + $sub_total_amount;
        $total_taxable_amount = $total_amount_with_service_charge;
        $total_tax_amount = $folio->tax_amount;
        $total_amount = $total_tax_amount + $total_taxable_amount;

        // } else {

        //   $total_service_charge =  0;
        //   $total_amount_with_service_charge = $total_service_charge + $sub_total_amount;
        //   $total_taxable_amount = $total_amount_with_service_charge;
        //   $total_tax_amount = env('SALES_TAX') / 100 * $total_taxable_amount;
        //   $total_amount = $total_tax_amount + $total_taxable_amount;
        // }

        //  dd($total_tax_amount);




        //$attributes_folio = $request->all();


        $current_fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;

        $type = \App\Models\Entrytype::find(11);

          $attributes_folio['number'] = \TaskHelper::generateId($type);

        $attributes_folio['entrytype_id'] = 11;
        $attributes_folio['tag_id'] = 30; //Hotel Invoice
        $attributes_folio['user_id'] = \Auth::user()->id;
        $attributes_folio['org_id'] = \Auth::user()->org_id;
        $attributes_folio['resv_id'] = $folio->reservation_id;
        $attributes_folio['source'] = 'AUTO_FOLIO';
        $attributes_folio['date'] = \Carbon\Carbon::today();
        $attributes_folio['notes'] = 'Entry created when folio is Created With Reservation id ' . $folio->reservation_id;
        $attributes_folio['dr_total'] = $total_amount;
        $attributes_folio['cr_total'] = $total_amount;
        $attributes_folio['fiscal_year_id'] = $current_fiscal_year;



        $entry = \App\Models\Entry::create($attributes_folio);




        $sub_amount = new \App\Models\Entryitem();
        $sub_amount->entry_id = $entry->id;
        $sub_amount->dc = 'C';
        $sub_amount->ledger_id = \FinanceHelper::get_ledger_id('ROOM_SALES_LEDGER_ID');
        $sub_amount->amount = $sub_total_amount;
        $sub_amount->narration = 'being sales made';
        $sub_amount->save();

        // Service Charge


        if ($folio->service_charge) {

          $service_charge = new \App\Models\Entryitem();
          $service_charge->entry_id = $entry->id;
          $service_charge->dc = 'C';
          $service_charge->ledger_id = \FinanceHelper::get_ledger_id('SERVICE_CHARGE_LEDGER_ID');
          $service_charge->amount = $total_service_charge;
          $service_charge->narration = 'service charge';
          $service_charge->save();
        }

        // Vat Amount

        $vat_amount = new \App\Models\Entryitem();
        $vat_amount->entry_id = $entry->id;
        $vat_amount->dc = 'C';
        $vat_amount->ledger_id = \FinanceHelper::get_ledger_id('VAT_LEDGER_ID');
        $vat_amount->amount = $total_tax_amount;
        $vat_amount->narration = 'being payble vat booked ';
        $vat_amount->save();

        // cash account

        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->dc = 'D';
        $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
        $cash_amount->amount = $total_amount;
        $cash_amount->narration = 'being sales made';
        $cash_amount->save();

          $folio->update(['posting_entry_id'=>$entry->id]);
      }

      $foliodetails = \App\Models\FolioDetail::where('folio_id', $id)->update(['is_posted' => 1]);
    } else {
      DB::commit();
      Flash::error('Already Posted');
      return redirect()->back();
    }
    DB::commit();
    Flash::success("Folio Successfully Posted");

    return redirect()->back();
  }

  public function FolioPostBillsToday()
  {


    $folio_details = \App\Models\FolioDetail::where('is_posted', 0)->distinct('folio_id')->pluck('folio_id');
    $folios = $folios = \App\Models\Folio::select('folio.*')->whereIn('folio.id', $folio_details)
          ->leftjoin('reservations','reservations.id','=','folio.reservation_id')
          ->where('folio.is_bill_active', 1)
          ->whereIn('reservations.reservation_status',['3','6'])
          ->get();

    if (count($folio_details) > 0) {

      foreach ($folios as $fo) {

        $folio  = \App\Models\Folio::find($fo->id);
        $foliodetails = \App\Models\FolioDetail::where('folio_id', $folio->id)->where('is_posted', '0')->get();

        if ($folio->reservation->guest_type != 'COM') {

          $sub_total_amount = 0;

          foreach ($foliodetails as $fd) {
            $sub_total_amount = $sub_total_amount + $fd->total;
          }

          if ($folio->service_charge) {

            $total_service_charge =  env('service_charge') / 100 * $sub_total_amount;
            $total_amount_with_service_charge = $total_service_charge + $sub_total_amount;
            $total_taxable_amount = $total_amount_with_service_charge;
            $total_tax_amount = env('SALES_TAX') / 100 * $total_taxable_amount;
            $total_amount = $total_tax_amount + $total_taxable_amount;
          } else {

            $total_service_charge =  0;
            $total_amount_with_service_charge = $total_service_charge + $sub_total_amount;
            $total_taxable_amount = $total_amount_with_service_charge;
            $total_tax_amount = env('SALES_TAX') / 100 * $total_taxable_amount;
            $total_amount = $total_tax_amount + $total_taxable_amount;
          }

          // $attributes_folio = $request->all();

          $current_fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;
            $type = \App\Models\Entrytype::find(11);

            $attributes_folio['number'] = \TaskHelper::generateId($type);
          $attributes_folio['entrytype_id'] = 11;
          $attributes_folio['tag_id'] = 8; //sales invoice
          $attributes_folio['user_id'] = \Auth::user()->id;
          $attributes_folio['org_id'] = \Auth::user()->org_id;
          $attributes_folio['resv_id'] = $folio->reservation_id;
          $attributes_folio['source'] = 'AUTO_FOLIO';
          $attributes_folio['date'] = \Carbon\Carbon::today();
          $attributes_folio['notes'] = 'Entry created when folio is Created with Reservation id '.$folio->reservation_id;
          $attributes_folio['dr_total'] = $total_amount;
          $attributes_folio['cr_total'] = $total_amount;
          $attributes_folio['current_fiscal_year'] = $current_fiscal_year;


          $entry = \App\Models\Entry::create($attributes_folio);

            $folio->update(['posting_entry_id'=>$entry->id]);


            $sub_amount = new \App\Models\Entryitem();
          $sub_amount->entry_id = $entry->id;
          $sub_amount->dc = 'C';
          $sub_amount->ledger_id =  \FinanceHelper::get_ledger_id('ROOM_SALES_LEDGER_ID');
          $sub_amount->amount = $sub_total_amount;
          $sub_amount->narration = 'being sales made';
          $sub_amount->save();

          // Service Charge


          if ($folio->service_charge) {

            $service_charge = new \App\Models\Entryitem();
            $service_charge->entry_id = $entry->id;
            $service_charge->dc = 'C';
            $service_charge->ledger_id = \FinanceHelper::get_ledger_id('SERVICE_CHARGE_LEDGER_ID');
            $service_charge->amount = $total_service_charge;
            $service_charge->narration = 'service charge';
            $service_charge->save();
          }

          // Vat Amount

          $vat_amount = new \App\Models\Entryitem();
          $vat_amount->entry_id = $entry->id;
          $vat_amount->dc = 'C';
          $vat_amount->ledger_id = \FinanceHelper::get_ledger_id('VAT_LEDGER_ID');
          $vat_amount->amount = $total_tax_amount;
          $vat_amount->narration = 'being payble vat booked ';
          $vat_amount->save();

          // cash account

          $cash_amount = new \App\Models\Entryitem();
          $cash_amount->entry_id = $entry->id;
          $cash_amount->dc = 'D';
          $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('GUEST_LEDGERS');
          $cash_amount->amount = $total_amount;
          $cash_amount->narration = 'being sales made';
          $cash_amount->save();
        }


        $foliodetails = \App\Models\FolioDetail::where('folio_id', $folio->id)->update(['is_posted' => 1]);
      }
    } else {

      Flash::error('Already Posted');
      return redirect()->back();
    }

    Flash::success("All Folio Successfully Posted");
    return redirect()->back();
  }

  public function settlementResturantBillsListing()
  {

    $posbills = Orders::orderBy('id', 'desc')
      ->select('fin_orders.*')
      ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
      ->where('fin_orders_meta.settlement', '0')
      ->where('fin_orders_meta.is_bill_active', 1)
      ->get();

    $posbills_to_post = Orders::orderBy('id', 'desc')
      ->select('fin_orders.*')
      ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
      ->where('fin_orders_meta.is_posted', '0')
      ->where('fin_orders_meta.is_bill_active', 1)
      ->get();

    $abbrposbills = AbbrOrders::orderBy('id', 'desc')
      ->select('abbr_fin_orders.*')
      ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id')
      ->where('abbr_fin_orders_meta.settlement', '0')
      ->where('abbr_fin_orders_meta.is_bill_active', 1)
      ->get();

    $abbrposbills_to_post = AbbrOrders::orderBy('id', 'desc')
      ->select('abbr_fin_orders.*')
      ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id')
      ->where('abbr_fin_orders_meta.is_posted', '0')
      ->where('abbr_fin_orders_meta.is_bill_active', 1)
      ->get();

      $abbr_orders = AbbrOrders::select('abbr_fin_orders.*','abbr_fin_orders_meta.is_bill_active')->where('is_converted', 0)
          ->where('abbr_fin_orders_meta.is_bill_active', 1)
          ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id')
          ->where('bill_date',date('Y-m-d'))->get();

      $business_date=BusinessDate::first();
    $page_title = 'POS Day End';
    $page_description = 'Post all Bills and Receipts before next business day';

    return view('admin.hotel.night-audit.settlementresturantbillslisting', compact('business_date','abbr_orders','abbrposbills_to_post','abbrposbills','posbills_to_post','posbills', 'page_title', 'page_description'));
  }
    public function completeDayEnd(Request $request){
        $request->validate([
            'business_date'=>'date|required'
        ]);
        $date=BusinessDate::first();
        $date?BusinessDate::first()->update(['business_date'=>$request->business_date]):BusinessDate::create(['business_date'=>$request->business_date]);
        Flash::success('Business Date configured successfully');
        return redirect('/admin/hotel/nightaudit-settlement/resturantbills/listing');

    }
  public function nepalIrdPOSSyncModal($id)
  {

    $error = null;

    $modal_title = 'Do You Want To Post This Bill To IRD';

    $modal_route = route('admin.nepal.ird.pos.sync', array('id' => $id));

    $modal_body = 'Are you Sure you Post This to IRD? Keep in Mind You cannot change this bill.';

    return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
  }

  private function convertdate($date)
  {
    $date = explode('-', $date);
    $cal = new \App\Helpers\NepaliCalendar();
    $converted = $cal->eng_to_nep($date[0], $date[1], $date[2]);
    $nepdate =  $converted['year'] . '.' . $converted['nmonth'] . '.' . $converted['date'];
    return $nepdate;
  }

  public function nepalIrdPOSSync($id)
  {

    $orders = \App\Models\Orders::find($id);

    $guest_name  = $orders->client->name;

    $bill_date_nepali = $this->convertdate($orders->bill_date);

    $bill_today_date_nep = $this->convertdate(date('Y-m-d'));
    //dd($bill_today_date_nep);
      $invoice_no='TI-'.$orders->outlet->short_name . '' . $orders->bill_no;
    $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'), "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $orders->client->vat, "fiscal_year" => $orders->fiscal_year, "buyer_name" => $guest_name, "invoice_number" => $invoice_no, "invoice_date" => $bill_date_nepali, "total_sales" => $orders->total_amount, "taxable_sales_vat" => $orders->taxable_amount, "vat" => $orders->tax_amount, "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0, "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0, "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);

    $irdsync = new \App\Models\NepalIRDSync();
    $response = $irdsync->postbill($data);

    if ($response == 200) {

      $orders->update(['sync_with_ird' => 1]);
      Flash::success('Successfully Posted to IRD. Code: ' . $response . '');
      return redirect()->back();
    } else {
      $orders->update(['sync_with_ird' => 1]);
      Flash::error('Post Cannot Due to Response Code: ' . $response . '');
      return redirect()->back();
    }
  }


  public function nepalIrdHotelSyncModal($id)
  {

    $error = null;
    $modal_title = 'Do You Want To Post This Hotel Bill To IRD';
    $modal_route = route('admin.nepal.ird.hotel.sync', array('id' => $id));
    $modal_body = 'Are you Sure you Post This to IRD? Keep in Mind You cannot change this Hotel bill.';

    return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
  }


  public function nepalIrdHotelSync($id)
  {

    $invoice = \App\Models\Invoice::find($id);
    $guest_name  = $invoice->reservation->client->name;
    $bill_date_nepali = $this->convertdate($invoice->bill_date);
    $bill_today_date_nep = $this->convertdate(date('Y-m-d'));

    $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'), "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $invoice->reservation->client->vat, "fiscal_year" => $invoice->fiscal_year, "buyer_name" => $guest_name, "invoice_number" => env('HOTEL_BILL_PREFIX') . $invoice->bill_no, "invoice_date" => $bill_date_nepali, "total_sales" => $invoice->total_amount, "taxable_sales_vat" => $invoice->taxable_amount, "vat" => $invoice->tax_amount, "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0, "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0, "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);

    $irdsync = new \App\Models\NepalIRDSync();

    $response = $irdsync->postbill($data);

    if ($response == 200) {

      $invoice->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
      Flash::success('Successfully Posted to IRD. Code: ' . $response . '');
      return redirect()->back();
    } else {

      $invoice->update(['is_realtime' => 1,'sync_with_ird'=>'1']);
      Flash::error('Post Cannot Due to Response Code: ' . $response . '');
      return redirect()->back();
    }
  }



  public function todayUnsettledFolio(){

    $today = date('Y-m-d');

    $activeReservation = \App\Models\Reservation::select('reservations.*')->where('reservation_status', 3)
                          ->where('parent_res',null)
                          ->whereDate('folio.last_settled','<',$today)
                          ->leftjoin('folio','folio.reservation_id','=','reservations.id')
                          ->get();

    return view('admin.hotel.night-audit.todayunsettled-res',compact('activeReservation'));
  }


  public function todayUnsettledFolioPost(){


    $today = date('Y-m-d');

    $activeReservation = \App\Models\Reservation::select('reservations.*')
                          ->where('reservation_status', 3)
                          ->where('parent_res',null)
                          ->whereDate('folio.last_settled','<',$today)
                          ->leftjoin('folio','folio.reservation_id','=','reservations.id')
                          ->get();

    if(count($activeReservation) == 0){

      Flash::error("Nothing to post");

      return redirect()->back();
    }


    foreach ($activeReservation as $key => $value) {


      $this->updateFolio($value);

      $folio = $value->folio;

      $folio->update(['last_settled'=>$today]);

      \ReservationHelper::updateOccupancy($value->id);
    }

    Flash::success(" Successfully Posted ");


    return redirect()->back();



  }

  public function createFolio($reservation,$folio_type = 'master'){
    $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->first();
    if(!$fiscal_year){
      Flash::error("NO FISCAL YEAR");
      abort(404);
    }
    $bill_no = \App\Models\Orders::select('bill_no')
        ->where('fiscal_year', $fiscal_year->fiscal_year)
        ->where('outlet_id', env('HOTEL_OUTLET_ID'))
        ->orderBy('bill_no', 'desc')
        ->first();

    $bill_no = ($bill_no->bill_no??0) + 1;

    $attributes = new \App\Models\Folio();
    $attributes->user_id = \Auth::user()->id;
    $attributes->folio_type = $folio_type;
    $attributes->reservation_id = $reservation->id;
    $attributes->org_id = \Auth::user()->org_id;
    $attributes->bill_date = \Carbon\Carbon::now();
    $attributes->subtotal = 0;
    $attributes->service_charge = 0;
    $attributes->amount_with_service = 0;
    $attributes->discount_percent = 0;
    $attributes->taxable_amount = 0;
    $attributes->tax_amount = 0;
    $attributes->total_amount = 0;
    $attributes->vat_type = 'yes';
    $attributes->bill_no = $bill_no;
    $attributes->fiscal_year = $fiscal_year->fiscal_year;
    $attributes->service_type = 'yes';
    $attributes->save();
    return $attributes;

  }


  public function adjustSplitedFolio($childResId){ //both must be eloquent obj


    $chilFolioDetails = \App\Models\FolioDetail::where('res_id',$childResId)
                        ->get()->toArray();

    $childFolio = \App\Models\Folio::where('reservation_id',$childResId)->first();

    $childFolioNewDetail = [];

    foreach ($chilFolioDetails as $key => $value) {

      unset($value['id']);
      $value['folio_id']  = $childFolio->id;
      $value['res_id'] = $childResId;
      $value['created_at'] = date('Y-m-d H:i:s');
      $value['updated_at'] = date('Y-m-d H:i:s');
      $childFolioNewDetail [] = $value;

    }
     //now delete parent folio with child res
    \App\Models\FolioDetail::where('res_id',$childResId)->delete();
    //then add
    \App\Models\FolioDetail::insert($childFolioNewDetail);



    return 1;


  }







  public function adjustFolioAmount($res_id){


    $folio = \App\Models\Folio::where('reservation_id',$res_id)->first();

    $attributes = \ReservationHelper::adjustFolioAmount($folio->id);

    $folio->update($attributes);

    return 1;
  }


  public function splitReservation(Request $request){

        $childRes = \App\Models\Reservation::find($request->res_id);

        $parentRes = \App\Models\Reservation::find($childRes->parent_res);



        if(!$parentRes){
            Flash::error('No Parent Reservation');
            return redirect()->back();
        }



        $this->createFolio($childRes); //create folio
        $this->adjustSplitedFolio($childRes->id);//split res
        $this->adjustFolioAmount($parentRes->id); // adjust parent amount
        $this->adjustFolioAmount($childRes->id);// adjust child amount


        if (!$childRes->client->ledger_id) {

            $reservation_full_name = '' . ucwords($childRes->guest_name) . ' ' . env('RES_CODE') . '' . $res_id . '';
            $_ledgers =app('\App\Http\Controllers\Hotel\ReservationController')->PostLedgers($reservation_full_name, env('Guest_Ledger_Group'));
        } else {

            $_ledgers = $childRes->client->ledger_id;
        }


        $childRes->update(['reservation_status' => 3,'ledger_id' => $_ledgers,'parent_res'=>null,'ref_reservation'=>$parentRes->id]);

        Flash::success("Reservation Successfully Splited");

        return redirect()->bacK();

    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Attraction;
use Flash;

class POSSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {   

        $sessions = \App\Models\PosSession::orderBy('id','desc')->get();

        $page_title = 'Admin | Hotel | POS Sessions';

        return view('admin.hotel.possessions.index',compact('page_title','sessions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Admin | Hotel | Pos Session';
        $description = 'Create a new Session';

        //dd(4);

        return view('admin.hotel.possessions.create',compact('page_title','description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

            $tables = $request->all();
            \App\Models\PosSession::create($tables);

            Flash::success('POS Session added');

            return redirect('/admin/hotel/pos-sessions/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {   

        $description = 'Edit an Pos Session ';
        $page_title = 'Admin | Hotel | Session';
        $edit =  \App\Models\PosSession::find($id);

        return view('admin.hotel.possessions.edit',compact('edit','description','page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $attributes = $request->all();
        $pos_tables =  \App\Models\PosSession::find($id);
        $pos_tables->update($attributes);

        Flash::success('POS Session Updated');

        return redirect('/admin/hotel/pos-sessions/index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $kitchen = \App\Models\PosSession::find($id)->delete();

        Flash::success('Pos Table deleted');

        return redirect('/admin/hotel/pos-sessions/index');

    }
   
    public function getModalDelete($id)
    { 

        $error = null;

        $outlets =  \App\Models\PosSession::find($id);

        $modal_title = "Delete Pos Session";

        $modal_body ="Are you sure that you want to delete POS Session ID ".$outlets->id .". This operation is irreversible";

        $modal_route = route('admin.hotel.pos-sessions.delete',$outlets->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }
}

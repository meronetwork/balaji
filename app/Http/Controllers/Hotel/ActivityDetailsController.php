<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Room;
use App\Models\Reservation;
use ReservationHelper;
class ActivityDetailsController extends Controller
{
   public function showroomoccupied()
   {

    $reservation = ReservationHelper::room_occupied()->where('parent_res',null)->paginate(30);
      $page_title = 'Dayout';
      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'GOV' => 'Government', 'FIT' => 'FIT', 'COM' => "Complementry"];

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function showdayout()
   {
      $reservation = ReservationHelper::due_out()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Dayout';
      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'GOV' => 'Government', 'FIT' => 'FIT', 'COM' => "Complementry"];

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }

   public function showdayin()
   {
      $reservation = ReservationHelper::due_in()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Dayin';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];

      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }

   public function showstayover()
   {

      $reservation = ReservationHelper::stay_over()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Stay over';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();



      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];

      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function showarrivalconfirmed()
   {
      $reservation = \ReservationHelper::arrival_confirm()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Arrival Confirmed';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function showarrivalunconfirmed()
   {
      $reservation = \ReservationHelper::arrival_unconfirm()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Arrival Unconfirmed';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function showarrived()
   {
      $reservation = \ReservationHelper::arrived()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Arrival Unconfirmed';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function showwalkin()
   {
      $reservation = \ReservationHelper::walk_in()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'Walk in';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function duearrival()
   {
      $reservation = \ReservationHelper::due_in()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'duearrival';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function chekedOut()
   {
      $reservation = \ReservationHelper::check_out()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'checkout';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }
   public function dayuse()
   {
      $reservation = \ReservationHelper::day_use()->orderBy('id', 'desc')->paginate(20);
      $page_title = 'checkout';

      $agents = \App\Models\Lead::pluck('name', 'id')->all();
      $users = \App\User::pluck('username', 'id')->all();

      $guest_type = ['TOU' => 'Tourist', 'BUS' => 'Business Travellers', 'FAM' => 'Families', 'STA' => 'Hotel Staffs', 'DEl' => 'Delegates', 'VIP' => 'VIP', 'COR' => 'Corporate', 'FIT' => 'FIT', 'GOV' => 'Government', 'COM' => "Complementry"];
      return view('admin.hotel.reservation.index', compact('reservation', 'page_title', 'guest_type', 'agents', 'users'));
   }

   public function houseclean()
   {
      $page_title = 'houseclean';
      $room = Room::select('room.room_number', 'room.room_id', 'room.floor_number', 'room.status_id', 'room_types.room_name')
         ->leftjoin('room_status', 'room_status.status_id', '=', 'room.status_id')
         ->leftjoin('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')->where('house_status_id', '2')
         ->orderBy('room.room_id', 'DESC')->paginate(30);
      return view('admin.hotel.room.room-index', compact('room'));
   }
   public function housedirty()
   {

      $page_title = 'housedirty';
      $room = Room::select('room.room_number', 'room.room_id', 'room.floor_number', 'room.status_id', 'room_types.room_name')
         ->leftjoin('room_status', 'room_status.status_id', '=', 'room.status_id')
         ->leftjoin('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')->where('house_status_id', '1')
         ->orderBy('room.room_id', 'DESC')->paginate(30);
      return view('admin.hotel.room.room-index', compact('room', 'page_title'));
   }
   public function houseinspected()
   {
      $page_title = 'houseinspected';
      $room = Room::select('room.room_number', 'room.room_id', 'room.floor_number', 'room.status_id', 'room_types.room_name')
         ->leftjoin('room_status', 'room_status.status_id', '=', 'room.status_id')
         ->leftjoin('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')->where('house_status_id', '4')
         ->orderBy('room.room_id', 'DESC')->paginate(30);
      return view('admin.hotel.room.room-index', compact('room', 'page_title'));
   }
   public function housepickup()
   {
      $page_title = 'housepickup';
      $room = Room::select('room.room_number', 'room.room_id', 'room.floor_number', 'room.status_id', 'room_types.room_name')
         ->leftjoin('room_status', 'room_status.status_id', '=', 'room.status_id')
         ->leftjoin('room_types', 'room_types.roomtype_id', '=', 'room.roomtype_id')->where('house_status_id', '3')
         ->orderBy('room.room_id', 'DESC')->paginate(30);

      return view('admin.hotel.room.room-index', compact('room', 'page_title'));
   }
}

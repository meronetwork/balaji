<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Attraction;
use Flash;

class POSKitchenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {   

        $costcenters = \App\Models\PosKitchen::orderBy('id','desc')->get();




        $page_title = 'Admin | hotel | POS Kitchen';

        return view('admin.hotel.poskitchen.index',compact('page_title','costcenters'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page_title = 'Admin | Hotel | Kitchen';
        $description = 'Create a new Kitchen';

        return view('admin.hotel.poskitchen.create',compact('page_title','description'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

            $kitchen = $request->all();

            \App\Models\PosKitchen::create($kitchen);

            Flash::success('POS Kitchen added');

            return redirect('/admin/hotel/pos-kitchen/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  

        $description = 'Edit an Pos Kitchen';

        $page_title = 'Admin | Hotel';

        $edit =  \App\Models\PosKitchen::find($id);

        return view('admin.hotel.poskitchen.edit',compact('edit','description','page_title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $attributes = $request->all();

        $pos_kitchen =  \App\Models\PosKitchen::find($id);

        $pos_kitchen->update($attributes);

        Flash::success('POS Kitchen updated');

        return redirect('/admin/hotel/pos-kitchen/index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $kitchen = \App\Models\PosKitchen::find($id)->delete();

        Flash::success('Pos Kitchen deleted');

        return redirect('/admin/hotel/pos-kitchen/index');

    }
   
    public function getModalDelete($id)
    { 


        $error = null;

        $kitchen =  \App\Models\PosKitchen::find($id);

        $modal_title = "Delete Pos Kitchen";

        $modal_body ="Are you sure that you want to delete POS Kitchen ID ".$kitchen->id .". This operation is irreversible";

        $modal_route = route('admin.hotel.pos-kitchen.delete',$kitchen->id);


        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }
}

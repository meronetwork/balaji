<?php
namespace App\Http\Controllers\Hotel;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class ReservationCalenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = "Reservation Calendar";
        $page_description = "Syncs with Google Calendar";
        $reservation = \App\Models\Reservation::all();
        $allReservation = array();
        foreach ($reservation as $key => $value) {
            if($value->guest_id)
                $guest_name = $value->guest->full_name;
            else
                $guest_name = $value->guest_name;
            $room = $value->room_num .($value->room->room_type->type_code ?? null);
            $bgcolor = $value->reservationStatus->status_color;
            $new = array("title"=> $room .'  '.$guest_name, "start"=> $value->check_in, "end"=> $value->check_out, 'url' => '/admin/hotel/reservation-edit/'.$value->id ,'backgroundColor'=>$bgcolor);
            array_push($allReservation, $new);
        }
        $allReservation = json_encode($allReservation);
        return view('admin.hotel.calendar', compact('page_title', 'page_description', 'allReservation'));
    }
    public function modal()
    {
        $page_title = "Reservation Calendar";
        $page_description = "Syncs with Google Calendar";
        $reservation = \App\Models\Reservation::all();
        $allReservation = array();
        foreach ($reservation as $key => $value) {
            if($value->guest_id)
                $guest_name = $value->guest->full_name;
            else
                $guest_name = $value->guest_name;
            $room = $value->room_num .$value->room->room_type->type_code;
            $bgcolor = $value->reservationStatus->status_color;
            $new = array("title"=> $room .'  '.$guest_name, "start"=> $value->check_in, "end"=> $value->check_out, 'url' => '/admin/hotel/reservation-edit/'.$value->id ,'backgroundColor'=>$bgcolor);
            array_push($allReservation, $new);
        }
        $allReservation = json_encode($allReservation);
        return view('admin.hotel.calendar_modal', compact('page_title', 'page_description', 'allReservation'));
    }
    
}
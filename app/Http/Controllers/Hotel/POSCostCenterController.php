<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Attraction;
use Flash;

class POSCostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $costcenters = \App\Models\PosCostCenter::orderBy('id','desc')->get();
        $page_title = 'Admin | hotel | POS Cost Center';
        return view('admin.hotel.poscostcenter.index',compact('page_title','costcenters'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page_title = 'Admin | hotel';
        $description = 'Create a new Cost Center';
        return view('admin.hotel.poscostcenter.create',compact('page_title','description'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $cos_center = $request->all();

            \App\Models\PosCostCenter::create($cos_center);

            Flash::success('POS Cost Center added');

            return redirect('/admin/hotel/pos-cost-center/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  

        $description = 'Edit an Pos Cost Center';

        $page_title = 'Admin | Hotel';

        $edit =  \App\Models\PosCostCenter::where('id',$id)->first();

        return view('admin.hotel.poscostcenter.edit',compact('edit','description','page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $attributes = $request->all();

        $cost_center =  \App\Models\PosCostCenter::find($id);
        
        $cost_center->update($attributes);

        Flash::success('POS Cost Center updated');

        return redirect('/admin/hotel/pos-cost-center/index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $attraction = \App\Models\PosCostCenter::find($id)->delete();

        Flash::success('Pos Cost center deleted');
        return redirect('/admin/hotel/pos-cost-center/index');

    }
   
    public function getModalDelete($id)
    {

        $error = null;

        $cost_center =  \App\Models\PosCostCenter::find($id);
       
        $modal_title = "Delete Pos Cost Center";

        $modal_body ="Are you sure that you want to delete POS Cost Center ID ".$cost_center->id .". This operation is irreversible";

        $modal_route = route('admin.hotel.pos-cost-center.delete',$cost_center->id);


        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }
}

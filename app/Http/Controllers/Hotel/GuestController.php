<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Guest;
use Flash;

class GuestController extends Controller
{
    public function __construct(Guest $guest)
    {
        $this->guest = $guest;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guests = $this->guest->orderBy('id', 'desc')->paginate(30);
        $guest_type = [
            'TOU' => ['Tourist', 'btn bg-green  margin'],
            'BUS' => ['Business Travellers', 'btn bg-red  margin'],
            'FAM' => ['Families', 'btn bg-navy  margin'],
            'STA' => ['Hotel Staffs', 'btn bg-red  margin'],
            'DEl' => ['Delegates', 'btn bg-olive  margin'],
            'VIP' => ['VIP', 'btn bg-maroon margin'],
            'COR' => ['Corporate', 'btn bg-purple margin'],
            'GOV' => ['Government', 'btn bg-blue margin'],
            'FIT' => ["FIT", 'btn bg-orange margin'],
            'COM' => ["Complementry", 'btn bg-green margin']
        ];
        $page_description = 'Listing guest';

        $page_title = 'Admin | Hotel | Guests';

        return view('admin.hotel.guest.index', compact('guests', 'guest_type', 'page_description', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_description = 'Create New Guests';

        $page_title = 'Admin | Hotel | Guests';

        return view('admin.hotel.guest.create', compact('page_description', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guests = $request->all();
        $guests['user_id'] = \Auth::user()->id;
        $guest =  Guest::create($guests);
        $_ledgers = $this->PostLedgers($guest->first_name . ' ' . $guest->last_name, env('Guest_Ledger_Group'));
        $attributes['ledger_id'] = $_ledgers;
        $guest->update($attributes);
        if ($request->ajax()) {
            return ['guest' => $guest, 'type' => 'guest'];
        }
        Flash::success('Guest Sucessfully created');
        return redirect()->route('admin.hotel.guests.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guest = $this->guest->find($id);
        $page_description = 'Showing Guests #{$id}';

        $page_title = 'Admin | Hotel | Guests';

        $guest_type = [
            'TOU' => ['Tourist', 'btn bg-green  margin'],
            'BUS' => ['Business Travellers', 'btn bg-red  margin'],
            'FAM' => ['Families', 'btn bg-navy  margin'],
            'STA' => ['Hotel Staffs', 'btn bg-red  margin'],
            'DEl' => ['Delegates', 'btn bg-olive  margin'],
            'VIP' => ['VIP', 'btn bg-maroon margin'],
            'COR' => ['Corporate', 'btn bg-purple margin'],
            'GOV' => ['Government', 'btn bg-blue margin'],
            'FIT' => ["FIT", 'btn bg-orange margin'],
            'COM' => ["Complementry", 'btn bg-green margin']
        ];
        $reservation = \App\Models\Reservation::where('guest_id', $id)->get();
        return view('admin.hotel.guest.show', compact('guest', 'guest_type', 'page_description', 'page_title', 'reservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guest = $this->guest->find($id);
        $page_description = "Edit Guests #{$id}";

        $page_title = 'Admin | Hotel | Guests';
        return view('admin.hotel.guest.edit', compact('guest', 'page_description', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = $request->all();
        $guest = $this->guest->find($id);
        if (!$guest->isEditable()) {
            abort(403);
        }
        $guest->update($update);
        Flash::success('Guest Sucessfully updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guest = $this->guest->find($id);
        if (!$guest->isDeletable()) {
            abort(403);
        }
        $guest->delete();
        Flash::success('Guest Sucessfully deleted');
        return redirect()->back();
    }
    public function getModalDelete($id)
    {
        $error = null;
        $guest = $this->guest->find($id);
        $modal_title = "Delete Guest ? Cannot be undone";
        $modal_body = "Are you sure that you want to delete guest id #" . $guest->id . " This operation is irreversible";
        $modal_route = route('admin.hotel.guests-delete', $guest->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    private function getNextCodeLedgers($id)
    {

        $group_data = \App\Models\COAgroups::find($id);
        $group_code = $group_data->code;
        $q = \App\Models\COALedgers::where('group_id', $id)->get();

        if ($q) {
            $last = $q->last();
            $last = $last->code;
            $l_array = explode('-', $last);
            $new_index = end($l_array);
            $new_index += 1;
            $new_index = sprintf("%04d", $new_index);
            return $group_code . "-" . $new_index;
        } else {
            return $group_code . "-0001";
        }
    }


    private function PostLedgers($name, $id)
    {

        $detail = new \App\Models\COALedgers();
        $staff_or_company_id = \App\Models\COAgroups::find($id);
        $detail->group_id = $id;

        $detail->org_id = \Auth::user()->org_id;
        $detail->user_id = \Auth::user()->id;

        $detail->code = $this->getNextCodeLedgers($id);
        $detail->name = $name;
        $detail->op_balance_dc = 'D';
        $detail->op_balance = 0.00;
        $detail->notes = $name;
        $detail->ledger_type = $staff_or_company_id->name;
        $detail->staff_or_company_id = $staff_or_company_id->parent_id;
        if ($request->type == 1) {
            $detail->type = $request->type;
        } else {
            $detail->type = 0;
        }
        if ($request->reconciliation == 1) {
            $detail->type = $request->reconciliation;
        } else {
            $detail->reconciliation = 0;
        }
        $detail->save();
        return $detail->id;
    }
    public function ajaxgetGuest()
    {
        $term = strtolower(\Request::get('term'));

        $guests = $this->guest->where('first_name', 'LIKE', '%' . $term . '%')
            ->orWhere('last_name', 'LIKE', '%' . $term . '%')
            ->groupBy('id')->take(5)->get();


        foreach ($guests as $key => $value) {
            $return_array[] = array(
                'value' => $value->first_name . ' ' . $value->last_name,
                'id' => $value->id,
                'label' => $value->first_name . ' ' . $value->last_name . '(#' . $value->id . ')',
                'data' => json_encode($value),
            );
        }
        return \Response::json($return_array);
    }
    public function openModal()
    {
        $page_description = 'Create New Guests';

        $page_title = 'Admin | Hotel | Guests';

        return view('admin.hotel.guest.modals.create', compact('page_description', 'page_title'));
    }

    public function search()
    {
        $term = trim(\Request::get('search'));


        $guests = \App\Models\Guest::where('first_name', 'LIKE', '%' . $term . '%')
            ->orwhere('last_name', 'LIKE', '%' . $term . '%')
            ->orWhere('email', 'LIKE', '%' . $term . '%')
            ->orWhere('dob', 'LIKE', '%' . $term . '%')
            ->orWhere('mobile', 'LIKE', '%' . $term . '%')
            ->orWhere('landline', 'LIKE', '%' . $term . '%')
            ->orWhere('email', 'LIKE', '%' . $term . '%')
            ->orderBy('first_name', 'asc')->paginate(30);


        // dd($reservation);  



        $page_title = "Admin | Hotel | Guests | Search";
        $page_description = "List of Guest by Keyword: " . $term;

        return view('admin.hotel.guest.search', compact('guests', 'page_title', 'page_description'));
    }
}

<?php

namespace App\Http\Controllers\Hotel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Flash;
use App\Models\Attraction;
use App\Models\Hotel;
use App\Models\Reservation;
use App\Models\ReservationOccupancy;
class ReservationReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {  
        $page_title = 'Admin | Reservation | Reports | Index';
        $page_description = 'Lists of Reports Available.'; 
       
        return view('admin.hotel.reservationreports.index',compact('page_title','page_description'));
    }



    public function getFlashReport($nepali_date,$date){ //requires  conveted eng_to_nep date year(start,end) ,month(start,end) & today date(Y-m-d) 

        $flashReport = new FlashReport($nepali_date,$date);

        $totalRoom = $flashReport->totalRoom();

        $occupiedRoom = $flashReport->roomOccupied();

        $availableRoom = $flashReport->availableRoom($occupiedRoom);

        $roomByGuestType = $flashReport->roomByGuestType();

        $inHouseperson = $flashReport->inHouseperson();

        $inHouseIndividualperson = $flashReport->inHouseIndividualperson();

        $noSHow =  $flashReport->noSHow();

        $cancelledReservation = $flashReport->cancelledReservation();

        $reservationMade = $flashReport->reservationMade();

        $reservationTomorrow = $flashReport->reservationTomorrow();

        $roomOccupiedPercent = $flashReport->roomOccupiedPercent($occupiedRoom);

        $outletIncome =  $flashReport->outletIncome();

        $hotelOutletIncome = $flashReport->hotelOutletIncome();

        return ['dateInfo'=>$nepali_date,'totalRoom'=>$totalRoom,'occupiedRoom'=>$occupiedRoom,'availableRoom'=>$availableRoom,'roomByGuestType'=>$roomByGuestType,'inHouseperson'=>$inHouseperson,'inHouseIndividualperson'=>$inHouseIndividualperson,'noSHow'=>$noSHow,'cancelledReservation'=>$cancelledReservation,'reservationMade'=>$reservationMade,'reservationTomorrow'=>$reservationTomorrow,'roomOccupiedPercent'=>$roomOccupiedPercent,'outletIncome'=>$outletIncome,'hotelOutletIncome'=>$hotelOutletIncome];


    }

    public function ManagerFlashReport(){


        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d');


        $nepali_date = \ReservationHelper::findNepaliDate($start_date); //convert & get neplai year , month => start & end in engdate  from today eng_date

        $datePrevYear  = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start_date)) . " - 1 year"));

        $nepali_datePrevYear = \ReservationHelper::findNepaliDate($datePrevYear);

        $getFlashReportCurrent = $this->getFlashReport($nepali_date,$start_date);

        $getFlashReportPrev = $this->getFlashReport($nepali_datePrevYear,$datePrevYear);

        $stamp = time();
        // dd($getFlashReportCurrent);
        // return view('admin.hotel.reservationreports.managerflashreport',compact('getFlashReportCurrent','getFlashReportPrev'));
        $pdf = \PDF::loadView('admin.hotel.reservationreports.managerflashreport',compact('getFlashReportCurrent','getFlashReportPrev'));

        $file =''.$stamp.'-managerflashreport.pdf';
        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
        return view('admin.hotel.reservationreports.managerflashreport',compact('getFlashReportCurrent','getFlashReportPrev'));
    }

    public function GuestLedger(){

        $reservations = \App\Models\Reservation::orderBy('id','desc')->where('reservation_status','3')->get();

        //dd($reservations);

        $stamp = time();

        $pdf = \PDF::loadView('admin.hotel.reservationreports.guestledger', compact('reservations')); 
        $file =''.$stamp.'-guestledgers.pdf';

        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
    }

    public function RevenueReport(){

        $sources = \App\Models\Communication::orderBy('id','desc')->get();

        $stamp = time(); 
        $pdf = \PDF::loadView('admin.hotel.reservationreports.revenuereports',compact('sources'));
        $file =''.$stamp.'-revenuereports.pdf';

        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
    }

    public function ResturantSales(){

        $page_title = 'Admin | Reservation | Reports | Resturant Sales';  

        $date_now = date('Y-m-d');

        $date_today = date('Y-m-d');

        $date_week_after = date('Y-m-d', strtotime($date_now . ' -7 day'));

       // dd($date_week_after);
        $dates = array();

         for($i=1;$i<=7;$i++){

              array_push($dates,$date_now);
              $date_now = date('Y-m-d', strtotime($date_now . ' -1 day'));
        }

        //dd($dates);

        $producttypemasters = \App\Models\ProductTypeMaster::orderBy('id','desc')->get();

        //dd($producttypemasters);

        return view('admin.hotel.reservationreports.resturantsales',compact('dates','producttypemasters')); 


        // $stamp = time(); 
        // $pdf = \PDF::loadView('admin.hotel.reservationreports.resturantsales',compact('sources'));
        // $file =''.$stamp.'-revenuereports.pdf';

        // if (\File::exists('reports/'.$file)) 
        // {
        //     \File::Delete('reports/'.$file);
        // }
        // return $pdf->download($file);
    }



    public function RoomFlashReport(){

        $date = date('Y-m-d');

        $inHouseGuest = Reservation::where('reservation_status','3')->get();
        $expectedArrival = Reservation::where('book_in','>',$date);
        $expectedDepature = Reservation::where('book_out','>',$date);
        $stamp = time();
        $file =''.$stamp.'-managerflashreport.pdf';
        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        $pdf =  \PDF::loadView('admin.hotel.reservationreports.roomflashreport',compact('inHouseGuest','expectedArrival','expectedDepature'));
        return $pdf->download($file);

    }

   
}



class FlashReport 
{

    public function __construct($dateArr,$date){

        $this->month_start_date  = $dateArr['month']['start'];
        
        $this->month_end_date  = $dateArr['month']['end'];

        $this->year_start_date  = $dateArr['year']['start'];

        $this->year_end_date  = $dateArr['year']['end'];

        $this->today_date = $date;

        $this->total_rooms_today = \App\Models\Room::count();

        $this->total_rooms_months = $this->total_rooms_today * date('t',strtotime($this->month_start_date));

        $this->total_rooms_years = $this->total_rooms_today * 365;


    }


    public function totalRoom(){


        return ['today'=>$this->total_rooms_today,'month'=>$this->total_rooms_months,'year'=>$this->total_rooms_years];


    }

    public function roomOccupiedPercent($occupiedRoom ){


        $today =  round($occupiedRoom['today'] / $this->total_rooms_today    *100,2);

        $month =  round($occupiedRoom['month'] / $this->total_rooms_months   * 100,2);

        $year = round($occupiedRoom['year'] / $this->total_rooms_years  *100,2);

        return ['today'=>$today,'month'=>$month,'year'=>$year];




    }




 
    public function roomOccupied(){

        if($this->today_date == date('Y-m-d')){


            $today = Reservation::where('reservation_status','3')->count();

        }else{

            $today = ReservationOccupancy::where('date',$this->today_date)->count();

        }
        

        
        $month = ReservationOccupancy::where('date','>=',$this->month_start_date)
                        ->where('date','<=',$this->month_end_date)
                        ->count();

        
        $year = ReservationOccupancy::where('date','>=',$this->year_start_date)
                        ->where('date','<=',$this->year_end_date)
                        ->count();
        


        return ['today'=>$today,'month'=>$month,'year'=>$year];

    }



    
    public function availableRoom($occupiedRoom){

        
        $today = $this->total_rooms_today -  $occupiedRoom['today'];

        $month = $this->total_rooms_months -  $occupiedRoom['month'];

        $year = $this->total_rooms_months -  $occupiedRoom['year'];

        return ['today'=>$today,'month'=>$month,'year'=>$year];

    }

    public function roomByGuestType(){

        $requestType = ['COM','VIP'];

        $allData = [];

        foreach ($requestType as $key => $value) {


            if($this->today_date == date('Y-m-d')){


               $today = Reservation::where('reservation_status','3')
                            ->where('guest_type',$value)
                            ->count();

            }else{

                $today = ReservationOccupancy::where('date',$this->today_date)->where('guest_type',$value)
                        ->count();

            }
        
            

            
            $month = ReservationOccupancy::where('date','>=',$this->month_start_date)
                            ->where('date','<=',$this->month_end_date)
                             ->where('guest_type',$value)
                            ->count();

            
            $year = ReservationOccupancy::where('date','>=',$this->year_start_date)
                            ->where('date','<=',$this->year_end_date)
                            ->where('guest_type',$value)
                            ->count();

            $allData [$value] = [

                'today'=>$today,
                'month'=>$month,
                'year'=>$year
            ];        
        }
        return $allData;


    }

    public function inHouseperson(){

        if($this->today_date == date('Y-m-d')){

            $today = Reservation::select(\DB::raw('SUM(occupancy) as adult'),
                        \DB::raw('SUM(occupancy_child) as child'),\DB::raw('SUM(occupancy + occupancy_child) as person'))
                        ->where('reservation_status','3')
                        ->first();
        }else{

            $today = ReservationOccupancy::select(\DB::raw('SUM(occupancy_adult) as adult'),
                        \DB::raw('SUM(occupancy_child) as child'),\DB::raw('SUM(occupancy_adult + occupancy_child) as person'))
                        ->where('date',$this->today_date)
                        ->first();


        }
 
    
        $month = ReservationOccupancy::select(\DB::raw('SUM(occupancy_adult) as adult'),
                        \DB::raw('SUM(occupancy_child) as child'),\DB::raw('SUM(occupancy_adult + occupancy_child) as person'))->where('date','>=',$this->month_start_date)
                        ->where('date','<=',$this->month_end_date)
                        ->first();

        
        $year = ReservationOccupancy::select(\DB::raw('SUM(occupancy_adult) as adult'),
                        \DB::raw('SUM(occupancy_child) as child'),\DB::raw('SUM(occupancy_adult + occupancy_child) as person'))->where('date','>=',$this->year_start_date)
                        ->where('date','<=',$this->year_end_date)
                        ->first();


        return ['today'=>$today,'month'=>$month,'year'=>$year];




    }
    public function inHouseIndividualperson(){

        if($this->today_date == date('Y-m-d')){

            $today = Reservation::where('occupancy','1')->where('occupancy_child','0')
                        ->where('reservation_status','3')
                        ->count();

        }
        else{

            $today = ReservationOccupancy::where('occupancy_adult','1')
                        ->where('occupancy_child','0')
                        ->where('date',$this->today_date)
                        ->count();


        }
        $month = ReservationOccupancy::where('occupancy_adult','1')
                        ->where('occupancy_child','0')
                        ->where('date','>=',$this->month_start_date)
                        ->where('date','<=',$this->month_end_date)
                        ->count();

        
        $year = ReservationOccupancy::where('occupancy_adult','1')->where('occupancy_child','0')
                        ->where('date','>=',$this->year_start_date)
                        ->where('date','<=',$this->year_end_date)
                        ->count();


        return ['today'=>$today,'month'=>$month,'year'=>$year];


    }

    public function noSHow(){


      $today = Reservation::select(\DB::raw('COUNT(*) as room'),
                        \DB::raw('SUM(occupancy + occupancy_child) as person'))
                        ->where('reservation_status','10')
                        ->where('book_in',$this->today_date)
                        ->first();
      
      $month = Reservation::select(\DB::raw('COUNT(*) as room'),
                        \DB::raw('SUM(occupancy + occupancy_child) as person'))
                        ->where('reservation_status','10')
                        ->where('book_in','>=',$this->month_start_date)
                        ->where('book_in','<=',$this->month_end_date)
                        ->first();

      $year = Reservation::select(\DB::raw('COUNT(*) as room'),
                        \DB::raw('SUM(occupancy + occupancy_child) as person'))
                        ->where('reservation_status','10')
                        ->where('book_in',$this->today_date)
                        ->where('book_in','>=',$this->year_start_date)
                        ->where('book_in','<=',$this->year_end_date)
                        ->first();                        
        return ['today'=>$today,'month'=>$month,'year'=>$year];


    }

    public function cancelledReservation(){


      $today = Reservation::where('reservation_status','11')
                        ->where('book_in',$this->today_date)
                        ->count();
      
      $month = Reservation::where('reservation_status','11')
                        ->where('book_in','>=',$this->month_start_date)
                        ->where('book_in','<=',$this->month_end_date)
                        ->count();

      $year = Reservation::where('reservation_status','11')
                        ->where('book_in','>=',$this->year_start_date)
                        ->where('book_in','<=',$this->year_end_date)
                        ->count();                        

      return ['today'=>$today,'month'=>$month,'year'=>$year];

    }

    public function reservationMade(){


      $today = Reservation::where('book_in',$this->today_date)->count();
      
      $month = Reservation::where('book_in','>=',$this->month_start_date)
                        ->where('book_in','<=',$this->month_end_date)
                        ->count();

      $year = Reservation::where('book_in','>=',$this->year_start_date)
                        ->where('book_in','<=',$this->year_end_date)
                        ->count();                        

      return ['today'=>$today,'month'=>$month,'year'=>$year];

    }

    public function reservationTomorrow(){


      $tomorrow = date('Y-m-d',strtotime("+1 day", strtotime($this->today_date))) ;

      $tomorrowArrive = Reservation::select(\DB::raw('COUNT(*) as res'),\DB::raw('SUM(occupancy + occupancy_child) as person'))->where('book_in',$tomorrow)->first();


      $tomorrowDepart = Reservation::select(\DB::raw('COUNT(*) as res'),\DB::raw('SUM(occupancy + occupancy_child) as person'))->where('book_out',$tomorrow)->first();
 
                        
      return ['tomorrowArrive'=>$tomorrowArrive,'tomorrowDepart'=>$tomorrowDepart];

    }

    public function outletIncome(){

        $allOutlet = \App\Models\PosOutlets::where('id','!=',env('HOTEL_OUTLET_ID'))->get();
        $outletSummary = [];
        foreach ($allOutlet as $key => $outlet) {
            
            $today =  \App\Models\Orders::select(\DB::raw("SUM(fin_orders.total_amount) as total"))
                ->where('reservation_id','0')
                ->where('outlet_id',$outlet->id)
                ->where('fin_orders_meta.is_bill_active','1')
                ->where('bill_date',$this->today_date)
                ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                ->first();


            $month =  \App\Models\Orders::select(\DB::raw("SUM(fin_orders.total_amount) as total"))
                ->where('reservation_id','0')
                ->where('outlet_id',$outlet->id)
                ->where('fin_orders_meta.is_bill_active','1')
                ->where('bill_date','>=',$this->month_start_date)
                 ->where('bill_date','<=',$this->month_end_date)
                ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                ->first();

            $year =  \App\Models\Orders::select(\DB::raw("SUM(fin_orders.total_amount) as total"))
                ->where('reservation_id','0')
                ->where('outlet_id',$outlet->id)
                ->where('fin_orders_meta.is_bill_active','1')
                ->where('bill_date','>=',$this->year_start_date)
                ->where('bill_date','<=',$this->year_end_date)
                ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                ->first();

        $outletSummary[$outlet->outlet_code] = [

            'today'=>$today,
            'month'=>$month,
            'year'=>$year,
            'outletName'=>$outlet->name,
        ];



        }

        return $outletSummary;




    }


    public function hotelOutletIncome(){

        $salesReport = function($obj){

            $roomCharge = 0;$restroCharge = 0;$extraCharge = 0;

            foreach ($obj as $key => $value) {
                

                $folioDetails = $value->folio->folio_details;

                $roomCharge += $folioDetails->where('flag','accommodation')->sum('total');
                
                $restroCharge += $folioDetails->where('flag','restaurant')
                    ->sum('total');

                $extraCharge += $folioDetails->where('flag','extracharge')
                    ->sum('total');

            }

            return ['roomCharge'=>$roomCharge,'restroCharge'=>$restroCharge,'extracharge'=>$extraCharge];

        };

        $today =  \App\Models\Orders::select('fin_orders.*')
                ->where('reservation_id','!=','0')
                ->where('folio_id','!=','0')
                ->where('fin_orders_meta.is_bill_active','1')
                ->where('bill_date',$this->today_date)
                ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                ->get();

        $today = $salesReport($today);

        $month =  \App\Models\Orders::select('fin_orders.*')
                ->where('reservation_id','!=','0')
                ->where('folio_id','!=','0')
                ->where('fin_orders_meta.is_bill_active','1')
                ->where('bill_date','>=',$this->month_start_date)
                ->where('bill_date','<=',$this->month_end_date)
                ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                ->get();

        $month = $salesReport($month);

        $year =  \App\Models\Orders::select('fin_orders.*')
                ->where('reservation_id','!=','0')
                ->where('folio_id','!=','0')
                ->where('fin_orders_meta.is_bill_active','1')
                ->where('bill_date','>=',$this->year_start_date)
                ->where('bill_date','<=',$this->year_end_date)
                ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                ->get();


        $year = $salesReport($year);

        return ['today'=>$today,'month'=>$month,'year'=>$year];

    }

}

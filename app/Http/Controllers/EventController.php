<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventVenues;
use App\Models\EventSpace;
use App\User;
use Flash;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::select('events.id as eid' ,'events.event_type','event_venues.venue_name','events.event_name','events.source','events.event_start_date','events.event_status','events.num_participants','events.event_start_date','users.username')

        ->where(function($query){
          $start_date = \Request::get('start_date');
          $end_date = \Request::get('end_date');
          if($start_date && $end_date)
          {

            return $query->whereDate('event_start_date','<=',$start_date)->whereDate('event_end_date','>=',$end_date);
          }


        })
        ->where(function($query){
          $venue_id = \Request::get('venue_id');
          if($venue_id)
          {

            return $query->where('events.venue_id',$venue_id);
          }


        })
        ->leftjoin('users','users.id','=','events.user_id')
        ->leftjoin('event_venues','event_venues.id','=','events.venue_id')
        ->orderBy('events.event_start_date')->paginate(25);

        $page_title= "Admin | Event | Index";
        $page_description = "List of Events";
        $eventsVenue = EventVenues::pluck('venue_name as name','id');
        return view('admin.events.index',compact('events','page_title','page_description','eventsVenue'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $venue = EventVenues::all();
        $clients = \App\Models\Client::all();
        $events_type = ['concert','dinner','lunch','hightea','cocktail','picnic','party','seminar','conference','workshop','galas','csr','expo','other'];
        $event_status = ['registered','paid','cancelled','postpone'];
        $users = User::all();
        $page_title= "Admin | Event | Create";
        $page_description = "Create Event";
        $prod_cat = \App\Models\ProductCategory::where('name','!=','Bar')->orderBy('name')->get();
        foreach($prod_cat as $pc){

          $products[$pc->name] = \App\Models\Product::where('category_id',$pc->id)->where('outlet_id',env('BANQUET_OUTLET',6))->get();

        }


        return view('admin.events.createevent',compact('events_type','venue','event_status','users','page_title','page_description','clients','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $event = $request->all();

        //dd($event);

        $event['booked_by'] = \Auth::user()->id;
        unset($event['_token']);
        $events = Event::create($event);

        foreach($request->requirements as $requirements){
          $req = ['name'=>$requirements,'event_id'=>$events->id];
          \App\Models\EventRequirements::create($req);
        }

        foreach($request->products as $pro){
          $req = ['name'=>$pro,'event_id'=>$events->id];
          \App\Models\EventProduct::create($req);
        }
        Flash::success('Event Added');
        return redirect('/admin/events');
    }
      /// Store Event From Enquiry Page

    public function postEventFromEnquiry(Request $request){


        $this->validate($request, array(
          'event_name' => 'required',
          'contact_num' => 'required',
          'num_participants' => 'required'
        ));
   
      $attributes = $request->all();

      $attributes['user_id'] = 1;

      $events = Event::create($attributes);

      foreach($request->requirements as $requirements){

          $req = ['name'=>$requirements,'event_id'=>$events->id];
          \App\Models\EventRequirements::create($req);

        }

      //dd($attributes);

      //send email
//     $mail = \Mail::send('emails.application-letter', ['lead'=>$lead], function ($message) use ($request) {
//      $message->subject('Online Enquiry from '.env('APP_NAME'));
//      $message->from(env('APP_EMAIL'), env('APP_NAME'));
//      $message->to($request['email'], '');
//    });


     return redirect('/enquiry_thankyou')->with('lead_id', $events->id)->with('event_name', $request['event_name'])->with('person_name', $request['person_name'])->with('mob_phone', $request['contact_num']);   
     
    }  





    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Event::find($id);
        $clients = \App\Models\Client::all();
        $venue = EventVenues::all();
        $events_type = ['concert','dinner','lunch','hightea','cocktail','picnic','party','seminar','conference','workshop','galas'];
        $event_status = ['registered','paid','cancelled','postpone'];
        $users = User::all();

        $page_title= "Admin | Event | Edit";
        $page_description = "Edit Event";

        $prod_cat = \App\Models\ProductCategory::where('name','!=','Bar')->orderBy('name')->get();
        foreach($prod_cat as $pc){
          $products[$pc->name] = \App\Models\Product::where('category_id',$pc->id)->get();
        }
        $requirements = \App\Models\EventRequirements::where('event_id',$id)->pluck('name')->all();
        $selected_pro = \App\Models\EventProduct::where('event_id',$id)->pluck('name')->all();

        return view('admin.events.editevent',compact('events_type','venue','event_status','users','edit','page_title','page_description','clients','products','requirements','selected_pro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $event = $request->all();
        unset($event['_token']);
        $events=Event::find($id)->update($event);
        \App\Models\EventRequirements::where('event_id',$id)->delete();
        \App\Models\EventProduct::where('event_id',$id)->delete();
          foreach($request->requirements as $requirements){
          $req = ['name'=>$requirements,'event_id'=>$id];
          \App\Models\EventRequirements::create($req);
        }
        foreach($request->products as $pro){
          $req = ['name'=>$pro,'event_id'=>$id];
          \App\Models\EventProduct::create($req);
        }
        Flash::success('Event Updated !!');
        return redirect('/admin/events');

    }
      public function getModalDelete($id)
    {
        $error = null;
        $event = Event::find($id);

        // if (!$event->isdeletable())
        // {
        //     abort(403);
        // }

        $modal_title = "Delete Event";
        $modal_body ="Are you sure that you want to delete event ID ".$event->id ." with the name ".$event->event_name."? This operation is irreversible";
        // $lead = $this->lead->find($id);
        // $type = \Input::get('type');
        $modal_route = route('delete-event',$event->id);

        // $modal_body = trans('admin/leads/dialog.delete-confirm.body', ['id' => $lead->id, 'name' => $lead->name]);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Event::find($id)->delete();
        \App\Models\EventRequirements::where('event_id',$id)->delete();
        \App\Models\EventProduct::where('event_id',$id)->delete();
        Flash::success('Event Deleted !!');
        return redirect('admin/events');
    }
    public function showVenue(){
        $venue = EventVenues::select('event_venues.id','event_venues.venue_name','event_venues.venue_facilities','event_venues.other_details','users.username')->leftjoin('users','event_venues.user_id','=','users.id')->get();
        return view('admin.events.event-venue',compact('venue'));
    }
    public function createVenues(){
        $users = User::all();
        return view('admin.events.add-venue',compact('users'));
    }
    public function storeVenues(Request $request){
        $venue = $request->all();
        unset($venue['_token']);
        EventVenues::insert($venue);
        Flash::success("Venue Added");
        return redirect('admin/event-venues');
    }
    public function editVenues($id){
        $edit = EventVenues::find($id);
        $users = User::all();
        return view('admin.events.edit-venue',compact('edit','users'));

    }
    public function updateVenues(Request $request,$id){
        $venue = $request->all();
        unset($event['_token']);
        EventVenues::find($id)->update($venue);
        Flash::success('Venue Updated !!');
        return redirect('/admin/event-venues');
    }
    public function getVenueDelete($id){
        $error = null;
        $venue = EventVenues::find($id);

        // if (!$event->isdeletable())
        // {
        //     abort(403);
        // }

        $modal_title = "Delete Venue";
        $modal_body ="Are you sure that you want to delete venue ID ".$venue->id ." with the name ".$venue->venue_name."? This operation is irreversible";
        $modal_route = route('delete-venue',$venue->id);
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function destroyVenue($id){
        EventVenues::find($id)->delete();
        Flash::success('Venue Deleted  SucessFully!!');
        return redirect('/admin/event-venues');
    }

    public function showSpace(){
        $spaces = EventSpace::select('events.event_name','event_space.id','event_space.room_name','event_space.room_capability','event_space.daily_rate','event_space.occupied_date_from','event_space.occupied_date_to','event_space.booking_status','event_space.other_details','users.username')->leftjoin('users','users.id','=','event_space.user_id')->leftjoin('events','events.id','=','event_space.event_id')->get();
        return view('admin.events.event-space',compact('spaces'));
    } 
    public function createSpace(){
        $event = Event::all();
        $users = User::all();
        $bstatus = ['confirmed', 'provisional'];
        return view('admin.events.add-space',compact('event','bstatus','users'));
    }
    public function storeSpace(Request $request){
       $space = $request->all();
       unset($space['_token']);
       EventSpace::create($space);
       Flash::success('Event Space added SucessFully!!');
       return redirect('/admin/event-space');
    }
    public function editSpace($id){
        $edit = EventSpace::find($id);
        $event = Event::all();
        $users = User::all();
        $bstatus = ['confirmed', 'provisional'];
        return view('admin.events.edit-space',compact('edit','event','users','bstatus'));
    }
    public function updateSpace(Request $request,$id){
        $venue = $request->all();
        unset($event['_token']);
        EventSpace::find($id)->update($venue);
        Flash::success('Space Updated !!');
        return redirect('/admin/event-venues');
    }
    public function getspaceDelete($id){
        $error = null;
        $space = EventVenues::find($id);

        // if (!$event->isdeletable())
        // {
        //     abort(403);
        // }

        $modal_title = "Delete Venue";
        $modal_body ="Are you sure that you want to delete venue ID ".$space->id ." with the room name ".$space->room_name."? This operation is irreversible";
        $modal_route = route('delete-space',$space->id);
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function destroySpace($id){

        EventSpace::find($id)->delete();
        Flash::success('Venue Deleted  SucessFully!!');
        return redirect('/admin/event-space');
    }
    public function eventscalendar(){
      $venue = EventVenues::all();
      $page_title = "Event Reports";
      return view('admin.events.event-calendar',compact('venue','page_title'));
    }
    public function eventscalendarview(Request $request){
      $venue_id = $request->venue_id;
      $page_title = "Event Reports";
      $venue = EventVenues::all();
      $events = Event::where('venue_id',$venue_id)->get();
      $events_list = array();
      foreach ($events as $key => $value) {
        $new = array("title"=> $value->event_name, "start"=> $value->event_start_date, "end"=> $value->event_end_date,'url' => '/admin/editevent/'.$value->id);
        array_push($events_list, $new);
      }
      $events_list = json_encode($events_list);
      return view('admin.events.event-calendar',compact('venue','page_title','events_list','venue_id'));
    }


    public function eventPDF($event_id){

        $ord = \App\Models\Event::find($event_id);

        $prod_cat = \App\Models\ProductCategory::where('name','!=','Bar')->orderBy('name')->get();

        foreach($prod_cat as $pc){
          $products[$pc->name] = \App\Models\Product::where('category_id',$pc->id)->get();
        }

        $requirements = \App\Models\EventRequirements::where('event_id',$event_id)->pluck('name')->all();
        $selected_pro = \App\Models\EventProduct::where('event_id',$event_id)->pluck('name')->all();

      //  dd($selected_pro);


        $pdf = \PDF::loadView('admin.events.eventpdf',compact('ord','requirements','selected_pro','products'));
        $file = $event_id.'_'.$ord->event_name.'.pdf';

        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }

        return $pdf->download($file);

    }


}

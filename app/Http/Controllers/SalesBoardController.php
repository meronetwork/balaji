<?php

namespace App\Http\Controllers;

use App\Exports\ExcelExport;
use App\Models\ProductTypeMaster;
use App\Models\Role as Permission;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER.
 */
class SalesBoardController extends Controller
{
    /**
     * @var Permission
     */
    private $permission;
    private $org_id;
    /**
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {
        parent::__construct();
        $this->permission = $permission;
        $this->middleware(function ($request, $next) {
            $this->org_id = \Auth::user()->org_id;
            return $next($request);
        });
    }
    // private function getQuotaion($start_date,$end_date){
    //     $quotation =
    //     return $quotation;
    // }



    public function product_sales_data($product_type_id,$start_date,$end_date,$field = 'total'){

        $product_sales = \DB::select("SELECT fin_order_detail.product_id, products.name,
            SUM(fin_order_detail.{$field}) as total ,fin_orders.bill_date
            FROM fin_order_detail
            LEFT JOIN products ON products.id = fin_order_detail.product_id
            LEFT JOIN fin_orders ON fin_orders.id = fin_order_detail.order_id
            LEFT JOIN fin_orders_meta ON fin_orders_meta.order_id = fin_orders.id
            WHERE fin_orders_meta.is_bill_active = '1'
            AND fin_order_detail.product_id != '0' AND products.org_id = '$this->org_id'
            AND products.product_type_id='{$product_type_id}'
            AND fin_orders.bill_date >= '{$start_date}'
            AND fin_orders.bill_date <= '{$end_date}'
            GROUP BY fin_order_detail.product_id" );

        $product_sales_data = array();

        foreach ($product_sales as $key => $value) {
            if(isset($product_sales_data[$value->product_id]))
                $product_sales_data [$value->product_id]['y'] += $value->total;
            elseif($value->name)
                $product_sales_data [$value->product_id] = ['name'=>$value->name,'y'=>(int)$value->total];
        }

        $product_sales_data = array_values($product_sales_data);

        array_multisort( array_column($product_sales_data, "y"), SORT_DESC, $product_sales_data );


        if(!\Request::get('limit')){


            $product_sales_data = array_slice($product_sales_data,0,50);



        }



        return $product_sales_data;

    }


    // Stock Category
    public function index()
    {
        $page_title = 'Sales Dashboard';
        $page_description = 'List of all Figures';
        $all_fiscal_year = \App\Models\Fiscalyear::where('org_id',$this->org_id)->orderBy('numeric_fiscal_year','desc')->pluck('fiscal_year as name','id')->all();

        if (\Auth::user()->hasRole('admins')) {
            $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        } else {
            $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
            $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
                ->orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        }



        //dd(\Request::get('fiscal_year'));
        if(\Request::get('start_date') && \Request::get('end_date') !=''){

            $selected_start_date = \Request::get('start_date');

            $selected_end_date = \Request::get('end_date');

        }
        else{

            $fiscal = \App\Models\Fiscalyear::where('org_id',$this->org_id)->where('current_year','1')->first();

            $selected_start_date = $fiscal->start_date;

            $selected_end_date = $fiscal->end_date;
        }





        $start    = (new \DateTime($selected_start_date))->modify('first day of this month');
        $end      = (new \DateTime($selected_end_date))->modify('first day of next month');
        // dd($start,$end);
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);

        $quotation_data = array();

        $invoices_data = array();

        $tax_invoices_data = array();

        $purchase_bill_data = array();

        $order_income_data = array();

        $invoice_income_data = array();

        $expense_data = array();

        $line_chart_purchasetax_salestax  = array(
            'purch'=>['name'=>'Purchase Tax','data'=>[]],
            'sales'=>['name'=>'Sales tax','data'=>[]]
        );
        $line_chart_purchasetax_salestax_categories = array();
        $data_count = ['quotation'=>0,'invoices'=>0,'tax_invoices'=>0,'purchase_bill'=>0];

        $data_count['invoices'] = \App\Models\Orders::where('fin_orders.bill_date','>=',
            $selected_start_date)
            ->where('fin_orders.bill_date','<=',$selected_end_date)
            ->where(function($query){
                $outlet_id = \Request::get('outlet_id');
                if($outlet_id){

                    return $query->where('outlet_id',$outlet_id);
                }


            })
            ->where('fin_orders_meta.is_bill_active','1')
            ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')

            ->sum('fin_orders.total_amount');

        $data_count['purchase_bill'] = \App\Models\PurchaseOrder::where('bill_date','>=',$selected_start_date)
            ->where('bill_date','<=',$selected_end_date)
            ->where('purchase_type','bills')
            ->sum('subtotal');

        foreach ($period as $key=>$dt) {
            $line_chart_purchasetax_salestax_categories [] = $dt->format("M");
            $month_start = $dt->format("Y-m-d");
            $month_end =$dt->format('Y-m-t');
            $month = $dt->format('M Y');
            $tax_invoices = \App\Models\Orders::where('bill_date','>=',$month_start)
                ->where('bill_date','<=',$month_end)
                ->where(function($query){
                    $outlet_id = \Request::get('outlet_id');
                    if($outlet_id){

                        return $query->where('outlet_id',$outlet_id);
                    }


                })
                ->sum('total_amount');

            array_push($tax_invoices_data, ['name'=>$month, 'y'=>(float) $tax_invoices ?? 0]);

            $purchase_bill = \App\Models\PurchaseOrder::where('bill_date','>=',$month_start)
                ->where('bill_date','<=',$month_end)
                ->where('purchase_type','bills')
                ->sum('subtotal');

            array_push($purchase_bill_data, ['name'=>$month, 'y'=>(float) $purchase_bill ?? 0]);
            // echo $month_start .' '.$month_end.'<br>';

            $salesTax = \App\Models\Orders::where('bill_date','>=',$month_start)
                ->where('bill_date','<=',$month_end)
                ->where(function($query){
                    $outlet_id = \Request::get('outlet_id');
                    if($outlet_id){

                        return $query->where('outlet_id',$outlet_id);
                    }

                })
                ->sum('tax_amount');

            $purchTax = \App\Models\PurchaseOrder::where('bill_date','>=',$month_start)
                ->where('bill_date','<=',$month_end)
                ->sum('tax_amount');

            array_push($line_chart_purchasetax_salestax['sales']['data'], (float) $salesTax ?? 0);

            array_push($line_chart_purchasetax_salestax['purch']['data'], (float) $purchTax ?? 0);


        }
        // dd($line_chart_purchasetax_salestax);
        $line_chart_purchasetax_salestax = array_values($line_chart_purchasetax_salestax);


        //from here we start counting sales & Nos of product based on outlets

        $productTypeMaster = \App\Models\Product::where(function($query){

            $outlet_id = \Request::get('outlet_id');
            if($outlet_id){

                return $query->where('outlet_id',$outlet_id);
            }

        })
            ->get()->unique('product_type_id');

        $products_sale_Qty = [];
        $products_sale_Amount = [];

        foreach ($productTypeMaster as $key => $value) {
            $type_master = $value->producttypemaster;


            $products_sale_Qty [] = [

                'name'=>$type_master->name .' Counts',
                'value'=>$this->product_sales_data($type_master->id,$selected_start_date,$selected_end_date,'quantity'),
                'id'=>"{$type_master->id}_sales_count",

            ];
            $products_sale_Amount [] = [

                'name'=>$type_master->name .' Sales',
                'value'=>$this->product_sales_data($type_master->id,$selected_start_date,$selected_end_date),
                'id'=>"{$type_master->id}_sales_Amount",

            ];

        }


        $purchase_product = \DB::select("SELECT purch_order_details.product_id, products.name, SUM(purch_order_details.total) as total FROM purch_order_details  LEFT JOIN products ON products.id = purch_order_details.product_id WHERE purch_order_details.product_id != '0' AND products.org_id = '$this->org_id' GROUP BY purch_order_details.product_id" );
        $product_purchase_data = array();
        foreach ($purchase_product as $key => $value) {
            if($value->name) //name is not null
                array_push($product_purchase_data,['name'=>$value->name,'y'=>(int)$value->total]);
        }


        $sales_by_loc = \DB::select("SELECT fin_orders.from_stock_location , product_location.location_name ,SUM(fin_orders.total_amount) as total FROM fin_orders LEFT JOIN product_location ON product_location.id = fin_orders.from_stock_location WHERE fin_orders.from_stock_location != '0' AND fin_orders.org_id = '$this->org_id' GROUP BY fin_orders.from_stock_location" );
        $sales_by_loc_data = array();
        foreach ($sales_by_loc as $key => $value) {
            if($value->location_name) //name is not null
                array_push($sales_by_loc_data,['name'=>$value->location_name,'y'=>(int)$value->total]);
        }

        $purch_by_loc  = \DB::select("SELECT purch_orders.into_stock_location, product_location.location_name, SUM(purch_orders.total) as total FROM purch_orders  LEFT JOIN product_location ON product_location.id = purch_orders.into_stock_location WHERE purch_orders.into_stock_location != '0' AND purch_orders.org_id = '$this->org_id' GROUP BY purch_orders.into_stock_location" );
        //dd($purch_by_loc);
        $purc_by_location_data = array();
        foreach ($purch_by_loc as $key => $value) {
            if($value->location_name) //name is not null
                array_push($purc_by_location_data,['name'=>$value->location_name,'y'=>(int)$value->total]);
        }

        $purch_by_supplier  = \DB::select("SELECT purch_orders.supplier_id, clients.name, SUM(purch_orders.total) as total FROM purch_orders  LEFT JOIN clients ON clients.id = purch_orders.supplier_id WHERE purch_orders.supplier_id != '0'
            AND purch_orders.org_id = '$this->org_id' GROUP BY purch_orders.into_stock_location");

        $purch_by_supplier_data = array();
        foreach ($purch_by_supplier as $key => $value) {
            if($value->name) //name is not null
                array_push($purch_by_supplier_data,['name'=>ucfirst($value->name),'y'=>(int)$value->total]);
        }

        //dd($purch_by_supplier_data);
        return view('salesb', compact('page_title','page_description','quotation_data','invoices_data','fiscal','tax_invoices_data','purchase_bill_data','order_income_data','invoice_income_data','expense_data','all_fiscal_year','data_count','product_purchase_data','sales_by_loc_data','purc_by_location_data','purch_by_supplier_data','line_chart_purchasetax_salestax','line_chart_purchasetax_salestax_categories','selected_start_date','selected_end_date','products_sale_Qty','outlets','products_sale_Amount'));
    }

    public function show(Request $request){

        $product_type=$request->product_type?$request->product_type:'';
        $startdate=$request->startdate?$request->startdate:'';
        $enddate=$request->enddate?$request->enddate:'';
        $search=$request->search?$request->search:'';
        $data=[];
        if ($product_type||($startdate&&$enddate)||$search) {
            $data = \DB::table('fin_order_detail')
                ->selectRaw("fin_order_detail.product_id, products.name,products.price,products.product_type_id,product_type_masters.name as type_name,
            SUM(fin_order_detail.total) as total_price,SUM(fin_order_detail.quantity) as quantity ,fin_orders.bill_date")
                ->leftJoin('products', 'products.id', '=', 'fin_order_detail.product_id')
                ->leftJoin('product_type_masters', 'product_type_masters.id', '=', 'products.product_type_id')
                ->leftJoin('fin_orders', 'fin_orders.id', '=', 'fin_order_detail.order_id')
                ->leftJoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
                ->where([['fin_orders_meta.is_bill_active', 1], ['fin_order_detail.product_id', '!=', 0], ['products.org_id', $this->org_id]])
                ->when($product_type, function ($q) use ($product_type) {
                    $q->where('products.product_type_id', $product_type);
                })
                ->when($search, function ($q) use ($search) {
                    $q->where('products.name','like', '%'.$search.'%');
                })
                ->when($startdate && $enddate, function ($q) use ($startdate, $enddate) {
                    $q->where('fin_orders.bill_date', '>=', $startdate);
                    $q->where('fin_orders.bill_date', '>=', $startdate);
                    $q->where('fin_orders.bill_date', '<=', $enddate);
                })
                ->groupBy('fin_order_detail.product_id')
                ->when($request->sort_by=='Amount', function ($q) use ($search) {
                    $q->orderBy('total_price','Desc');
                })
                ->when($request->sort_by=='Quantity', function ($q) use ($search) {
                    $q->orderBy('quantity','Desc');
                })
                ->when($request->export, function ($q) use ($search) {
                    return $q->get();
                })
                ->when(!$request->export, function ($q) use ($search) {
                    return $q->paginate(20);
                });
        }

        if ($request->export){
            return \Excel::download(new \App\Exports\ProductSalesReportExport($data,'Product Sales Report',$request->startdate,$request->enddate,$request->search,$request->product_type?ProductTypeMaster::find($request->product_type):null,$request->sort_by), "product_sales_report_".date('d M Y').".csv");
        }
        $product_sales = $data;

        $product_types=ProductTypeMaster::select('id','name')->get();
        return view('admin.reports.salesReport',compact('product_sales','product_types'));

    }
    public function arrayPaginator($array, $request)
    {
        $page = $request->page?? 1;
        $perPage = 20;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }


}

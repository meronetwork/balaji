<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $currency;
    public function __construct(Currency $currency)
    {
        $this->currency=$currency;
    }

    public function index()
    {
        $page_title = 'Admin | Currencies';
        $currencies = $this->currency->orderBy('id', 'desc')->paginate(30);

        return view('admin.currency.index', compact('page_title', 'currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Admin| Currency';
        $page_description = 'Create new Currency';

        return view('admin.currency.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $currency=$this->currency->create($request->all());

        Flash::success('Currency SuccessFully Created');
        return redirect()->to('/admin/currencies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Admin| Currency';
        $page_description = 'Create new Currency';
        $currency = $this->currency->find($id);

        return view('admin.currency.edit', compact('page_title', 'page_description','currency'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $currency=$this->currency->find($id)->update($request->all());



        Flash::success('Currency Update Successfully');
        return redirect()->to('/admin/currencies');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = $this->currency->find($id);

//        if (! $manualCurrency->isdeletable()) {
//            abort(403);
//        }
        $currency->delete();


        Flash::success('Currency deleted.');


        return redirect()->back();
    }

    public function deleteModal($id)
    {
        $error = null;

        $currency= $this->currency->find($id);

//        if (! $manualCurrency->isdeletable()) {
//            abort(403);
//        }

        $modal_title = 'Delete Currency';

        $currency = $this->currency->find($id);
        if (\Request::get('type')) {
            $modal_route = route('admin.currency.delete', $currency->id).'?type='.\Request::get('type');
        } else {
            $modal_route = route('admin.currency.delete',$currency->id);
        }

        $modal_body = 'Are you sure you want to delete this  Currency ?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
}

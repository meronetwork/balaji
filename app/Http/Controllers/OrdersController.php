<?php

namespace App\Http\Controllers;

use App\Models\AbbrOrderDetail;
use App\Models\AbbrOrders;
use App\Models\BusinessDate;
use App\Models\Fiscalyear;
use App\Models\Folio;
use App\Models\Role as Permission;
use App\Models\Audit as Audit;
use Flash;
use DB;
use Auth;
use Session;
use Illuminate\Support\Facades\File;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Orders;
use App\Models\OrderProxy;
use App\Models\Reservation;
use App\Models\Lead;
use App\Models\OrderDetail;
use App\Models\OrderDetailProxy;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Product;
use App\Models\StockMove;
use App\User;
use App\Models\MasterComments;

use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER
 */
class OrdersController extends Controller
{
    /**
     * @var Client
     */
    private $orders;

    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param Client $bug
     * @param Permission $permission
     * @param User $user
     */
    public function __construct(Permission $permission, Orders $order, OrderProxy $orderproxy)
    {
        parent::__construct();

        $this->permission = $permission;
        $this->orders = $order;
        $this->orderproxy = $orderproxy;
        $this->pay_method = [
            'cash' => 'Cash',
            'check' => 'Check',
            'travel-agent' => 'Travel Agent',
            'city-ledger' => 'City Ledger',
            'complementry' => 'Complementry',
            'staff' => 'Staff',
            'credit-cards' => 'Credit  Cards',
            'room' => 'Room',
            'e-sewa' => 'e-sewa'
        ];


    }

    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request, $outlet_id)
    {

        $orders = $this->orderproxy->orderBy('id', 'desc')
            ->where('outlet_id', $outlet_id)
            ->where(function ($query) {
                if (\Request::get('type') && \Request::get('type') == 'invoice')
                    return $query->where('order_type', 'invoice');
            })
            ->where(function ($query) {
                if (\Request::get('status') && \Request::get('status') != '')
                    return $query->where('status', \Request::get('status'));
            })
            ->where(function ($query) {
                if (\Request::get('customer_id') && \Request::get('customer_id') != '')
                    return $query->where('client_id', \Request::get('customer_id'));
            })
            ->paginate(25);

        $locations = \App\Models\ProductLocation::where('enabled', '1')
            ->where('org_id', \Auth::user()->org_id)
            ->pluck('location_name', 'id')->all();

        $fiscalyears = \App\Models\Fiscalyear::orderBy('fiscal_year', 'desc')->pluck('fiscal_year', 'id')->all();

        $page_title = 'Orders';
        $page_description = 'Manage Orders';
        $outlet = \App\Models\PosOutlets::where('id', \Request::segment(4))->first();

        return view('admin.orders.index', compact('orders', 'outlet_id', 'outlet', 'page_title', 'page_description', 'locations'));
    }

    public function allPOSSales(Request $request)
    {

        $current_fiscal=\App\Models\Fiscalyear::where('current_year', 1)->first();

        $fiscal_year = $request->fiscal_year ? $request->fiscal_year : $current_fiscal->fiscal_year;

        $op = \Request::get('op');
        $outlet_name = \App\Models\PosOutlets::find($request->outlet_id)->name ?? '';
        $users = \App\User::where('enabled', '1')->pluck('username as name', 'id')->all();
        $allFiscalYear = \App\Models\Fiscalyear::pluck('fiscal_year', 'fiscal_year')->all();

        if (\Request::get('start_date_nep') != '' && \Request::get('end_date_nep') != '') {
            $start_date = \Request::get('start_date_nep');
            $end_date = \Request::get('end_date_nep');
            $cal = new \App\Helpers\NepaliCalendar();
            $startdate = explode('-', $start_date);
            $date = $cal->nep_to_eng($startdate[0], $startdate[1], $startdate[2]);
            $startdate = $date['year'] . '-' . $date['month'] . '-' . $date['date'];
            $enddate = explode('-', $end_date);
            $date = $cal->nep_to_eng($enddate[0], $enddate[1], $enddate[2]);
            $enddate = $date['year'] . '-' . $date['month'] . '-' . $date['date'];

        } else {
            $startdate = \Request::get('start_date');
            $enddate = \Request::get('end_date');
        }

        $prefix = '';
        if ($fiscal_year != $current_fiscal->fiscal_year) {
            $prefix = Fiscalyear::where('fiscal_year', $fiscal_year)->first()->numeric_fiscal_year . '_';
        }
        $ord = new Orders();
        $new_table = $prefix . $ord->getTable();
        $ord->setTable($new_table);
        $orders = $ord->select($prefix . 'fin_orders_meta.credit_note_no', $prefix . 'fin_orders_meta.is_bill_active', $prefix . 'fin_orders_meta.settlement', $prefix . 'fin_orders.*')
            ->leftjoin($prefix . 'fin_orders_meta', $prefix . 'fin_orders.id', '=', $prefix . 'fin_orders_meta.order_id')
            ->latest()
            ->groupBy($prefix . 'fin_orders.id')
            ->where(function ($query) use ($startdate, $enddate, $prefix) {
                if (\Request::get('start_date') != '' && \Request::get('end_date') != '')
                    return $query->whereBetween($prefix . 'fin_orders.bill_date', array(\Request::get('start_date'), \Request::get('end_date')));
                elseif (\Request::get('start_date_nep') != '' && \Request::get('end_date_nep') != '') {

                    return $query->whereBetween($prefix . 'fin_orders.bill_date', array($startdate, $enddate));
                }
            })
            ->where(function ($query) use ($prefix) {
                if (\Request::get('outlet_id') && \Request::get('outlet_id') != '')
                    return $query->where($prefix . 'fin_orders.outlet_id', \Request::get('outlet_id'));
            })
            ->where(function ($query) {
                if (\Request::get('user_id')) {

                    return $query->where('user_id', \Request::get('user_id'));

                }

            })
//            ->when(\Request::get('type')=='abbreviated',function ($query) {
//                $query->where('total_amount','<=',10000);
//
//            })
//            ->when(\Request::get('type')=='non_abbreviated',function ($query) {
//                $query->where('total_amount','>',10000);
//
//            })
            ->orderBy($prefix . 'fin_orders.bill_no', 'desc');

        if ($op == 'print' || $op == 'excel' || $op == 'pdf') {


            $orders = $orders->get();
        } else {

            $orders = $orders->paginate(30);
        }

        if ($op == 'print') {
            $orders_print = $orders;
            return view('print.salesbook-print', compact('fiscal_year','orders_print', 'startdate', 'enddate', 'outlet_name'));
        } elseif ($op == 'pdf') {
            $outlet = \App\Models\PosOutlets::find(\Request::get('outlet_id'));
            $orders_pdf = $orders;
            $pdf = \PDF::loadView('pdf.filteredsales', compact('orders_pdf', 'fiscal_year', 'startdate', 'enddate', 'outlet_name', 'outlet'))->setPaper('a4', 'landscape');
            $file = 'Report_salesbook_filtered' . date('_Y_m_d') . '.pdf';
            if (File::exists('reports/' . $file)) {
                File::Delete('reports/' . $file);
            }

            return $pdf->download($file);
        } elseif ($op == 'excel') {
            $allData = [];
            $pos_total_amount = 0;
            $pos_taxable_amount = 0;
            $pos_non_taxable_amount = 0;
            $pos_tax_amount = 0;
            $totalServiceCharge = 0;
            $totalDiscount = 0;
            foreach ($orders as $key => $o) {


                $data['sn'] = ++$key;
                $data['bill_date'] = $o->bill_date;
                $data['bill_no'] = 'TI-'.$o->outlet->outlet_code . '' . $o->bill_no;

                if ($o->reservation_id) {
                    $data['resturant_guest'] = env('RES_CODE') . $o->reservation_id . '-' . $o->reservation->guest_name;
                } elseif ($o->folio_id) {
                    $data['resturant_guest'] = $o->folio->reservation->client->name ?? '';
                } else {
                    $data['resturant_guest'] = $o->folio->reservation->guest_name ?? '';
                }

                if ($o->pos_customer_id) {

                    $data['Pos_Guest'] = $o->client->name;

                } else {
                    $data['Pos_Guest'] = '';
                }

                if ($o->pos_customer_id) {
                    $data['Guest_pan'] = $o->client->vat;
                } elseif ($o->reservation_id) {
                    $data['Guest_pan'] = $o->reservation->client->vat ?? '';
                } else {
                    $data['Guest_pan'] = $o->folio->reservation->client->vat ?? '';
                }

                $data['Total_Sales'] = $o->total_amount;
                $data['Non_Tax_Sales'] = $o->non_taxable_amount;
                $data['Export_Sales'] = '';

                $data['Service_Charge'] = round($o->service_charge, 3);
                $totalServiceCharge += round($o->service_charge, 3);
                $data['Discount'] = $o->discount_amount;
                $totalDiscount += $o->discount_amount;


                $data['Outlet'] = $o->outlet->name;
                $data['Taxable_Amount'] = $o->taxable_amount;
                $data['TAX'] = $o->tax_amount;

                $data['paid_by'] = \ReservationHelper::paidByArr($o->payments);
                $allData [] = $data;

                $pos_total_amount = $pos_total_amount + $o->total_amount;

                $pos_taxable_amount = $pos_taxable_amount + $o->taxable_amount;
                $pos_non_taxable_amount = $pos_non_taxable_amount + $o->non_taxable_amount;

                $pos_tax_amount = $pos_tax_amount + $o->tax_amount;

                if ($o->is_bill_active === 0) {
                    $data = [];
                    $data['sn'] = ++$key;
                    $data['bill_date'] = $o->bill_date;
                    $data['bill_no'] = "Ref of" . 'TI-'.$o->outlet->outlet_code . $o->bill_no . "CN-" . 'TI-'.$o->outlet->outlet_code.$o->credit_note_no;

                    if ($o->reservation_id) {
                        $data['resturant_guest'] = env('RES_CODE') . $o->reservation_id . '-' . $o->reservation->guest_name;
                    } elseif ($o->folio_id) {
                        $data['resturant_guest'] = $o->folio->reservation->client->name ?? '';
                    } else {
                        $data['resturant_guest'] = $o->folio->reservation->guest_name ?? '';
                    }

                    if ($o->pos_customer_id) {

                        $data['Pos_Guest'] = $o->client->name;

                    } else {
                        $data['Pos_Guest'] = '';
                    }

                    if ($o->pos_customer_id) {
                        $data['Guest_pan'] = $o->client->vat;
                    } elseif ($o->reservation_id) {
                        $data['Guest_pan'] = $o->reservation->client->vat;
                    } else {
                        $data['Guest_pan'] = $o->folio->reservation->client->vat;
                    }

                    $data['Total_Sales'] = '-' . $o->total_amount;
                    $data['Non_Tax_Sales'] = '-'.$o->non_taxable_amount;
                    $data['Export_Sales'] = '';

                    $data['Service_Charge'] = '-' . round($o->service_charge, 3);

                    $data['Discount'] = $o->discount_amount;
                    $totalDiscount -= $o->discount_amount;
                    $data['Outlet'] = $o->outlet->name;
                    $data['Taxable_Amount'] = '-' . $o->taxable_amount;
                    $data['TAX'] = '-' . $o->tax_amount;
                    $data['paid_by'] = \ReservationHelper::paidByArr($o->payments);

                    $allData[] = $data;
                    $pos_total_amount = $pos_total_amount - $o->total_amount;
                    $pos_taxable_amount = $pos_taxable_amount - $o->taxable_amount;
                    $pos_non_taxable_amount = $pos_non_taxable_amount - $o->non_taxable_amount;
                    $pos_tax_amount = $pos_tax_amount - $o->tax_amount;
                    $totalServiceCharge -= round($o->service_charge, 3);
                }

            }


            $date = date('Y-m-d');
            $total = [

                'sn' => '',
                'bill_date' => '',
                'bill_no' => '',
                'resturant_guest' => '',
                'Pos_Guest' => '',
                'Guest_pan' => 'Total Amount:',
                'Total_Sales' => $pos_total_amount,
                'Non_Tax_Sales' => $pos_non_taxable_amount,
                'Export_Sales' => '',
                'Service_Charge' => $totalServiceCharge,
                'Discount' => $totalDiscount,
                'Outlet' => '',
                'Taxable_Amount' => $pos_taxable_amount,
                'TAX' => $pos_tax_amount,
            ];

            array_push($allData, $total);
            return \Excel::download(new \App\Exports\ExcelExport($allData), "sales_book_{$date}.csv");


        }

        $outlet = \App\Models\PosOutlets::pluck('name', 'id')->all();

        $page_title = "Sales Book";

        $page_description = 'Sales Book for Tax Invoices';
//            dd($orders);

        return view('admin.orders.allposshow', compact('outlet', 'orders', 'page_description', 'page_title', 'users', 'allFiscalYear', 'fiscal_year'));
    }
    public function allPOSSalesAbbr(Request $request)
    {
        $current_fiscal=\App\Models\Fiscalyear::where('current_year', 1)->first();
        $fiscal_year = $request->fiscal_year ? $request->fiscal_year : $current_fiscal->fiscal_year;

        $op = \Request::get('op');
        $outlet_name = \App\Models\PosOutlets::find($request->outlet_id)->name ?? '';
        $users = \App\User::where('enabled', '1')->pluck('username as name', 'id')->all();
        $allFiscalYear = \App\Models\Fiscalyear::pluck('fiscal_year', 'fiscal_year')->all();

        if (\Request::get('start_date_nep') != '' && \Request::get('end_date_nep') != '') {
            $start_date = \Request::get('start_date_nep');
            $end_date = \Request::get('end_date_nep');
            $cal = new \App\Helpers\NepaliCalendar();
            $startdate = explode('-', $start_date);
            $date = $cal->nep_to_eng($startdate[0], $startdate[1], $startdate[2]);
            $startdate = $date['year'] . '-' . $date['month'] . '-' . $date['date'];
            $enddate = explode('-', $end_date);
            $date = $cal->nep_to_eng($enddate[0], $enddate[1], $enddate[2]);
            $enddate = $date['year'] . '-' . $date['month'] . '-' . $date['date'];

        } else {
            $startdate = \Request::get('start_date');
            $enddate = \Request::get('end_date');
        }

        $prefix = '';
        if ($fiscal_year != $current_fiscal->fiscal_year) {
            $prefix = Fiscalyear::where('fiscal_year', $fiscal_year)->first()->numeric_fiscal_year . '_';
        }
        $ord = new AbbrOrders();
        $new_table = $prefix . $ord->getTable();
        $ord->setTable($new_table);
        $orders = $ord->select($prefix . 'abbr_fin_orders_meta.credit_note_no', $prefix . 'abbr_fin_orders_meta.is_bill_active', $prefix . 'abbr_fin_orders_meta.settlement', $prefix . 'abbr_fin_orders.*')
            ->leftjoin($prefix . 'abbr_fin_orders_meta', $prefix . 'abbr_fin_orders.id', '=', $prefix . 'abbr_fin_orders_meta.order_id')
            ->latest()
            ->groupBy($prefix . 'abbr_fin_orders.id')
            ->where(function ($query) use ($startdate, $enddate, $prefix) {
                if (\Request::get('start_date') != '' && \Request::get('end_date') != '')
                    return $query->whereBetween($prefix . 'abbr_fin_orders.bill_date', array(\Request::get('start_date'), \Request::get('end_date')));
                elseif (\Request::get('start_date_nep') != '' && \Request::get('end_date_nep') != '') {

                    return $query->whereBetween($prefix . 'abbr_fin_orders.bill_date', array($startdate, $enddate));
                }
            })
            ->where(function ($query) use ($prefix) {
                if (\Request::get('outlet_id') && \Request::get('outlet_id') != '')
                    return $query->where($prefix . 'abbr_fin_orders.outlet_id', \Request::get('outlet_id'));
            })
            ->where(function ($query) {
                if (\Request::get('user_id')) {

                    return $query->where('user_id', \Request::get('user_id'));

                }

            })
            ->orderBy($prefix . 'abbr_fin_orders.bill_no', 'desc');

        if ($op == 'print' || $op == 'excel' || $op == 'pdf') {


            $orders = $orders->get();
        } else {

            $orders = $orders->paginate(30);
        }

        if ($op == 'print') {
            $orders_print = $orders;
            $invoice_type='abbr';
            return view('print.salesbook-print', compact('invoice_type','fiscal_year','orders_print', 'startdate', 'enddate', 'outlet_name'));
        } elseif ($op == 'pdf') {
            $outlet = \App\Models\PosOutlets::find(\Request::get('outlet_id'));
            $orders_pdf = $orders;
            $invoice_type='abbr';

            $pdf = \PDF::loadView('pdf.filteredsales', compact('invoice_type','orders_pdf', 'fiscal_year', 'startdate', 'enddate', 'outlet_name', 'outlet'))->setPaper('a4', 'landscape');
            $file = 'Report_salesbook_filtered' . date('_Y_m_d') . '.pdf';
            if (File::exists('reports/' . $file)) {
                File::Delete('reports/' . $file);
            }

            return $pdf->download($file);
        } elseif ($op == 'excel') {
            $allData = [];
            $pos_total_amount = 0;
            $pos_taxable_amount = 0;
            $pos_non_taxable_amount = 0;
            $pos_tax_amount = 0;
            $totalServiceCharge = 0;
            $totalDiscount = 0;
            foreach ($orders as $key => $o) {


                $data['sn'] = ++$key;
                $data['bill_date'] = $o->bill_date;
                $data['bill_no'] ='AI-'.$o->outlet->outlet_code . '' . $o->bill_no;

                if ($o->reservation_id) {
                    $data['resturant_guest'] = env('RES_CODE') . $o->reservation_id . '-' . $o->reservation->guest_name;
                } elseif ($o->folio_id) {
                    $data['resturant_guest'] = $o->folio->reservation->client->name ?? '';
                } else {
                    $data['resturant_guest'] = $o->folio->reservation->guest_name ?? '';
                }

                if ($o->pos_customer_id) {

                    $data['Pos_Guest'] = $o->client->name;

                } else {
                    $data['Pos_Guest'] = '';
                }

                if ($o->pos_customer_id) {
                    $data['Guest_pan'] = $o->client->vat;
                } elseif ($o->reservation_id) {
                    $data['Guest_pan'] = $o->reservation->client->vat ?? '';
                } else {
                    $data['Guest_pan'] = $o->folio->reservation->client->vat ?? '';
                }

                $data['Total_Sales'] = $o->total_amount;
                $data['Non_Tax_Sales'] = $o->non_taxable_amount;
                $data['Export_Sales'] = '';

                $data['Service_Charge'] = round($o->service_charge, 3);
                $totalServiceCharge += round($o->service_charge, 3);
                $data['Discount'] = $o->discount_amount;
                $totalDiscount += $o->discount_amount;


                $data['Outlet'] = $o->outlet->name;
                $data['Taxable_Amount'] = $o->taxable_amount;
                $data['TAX'] = $o->tax_amount;

                $data['paid_by'] = \ReservationHelper::paidByArr($o->payments);
                $allData [] = $data;

                $pos_total_amount = $pos_total_amount + $o->total_amount;

                $pos_taxable_amount = $pos_taxable_amount + $o->taxable_amount;
                $pos_non_taxable_amount = $pos_non_taxable_amount + $o->non_taxable_amount;

                $pos_tax_amount = $pos_tax_amount + $o->tax_amount;

                if ($o->is_bill_active === 0) {
                    $data = [];
                    $data['sn'] = ++$key;
                    $data['bill_date'] = $o->bill_date;
                    $data['bill_no'] = "Ref of" . 'AI-'.$o->outlet->outlet_code . $o->bill_no . "CN" . $o->credit_note_no;

                    if ($o->reservation_id) {
                        $data['resturant_guest'] = env('RES_CODE') . $o->reservation_id . '-' . $o->reservation->guest_name;
                    } elseif ($o->folio_id) {
                        $data['resturant_guest'] = $o->folio->reservation->client->name ?? '';
                    } else {
                        $data['resturant_guest'] = $o->folio->reservation->guest_name ?? '';
                    }

                    if ($o->pos_customer_id) {

                        $data['Pos_Guest'] = $o->client->name;

                    } else {
                        $data['Pos_Guest'] = '';
                    }

                    if ($o->pos_customer_id) {
                        $data['Guest_pan'] = $o->client->vat;
                    } elseif ($o->reservation_id) {
                        $data['Guest_pan'] = $o->reservation->client->vat;
                    } else {
                        $data['Guest_pan'] = $o->folio->reservation->client->vat;
                    }

                    $data['Total_Sales'] = '-' . $o->total_amount;
                    $data['Non_Tax_Sales'] = '-'.$o->non_taxable_amount;
                    $data['Export_Sales'] = '';

                    $data['Service_Charge'] = '-' . round($o->service_charge, 3);

                    $data['Discount'] = $o->discount_amount;
                    $totalDiscount -= $o->discount_amount;
                    $data['Outlet'] = $o->outlet->name;
                    $data['Taxable_Amount'] = '-' . $o->taxable_amount;
                    $data['TAX'] = '-' . $o->tax_amount;
                    $data['paid_by'] = \ReservationHelper::paidByArr($o->payments);

                    $allData[] = $data;
                    $pos_total_amount = $pos_total_amount - $o->total_amount;
                    $pos_taxable_amount = $pos_taxable_amount - $o->taxable_amount;
                    $pos_non_taxable_amount = $pos_non_taxable_amount - $o->non_taxable_amount;
                    $pos_tax_amount = $pos_tax_amount - $o->tax_amount;
                    $totalServiceCharge -= round($o->service_charge, 3);
                }

            }


            $date = date('Y-m-d');
            $total = [

                'sn' => '',
                'bill_date' => '',
                'bill_no' => '',
                'resturant_guest' => '',
                'Pos_Guest' => '',
                'Guest_pan' => 'Total Amount:',
                'Total_Sales' => $pos_total_amount,
                'Non_Tax_Sales' => $pos_non_taxable_amount,
                'Export_Sales' => '',
                'Service_Charge' => $totalServiceCharge,
                'Discount' => $totalDiscount,
                'Outlet' => '',
                'Taxable_Amount' => $pos_taxable_amount,
                'TAX' => $pos_tax_amount,
            ];

            array_push($allData, $total);
            return \Excel::download(new \App\Exports\ExcelExport($allData), "sales_book_{$date}.csv");


        }

        $outlet = \App\Models\PosOutlets::pluck('name', 'id')->all();

        $page_title = "Abbreviated Sales Book";

        $page_description = 'Sales Book for Abbreviated Invoices';
//            dd($orders);

        return view('admin.orders.abbr-salesbook', compact('outlet', 'orders', 'page_description', 'page_title', 'users', 'allFiscalYear', 'fiscal_year'));
    }


    public function returnpos(Request $request)
    {

        $page_title = "Return Sales Book";
        $page_description = 'Return Sales Book';

        $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();

        if (\Auth::user()->hasRole('admins')) {
            $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        } else {
            $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
            $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
                ->orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        }


        return view('admin.orders.returnpos', compact('outlets', 'page_description', 'page_title', 'users'));
    }


    public function returnposlist(Request $request)
    {
        if ($request->bill_type=='abbr'){
        $page_title = 'Admin | Sales Return Book';
        $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();

        $op = \Request::get('op');

        $outlet_name = \App\Models\PosOutlets::find($request->outlet_id)->name;


        if (\Auth::user()->hasRole('admins')) {
            $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        } else {
            $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
            $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
                ->orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        }

        $orders = \App\Models\AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.credit_note_no','abbr_fin_orders_meta.cancel_date','abbr_fin_orders_meta.void_reason')
            ->where('abbr_fin_orders.bill_date', '>=', $request->start_date)
            ->where('abbr_fin_orders.bill_date', '<=', $request->end_date)
            ->where('abbr_fin_orders_meta.is_bill_active', '0')
            ->where(function ($query) use ($request) {
                if ($request->outlet_id) {
                    return $query->where('abbr_fin_orders.outlet_id', $request->outlet_id);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->user_id) {
                    return $query->where('abbr_fin_orders.user_id', $request->user_id);
                }
            })
            ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders.id', '=', 'abbr_fin_orders_meta.order_id')
            ->groupBy('abbr_fin_orders.id')
            ->orderBy('abbr_fin_orders_meta.credit_note_no', 'DESC')
            ->paginate(50);

        if ($op == 'print') {
            $startdate = $request->start_date;
            $enddate = $request->end_date;
            $invoice_print = \App\Models\AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.*')
                ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders.id', '=', 'abbr_fin_orders_meta.order_id')
                ->where('abbr_fin_orders.bill_date', '>=', $request->start_date)
                ->where('abbr_fin_orders.bill_date', '<=', $request->end_date)
                ->where('abbr_fin_orders.org_id', \Auth::user()->org_id)
                ->where('abbr_fin_orders_meta.is_bill_active', 0)
                ->where(function ($query) use ($request) {
                    if ($request->outlet_id) {
                        return $query->where('abbr_fin_orders.outlet_id', $request->outlet_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->user_id) {
                        return $query->where('abbr_fin_orders.user_id', $request->user_id);
                    }
                })
                ->get();
            $invoice_type=$request->bill_type;

            return view('print.returnbook', compact('invoice_type','invoice_print', 'startdate', 'enddate', 'outlet_name'));
        } elseif ($op == 'pdf') {
            $invoice_type=$request->bill_type;

            $startdate = $request->start_date;
            $enddate = $request->end_date;
            $orders_pdf = \App\Models\AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.*')
                ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders.id', '=', 'abbr_fin_orders_meta.order_id')
                ->where('abbr_fin_orders.bill_date', '>=', $request->start_date)
                ->where('abbr_fin_orders.bill_date', '<=', $request->end_date)
                ->where('abbr_fin_orders.org_id', \Auth::user()->org_id)
                ->where('abbr_fin_orders_meta.is_bill_active', 0)
                ->where(function ($query) use ($request) {
                    if ($request->outlet_id) {
                        return $query->where('abbr_fin_orders.outlet_id', $request->outlet_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->user_id) {
                        return $query->where('abbr_fin_orders.user_id', $request->user_id);
                    }
                })
                ->get();

            $pdf = \PDF::loadView('pdf.returnbook', compact('invoice_type','orders_pdf', 'fiscal_year', 'startdate', 'enddate', 'outlet_name'))->setPaper('a4', 'landscape');
            $file = 'Report_returnbook_filtered' . date('_Y_m_d') . '.pdf';
            if (File::exists('reports/' . $file)) {
                File::Delete('reports/' . $file);
            }

            return $pdf->download($file);
        } elseif ($op == 'excel') {

            $orders_pdf = \App\Models\AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.*')
                ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders.id', '=', 'abbr_fin_orders_meta.order_id')
                ->where('abbr_fin_orders.bill_date', '>=', $request->start_date)
                ->where('abbr_fin_orders.bill_date', '<=', $request->end_date)
                ->where('abbr_fin_orders.org_id', \Auth::user()->org_id)
                ->where('abbr_fin_orders_meta.is_bill_active', 0)
                ->where(function ($query) use ($request) {
                    if ($request->outlet_id) {
                        return $query->where('abbr_fin_orders.outlet_id', $request->outlet_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->user_id) {
                        return $query->where('abbr_fin_orders.user_id', $request->user_id);
                    }
                })
                ->get();
            $allData = [];
            $pos_total_amount = 0;
            $pos_taxable_amount = 0;
            $pos_non_taxable_amount = 0;
            $pos_tax_amount = 0;
            foreach ($orders_pdf as $key => $o) {

                if ($o->reservation_id) {
                    $hotelGuest = env('RES_CODE') . $o->reservation_id . $o->reservation->guest_name;
                } elseif ($o->folio_id) {
                    $hotelGuest = $o->folio->reservation->client->name;
                } else {
                    $hotelGuest = $o->folio->reservation->guest_name;
                }

                if ($o->pos_customer_id) {

                    $guest_pan = $o->client->vat;

                } elseif ($o->reservation_id) {
                    $guest_pan = $o->reservation->client->vat;
                } else {
                    $guest_pan = $o->folio->reservation->client->vat;
                }


                $data = [
                    'Fiscal Year' => $o->fiscal_year,
                    'Bill Date' => $o->bill_date,
                    'Ref Bill No' => $o->outlet->outlet_code . $o->bill_no,
                    'Cancel Date' => $o->cancel_date,
                    'Credit Note No' => 'CN' . $o->credit_note_no,
                    'Cancel Reason' => $o->void_reason,
                    'Hotel Guest' => $hotelGuest,
                    'POS Guest' => $o->pos_customer_id ? $o->client->name : '',
                    'Guest PAN' => $guest_pan,
                    'Total Sales' => round($o->total_amount, 2),
                    'Non Tax Sale' => round($o->non_taxable_amount, 2),
                    'Export Sale' => '',
                    'Outlet' => $o->outlet->name,
                    'Taxable Amount' => round($o->taxable_amount, 2),
                    'TAX' => round($o->tax_amount, 2)


                ];
                $allData [] = $data;

                $pos_taxable_amount = $pos_taxable_amount + $o->taxable_amount;
                $pos_non_taxable_amount = $pos_non_taxable_amount + $o->non_taxable_amount;
                $pos_total_amount = $pos_total_amount + $o->total_amount;
                $pos_tax_amount = $pos_tax_amount + $o->tax_amount;


            }

            $allData[] = [
                'Fiscal Year' => '',
                'Bill Date' => '',
                'Ref Bill No' => '',
                'Cancel Date' => '',
                'Credit Note No' => '',
                'Cancel Reason' => '',
                'Hotel Guest' => '',
                'POS Guest' => '',
                'Guest PAN' => '',
                'Total Sales' => $pos_total_amount,
                'Non Tax Sale' => $pos_non_taxable_amount,
                'Export Sale' => '',
                'Outlet' => '',
                'Taxable Amount' => $pos_taxable_amount,
                'TAX' => $pos_tax_amount


            ];
            return \Excel::download(new \App\Exports\ExcelExport($allData), "sales_return.csv");
        }

        $request = $request->all();

        return view('admin.orders.returnpos', compact('outlets', 'orders', 'page_description', 'page_title', 'users', 'request'));

    }



        $page_title = 'Admin | Sales Return Book';
        $users = \App\User::where('enabled', '1')->pluck('username', 'id')->all();

        $op = \Request::get('op');

        $outlet_name = \App\Models\PosOutlets::find($request->outlet_id)->name;


        if (\Auth::user()->hasRole('admins')) {
            $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        } else {
            $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
            $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
                ->orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        }

        $orders = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.credit_note_no','fin_orders_meta.cancel_date','fin_orders_meta.void_reason')
            ->where('fin_orders.bill_date', '>=', $request->start_date)
            ->where('fin_orders.bill_date', '<=', $request->end_date)
            ->where('fin_orders_meta.is_bill_active', '0')
            ->where(function ($query) use ($request) {
                if ($request->outlet_id) {
                    return $query->where('fin_orders.outlet_id', $request->outlet_id);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->user_id) {
                    return $query->where('fin_orders.user_id', $request->user_id);
                }
            })
            ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
            ->groupBy('fin_orders.id')
            ->orderBy('fin_orders_meta.credit_note_no', 'DESC')
            ->paginate(50);

        if ($op == 'print') {
            $startdate = $request->start_date;
            $enddate = $request->end_date;
            $invoice_print = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.*')
                ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
                ->where('fin_orders.bill_date', '>=', $request->start_date)
                ->where('fin_orders.bill_date', '<=', $request->end_date)
                ->where('fin_orders.org_id', \Auth::user()->org_id)
                ->where('fin_orders_meta.is_bill_active', 0)
                ->where(function ($query) use ($request) {
                    if ($request->outlet_id) {
                        return $query->where('fin_orders.outlet_id', $request->outlet_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->user_id) {
                        return $query->where('fin_orders.user_id', $request->user_id);
                    }
                })
                ->get();
            return view('print.returnbook', compact('invoice_print', 'startdate', 'enddate', 'outlet_name'));
        } elseif ($op == 'pdf') {
            $startdate = $request->start_date;
            $enddate = $request->end_date;
            $orders_pdf = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.*')
                ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
                ->where('fin_orders.bill_date', '>=', $request->start_date)
                ->where('fin_orders.bill_date', '<=', $request->end_date)
                ->where('fin_orders.org_id', \Auth::user()->org_id)
                ->where('fin_orders_meta.is_bill_active', 0)
                ->where(function ($query) use ($request) {
                    if ($request->outlet_id) {
                        return $query->where('fin_orders.outlet_id', $request->outlet_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->user_id) {
                        return $query->where('fin_orders.user_id', $request->user_id);
                    }
                })
                ->get();

            $pdf = \PDF::loadView('pdf.returnbook', compact('orders_pdf', 'fiscal_year', 'startdate', 'enddate', 'outlet_name'))->setPaper('a4', 'landscape');
            $file = 'Report_returnbook_filtered' . date('_Y_m_d') . '.pdf';
            if (File::exists('reports/' . $file)) {
                File::Delete('reports/' . $file);
            }

            return $pdf->download($file);
        } elseif ($op == 'excel') {

            $orders_pdf = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.*')
                ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
                ->where('fin_orders.bill_date', '>=', $request->start_date)
                ->where('fin_orders.bill_date', '<=', $request->end_date)
                ->where('fin_orders.org_id', \Auth::user()->org_id)
                ->where('fin_orders_meta.is_bill_active', 0)
                ->where(function ($query) use ($request) {
                    if ($request->outlet_id) {
                        return $query->where('fin_orders.outlet_id', $request->outlet_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->user_id) {
                        return $query->where('fin_orders.user_id', $request->user_id);
                    }
                })
                ->get();
            $allData = [];
            $pos_total_amount = 0;
            $pos_taxable_amount = 0;
            $pos_tax_amount = 0;
            foreach ($orders_pdf as $key => $o) {

                if ($o->reservation_id) {
                    $hotelGuest = env('RES_CODE') . $o->reservation_id . $o->reservation->guest_name;
                } elseif ($o->folio_id) {
                    $hotelGuest = $o->folio->reservation->client->name;
                } else {
                    $hotelGuest = $o->folio->reservation->guest_name;
                }

                if ($o->pos_customer_id) {

                    $guest_pan = $o->client->vat;

                } elseif ($o->reservation_id) {
                    $guest_pan = $o->reservation->client->vat;
                } else {
                    $guest_pan = $o->folio->reservation->client->vat;
                }


                $data = [
                    'Fiscal Year' => $o->fiscal_year,
                    'Bill Date' => $o->bill_date,
                    'Ref Bill No' => $o->outlet->outlet_code . $o->bill_no,
                    'Cancel Date' => $o->cancel_date,
                    'Credit Note No' => 'CN' . $o->credit_note_no,
                    'Cancel Reason' => $o->void_reason,
                    'Hotel Guest' => $hotelGuest,
                    'POS Guest' => $o->pos_customer_id ? $o->client->name : '',
                    'Guest PAN' => $guest_pan,
                    'Total Sales' => number_format($o->total_amount, 2),
                    'Non Tax Sale' => '',
                    'Export Sale' => '',
                    'Outlet' => $o->outlet->name,
                    'Taxable Amount' => number_format($o->taxable_amount, 2),
                    'TAX' => number_format($o->tax_amount, 2)


                ];
                $allData [] = $data;

                $pos_taxable_amount = $pos_taxable_amount + $o->taxable_amount;
                $pos_total_amount = $pos_total_amount + $o->total_amount;
                $pos_tax_amount = $pos_tax_amount + $o->tax_amount;


            }

            $allData[] = [
                'Fiscal Year' => '',
                'Bill Date' => '',
                'Ref Bill No' => '',
                'Cancel Date' => '',
                'Credit Note No' => '',
                'Cancel Reason' => '',
                'Hotel Guest' => '',
                'POS Guest' => '',
                'Guest PAN' => '',
                'Total Sales' => $pos_total_amount,
                'Non Tax Sale' => '',
                'Export Sale' => '',
                'Outlet' => '',
                'Taxable Amount' => $pos_taxable_amount,
                'TAX' => $pos_tax_amount


            ];
            return \Excel::download(new \App\Exports\ExcelExport($allData), "sales_return.csv");
        }

        $request = $request->all();

        return view('admin.orders.returnpos', compact('outlets', 'orders', 'page_description', 'page_title', 'users', 'request'));
    }

    public function roomResvGuestPosBills($reservation_id)
    {
        $orders = \App\Models\Orders::orderBy('id', 'desc')
            ->where('org_id', \Auth::user()->org_id)
            ->where('reservation_id', $reservation_id)
            ->paginate(25);

        $page_title = 'Guest Bills';
        $page_description = 'Bills';

        $resv = \App\Models\Reservation::where('id', \Request::segment(4))->first();

        return view('admin.orders.guestbills', compact('orders', 'page_title', 'page_description', 'resv'));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function show($id,Request $request)
    {
        $current_fiscal = \App\Models\Fiscalyear::where('current_year', 1)->first();

        $fiscal_year = request()->fiscal_year ? request()->fiscal_year :$current_fiscal->fiscal_year;
        $prefix = '';
        if ($fiscal_year != $current_fiscal->fiscal_year) {
            $prefix = Fiscalyear::where('fiscal_year', $fiscal_year)->first()->numeric_fiscal_year . '_';
        }
        $order = new Orders();
        $abbrorder = new AbbrOrders();
        $order_details = new OrderDetail();
        $abbrorder_details = new AbbrOrderDetail();

        $new_table = $prefix . $order->getTable();
        $new_detail_table = $prefix . $order_details->getTable();
        $abbrnew_table = $prefix . $abbrorder->getTable();
        $abbrnew_detail_table = $prefix . $abbrorder_details->getTable();
        $order->setTable($new_table);
        $order_details->setTable($new_detail_table);
        $abbrorder->setTable($abbrnew_table);
        $abbrorder_details->setTable($abbrnew_detail_table);
        if (\Request::get('invoice_type') == 'abbr') {
            $ord = $abbrorder->find($id);

        }
        else if (\Request::get('type') == 'edm') {
            $ord = $this->orderproxy->find($id);
            $ord->order_type = 'EDM';
            $ord->bill_no = $ord->edm_no;

        } else {

            $ord = $order->find($id);
        }

        $page_title = 'Orders';
        $page_description = 'View Order';
        if (\Request::get('invoice_type') == 'abbr') {
            $orderDetails = $abbrorder_details->where('order_id', $ord->id)->get();

        }
        else
        $orderDetails = $order_details->where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;

        if (\Request::ajax()) {

            return view('admin.orders.showajax', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));

        }


        return view('admin.orders.show', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
    }

    public function printInvoice($id)
    {
        $ord = $this->orders->find($id);
        $orderDetails = OrderDetail::where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;

        $print_no = \App\Models\POSPrint::where('order_id', $id)->count();

        $attributes = new \App\Models\POSPrint();
        $attributes->order_id = $id;
        $attributes->printed_date = \Carbon\Carbon::now();
        $attributes->printed_by = \Auth::user()->id;
        $attributes->save();

        $ord->update(['is_bill_printed' => 1]);

        return view('admin.orders.print', compact('ord', 'imagepath', 'orderDetails', 'print_no'));
    }


    public function getInvoicePrint($id)
    {

        if (\Request::get('type') == 'proxy') {

            $ord = $this->orderproxy->find($id);
            //dd($ord);
            $orderDetails = OrderDetailProxy::where('order_id', $ord->id)->get()->groupBy('product_type_id');

            $print_no = 1;

        } else {
            if (request('invoice_type')=='abbr'){
                $ord = AbbrOrders::find($id);
                //dd($ord);
                $orderDetails = AbbrOrderDetail::where('order_id', $ord->id)->get()->groupBy('product_type_id');

                $print_no = 1;
            }
            else {

                $ord = $this->orders->find($id);
                //dd($ord);
                $orderDetails = OrderDetail::where('order_id', $ord->id)->get()->groupBy('product_type_id');

                $print_no = 1;
            }

        }

        $imagepath = \Auth::user()->organization->logo;
        $invoice_type=request('invoice_type');

        if ($ord) {
            $template = \App\Models\BillPrintTemplate::where('outlet_id', $ord->outlet_id)->get()->first();

            if (!$template) {
                Flash::warning('No Bill Template Created For This Outlet. Please Contact Support For It.');
                return \Redirect::back();
            }

            $path = resource_path('views\admin\orders');
            File::put($path . '/' . $template->name . '.blade.php', $template->description);

            $view = 'admin.orders.' . $template->name;

            return view($view, compact('ord', 'imagepath', 'orderDetails', 'print_no','invoice_type'));
        }

        return view('admin.orders.thermalprint', compact('ord', 'imagepath', 'orderDetails', 'print_no','invoice_type'));

    }


    public function thermalprint($id)
    {

        if (\Request::get('type') == 'proxy') {

            $ord = $this->orderproxy->find($id);
            $orderDetails = OrderDetailProxy::where('order_id', $ord->id)->get()->groupBy('product_type_id');

            $print_no = 0;

        } else {
            if (request('invoice_type')=='abbr'){
                $ord = AbbrOrders::find($id);
                //dd($ord);
                $orderDetails = AbbrOrderDetail::where('order_id', $ord->id)->get()->groupBy('product_type_id');

                $print_no = \App\Models\POSPrint::where('abbr_order_id', $id)->count();
            }
            else {
                $ord = $this->orders->find($id);

                $orderDetails = OrderDetail::where('order_id', $ord->id)->get()->groupBy('product_type_id');

                $print_no = \App\Models\POSPrint::where('order_id', $id)->count();
            }

        }

        $imagepath = \Auth::user()->organization->logo;
        $invoice_type=request('invoice_type');


        if ($ord) {
            $template = \App\Models\BillPrintTemplate::where('outlet_id', $ord->outlet_id)->get()->first();

            if (!$template) {
                Flash::warning('No Bill Template Created For This Outlet. Please Contact Support For It.');
                return \Redirect::back();
            }

            $path = resource_path('views\admin\orders');
            try {
                File::put($path . '/' . $template->name . '.blade.php', $template->description);

            } catch (\Exception $e) {

            }


            $view = 'admin.orders.' . $template->name;

            if ($print_no > 0) {
                Audit::log(Auth::user()->id, "Billing", "Bill is RePrinted. ID-" . $id . "");
            } else {
                Audit::log(Auth::user()->id, "Billing", "Bill is Printed. ID-" . $id . "");
            }
            //to do
            $attributes = new \App\Models\POSPrint();
            if (request('invoice_type')=='abbr'){
                $attributes->abbr_order_id = $id;
            }
            else{
                $attributes->order_id = $id;
            }
            $attributes->printed_date = \Carbon\Carbon::now();
            $attributes->printed_by = \Auth::user()->id;
            $attributes->save();

            return view($view, compact('ord', 'imagepath', 'orderDetails', 'print_no','invoice_type'));
        }

        return view('admin.orders.thermalprint', compact('invoice_type','ord', 'imagepath', 'orderDetails', 'print_no'));
    }

    public function kitchenprint($id)
    {

        if (\Request::get('type') == 'proxy') {

            $ord = $this->orderproxy->find($id);
            $orderDetails = OrderDetailProxy::where('order_id', $ord->id)->whereNotIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();
        } else {
            $ord = $this->orders->find($id);
            $orderDetails = OrderDetail::where('order_id', $ord->id)->whereNotIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();

        }

        $imagepath = \Auth::user()->organization->logo;

        $table_name = \App\Models\PosTable::find($ord->table);

        $table = $table_name->tablearea->posfloor->outlet->outlet_code . ' > ' . $table_name->tablearea->posfloor->name . ' > ' . $table_name->tablearea->name . ' > ' . $table_name->table_number;

        try {
            $connector = new NetworkPrintConnector($ord->outlet->kot_printer, $ord->outlet->kot_printer_port);
            $printer = new Printer($connector);

            $app_company = env('APP_COMPANY');
            $bill_no = $ord->outlet->outlet_code . $ord->bill_no;

            $outletname = $ord->outlet->name;
            $app_address1 = env('APP_ADDRESS1');
            $app_address2 = env('APP_ADDRESS2');

            $printer->text("{$app_company}\n{$outletname}-KOT\n Bill no:{$bill_no}\nTable : {$table}\n{$app_address1}\n{$app_address2}\n");
            $printer->text("--------------------------------------------\n");
            $printer->text("Particular                          QTY\n");
            foreach ($orderDetails as $odv) {

                if ($odv->is_inventory == 1) {
                    $printer->text("{$odv->product->name}({$odv->remarks})                  {$odv->quantity}\n");
                } else {
                    $printer->text("{$odv->description}({$odv->remarks})                 {$odv->quantity}\n");
                }
            }
            $printer->text("--------------------------------------------\n");
            $printer->text("\n");
            $printer->text("\n");
            $printer->cut();
            $printer->close();
        } catch (\Exception $e) {
            return view('admin.orders.kitchenprint', compact('ord', 'imagepath', 'orderDetails', 'table'));
        }

        return view('admin.orders.kitchenprint', compact('ord', 'imagepath', 'orderDetails', 'table'));
    }

    public function botprint($id)
    {

        $ord = $this->orderproxy->find($id);

        $orderDetails = OrderDetailProxy::where('order_id', $ord->id)->whereIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();

        $imagepath = \Auth::user()->organization->logo;

        $table_name = \App\Models\PosTable::find($ord->table);

        $table = $table_name->tablearea->posfloor->outlet->outlet_code . ' > ' . $table_name->tablearea->posfloor->name . ' > ' . $table_name->tablearea->name . ' > ' . $table_name->table_number;

        try {
            $connector = new NetworkPrintConnector($ord->outlet->bot_printer, $ord->outlet->bot_printer_port);
            $printer = new Printer($connector);

            $app_company = env('APP_COMPANY');
            $bill_no = $ord->outlet->outlet_code . $ord->bill_no;

            $outletname = $ord->outlet->name;
            $app_address1 = env('APP_ADDRESS1');
            $app_address2 = env('APP_ADDRESS2');

            $printer->text("{$app_company}\n{$outletname}-BOT\nBill No:{$bill_no}\nTable : {$table}\n{$app_address1}\n{$app_address2}\n");
            $printer->text("---------------------------------\n");
            $printer->text("Particular                          QTY\n");

            foreach ($orderDetails as $odv) {

                if ($odv->is_inventory == 1) {
                    $printer->text("{$odv->product->name} ({$odv->remarks})                 {$odv->quantity}\n");
                } else {
                    $printer->text("{$odv->description}({$odv->remarks})                 {$odv->quantity}\n");
                }
            }

            $printer->text("--------------------------------------------\n");
            $printer->text("\n");
            $printer->text("\n");
            $printer->cut();
            $printer->close();
        } catch (\Exception $e) {

            return view('admin.orders.botprint', compact('ord', 'imagepath', 'orderDetails', 'table'));
        }

        return view('admin.orders.botprint', compact('ord', 'imagepath', 'orderDetails', 'table'));
    }

    public function estimateprint($id)
    {

        $ord = $this->orders->find($id);
        $orderDetails = OrderDetail::where('order_id', $ord->id)->get();

        return view('admin.orders.estimateprint', compact('ord', 'imagepath', 'orderDetails', 'table'));
    }

    public function copyDoc($id)
    {

        $order = Orders::where('id', $id)->first();

        $new_item = $order->replicate(); //copy attributes
        $new_item->push();

        Flash::success('Quotation successfully duplicated.');
        return redirect('/admin/orders?type=quotation');
    }

    public function generatePDF($id)
    {
        $ord = $this->orders->find($id);
        $orderDetails = OrderDetail::where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;

        $pdf = \PDF::loadView('admin.orders.generateInvoicePDF', compact('ord', 'imagepath', 'orderDetails'));
        $file = $id . '_' . $ord->name . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';

        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }
        return $pdf->download($file);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
//        dd(\App\Models\OrderProxy::find(1));

        $page_title = 'Orders';
        $page_description = 'Add Orders';
        $order = NULL;
        $orderDetail = NULL;
        $products = Product::select('id', 'name')->get();
        $users = \App\User::where('enabled', '1')->where('org_id', \Auth::user()->org_id)->pluck('first_name', 'id');
        $last_order = Orders::orderBy('id', 'Desc')->first();

        $selectedOutlet = \App\Models\PosOutlets::find(\Request::get('outlet_id'));

        $productlocation = \App\Models\ProductLocation::pluck('location_name', 'id')->all();
        $clients = Client::select('id', 'name', 'location')->orderBy('id', 'DESC')->get();

        $reservation = \App\Models\Reservation::select('id', 'room_num', 'guest_id', 'guest_name')->orderBy('id', 'desc')->whereIn('reservation_status', [3])->get();

        $outlet_id = \Request::get('outlet_id');

        $order_no = \App\Models\OrderProxy::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->first();

        $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first();

        $bill_no = OrderProxy::select('bill_no')
            ->where('fiscal_year', $fiscal_year->fiscal_year)
            ->where('outlet_id', $outlet_id)
            ->orderBy('bill_no', 'desc')
            ->first();
//        $bill_no = \App\Models\OrderProxy::first();

        $bill_no = ($bill_no->bill_no ?? 0) + 1;

        $productscategory = \App\Models\ProductCategory::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->where('enabled', '1')->get();

        return view('admin.orders.create', compact('page_title', 'users', 'productlocation', 'page_description', 'order', 'orderDetail', 'products', 'clients', 'last_order', 'outlet_id', 'reservation', 'productscategory', 'bill_no', 'selectedOutlet'));
    }

    public function store(Request $request)
    {

        $business_date = \App\Models\BusinessDate::first();

//        if (!$business_date || ($business_date && $business_date->business_date != date('Y-m-d'))) {
//            Flash::error("Business Date is not configured");
//            return redirect()->back();
//        }
        $request->validate(['invoice_type'=>'required|string']);
        DB::beginTransaction();
        $order_attributes = $request->all();
        if (count($order_attributes['product_id']) == 0) {
            Flash::error("Please add atleast one product");
            return redirect()->back();
        }
        if ($request->taxable_amount > 10000 && $request->invoice_type=='abbr') {
            Flash::error("Abbreviated Invoice cannot be created. Total taxable amount is greater than Rs. 10,000.");
            return redirect()->back();
        }
        $order_attributes['user_id'] = \Auth::user()->id;
        $order_attributes['org_id'] = \Auth::user()->org_id;
        $order_attributes['user_id'] = \Auth::user()->id;
        $order_attributes['tax_amount'] = $request->taxable_tax;
        $order_attributes['total_amount'] = $request->final_total;
        $order_attributes['non_taxable_amount'] = $request->non_taxable_amount;
        $order_attributes['discount_amount'] = $request->discount_amount;
        $order_attributes['bill_date']= BusinessDate::first()->business_date;

        $ckfiscalyear = \App\Models\Fiscalyear::where('current_year', '1')
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->first();
        if (!$ckfiscalyear)
            return \Redirect::back()->withErrors(['Please update fiscal year <a href="/admin/fiscalyear/create">Click Here</a>!']);

        $order_attributes['fiscal_year'] = \App\Models\Fiscalyear::where('current_year', 1)->first()->fiscal_year;

        $order = $this->orderproxy->create($order_attributes);

        $product_id = $request->product_id;
        $price = $request->price;
        $quantity = $request->quantity;
        $tax = $request->includes_tax;
        $tax_amount = $request->tax_amount;
        $total = $request->total;
        $remarks = $request->remarks;
//        $discount_amount = $request->discount_amount;
//        $discount_percent = $request->discount_percent;

        foreach ($product_id as $key => $value) {
            if ($value != '') {
                $detail = new OrderDetailProxy();
                $detail->order_id = $order->id;

                $temp_product = \App\Models\Product::where('name', $product_id[$key])->first();
                $detail->product_id = $temp_product->id;
                $detail->product_type_id = $temp_product->product_type_id;

                $detail->price = $price[$key];
                $detail->quantity = $quantity[$key];
                $detail->remarks = $remarks[$key];
                $detail->tax = $tax[$key] ?? null;
                $detail->tax_amount = $tax_amount[$key] ?? '';
//                $detail->discount_percent = $discount_percent[$key];
//                $detail->discount_amount = $discount_amount[$key];

                $detail->total = $total[$key];
                $detail->date = date('Y-m-d H:i:s');
                $detail->is_inventory = 1;
                $detail->save();

                $stockMove = new StockMove();
                $stockMove->stock_id = $temp_product->id;
                $stockMove->tran_date = \Carbon\Carbon::now();
                $stockMove->user_id = \Auth::user()->id;
                $stockMove->reference = 'store_out_' . $order->id;
                $stockMove->transaction_reference_id = $order->id;
                $stockMove->qty = '-' . $quantity[$key];
                $stockMove->trans_type = SALESINVOICE;
                $stockMove->order_no = $order->id;
                $stockMove->price = $price[$key];
                $stockMove->location = $request->from_stock_location;
                $stockMove->order_reference = $order->id;
                $stockMove->save();
            }
        }

        // Custom items generally not enabled
        $tax_id_custom = $request->custom_tax_amount;
        $custom_items_name = $request->custom_items_name;
        $custom_items_rate = $request->custom_items_rate;
        $custom_items_qty = $request->custom_items_qty;
        $custom_items_price = $request->custom_items_price;

        $custom_tax_amount = $request->custom_tax_amount;
        $custom_total = $request->custom_total;
        $remarks_custom = $request->remarks_custom;

        foreach ($custom_items_name ?? [] as $key => $value) {
            if ($value != '') {
                $detail = new OrderDetailProxy();
                $detail->order_id = $order->id;
                $detail->description = $custom_items_name[$key];

                $detail->price = $custom_items_price[$key];
                $detail->quantity = $custom_items_qty[$key];
                $detail->remarks = $remarks_custom[$key];
                $detail->tax = $tax_id_custom[$key];
                $detail->tax_amount = $custom_tax_amount[$key];
                $detail->total = $custom_total[$key];
                $detail->date = date('Y-m-d H:i:s');
                $detail->is_inventory = 0;
                $detail->save();
            }
        }

        $ord = $order;
        $orderDetails = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->get();

        $foods = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->whereNotIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();
        $beverages = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->whereIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();
        $bill_no = $ord->outlet->outlet_code . $ord->bill_no;

        //NOW start printing KOT and BOT based on IP address
        DB::commit();
        if ($ord->outlet->fnb_outlet) {
            if (count($foods) > 0) {
                try {
                    $connector = new NetworkPrintConnector($ord->outlet->kot_printer, $ord->outlet->kot_printer_port);
                    $printer = new Printer($connector);
                    $app_company = env('APP_COMPANY');

                    $table_name = \App\Models\PosTable::find($ord->table);

                    $table = $table_name->tablearea->posfloor->outlet->outlet_code . ' > ' . $table_name->tablearea->posfloor->name . ' > ' . $table_name->tablearea->name . ' > ' . $table_name->table_number;


                    $outletname = $ord->outlet->name;
                    $app_address1 = env('APP_ADDRESS1');
                    $app_address2 = env('APP_ADDRESS2');
                    $print_time = date('Y-m-d h:i');

                    $printer->text("{$app_company}\n{$outletname}-KOT\nBill No:{$bill_no}\nTable : {$table}\n{$app_address1}\n{$app_address2}\n{$print_time}\n");
                    $printer->text("--------------------------------------------\n");
                    $printer->text("Particular------------QTY\n");
                    foreach ($foods as $odv) {

                        if ($odv->is_inventory == 1) {
                            $printer->text("{$odv->product->name} ({$odv->remarks}) ---------- {$odv->quantity}\n");
                        } else {
                            $printer->text("{$odv->description} ({$odv->remarks}) --------- {$odv->quantity}\n");
                        }

                        $odv->update(['is_printed_kot_bot' => 1]);
                    }
                    $printer->text("--------------------------------------------\n");
                    $printer->text("\n");
                    $printer->text("\n");
                    $printer->cut();
                } catch (\Exception $e) {
                    Flash::error('Order created Successfully. But failed to print KOT');
                }
            }

            if (count($beverages) > 0) {

                try {
                    $connector = new NetworkPrintConnector($ord->outlet->bot_printer, $ord->outlet->bot_printer_port);
                    $printer = new Printer($connector);

                    $app_company = env('APP_COMPANY');

                    $table_name = \App\Models\PosTable::find($ord->table);

                    $table = $table_name->tablearea->posfloor->outlet->outlet_code . ' > ' . $table_name->tablearea->posfloor->name . ' > ' . $table_name->tablearea->name . ' > ' . $table_name->table_number;

                    $outletname = $ord->outlet->name;
                    $app_address1 = env('APP_ADDRESS1');
                    $app_address2 = env('APP_ADDRESS2');
                    $print_time = date('Y-m-d h:i');

                    $printer->text("{$app_company}\n{$outletname}-BOT\nBill No:{$bill_no}\nTable : {$table}\n{$app_address1}\n{$app_address2}\n{$print_time}\n");
                    $printer->text("--------------------------------------------\n");
                    $printer->text("Particular----------- QTY\n");
                    foreach ($beverages as $odv) {
                        if ($odv->is_inventory == 1) {
                            $printer->text("{$odv->product->name}({$odv->remarks})-------{$odv->quantity}\n");
                        } else {
                            $printer->text("{$odv->description}({$odv->remarks})---------{$odv->quantity}\n");
                        }
                        $odv->update(['is_printed_kot_bot' => 1]);
                    }
                    $printer->text("--------------------------------------------\n");
                    $printer->text("\n");
                    $printer->text("\n");
                    $printer->cut();
                    $printer->close();
                } catch (\Exception $e) {
                    Flash::warning('Order created Successfully. But failed to print BOT');

                    if ($ord->outlet->fnb_outlet) {
                        return redirect('/admin/hotel/showtablelists?type=invoice&outlet_id=' . $request->outlet_id);
                    } else {
                        return redirect('/admin/orders/outlet/' . $request->outlet_id);
                    }
                }
            }
        }

        Flash::success('New Order created Successfully.');

        if ($ord->outlet->fnb_outlet) {
            return redirect('/admin/hotel/showtablelists?type=invoice&outlet_id=' . $request->outlet_id);
        } else {
            return redirect('/admin/orders/outlet/' . $request->outlet_id);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $page_title = 'Orders';
        $page_description = 'Edit Orders';
        $order = OrderProxy::where('id', $id)->first();
        $orderDetails = OrderDetailProxy::where('order_id', $order->id)->get();
        $selectedOutlet = \App\Models\PosOutlets::find($order->outlet_id);
        $products = Product::select('id', 'name')->get();
        // $clients = Client::select('id', 'name', 'location')->get();
        $clients = Client::select('id', 'name', 'location')->orderBy('id', 'DESC')->get();

        $users = \App\User::where('enabled', '1')->where('org_id', \Auth::user()->org_id)->pluck('first_name', 'id');

        $productlocation = \App\Models\ProductLocation::where('enabled', '1')->pluck('location_name', 'id')->all();

        $reservation = \App\Models\Reservation::select('id', 'room_num', 'guest_name')->orderBy('id', 'desc')->whereIn('reservation_status', [3])->get();

        $productscategory = \App\Models\ProductCategory::orderBy('id', 'desc')->where('outlet_id', $order->outlet_id)->where('enabled', '1')->get();

        $menus = \App\Models\PosMenu::where('outlet_id', $order->outlet_id)->where('enabled', '1')->pluck('menu_name', 'id')->all();

        return view('admin.orders.edit', compact('page_title', 'users', 'page_description', 'productlocation', 'order', 'orderDetails', 'products', 'clients', 'reservation', 'productscategory', 'menus', 'selectedOutlet'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function canCombineLineProduct($orderId, $given_prod_id, $qty)
    { //anilbro  changes


        $checkProduct = OrderDetailProxy::where('order_id', $orderId)
            ->where('product_id', $given_prod_id)
            ->first();


        if ($checkProduct) {


            $checkProduct->update([
                'quantity' => $checkProduct->quantity + $qty,
                'total' => $checkProduct->total + $checkProduct->price * $qty,
            ]);

            return false;

        }

        return true;


    }

    public function posItemsToKotBot($id, $addedKotBot, $remarks = 'Running')
    {


        $produts_ids = array_column($addedKotBot, 'product_id');


        $ord = $this->orderproxy->find($id);

        $foods = array_filter($addedKotBot, function ($var) {

            if (!in_array($var['produt_type'], [2, env('BWS_PRODUCT_TYPE_ID')])) {

                return true;
            }
            return false;
        });

        $beverages = array_filter($addedKotBot, function ($var) {

            if (in_array($var['produt_type'], [2, env('BWS_PRODUCT_TYPE_ID')])) {

                return true;
            }
            return false;
        });

        // $foods = \App\Models\Product::where('order_id', $ord->id)->whereNotIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();
        // $beverages = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->whereIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->get();
        if (count($foods) > 0) {
            try {
                $connector = new NetworkPrintConnector($ord->outlet->kot_printer, $ord->outlet->kot_printer_port);
                $printer = new Printer($connector);
                $app_company = env('APP_COMPANY');

                $table_name = \App\Models\PosTable::find($ord->table);

                $table = $table_name->tablearea->posfloor->outlet->outlet_code . ' > ' . $table_name->tablearea->posfloor->name . ' > ' . $table_name->tablearea->name . ' > ' . $table_name->table_number;


                $outletname = $ord->outlet->name;
                $app_address1 = env('APP_ADDRESS1');
                $app_address2 = env('APP_ADDRESS2');
                $print_time = date('Y-m-d h:i');

                $printer->text("{$app_company}\n{$outletname}-KOT\nBill No:{$bill_no}\nTable : {$table}\n{$app_address1}\n{$app_address2}\n{$print_time}\n");
                $printer->text("\n");
                $printer->text("{$remarks} Order");
                $printer->text("--------------------------------------------\n");
                $printer->text("Particular------------QTY\n");
                foreach ($foods as $odv) {
                    $odv = (object)$odv;
                    if ($odv->is_inventory == 1) {
                        $printer->text("{$odv->name} {$remarks}- ( {$odv->remarks} )  ---------- {$odv->qty}\n");
                    } else {
                        $printer->text("{$odv->name} {$remarks}- ({$odv->remarks}) --------- {$odv->qty}\n");
                    }

                    // $odv->update(['is_printed_kot_bot' => 1]);
                }

                $printer->text("--------------------------------------------\n");
                $printer->text("\n");
                $printer->text("\n");
                $printer->cut();
            } catch (\Exception $e) {
                Flash::error('But failed to print KOT');
            }
        }

        if (count($beverages) > 0) {

            try {
                $connector = new NetworkPrintConnector($ord->outlet->bot_printer, $ord->outlet->bot_printer_port);
                $printer = new Printer($connector);

                $app_company = env('APP_COMPANY');

                $table_name = \App\Models\PosTable::find($ord->table);

                $table = $table_name->tablearea->posfloor->outlet->outlet_code . ' > ' . $table_name->tablearea->posfloor->name . ' > ' . $table_name->tablearea->name . ' > ' . $table_name->table_number;

                $outletname = $ord->outlet->name;
                $app_address1 = env('APP_ADDRESS1');
                $app_address2 = env('APP_ADDRESS2');
                $print_time = date('Y-m-d h:i');

                $printer->text("{$app_company}\n{$outletname}-BOT\nBill No:{$bill_no}\nTable : {$table}\n{$app_address1}\n{$app_address2}\n{$print_time}\n");
                $printer->text("--------------------------------------------\n");
                $printer->text("Particular----------- QTY\n");
                foreach ($beverages as $odv) {
                    $odv = (object)$odv;
                    if ($odv->is_inventory == 1) {
                        $printer->text("{$odv->name} {$remarks}-- ( {$odv->remarks} )---------- {$odv->qty}\n");
                    } else {
                        $printer->text("{$odv->name} {$remarks}-- ( {$odv->remarks} )--------- {$odv->qty}\n");
                    }
                }
                $printer->text("--------------------------------------------\n");
                $printer->text("\n");
                $printer->text("\n");
                $printer->cut();
                $printer->close();
            } catch (\Exception $e) {
                Flash::warning('Order failed to print BOT');

                if ($ord->outlet->fnb_outlet) {
                    return redirect('/admin/hotel/showtablelists?type=invoice&outlet_id=' . $request->outlet_id);
                } else {
                    return redirect('/admin/orders/outlet/' . $request->outlet_id);
                }
            }
        }
    }

    private function detectNewProduct($previousProduct, $id)
    {

        $allProduts = OrderDetailProxy::where('order_id', $id)->get();

        $p = count($allProduts); //present
        $n = count($previousProduct); //new
        //dd($previousProduct);

        $letsLoop = $p > $n ? $allProduts : $previousProduct;

        $newItemsArray = [];
        foreach ($letsLoop as $key => $value) {

            $previousItem = $previousProduct->where('product_id', $value->product_id)->sum('quantity');

            $newItem = $allProduts->where('product_id', $value->product_id)->sum('quantity');

            $remarks = $allProduts->where('product_id', $value->product_id)->first()->remarks ?? '';


            if ($newItem > $previousItem) { //item updated

                $newItemsArray[] = [

                    'name' => $value->product->name,
                    'qty' => ($newItem - $previousItem),
                    'product_id' => $value->product_id,
                    'produt_type' => $value->product->product_type_id,
                    'remarks' => $remarks,

                ];


            }
        }
        $syncRemovedProd = [];
        foreach ($previousProduct as $key => $value) {
            $currentProd = $allProduts->where('product_id', $value->product_id)->sum('quantity');
            $prevProd = $value->quantity;
            $total = $prevProd - $currentProd;
            $remarks = $allProduts->where('product_id', $value->product_id)->first()->remarks ?? 'Deleted';
            if ($total > 0) {

                $syncRemovedProd [] = [

                    'name' => $value->product->name,
                    'qty' => $total,
                    'product_id' => $value->product_id,
                    'produt_type' => $value->product->product_type_id,
                    'remarks' => '',


                ];

            }
        }

        $this->posItemsToKotBot($id, $newItemsArray);
        $this->posItemsToKotBot($id, $syncRemovedProd, 'Deleted');
        return $newItemsArray;

    }


    public function update(Request $request, $id)
    {

        $request->validate(['invoice_type'=>'required|string']);

        DB::beginTransaction();

        if ($request->ajax() && $request->op == 'status') {
            $order = $this->orderproxy->find($id);
            $order->update(['ready_status' => $request->value]);
            return 1;
        }

        if (count($request['product_id']) == 0 && count($request['product_id_new']) == 0) {
            Flash::error("Please add at-least one product");
            return redirect()->back();
        }
        if ($request->taxable_amount > 10000 && $request->invoice_type=='abbr') {
            Flash::error("Abbreviated Invoice cannot be created. Total taxable amount is greater than Rs. 10,000.");
            return redirect()->back();
        }
        $this->validate($request, []);


        $order = $this->orderproxy->find($id);

        if ($order->isEditable()) {
            $this->record_histroy($order); //add record to histoy
            $order_attributes = $request->all();
            $order_attributes['org_id'] = \Auth::user()->org_id;
            $order_attributes['user_id'] = \Auth::user()->id;
            $order_attributes['tax_amount'] = $request->taxable_tax;
            $order_attributes['total_amount'] = $request->final_total;
            $order_attributes['non_taxable_amount'] = $request->non_taxable_amount;
            $order_attributes['discount_amount'] = $request->discount_amount;


            $order->update($order_attributes);
            Audit::log(Auth::user()->id, "Billing", "Bill Is Updated: ID-" . $order->id . "");
            $orderdetail = OrderDetailProxy::where('order_id', $order->id)->get();
            foreach ($orderdetail as $od) {

                $stockmove = StockMove::where('reference', 'store_out_' . $order->id)->where('stock_id', $od->product_id)->delete();
            }

            $currentProduts = OrderDetailProxy::where('order_id', $order->id)
                ->select('product_id', 'quantity', 'remarks')
                ->get();

            OrderDetailProxy::where('order_id', $order->id)->forceDelete();

            $product_id = $request->product_id;
            $price = $request->price;
            $quantity = $request->quantity;

            $tax = $request->includes_tax;
            $tax_amount = $request->tax_amount;
            $total = $request->total;
            $remarks = $request->remarks;
            $is_printed_kot_bot = $request->is_printed_kot_bot;
//            $discount_amount = $request->discount_amount;
//            $discount_percent = $request->discount_percent;
            foreach ($product_id as $key => $value) {
                if ($value != '') {
                    $detail = new OrderDetailProxy();
                    $detail->order_id = $order->id;

                    $temp_product = \App\Models\Product::where('name', $product_id[$key])->first();
                    if ($this->canCombineLineProduct($order->id,
                        $temp_product->id,
                        $quantity[$key])) {
                        $detail->product_id = $temp_product->id;
                        $detail->product_type_id = $temp_product->product_type_id;

                        $detail->price = $price[$key];
                        $detail->quantity = $quantity[$key];
//                        $detail->discount_percent = $discount_percent[$key];
//                        $detail->discount_amount = $discount_amount[$key];
                        $detail->remarks = $remarks[$key];
                        $detail->tax = $tax[$key] ?? null;
                        $detail->tax_amount = $tax_amount[$key] ?? null;
                        $detail->total = $total[$key];

                        $detail->is_inventory = 1;
                        $detail->is_printed_kot_bot = $is_printed_kot_bot[$key] ?? null;
                        $detail->date = date('Y-m-d H:i:s');
                        $detail->save();
                    }


                    // create stockMove
                    $stockMove = new StockMove();
                    $stockMove->stock_id = $temp_product->id;
                    $stockMove->tran_date = \Carbon\Carbon::now();
                    $stockMove->user_id = \Auth::user()->id;
                    $stockMove->reference = 'store_out_' . $order->id;
                    $stockMove->transaction_reference_id = $order->id;
                    $stockMove->qty = '-' . $quantity[$key];
                    $stockMove->trans_type = SALESINVOICE;
                    $stockMove->order_no = $order->id;
                    $stockMove->price = $price[$key];
                    $stockMove->location = $request->from_stock_location;
                    $stockMove->order_reference = $order->id;
                    $stockMove->save();
                }
            }

            $description_custom = $request->description_custom;
            $price_custom = $request->price_custom;
            $quantity_custom = $request->quantity_custom;
            $remarks_custom = $request->remarks_custom;
            $tax_custom = $request->tax_custom;
            $tax_amount_custom = $request->tax_amount_custom;
            $total_custom = $request->total_custom;
            $is_printed_kot_bot_custom = $request->is_printed_kot_bot_custom;

            foreach ($description_custom ?? [] as $key => $value) {
                if ($value != '') {
                    $detail = new OrderDetailProxy();
                    $detail->order_id = $order->id;
                    $detail->description = $description_custom[$key];
                    $detail->price = $price_custom[$key];
                    $detail->quantity = $quantity_custom[$key];
                    $detail->remarks = $remarks_custom[$key];
                    $detail->tax = $tax_custom[$key];
                    $detail->tax_amount = $tax_amount_custom[$key];
                    $detail->total = $total_custom[$key];
                    $detail->is_inventory = 0;
                    $detail->is_printed_kot_bot = $is_printed_kot_bot_custom;
                    $detail->date = date('Y-m-d H:i:s');
                    $detail->save();
                }
            }


            if ($request->product_id_new != null) {
                $product_id_new = $request->product_id_new;
                $ticket_new = $request->ticket_new;
                $price_new = $request->price_new;
                $quantity_new = $request->quantity_new;
                $flight_date_new = $request->flight_date_new;
                $tax_new = $request->includes_tax_new;
                $tax_amount_new = $request->tax_amount_new;
                $total_new = $request->total_new;
                $remarks_new = $request->remarks_new;
//                $discount_amount_new = $request->discount_amount_new;
//                $discount_percent_new = $request->discount_percent_new;

                foreach ($product_id_new as $key => $value) {

                    $detail = new OrderDetailProxy();


                    $detail->order_id = $order->id;
                    $temp_product_new = \App\Models\Product::where('name', $product_id_new[$key])->first();
                    if ($this->canCombineLineProduct($order->id,
                        $temp_product_new->id, $quantity_new[$key])) {
                        //we will merge added product to old one
                        $detail->product_id = $temp_product_new->id;
                        $detail->product_type_id = $temp_product_new->product_type_id;

                        $detail->price = $price_new[$key];
                        $detail->quantity = $quantity_new[$key];
                        $detail->tax = $tax_new[$key];
                        $detail->tax_amount = $tax_amount_new[$key];
//                        $detail->discount_percent = $discount_percent_new[$key];
//                        $detail->discount_amount = $discount_amount_new[$key];
                        $detail->total = $total_new[$key];
                        $detail->remarks = $remarks_new[$key];
                        $detail->is_inventory = 1;
                        $detail->date = date('Y-m-d H:i:s');


                        $detail->save();

                    }


                    $stockMove = new StockMove();
                    $stockMove->stock_id = $temp_product_new->id;
                    $stockMove->tran_date = \Carbon\Carbon::now();
                    $stockMove->user_id = \Auth::user()->id;
                    $stockMove->reference = 'store_out_' . $order->id;
                    $stockMove->transaction_reference_id = $order->id;
                    $stockMove->qty = '-' . $quantity_new[$key];
                    $stockMove->trans_type = SALESINVOICE;
                    $stockMove->order_no = $order->id;
                    $stockMove->location = $request->from_stock_location;
                    $stockMove->order_reference = $order->id;
                    $stockMove->save();
                }
            }

            // Custom items Start
            $custom_items_name_new = $request->custom_items_name_new;
            $custom_ticket_new = $request->custom_ticket_new;
            $custom_items_price_new = $request->custom_items_price_new;
            $custom_items_qty_new = $request->custom_items_qty_new;
            $custom_flight_date_new = $request->custom_flight_date_new;
            $tax_id_custom_new = $request->tax_id_custom_new;
            $custom_tax_amount_new = $request->custom_tax_amount_new;
            $custom_total_new = $request->custom_total_new;

            if (!empty($custom_items_name_new)) {

                foreach ($custom_items_name_new as $key => $value) {

                    $detail = new OrderDetailProxy();
                    $detail->order_id = $order->id;
                    $detail->description = $custom_items_name_new[$key];
                    $detail->price = $custom_items_price_new[$key];
                    $detail->quantity = $custom_items_qty_new[$key];
                    $detail->tax = $tax_id_custom_new[$key];
                    $detail->tax_amount = $custom_tax_amount_new[$key];
                    $detail->total = $custom_total_new[$key];
                    $detail->is_inventory = 0;
                    $detail->date = date('Y-m-d H:i:s');
                    $detail->save();
                }
            }


            $ord = $this->orderproxy->find($id);
            $orderDetails = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->get();

            $foods = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->whereNotIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])->where('is_printed_kot_bot', '!=', 1)->get();
            $beverages = \App\Models\OrderDetailProxy::where('order_id', $ord->id)->whereIn('product_type_id', [2, env('BWS_PRODUCT_TYPE_ID')])
                ->where('is_printed_kot_bot', '!=', 1)->get();

            $bill_no = $ord->outlet->outlet_code . $ord->bill_no;


            Flash::success('Order updated Successfully.');
        } else
            Flash::success('Error in updating Order.');

        $newProdutcs = $this->detectNewProduct($currentProduts, $id);
        DB::commit();
        return \Redirect::back()->withInput(\Request::all());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {

        $orders = $this->orderproxy->find($id);

        if (!$orders->isdeletable()) {
            abort(403);
        }
        \DB::beginTransaction();
        $orderdetail = OrderDetailProxy::where('order_id', $orders->id)->get();

        foreach ($orderdetail as $od) {

            $stockmove = StockMove::where('reference', 'store_out_' . $orders->id)->where('stock_id', $od->product_id)->delete();
        }

        $mergedTable = $this->orderproxy->where('merged_to', $id)->pluck('id')->toArray();

        $orders->delete();


        $this->orderproxy->whereIn('id', $mergedTable)->delete();

        OrderDetailProxy::whereIn('order_id', $mergedTable)->delete();

        OrderDetailProxy::where('order_id', $id)->delete();

        MasterComments::where('type', 'orders')->where('master_id', $id)->delete();
        \DB::commit();

        Flash::success('Order successfully deleted.');

        if (\Request::get('type'))
            return redirect('/admin/orders?type=' . \Request::get('type'));

        return redirect('/admin/hotel/openresturantoutlets');
    }

    public function getProductDetailAjax($productId)
    {
        if (\Request::get('term')) {


            $product = Product::select('id', 'name', 'price','includes_tax','tax_amount', 'cost')->where('name','like', \Request::get('term'))->first();

        } else {

            $product = Product::select('id', 'name', 'price','includes_tax','tax_amount', 'cost', 'product_unit')->with('unit:id,symbol')->where('id', $productId)->first();

        }


        return ['data' => json_encode($product)];
    }


    /**
     * Delete Confirm
     *
     * @param int $id
     * @return  View
     */
    public function getModalDelete($id)
    {
        $error = null;

        $orders = $this->orderproxy->find($id);

        if (!$orders->isdeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Order';

        $orders = $this->orderproxy->find($id);

        if (\Request::get('type'))
            $modal_route = route('admin.orders.delete', $orders->id) . '?type=' . \Request::get('type');
        else
            $modal_route = route('admin.orders.delete', $orders->id);

        $modal_body = 'Are you sure you want to delete this order?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }


    /**
     * @param Request $request
     * @return array|static[]
     */
    public function searchByName(Request $request)
    {
        $return_arr = null;

        $query = $request->input('query');

        $orders = $this->orders->pushCriteria(new ordersWhereDisplayNameLike($query))->all();

        foreach ($orders as $orders) {
            $id = $orders->id;
            $name = $orders->name;
            $email = $orders->email;

            $entry_arr = ['id' => $id, 'text' => "$name ($email)"];
            $return_arr[] = $entry_arr;
        }

        return $return_arr;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getInfo(Request $request)
    {
        $id = $request->input('id');
        $orders = $this->orders->find($id);

        return $orders;
    }

    public function get_client()
    {
        $term = strtolower(\Request::get('term'));
        $contacts = ClientModel::select('id', 'name')->where('name', 'LIKE', '%' . $term . '%')->groupBy('name')->take(5)->get();
        $return_array = array();

        foreach ($contacts as $v) {
            if (strpos(strtolower($v->name), $term) !== FALSE) {
                $return_array[] = array('value' => $v->name, 'id' => $v->id);
            }
        }
        return \Response::json($return_array);
    }


    public function ajaxGetProductcategory(Request $request)
    {

        if ($request->menu_id != '') {

            $productcategory = \App\Models\ProductCategory::orderBy('id', 'desc')->where('outlet_id', $request->outlet_id)->where('menu_id', $request->menu_id)->where('enabled', '1')->get();
        } else {

            $productcategory = \App\Models\ProductCategory::orderBy('id', 'desc')->where('outlet_id', $request->outlet_id)->where('enabled', '1')->get();
        }

        $data = '<option value="">Select Product category...</option>';

        foreach ($productcategory as $key => $value) {

            $data .= '<option value="' . $value->id . '">' . $value->name . '</option>';
        }

        return ['success' => 1, 'data' => $data];
    }


    public function getProduct(Request $request)
    {

        $outlet_id = $request->outlet_id;

        if ($request->category_id) {
            $products = \App\Models\Product::where('category_id', $request->category_id)->where('outlet_id', $outlet_id)->where('enabled', '1')->get();
        } else {
            $products = \App\Models\Product::where('outlet_id', $outlet_id)->where('enabled', '1')->get();
        }

        $organization = \App\Models\Organization::find(\Auth::user()->org_id);

        $data = '';

        foreach ($products as $key => $value) {

            $name = $value->name;
            if (strlen($name) >= 15) {
                $name = substr($value->name, 0, 25) . (strlen($value->name)>25?'...':'');
            }
            $price = $value->price;
            $data .= '<a href="javascript:void(0)" class="product-title" onclick="AddMore(this)";
                        data-includes_tax="' . $value->includes_tax . '"data-tax_amount="' . $value->tax_amount . '"data-name="' . $value->name . '" data-price="' . $price . '" data-id=' . $value->id . ' >
                        <div class="col-md-6 col-xs-6" title="' . $value->name . '">
                        <div class="small-box" style="background-color: #34495e; color:#fff;" >
                        <div class="inner">
                        <h4>' . ucfirst($name) . '</h4>
                        </div>
                        </div>
                        </div>
                        </a>';
        }
        return ['success' => 1, 'data' => $data];
    }


    public function allPosInvoice()
    {

        $limit = 30;
        $current_fiscal = \App\Models\Fiscalyear::where('current_year', 1)->first();
        $fiscal_year = \Request::get('fiscal_year') ? \Request::get('fiscal_year') : $current_fiscal->fiscal_year;
        $allFiscalYear = \App\Models\Fiscalyear::pluck('fiscal_year', 'fiscal_year')->all();

        $prefix = '';
        if ($fiscal_year != $current_fiscal->fiscal_year) {
            $prefix = Fiscalyear::where('fiscal_year', $fiscal_year)->first()->numeric_fiscal_year . '_';
        }
        $ord = new Orders();
        $abbrord = new AbbrOrders();
        $new_table = $prefix . $ord->getTable();
        $abbrnew_table = $prefix . $abbrord->getTable();
        $ord->setTable($new_table);
        $abbrord->setTable($abbrnew_table);

        if (\Request::get('invoice_type')=='abbr'){
            $orders = $abbrord->select($prefix . 'abbr_fin_orders.*', $prefix . 'abbr_fin_orders_meta.credit_note_no', $prefix . 'abbr_fin_orders_meta.settlement', $prefix . 'abbr_fin_orders_meta.is_bill_active')
                ->with('payments')->where(function ($query) use ($prefix) {
                    $start_date = \Request::get('start_date');
                    $end_date = \Request::get('end_date');
                    if ($start_date && $end_date) {

                        return $query->where($prefix . 'abbr_fin_orders.bill_date', '>=', $start_date)
                            ->where($prefix . 'abbr_fin_orders.bill_date', '<=', $end_date);
                    }
                })->where(function ($query) use ($prefix) {

                    $outlet_id = \Request::get('outlet_id');
                    if ($outlet_id) {

                        return $query->where($prefix . 'abbr_fin_orders.outlet_id', $outlet_id);
                    }
                })
                ->where(function ($query) use ($prefix) {

                    $bill_no = \Request::get('bill_no');
                    if ($bill_no) {

                        return $query->where($prefix . 'abbr_fin_orders.bill_no', $bill_no);

                    }

                })
                ->where(function ($query) {


                    $customer_id = \Request::get('clients_id');

                    if ($customer_id) {

                        return $query->where('pos_customer_id', $customer_id);
                    }


                })
                ->when(\Request::get('paid_by'), function ($query) {
                    $query->whereHas('payments', function ($query) {
                        $paid_by = \Request::get('paid_by');
                        if ($paid_by) {

                            return $query->where('paid_by', $paid_by);

                        }

                    });
                })
                ->where('reservation_id', 0)
                ->leftjoin($prefix . 'abbr_fin_orders_meta', $prefix . 'abbr_fin_orders_meta.order_id', '=', $prefix . 'abbr_fin_orders.id')
                ->groupBy($prefix . 'abbr_fin_orders.id')
                ->orderBy($prefix . 'abbr_fin_orders.id', 'desc')
                ->paginate($limit);

        }
        else {
            $orders = $ord->select($prefix . 'fin_orders.*', $prefix . 'fin_orders_meta.credit_note_no', $prefix . 'fin_orders_meta.settlement', $prefix . 'fin_orders_meta.is_bill_active')
                ->with('payments')->where(function ($query) use ($prefix) {
                    $start_date = \Request::get('start_date');
                    $end_date = \Request::get('end_date');
                    if ($start_date && $end_date) {

                        return $query->where($prefix . 'fin_orders.bill_date', '>=', $start_date)
                            ->where($prefix . 'fin_orders.bill_date', '<=', $end_date);
                    }
                })->where(function ($query) use ($prefix) {

                    $outlet_id = \Request::get('outlet_id');
                    if ($outlet_id) {

                        return $query->where($prefix . 'fin_orders.outlet_id', $outlet_id);
                    }
                })
                ->where(function ($query) use ($prefix) {

                    $bill_no = \Request::get('bill_no');
                    if ($bill_no) {

                        return $query->where($prefix . 'fin_orders.bill_no', $bill_no);

                    }

                })
                ->where(function ($query) {


                    $customer_id = \Request::get('clients_id');

                    if ($customer_id) {

                        return $query->where('pos_customer_id', $customer_id);
                    }


                })
                ->when(\Request::get('paid_by'), function ($query) {
                    $query->whereHas('payments', function ($query) {
                        $paid_by = \Request::get('paid_by');
                        if ($paid_by) {

                            return $query->where('paid_by', $paid_by);

                        }

                    });
                })
                ->where('reservation_id', 0)
                ->leftjoin($prefix . 'fin_orders_meta', $prefix . 'fin_orders_meta.order_id', '=', $prefix . 'fin_orders.id')
                ->groupBy($prefix . 'fin_orders.id')
                ->orderBy($prefix . 'fin_orders.id', 'desc')
                ->paginate($limit);

        }

        $page_title = ('All '.(\Request::get('invoice_type')=='abbr'?'Abbreviated Tax':'Tax').' Invoice');

        $page_description = 'Invoice List';

        $outlets = \App\Models\PosOutlets::pluck('name', 'id');

        $clients = Client::select('id', 'name')->orderBy('id', 'DESC')->pluck('name', 'id');


        $pay_method = $this->pay_method;

        return view('admin.hotel.possalesdetail.allposinvoice', compact('allFiscalYear', 'fiscal_year', 'orders', 'page_title', 'page_description', 'outlets', 'pay_method', 'clients'));
    }

    public function abbr_finInvoice()
    {

//        dd(asa);
        if (\Request::get('outlet')) {


            $page_title = 'Todays Invoice';

            $page_description = 'Invoice List of Today';
            if (\Request::get('invoice_type')=='abbr'){
                $orders = \App\Models\AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.credit_note_no', 'abbr_fin_orders_meta.settlement', 'abbr_fin_orders_meta.is_bill_active')
                    ->where('abbr_fin_orders.outlet_id', \Request::get('outlet'))
                    ->where('abbr_fin_orders.bill_date', date('Y-m-d'))
                    ->where(function ($query)  {

                        $bill_no = \Request::get('bill_no');
                        if ($bill_no) {

                            return $query->where('abbr_fin_orders.bill_no', $bill_no);

                        }

                    })
                    ->where(function ($query) {


                        $customer_id = \Request::get('clients_id');

                        if ($customer_id) {

                            return $query->where('pos_customer_id', $customer_id);
                        }


                    })
                    ->when(\Request::get('paid_by'), function ($query) {
                        $query->whereHas('payments', function ($query) {
                            $paid_by = \Request::get('paid_by');
                            if ($paid_by) {

                                return $query->where('paid_by', $paid_by);

                            }

                        });
                    })
                    ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id')
                    ->groupBy('abbr_fin_orders.id')
                    ->orderBy('abbr_fin_orders.id', 'desc')
                    ->paginate(25);

                $last_page = $orders->lastPage();

                $current_page = $orders->currentPage();


                $totalAmount = \App\Models\AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.is_bill_active')
                    ->where('abbr_fin_orders.outlet_id', \Request::get('outlet'))
                    ->where(function ($query)  {

                        $bill_no = \Request::get('bill_no');
                        if ($bill_no) {

                            return $query->where('abbr_fin_orders.bill_no', $bill_no);

                        }

                    })
                    ->where(function ($query) {


                        $customer_id = \Request::get('clients_id');

                        if ($customer_id) {

                            return $query->where('pos_customer_id', $customer_id);
                        }


                    })
                    ->when(\Request::get('paid_by'), function ($query) {
                        $query->whereHas('payments', function ($query) {
                            $paid_by = \Request::get('paid_by');
                            if ($paid_by) {

                                return $query->where('paid_by', $paid_by);

                            }

                        });
                    })
                    ->where('abbr_fin_orders.bill_date', date('Y-m-d'))
                    ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id')
                    ->where('abbr_fin_orders_meta.is_bill_active', 1)
                    ->sum('total_amount');
            }


            else{
                $orders = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.credit_note_no', 'fin_orders_meta.settlement', 'fin_orders_meta.is_bill_active')
                    ->where('fin_orders.outlet_id', \Request::get('outlet'))
                    ->where('fin_orders.bill_date', date('Y-m-d'))
                    ->where(function ($query) {

                        $bill_no = \Request::get('bill_no');
                        if ($bill_no) {

                            return $query->where('fin_orders.bill_no', $bill_no);

                        }

                    })
                    ->where(function ($query) {


                        $customer_id = \Request::get('clients_id');

                        if ($customer_id) {

                            return $query->where('pos_customer_id', $customer_id);
                        }


                    })
                    ->when(\Request::get('paid_by'), function ($query) {
                        $query->whereHas('payments', function ($query) {
                            $paid_by = \Request::get('paid_by');
                            if ($paid_by) {

                                return $query->where('paid_by', $paid_by);

                            }

                        });
                    })
                    ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
                    ->groupBy('fin_orders.id')
                    ->orderBy('fin_orders.id', 'desc')
                    ->paginate(25);

                $last_page = $orders->lastPage();

                $current_page = $orders->currentPage();


                $totalAmount = \App\Models\Orders::select('fin_orders.*', 'fin_orders_meta.is_bill_active')
                    ->where('fin_orders.outlet_id', \Request::get('outlet'))
                    ->where(function ($query) {

                        $bill_no = \Request::get('bill_no');
                        if ($bill_no) {

                            return $query->where('fin_orders.bill_no', $bill_no);

                        }

                    })
                    ->where(function ($query) {


                        $customer_id = \Request::get('clients_id');

                        if ($customer_id) {

                            return $query->where('pos_customer_id', $customer_id);
                        }


                    })
                    ->when(\Request::get('paid_by'), function ($query) {
                        $query->whereHas('payments', function ($query) {
                            $paid_by = \Request::get('paid_by');
                            if ($paid_by) {

                                return $query->where('paid_by', $paid_by);

                            }

                        });
                    })
                    ->where('fin_orders.bill_date', date('Y-m-d'))
                    ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
                    ->where('fin_orders_meta.is_bill_active', 1)
                    ->sum('total_amount');
            }
            $clients = Client::select('id', 'name')->orderBy('id', 'DESC')->pluck('name', 'id');


            $pay_method = $this->pay_method;

            return view('admin.hotel.possalesdetail.todaysposinvoice', compact('clients','pay_method','orders', 'page_title', 'page_description', 'totalAmount', 'last_page', 'current_page'));

        }
        if (\Auth::user()->hasRole('admins')) {
            $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        } else {
            $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
            $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
                ->orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->get();
        }
        $description = 'Today Invoice Outlet';

        $page_title = 'Admin | Hotel | POS Outlets | VoildBills';


        return view('admin.hotel.posdashboard.todayinvoiceoutlet', compact('page_title', 'outlets', 'description'));


    }

    public function allPosPayment()
    {

        $payment_list = \App\Models\Payment::select('payments.*')
            ->where(function ($query) {

                if (\Request::get('start_date') && \Request::get('end_date')) {

                    $start_date = \Request::get('start_date');

                    $end_date = \Request::get('end_date');
                } else {

                    $start_date = date('Y-m-d');

                    $end_date = date('Y-m-d');
                }

                return $query->where('payments.date', '>=', $start_date)
                    ->where('payments.date', '<=', $end_date);

            })
            ->where(function ($query) {
                $outlet_id = \Request::get('outlet_id');
                if ($outlet_id) {
                    return $query->where('fin_orders.outlet_id', $outlet_id)
                        ->orWhere('abbr_fin_orders.outlet_id', $outlet_id);
                }
            })->where(function ($query) {
                $paid_by = \Request::get('paid_by');
                if ($paid_by) {
                    return $query->where('payments.paid_by', $paid_by);
                }
            })->where(function ($query) {

                $bill_no = \Request::get('bill_no');
                if ($bill_no) {

                    return $query->where('fin_orders.bill_no','like', '%'.$bill_no.'%')
                        ->orWhere('abbr_fin_orders.bill_no','like', '%'.$bill_no.'%');

                }

            })->where(function ($query) {


                $customer_id = \Request::get('client_id');

                if ($customer_id) {

                    return $query->where('fin_orders.pos_customer_id', $customer_id)
                        ->orWhere('fin_orders.pos_customer_id', $customer_id);
                }

            })->whereNotNull('sale_id')->whereNull('purchase_id')
//            ->leftJoin('fin_orders', function($join){
//                $join->on('fin_orders.id', '=', 'payments.sale_id');
//                $join->where('payments.type','!=',"abbr");
//            })
////            ->leftjoin('fin_orders', 'fin_orders.id', '=', 'payments.sale_id')
//            ->leftjoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id')
//            ->leftJoin('abbr_fin_orders', function($join){
//                $join->on('abbr_fin_orders.id', '=', 'payments.sale_id');
//                $join->where('payments.type','=','abbr');
//            })
////             ->leftjoin('abbr_fin_orders', 'abbr_fin_orders.id', '=', 'payments.sale_id')
//            ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id')
            ->orderby('payments.id', 'desc')
            ->groupBy('payments.id')
            ->paginate(50);


        $page_title = 'All Payment';

        $page_description = '
        Payment List';
        $outlets = \App\Models\PosOutlets::pluck('name', 'id');
        $pay_method = $this->pay_method;
        $clients = Client::pluck('name', 'id');

        return view('admin.hotel.possalesdetail.allpospayments', compact('payment_list', 'page_title', 'page_description', 'outlets', 'pay_method', 'clients'));
    }

    public function todayPosPayment()
    {

        $payment_list =  \App\Models\Payment::select('payments.*')
//            'fin_orders_meta.is_bill_active as is_bill_active','abbr_fin_orders_meta.is_bill_active as is_bill_active_abbr')
            ->where('payments.created_at', '>=', \Carbon\Carbon::today())
            ->where('payments.created_at', '<=', \Carbon\Carbon::tomorrow())
            ->where(function ($query) {
                $outlet_id = \Request::get('outlet_id');
                if ($outlet_id) {
                    return $query->where('fin_orders.outlet_id', $outlet_id)
                        ->orWhere('abbr_fin_orders.outlet_id', $outlet_id);
                }
            })->where(function ($query) {
                $paid_by = \Request::get('paid_by');
                if ($paid_by) {
                    return $query->where('payments.paid_by', $paid_by);
                }
            })->where(function ($query) {

                $bill_no = \Request::get('bill_no');
                if ($bill_no) {

                    return $query->where('fin_orders.bill_no','like', '%'.$bill_no.'%')
                        ->orWhere('abbr_fin_orders.bill_no','like', '%'.$bill_no.'%');

                }

            })->where(function ($query) {


                $customer_id = \Request::get('client_id');

                if ($customer_id) {

                    return $query->where('fin_orders.pos_customer_id', $customer_id)
                        ->orWhere('fin_orders.pos_customer_id', $customer_id);
                }

            })->whereNotNull('sale_id')->whereNull('purchase_id')

//            ->leftJoin('abbr_fin_orders', function($join){
//                $join->where('payments.type','=','abbr');
//                $join->on('abbr_fin_orders.id', '=', 'payments.sale_id')
//                ->leftJoin('abbr_fin_orders_meta', 'abbr_fin_orders_meta.order_id', '=', 'abbr_fin_orders.id');
//
//            }) ->leftJoin('fin_orders', function($join){
//                $join->where('payments.type',"");
//                $join->on('fin_orders.id', '=', 'payments.sale_id');
//                $join->leftJoin('fin_orders_meta', 'fin_orders_meta.order_id', '=', 'fin_orders.id');
//
//            })

            //             ->leftjoin('abbr_fin_orders', 'abbr_fin_orders.id', '=', 'payments.sale_id')
            ->orderby('payments.id', 'desc')
            ->groupBy('payments.id')
            ->get();

        $outlets = \App\Models\PosOutlets::pluck('name', 'id');

        $page_title = 'Today Payment';

        $page_description = 'Payment List of Today';

        $pay_method = $this->pay_method;
        $clients = Client::pluck('name', 'id');
        return view('admin.hotel.possalesdetail.todayspospayments', compact('payment_list', 'page_title', 'page_description', 'outlets', 'pay_method', 'clients'));
    }


    public function voidfromedit(Request $request, $id)
    {

        $reason = $request->reason;

        $folio = \App\Models\Orders::find($id);

        $makevoid = ['void_reason' => $reason, 'is_bill_active' => 0];

        \App\Models\Orders::find($id)->update($makevoid);


        Flash::success('Order sucessfully marked as void');

        return redirect()->back();
    }

    public function voidConfirm($id)
    {

        $error = null;

        $res = \App\Models\Orders::find($id);

        $modal_title = "Void This Order";

        $modal_route = route('admin.orders.makevoid', $res->id);

        return view('modal_void_reason', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }


    public function getPanNUM($id)
    {

        $res = \App\Models\Reservation::find($id);
        $vat_no = $res->client->vat;
        $company_name = $res->client->name;
        $guest_name = $res->guest_name;

        return response()->json(['vat_no' => $vat_no, 'company_name' => $company_name, 'guest_name' => $guest_name]);
    }

    public function downloadpdfDailyMonthly(Request $request)
    {

        $attributes = $request->all();
        $date = date('Y-m-d');
        $month_before_date = \Carbon\Carbon::now()->subDays(30)->format('Y-m-d');

        if (isset($request->daily)) {

            $orders = \App\Models\Orders::orderBy('id', 'asc')
                ->where('org_id', \Auth::user()->org_id)
                ->where('is_bill_active', 1)
                ->where(function ($query) {
                    if (\Request::get('outlet_id') && \Request::get('outlet_id') != '')
                        return $query->where('outlet_id', \Request::get('outlet_id'));
                })
                ->where('bill_date', $date)
                ->get();

            $imagepath = \Auth::user()->organization->logo;

            $pdf = \PDF::loadView('admin.orders.dailymonthlypdf', compact('orders', 'imagepath', 'date'));
            $file = 'daily-' . date('Y-m-d') . '.pdf';

            if (\File::exists('reports/' . $file)) {
                \File::Delete('reports/' . $file);
            }
            return $pdf->download($file);
        } else {

            $orders = \App\Models\Orders::orderBy('id', 'asc')
                ->where('org_id', \Auth::user()->org_id)
                ->where('is_bill_active', 1)
                ->where(function ($query) {
                    if (\Request::get('outlet_id') && \Request::get('outlet_id') != '')
                        return $query->where('outlet_id', \Request::get('outlet_id'));
                })
                ->where('bill_date', '>=', $month_before_date)
                ->where('bill_date', '<=', $date)
                ->get();

            $imagepath = \Auth::user()->organization->logo;

            $pdf = \PDF::loadView('admin.orders.dailymonthlypdf', compact('orders', 'imagepath', 'date', 'month_before_date'));
            $file = 'monthly-' . date('Y-m-d') . '.pdf';

            if (\File::exists('reports/' . $file)) {
                \File::Delete('reports/' . $file);
            }
            return $pdf->download($file);

        }
    }


    private function record_histroy($order)
    {

        $order = $order->toArray();
        $order['parent_id'] = $order['id'];

        $clone_order = \App\Models\OrderHistroy::create($order);
        $details = \App\Models\OrderDetail::where('order_id', $order['id'])->get()->toArray();

        foreach ($details as $k => $val) {

            unset($details[$k]['id'], $details[$k]['created_at'], $details[$k]['updated_at']);
            unset($details[$k]['discount_percent']);
            $details[$k]['order_id'] = $clone_order->id;
        }

        \App\Models\OrderDetailsHistroy::insert($details);

        return 1;
    }


    public function record_histroy_index($orderId)
    {

        $orders = \App\Models\OrderHistroy::where('parent_id', $orderId)->orderBy('created_at', 'desc')->paginate(30);

        if (count($orders) == 0) {
            Flash::error('No any histoy Found');

            return redirect()->back();
        }
        $page_title = "Order Histroy For Order Id #{$orderId}";
        $outlet = \App\Models\PosOutlets::find($orders[0]->outlet_id);


        return view('admin.orders.histroy.order_histroy', compact('orders', 'page_title', 'outlet'));
    }


    public function record_histroy_show($id)
    {

        $ord = \App\Models\OrderHistroy::find($id);
        $page_title = 'Orders';
        $page_description = 'View Order Histroy';
        $orderDetails = \App\Models\OrderDetailsHistroy::where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;

        return view('admin.orders.histroy.order_histroy_show', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
    }

    public function showcreditnote($id)
    {
        if (request('bill_type')=='abbr'){
            $ord = AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.credit_note_no')
                ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders.id', '=', 'abbr_fin_orders_meta.order_id')
                ->where('abbr_fin_orders.id',$id)->first();

            $page_title = 'Orders';
            $page_description = 'View Order';
            $orderDetails = AbbrOrderDetail::where('order_id', $ord->id)->get();
            $imagepath = \Auth::user()->organization->logo;
            return view('admin.orders.credit_note_show', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));

        }
        $ord = Orders::select('fin_orders.*', 'fin_orders_meta.credit_note_no')
            ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
            ->where('fin_orders.id',$id)->first();

        $page_title = 'Orders';
        $page_description = 'View Order';
        $orderDetails = OrderDetail::where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;
        return view('admin.orders.credit_note_show', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
    }

    public function printcreditnote($id, $type)
    {        if (request('bill_type')=='abbr') {

         $ord = AbbrOrders::select('abbr_fin_orders.*', 'abbr_fin_orders_meta.credit_note_no')
            ->leftjoin('abbr_fin_orders_meta', 'abbr_fin_orders.id', '=', 'abbr_fin_orders_meta.order_id')
            ->where('abbr_fin_orders.id', $id)->first();
        $orderDetails = AbbrOrderDetail::where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;
        if ($type == 'print') {
            // $pdf =  \PDF::loadView('admin.orders.credit_note_print', compact('ord', 'imagepath', 'orderDetails'));
            // return  $pdf->download($file);
            return view('admin.orders.credit_note_print', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));

        }

        return view('admin.orders.generatecreditnotePDF', compact('ord', 'imagepath', 'orderDetails'));
        $file = $id . '_' . $ord->name . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';
        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }
        return $pdf->download($file);
    }

        $ord = Orders::select('fin_orders.*', 'fin_orders_meta.credit_note_no')
            ->leftjoin('fin_orders_meta', 'fin_orders.id', '=', 'fin_orders_meta.order_id')
            ->where('fin_orders.id',$id)->first();
        $orderDetails = OrderDetail::where('order_id', $ord->id)->get();
        $imagepath = \Auth::user()->organization->logo;
        if ($type == 'print') {
            // $pdf =  \PDF::loadView('admin.orders.credit_note_print', compact('ord', 'imagepath', 'orderDetails'));
            // return  $pdf->download($file);
            return view('admin.orders.credit_note_print', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));


        }


        return view('admin.orders.generatecreditnotePDF', compact('ord', 'imagepath', 'orderDetails'));
        $file = $id . '_' . $ord->name . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';
        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }
        return $pdf->download($file);
    }

    public function OutletForStatus(Request $request)
    {
        $page_title = "Admin | Orders | Outlets Status";

        if (\Auth::user()->hasRole('admins')) {
            $outlets = \App\Models\PosOutlets::orderBy('id', 'DESC')->where('fnb_outlet', 1)->get();
        } else {
            $outletusers = \App\Models\OutletUser::where('user_id', \Auth::user()->id)->get()->pluck('outlet_id');
            $outlets = \App\Models\PosOutlets::whereIn('id', $outletusers)
                ->orderBy('id', 'DESC')
                ->where('enabled', 1)
                ->where('fnb_outlet', 1)
                ->get();
        }

        return view('admin.orders.outletforstatus', compact('page_title', 'outlets'));
    }

    public function ordersByStatus(Request $request)
    {

        $outlet_id = \Request::get('outlet_id');
        $page_title = "Admin | Orders | By Status";
        $page_description = "pluck of Orders By Status";
        $ordered = \App\Models\Orders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->where('ready_status', 'ordered')->get();
        $cooking = \App\Models\Orders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->where('ready_status', 'cooking')->get();
        $cooked = \App\Models\Orders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->where('ready_status', 'cooked')->get();
        $served = \App\Models\Orders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->where('ready_status', 'served')->get();
        $checkedout = \App\Models\Orders::orderBy('id', 'desc')->where('outlet_id', $outlet_id)->where('ready_status', 'checkedout')->get();
        return view('admin.orders.ordersbystatus', compact('page_title', 'page_description', 'ordered', 'cooking', 'cooked', 'served', 'checkedout'));
    }

    private function convertdate($date)
    {

        $date = explode('-', $date);
        $cal = new \App\Helpers\NepaliCalendar();
        $converted = $cal->eng_to_nep($date[0], $date[1], $date[2]);
        $nepdate = $converted['year'] . '.' . $converted['nmonth'] . '.' . $converted['date'];
        return $nepdate;
    }

    public function getbillinfo()
    {
        if (\Request::get('bill_type')=='abbreviated') {
            $order = \App\Models\AbbrOrders::select('abbr_fin_orders.*','abbr_fin_orders_meta.credit_note_no')
                ->join('abbr_fin_orders_meta','abbr_fin_orders_meta.order_id','abbr_fin_orders.id')
                ->where('bill_no', \Request::get('bill_no'))
                ->where('outlet_id', \Request::get('outlet_id'))
                ->where('is_converted', 0)
                ->where('fiscal_year', \Request::get('fiscal_year'))->first();
            return $order;
        }

        elseif (\Request::get('bill_type')=='non_abbreviated') {
            $order = \App\Models\Orders::select('fin_orders.*','fin_orders_meta.credit_note_no')
                ->join('fin_orders_meta','fin_orders_meta.order_id','fin_orders.id')
            ->where('bill_no', \Request::get('bill_no'))
                ->where('outlet_id', \Request::get('outlet_id'))
                ->where('fiscal_year', \Request::get('fiscal_year'))->first();
            return $order;
        }

    }

    public function getbillinfoWithCN()
    {

        if (\Request::get('bill_type')=='abbreviated') {
            $order = \App\Models\AbbrOrders::select('abbr_fin_orders.*','abbr_fin_orders_meta.credit_note_no')
                ->join('abbr_fin_orders_meta','abbr_fin_orders_meta.order_id','abbr_fin_orders.id')
                ->where('abbr_fin_orders_meta.is_bill_active', 1)
                ->where('bill_no', \Request::get('bill_no'))
                ->where('outlet_id', \Request::get('outlet_id'))
                ->where('is_converted', 0)
                ->where('fiscal_year', \Request::get('fiscal_year'))->first();
            $credit_note_no =  (\DB::table('abbr_fin_orders_meta')->select('abbr_fin_orders.fiscal_year','abbr_fin_orders_meta.credit_note_no')
                ->join('abbr_fin_orders','abbr_fin_orders_meta.order_id','abbr_fin_orders.id')->orderBy('abbr_fin_orders_meta.credit_note_no', 'desc')
        ->where('credit_note_no', '!=', 'null')->where('abbr_fin_orders.fiscal_year',\Request::get('fiscal_year') )->first()->credit_note_no ?? 0) + 1;
            return ['order'=>$order,'credit_note_no'=>$credit_note_no];
        }

        elseif (\Request::get('bill_type')=='non_abbreviated') {
            $order = \App\Models\Orders::select('fin_orders.*','fin_orders_meta.credit_note_no')
                ->join('fin_orders_meta','fin_orders_meta.order_id','fin_orders.id')
            ->where('bill_no', \Request::get('bill_no'))
                ->where('fin_orders_meta.is_bill_active', 1)
                ->where('outlet_id', \Request::get('outlet_id'))
                ->where('fiscal_year', \Request::get('fiscal_year'))->first();

            $credit_note_no =  (\DB::table('fin_orders_meta')->select('fin_orders.fiscal_year','fin_orders_meta.credit_note_no')
                        ->join('fin_orders','fin_orders_meta.order_id','fin_orders.id')->orderBy('fin_orders_meta.credit_note_no', 'desc')
                        ->where('credit_note_no', '!=', 'null')->where('fin_orders.fiscal_year',\Request::get('fiscal_year'))->first()->credit_note_no ?? 0) + 1;
            return ['order'=>$order,'credit_note_no'=>$credit_note_no];
        }

    }


    public function ConvertFoliotoFinalInvoice($id, $fromOtherController = false)
    {
        DB::beginTransaction();
        $alreadyinvoice = \App\Models\Orders::where('folio_id', $id)->get();

        $alreadyfolio = \App\Models\Folio::find($id);
        $alreadyinvoicedetail = \App\Models\OrderDetail::where('folio_id', $id)->get();

        $fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', '1')->first();

        $bill_no = \App\Models\Orders::select('bill_no')
            ->where('fiscal_year', $fiscal_year->fiscal_year)
            ->where('outlet_id', env('HOTEL_OUTLET_ID'))
            ->orderBy('bill_no', 'desc')
            ->first();

        $bill_no = ($bill_no->bill_no ?? 0) + 1;

        $ckfiscalyear = \App\Models\Fiscalyear::where('current_year', '1')
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->first();

        if (!$ckfiscalyear)
            return \Redirect::back()->withErrors(['Please update fiscal year <a href="/admin/fiscalyear/create">Click Here</a>!']);

        $fiscal_year = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->fiscal_year;


        if (count($alreadyinvoice) > 0) {

            Flash::warning('Already Invoice Exists For This Folio');
            return \Redirect::back();
        } else {

            if ($alreadyfolio->reservation->bill_to == 'companyandguest') {

                $foliodetail = \App\Models\FolioDetail::where('folio_id', $id)->get();

                $accommodation = \App\Models\FolioDetail::where('folio_id', $id)->where('flag', 'accommodation')->get();

                $extra = \App\Models\FolioDetail::where('folio_id', $id)->where('flag', '!=', 'accommodation')->get();

                $folio_main = \App\Models\Folio::where('id', $id)->first();

                if ($accommodation) {

                    $accommodation_total_amount = $accommodation->sum('total');
                    $accommodation_service_charge = $accommodation_total_amount * env('SERVICE_CHARGE') / 100;
                    $accommodation_service_total_amount = $accommodation_service_charge + $accommodation_total_amount;
                    $accommodation_tax_amount = $accommodation_service_total_amount * 13 / 100;

                    $folio = $folio_main->toArray();
                    unset($folio['id']);

                    $folio['folio_id'] = $id;
                    $folio['outlet_id'] = env('HOTEL_OUTLET_ID');
                    $folio['bill_no'] = $bill_no++;
                    $folio['bill_date'] = date('Y-m-d');
                    $folio['ready_status'] = 'checkedout';
                    $folio['subtotal'] = $accommodation_total_amount;
                    $folio['service_charge'] = $accommodation_service_charge;
                    $folio['amount_with_service'] = $accommodation_service_total_amount;
                    $folio['taxable_amount'] = $accommodation_service_total_amount;
                    $folio['tax_amount'] = $accommodation_tax_amount;
                    $folio['total_amount'] = $accommodation_service_total_amount + $accommodation_tax_amount;


                    $folio['payment_status'] = 'Paid'; //bibek changes


                    $order = \App\Models\Orders::create($folio);

                    $ordersmeta = new \App\Models\OrderMeta();
                    $ordersmeta->order_id = $order->id;
                    $ordersmeta->sync_with_ird = 0;
                    $ordersmeta->is_bill_active = 1;
                    $ordersmeta->settlement = 1;
                    $ordersmeta->is_posted = 1;
                    $ordersmeta->posting_entry_id = $folio['posting_entry_id'];
                    $ordersmeta->settle_entry_id = $folio['settle_entry_id'];
                    $ordersmeta->save();

                    foreach ($accommodation as $fd) {

                        $newinvoicedetails = $fd->toArray();
                        unset($newinvoicedetails['id']);
                        $newinvoicedetails['order_id'] = $order->id;

                        \App\Models\OrderDetail::create($newinvoicedetails);
                    }
                }
                if (count($extra) > 0) {


                    $extra_total_amount = $extra->sum('total');
                    $extra_service_charge = $extra_total_amount * env('SERVICE_CHARGE') / 100;
                    $extra_service_total_amount = $extra_service_charge + $extra_total_amount;
                    $extra_tax_amount = $extra_service_total_amount * 13 / 100;

                    $folio = $folio_main->toArray();
                    unset($folio['id']);

                    $folio['folio_id'] = $id;
                    $folio['bill_no'] = $bill_no++;
                    $folio['outlet_id'] = env('HOTEL_OUTLET_ID');
                    $folio['ready_status'] = 'checkedout';
                    $folio['bill_date'] = date('Y-m-d');
                    $folio['subtotal'] = $extra_total_amount;
                    $folio['service_charge'] = $extra_service_charge;
                    $folio['amount_with_service'] = $extra_service_total_amount;
                    $folio['taxable_amount'] = $extra_service_total_amount;
                    $folio['tax_amount'] = $extra_tax_amount;

                    $folio['total_amount'] = $extra_service_total_amount + $extra_tax_amount;
                    $folio['payment_status'] = 'Paid'; //bibek changes

                    $order = \App\Models\Orders::create($folio);

                    $ordersmeta = new \App\Models\OrderMeta();
                    $ordersmeta->order_id = $order->id;
                    $ordersmeta->sync_with_ird = 0;
                    $ordersmeta->is_bill_active = 1;
                    $ordersmeta->settlement = 1;
                    $ordersmeta->is_posted = 1;
                    $ordersmeta->posting_entry_id = $folio['posting_entry_id'];
                    $ordersmeta->settle_entry_id = $folio['settle_entry_id'];
                    $ordersmeta->save();

                    foreach ($extra as $fd) {

                        $newinvoicedetails = $fd->toArray();
                        unset($newinvoicedetails['id']);
                        $newinvoicedetails['order_id'] = $order->id;

                        \App\Models\OrderDetail::create($newinvoicedetails);
                    }
                }
            } else {

                $folio = \App\Models\Folio::where('id', $id)->first();
                $folio = $folio->toArray();
                unset($folio['id']);

                $folio['folio_id'] = $id;
                $folio['outlet_id'] = env('HOTEL_OUTLET_ID');
                $folio['ready_status'] = 'checkedout';
                $folio['bill_no'] = $bill_no++;
                $folio['bill_date'] = date('Y-m-d');
                $folio['payment_status'] = 'Paid';

                $order = \App\Models\Orders::create($folio);

                $ordersmeta = new \App\Models\OrderMeta();
                $ordersmeta->order_id = $order->id;
                $ordersmeta->sync_with_ird = 0;
                $ordersmeta->is_bill_active = 1;
                $ordersmeta->settlement = 1;
                $ordersmeta->is_posted = 1;
                $ordersmeta->posting_entry_id = $folio['posting_entry_id'];
                $ordersmeta->settle_entry_id = $folio['settle_entry_id'];
                $ordersmeta->save();


                $foliodetail = \App\Models\FolioDetail::where('folio_id', $id)->get();
                foreach ($foliodetail as $fd) {

                    $newinvoicedetails = $fd->toArray();
                    unset($newinvoicedetails['id']);
                    $newinvoicedetails['order_id'] = $order->id;

                    \App\Models\OrderDetail::create($newinvoicedetails);
                }
            }
        }





        $folio = \App\Models\Folio::find($id);
        $folio->update(['is_final_invoice' => 1]);


        if (request()->checkout==true){
            $_check =  \App\Models\ReservationStatus::select('id')->where('status_name', 'Checked out')->first();
            $check_out = date('Y-m-d');
            $check_out_time = date('H:i');
            $user = \Auth::user()->id;
            Reservation::find($folio->reservation_id)->update(['reservation_status' => $_check->id, 'check_out' => $check_out, 'check_in_time' => $check_out_time, 'check_out_by' => $user]);

            Reservation::where('parent_res', $folio->reservation_id)->update(['reservation_status' => $_check->id, 'check_out' => $check_out, 'check_in_time' => $check_out_time, 'check_out_by' => $user]);
            $folio->reservation->room()->update(['house_status_id'=>request()->house_status_id]);
            Flash::success('Checked out successfully');

        }
        $order = \App\Models\Orders::where('folio_id', $id)->get();
        DB::commit();
        foreach ($order as $order) {

            Audit::log(Auth::user()->id, "Hotel Invoice", "Hotel Final Bill Is Created: ID-" . $order->id . "");

            if ($order) {

                if ($order->folio->reservation->client) {
                    $guest_name = $order->folio->reservation->client->name;
                    $buyer_pan = $order->folio->reservation->client->vat;
                } else {
                    $guest_name = $order->folio->reservation->guest_name;
                    $buyer_pan = 0;
                }

                $bill_date_nepali = $this->convertdate($order->bill_date);
                $bill_today_date_nep = $this->convertdate(date('Y-m-d'));

                $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'), "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $buyer_pan, "fiscal_year" => $order->fiscal_year, "buyer_name" => $guest_name, "invoice_number" => $order->outlet->outlet_code . $order->bill_no, "invoice_date" => $bill_date_nepali, "total_sales" => $order->total_amount, "taxable_sales_vat" => $order->taxable_amount, "vat" => $order->tax_amount, "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0, "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0, "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);


                $irdsync = new \App\Models\NepalIRDSync();
                $response = $irdsync->postbill($data);

                if ($response == 200) {

                    \App\Models\OrderMeta::where('order_id', $order->id)->first()->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
                    Audit::log(Auth::user()->id, "Hotel Invoice", "Successfully Posted to IRD, ID-" . env('HOTEL_BILL_PREFIX') . $invoice->bill_no . " Response:" . $response . "");

                    Flash::success('Final Invoice successfully Generated. Successfully Posted to IRD. Code: ' . $response . '');
                } else {
                    if ($response == 101) {
                        \App\Models\OrderMeta::where('order_id', $order->id)->first()->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
                    } else {
                        \App\Models\OrderMeta::where('order_id', $order->id)->first()->update(['is_realtime' => 1]);
                    }
                    Audit::log(Auth::user()->id, "Hotel Invoice", "Failed To post in IRD, ID-" . env('HOTEL_BILL_PREFIX') . $order->bill_no . ", Response:" . $response . "");
                    Flash::error('Final Invoice successfully Generated. Post Cannot Due to Response Code: ' . $response . '');
                }
            }
        }

        // if($fromOtherController){

        //     return ['order_id'=>''];
        // }
        Flash::success('Final Invoice successfully Generated.');
//        request()->checkout==true?redirect('/admin/hotel/reservation-edit/'.$folio->reservation_id):
        return \Redirect::back();
    }


    public function getcategorybyid(Request $request)
    {
        $id = $request->id;

        // $id = OrderDetail::whereIn('id',$id)->get();


        $productstype = \App\Models\OrderDetailProxy::whereIn('id', $id)->get();

        $pc = [];

        $allcat = [];


        foreach ($productstype as $key => $value) {

            $type = $value->product->product_type_id;

            $pc[] = ['id' => $value->id, 'cid' => $type];
            $master = $value->product->producttypemaster;
            if (!$allcat[$type]) {
                $allcat[$type] =
                    ['id' => $type,
                        'name' => $master->name,
                        'discount_limit' => $master->discount_limit,
                    ];
            }


        }
        $allcat = array_values($allcat);

        return ['types' => $allcat, 'itemcat' => $pc, 'id' => $id];


    }


    public function getposdiscount($client_id)
    {


        $clients = \App\Models\Client::find($client_id);


        return ['pos_discount' => $clients->pos_discount];


    }


    public function mergeTable($id)
    {

        $orderproxy = $this->orderproxy->find($id);

        $busytable = $this->orderproxy->where('ready_status', 'ordered')
            ->where('id', '!=', $id)
            ->where('table', '!=', $orderproxy->table)
            ->where('outlet_id', $orderproxy->outlet_id)
            ->get();

        return view('admin.orders.merge.merge', compact('orderproxy', 'busytable'));


    }


    public function merge($from, $to)
    {

        $fromOrder = $this->orderproxy->find($from);

        $toOrder = $this->orderproxy->find($to);

        $toOrder->amount += $fromOrder->amount;
        $toOrder->discount_amount += $fromOrder->discount_amount;
        $toOrder->taxable_amount += $fromOrder->taxable_amount;
        $toOrder->tax_amount += $fromOrder->tax_amount;
        $toOrder->total_amount += $fromOrder->total_amount;
        $toOrder->paid_amount += $fromOrder->paid_amount;
        $toOrder->subtotal += $fromOrder->subtotal;
        $toOrder->amount_with_service += $fromOrder->amount_with_service;
        $toOrder->service_charge += $fromOrder->service_charge;
        $toOrder->update();

        $fromOrderDetails = OrderDetailProxy::where('order_id', $from)->get()->toArray();

        $toOrderDetails = [];

        foreach ($fromOrderDetails as $key => $value) {
            # code...
            unset($value['id']);
            $value['order_id'] = $to;
            $toOrderDetails [] = $value;

        }

        OrderDetailProxy::where('order_id', $from)->delete();

        $this->orderproxy->find($from)->update(['ready_status' => 'merged', 'merged_to' => $to]);

        $this->orderproxy->find($from)->delete();

        OrderDetailProxy::insert($toOrderDetails);

        return 1;

    }


    public function mergeTablePost(Request $request, $id)
    {

        $attributes = $request->all();

        $toOrder = $this->orderproxy->find($request->order_id);

        if (!$toOrder) {

            Flash::warning("Nothing to merge");

            return redirect()->back();
        }

        $this->merge($id, $request->order_id);

        Flash::success("Table Merged");
        return redirect("/admin/orders/{$request->order_id}/edit?type=invoice");


    }


    public function edmPrint($id)
    { //proxy id


        $ord = $this->orderproxy->find($id);
        if ($ord->is_final_invoice || !$ord->id) {

            Flash::warning("Order Already Posted Please Dont Move back and post order Now :D");
            return redirect('/admin/pos/dashboard');
        }

        $orderDetails = OrderDetailProxy::where('order_id', $ord->id)->get()->groupBy('product_type_id');

        $print_no = 0;


        $mergedId = $this->orderproxy->where('merged_to', $id)->pluck('id')->toArray();

        $this->orderproxy->whereIn('id', $mergedId)->delete();

        OrderDetailProxy::whereIn('order_id', $mergedId)->delete();

        $edm_no = $this->orderproxy->where(['is_edm' => 1])->orderBy('id', 'desc')->first()->edm_no + 1;


        $this->orderproxy->find($id)->update(['is_edm' => 1, 'ready_status' => 'checkedout', 'is_final_invoice' => 1, 'edm_no' => $edm_no, 'served_waiter' => \Request::get('waiter')]);

        $ord = $this->orderproxy->find($id);

        $imagepath = \Auth::user()->organization->logo;

        return view('admin.orders.edmtemplate', compact('ord', 'imagepath', 'orderDetails', 'print_no', 'edm_no'));
    }

    public function edmlist()
    {

        $clients = Client::pluck('name', 'id');
        $orders = $this->orderproxy->where('is_edm', '1')
            ->where(function ($query) {

                $start_date = \Request::get('start_date');

                $end_date = \Request::get('end_date');

                if ($end_date && $start_date) {

                    return $query->where('transaction_date', '>=', $start_date)
                        ->where('transaction_date', '<=', $end_date);
                }
            })->where(function ($query) {

                $outlet_id = \Request::get('outlet_id');

                if ($outlet_id) {

                    return $query->where('outlet_id', $outlet_id);
                }

            })->where(function ($query) {

                $clients_id = \Request::get('clients_id');


                if ($clients_id) {

                    return $query->where('pos_customer_id', $clients_id);
                }


            })->where(function ($query) {

                $waiter = \Request::get('waiter');
                if ($waiter) {


                    return $query->where('served_waiter', 'LIKE', '%' . $waiter . '%');
                }

            })
            ->orderBy('id', 'desc');

        if (\Request::get('submit') && \Request::get('submit') == 'excel') {

            //excel
            $orders = $orders->get();
            $view = view('admin.hotel.possalesdetail._edmlist_excel', compact('orders'));
            return \Excel::download(new \App\Exports\ExcelExportFromView($view), 'edmlist.xlsx');


        } else {
            $orders = $orders->paginate(30);


        }


        $outlets = \App\Models\PosOutlets::pluck('name', 'id');

        $page_title = 'EDM';

        $page_description = 'All EDM List';
        $pay_method = $this->pay_method;

        return view('admin.hotel.possalesdetail.edmlist', compact('orders', 'page_title', 'page_description', 'outlets', 'pay_method', 'clients'));


    }


    public function moveToRoom($proxyId)
    {


        return view('admin.orders.merge.move_to_room', compact('proxyId'));

    }

    public function moveToRoomPost(Request $request, $proxyId)
    {

        $attributes = $request->all();
        $room_no = $request['room_no'];

        $orderProxy = $this->orderproxy->find($proxyId);


        $reservation = \App\Models\Reservation::find($request['room_reservation_id']);

        if ($reservation->parent_res == null) {

            $reservation_id = $reservation->id;
        } else {

            $reservation_id = $reservation->parent_res;
        }

        $folio = \App\Models\Folio::orderBy('id', 'asc')->where('reservation_id',
            $reservation_id)->first();
        if (!$folio) {
            Flash::error("FOlio not settled");
            return redirect()->back();
        }


        $attributes = [];
        $attributes['subtotal'] = $folio->subtotal + $orderProxy->subtotal;
        $attributes['service_charge'] = $folio->service_charge + $orderProxy->service_charge;
        $attributes['amount_with_service'] = $folio->amount_with_service + $orderProxy->amount_with_service;
        $attributes['discount_percent'] = 0;
        $attributes['taxable_amount'] = $folio->taxable_amount + $orderProxy->taxable_amount;
        $attributes['tax_amount'] = $folio->tax_amount + $orderProxy->tax_amount;
        $attributes['total_amount'] = $folio->total_amount + $orderProxy->total_amount;

        $room_description = $orderProxy->outlet->name;

        \App\Models\Folio::find($folio->id)->update($attributes);

        $detail_attributes = new \App\Models\FolioDetail();
        $detail_attributes->folio_id = $folio->id;
        $detail_attributes->description = $room_description;
        $detail_attributes->price = $orderProxy->subtotal;
        $detail_attributes->flag = 'restaurant';
        $detail_attributes->quantity = 1;
        $detail_attributes->total = $orderProxy->subtotal;
        $detail_attributes->date = \Carbon\Carbon::now();
        $detail_attributes->is_inventory = 0;
        $detail_attributes->posted_to_ledger = 1;
        $detail_attributes->is_posted = 1;
        $detail_attributes->res_id = $reservation->id;
        $detail_attributes->save();

        $mergedTable = $this->orderproxy->where('merged_to', $orderProxy->id)->pluck('id')->toArray();

        $this->orderproxy->find($proxyId)->update(['reservation_id' => $reservation->id, 'room_no' => $reservation->room_num]);

        $this->orderproxy->whereIn('id', $mergedTable)->delete();

        OrderDetailProxy::whereIn('order_id', $mergedTable)->delete();

        OrderDetailProxy::where('order_id', $orderProxy->id)->delete();

        $orderProxy->delete();

        Flash::success("Order Successfully Moved to room");

        return redirect('/admin/hotel/openresturantoutlets');


    }


    public function roomtransferItem()
    {


        $orders = $this->orderproxy->where('reservation_id', "!=", '0')
            ->where(function ($query) {

                $start_date = \Request::get('start_date');

                $end_date = \Request::get('end_date');

                if ($end_date && $start_date) {

                    return $query->where('transaction_date', '>=', $start_date)
                        ->where('transaction_date', '<=', $end_date);
                }
            })->where(function ($query) {

                $outlet_id = \Request::get('outlet_id');

                if ($outlet_id) {

                    return $query->where('outlet_id', $outlet_id);
                }

            })
            ->orderBy('id', 'desc')->withTrashed()->paginate(30);
        $page_title = 'Room Transfer';

        return view('admin.hotel.possalesdetail.roomtranferitem', compact('orders', 'page_title'));


    }


    public function roomtransferEstimate($id)
    {

        $ord = $this->orderproxy->withTrashed()->find($id);
        $orderDetails = OrderDetailProxy::where('order_id', $ord->id)->withTrashed()->get()->groupBy('product_type_id');

        $print_no = 1;

        $imagepath = \Auth::user()->organization->logo;

        if ($ord) {

            if ($ord->is_edm) {

                $view = 'admin.orders.edmtemplate';
            } elseif (!$ord->reservation_id) {
                $view = 'admin.orders.estimateprint';
            } else {
                $view = 'admin.orders.room_transfer_bill_estimate';
            }


            return view($view, compact('ord', 'imagepath', 'orderDetails', 'print_no'));
        }


    }


    public function splitBill($id)
    {


        $order_details = OrderDetailProxy::with('product')->where('order_id', $id)->get();


        return view('admin.orders.split.split', compact('order_details', 'id'));
    }


    public function splitBillSave(Request $request, $id)
    {


        $attributes = $request->all();

        $getProductAndQuantity = function ($arr) {
            $toReturn = [];
            foreach ($arr as $key => $value) {
                if (isset($toReturn[$value])) {

                    $toReturn[$value]['qty'] += 1;
                } else {
                    $toReturn[$value] = [
                        'product_id' => $value,
                        'qty' => 1,
                    ];
                }
            }
            return $toReturn;
        };

        $parentOrder = $this->orderproxy->find($id);

        $fromOrder = $getProductAndQuantity($attributes['from_product']);

        $toOrder = $getProductAndQuantity($attributes['to_product']);

        $saveOrdersArr = function ($orders, $orderInfo, $isSplitted = null) {

            $service_charge = $orders->service_type == 'yes' ? 1 : 0;
            $arrayOrder = $orders->toArray();
            $fromOrderAmounts = \ReservationHelper::calculateRestroAmount($orderInfo, $service_charge);

            $arrayOrder = array_merge($arrayOrder, $fromOrderAmounts);
            $arrayOrder['is_splitted'] = $isSplitted;

            return $arrayOrder;
        };

        $saveOrderDetails = function ($orders, $orderInfo) {

            OrderDetailProxy::where('order_id', $orders->id)->delete(); //delete previous orders;

            foreach ($orderInfo as $key => $value) {
                $detail = new OrderDetailProxy();
                $temp_product = \App\Models\Product::find($value['product_id']);
                $detail->order_id = $orders->id;
                $detail->product_id = $temp_product->id;
                $detail->product_type_id = $temp_product->product_type_id;
                $detail->price = $temp_product->price;
                $detail->quantity = $value['qty'];
                $detail->remarks = 'splitted';
                $detail->tax = '';
                $detail->tax_amount = null;
                $detail->total = $temp_product->price * $value['qty'];
                $detail->date = date('Y-m-d H:i:s');
                $detail->is_inventory = 1;
                $detail->save();
            }

            return true;
        };

        $parentOrder->update($saveOrdersArr($parentOrder, $fromOrder, $parentOrder->is_splitted));

        $saveOrderDetails($parentOrder, $fromOrder);

        $newOrder = $this->orderproxy->create($saveOrdersArr($parentOrder, $toOrder, '1'));

        $saveOrderDetails($newOrder, $toOrder);


        Flash::success("Bill Successfully Splited");

        return redirect()->back();


    }


}

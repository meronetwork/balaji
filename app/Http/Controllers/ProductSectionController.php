<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductsSection;
use Flash;
class ProductSectionController extends Controller
{
    public function productSectionIndex(){
        $page_title = "Admin | show";
        $productsection= ProductsSection::all();
        return view('admin.productsection.product-section-index',compact('productsection','page_title'));
    }
    public function productSection(){
      $page_title = "Admin | create";
      $description ="Create product section";   
      return view('admin.productsection.product-section-create',compact('description','page_title'));
    }
    public function productSectionStore(Request $request){
       $_section = $request->all();
       $product_section = ProductsSection::create($_section);
        Flash::success('Product Section Added');
        return redirect('/admin/productsection/product-section-index/');
    }
    public function deleteproductSectionModal($id){
        $error = null;
        $product_section = ProductsSection::find($id);
        $modal_title = "Delete product section";
        $modal_body ="Are you sure that you want to delete product section ".$product_section->id ."? This operation is irreversible";
        $modal_route = route('admin.productsection.delete-prodsection',$product_section->id);
        return view('modal_confirmation',compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
    public function deleteproductSection($id){
        $d = ProductsSection::find($id)->delete();
        Flash::success('Product section SucessFully Deleted !!');
        return redirect('/admin/productsection/product-section-index/');
    }
    public function editproductSection($id){
        $page_title = "Admin | edit";
        $description ="Edit product section";  
        $edit = ProductsSection::find($id);
        return view('admin.productsection.product-section-create',compact('description','edit','page_title'));
    }
    public function updateproductSection($id,Request $request){
        $product_section = $request->all();
        ProductsSection::find($id)->update($product_section);
        Flash::success('Product section SucessFully Updated !!');
        return redirect('/admin/productsection/product-section-index/');
    }
}

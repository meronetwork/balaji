<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use App\Models\PrivilegeCard;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class PrivilegeCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $privilegeCard;
    public function __construct(PrivilegeCard $privilegeCard)
    {
        $this->privilegeCard=$privilegeCard;
    }

    public function index()
    {
        $page_title = "Admin | Privilege Card";
        $isindex = true;
        $privilegeCards = $this->privilegeCard->orderBy('id','desc')
            ->paginate(30);
        return view('admin.privilegeCard.index',compact('steps','isindex','page_title','privilegeCards'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Admin | Privilege Card | Create";
        $page_description = "Creating a Recipe Step Ingredient";
        return view('admin.privilegeCard.create', compact(  'page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'dob' => 'required',
            'occupation' => 'required',
        ]);
//        dd($request->file());
        $attributes = $request->all();
        if ($request->file('image')) {
            $stamp = time();
            $imageUploadPath = '/images/';
            $file = \Request::file('image');
            $destinationPath = public_path() . $imageUploadPath;
            $filename = $file->getClientOriginalName();
            \Request::file('image')->move($destinationPath, $stamp . $filename);

            $image = \Image::make($destinationPath . $stamp . $filename)
                ->save($destinationPath . $stamp . $filename);

            $attributes['image'] = $stamp . $filename;
        }

        $this->privilegeCard->create($attributes);
        Flash::success('Privilege Card created Successfully.');

        return redirect('/admin/privilege-card');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page_title = "Admin | Privilege Card | show";
        $page_description ="Privilege Card";
        $privilegeCard=$this->privilegeCard->find($id);

        return view('admin.privilegeCard.show', compact(  'page_title', 'page_description', 'privilegeCard'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Admin | Privilege Card | Edit";

        $page_description = "Update Privilege Card";

        $privilegeCard=$this->privilegeCard->find($id);

        return view('admin.privilegeCard.edit', compact(  'page_title', 'page_description','privilegeCard'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'dob' => 'required',
            'occupation' => 'required',
        ]);
       $privilegeCard=$this->privilegeCard->find($id);
        $attributes = $request->all();
        if ($request->file('image')) {
            if ($privilegeCard->image) {
                $old_file = public_path() . '/images/privilege-card/' . $privilegeCard->image;
                File::delete($old_file);
            }
            $stamp = time();
            $imageUploadPath = '/images/privilege-card/';
            $file = \Request::file('image');
            $destinationPath = public_path() . $imageUploadPath;
            $filename = $file->getClientOriginalName();
            \Request::file('image')->move($destinationPath, $stamp . $filename);

            $image = \Image::make($destinationPath . $stamp . $filename)
                ->save($destinationPath . $stamp . $filename);

            $attributes['image'] = $stamp . $filename;
        }

        $this->privilegeCard->find($id)->update($attributes);
        Flash::success('Privilege Card Updated Successfully.');

        return redirect('/admin/privilege-card');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $privilegeCard= $this->privilegeCard->find($id);
//        \TaskHelper::authorizeOrg($supportJob);
        if (!$privilegeCard->isDeletable()) {
            abort(403);
        }

        $privilegeCard->delete();

//        MasterComments::where('type', 'orders')->where('master_id', $id)->delete();

        Flash::success('Privilege Card deleted.');

//        if (\Request::get('type')) {
//            return redirect('/admin/orders?type='.\Request::get('type'));
//        }

        return redirect('/admin/privilege-card');
    }

    public function deleteModal($id)
    {
        $error = null;

        $privilegeCard = $this->privilegeCard->find($id);

        if (! $privilegeCard->isDeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Privilege Card';

        $privilegeCard = $this->privilegeCard->find($id);
        if (\Request::get('type')) {
            $modal_route = route('admin.privilege-card.delete', ['id' => $privilegeCard->id]).'?type='.\Request::get('type');
        } else {
            $modal_route = route('admin.privilege-card.delete', ['id' => $privilegeCard->id]);
        }

        $modal_body = 'Are you sure you want to delete this Privilege Card?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function Search(Request $request){
        $key = $request->search;
        $page_title = "Admin | show";
//        $recipe = Recipe::select('recipes.id','recipes.name','recipes.description','fact_batches.product_id')->leftjoin('products','fact_batches.product_id','=','products.id')->where('fact_batches.id','LIKE','%'.$key.'%')->orWhere('products.name','LIKE','%'.$key.'%')->orWhere('fact_batches.max_qty','LIKE','%'.$key.'%')->orWhere('fact_batches.batch_no','LIKE','%'.$key.'%')->orderBy('fact_batches.id','desc')->paginate(30);
        $recipes = Recipe::select('recipes.id','recipes.name','recipes.description')->where('recipes.id','LIKE','%'.$key.'%')->orWhere('recipes.name','LIKE','%'.$key.'%')->orWhere('recipes.description','LIKE','%'.$key.'%')->orderBy('recipes.id','desc')->paginate(30);
        return view('admin.factoryBatch.index',compact('factoryBatches','page_title'));
    }
    public function printCard($id)
        {
            $privilegeCard = $this->privilegeCard->find($id);
            return view('admin.privilegeCard.print', compact('privilegeCard'));
        }

    public function download($id){


        $privilegeCard = $this->privilegeCard->find($id);
        $brandLogo= base64_encode(file_get_contents(public_path('/images/privilege-card/indreni_logo.png')));
        $banner= base64_encode(file_get_contents(public_path('/images/privilege-card/privilege.png')));
        $profile= base64_encode(file_get_contents(public_path('/images/privilege-card/'.$privilegeCard->image)));
//        return view('admin.privilegeCard.pdf', compact('privilegeCard','brandLogo','banner','profile'));
        $pdf = \PDF::loadView('admin.privilegeCard.pdf', compact('privilegeCard','brandLogo','banner','profile'))->setPaper('a4', 'portrait');
        $file = 'Privilege-card'.date('_Y_m_d').'.pdf';
        if (File::exists('reports/'.$file)) {
            File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
    }


}

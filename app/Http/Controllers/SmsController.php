<?php

namespace App\Http\Controllers;

use App\Models\Role as Permission;
use App\Models\Sms as Sms;
use Flash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    /**
     * @param
     * @param Permission $permission
     * @param User $user
     */
    public function __construct(Sms $sms, Permission $permission)
    {
        parent::__construct();
        $this->sms = $sms;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $page_title = trans('admin/sms/general.page.send.title'); // "Admin | SMS | Create";
        $page_description = trans('admin/sms/general.page.send.description'); // "Creating a new course";

        $perms = $this->permission->all();

        return view('admin.sms.send', compact('perms', 'page_title', 'page_description'));
    }

    /**
     * Send SMS - Outbound.
     *
     * @return Response
     */
    public function postSend()
    {
        $recipients = str_replace(' ', ',', trim(Request::get('recipient')));
        $message = trim(Request::get('message')).' - '.env('APP_COMPANY');
        //dd($text);
        $ch = curl_init();
        $curlUrl = 'https://smsprima.com/api/api/index?username=luckyt&password=12345678&sender=LuckyTravel&destination='.$recipients.'&type=1&message='.urlencode($message).''; // api url as provided in the document.
        curl_setopt($ch, CURLOPT_URL, $curlUrl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        //dd($response);
        if (! curl_errno($ch)) {
            Flash::success(trans('admin/sms/general.status.sms-sent'));
        } else {
            Flash::warning(trans('admin/sms/general.status.no-msg'));
        }
    }

    public function getBulkSMSForm()
    {
        $page_title = 'Send the Bulk SMS';
        $page_description = 'Send the Bulk SMS to all the users';

        $courses = \App\Models\Product::select('id', 'name')->where('enabled', '1')->get();
        $lead_status = \App\Models\Leadstatus::select('id', 'name')->where('enabled', '1')->get();

        return view('admin.sms.bulkSMSForm', compact('page_title', 'page_description', 'courses', 'lead_status'));
    }

    public function postBulkSMS(Request $request)
    {
        $this->validate($request, ['message' => 'required']);
        $error = 0;
        $start = \Request::get('start_date');
        $end = \Request::get('end_date');

        $leads = \App\Models\Lead::select('mob_phone')
                            ->where('mob_phone', '!=', '')
                            ->where('enabled', '1');

        if ($start != '' && $end != '') {
            $leads->whereBetween('created_at', [$start, $end]);
        }
        if ($request['course_id'] != 0) {
            $leads->where('product_id', $request['course_id']);
        }
        if ($request['status_id'] != 0) {
            $leads->where('status_id', $request['status_id']);
        }

        $allLeads = $leads->pluck('mob_phone');
        $recipients = implode(',', $allLeads->toArray());

        $message = trim(\Request::get('message')).' - '.env('APP_COMPANY');
        //dd($text);
        $ch = curl_init();
        $curlUrl = 'https://smsprima.com/api/api/index?username=luckyt&password=12345678&sender=LuckyTravel&destination='.$recipients.'&type=1&message='.urlencode($message).''; // api url as provided in the document.
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        //dd($response);
        if(! curl_errno($ch)){
            Flash::success(trans('admin/sms/general.status.sms-sent'));
        }else{
            Flash::warning('Error in sending SMS. Please try again.');
        }

        return Redirect::back();
    }

    public function getBulkSMSContact()
    {
        $page_title = 'Send the Bulk SMS to Contacts';
        $page_description = 'Send the Bulk SMS to all the contacts';
        $clients = \App\Models\Client::select('name', 'id')->where('enabled', '1')->get();

        return view('admin.sms.bulkSMSContact', compact('page_title', 'page_description', 'clients'));
    }

    public function postBulkSMSContact(Request $request)
    {
        $this->validate($request, ['message'  => 'required',
        ]);

        //dd($request->message);


        $error = 0;
        $start = \Request::get('start_date');
        $end = \Request::get('end_date');

        if ($start != '' && $end != '') {
            if ($request['client_id'] != '0') {
                $clients = \App\Models\Contact::whereBetween('created_at', [$start, $end])->where('phone', '!=', '')->where('enabled', '1')->where('client_id', $request['client_id'])->pluck('phone');
            } else {
                $clients = \App\Models\Contact::whereBetween('created_at', [$start, $end])->where('phone', '!=', '')->where('enabled', '1')->pluck('phone');
            }
        } else {
            if ($request['client_id'] != '0') {
                $clients = \App\Models\Contact::where('phone', '!=', '')->where('enabled', '1')->where('client_id', $request['client_id'])->pluck('phone');
            } else {
                $clients = \App\Models\Contact::where('phone', '!=', '')->where('enabled', '1')->pluck('phone');
            }
        }

        $recipients = implode(',', $clients->toArray());
        //dd($recipients);
        $message = trim(\Request::get('message')).' - '.env('APP_COMPANY');
        //dd($text);
        $ch = curl_init();
        $curlUrl = 'https://smsprima.com/api/api/index?username=luckyt&password=12345678&sender=LuckyTravel&destination='.$recipients.'&type=1&message='.urlencode($message).''; // api url as provided in the document.
        curl_setopt($ch, CURLOPT_URL, $curlUrl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        //dd($response);
        if (! curl_errno($ch)) {
            Flash::success(trans('admin/sms/general.status.sms-sent'));
        } else {
            Flash::warning("Message Cannot Be at the Moment");
        }
        return Redirect::back();
    }
}

<?php namespace App\Http\Controllers;

use App\Models\FactoryBatch;
use App\Models\FactoryIssue;
use App\Models\FactoryProductLedger;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductUnit;
use App\Models\ProductsSection;
use App\Models\IssueSlip;
use App\Models\IssueSlipsDetail;
use App\Models\Process;
use App\Models\ProcessNewRaw;
use App\Models\ProcessRawUsed;
use Flash;
use DB;
use App\Models\Product;
use App\Models\StockMove;
class ProductionController extends Controller
{

    public function warehouseIndex(){
        $page_title = "Admin | Warehouse Issue";
        $isindex = true;
        $slips = [];
        $statusColor = [

        'started'=>'label-primary',
        'finished'=>'label-success',
        'scheduled'=>'label-warning',
        'paused'=>'label-info'

        ];



        $slips = FactoryIssue::orderBy('id','desc')->paginate(30);
        return view('admin.production.issue_warehouse.index',compact('slips','isindex','page_title','statusColor'));
    }

    public function statistics(){
        $page_title = "Admin | Warehouse Issue";
        $isindex = true;
        $slips = [];
        $statusColor = [

        'started'=>'label-primary',
        'finished'=>'label-success',
        'scheduled'=>'label-warning',
        'paused'=>'label-info'

        ];



        $slips = FactoryIssue::orderBy('id','desc')->paginate(30);
        return view('admin.production.issue_warehouse.statistics',compact('slips','isindex','page_title','statusColor'));
    }

    public function warehouseCreate(){
        $page_title = "Admin | Warehouse create";
        $product_location = \App\Models\ProductLocation::all();
        $product_section =ProductsSection::all();
        $final_products = \App\Models\Product::where('assembled_product',1)->get();
        // dd($final_products);
        $users = \App\User::where('enabled','1')->get();
        return view('admin.production.issue_warehouse.create',compact('product_location','product_section','final_products','page_title','users'));
    }

    public function getBomCost($id,$qty,$bom_id = false){

        return \StockHelper::getBomCost($id,$qty,$bom_id);
    }




    public function productStockMoveSingle($value,$isuueWarehouse){ //$value means  FactIssueBOM both must be object

        $stockMove = new StockMove();
        $stockMove->stock_id = $value->raw_product_id;
        $stockMove->tran_date = \Carbon\Carbon::now();
        $stockMove->user_id = \Auth::user()->id;
        $stockMove->reference = 'store_out_' . $isuueWarehouse->id;
        $stockMove->transaction_reference_id = $isuueWarehouse->id;
        $stockMove->qty = '-' . $value->qty * $isuueWarehouse->qty;
        $stockMove->trans_type = RAWMATERIALOUT;
        $stockMove->order_no = $isuueWarehouse->id;
        $stockMove->location = '';
        $stockMove->order_reference = $isuueWarehouse->id;
        $stockMove->save();

        return $stockMove;


    }

    public function stockMoves($issue_id){

        $stockDetails = \App\Models\FactIssueBOM::where('issue_id',$issue_id)->get();
        
        $isuueWarehouse =  FactoryIssue::find($issue_id);  

        StockMove::where('trans_type',RAWMATERIALOUT)->where('order_no',$issue_id)
            ->delete();

        foreach ($stockDetails as $key => $value) {
            // create stockMove
            $this->productStockMoveSingle($value,$isuueWarehouse);
        }


        return true;
            




    }

    public function warehouseStore(Request $request){


        $attributes = $request->all();

        $total_cost = $this->getBomCost($request->product_id,$request->qty,
            $request->selected_bom)[0];

        $attributes['total_cost'] = $total_cost['approxCost'];

        $boomInfo = \App\Models\FactBOM::find($request->selected_bom);

        $routingInfo = $boomInfo->routing;

        if(!$routingInfo){

            Flash::error("Please Configure Route first ");

            return redirect()->back();
        }

        $warehouse = FactoryIssue::create($attributes);

        $FactIssueBOMInsert = [];

        foreach( $boomInfo->factBomDetail as $key=>$value ){

            $FactIssueBOMInsert [] = [

                'issue_id'=>$warehouse->id,
                'raw_product_id'=>$value->raw_prod_id,
                'qty'=>$value->qty,

            ];
            
    
        }

        \App\Models\FactIssueBOM::insert($FactIssueBOMInsert);

        $FactIssueRoutingInsert = [];

        foreach( $routingInfo->routingDetails as $key=>$value ){

            $FactIssueRoutingInsert [] = [
                'issue_id'=>$warehouse->id,
                'area_id'=>$value->area_id,
                'description'=>$value->descr,
                'setup_time'=>$value->setup_time,
                'cycle_time'=>$value->cycle_time,
                'overhead_cost'=>$value->overhead_cost,
                'labour_cost'=>$value->labour_cost,
                'to_user_id'=>\Auth::user()->id,
                'process_order'=>$value->process_order

            ];
        }

        \App\Models\FactIssueRouting::insert($FactIssueRoutingInsert);
        
        $this->stockMoves($warehouse->id);

        Flash::success("Warehouse Successfully Stored");
        
        return redirect(route('admin.production.issue-warehouse-show',$warehouse->id));

    }




    public function showWarehouse($id){


        $warehouse =  FactoryIssue::find($id);  
        $factRouteDetails = $warehouse->issueRouteDetail->sortBy('process_order');
        $bomDetails = $warehouse->issueBomDetail;
        $page_title = "Production | SLIP";
        $products = \App\Models\Product::where('enabled','1')->pluck('name','id');
        return view('admin.production.issue_warehouse.show',compact('warehouse','bomInfo','bomDetails','page_title','users','factRouteDetails','factRoute','products'));
    }


    public function getCostFromIssued($issue_id){

        $warehouse =  FactoryIssue::find($issue_id);

        $getBoomDetails = $warehouse->issueBomDetail;
        
        $partCost = 0;

        foreach ($getBoomDetails  as $k => $dt) {
            
            $productInfo = $dt->product;

            $partCost += $productInfo->price * $dt->qty;
        }

        $approxCost = $partCost * $warehouse->qty;

        return $approxCost;


    }



    public function warehouseUpdateProd(Request $request,$issue_id){

        $warehouse =  FactoryIssue::find($issue_id);
        
        \App\Models\FactIssueBOM::where('issue_id',$issue_id)->delete();

        $FactIssueBOMInsert = [];

        foreach( $request->product_id as $key=>$value ){
                                                    
           $FactIssueBOMInsert[]  = [
                'issue_id'=>$issue_id,
                'raw_product_id'=>$value,
                'qty'=>$request->qty[$key],

            ];         
    
        }
      
        \App\Models\FactIssueBOM::insert($FactIssueBOMInsert);

        $this->stockMoves($issue_id);

        $warehouse->update([
            'total_cost'=>$this->getCostFromIssued($issue_id),
        ]);
        Flash::success("Product Successfully Updated");

        return redirect()->back();


    }


    public function warehouseReturnProduct($issue_id,$id){


        $warehouse =  FactoryIssue::find($issue_id);




        \App\Models\FactIssueBOM::find($id)->delete();






        dd($issue_id,$id);



    }


    public function production_plan($id){

        $warehouse =  FactoryIssue::find($id);
      
        $factRouteDetails = $warehouse->issueRouteDetail;
        $bomDetails = $warehouse->issueBomDetail;
        $page_title = "Production | SLIP";

        return view('admin.production.issue_warehouse.production',compact('warehouse','bomInfo','bomDetails','page_title','users','factRouteDetails','factRoute'));


    }


    public function startProduction(Request $request){


        $warehouse = FactoryIssue::find($request->fact_id);

        $factRouteDetails = $warehouse->issueRouteDetail->where('id',$request->route_detail_id)->first();
   
        if(strtotime($factRouteDetails->actual_start)){

            $toUpdate = [
                'is_paused'=>0,
            ];
        }else{

            $toUpdate = [
                'actual_start'=>date('Y-m-d H:i:s'),
                'is_paused'=>0,
            ];

        }
        $factRouteDetails->update($toUpdate);

        $warehouse->update([

            'status'=>'started',

        ]);
        Flash::success("Production #".$factRouteDetails->description.' Started');
        return redirect()->back();



    }


    public function pauseProduction(Request $request){
        $warehouse = FactoryIssue::find($request->fact_id);

        $factRouteDetails =$warehouse->issueRouteDetail->where('id',$request->route_detail_id)->first();

        $factRouteDetails->update([
            'actual_end'=>date('Y-m-d H:i:s'),
            'actutal_qty'=>$request->qty + $factRouteDetails->actutal_qty,
            'is_paused'=>'1',
        ]);

        $warehouse->update([

            'status'=>'paused',

        ]);
        Flash::success("Production ".$factRouteDetails->description.' Started');
        return redirect()->back();

    }

    public function stopProduction(Request $request){
        $warehouse = FactoryIssue::find($request->fact_id);

        $factRouteDetails =$warehouse->issueRouteDetail->where('id',$request->route_detail_id)->first();

        $factRouteDetails->update([
            'actual_end'=>date('Y-m-d H:i:s'),
            'actutal_qty'=>$request->qty + $factRouteDetails->actutal_qty,
            'is_stop'=>'1',
        ]);

        $warehouse->update([

            'status'=>'paused',

        ]);
        Flash::success("Production #".$factRouteDetails->description.' Started');
        return redirect()->back();

    }


    public function finishProduction(Request $request,$id){

        $warehouse = FactoryIssue::find($id);

        $checkInComplete = \App\Models\FactRoutingDetail::where('routing_id',$request->route_id)->where('is_stop','!=','1')->exists();

        if($checkInComplete){

            Flash::error("Finish all Process First");
            return redirect()->back();
        }

        $warehouse->update([

            'status'=>'finished',

        ]);

        return redirect()->back();



    }


    public function lot_print($id){


        $warehouse = FactoryIssue::find($id);

        $pdf = \PDF::loadView('admin.production.issue_warehouse.lot_pdf', compact('warehouse'));
        $id = sprintf('%03d',$warehouse->id);
        $file = "lot_{$id}.pdf";

        return $pdf->download($file);
        return view('admin.production.issue_warehouse.lot_pdf',compact('warehouse'));

    }

    public function target_lot_print($id){

        $warehouse = FactoryIssue::find($id);

        $pdf = \PDF::loadView('admin.production.issue_warehouse.target_lot_print', compact('warehouse'));
        $id = sprintf('%03d',$warehouse->id);
        $file = "lot_{$id}.pdf";

        return $pdf->download($file);
        return view('admin.production.issue_warehouse.lot_pdf',compact('warehouse'));


    }



    //  ajax call
    public function checkStock(Request $request,$id){
        $issue_id = $request->input('issue_id');
        if($issue_id){
            $total = DB::select("select sum(product_stock_moves.qty) as total,products.unit,products.price,product_units.symbol,product_units.id as product_units_id from product_stock_moves LEFT JOIN  products ON product_stock_moves.stock_id = products.id LEFT JOIN product_units ON products.product_unit = product_units.id where product_stock_moves.stock_id ='$id'");
            $qty  = FactoryProductLedger::where([['type','WAREHOUSE'],['issue_id',$issue_id],['product_id',$id]])->pluck('debit_qty');
            $total[0]->total = $total[0]->total + $qty[0];
        }else{
            $total = DB::select("select sum(product_stock_moves.qty) as total,products.unit,products.price,product_units.symbol,product_units.id as product_units_id  from product_stock_moves LEFT JOIN  products ON product_stock_moves.stock_id = products.id LEFT JOIN product_units ON products.product_unit = product_units.id where product_stock_moves.stock_id ='$id'");
        }
        if ($total[0]->symbol == null){
            $product = Product::select('products.unit','products.price','product_units.symbol','product_units.id as product_units_id')
                ->leftjoin('product_units','products.product_unit','=','product_units.id')
                ->find($id);
            $total[0]=[
                'total' => null,
                'unit' => $product->unit,
                'price' => $product->price,
                'symbol' => $product->symbol,
                'product_units_id' => $product->product_units_id,
            ];
        }
        return json_encode($total);
    }
    public function getFactoryStock(Request $request,$id){
        $issue_id = $request->input('issue_id');
        if($issue_id){
            $total = FactoryProductLedger::select('fact_product_ledger.id','product_id','products.name','products.unit',DB::raw('SUM(credit_qty) as credit_qty'),DB::raw('SUM(debit_qty) as debit_qty'),'products.unit','product_units.symbol','product_units.id as product_units_id')
                ->leftjoin('products','products.id','=','fact_product_ledger.product_id')
                ->leftjoin('product_units','products.product_unit','=','product_units.id')
                ->where('type','SECTION')->where('description','<>','PROCESS-')->where('product_id',$id)->where('issue_id','<>',$issue_id)
                ->groupBy('product_id')->get()->toArray();
            $total[0]['total'] = $total[0]['credit_qty'] - $total[0]['debit_qty'];
        }else{
            $total = FactoryProductLedger::select('fact_product_ledger.id','product_id','products.name','products.unit',DB::raw('SUM(credit_qty) as credit_qty'),DB::raw('SUM(debit_qty) as debit_qty'),'products.unit','product_units.symbol','product_units.id as product_units_id')
                ->leftjoin('products','products.id','=','fact_product_ledger.product_id')
                ->leftjoin('product_units','products.product_unit','=','product_units.id')
                ->where('type','SECTION')->where('description','<>','PROCESS-')->where('product_id',$id)
                ->groupBy('product_id')->get()->toArray();
            $total[0]['total'] = $total[0]['debit_qty'] > 0 ? $total[0]['credit_qty'] - $total[0]['debit_qty']:$total[0]['credit_qty'];
        }

//        $total = DB::select("select sum(product_stock_moves.qty) as total,products.unit,product_units.symbol from product_stock_moves LEFT JOIN  products ON product_stock_moves.stock_id = products.id LEFT JOIN product_units ON products.product_unit = product_units.id where product_stock_moves.stock_id ='$id'");
        return json_encode($total);
    }







}

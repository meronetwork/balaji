<?php

namespace App\Http\Controllers;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class LostAndFoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Admin | Lost and Found | Index';

        $page_description = 'List of Lost and Founds';

        $lost_and_founds = \App\Models\LostAndFound::orderBy('id', 'desc')->paginate(20);

        return view('admin.lost-and-found.index', compact('lost_and_founds', 'page_title', 'page_description'));
    }

    public function create()
    {
        $page_title = 'Create | Lost and Found';
        $page_description = '';

        return view('admin.lost-and-found.create', compact('page_title', 'page_description'));
    }

    public function store(Request $request)
    {
//        $this->validate($request, [
//            'name'      => 'required',
//
//        ]);

        $attributes = $request->all();

        $explode=explode(' ',$attributes['date']);
        $attributes['date']=date('Y-m-d', strtotime($explode[0]));;
        $attributes['time']=$explode[1];
        $attributes['user_id']=auth()->user()->id;
        $transactiongroups = \App\Models\LostAndFound::create($attributes);
        Flash::success('Lost/Found record successfully added');


        return redirect('/admin/lost-and-founds');
    }

    public function edit($id)
    {
        $lost_and_found = \App\Models\LostAndFound::find($id);

        $page_title = 'Edit | Lost and Found';
        $page_description = '';

        return view('admin.lost-and-found.edit', compact('lost_and_found', 'page_title', 'page_description'));
    }

    public function update(Request $request, $id)
    {
//        $this->validate($request, [
//            'name'      => 'required',
//
//        ]);

        $attributes = $request->all();
        $explode=explode(' ',$attributes['date']);
        $attributes['date']=date('Y-m-d', strtotime($explode[0]));;
        $attributes['time']=$explode[1];
        \App\Models\LostAndFound::find($id)->update($attributes);

        Flash::success('Lost/Found record successfully updated');

        return redirect('/admin/lost-and-founds');
    }

    public function getModalDelete($id)
    {
        $error = null;
        $lost_and_found = \App\Models\LostAndFound::find($id);

        $modal_title = 'Delete Lost/Found Record';
        $modal_body = 'Are you sure that you want to delete Lost/Found Record with id '.$lost_and_found->id.' of the room number '.$lost_and_found->room_no.'? This operation is irreversible';

        $modal_route = route('admin.lostAndFound.delete', $lost_and_found->id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function destroy($id)
    {
         \App\Models\LostAndFound::find($id)->delete();
        Flash::success('Lost/Found record successfully deleted');

        return redirect('/admin/lost-and-founds');
    }
    public function getReservation()
    {
        $term = strtolower(\Request::get('term'));
        $products = \App\Models\Reservation::select('id', 'guest_name','room_num','phone','email')
            ->whereNull('check_out')
            ->where(function ($q) use ($term) {
                $q->where('guest_name', 'LIKE', '%'.$term.'%')
                    ->orWhere('room_num', 'LIKE', '%'.$term.'%');
            })
            ->groupBy('guest_name')->latest()->take(5)->get();
        $return_array = [];

        foreach ($products as $v) {
            $return_array[] = ['guest_name' => $v->guest_name, 'id' =>$v->id,'value'=>$v->room_num,
                'phone'=>$v->phone,'email'=>$v->email];
        }

        return Response::json($return_array);
    }
}

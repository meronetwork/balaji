<?php

namespace App\Http\Controllers;

use App\Models\AbbrOrderDetail;
use App\Models\AbbrOrders;
use App\Models\Role as Permission;
use App\Models\Audit as Audit;
use Flash;
use DB;
use Auth;

use App\User;
use App\Models\EmployeePayroll;
use App\Models\SalaryAllowance;
use App\Models\SalaryDeduction;
use App\Models\SalaryPayment;
use App\Models\SalaryPaymentAllowance;
use App\Models\SalaryPaymentDeduction;
use App\Models\SalaryPaymentDetail;
use App\Models\SalaryPayslip;
use App\Models\SalaryTemplate;
use App\Models\Department;
use App\Models\Paymentmethod;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeAward;
use App\Models\Payment;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER
 */
class PurchaseSalePaymentController extends Controller
{
    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {
        parent::__construct();
        $this->permission = $permission;
    }


    public function PurchasePaymentlist(Request $request, $id)
    {

        $purchase_id = $id;

        $purchase_detail = \App\Models\PurchaseOrder::find($id);


        if ($purchase_detail->supplier_type != 'supplier') {
            Flash::error('Cash Equivalent Payment Cannot be made');
            return redirect()->back();
        }

        $purchase_name = $purchase_detail->client->name;

        $payment_list = \App\Models\Payment::where('purchase_id', $id)->orderby('id', 'desc')->get();
        $page_title = 'Purchase Payment List';

        $page_description = 'Payment List Of ' . $purchase_name . '';

        return view('admin.purchase.paymentlist', compact('page_title', 'page_description', 'purchase_id', 'payment_list'));
    }

    public function SalePaymentlist(Request $request, $id)
    {
        $purchase_id = $id;
        $payment_list = \App\Models\Payment::where('sale_id', $id)->orderby('id', 'desc')->get();

        $order_detail = \App\Models\Orders::find($id);

        $lead_name = $order_detail->lead->name;

        $page_title = 'Sales Payment List';

        $page_description = 'Payment List Of ' . $lead_name . ' Order No ' . $id . '';

        return view('admin.orders.paymentlist', compact('page_title', 'page_description', 'purchase_id', 'payment_list'));
    }

    public function PurchasePaymentcreate(Request $request, $id)
    {

        $page_title = 'Purchase Payment Create';
        $page_description = 'create payments of purchase';
        $purchase_id = $id;

        $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');

        $purchase_order = \App\Models\PurchaseOrder::where('id', $id)->first();
        $purchase_total = $purchase_order->total;
        $paid_amount = DB::table('payments')->where('purchase_id', $id)->sum('amount');
        $payment_remain = $purchase_total - $paid_amount;


        return view('admin.purchase.paymentcreate', compact('page_title', 'payment_remain', 'page_description', 'purchase_id', 'payment_method'));
    }

    public function SalePaymentcreate(Request $request, $id)
    {
        if ($request->invoice_type == 'abbr') {
            $ordersmeta = \App\Models\AbbrOrderMeta::where('order_id', $id)->first();

            $orders = \App\Models\AbbrOrders::find($id);
            if ($ordersmeta->is_bill_active != 1) {
                Flash::error('This Bill is not Active');
                return redirect()->back();
            } elseif ($ordersmeta->settlement == 1) {
                Flash::error('This Bill is already settled unsettle first');
                return redirect()->back();
            }

            $page_title = 'POS Settlement';
            $page_description = 'for ' . $orders->outlet->name . ' of ORD' . $id . '';
            $sale_id = $id;

            $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');

            $purchase_order = \App\Models\AbbrOrders::where('id', $id)->first();
            $purchase_total = $purchase_order->total_amount;
            $paid_amount = DB::table('abbr_payments')->where('sale_id', $id)->where('type','abbr')->sum('amount');
            $payment_remain = $purchase_total - $paid_amount;

            return view('admin.orders.paymentcreate', compact('page_title', 'page_description', 'sale_id', 'payment_method', 'payment_remain'));
        }
        $ordersmeta = \App\Models\OrderMeta::where('order_id', $id)->first();

        $orders = \App\Models\Orders::find($id);
        if ($ordersmeta->is_bill_active != 1) {
            Flash::error('This Bill is not Active');
            return redirect()->back();
        } elseif ($ordersmeta->settlement == 1) {
            Flash::error('This Bill is already settled unsettle first');
            return redirect()->back();
        }

        $page_title = 'POS Settlement';
        $page_description = 'for ' . $orders->outlet->name . ' of ORD' . $id . '';
        $sale_id = $id;

        $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');

        $purchase_order = \App\Models\Orders::where('id', $id)->first();
        $purchase_total = $purchase_order->total_amount;
        $paid_amount = DB::table('payments')->where('sale_id', $id)
            ->where(function ($q){
                $q->where('type',null)->orWhere('type','');

            })->sum('amount');
        $payment_remain = $purchase_total - $paid_amount;

        return view('admin.orders.paymentcreate', compact('page_title', 'page_description', 'sale_id', 'payment_method', 'payment_remain'));
    }

    public function PurchasePaymentPost(Request $request, $id)
    {
        $attributes = $request->all();

        $attributes['paid_by'] = $request->payment_method;

        $attributes['created_by'] = \Auth::user()->id;

        $attributes['bill_amount'] = $request->amount - $request->tds; //this amount will be used to adjust bill amount

        if ($request->file('attachment')) {
            $stamp = time();
            $file = $request->file('attachment');
            //dd($file);
            $destinationPath = public_path() . '/attachment/';
            $filename = $file->getClientOriginalName();
            $request->file('attachment')->move($destinationPath, $stamp . '_' . $filename);

            $attributes['attachment'] = $stamp . '_' . $filename;
        }

        $payment = \App\Models\Payment::create($attributes);

        $paid_amount = DB::table('payments')->where('purchase_id', $id)->sum('amount');

        $purchase_order = \App\Models\PurchaseOrder::find($id);

        if ($paid_amount >= $purchase_order->total) {
            $attributes_purchase['payment_status'] = 'Paid';
            $purchase_order->update($attributes_purchase);
        } elseif ($paid_amount <= $purchase_order->total && $paid_amount > 0) {
            $attributes_purchase['payment_status'] = 'Partial';
            $purchase_order->update($attributes_purchase);
        } else {
            $attributes_purchase['payment_status'] = 'Pending';
            $purchase_order->update($attributes_purchase);
        }

        $paid_amount = $request->amount;

        $this->creatOrUpdatePuchPaymentEntries($payment, $paid_amount, $purchase_order, $request);


        Flash::success('Payment Created');

        return redirect('/admin/payment/purchase/' . $id . '');
    }

    public function creatOrUpdatePuchPaymentEntries($payments, $paid_amount, $purchase_order, $request)
    {
        $attributes['entrytype_id'] = '2'; //receipt
        $attributes['tag_id'] = '24'; //bill payment
        $attributes['user_id'] = \Auth::user()->id;
        $attributes['org_id'] = \Auth::user()->org_id;
        $attributes['number'] = $paymentId;
        $attributes['date'] = \Carbon\Carbon::today();
        $attributes['dr_total'] = $paid_amount;
        $attributes['cr_total'] = $paid_amount;
        $attributes['source'] = 'Auto Payment';
        $attributes['fiscal_year_id'] = \FinanceHelper::cur_fisc_yr()->id;
        $entry = \App\Models\Entry::find($payments->entry_id);

        if ($entry) { //updateEntrues

            $entry = $entry->update($attributes);
            $entry = \App\Models\Entry::find($payments->entry_id);
        } else {

            $entry = \App\Models\Entry::create($attributes);
            $payments = \App\Models\Payment::find($payments->id);
            $payments->update(['entry_id' => $entry->id]);
        }
        \App\Models\Entryitem::where('entry_id', $payments->entry_id)->delete();

        //Purchase account
        $sub_amount = new  \App\Models\Entryitem();
        $sub_amount->entry_id = $entry->id;
        $sub_amount->user_id = \Auth::user()->id;
        $sub_amount->org_id = \Auth::user()->org_id;
        $sub_amount->dc = 'D';
        $sub_amount->ledger_id = $purchase_order->client->ledger_id; //Material Purchased Ledger
        $sub_amount->amount = $paid_amount;
        $sub_amount->narration = 'Order Receipts';
        $sub_amount->save();

        $creditTds = $request->tds;
        $createPaymentWithTds = $paid_amount - $creditTds;

        // cash account
        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->user_id = \Auth::user()->id;
        $cash_amount->org_id = \Auth::user()->org_id;
        $cash_amount->dc = 'C';
        $cash_amount->ledger_id = $request->payment_method; //
        $cash_amount->amount = $createPaymentWithTds;
        $cash_amount->narration = 'being payment made';
        $cash_amount->save();

        $cash_amount = new \App\Models\Entryitem();
        $cash_amount->entry_id = $entry->id;
        $cash_amount->user_id = \Auth::user()->id;
        $cash_amount->org_id = \Auth::user()->org_id;
        $cash_amount->dc = 'C';
        $cash_amount->ledger_id = 867; //
        $cash_amount->amount = $creditTds;
        $cash_amount->narration = 'Tds amount added';
        $cash_amount->save();

        return 0;

    }

    private function convertdate($date)
    {
        $date = explode('-', $date);
        $cal = new \App\Helpers\NepaliCalendar();
        $converted = $cal->eng_to_nep($date[0], $date[1], $date[2]);
        $nepdate = $converted['year'] . '.' . $converted['nmonth'] . '.' . $converted['date'];
        return $nepdate;
    }

    public function checkLeder($request)
    {

        foreach ($request->paid_by as $key => $value) {
            if ($value == 'city-ledger') {

                $ledgername = $request->city_ledger[$key];

                $temp_city_ledger = \App\Models\COALedgers::where('name', $ledgername)->first();
                if (!$temp_city_ledger) {

                    return false;
                }
            }


        }

        return true;


    }


    public function SalePaymentPostSave($request, $id)
    {
        if ($request->invoice_type == 'abbr') {
            $attributes = $request->all();
            $attributes['reservation_id'] = $request->room_reservation_id;

            $attributes['created_by'] = \Auth::user()->id;

            // if ($request->file('attachment')) {
            //     $stamp = time();
            //     $file = $request->file('attachment');
            //     $destinationPath = public_path() . '/attachment/';
            //     $filename = $file->getClientOriginalName();
            //     $request->file('attachment')->move($destinationPath, $stamp . '_' . $filename);
            //     $attributes['attachment'] = $stamp . '_' . $filename;
            // }

            $attributes['type']='abbr';
            $paymentsCreate = \App\Models\Payment::create($attributes);


            $order = \App\Models\AbbrOrders::find($id);

            $order_product_type = \App\Models\AbbrOrderDetail::where('order_id', $order->id)->select('product_type_id')->distinct('product_type_id')->get();


            $attributes_order = (array)$request;


            $current_ledger_id = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;


            $outlet_ledgers = $order->outlet->ledger_id;

            $attributes_order['entrytype_id'] = 13;
            $attributes_order['user_id'] = \Auth::user()->id;
            $attributes_order['org_id'] = \Auth::user()->org_id;
            $type = \App\Models\Entrytype::find(13);
            $attributes_order['number'] = \TaskHelper::generateId($type);
            $attributes_order['bill_no'] = 'AI' . $order->bill_no;
            $attributes_order['resv_id'] = $order->reservation_id;
            $attributes_order['source'] = 'POS_PAYMENT';
            $attributes_order['date'] = \Carbon\Carbon::today();
            $attributes_order['fiscal_year_id'] = $current_ledger_id;
            $attributes_order['order_id'] = $id;

            if ($request->paid_by == 'complementry') {

                $attributes_order['dr_total'] = 0;
                $attributes_order['cr_total'] = 0;
                $attributes_order['tag_id'] = 31;
                $attributes_order['notes'] = $request->note . ' Payment received from outlet ' . $order->outlet->name . ' with invoice Id ' . $order->id . ' #Complementary';
            } else {

                $attributes_order['dr_total'] = $request->amount;
                $attributes_order['cr_total'] = $request->amount;
                $attributes_order['tag_id'] = 19; //sales invoice
                $attributes_order['notes'] = $request->note . ' Payment received from outlet ' . $order->outlet->name . ' with invoice Id ' . $order->id;
            }

            $entry = \App\Models\Entry::create($attributes_order);

            $paymentsCreate->update(['entry_id' => $entry->id]);
            \App\Models\AbbrOrderMeta::where('order_id', $id)->first()->update(['settle_entry_id' => $entry->id]);

            if ($request->paid_by == 'cash') {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being sales made by cash';
                $cash_amount->save();


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made by cash';
                $cash_amount->save();
            } elseif ($request->paid_by == 'check') {


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
                $cash_amount->save();


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CHEQUE_AMOUNT_LEDGER_ID');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
                $cash_amount->save();
            } elseif ($request->paid_by == 'travel-agent') {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made travel agent';
                $cash_amount->save();


                $temp_ledger = \App\Models\COALedgers::where('name', $request['travel_agent_ledger'])->first();

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $temp_ledger->id;
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made travel agent';
                $cash_amount->save();
            } elseif ($request->paid_by == 'city-ledger') {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made';
                $cash_amount->save();


                $temp_city_ledger = \App\Models\COALedgers::where('name', $request['city_ledger'])->first();


                $credit_amount = new \App\Models\Entryitem();
                $credit_amount->entry_id = $entry->id;
                $credit_amount->dc = 'D';
                $credit_amount->ledger_id = $temp_city_ledger->id;
                $credit_amount->amount = $request->amount;
                $credit_amount->narration = 'being payment made city ledger';
                $credit_amount->save();
            } elseif ($request->paid_by == 'complementry') {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = 0.0;
                $cash_amount->narration = 'being payment made complementary';
                $cash_amount->save();

                $credit_amount = new \App\Models\Entryitem();
                $credit_amount->entry_id = $entry->id;
                $credit_amount->dc = 'D';
                $credit_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
                $credit_amount->amount = 0.0;
                $credit_amount->narration = 'being payment made complementary';
                $credit_amount->save();
            } elseif ($request->paid_by == 'staff') {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being sales made staff ';
                $cash_amount->save();

                $temp_staff_name = \App\Models\COALedgers::where('name', $request['staff_name'])->first();

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $temp_staff_name->id;
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made of ' . $request->staff_name . '';
                $cash_amount->save();
            } elseif ($request->paid_by == 'credit-cards') {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being sales made CC';
                $cash_amount->save();


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CREDIT_CARD');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made';
                $cash_amount->save();
            } elseif ($request->paid_by == 'room') {

                $room_no = $request['room_no'];


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made Romm No:' . $room_no . '';
                $cash_amount->save();


                $reservation = \App\Models\Reservation::find($request['room_reservation_id']);

                if ($reservation->parent_res == null) {

                    $reservation_id = $reservation->id;
                } else {

                    $reservation_id = $reservation->parent_res;
                }

                $reservation_ledger_id = $reservation->ledger_id;

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $reservation_ledger_id;
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made Romm No:' . $room_no . '';
                $cash_amount->save();

                $folio = \App\Models\Folio::orderBy('id', 'asc')->where('reservation_id', $reservation_id)->first();

                $attributes = [];
                $attributes['subtotal'] = $folio->subtotal + $order->subtotal;
                $attributes['service_charge'] = $folio->service_charge + $order->service_charge;
                $attributes['amount_with_service'] = $folio->amount_with_service + $order->amount_with_service;
                $attributes['discount_percent'] = 0;
                $attributes['taxable_amount'] = $folio->taxable_amount + $order->taxable_amount;
                $attributes['tax_amount'] = $folio->tax_amount + $order->tax_amount;
                $attributes['total_amount'] = $folio->total_amount + $request->amount;

                $room_description = $order->outlet->outlet_code;

                \App\Models\Folio::find($folio->id)->update($attributes);

                $orderdetail = \App\Models\AbbrOrderDetail::where('order_id', $order->id)->get();
                foreach ($orderdetail as $od) {
                    $detail_attributes = new \App\Models\FolioDetail();
                    $detail_attributes->folio_id = $folio->id;
                    if ($od->product_id) {
                        $product_name = \App\Models\Product::find($od->product_id)->name;
                        if ($product_name) {
                            $detail_attributes->description = $product_name . '-' . $room_description;
                        } else {
                            $detail_attributes->description = $room_description;
                        }
                    } else {
                        $detail_attributes->description = $od->description;
                    }

                    $detail_attributes->price = $od->price;
                    $detail_attributes->quantity = $od->quantity;
                    $detail_attributes->total = $od->total;
                    $detail_attributes->date = \Carbon\Carbon::now();
                    $detail_attributes->is_inventory = 0;
                    $detail_attributes->posted_to_ledger = 1;
                    $detail_attributes->is_posted = 1;
                    $detail_attributes->flag = 'restaurant';
                    $detail_attributes->save();
                }
            } elseif ($request->paid_by == 'e-sewa') {


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made  E-sewa id ' . $request->e_sewa_id . '';
                $cash_amount->save();


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made  E-sewa id ' . $request->e_sewa_id . '';
                $cash_amount->save();
            } elseif ($request->paid_by == 'edm') {


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('EDM'); //
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made Executive Dining Meal';
                $cash_amount->save();


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made  Executive Dining Meal';
                $cash_amount->save();

            } elseif ($request->paid_by == 'mnp') {


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('MNP'); //
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made Marketing & promotion ';
                $cash_amount->save();


                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
                $cash_amount->amount = $request->amount;
                $cash_amount->narration = 'being payment made  Marketing & promotion ';
                $cash_amount->save();

            }

            return true;
        }


        $attributes = $request->all();
        $attributes['reservation_id'] = $request->room_reservation_id;

        $attributes['created_by'] = \Auth::user()->id;

        // if ($request->file('attachment')) {
        //     $stamp = time();
        //     $file = $request->file('attachment');
        //     $destinationPath = public_path() . '/attachment/';
        //     $filename = $file->getClientOriginalName();
        //     $request->file('attachment')->move($destinationPath, $stamp . '_' . $filename);
        //     $attributes['attachment'] = $stamp . '_' . $filename;
        // }


        $paymentsCreate = \App\Models\Payment::create($attributes);


        $order = \App\Models\Orders::find($id);

        $order_product_type = \App\Models\OrderDetail::where('order_id', $order->id)->select('product_type_id')->distinct('product_type_id')->get();


        $attributes_order = (array)$request;


        $current_ledger_id = \App\Models\Fiscalyear::where('org_id', \Auth::user()->org_id)->where('current_year', 1)->first()->id;


        $outlet_ledgers = $order->outlet->ledger_id;

        $attributes_order['entrytype_id'] = 13;
        $attributes_order['user_id'] = \Auth::user()->id;
        $attributes_order['org_id'] = \Auth::user()->org_id;
        $type = \App\Models\Entrytype::find(13);
        $attributes_order['number'] = \TaskHelper::generateId($type);
        $attributes_order['bill_no'] = 'TI' . $order->bill_no;
        $attributes_order['resv_id'] = $order->reservation_id;
        $attributes_order['source'] = 'POS_PAYMENT';
        $attributes_order['date'] = \Carbon\Carbon::today();
        $attributes_order['fiscal_year_id'] = $current_ledger_id;
        $attributes_order['order_id'] = $id;

        if ($request->paid_by == 'complementry') {

            $attributes_order['dr_total'] = 0;
            $attributes_order['cr_total'] = 0;
            $attributes_order['tag_id'] = 31;
            $attributes_order['notes'] = $request->note . ' Payment received from outlet ' . $order->outlet->name . ' with invoice Id ' . $order->id . ' #Complementary';
        } else {

            $attributes_order['dr_total'] = $request->amount;
            $attributes_order['cr_total'] = $request->amount;
            $attributes_order['tag_id'] = 19; //sales invoice
            $attributes_order['notes'] = $request->note . ' Payment received from outlet ' . $order->outlet->name . ' with invoice Id ' . $order->id;
        }

        $entry = \App\Models\Entry::create($attributes_order);

        $paymentsCreate->update(['entry_id' => $entry->id]);
        \App\Models\OrderMeta::where('order_id', $id)->first()->update(['settle_entry_id' => $entry->id]);

        if ($request->paid_by == 'cash') {

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being sales made by cash';
            $cash_amount->save();


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made by cash';
            $cash_amount->save();
        } elseif ($request->paid_by == 'check') {


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
            $cash_amount->save();


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CHEQUE_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
            $cash_amount->save();
        } elseif ($request->paid_by == 'travel-agent') {

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made travel agent';
            $cash_amount->save();


            $temp_ledger = \App\Models\COALedgers::where('name', $request['travel_agent_ledger'])->first();

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = $temp_ledger->id;
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made travel agent';
            $cash_amount->save();
        } elseif ($request->paid_by == 'city-ledger') {

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made';
            $cash_amount->save();


            $temp_city_ledger = \App\Models\COALedgers::where('name', $request['city_ledger'])->first();


            $credit_amount = new \App\Models\Entryitem();
            $credit_amount->entry_id = $entry->id;
            $credit_amount->dc = 'D';
            $credit_amount->ledger_id = $temp_city_ledger->id;
            $credit_amount->amount = $request->amount;
            $credit_amount->narration = 'being payment made city ledger';
            $credit_amount->save();
        } elseif ($request->paid_by == 'complementry') {

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = 0.0;
            $cash_amount->narration = 'being payment made complementary';
            $cash_amount->save();

            $credit_amount = new \App\Models\Entryitem();
            $credit_amount->entry_id = $entry->id;
            $credit_amount->dc = 'D';
            $credit_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
            $credit_amount->amount = 0.0;
            $credit_amount->narration = 'being payment made complementary';
            $credit_amount->save();
        } elseif ($request->paid_by == 'staff') {

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being sales made staff ';
            $cash_amount->save();

            $temp_staff_name = \App\Models\COALedgers::where('name', $request['staff_name'])->first();

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = $temp_staff_name->id;
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made of ' . $request->staff_name . '';
            $cash_amount->save();
        } elseif ($request->paid_by == 'credit-cards') {

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being sales made CC';
            $cash_amount->save();


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CREDIT_CARD');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made';
            $cash_amount->save();
        } elseif ($request->paid_by == 'room') {

            $room_no = $request['room_no'];


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made Romm No:' . $room_no . '';
            $cash_amount->save();


            $reservation = \App\Models\Reservation::find($request['room_reservation_id']);

            if ($reservation->parent_res == null) {

                $reservation_id = $reservation->id;
            } else {

                $reservation_id = $reservation->parent_res;
            }

            $reservation_ledger_id = $reservation->ledger_id;

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = $reservation_ledger_id;
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made Romm No:' . $room_no . '';
            $cash_amount->save();

            $folio = \App\Models\Folio::orderBy('id', 'asc')->where('reservation_id', $reservation_id)->first();

            $attributes = [];
            $attributes['subtotal'] = $folio->subtotal + $order->subtotal;
            $attributes['service_charge'] = $folio->service_charge + $order->service_charge;
            $attributes['amount_with_service'] = $folio->amount_with_service + $order->amount_with_service;
            $attributes['discount_percent'] = 0;
            $attributes['taxable_amount'] = $folio->taxable_amount + $order->taxable_amount;
            $attributes['tax_amount'] = $folio->tax_amount + $order->tax_amount;
            $attributes['total_amount'] = $folio->total_amount + $request->amount;

            $room_description = $order->outlet->outlet_code;

            \App\Models\Folio::find($folio->id)->update($attributes);

            $orderdetail = \App\Models\OrderDetail::where('order_id', $order->id)->get();
            foreach ($orderdetail as $od) {
                $detail_attributes = new \App\Models\FolioDetail();
                $detail_attributes->folio_id = $folio->id;
                if ($od->product_id) {
                    $product_name = \App\Models\Product::find($od->product_id)->name;
                    if ($product_name) {
                        $detail_attributes->description = $product_name . '-' . $room_description;
                    } else {
                        $detail_attributes->description = $room_description;
                    }
                } else {
                    $detail_attributes->description = $od->description;
                }

                $detail_attributes->price = $od->price;
                $detail_attributes->quantity = $od->quantity;
                $detail_attributes->total = $od->total;
                $detail_attributes->date = \Carbon\Carbon::now();
                $detail_attributes->is_inventory = 0;
                $detail_attributes->posted_to_ledger = 1;
                $detail_attributes->is_posted = 1;
                $detail_attributes->flag = 'restaurant';
                $detail_attributes->save();
            }
        } elseif ($request->paid_by == 'e-sewa') {


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('POS_GUEST');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made  E-sewa id ' . $request->e_sewa_id . '';
            $cash_amount->save();


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made  E-sewa id ' . $request->e_sewa_id . '';
            $cash_amount->save();
        } elseif ($request->paid_by == 'edm') {


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('EDM'); //
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made Executive Dining Meal';
            $cash_amount->save();


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made  Executive Dining Meal';
            $cash_amount->save();

        } elseif ($request->paid_by == 'mnp') {


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('MNP'); //
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made Marketing & promotion ';
            $cash_amount->save();


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'D';
            $cash_amount->ledger_id = \FinanceHelper::get_ledger_id('CASH_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $request->amount;
            $cash_amount->narration = 'being payment made  Marketing & promotion ';
            $cash_amount->save();

        }

        return true;
    }


    public function SalePaymentPost(Request $request, $id)
    {
        if ($request->invoice_type == 'abbr') {

            DB::beginTransaction();

            // $request->validate([
            //     'paid_by'=>'required',
            //     'date'=>'required',
            //     'amount'=>'required',
            // ]);


            if (!$this->checkLeder($request)) {

                Flash::error("LEDGER NOT FOUND");

                return redirect()->back();
            }

            $attributes = $request->all();

            $sale_order = \App\Models\AbbrOrders::find($id);

            $paid_amount = array_sum($request->amount);

            if ($request->old_payments && count($request->old_payments) > 0) {
                $oldPaymentsIds = $request->old_payments;
                $oldEntriesIds = \App\Models\Payment::whereIn('id', $oldPaymentsIds)->pluck('entry_id')->toArray();
                \App\Models\Entryitem::whereIn('entry_id', $oldEntriesIds)->delete();
                \App\Models\Entry::whereIn('id', $oldEntriesIds)->delete();
                \App\Models\Payment::whereIn('id', $oldPaymentsIds)->delete();
            }

            foreach ($request->paid_by as $key => $paid_by) {
                $value = $attributes;

                $value['paid_by'] = $paid_by;
                $value['amount'] = $request->amount[$key];
                $value['remarks'] = $request->remarks[$key];
                $value['bank_name'] = $request->bank_name[$key];
                $value['cheque_no'] = $request->cheque_no[$key];
                $value['cheque_date'] = $request->cheque_date[$key];
                $value['travel_agent_ledger'] = $request->travel_agent_ledger[$key];
                $value['city_ledger'] = $request->city_ledger[$key];
                $value['staff_nc_board'] = $request->staff_nc_board[$key];
                $value['complementry_staff'] = $request->complementry_staff[$key];
                $value['staff_name'] = $request->staff_name[$key];
                $value['cc_type'] = $request->cc_type[$key];
                $value['cc_no'] = $request->cc_no[$key];
                $value['cc_holder'] = $request->cc_holder[$key];
                $value['e_sewa_id'] = $request->e_sewa_id[$key];
                $value['room_no'] = $request->room_no[$key];

                $newrequest = new \Illuminate\Http\Request();
                $newrequest->replace($value);

                $this->SalePaymentPostSave($newrequest, $id);


            }
            \App\Models\AbbrOrderMeta::where('order_id', $id)->first()->update(['settlement' => 0]); //first unsettle

            if ($paid_amount >= $sale_order->total_amount) {
                $attributes_purchase['payment_status'] = "Paid";
                $sale_order->update($attributes_purchase);
            } elseif ($paid_amount <= $sale_order->total_amount && $paid_amount > 0) {

                $attributes_purchase['payment_status'] = "Partial";
                $sale_order->update($attributes_purchase);
            } else {

                $attributes_purchase['payment_status'] = "Pending";
                $sale_order->update($attributes_purchase);
            }

            \App\Models\AbbrOrders::find($id)->update(['ready_status' => 'checkedout']);

            \App\Models\AbbrOrderMeta::where('order_id', $id)->first()->update(['settlement' => 1]);
            $orders = \App\Models\AbbrOrders::find($id);
            Audit::log(Auth::user()->id, "POS Billing", "Bill Is Settled: Bill Number-" . $orders->bill_no . "");


            \App\Models\Loyality::create([
                'invoice_id' => $orders->id,
                'outlet_id' => $orders->outlet_id,
                'client_id' => $orders->client_id ?? $orders->pos_customer_id,
                'invoice_amount' => $orders->total_amount,
                'type' => 'restro_abbr'
            ]);

            DB::commit();
            return redirect('/admin/orders/todaypos/payments');

        }

        // $request->validate([
        //     'paid_by'=>'required',
        //     'date'=>'required',
        //     'amount'=>'required',
        // ]);


        DB::beginTransaction();
        if (!$this->checkLeder($request)) {

            Flash::error("LEDGER NOT FOUND");

            return redirect()->back();
        }

        $attributes = $request->all();

        $sale_order = \App\Models\Orders::find($id);

        $paid_amount = array_sum($request->amount);

        if ($request->old_payments && count($request->old_payments) > 0) {
            $oldPaymentsIds = $request->old_payments;
            $oldEntriesIds = Payment::whereIn('id', $oldPaymentsIds)->pluck('entry_id')->toArray();
            \App\Models\Entryitem::whereIn('entry_id', $oldEntriesIds)->delete();
            \App\Models\Entry::whereIn('id', $oldEntriesIds)->delete();
            Payment::whereIn('id', $oldPaymentsIds)->delete();
        }

        foreach ($request->paid_by as $key => $paid_by) {
            $value = $attributes;

            $value['paid_by'] = $paid_by;
            $value['amount'] = $request->amount[$key];
            $value['remarks'] = $request->remarks[$key];
            $value['bank_name'] = $request->bank_name[$key];
            $value['cheque_no'] = $request->cheque_no[$key];
            $value['cheque_date'] = $request->cheque_date[$key];
            $value['travel_agent_ledger'] = $request->travel_agent_ledger[$key];
            $value['city_ledger'] = $request->city_ledger[$key];
            $value['staff_nc_board'] = $request->staff_nc_board[$key];
            $value['complementry_staff'] = $request->complementry_staff[$key];
            $value['staff_name'] = $request->staff_name[$key];
            $value['cc_type'] = $request->cc_type[$key];
            $value['cc_no'] = $request->cc_no[$key];
            $value['cc_holder'] = $request->cc_holder[$key];
            $value['e_sewa_id'] = $request->e_sewa_id[$key];
            $value['room_no'] = $request->room_no[$key];

            $newrequest = new \Illuminate\Http\Request();
            $newrequest->replace($value);

            $this->SalePaymentPostSave($newrequest, $id);


        }
        \App\Models\OrderMeta::where('order_id', $id)->first()->update(['settlement' => 0]); //first unsettle

        if ($paid_amount >= $sale_order->total_amount) {
            $attributes_purchase['payment_status'] = "Paid";
            $sale_order->update($attributes_purchase);
        } elseif ($paid_amount <= $sale_order->total_amount && $paid_amount > 0) {

            $attributes_purchase['payment_status'] = "Partial";
            $sale_order->update($attributes_purchase);
        } else {

            $attributes_purchase['payment_status'] = "Pending";
            $sale_order->update($attributes_purchase);
        }

        \App\Models\Orders::find($id)->update(['ready_status' => 'checkedout']);

        \App\Models\OrderMeta::where('order_id', $id)->first()->update(['settlement' => 1]);
        $orders = \App\Models\Orders::find($id);
        Audit::log(Auth::user()->id, "POS Billing", "Bill Is Settled: Bill Number-" . $orders->bill_no . "");


        \App\Models\Loyality::create([
            'invoice_id' => $orders->id,
            'outlet_id' => $orders->outlet_id,
            'client_id' => $orders->client_id ?? $orders->pos_customer_id,
            'invoice_amount' => $orders->total_amount,
            'type' => 'restro'
        ]);
        DB::commit();

        if ($orders) {

            if (env('IS_IRD')) {


                if ($orders->pos_customer_id) {
                    $guest_name = $orders->client->name;
                    $guest_pan = $orders->client->vat;
                } else {
                    if ($orders->reservation->company_id) {
                        $guest_name = $orders->reservaion->client->name;
                        $guest_pan = $orders->reservaion->client->vat;
                    } else {
                        $guest_name = $orders->reservaion->guest_name;
                        $guest_pan = $orders->client->vat;
                    }
                }

                $bill_date_nepali = $this->convertdate($orders->bill_date);
                $bill_today_date_nep = $this->convertdate(date('Y-m-d'));
                //dd($bill_today_date_nep);
                $invoice_no = 'TI-' . $orders->outlet->short_name . '' . $orders->bill_no;

                $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'), "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $guest_pan, "fiscal_year" => $orders->fiscal_year, "buyer_name" => $guest_name, "invoice_number" => $invoice_no, "invoice_date" => $bill_date_nepali, "total_sales" => $orders->total_amount, "taxable_sales_vat" => $orders->taxable_amount, "vat" => $orders->tax_amount, "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0, "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0, "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);

                $irdsync = new \App\Models\NepalIRDSync();
                $response = $irdsync->postbill($data);

                if ($response == 200) {

                    \App\Models\OrderMeta::where('order_id', $orders->id)->first()->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
                    //Flash::success("Payment Settled ");
                    Flash::success('Payment Settled and Successfully Posted to IRD. Code: ' . $response . '');
                    Audit::log(Auth::user()->id, "POS Billing", "Bill Is Posted To IRD: ID-" . $id . " Response :" . $response . "");
                    return redirect('/admin/orders/todaypos/payments');
                } else {

                    if ($response == 101) {

                        \App\Models\OrderMeta::where('order_id', $orders->id)->first()->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
                    } else {

                        \App\Models\OrderMeta::where('order_id', $orders->id)->first()->update(['is_realtime' => 1]);
                    }

                    Flash::warning('Payment Created Successfully. Post Cannot Due to Response Code: ' . $response . '');
                    Audit::log(Auth::user()->id, "POS Billing", "Bill Is NOt Posted TO IRD: ID-" . $id . " Response :" . $response . "");

                    return redirect('/admin/orders/todaypos/payments');
                }
            } else {
                \App\Models\OrderMeta::where('order_id', $orders->id)->first()->update(['is_realtime' => 1]);
                Audit::log(Auth::user()->id, "POS Billing", "Bill Is Not Synced with IRD To Return: ID-" . $orders->id . "");
                \Flash::warning('This Bill Is Not Synced With IRD To Return This Bill');

                return redirect("/admin/hotel/orders/outlet/{$orders->outlet_id}/settlement");
            }
        } else {

            Audit::log(Auth::user()->id, "POS Billing", "Bill Is Not Found: ID-" . $orders->id . "");
            \Flash::warning('No Bill Found Of this Number');
            return redirect()->back();
        }
    }

    public function moveAbbrToOrder($id)
    {

        $finOrderProxy = \App\Models\AbbrOrders::find($id);
        if ($finOrderProxy && ($finOrderProxy->ordermeta->sync_with_ird == 1 || $finOrderProxy->ordermeta->is_bill_active == 0)) {

            return false;

        } elseif (!$finOrderProxy) {
            return false;
        }

        $finOrderProxyArr = $finOrderProxy->toArray();

        unset($finOrderProxyArr['id']);

        $finOrderProxyArr['bill_no'] = $this->getNextAbbrBillNumber($finOrderProxyArr['outlet_id']);

        //dd($finOrderProxyArr);
        $finOrderProxyArr ['bill_date'] = date('Y-m-d');

        $finOrder = \App\Models\AbbrOrders::create($finOrderProxyArr);

        $this->createAbbrOrderMeta($finOrder);

        $finOrderProxy->update(['reference_order' => $finOrder->id, 'ready_status' => 'checkedout', 'is_final_invoice' => '1']); //to check

        $finOrderProxyDetails = \App\Models\OrderDetailProxy::where('order_id', $id)->get()
            ->toArray();


        $finOrdersDetails = [];

        foreach ($finOrderProxyDetails as $key => $value) {

            unset($value['id']);

            $value['order_id'] = $finOrder->id;

            $finOrdersDetails [] = $value;
        }

        \App\Models\AbbrOrderDetail::insert($finOrdersDetails);

        \App\Models\OrderDetailProxy::where('order_id', $id)->delete();

        $mergedTable = \App\Models\OrderProxy::where('merged_to', $id)->pluck('id')->toArray();

        \App\Models\OrderProxy::whereIn('id', $mergedTable)->delete();

        \App\Models\OrderDetailProxy::whereIn('order_id', $mergedTable)->delete();
        if (!$finOrderProxy->is_edm) {

            $finOrderProxy->delete();
        }

        return $finOrder->id;


    }

    public function postAbbrToTax(Request $request)
    {
        DB::beginTransaction();
        $abbr_orders = AbbrOrders::where('is_converted', 0)
            ->where('abbr_fin_orders_meta.is_bill_active',1)
            ->leftJoin('abbr_fin_orders_meta','abbr_fin_orders_meta.order_id','=','abbr_fin_orders.id')
            ->where('bill_date',date('Y-m-d'))->get();
        $total_sales = $abbr_orders->sum('total_amount');
        $taxable_sales_vat= $abbr_orders->sum('taxable_amount');
        $vat = $abbr_orders->sum('tax_amount');
        $non_taxable = $abbr_orders->sum('non_taxable_amount');
        $discount_amount = $abbr_orders->sum('discount_amount');
        $subtotal = $abbr_orders->sum('subtotal');
        $fiscal_year = \App\Models\Fiscalyear::where('current_year', '1')->first();

        $bill_no = \App\Models\Orders::select('bill_no')
            ->where('fiscal_year', $fiscal_year->fiscal_year)
            ->orderBy('bill_no', 'desc')
            ->first();

        $bill_no = $bill_no->bill_no + 1;

        $abbr_collection['fiscal_year']=$fiscal_year->fiscal_year;
        $abbr_collection['bill_no']=$bill_no;
        $abbr_collection ['bill_date'] = date('Y-m-d');
        $abbr_collection ['total_amount'] =$total_sales;
        $abbr_collection ['taxable_amount'] =$taxable_sales_vat;
        $abbr_collection ['tax_amount'] =$vat;
        $abbr_collection ['non_taxable_amount'] =$non_taxable;
        $abbr_collection ['user_id'] =auth()->user()->id;
        $abbr_collection ['pos_customer_id'] =1;
        $abbr_collection ['org_id'] =auth()->user()->org_id;
        $abbr_collection ['order_type'] ='invoice';
        $abbr_collection ['outlet_id'] =1;
        $abbr_collection ['discount_amount'] =$discount_amount;
        $abbr_collection ['subtotal'] =$subtotal;
        $abbr_collection ['payment_status'] ='Paid';
        $abbr_collection ['ready_status'] ='checkedout';
        $abbr_collection ['bill_to'] ='normal';
        $abbr_collection ['transaction_date'] =date('Y-m-d');
        $abbr_collection ['bill_type'] ='abbreviated';
        $abbr_collection ['settlement'] =1;
        $abbr_collection ['is_posted'] =1;

        $finOrder = \App\Models\Orders::create($abbr_collection);

        $ordermeta = new \App\Models\OrderMeta();
        $ordermeta->order_id = $finOrder->id;
        $ordermeta->sync_with_ird = 0;
        $ordermeta->is_bill_active = 1;
        $ordermeta->settlement = 1;
        $ordermeta->is_posted = 1;
        $ordermeta->save();

        foreach($abbr_orders as $order){
            $order_details=AbbrOrderDetail::where('order_id',$order->id)->get()->toArray();
            $order->update(['is_converted'=>1]);


            $finOrdersDetails = [];

            foreach ($order_details as $key => $value) {

                unset($value['id']);

                $value['order_id'] = $finOrder->id;

                $finOrdersDetails [] = $value;
            }

            \App\Models\OrderDetail::insert($finOrdersDetails);

        }
        DB::commit();
        Flash::success('All Abbreviated bills have been posted');

        if ($finOrder) {

            if (env('IS_IRD')) {


                if ($finOrder->pos_customer_id) {
                    $guest_name = $finOrder->client->name;
                    $guest_pan = $finOrder->client->vat;
                }

                $bill_date_nepali = $this->convertdate($finOrder->bill_date);
                $bill_today_date_nep = $this->convertdate(date('Y-m-d'));
                //dd($bill_today_date_nep);
                $invoice_no = 'TI-' . $finOrder->outlet->short_name . '' . $finOrder->bill_no;

                $data = json_encode(["username" => env('IRD_USERNAME'), "password" => env('IRD_PASSWORD'),
                    "seller_pan" => env('SELLER_PAN'), "buyer_pan" => $guest_pan,
                    "fiscal_year" => $finOrder->fiscal_year, "buyer_name" => $guest_name, "invoice_number" => $invoice_no,
                    "invoice_date" => $bill_date_nepali, "total_sales" => $finOrder->total_amount,
                    "taxable_sales_vat" => $finOrder->taxable_amount, "vat" => $finOrder->tax_amount,
                    "excisable_amount" => 0, "excise" => 0, "taxable_sales_hst" => 0, "hst" => 0,
                    "amount_for_esf" => 0, "esf" => 0, "export_sales" => 0, "tax_exempted_sales" => 0,
                    "isrealtime" => true, "datetimeClient" => $bill_today_date_nep]);

                $irdsync = new \App\Models\NepalIRDSync();
                $response = $irdsync->postbill($data);

                $id=$finOrder->id;
                if ($response == 200) {

                    \App\Models\OrderMeta::where('order_id', $finOrder->id)->first()->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
                    //Flash::success("Payment Settled ");
                    Flash::success('Payment Settled and Successfully Posted to IRD. Code: ' . $response . '');
                    Audit::log(Auth::user()->id, "POS Billing", "Bill Is Posted To IRD: ID-" . $id . " Response :" . $response . "");
                    return redirect('/admin/hotel/nightaudit-settlement/resturantbills/listing');
                } else {

                    if ($response == 101) {

                        \App\Models\OrderMeta::where('order_id', $finOrder->id)->first()->update(['sync_with_ird' => 1, 'is_realtime' => 1]);
                    } else {

                        \App\Models\OrderMeta::where('order_id', $finOrder->id)->first()->update(['is_realtime' => 1]);
                    }

                    Flash::warning('Payment Created Successfully. Post Cannot Due to Response Code: ' . $response . '');
                    Audit::log(Auth::user()->id, "POS Billing", "Bill Is NOt Posted TO IRD: ID-" . $id . " Response :" . $response . "");

                    return redirect('/admin/hotel/nightaudit-settlement/resturantbills/listing');
                }
            } else {
                \App\Models\OrderMeta::where('order_id', $finOrder->id)->first()->update(['is_realtime' => 1]);
                Audit::log(Auth::user()->id, "POS Billing", "Bill Is Not Synced with IRD To Return: ID-" . $finOrder->id . "");
                \Flash::warning('This Bill Is Not Synced With IRD To Return This Bill');

                return redirect("/admin/hotel/nightaudit-settlement/resturantbills/listing");
            }
        } else {

            Audit::log(Auth::user()->id, "POS Billing", "Bill Is Not Found: ID-" . $finOrder->id . "");
            \Flash::warning('No Bill Found Of this Number');
            return redirect()->back();
        }
    }


    public function PurchasePaymentedit(Request $request, $id, $payment_id)
    {
        $page_title = 'Purchase Payment Edit';
        $page_description = 'create payments of purchase';
        $purchase_id = $id;
        $payments = \App\Models\Payment::where('id', $payment_id)->first();
        $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');

        return view('admin.purchase.paymentedit', compact('page_title', 'page_description', 'purchase_id', 'payment_method', 'payments'));
    }

    public function SalePaymentedit(Request $request, $id, $payment_id)
    {


        $ordersmeta = \App\Models\OrderMeta::where('order_id', $id)->first();

        $orders = \App\Models\Orders::find($id);
        if ($ordersmeta->is_bill_active != 1) {
            Flash::error('This Bill is not Active');
            return redirect()->back();
        }

        $page_title = 'POS Settlement';
        $page_description = 'for ' . $orders->outlet->name . ' of Bill Num ' . $orders->outlet->outlet_code . $orders->bill_no . '';
        $sale_id = $id;

        $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');

        $purchase_order = \App\Models\Orders::where('id', $id)->first();
        $payment_remain = $purchase_order->total_amount;

        $page_title = 'Sales Payment Edit';

        $sale_id = $id;

        $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');
        $paymentList = Payment::where('sale_id', $sale_id)->orderBy('id', 'asc')->get();
        $topPayments = $paymentList->first(); //to adjust in html form
        $subPayments = $paymentList->where('id', '!=', $topPayments->id);
        $topPayments = [$topPayments];


        return view('admin.orders.paymentedit', compact('page_title', 'page_description', 'sale_id', 'payment_method', 'payment_remain', 'payment_id', 'payments', 'settledLeger', 'topPayments', 'subPayments'));
        // return view('admin.orders.paymentedit', compact('page_title', 'page_description', 'sale_id', 'payment_method', 'payments'));
    }

    public function PurchasePaymentUpdate(Request $request, $id, $payment_id)
    {

        $payment = \App\Models\Payment::find($payment_id);

        //dd($payment);
        $attributes = $request->all();
        $attributes['created_by'] = \Auth::user()->id;

        if ($request->file('attachment')) {
            $stamp = time();
            $file = $request->file('attachment');
            //dd($file);
            $destinationPath = public_path() . '/attachment/';
            $filename = $file->getClientOriginalName();
            $request->file('attachment')->move($destinationPath, $stamp . '_' . $filename);

            $attributes['attachment'] = $stamp . '_' . $filename;
        }

        $payment->update($attributes);

        $paid_amount = DB::table('payments')->where('purchase_id', $id)->sum('amount');

        $purchase_order = \App\Models\PurchaseOrder::find($id);

        if ($paid_amount >= $purchase_order->total) {

            $attributes_purchase['payment_status'] = "Paid";
            $purchase_order->update($attributes_purchase);
        } elseif ($paid_amount <= $purchase_order->total && $paid_amount > 0) {

            $attributes_purchase['payment_status'] = "Partial";
            $purchase_order->update($attributes_purchase);
        } else {

            $attributes_purchase['payment_status'] = "Pending";
            $purchase_order->update($attributes_purchase);
        }

        Flash::success("Payment Updated Successfully");
        return redirect('/admin/payment/purchase/' . $id . '');
    }


    public function SalePaymentUpdate(Request $request, $id, $payment_id)
    {

        $payment = \App\Models\Payment::find($payment_id);

        //dd($payment);

        $attributes = $request->all();
        $attributes['created_by'] = \Auth::user()->id;

        if ($request->file('attachment')) {
            $stamp = time();
            $file = $request->file('attachment');
            //dd($file);
            $destinationPath = public_path() . '/attachment/';
            $filename = $file->getClientOriginalName();
            $request->file('attachment')->move($destinationPath, $stamp . '_' . $filename);

            $attributes['attachment'] = $stamp . '_' . $filename;
        }

        $payment->update($attributes);

        $paid_amount = DB::table('payments')->where('sale_id', $id)->sum('amount');

        $sale_order = \App\Models\Orders::find($id);

        if ($paid_amount >= $sale_order->total_amount) {

            $attributes_purchase['payment_status'] = "Paid";
            $sale_order->update($attributes_purchase);
        } elseif ($paid_amount <= $sale_order->total_amount && $paid_amount > 0) {

            $attributes_purchase['payment_status'] = "Partial";
            $sale_order->update($attributes_purchase);
        } else {

            $attributes_purchase['payment_status'] = "Pending";
            $sale_order->update($attributes_purchase);
        }

        Flash::success("Payment Updated Successfully");

        return redirect('/admin/payment/orders/' . $id . '');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $payment = \App\Models\Payment::find($id);

        if (!$payment->isdeletable()) {
            abort(403);
        }

        $payment->delete();

        Flash::success('Payment successfully deleted.');

        return redirect()->back();
    }

    public function destroySalePayment($id)
    {
        $payment = \App\Models\Payment::find($id);

        if (!$payment->isdeletable()) {
            abort(403);
        }

        $payment->delete();


        Flash::success('Payment successfully deleted.');

        return redirect()->back();
    }


    public function getProductDetailAjax($productId)
    {
        $product = Course::select('id', 'name', 'price', 'cost')->where('id', $productId)->first();
        return ['data' => json_encode($product)];
    }

    /**
     * Delete Confirm
     *
     * @param int $id
     * @return  View
     */
    public function getModalDelete($id)
    {
        $error = null;

        $payment = \App\Models\Payment::find($id);

        if (!$payment->isdeletable()) {
            abort(403);
        }
        $modal_title = 'Delete Payment';
        $payment = \App\Models\Payment::find($id);
        $modal_route = route('admin.payment.purchase.orders.delete', $payment->id);
        $modal_body = 'Are you sure you want to delete this Payment?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    /**
     * Delete Confirm
     *
     * @param int $id
     * @return  View
     */
    public function getModalDeleteSalePayment($id)
    {
        $error = null;

        $payment = \App\Models\Payment::find($id);

        if (!$payment->isdeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Payment';
        $payment = \App\Models\Payment::find($id);
        $modal_route = route('admin.payment.purchase.orders.delete', $payment->id);
        $modal_body = 'Are you sure you want to delete this Payment?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function ajaxPurchaseStatus(Request $request)
    {

        $purchase_order = \App\Models\PurchaseOrder::find($request->id);

        // dd($purchase_order);

        $attributes['status'] = $request->purchase_status;

        $purchase_order->update($attributes);

        return ['status' => 1];
    }

    public function ajaxSaleStatus(Request $request)
    {

        $sale_order = \App\Models\Orders::find($request->id);

        $attributes['status'] = $request->purchase_status;


        $sale_order->update($attributes);

        return ['status' => 1];
    }

    public function unSettlementCreate(Request $request, $id)
    {

        $orders = \App\Models\Orders::find($id);


        if ($orders->is_bill_active != 1) {

            Flash::error('This Bill is not Active');
            return redirect()->back();
        } elseif ($orders->settlement == 0) {

            Flash::error('This Bill is already unsettled settle first');
            return redirect()->back();
        }

        $page_title = 'POS UnSettlement';
        $page_description = 'for ' . $orders->outlet->name . ' of ORD' . $id . '';
        $sale_id = $id;

        $payment_method = Paymentmethod::orderby('id')->pluck('name', 'id');

        $purchase_order = \App\Models\Orders::where('id', $id)->first();
        $purchase_total = $purchase_order->total_amount;
        $paid_amount = DB::table('payments')->where('sale_id', $id)->sum('amount');
        $payment_remain = $paid_amount;

        $settlement_detail = \App\Models\Payment::where('sale_id', $id)->where('amount', '>', 0)->orderBy('id', 'desc')->first();

        // dd($settlement_detail);

        return view('admin.orders.unsettlementcreate', compact('page_title', 'page_description', 'sale_id', 'payment_method', 'payment_remain', 'settlement_detail'));
    }


    public function unSettlementOrders(Request $request, $id)
    {

        $attributes = $request->all();
        $attributes['reservation_id'] = $request->room_reservation_id;

        // dd($attributes);

        $attributes['created_by'] = \Auth::user()->id;

        if ($request->file('attachment')) {
            $stamp = time();
            $file = $request->file('attachment');
            $destinationPath = public_path() . '/attachment/';
            $filename = $file->getClientOriginalName();
            $request->file('attachment')->move($destinationPath, $stamp . '_' . $filename);
            $attributes['attachment'] = $stamp . '_' . $filename;
        }

        \App\Models\Payment::create($attributes);

        $paid_amount = DB::table('payments')->where('sale_id', $id)->sum('amount');

        $sale_order = \App\Models\Orders::find($id);

        if ($paid_amount >= $sale_order->total_amount) {

            $attributes_purchase['payment_status'] = "Paid";
            $sale_order->update($attributes_purchase);
        } elseif ($paid_amount <= $sale_order->total_amount && $paid_amount > 0) {

            $attributes_purchase['payment_status'] = "Partial";
            $sale_order->update($attributes_purchase);
        } else {

            $attributes_purchase['payment_status'] = "Pending";
            $sale_order->update($attributes_purchase);
        }

        $order = \App\Models\Orders::find($id);

        $order_product_type = \App\Models\OrderDetail::where('order_id', $order->id)->select('product_type_id')->distinct('product_type_id')->get();


        $attributes_order = $request->all();

        $outlet_ledgers = $order->outlet->ledger_id;

        $attributes_order['entrytype_id'] = 15;
        $attributes_order['tag_id'] = 29; //sales invoice
        $attributes_order['user_id'] = \Auth::user()->id;
        $attributes_order['org_id'] = \Auth::user()->org_id;
        $attributes_order['number'] = $id;
        $attributes_order['resv_id'] = $order->reservation_id;
        $attributes_order['source'] = 'POS_PAYMENT';
        $attributes_order['date'] = \Carbon\Carbon::today();

        $attributes_order['notes'] = $request->note;

        if ($request->paid_by == 'complementry') {

            $attributes_order['dr_total'] = 0;
            $attributes_order['cr_total'] = 0;
        } else {

            $attributes_order['dr_total'] = $order->total_amount;
            $attributes_order['cr_total'] = $order->total_amount;
        }

        $entry = \App\Models\Entry::create($attributes_order);


        if ($request->paid_by == 'cash') {


            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being sales made ';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being sales made ';
                $cash_amount->save();
            }

            // cash account

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = env('CASH_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being payment made';
            $cash_amount->save();
        } elseif ($request->paid_by == 'check') {


            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
                $cash_amount->save();
            }

            // cash account

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = env('CHEQUE_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being unsettlemnt made Bank Name: ' . $request['bank_name'] . ' Cheque No: ' . $request['cheque_no'] . ' Cheque Date: ' . $request['cheque_date'] . '';
            $cash_amount->save();
        } elseif ($request->paid_by == 'travel-agent') {

            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made';
                $cash_amount->save();
            }


            $temp_ledger = \App\Models\COALedgers::where('name', $request['travel_agent_ledger'])->first();

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = $temp_ledger->id;
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being unsettlemnt made';
            $cash_amount->save();
        } elseif ($request->paid_by == 'city-ledger') {

            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made';
                $cash_amount->save();
            }

            // Credit account

            $temp_city_ledger = \App\Models\COALedgers::where('name', $request['city_ledger'])->first();

            $credit_amount = new \App\Models\Entryitem();
            $credit_amount->entry_id = $entry->id;
            $credit_amount->dc = 'C';
            $credit_amount->ledger_id = $temp_city_ledger->id;
            $credit_amount->amount = $order->total_amount;
            $credit_amount->narration = 'being unsettlemnt made';
            $credit_amount->save();
        } elseif ($request->paid_by == 'complementry') {

            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = 0.0;
                $cash_amount->narration = 'being unsettlemnt made';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = 0.0;
                $cash_amount->narration = 'being unsettlemnt made';
                $cash_amount->save();
            }

            // Credit account

            $credit_amount = new \App\Models\Entryitem();
            $credit_amount->entry_id = $entry->id;
            $credit_amount->dc = 'C';
            $credit_amount->ledger_id = env('CASH_AMOUNT_LEDGER_ID');
            $credit_amount->amount = 0.0;
            $credit_amount->narration = 'being unsettlemnt made';
            $credit_amount->save();
        } elseif ($request->paid_by == 'staff') {


            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made ';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made ';
                $cash_amount->save();
            }

            // cash account

            $temp_staff_name = \App\Models\COALedgers::where('name', $request['staff_name'])->first();

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = $temp_staff_name->id;
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being unsettlemnt made of ' . $request->staff_name . '';
            $cash_amount->save();
        } elseif ($request->paid_by == 'credit-cards') {


            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made ';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made ';
                $cash_amount->save();
            }


            // cash account

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = 5;
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being unsettlemnt made';
            $cash_amount->save();
        } elseif ($request->paid_by == 'room') {

            $room_no = $request['room_no'];


            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made Romm No:' . $room_no . '';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made Romm No:' . $room_no . '';
                $cash_amount->save();
            }

            // cash account

            $reservation = \App\Models\Reservation::find($request['room_reservation_id']);

            if ($reservation->parent_res == null) {

                $reservation_id = $reservation->id;
            } else {

                $reservation_id = $reservation->parent_res;
            }

            $reservation_ledger_id = $reservation->ledger_id;


            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = $reservation_ledger_id;
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being unsettlemnt made Romm No:' . $room_no . '';
            $cash_amount->save();


            $folio = \App\Models\Folio::orderBy('id', 'asc')->where('reservation_id', $reservation_id)->first();


            $attributes = [];
            $attributes['subtotal'] = $folio->subtotal + $order->subtotal;
            $attributes['service_charge'] = $folio->service_charge + $order->service_charge;
            $attributes['amount_with_service'] = $folio->amount_with_service + $order->amount_with_service;
            $attributes['discount_percent'] = 0;
            $attributes['taxable_amount'] = $folio->taxable_amount + $order->taxable_amount;
            $attributes['tax_amount'] = $folio->tax_amount + $order->tax_amount;
            $attributes['total_amount'] = $folio->total_amount + $order->total_amount;

            $room_description = $order->outlet->name;

            \App\Models\Folio::find($folio->id)->update($attributes);

            $detail_attributes = new \App\Models\FolioDetail();
            $detail_attributes->folio_id = $folio->id;
            $detail_attributes->description = $room_description;
            $detail_attributes->price = $order->subtotal;
            $detail_attributes->quantity = 1;
            $detail_attributes->total = $order->subtotal;
            $detail_attributes->date = \Carbon\Carbon::now();
            $detail_attributes->is_inventory = 0;
            $detail_attributes->posted_to_ledger = 1;
            $detail_attributes->is_posted = 1;
            $detail_attributes->save();
        } elseif ($request->paid_by == 'e-sewa') {


            if ($order->reservation_id) {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $order->reservation->ledger_id;
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made  E-sewa id ' . $request->e_sewa_id . '';
                $cash_amount->save();
            } else {

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = env('POS_GUEST');
                $cash_amount->amount = $order->total_amount;
                $cash_amount->narration = 'being unsettlemnt made  E-sewa id ' . $request->e_sewa_id . '';
                $cash_amount->save();
            }

            // cash account

            $cash_amount = new \App\Models\Entryitem();
            $cash_amount->entry_id = $entry->id;
            $cash_amount->dc = 'C';
            $cash_amount->ledger_id = env('CASH_AMOUNT_LEDGER_ID');
            $cash_amount->amount = $order->total_amount;
            $cash_amount->narration = 'being unsettlemnt made  E-sewa id ' . $request->e_sewa_id . '';
            $cash_amount->save();
        }

        \App\Models\Orders::find($id)->update(['settlement' => 0]);


        Flash::success("UnSettlement Done Successfully");
        return redirect('/admin/orders/todaypos/payments');
    }
}

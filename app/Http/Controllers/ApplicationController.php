<?php

namespace App\Http\Controllers;

use App\Models\Lead as Lead;
use App\Models\Audit as Audit;
use App\Models\Product as Course;
use App\Models\Cases as Cases;
use Flash;
use DB;
use Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

use App\Models\Client;
use Illuminate\Support\Facades\File;

use App\Models\JobApplication;
use App\Models\JobCircular;
use App\Models\Designation;


/**

FOR ONLINE ENQUIRY

 **/

class ApplicationController extends Controller
{
    /**
     * @var Lead
     */
    private $Application;

    public function __construct(Lead $Lead, Course $course, Cases $cases, JobApplication $jobapplication)
    {
        parent::__construct();
        $this->lead = $Lead;
        $this->cases = $cases;
        $this->course = $course;
        $this->jobapplication = $jobapplication;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function form()
    {
        
        $date = date('Y-m-d');
        $room = DB::select("select room.room_number,room_types.room_name FROM room LEFT JOIN room_types ON room.roomtype_id = room_types.roomtype_id WHERE room.room_number NOT In(select room_num FROM reservations WHERE '$date' >= check_in AND '$date' < check_out )");

        $room = json_decode(json_encode($room), true);

        $guest = \App\Models\Contact::all();
        $rstatus = \App\Models\ReservationStatus::all();
        $rtype = \App\Models\ReservationType::all();
        $rates = \App\Models\Rateplan::all();
        $sources = \App\Models\Communication::all();

        $paymenttype = \App\Models\PaymentType::all();

        return view('application.enquiry', compact('room', 'guest','rstatus', 'services', 'rtype', 'rates', 'sources', 'paymenttype'));
    }

    public function getroomPlan($id)
    {

        $room_type_id = \App\Models\Room::select('roomtype_id')->where('room_number', $id)->first();
        $rateplan = \App\Models\Rateplan::where('roomtype_id', $room_type_id->roomtype_id)->get();
        return response()->json($rateplan);
    }

    public function checkRoomForDate($date)
    {

        $room = DB::select("select room.room_number,room_types.room_name FROM room LEFT JOIN room_types ON room.roomtype_id = room_types.roomtype_id WHERE room.room_number NOT In(select room_num FROM reservations WHERE '$date' >= check_in AND '$date' < check_out )");
        return response()->json($room);
    }

    public function events()
    {

        $venue = \App\Models\EventVenues::all();
        $events_type = ['concert', 'dinner', 'lunch', 'hightea', 'cocktail', 'picnic', 'party', 'seminar', 'conference', 'workshop', 'galas', 'csr', 'expo', 'other'];

        return view('application.eventsenquiry', compact('courses', 'communication_id', 'events_type', 'venue'));
    }

    public function postEventFromEnquiry(Request $request)
    {

        $this->validate($request, array(
            'event_name' => 'required',
            'contact_num' => 'required',
            'num_participants' => 'required'
        ));

        $attributes = $request->all();

        //dd($attributes);

        $attributes['user_id'] = 1;

        $events = \App\Models\Event::create($attributes);

        foreach ($request->requirements as $requirements) {

            $req = ['name' => $requirements, 'event_id' => $events->id];
            \App\Models\EventRequirements::create($req);
        }

        return redirect('/event_enquiry_thankyou')->with('lead_id', $events->id)->with('event_name', $request['event_name'])->with('person_name', $request['person_name'])->with('mob_phone', $request['contact_num']);
    }
    public function thankyouEvent()
    {

        return view('application.eventsthankyou');
    }


    public function leads()
    {

        $key1 = \Request::get('communication_id');
        //dd($key1);
        $key2 = \Request::get('lead_type');
        if ($key2 != '') {

            $leadtypes = \App\Models\Leadtype::where('name', $key2)->where('enabled', '1')->select('name', 'id')->get();
        } else {
            $leadtypes = \App\Models\Leadtype::where('enabled', '1')->select('name', 'id')->get();
        }

        if ($key1 != '') {
            $communication_id = $key1;
        } else {
            $communication_id = 8;
        }

        //dd($leadtypes);

        return view('application.leadsenquiry', compact('leadtypes', 'communication_id'));
    }
    public function postLeadFromEnquiry(Request $request)
    {

        $this->validate(
            $request,
            array(
                'lead_type_id' => 'required',
                'name' => 'required',
                'mob_phone' => 'required',
                'email' => 'required'
            )
        );

        $attributes = $request->all();

        //$attributes['communication_id'] = '8';    //Since the communication is from Website, 8 = Crmenquiry from databaseenqiury mode from database
        $attributes['status_id'] = '28';    // 17 = Pending status from database
        $attributes['user_id'] = '1';   // 1 = Root, user is setup as online to post this entry
        $attributes['viewed'] = '0';
        $attributes['org_id'] = '1';


        $lead = $this->lead->create($attributes);


        //send email
        //       $mail = \Mail::send('emails.application-letter', ['lead'=>$lead], function ($message) use ($request) {
        //          $message->subject('Online Enquiry from '.env('APP_NAME'));
        //          $message->from(env('APP_EMAIL'), env('APP_NAME'));
        //          $message->to($request['email'], '');
        //      });

        return redirect('/lead_enquiry_thankyou')->with('lead_id', $lead->id)->with('name', $request['name'])->with('mob_phone', $request['mob_phone']);
    }
    public function thankyouLead()
    {

        return view('application.leadsthankyou');
    }

    public function postPreview(Request $request)
    {
        $this->validate(
            $request,
            array(
                'course_id' => 'required',
                'name' => 'required',
                'mob_phone' => 'required',
                'email' => 'required'
            )
        );

        $attributes = $request->all();
        // TODO: This lead_type_id can be dynamic later when this CRM grows like post_type in wordpress
        $attributes['lead_type_id'] = '1';

        $attributes['communication_id'] = '8';    //Since the communication is from Website, 8 = Crmenquiry from databaseenqiury mode from database
        $attributes['status_id'] = '17';    // 17 = Pending status from database
        $attributes['user_id'] = '1';    // 1 = Root, user is setup as online to post this entry
        $attributes['viewed'] = '0';

        $lead = $this->lead->create($attributes);
        $request['lead_id'] = $lead->id;
        return view('application.form-preview', $request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEnquiry(Request $request)
    {
        $this->validate(
            $request,
            array(
                'course_id' => 'required',
                'name' => 'required',
                'mob_phone' => 'required',
                'email' => 'required'
            )
        );

        $attributes = $request->all();
        // TODO: This lead_type_id can be dynamic later when this CRM grows like post_type in wordpress
        $attributes['lead_type_id'] = '1';

        //$attributes['communication_id'] = '8';	//Since the communication is from Website, 8 = Crmenquiry from databaseenqiury mode from database
        $attributes['status_id'] = '28';    // 17 = Pending status from database
        $attributes['user_id'] = '1';    // 1 = Root, user is setup as online to post this entry
        $attributes['viewed'] = '0';
        $attributes['org_id'] = '1';

        $lead = $this->lead->create($attributes);


        //send email
        //		 $mail = \Mail::send('emails.application-letter', ['lead'=>$lead], function ($message) use ($request) {
        //			$message->subject('Online Enquiry from '.env('APP_NAME'));
        //			$message->from(env('APP_EMAIL'), env('APP_NAME'));
        //			$message->to($request['email'], '');
        //		});

        return redirect('/enquiry_thankyou')->with('lead_id', $lead->id)->with('name', $request['name'])->with('course', \TaskHelper::getCourseName($request['course_id']))->with('mob_phone', $request['mob_phone']);
    }


    public function postReservation(Request $request)
    {

        $this->validate($request, array(
            'check_in'    => 'required|date|before:check_out',
            'check_out'   => 'required|date',
            'rate_id' => 'required'
        ));

        // $check_checkout=Reservation::where('check_in','<=',$request->check_out)->where('room_num',$request->room_num)->where('check_in','>',$request->check_in)->first();

        // if($check_checkout){
        // $error = "Room number ".$request->room_num." is already booked from ".$check_checkout->check_in." to ".$check_checkout->check_out;
        // return \Redirect::back()->withErrors([$error]);
        // }

        $reservation = $request->all();

        //dd($reservation);

        // dd($reservation);

        if ($request->submit_method == 'check_in') {
            $status = \App\Models\ReservationStatus::where('status_name', 'Checked In')->first();
            $reservation['reservation_status'] = $status->id;
        }
        if ($request->submit_method == 'reserv') {
            $status = \App\Models\ReservationStatus::where('status_name', 'Due In')->first();
            $reservation['reservation_status'] = $status->id;
        }

        if ($request->company_id) {

            $reservation['ledger_id'] = \App\Models\Client::find($request->company_id)->ledger_id;
        }

        // dd($reservation);


        $reservation['user_id'] = \Auth::user()->id;
        $reservation = \App\Models\Reservation::create($reservation);

        \App\Models\Room::where('room_number', $reservation->room_num)->update(['status_id' => '0']);
        foreach ($request->room_no1 as $moreroom) {
            $more = ['room_num' => $moreroom, 'res_id' => $reservation->id];
            ReservationRoom::create($more);
            if ($update_room) {
                \App\Models\Room::where('room_number', $moreroom)->update($update_room);
            }
        }


        foreach ($request->full_name as $key => $fullname) {
            $moreguest = ['full_name' => $fullname, 'res_id' => $reservation->id, 'doc_type' => $request->doc_type[$key], 'doc_num' => $request->doc_num[$key], 'guests_type' => $request->guests_type[$key]];
            \App\Models\ReservationGuest::create($moreguest);
        }
        $files = $request->file('doc');
        foreach ($files as $doc_) {
            $doc_name = time() . "" . $doc_->getClientOriginalName();
            $destinationPath = public_path('/reservation/');
            $doc_->move($destinationPath, $doc_name);
            $doc_name = "/reservation/" . $doc_name;
            $reservation_doc = ['res_id' => $reservation->id, 'doc' => $doc_name, 'user_id' => \Auth::user()->id];
            \App\Models\ReservationDoc::create($reservation_doc);
        }

        return redirect('/enquiry_thankyou')->with('lead_id', $reservation->id)->with('name', $request['guest_name'])->with('mob_phone', $request['mobile']);
    }


    /**
     * @return \Illuminate\View\View
     */
    public function thankyou()
    {
        return view('application.thankyou');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function ticket()
    {
        // Courses means Product here
        $courses = \App\Models\Product::where('enabled', '1')->orderBy('name', 'asc')->select('name', 'id')->get();
        return view('application.ticket', compact('courses'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postTicket(Request $request)
    {
        $this->validate(
            $request,
            array(
                'client_id' => 'required',
                'ticket_name' => 'required',
                'ticket_email' => 'required',
                'subject' => 'required'
            )
        );

        $attributes = $request->all();
        $attributes['user_id'] = \Auth::user()->id;

        $temp_client = Client::where('name', $request['client_id'])->first();
        if (!$temp_client) {
            return \Redirect::back()->withInput(\Request::all())->with('warning', 'Please select the valid Client from autocomplete.');
        } else
            $attributes['client_id'] = $temp_client->id;


        /*$temp_contact = Contact::where('full_name', $request['contact_id'])->first();
        if(!$temp_contact)
        {
            Flash::warning('Please select the valid Contact.');
            return \Redirect::back()->withInput(\Request::all());
        }
        else
            $attributes['contact_id'] = $temp_contact->id;*/

        $attributes['status'] = 'new';
        $attributes['assigned_to'] = '1';
        $attributes['viewed'] = 0;
        $attributes['enabled'] = 0;
        $attributes['enabled'] = 0;
        $attributes['type'] = 'ticket';

        // Save the attachment data and file
        if ($request->file('attachment')) {
            $stamp = date('Ymdhis') . '_';
            $file = $request->file('attachment');
            $destinationPath = public_path() . '/case_attachments/';
            $filename = $file->getClientOriginalName();
            $attributes['attachment'] = $stamp . $filename;
            $request->file('attachment')->move($destinationPath, $stamp . $filename);
        }

        $case = $this->cases->create($attributes);

        // Look at this email sending
        //send email
        $mail = \Mail::send('emails.ticket', ['case' => $case], function ($message) use ($request) {
            $message->subject('Online Ticket from ' . env('APP_NAME'));
            $message->from(env('APP_EMAIL'), env('APP_NAME'));
            $message->to(env('APP_EMAIL'), '');
        });

        return redirect('/ticket_thankyou')->with('id', $case->id)->with('type', $case->type)->with('priority', $case->priority)->with('subject', $case->subject)->with('description', $case->description)->with('attachment', $case->attachment);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function ticketThankyou()
    {
        return view('application.ticketThankyou');
    }

    // For load clients by ajax
    public function get_client()
    {
        $term = strtolower(\Request::get('term'));
        $contacts = Client::select('id', 'name')->where('name', 'LIKE', '%' . $term . '%')->where('enabled', '1')->groupBy('name')->take(5)->get();
        $return_array = array();

        foreach ($contacts as $v) {
            if (strpos(strtolower($v->name), $term) !== FALSE) {
                $return_array[] = array('value' => $v->name, 'id' => $v->id);
            }
        }
        return \Response::json($return_array);
    }

    public function listJob()
    {
        $page_title = 'Jobs';
        $page_description = 'All Jobs';

        $circulars = JobCircular::where('status', 'published')->orderBy('created_at', 'desc')->get();
        return view('application.listjob', compact('circulars'));
    }

    public function applyJob($job_circular_id)
    {
        $circular = JobCircular::where('job_circular_id', $job_circular_id)->first();
        return view('application.applyjob', compact('circular'));
    }

    public function jobApplyForm()
    {
        return view('application.apply-form');
    }


    public function postJobApplication(Request $request)
    {
        $this->validate(
            $request,
            array(
                'name' => 'required',
                'mobile' => 'required',
                'email' => 'required',
                'resume' => 'required'
            )
        );

        $attributes = $request->all();
        $attributes['application_status'] = '0';

        $stamp = time();
        $file = $request->file('resume');
        //dd($file);
        $destinationPath = public_path() . '/job_applied/';
        $filename = $file->getClientOriginalName();
        $file->move($destinationPath, $stamp . '_' . $filename);
        $attributes['resume'] = $stamp . '_' . $filename;
        //dd($attributes);
        $jobapplication = $this->jobapplication->create($attributes);


        //send email
        //       $mail = \Mail::send('emails.application-letter', ['lead'=>$lead], function ($message) use ($request) {
        //          $message->subject('Online Enquiry from '.env('APP_NAME'));
        //          $message->from(env('APP_EMAIL'), env('APP_NAME'));
        //          $message->to($request['email'], '');
        //      });

        return redirect('/job_thankyou');
    }

    public function jobThankyou()
    {
        return view('application.job-thankyou');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\BusinessDate;
use Illuminate\Http\Request;
use Flash;

class BusinessDateController extends Controller
{
    public function getBusinessDate(){
        $page_title='Business Date';
        $page_description='Set Business Date';
        $business_date=BusinessDate::first();
        return view('admin.business-date.edit',compact('page_title','page_description','business_date'));

    }
    public function setBusinessDate(Request $request){
        $request->validate([
            'business_date'=>'date|required'
        ]);
        $date=BusinessDate::first();
        $date?BusinessDate::first()->update(['business_date'=>$request->business_date]):BusinessDate::create(['business_date'=>$request->business_date]);
        Flash::success('Business Date configured successfully');
        return redirect('admin/business-date');

    }
}

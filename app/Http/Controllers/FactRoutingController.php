<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Audit as Audit;
use App\Models\Client;
use App\Models\Contact;
use App\Models\FactBOM;
use App\Models\FactRouting;
use App\Models\FactRoutingDetail;
use App\Models\Lead;
use App\Models\MasterComments;
use App\Models\OrderDetail;
use App\Models\Orders;
use App\Models\Orders as Order;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use App\Models\Role as Permission;
use App\Models\Sauda;
use App\Models\SaudaDetails;
use App\Models\StockMove;
use App\User;
use Auth;
use DB;
use Flash;
use Illuminate\Http\Request;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER.
 */
class FactRoutingController extends Controller
{
    /**
     * @var Client
     */
    private $fact_routing;

    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param Client $bug
     * @param Permission $permission
     * @param User $user
     */
    public function __construct(Permission $permission, FactRouting $fact_routing)
    {
        parent::__construct();
        $this->permission = $permission;
        $this->fact_routing = $fact_routing;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $filterdate = function ($query) {

            $start_date = \Request::get('start_date');

            $end_date = \Request::get('end_date');

            if ($start_date && $end_date) {
                return $query->where('bill_date', '>=', $start_date)
                    ->where('bill_date', '<=', $end_date);
            }

        };

        $filterSupplier = function ($query) {

            $supplier = \Request::get('supplier_id');
            if ($supplier) {

                return $query->where('supplier_id', $supplier);
            }

        };

        $filterCurrency = function ($query) {

            $currency = \Request::get('currency');

            if ($currency) {

                return $query->where('currency', $currency);
            }

        };

        $orders = FactRouting::select('fact_routing.id as id', 'fact_routing.name as route_name', 'fact_routing.created_at as created_at', 'products.name as product_name',)
            ->leftjoin('products', 'fact_routing.product_id', '=', 'products.id')
          
            ->orderBy('id', 'DESC');


        if (\Request::get('search') && \Request::get('search') == 'true') {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate(40);
        }

        $page_title = ' Recipe Routing';
        $page_description = 'Manage Production Routing';
        $suppliers = Client::where('relation_type', 'supplier')->pluck('name', 'id')->all();
        $currency = \App\Models\Country::whereEnabled('1')->pluck('currency_name', 'currency_code as id')->all();


        return view('admin.fact_routing.index', compact('orders', 'currency', 'page_title', 'page_description', 'suppliers'));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function show(Request  $request ,$id)
    {
        $ord = $this->fact_routing->find($id);
        $page_title = ' Factory Routing';
        $page_description = 'Manage Production Routing';
        $orderDetails = FactRoutingDetail::where('routing_id', $ord->id)->get();
        //dd($orderDetails);
        $imagepath = \Auth::user()->organization->logo;
        if($request->is('admin/production/fact_routing/print/'.$id)){
            return view('admin.fact_routing.print',compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
        }
        // dd($imagepath);

        return view('admin.fact_routing.show', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $page_title = ' Recipe Routing';
        $page_description = 'Manage Production Routing';
        $order = null;
        $orderDetail = null;
        $final_products = \App\Models\Product::select('id', 'name')->where('assembled_product', 1)->get();
//        $products = FactoryProductLedger::select('fact_product_ledger.id','product_id','products.name','products.unit',DB::raw('SUM(credit_qty) as total_credit_qty'))
//            ->leftjoin('products','products.id','=','fact_product_ledger.product_id')
//            ->where('credit_qty','>',0)->groupBy('product_id')->where('products.component_product',1)->get();
        $products = \App\Models\Product::where('component_product', 1)->get();
        $users = \App\User::where('enabled', '1')->where('org_id', \Auth::user()->org_id)->pluck('first_name', 'id');

        $bom = \App\Models\FactBOM::orderBy('created_at', 'DESC')->get();
        $section = \App\Models\ProductsSection::pluck('section_name', 'id');

        return view('admin.fact_routing.create', compact('page_title', 'bom', 'section', 'final_products', 'users', 'order_count', 'page_description', 'products',));
    }


    public function store(Request $request)
    {
        $attributes = $request->all();
        $fact_routing = FactRouting::create($attributes);

        //  Request Detail Create
        $Fact_routing_id = $fact_routing->id;
        $area_id = $request->area_id;
        $descr = $request->descr;
        $setup_time = $request->setup_time;
        $cycle_time = $request->cycle_time;
        $overhead_cost = $request->overhead_cost;
        $labour_cost = $request->labour_cost;
        $to_user_id = $request->to_user_id;

        foreach ($area_id as $key => $value) {
            if ($value != '') {
                $detail = FactRoutingDetail::create([
                    "routing_id" => $Fact_routing_id,
                    "area_id" => $area_id[$key],
                    "descr" => $descr[$key],
                    "setup_time" => $setup_time[$key],
                    "cycle_time" => $cycle_time[$key],
                    "overhead_cost" => $overhead_cost[$key],
                    "labour_cost" => $labour_cost[$key],
                    "to_user_id" => $to_user_id[$key],
                    'process_order'=>$request->process_order[$key],
                ]);
            }
        }

        Flash::success('Production Routing created Successfully.');
        return redirect('/admin/production/fact_routing');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $page_title = ' Recipe Routing';
        $page_description = 'Manage Production Routing';
        $order = FactRouting::where('id', $id)->first();
        $orderDetails = FactRoutingDetail::where('routing_id', $id)->get();

        $final_products = \App\Models\Product::select('id', 'name')->where('assembled_product', 1)->get();
//        $products = FactoryProductLedger::select('fact_product_ledger.id','product_id','products.name','products.unit',DB::raw('SUM(credit_qty) as total_credit_qty'))
//            ->leftjoin('products','products.id','=','fact_product_ledger.product_id')
//            ->where('credit_qty','>',0)->groupBy('product_id')->where('products.component_product',1)->get();
        $products = \App\Models\Product::where('component_product', 1)->get();
        $users = \App\User::where('enabled', '1')->where('org_id', \Auth::user()->org_id)->pluck('first_name', 'id');

        $bom = \App\Models\FactBOM::orderBy('created_at', 'DESC')->get();
        $section = \App\Models\ProductsSection::pluck('section_name', 'id');

        return view('admin.fact_routing.edit', compact('page_title', 'bom', 'section', 'final_products', 'users', 'order_count', 'page_description', 'products', 'order', 'orderDetails',));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {


        $fact_routing = $this->fact_routing->find($id);

        $fact_routing->update($request->all());
        $orderDetails = FactRoutingDetail::where('routing_id', $id)->delete();

        //  Request Detail Create
        $Fact_routing_id = $fact_routing->id;
        $area_id = $request->area_id;
        $descr = $request->descr;
        $setup_time = $request->setup_time;
        $cycle_time = $request->cycle_time;
        $overhead_cost = $request->overhead_cost;
        $labour_cost = $request->labour_cost;
        $to_user_id = $request->to_user_id;


//        dd($request->all());
        //dd($order);
        if ($fact_routing->isEditable()) {
            foreach ($area_id as $key => $value) {
                if ($value != '') {
                    $detail = FactRoutingDetail::create([
                        "routing_id" => $Fact_routing_id,
                        "area_id" => $area_id[$key],
                        "descr" => $descr[$key],
                        "setup_time" => $setup_time[$key],
                        "cycle_time" => $cycle_time[$key],
                        "overhead_cost" => $overhead_cost[$key],
                        "labour_cost" => $labour_cost[$key],
                        "to_user_id" => $to_user_id[$key],
                        'process_order'=>$request->process_order[$key],
                    ]);
                }
            }
            Flash::success('Production Routing Updated Successfully.');
            return redirect('/admin/production/fact_routing');
        } else {
            Flash::success('Error in updating Production Routing.');
        }

        return \Redirect::back()->withInput(\Request::all());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $fact_routing = $this->fact_routing->find($id);

        if (!$fact_routing->isdeletable()) {
            abort(403);
        }

        $fact_routing->delete();
        FactRoutingDetail::where('routing_id', $id)->delete();

        Flash::success('Factory Routing successfully deleted.');
        return redirect('/admin/production/fact_routing');
    }

    public function getProductDetailAjax($productId)
    {
        $product = Course::select('id', 'name', 'price', 'cost')->where('id', $productId)->first();


        return ['data' => json_encode($product)];
    }

    /**
     * Delete Confirm.
     *
     * @param int $id
     * @return  View
     */
    public function deleteModal($id)
    {
        $error = null;

        $modal_title = 'Delete Factory Routing';

        $fact_routing = $this->fact_routing->find($id);

        $modal_body = 'Are you sure you want to delete this Factory Routing?';
        $modal_route = route('admin.production.fact_routing.delete',  $fact_routing->id
        );
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    private function convertdate($date)
    {
        $cal = new \App\Helpers\NepaliCalendar();
        $converted = explode('-', $date);
        $converted = $cal->nep_to_eng($converted[0], $converted[1], $converted[2]);
        $converted_date = $converted['year'] . '-' . $converted['month'] . '-' . $converted['date'];

        return $converted_date;
    }


    public function printInvoice($id)
    {
        $ord = $this->fact_routing->find($id);
        $orderDetails = PurchaseOrderDetail::where('order_no', $id)->get();
        //dd($orderDetails);
        $imagepath = \Auth::user()->organization->logo;

        return view('admin.fact_routing.print', compact('ord', 'imagepath', 'orderDetails'));
    }

    public function generatePDF($id)
    {
        $ord = $this->fact_routing->find($id);
        $orderDetails = PurchaseOrderDetail::where('order_no', $id)->get();
        $imagepath = \Auth::user()->organization->logo;

        $pdf = \PDF::loadView('admin.fact_routing.generateInvoicePDF', compact('ord', 'imagepath', 'orderDetails'));
        $file = $id . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';

        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }

        return $pdf->download($file);
    }




}

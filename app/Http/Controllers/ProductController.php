<?php

namespace App\Http\Controllers;

use App\Models\StockAdjustmentDetail;
use App\Models\StockMove;
use App\Models\Audit as Audit;
use App\Models\Product as Course;
use App\Models\Role as Permission;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER
 */

class ProductController extends Controller
{
    /**
     * @var Course
     */
    private $course;

    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param Course $course
     * @param Permission $permission
     * @param User $user
     */
    public function __construct(Course $course, Permission $permission)
    {
        parent::__construct();
        $this->course = $course;
        $this->permission = $permission;
    }

    /**
     * @return \Illuminate\View\View
     */


    public function downloadExcel($query){


        $courses = $query->get()->toArray();


        return \Excel::download(new \App\Exports\ExcelExport($courses), "products.csv");;


    }



    public function index()
    {
        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-index'));

        $courses = $this->course->select('products.*','product_categories.name as product_categories','pos_outlets.name as outlet_name')->where(function ($query) {
            $terms = \Request::get('term');
            if ($terms)
                return $query->where('products.name', 'LIKE', '%' . $terms . '%');
        })
            ->where(function ($q){
                $q->where('parent_product_id',0);
                $q->orWhereNull('parent_product_id');
            })->where(function($query){

            $type_master_id = \Request::get('product_cat_master');

            if($type_master_id){

                return $query->where('product_type_masters.id',$type_master_id);
            }


        })->where(function($query){

            $product_cat = \Request::get('product_cat');

            if($product_cat){

                return $query->where('product_categories.id',$product_cat);
            }


        })->leftjoin('product_type_masters','products.product_type_id','=','product_type_masters.id')
        ->leftjoin('pos_outlets','pos_outlets.id','=','products.outlet_id')
        ->leftjoin('product_categories','products.category_id','=','product_categories.id')
        ->orderBy('id', 'desc');



        if(\Request::get('productSearch') == 'excel'){


            return $this->downloadExcel($courses);

        }
        $courses = $courses->paginate(30);


        $page_title = 'Products & Inventory';

        $page_description = trans('admin/courses/general.page.index.description');

        $producttypeMaster = \App\Models\ProductTypeMaster::pluck('name','id');

        $productCategory = \App\Models\ProductCategory::pluck('name','id');
        //dd($transations);

        return view('admin.products.index', compact('courses', 'page_title', 'page_description','producttypeMaster','productCategory'));
    }



    /**
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $course = $this->course->find($id);

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-show', ['name' => $course->name]));

        $page_title = trans('admin/courses/general.page.show.title'); // "Admin | Course | Show";
        $page_description = trans('admin/courses/general.page.show.description'); // "Displaying course: :name";
        return view('admin.courses.show', compact('course', 'page_title', 'page_description'));

        return view('admin.products.show', compact('course', 'page_title', 'page_description'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $page_title = trans('admin/courses/general.page.create.title'); // "Admin | Course | Create";
        $page_description = trans('admin/courses/general.page.create.description'); // "Creating a new course";

        $course = new \App\Models\Product();
        $perms = $this->permission->all();
        $categories = \App\Models\ProductCategory::orderBy('name', 'ASC')->where('org_id', \Auth::user()->org_id)->pluck('name', 'id');
        $product_unit = \App\Models\ProductsUnit::pluck('name', 'id');
        $outlets  = \App\Models\PosOutlets::orderBy('id', 'asc')->pluck('name', 'id')->all();

        $product_type_masters = \App\Models\ProductTypeMaster::pluck('name', 'id');
        $products = \App\Models\Product::latest()->where('org_id', \Auth::user()->org_id)->pluck('name', 'id')->all();


        if(\Request::ajax()){

            return view('admin.products.modals.create', compact('course', 'perms', 'outlets', 'page_title', 'page_description', 'categories', 'product_unit', 'product_type_masters'));


        }

        return view('admin.products.create', compact('products','course', 'perms', 'outlets', 'page_title', 'page_description', 'categories', 'product_unit', 'product_type_masters'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (\Request::ajax()) {
            $validator = \Validator::make($request->all(), [
                'name'  => 'required|unique:products',
            ]);
            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            }
        }
        $this->validate($request, ['name'  => 'required|unique:products',]);

        $attributes = $request->all();

        $attributes['org_id'] = Auth::user()->org_id;
          $attributes['created_by'] = Auth::user()->id;
        if ($request->file('product_image')) {
            $stamp = time();
            $file = $request->file('product_image');
            $destinationPath = public_path() . '/products/';
            if (!\File::isDirectory($destinationPath)) {
                \File::makeDirectory($destinationPath, 0777, true, true);
            }
            $filename = $file->getClientOriginalName();
            $request->file('product_image')->move($destinationPath, $stamp . '_' . $filename);
            $attributes['product_image'] = $stamp . '_' . $filename;
        }

        // dd($attributes);

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-store', ['name' => $attributes['name']]));
        $tax_amount=0;
        if ($request->includes_tax==1){
            $amount_without_tax=$request->price/1.13;
            $tax_amount=round($request->price-$amount_without_tax,2);
        }
        $attributes['tax_amount']=$tax_amount;
        $course = $this->course->create($attributes);

        if ($request->is_fixed_assets) {
            $this->postledgers($course);
        }
        if (\Request::ajax()) {
            return ['status' => 'success', 'lastcreated' => $course];
        }
        Flash::success(trans('admin/courses/general.status.created')); // 'Course successfully created');

        return redirect('/admin/products');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $course = $this->course->find($id);
        $categories = \App\Models\ProductCategory::orderBy('name', 'ASC')->where('org_id', \Auth::user()->org_id)->pluck('name', 'id');

        //dd($categories);


        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-edit', ['name' => $course->name]));

        $page_title = trans('admin/courses/general.page.edit.title'); // "Admin | Course | Edit";
        $page_description = trans('admin/courses/general.page.edit.description', ['name' => $course->name]); // "Editing course";


        $transations = \App\Models\StockMove::where('product_stock_moves.stock_id', $id)
            ->leftjoin('products', 'products.id', '=', 'product_stock_moves.stock_id')
            ->leftjoin('product_location', 'product_location.id', '=', 'product_stock_moves.location')
            ->select('product_stock_moves.*', 'products.name', 'product_location.location_name')
            ->get();

        $locData = DB::table('product_location')->get();

        $loc =  DB::table('product_location')->get();

        $loc_name = array();
        foreach ($loc as $value) {
            $loc_name[$value->id] = $value->location_name;
        }

        $loc_name = $loc_name;


        if (!$course->isEditable() &&  !$course->canChangePermissions()) {
            abort(403);
        }

        $product_unit = \App\Models\ProductsUnit::pluck('name', 'id');

        $product_type_masters = \App\Models\ProductTypeMaster::pluck('name', 'id');

        $outlets  = \App\Models\PosOutlets::orderBy('id', 'asc')->pluck('name', 'id')->all();

        $menus = \App\Models\PosMenu::orderBy('id', 'desc')->where('outlet_id', $course->outlet_id)->pluck('menu_name', 'id')->all();
        $products = \App\Models\Product::latest()->where('org_id', \Auth::user()->org_id)
            ->where('id','!=',$id)
            ->pluck('name', 'id')->all();


        return view('admin.products.edit', compact('products','course', 'product_unit', 'outlets', 'page_title', 'locData', 'loc_name', 'page_description', 'transations', 'categories', 'product_type_masters', 'menus'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array('name'          => 'required|unique:products,name,' . $id,));
        $course = $this->course->find($id);

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-update', ['name' => $course->name]));

        $attributes = $request->all();
        $attributes['org_id'] = \Auth::user()->org_id;

        $lastedUpdated = $course->updated_by ? $course->updated_by : '{}';
        $lastedUpdated = json_decode($lastedUpdated,true);

        $lastedUpdated [] = [
            'user_id'=>Auth::user()->id,
            'date'=>date('Y-m-d H:i:s'),
            'price'=>$request->price,
            'name'=>$request->name
        ];

        $lastedUpdated = json_encode($lastedUpdated);

        $attributes['updated_by'] = $lastedUpdated;

        // dd($request->file('product_image'));

        if ($request->file('product_image')) {
            $stamp = time();
            $destinationPath = public_path() . '/products/';
            $file = \Request::file('product_image');

            $filename = $file->getClientOriginalName();
            \Request::file('product_image')->move($destinationPath, $stamp . '_' . $filename);

            $attributes['product_image'] = $stamp . '_' . $filename;
        }
        $tax_amount=0;
        if ($request->includes_tax==1){
            $amount_without_tax=$request->price/1.13;
            $tax_amount=round($request->price-$amount_without_tax,2);
        }
        $attributes['tax_amount']=$tax_amount;
        if ($course->isEditable()) {
            $course->update($attributes);
        }

        Flash::success(trans('admin/courses/general.status.updated')); // 'Course successfully updated');

        return redirect('/admin/products');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $course = $this->course->find($id);

        if (!$course->isdeletable()) {
            abort(403);
        }

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-destroy', ['name' => $course->name]));

        $course->delete();

        Flash::success(trans('admin/courses/general.status.deleted')); // 'Course successfully deleted');

        return redirect('/admin/products');
    }

    /**
     * Delete Confirm
     *
     * @param   int   $id
     * @return  View
     */
    public function getModalDelete($id)
    {
        $error = null;

        $course = $this->course->find($id);

        if (!$course->isdeletable()) {
            abort(403);
        }

        $modal_title = trans('admin/courses/dialog.delete-confirm.title');

        $course = $this->course->find($id);
        $modal_route = route('admin.products.delete', $course->id);

        $modal_body = trans('admin/courses/dialog.delete-confirm.body', ['id' => $course->id, 'name' => $course->name]);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable($id)
    {
        $course = $this->course->find($id);

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-enable', ['name' => $course->name]));

        $course->enabled = true;
        $course->save();

        Flash::success(trans('admin/courses/general.status.enabled'));

        return redirect('/admin/products');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable($id)
    {
        //TODO: Should we protect 'admins', 'users'??

        $course = $this->course->find($id);

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-disabled', ['name' => $course->name]));

        $course->enabled = false;
        $course->save();

        Flash::success(trans('admin/courses/general.status.disabled'));

        return redirect('/admin/products');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function enableSelected(Request $request)
    {
        $chkCourses = $request->input('chkCourse');

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-enabled-selected'), $chkCourses);

        if (isset($chkCourses)) {
            foreach ($chkCourses as $course_id) {
                $course = $this->course->find($course_id);
                $course->enabled = true;
                $course->save();
            }
            Flash::success(trans('admin/courses/general.status.global-enabled'));
        } else {
            Flash::warning(trans('admin/courses/general.status.no-course-selected'));
        }
        return redirect('/admin/products');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function disableSelected(Request $request)
    {
        //TODO: Should we protect 'admins', 'users'??

        $chkCourses = $request->input('chkCourse');

        Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-disabled-selected'), $chkCourses);

        if (isset($chkCourses)) {
            foreach ($chkCourses as $course_id) {
                $course = $this->course->find($course_id);
                $course->enabled = false;
                $course->save();
            }
            Flash::success(trans('admin/courses/general.status.global-disabled'));
        } else {
            Flash::warning(trans('admin/courses/general.status.no-course-selected'));
        }
        return redirect('/admin/courses');
    }

    /**
     * @param Request $request
     * @return array|static[]
     */
    public function searchByName(Request $request)
    {
        $return_arr = null;
        $query = $request->input('query');

        $courses = Product::where('name', 'LIKE', '%' . $query . '%')->get();


        foreach ($courses as $course) {
            $id = $course->id;
            $name = $course->name;
            $email = $course->email;

            $entry_arr = ['id' => $id, 'text' => "$name ($email)"];
            $return_arr[] = $entry_arr;
        }

        return $return_arr;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getInfo(Request $request)
    {
        $id = $request->input('id');
        $course = $this->course->find($id);

        return $course;
    }

    public function get_products()
    {
        $term = strtolower(\Request::get('term'));
        $outlet_id = \Request::get('outlet_id');
        $menu_id = \Request::get('menu_id');

        $productcategory_id = \Request::get('productcategory_id');

        // dd($productcategory_id);
        $categories = \App\Models\ProductCategory::where('enabled', '1')->pluck('id')->all();


        $products = \App\Models\Product::select('id', 'name')

//            ->where(function ($query)  use ($categories, $productcategory_id, $term) {
//                if ($categories && !$productcategory_id) {
//                    return $query->where('name', 'LIKE', '%' . $term . '%')->whereIn('category_id', $categories);
//                }
//            })
            ->where(function ($query)  use ($menu_id, $term) {
                if ($term) {
                    return $query->where('name', 'LIKE', '%' . $term . '%');
                }
            })
            ->where(function($query){

                return $query->whereNull('is_raw_material')
                        ->orWhere('is_raw_material','!=','1');

            })
            ->where('outlet_id',$outlet_id)
            ->where('enabled', '1')
            ->groupBy('name')
            ->take(15)
            ->get();
        $return_array = array();

        foreach ($products as $v) {
//            if (strpos(strtolower($v->name), $term) !== FALSE) {
                $return_array[] = array('value' => $v->name, 'id' => $v->id);
//            }
        }
        return \Response::json($return_array);
    }

    public function get_products_front()
    {

        $term = strtolower(\Request::get('term'));

        $products = \App\Models\Article::select('id', 'code', 'description')
            ->where('code', 'LIKE', '%' . $term . '%')
            ->orWhere('description', 'LIKE', '%' . $term . '%')
            ->groupBy('description')
            ->take(5)->get();

        //dd($products);




        $return_array = array();

        foreach ($products as $v) {

            $return_array[] = array('value' => $v->description, 'id' => $v->id,);
        }

        // dd($return_array);

        return \Response::json($return_array);
    }
    public function stocks_by_location()
    {

        $page_title = 'Stock Feeds';
        $page_description = 'counts';

        $location = \App\Models\ProductLocation::pluck('location_name', 'id')->all();

        //dd($location);

        return view('admin.products.stocksbylocation', compact('page_title', 'page_description', 'location'));
    }

    public function stocks_by_location_post(Request $request)
    {

        $page_title = 'Stock By Location';
        $page_description = 'counts';

        $transations =\App\Models\StockMove::leftjoin('products', 'products.id', '=', 'product_stock_moves.stock_id')
            ->leftjoin('product_location', 'product_location.id', '=', 'product_stock_moves.location')
            ->select('product_stock_moves.*', 'products.name', 'product_location.location_name')
            ->orderBy('product_stock_moves.tran_date', 'DESC')
            ->where('location', $request->location_id)
            ->get();


        $current_location = $request->location_id;


        $location = \App\Models\ProductLocation::pluck('location_name', 'id')->all();


        return view('admin.products.stocksbylocation', compact('page_title', 'page_description', 'location', 'transations', 'location', 'current_location'));
    }

    public function stocks_count()
    {

        $page_title = 'Stock Feeds';
        $page_description = 'counts';

        $transations =
            // ->where('product_stock_moves.stock_id',$id)
            \App\Models\StockMove::leftjoin('products', 'products.id', '=', 'product_stock_moves.stock_id')

            ->select('product_stock_moves.*', 'products.name',  'products.id as pid')
            ->orderBy('product_stock_moves.tran_date', 'DESC')
            ->get()
            ->groupBy('stock_id');

        return view('admin.products.stockscount', compact('page_title', 'page_description', 'transations'));
    }
    public function stock_adjustment()
    {

        $page_title = 'Stock Adjustment';
        $page_description = 'Add stock or remove stocks fr example damaged or others';

        $stockadjustment = \App\Models\StockAdjustment::orderBy('id', 'desc')->get();

        return view('admin.products.adjust.adjust', compact('page_title', 'page_description', 'stockadjustment'));
    }
    public function stock_adjustment_create()
    {


        $page_title = 'Stock Adjustment Create';
        $page_description = 'Add stock or remove stocks fr example damaged or others';

        $location = \App\Models\ProductLocation::pluck('location_name', 'id')->all();

        $account_ledgers = \App\Models\COALedgers::where('group_id', env('COST_OF_GOODS_SOLD'))->pluck('name', 'id')->all();

        $units = \App\Models\ProductsUnit::select('id', 'name', 'symbol')->get();
        $products = \App\Models\Product::where('enabled', '1')->get();

        $reasons  = \App\Models\AdjustmentReason::pluck('name', 'id')->all();


        return view('admin.products.adjust.create', compact('page_title', 'page_description', 'location', 'account_ledgers', 'units', 'products', 'reasons'));
    }
    public function stock_adjustment_store(Request $request)
    {

        $attributes = $request->all();

        $attributes['tax_amount'] = $request->taxable_tax;
        $attributes['total_amount'] = $request->final_total;
        $attributes['date'] = \Carbon\Carbon::now();

        $stock_adjustment = \App\Models\StockAdjustment::create($attributes);

        $product_id = $request->product_id;
        $price = $request->price;
        $quantity = $request->quantity;
        $tax_amount = $request->tax_amount;
        $total = $request->total;
        $units = $request->units;

        foreach ($product_id as $key => $value) {
            if ($value != '') {

                $detail = new \App\Models\StockAdjustmentDetail();
                $detail->adjustment_id = $stock_adjustment->id;
                $detail->product_id = $product_id[$key];
                $detail->price = $price[$key];
                $detail->qty = $quantity[$key];
                $detail->total = $total[$key];
                $detail->unit = $units[$key];
                $detail->save();


                $request_reason = \App\Models\AdjustmentReason::find($request->reason);

                if ($request_reason) {

                    $stockMove = new StockMove();
                    $stockMove->stock_id = $product_id[$key];
                    $stockMove->order_no = $stock_adjustment->id;
                    $stockMove->tran_date = \Carbon\Carbon::now();
                    $stockMove->user_id = \Auth::user()->id;

                    $stockMove->trans_type = $request_reason->trans_type;
                    $stockMove->reference = $request_reason->name . '_' . $stock_adjustment->id;
                    if ($request_reason->reason_type == 'positive') {

                        $stockMove->qty =  $quantity[$key]  * \StockHelper::getUnitPrice($detail->unit);
                    } else {

                        $stockMove->qty = '-' . $quantity[$key]  * \StockHelper::getUnitPrice($detail->unit);
                    }


                    $stockMove->transaction_reference_id = $stock_adjustment->id;
                    $stockMove->location = $request->location_id;

                    $stockMove->price = $price[$key];
                    $stockMove->save();
                }
            }
        }



        $this->updateEntries($stock_adjustment->id);

        Flash::success('Stock Adjustment Done Successfully');

        return redirect('/admin/product/stock_adjustment');
    }

    public function stock_adjustment_edit($id)
    {

        // dd($id);

        $page_title = 'Stock Adjustment Edit';
        $page_description = 'Add stock or remove stocks fr example damaged or others';

        $stock_adjustment = \App\Models\StockAdjustment::find($id);

        $stock_adjustment_details = \App\Models\StockAdjustmentDetail::where('adjustment_id', $stock_adjustment->id)->get();
        //dd($stock_adjustment_details);

        $location = \App\Models\ProductLocation::pluck('location_name', 'id')->all();

        $account_ledgers = \App\Models\COALedgers::where('group_id', env('COST_OF_GOODS_SOLD'))->pluck('name', 'id')->all();

        $units = \App\Models\ProductsUnit::select('id', 'name', 'symbol')->get();
        $products = \App\Models\Product::where('enabled', '1')->get();

        $reasons  = \App\Models\AdjustmentReason::pluck('name', 'id')->all();

        return view('admin.products.adjust.edit', compact('page_title', 'page_description', 'location', 'account_ledgers', 'units', 'products', 'stock_adjustment', 'stock_adjustment_details', 'reasons'));
    }

    public function stock_adjustment_update(Request $request, $id)
    {

        $old_reason_name = \App\Models\StockAdjustment::find($id)->adjustmentreason->name;

        $old_trans_type = \App\Models\StockAdjustment::find($id)->adjustmentreason->trans_type;


        $attributes = $request->all();
        $attributes['tax_amount'] = $request->taxable_tax;
        $attributes['total_amount'] = $request->final_total;

        $stock_adjustment = \App\Models\StockAdjustment::find($id)->update($attributes);

        $purchasedetails = StockAdjustmentDetail::where('adjustment_id', $id)->get();


        foreach ($purchasedetails as $pd) {

            $stockmove =  \App\Models\StockMove::where('stock_id', $pd->product_id)->where('order_no', $id)->where('trans_type', $old_trans_type)->where('reference', $old_reason_name . '_' . $id)->delete();
        }


        \App\Models\StockAdjustmentDetail::where('adjustment_id', $id)->delete();




        $product_id = $request->product_id;
        $price = $request->price;
        $quantity = $request->quantity;
        $tax_amount = $request->tax_amount;
        $total = $request->total;
        $units = $request->units;

        foreach ($product_id as $key => $value) {
            if ($value != '') {

                $detail = new \App\Models\StockAdjustmentDetail();
                $detail->adjustment_id = $id;
                $detail->product_id = $product_id[$key];
                $detail->price = $price[$key];
                $detail->qty = $quantity[$key];
                $detail->total = $total[$key];
                $detail->unit = $units[$key];
                $detail->save();

                $request_reason = \App\Models\AdjustmentReason::find($request->reason);

                if ($request_reason) {

                    $stockMove = new StockMove();
                    $stockMove->stock_id = $product_id[$key];
                    $stockMove->order_no = $id;
                    $stockMove->tran_date = \Carbon\Carbon::now();
                    $stockMove->user_id = \Auth::user()->id;

                    $stockMove->trans_type = $request_reason->trans_type;
                    $stockMove->reference = $request_reason->name . '_' . $id;
                    if ($request_reason->reason_type == 'positive') {

                        $stockMove->qty = $quantity[$key]  * \StockHelper::getUnitPrice($detail->unit) ;
                    } else {

                        $stockMove->qty = '-' .( $quantity[$key] * \StockHelper::getUnitPrice($detail->unit) );
                    }


                    $stockMove->transaction_reference_id = $id;
                    $stockMove->location = $request->location_id;

                    $stockMove->price = $price[$key];
                    $stockMove->save();
                }
            }
        }

        $product_id_new = $request->product_id_new;
        $price_new = $request->price_new;
        $quantity_new = $request->quantity_new;
        $tax_amount_new = $request->tax_amount_new;
        $total_new = $request->total_new;
        $units_new = $request->units_new;

        foreach ($product_id_new as $key => $value) {
            if ($value != '') {

                $detail = new \App\Models\StockAdjustmentDetail();
                $detail->adjustment_id = $id;
                $detail->product_id = $product_id_new[$key];
                $detail->price = $price_new[$key];
                $detail->qty = $quantity_new[$key];
                $detail->total = $total_new[$key];
                $detail->unit = $units_new[$key];
                $detail->save();

                $request_reason = \App\Models\AdjustmentReason::find($request->reason);

                if ($request_reason) {

                    $stockMove = new StockMove();
                    $stockMove->stock_id = $product_id_new[$key];
                    $stockMove->order_no = $id;
                    $stockMove->tran_date = \Carbon\Carbon::now();
                    $stockMove->user_id = \Auth::user()->id;

                    $stockMove->trans_type = $request_reason->trans_type;
                    $stockMove->reference = $request_reason->name . '_' . $id;

                    if ($request_reason->reason_type == 'positive') {
                        $stockMove->qty = $quantity_new[$key]  * \StockHelper::getUnitPrice($detail->unit);
                    } else {
                        $stockMove->qty = '-' . $quantity_new[$key]  * \StockHelper::getUnitPrice($detail->unit);
                    }


                    $stockMove->transaction_reference_id = $id;
                    $stockMove->location = $request->location_id;

                    $stockMove->price = $price[$key];
                    $stockMove->save();
                }
            }
        }


        $this->updateEntries($id);



        Flash::success('Stock Adjustment Updated Successfully');

        return redirect('/admin/product/stock_adjustment');
    }

    public function stock_adjustment_destroy($id)
    {

        //dd($id);

        $old_reason_name = \App\Models\StockAdjustment::find($id)->adjustmentreason->name;

        $old_trans_type = \App\Models\StockAdjustment::find($id)->adjustmentreason->trans_type;

        $stock_adjustment = \App\Models\StockAdjustment::find($id)->delete();

        $purchasedetails = StockAdjustmentDetail::where('adjustment_id', $id)->get();


        foreach ($purchasedetails as $pd) {

            $stockmove =  \App\Models\StockMove::where('stock_id', $pd->product_id)->where('order_no', $id)->where('trans_type', $old_trans_type)->where('reference', $old_reason_name . '_' . $id)->delete();
        }


        \App\Models\StockAdjustmentDetail::where('adjustment_id', $id)->delete();


        Flash::success('Stock Adjustment Destroyed');

        return redirect('/admin/product/stock_adjustment');
    }
    public function stock_adjustment_getModalDelete($id)
    {
        $error = null;



        $modal_title = 'Want to delete Stock Adjustment';

        $stock_adjustment = \App\Models\StockAdjustment::find($id);

        $stock_adjustment_details = \App\Models\StockAdjustmentDetail::where('adjustment_id', $stock_adjustment->id)->get();


        $modal_route = route('admin.products.stock_adjustment.delete', array('id' => $stock_adjustment->id));

        $modal_body = "Are you Sure to Delete This?";

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    private function updateEntries($orderId)
    {


        $purchaseorder = \App\Models\StockAdjustment::find($orderId);

        //dd($purchaseorder);

        $reason = \App\Models\AdjustmentReason::find($purchaseorder->reason);

        if ($purchaseorder->entry_id && $purchaseorder->entry_id != '0') { //update the ledgers
            $attributes['entrytype_id'] = '9'; //Adjustment
            $attributes['tag_id'] = '2'; //Adjustment
            $attributes['user_id'] = \Auth::user()->id;
            $attributes['org_id'] = \Auth::user()->org_id;
            $attributes['number'] =  $purchaseorder->id;
            $attributes['date'] = \Carbon\Carbon::today();
            $attributes['dr_total'] = $purchaseorder->total_amount;
            $attributes['cr_total'] = $purchaseorder->total_amount;
            $attributes['source'] = strtoupper($reason->name);
            $entry = \App\Models\Entry::find($purchaseorder->entry_id);
            $entry->update($attributes);

            // Creddited to Customer or Interest or eq ledger

            if ($reason->reason_type == 'negative') {

                $sub_amount = \App\Models\Entryitem::where('entry_id', $purchaseorder->entry_id)->where('dc', 'C')->first();
                $sub_amount->entry_id = $entry->id;
                $sub_amount->user_id = \Auth::user()->id;
                $sub_amount->org_id = \Auth::user()->org_id;
                $sub_amount->dc = 'C';
                $sub_amount->ledger_id = env('ADJUSTMENT_INVENTORY'); //Inventory ledger
                $sub_amount->amount =  $purchaseorder->total_amount;
                $sub_amount->narration = $reason->name; //$request->user_id
                //dd($sub_amount);
                $sub_amount->update();

                // Debitte to Bank or cash account that we are already in
                $cash_amount = \App\Models\Entryitem::where('entry_id', $purchaseorder->entry_id)->where('dc', 'D')->first();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->user_id = \Auth::user()->id;
                $cash_amount->org_id = \Auth::user()->org_id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id = $purchaseorder->ledgers_id; // Purchase ledger if selected or ledgers from .env
                // dd($cash_amount);
                $cash_amount->amount   =  $purchaseorder->total_amount;
                $cash_amount->narration = $reason->name;
                $cash_amount->update();
            } else {

                $sub_amount = \App\Models\Entryitem::where('entry_id', $purchaseorder->entry_id)->where('dc', 'D')->first();
                $sub_amount->entry_id = $entry->id;
                $sub_amount->user_id = \Auth::user()->id;
                $sub_amount->org_id = \Auth::user()->org_id;
                $sub_amount->dc = 'D';
                $sub_amount->ledger_id = env('ADJUSTMENT_INVENTORY'); //Inventory ledger
                $sub_amount->amount =  $purchaseorder->total_amount;
                $sub_amount->narration = $reason->name; //$request->user_id
                //dd($sub_amount);
                $sub_amount->update();

                // Debitte to Bank or cash account that we are already in
                $cash_amount = \App\Models\Entryitem::where('entry_id', $purchaseorder->entry_id)->where('dc', 'C')->first();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->user_id = \Auth::user()->id;
                $cash_amount->org_id = \Auth::user()->org_id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id =  $purchaseorder->ledgers_id; // Purchase ledger if selected or ledgers from .env
                // dd($cash_amount);
                $cash_amount->amount   =  $purchaseorder->total_amount;
                $cash_amount->narration = $reason->name;
                $cash_amount->update();
            }
        } else {
            //create the new entry items
            $attributes['entrytype_id'] = '9'; //Adjustment
            $attributes['tag_id'] = '2'; //Adjustment
            $attributes['user_id'] = \Auth::user()->id;
            $attributes['org_id'] = \Auth::user()->org_id;
            $attributes['number'] =  $purchaseorder->id;
            $attributes['date'] = \Carbon\Carbon::today();
            $attributes['dr_total'] = $purchaseorder->total_amount;
            $attributes['cr_total'] = $purchaseorder->total_amount;
            $attributes['source'] =  strtoupper($reason->name);
            $entry = \App\Models\Entry::create($attributes);


            if ($reason->reason_type == 'negative') {
                // Creddited to Customer or Interest or eq ledger
                $sub_amount = new \App\Models\Entryitem();
                $sub_amount->entry_id = $entry->id;
                $sub_amount->user_id = \Auth::user()->id;
                $sub_amount->org_id = \Auth::user()->org_id;
                $sub_amount->dc = 'C';
                $sub_amount->ledger_id = env('ADJUSTMENT_INVENTORY'); //Client ledger
                $sub_amount->amount =  $purchaseorder->total_amount;
                $sub_amount->narration = $reason->name; //$request->user_id
                //dd($sub_amount);
                $sub_amount->save();

                // Debitte to Bank or cash account that we are already in

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->user_id = \Auth::user()->id;
                $cash_amount->org_id = \Auth::user()->org_id;
                $cash_amount->dc = 'D';
                $cash_amount->ledger_id =  $purchaseorder->ledgers_id; // Puchase ledger if selected or ledgers from .env
                // dd($cash_amount);
                $cash_amount->amount   =   $purchaseorder->total_amount;
                $cash_amount->narration =  $reason->name;
                $cash_amount->save();
            } else {


                $sub_amount = new \App\Models\Entryitem();
                $sub_amount->entry_id = $entry->id;
                $sub_amount->user_id = \Auth::user()->id;
                $sub_amount->org_id = \Auth::user()->org_id;
                $sub_amount->dc = 'D';
                $sub_amount->ledger_id =  env('ADJUSTMENT_INVENTORY'); //Client ledger
                $sub_amount->amount =  $purchaseorder->total_amount;
                $sub_amount->narration = $reason->name; //$request->user_id
                //dd($sub_amount);
                $sub_amount->save();

                // Debitte to Bank or cash account that we are already in

                $cash_amount = new \App\Models\Entryitem();
                $cash_amount->entry_id = $entry->id;
                $cash_amount->user_id = \Auth::user()->id;
                $cash_amount->org_id = \Auth::user()->org_id;
                $cash_amount->dc = 'C';
                $cash_amount->ledger_id = $purchaseorder->ledgers_id; // Puchase ledger if selected or ledgers from .env
                // dd($cash_amount);
                $cash_amount->amount   =   $purchaseorder->total_amount;
                $cash_amount->narration =  $reason->name;
                $cash_amount->save();
            }

            //now update entry_id in income row
            $purchaseorder->update(['entry_id' => $entry->id]);
        }
        return 0;
    }

    public function ajaxGetMenu(Request $request)
    {

        if ($request->outlet_id != '') {

            $menus = \App\Models\PosMenu::orderBy('id', 'desc')->where('outlet_id', $request->outlet_id)->get();
        } else {

            $menus = \App\Models\PosMenu::orderBy('id', 'desc')->get();
        }

        // dd($menus);

        $data = '<option value="">Select Menu...</option>';

        foreach ($menus as $key => $value) {

            $data .= '<option value="' . $value->id . '">' . $value->menu_name . '</option>';
        }

        return ['success' => 1, 'data' => $data];
    }


    public function transExcel($id,$type){
        $transations = DB::table('product_stock_moves')
            ->where('product_stock_moves.stock_id', $id)
            ->leftjoin('products', 'products.id', '=', 'product_stock_moves.stock_id')
            ->leftjoin('product_location', 'product_location.id', '=', 'product_stock_moves.location')
            ->select('product_stock_moves.*', 'products.name', 'product_location.location_name')
            ->get();

        $product = $this->course->find($id);
        $transData = [];

        $sum = 0;
        $StockIn = 0;
        $StockOut = 0;


        // $tdata [] = [
        //     'Product Name',$product->name
        // ];

        foreach ($transations as $key => $result) {
            if($result->trans_type == PURCHINVOICE){

                $purchtype = 'Purchase';

            }elseif($result->trans_type == SALESINVOICE){

                $purchtype = 'Sale';

            }elseif($result->trans_type == STOCKMOVEIN) {

               $purchtype = 'Transfer';

            }elseif ($result->trans_type == STOCKMOVEOUT) {

               $purchtype = 'Transfer';

            }
            elseif ($result->trans_type == OPENINGSTOCK) {

               $purchtype = 'Opening Stock';

            }

            if($result->qty > 0){
                $StockIn +=$result->qty;
            }else{
                $StockOut +=$result->qty;
            }

            $tdata [] = [

                'TransactionType'=>$purchtype,
                'Date'=>$result->tran_date,
                'Location'=>$result->location_name,
                'Quantity In'=>($result->qty > 0) ? $result->qty : '-',
                'Quantity Out'=>($result->qty < 0) ? str_ireplace('-','',$result->qty) : '-',
                'Quantity On Hand(closing)'=> $sum += $result->qty


            ];


        }


        $tdata [] = [
            'TransactionType'=>'',
            'Date'=>'',
            'Location'=>'Total',
            'Quantity In'=>$StockIn,
            'Quantity Out'=>str_ireplace('-','',$StockOut),
            'Quantity On Hand(closing)'=>$StockIn+$StockOut,
        ];

        $products = [
            'Product Name',$product->name

        ];

        // dd($tdata);

        return \Excel::download(new \App\Exports\ExcelExport($tdata,true,$products), "products_trans.{$type}");

    }


    public function product_statement(Request $request){

        $page_title = 'Product Statement';
        $page_description = 'Search product to find stock ledger statement';

        $products = \App\Models\Product::orderBy('ordernum')->where('enabled', '1')
            ->where('org_id', Auth::user()->org_id)->orderBy('name', 'ASC')
            ->where(function ($q){
                $q->where('parent_product_id',0);
                $q->orWhereNull('parent_product_id');
            })
            ->pluck('name', 'id');

        $current_product = $request->product_id;



        $transations = \App\Models\StockMove::where(function($query) use ($current_product){

            return $query->where('stock_id',$current_product);
        })->orderBy('id')->get();





        // dd('');

        return view('admin.products.statement', compact('transations','page_description','page_title','products','current_product','transations'));

    }

    public function multipledelete(Request $request){

       $ids = $request->chkCourse;
       Audit::log(Auth::user()->id, trans('admin/courses/general.audit-log.category'), trans('admin/courses/general.audit-log.msg-destroy', ['name' => 'Deleted multiple products' ]));
        try{

            $this->course->whereIn('id',$ids)->delete();
            \App\Models\ProductModel::whereIn('product_id', $ids)->delete();
            \App\Models\ProductSerialNumber::whereIn('product_id', $ids)->delete();
            Flash::success(trans('admin/courses/general.status.deleted'));


        }catch(\Exception $e){

            Flash::error("The selected Products Are Related With Invoice");
        }



        return redirect('/admin/products');
    }

}

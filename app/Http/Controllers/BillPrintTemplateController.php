<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\BillPrintTemplate;
use Flash;
use Illuminate\Http\Request;

class BillPrintTemplateController extends Controller
{
    public function __construct(BillPrintTemplate $billprinttemplate)
    {
        $this->billprinttemplate = $billprinttemplate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $page_title = 'Admin | BillPrint |Index';
        $page_description = 'BillPrint Templates';
        $billprinttemplate = $this->billprinttemplate->where('org_id', \Auth::user()->org_id)->orderBy('id', 'desc')->paginate(30);

        return view('admin.billprinttemplate.index', compact('billprinttemplate', 'page_title', 'page_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Admin | Bill Print Templates |Create';
        $page_description = 'Create a BillPrint Templates';
        $outlets = \App\Models\PosOutlets::orderBy('id', 'desc')->get();

        return view('admin.billprinttemplate.create', compact('page_title', 'page_description', 'outlets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $attributes['user_id'] = \Auth::user()->id;
        $attributes['org_id'] = \Auth::user()->org_id;
        Flash::success('Template SucessFully Created');
        $this->billprinttemplate->create($attributes);

        return redirect('/admin/billprinttemplate/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $page_title = 'Admin | Bill Templates | Edit';
        $page_description = 'Edit a Template';
        $billprinttemplate = $this->billprinttemplate->find($id);

        $outlets = \App\Models\PosOutlets::orderBy('id', 'desc')->get();

        return view('admin.billprinttemplate.edit', compact('billprinttemplate', 'page_title', 'page_description', 'outlets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $billprinttemplate = $this->billprinttemplate->find($id);
        $attributes = $request->all();
        if (!$billprinttemplate->isEditable()) {
            abort(404);
        }
        $billprinttemplate->update($attributes);
        Flash::success('Template Updated');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $billprinttemplate = $this->billprinttemplate->find($id);
        if (!$billprinttemplate->isDeletable()) {
            abort(404);
        }
        $billprinttemplate->delete();
        Flash::success('hrlettertemplate deleted');

        return redirect('/admin/billprinttemplate/');
    }

    public function getModalDelete($id)
    {
        $billprinttemplate = $this->billprinttemplate->find($id);
        $modal_title = 'Delete Template';
        $modal_body = 'Are you sure you want to delte billprinttemplate with name ' . $billprinttemplate->name . ' and Id' . $id;
        $modal_route = route('admin.billprinttemplate.delete', $id);

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
}

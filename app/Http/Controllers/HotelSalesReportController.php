<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\OrderDetail;
use App\Models\Article;
use App\Models\Product;
use DB;
class HotelSalesReportController extends Controller
{


	public function getSalesByProductType($typeid,$start_date,$end_date){

		$total = OrderDetail::select(\DB::raw('SUM(fin_order_detail.total) as total'))
				->where('fin_order_detail.folio_id','0')
				->where('products.product_type_id',$typeid)
				->where('fin_orders_meta.is_bill_active','1')
				->whereBetween('fin_orders.bill_date',[$start_date,$end_date])
				->leftjoin('fin_orders','fin_orders.id','=','fin_order_detail.order_id')
				->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
				->leftjoin('products','products.id','=','fin_order_detail.product_id')
				->first();

		return ['total'=>$total->total ?? 0];
	}



	public function salesGroupsByFoodType($typeid,$start_date,$end_date){

		$sales =  OrderDetail::with('product')->select('product_id','total')
					->where('fin_order_detail.folio_id','0')
					->where('products.product_type_id',$typeid)
					->where('fin_orders_meta.is_bill_active','1')
					->whereBetween('fin_orders.bill_date',[$start_date,$end_date])
					->leftjoin('fin_orders','fin_orders.id','=','fin_order_detail.order_id')
					->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
					->leftjoin('products','products.id','=','fin_order_detail.product_id')
					->get();

		return $sales;

	}


	public function productCategorySalesByOutlet($typeid,$start_date,$end_date){

		$allOutlet = \App\Models\PosOutlets::orderBy('id','desc')->get();

		$salesByOulets = [];
		foreach ($allOutlet as $key => $value) {
			
			$sales = OrderDetail::with('product')
				->select('product_id','total')
				->where('fin_order_detail.folio_id','0')
				->where('products.product_type_id',$typeid)
				->where('fin_orders_meta.is_bill_active','1')
				->where('fin_orders.outlet_id',$value->id)
				->whereBetween('fin_orders.bill_date',[$start_date,$end_date])
				->leftjoin('fin_orders','fin_orders.id','=','fin_order_detail.order_id')
				->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
				->leftjoin('products','products.id','=','fin_order_detail.product_id')
				->get();
			$salesByOulets [] = [
				'name'=>$value->name,
				'total'=>$sales->sum('total'),
			];


		}

		return $salesByOulets;



	}


	public function getTaxAndServiceCharge($start_date,$end_date){


		$taxes = Orders::select(DB::raw('SUM(fin_orders.service_charge) as t_serviceCharge'),
						DB::raw('SUM(fin_orders.tax_amount) as t_taxAmount'))
					->whereBetween('fin_orders.bill_date',[$start_date,$end_date])
					->where('fin_orders_meta.is_bill_active','1')
					->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
					->first();

		return $taxes;


	}



	public function getArticlesSales($availableArticles,$start_date,$end_date){

		

		$total = OrderDetail::select('fin_order_detail.total as total',
				'fin_order_detail.product_id')
				->where('fin_order_detail.folio_id','!=','0')
				->whereIn('fin_order_detail.product_id',$availableArticles)
				->where('fin_orders_meta.is_bill_active','1')
				->whereBetween('fin_orders.bill_date',[$start_date,$end_date])
				->leftjoin('fin_orders','fin_orders.id','=','fin_order_detail.order_id')
				->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
				->get();
		return $total;
	}
    
    
	public function salesInfo($availableArticles,$start_date,$end_date){

		$availableArticles = $availableArticles->pluck('id')->toArray();

		$room_sale = Orders::select(\DB::raw('SUM(fin_orders.total_amount) as total'))
					->whereBetween('fin_orders.bill_date',[$start_date,$end_date])
					->where('reservation_id','!=','0')
					->leftjoin('fin_orders_meta','fin_orders_meta.order_id','=','fin_orders.id')
					->first();

		$room_sale = $room_sale->total ?? '0';
		
		

		$foodSales = $this->getSalesByProductType('1',$start_date,$end_date);

		$beverageSales = $this->getSalesByProductType('2',$start_date,$end_date);

		$articlesSales = $this->getArticlesSales($availableArticles,$start_date,$end_date);
		
		$foodsaleByAll = $this->salesGroupsByFoodType('1',$start_date,$end_date);

		$beveragesaleByAll = $this->salesGroupsByFoodType('2',$start_date,$end_date);

		$taxSummary = $this->getTaxAndServiceCharge($start_date,$end_date);


		$outletsSalesFood = $this->productCategorySalesByOutlet('1',$start_date,$end_date);
		
		$outletsSalesBeverage = $this->productCategorySalesByOutlet('2',$start_date,$end_date);

		$salesByCollection = $this->getSalesByPaymethod($start_date,$end_date);

		return ['food_sales'=>$foodSales,'beverage_sales'=>$beverageSales,'room_sale'=>$room_sale,
				'article_sale'=>$articlesSales,'food_sales_all'=>$foodsaleByAll,
				'beverage_sales_all'=>$beveragesaleByAll,'tax_summary'=>$taxSummary,
				'food_sales_by_outlet'=>$outletsSalesFood,
				'beverage_sales_by_outlet'=>$outletsSalesBeverage,
				'sales_by_collection'=>$salesByCollection,
			];


	}

	public function getNepaliDateArr($date){

		$nepali_date = \ReservationHelper::findNepaliDate($date);

		$currentDateArr = [
			'today'=>$date,
			'month_start'=>$nepali_date['month']['start'],
			'month_end'=>$nepali_date['month']['end'],
			'year_start'=>$nepali_date['year']['start'],
			'year_end'=>$nepali_date['year']['end'],
		];

		return $currentDateArr;


	}


	public function index(){

		$start_date = date('Y-m-d');
		$end_date = date('Y-m-d');
		
		if(\Request::get('date'))
		{

			$start_date = 	\Request::get('date');
			$end_date = \Request::get('date');
		}

		$currentDateArr = $this->getNepaliDateArr($start_date);

		$availableArticles = Article::all();
		
		$dataArrCurrent = [
			'today'=>$this->salesInfo($availableArticles,$start_date,$end_date),
			'month'=>$this->salesInfo($availableArticles,$currentDateArr['month_start'],$currentDateArr['month_end']),
			'year'=>$this->salesInfo($availableArticles,$currentDateArr['year_start'],$currentDateArr['year_end']),

		];
		
		$datePrevYear  = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start_date)) . "- 1 year"));

		$previousDateArr = $this->getNepaliDateArr($datePrevYear);

		$dataArrPrevious = [

			'today'=>$this->salesInfo($availableArticles,$previousDateArr['today'],$previousDateArr['today']),

			'month'=>$this->salesInfo($availableArticles,$previousDateArr['month_start'],$previousDateArr['month_end']),

			'year'=>$this->salesInfo($availableArticles,$previousDateArr['year_start'],$previousDateArr['year_end']),

		];


		$foodItems = Product::where('product_type_id','1')->get();
		
		$beverageItems = Product::where('product_type_id','2')->get();
	// /dd($dataArrCurrent);
		//dd($dataArrCurrent);
		$stamp = time();
		
		$pdf = \PDF::loadView('admin.hotel.reservationreports.sales_report',compact('dataArrCurrent','dataArrPrevious','availableArticles','start_date'));

        $file =''.$stamp.'-salesreport.pdf';
        
        if (\File::exists('reports/'.$file))
        {
            \File::Delete('reports/'.$file);
        }
        return $pdf->download($file);
		return view('admin.hotel.reservationreports.sales_report',compact('dataArrCurrent','dataArrPrevious','availableArticles','start_date'));



		


		

	}





}




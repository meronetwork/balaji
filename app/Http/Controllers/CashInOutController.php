<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CashInOutController extends Controller
{
    

	public function cash(){

		if(\Request::get('start_date') && \Request::get('end_date')){

			$start_date = \Request::get('start_date');

			$end_date = \Request::get('end_date');

		}else{

			$start_date = date('Y-m-d');

			$end_date = date('Y-m-d');

		}
		$types = ['customer_payment'=>'Customer Payment', 'customer_advance'=>'Customer Advance', 'sales_without_invoice'=>'Sales Without Invoice', 'other_income'=>'Other Income', 'interest_income'=>'Interest Income'];



		$payment = \App\Models\Payment::where('date','>=',$start_date)
					->where('date','<=',$end_date)
					->get();


		$bankingIncome = \App\Models\BankIncome::where('date_received','>=',$start_date)
					->where('date_received','<=',$end_date)
					->get();

		$expenses = \App\Models\Expense::where('date','>=',$start_date)
					->where('date','<=',$end_date)
					->get();

	//					dd($bankingIncome);

		$page_title = 'Day Book & Cash Flow';

		return view('admin.cashinout.list',compact('payment','bankingIncome','expenses','types','page_title','start_date','end_date'));


	}




}

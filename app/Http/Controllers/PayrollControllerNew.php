<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\EmployeeAward;
use App\Models\EmployeePayroll;
use App\Models\PaymentMethod;
use App\Models\SalaryPayment;
use App\Models\PayrollDetails;
use App\Models\SalaryPaymentAllowance;
use App\Models\payrollDetials;
use App\Models\SalaryPaymentDeduction;
use App\Models\Role as Permission;
use App\User;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER.
 */
class PayrollControllerNew extends Controller
{
    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param Permission $permission
     */
    public function __construct(Permission $permission)
    {
        parent::__construct();
        $this->permission = $permission;
    }

    public function index()
    {
        $page_title = 'Manage Payment';
        $page_description = 'Manage Payment';
        $payroll = \App\Models\Payroll::get();
        return view('admin.payrolllist.index', compact('page_title', 'page_description', 'users','payroll'));
    }


    public function create(Request $request)
    {
    	$page_title = 'Create Payroll Payment';
        $page_description = 'Manage Payment';
        $departments = Department::orderBy('deptname', 'asc')->get();
        if(\Request::get('payment_month')){
        $payroll_check = \App\Models\Payroll::where('departments_id',\Request::get('department_id'))->where('date',\Request::get('payment_month'))->first();
         if(!empty($payroll_check))
                {
                    Flash::warning('Already Created Payroll View Payroll From List Page');
                    return redirect()->route('admin.payrolllist.create');
                }  
        	$users = DB::table('users')
                        ->select('first_name', 'last_name', 'id as user_id')
                        ->where('departments_id', $request->department_id)
                        ->where('enabled', '1')
                        ->whereNotIn('users.id', [1, 2, 3, 28])
                        ->groupBy('users.id')
                        ->get();
        $paymentmonth = explode('-', \Request::get('payment_month'));
        $year = $paymentmonth[0];
        $month   = $paymentmonth[1];                
        $total_days = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
        }


    	return view('admin.payrolllist.create',compact('users','departments','total_days'));
    }


    public function store(Request $request)
    {
    	$attributes['departments_id'] = $request->departments_id;
    	$attributes['created_by'] = auth()->user()->id;
    	$attributes['date'] = $request->date;


    	$payroll = \App\Models\Payroll::create($attributes);
    	$user_ids = $request->user_id;
        $attendance = $request->attendance;
    	if($payroll)
    	{
    		
    		foreach($user_ids as $index=>$user)
    		{
    		  
               $attributesDetials['user_id'] = $user;
               $attributesDetials['payroll_id'] = $payroll->id;
               $attributesDetials['attendance'] = $attendance[$index];
               $attributesDetials['anual_leave'] = $request->anual_leave[$index];
               $attributesDetials['sick_leave'] = $request->sick_leave[$index];
               $attributesDetials['phl'] = $request->phl[$index];
               $attributesDetials['mol_ml_pl'] = $request->mol_ml_pl[$index];
               $attributesDetials['lwp'] = $request->lwp[$index];
               $attributesDetials['absent'] = $request->absent[$index];
               $attributesDetials['late_half_days'] = $request->late_half_days[$index];
               $attributesDetials['total_days'] = $request->total_days[$index];
               $attributesDetials['payable_attendance'] = $request->payable_attendance[$index];
               $attributesDetials['ot_hours'] = $request->ot_hours[$index];
               $attributesDetials['t_basic'] = $request->t_basic[$index];
               $attributesDetials['other_allowance'] = $request->other_allowance[$index];
               $attributesDetials['da'] = $request->da[$index];
               $attributesDetials['total_after_allowance'] = $request->total_after_allowance[$index];
               $attributesDetials['additional_attendance_two_hours'] = $request->additional_attendance_two_hours[$index];
               $attributesDetials['total_salary'] = $request->total_salary[$index];
               $attributesDetials['salary_for_the_month'] = $request->salary_for_the_month[$index];
               $attributesDetials['working_days_basic'] = $request->working_days_basic[$index];
               $attributesDetials['ot_amount'] = $request->ot_amount[$index];
               $attributesDetials['error_adjust'] = $request->error_adjust[$index];
               $attributesDetials['gratuity'] = $request->gratuity[$index];
               $attributesDetials['pf'] = $request->pf[$index];
               $attributesDetials['ctc'] = $request->ctc[$index];
               $attributesDetials['adv'] = $request->adv[$index];
               $attributesDetials['pf_after_ctc'] = $request->pf_after_ctc[$index];
               $attributesDetials['uniform_deduction'] = $request->uniform_deduction[$index];
               $attributesDetials['monthly_payable_amount'] = $request->monthly_payable_amount[$index];
               $attributesDetials['net_salary'] = $request->net_salary[$index];
               $attributesDetials['sst'] = $request->sst[$index];
               $attributesDetials['cit'] = $request->sst[$index];
               $attributesDetials['remarks'] = $request->remarks[$index];
               \App\Models\PayrollDetails::create($attributesDetials);
    		}

    	}
    	  Flash::success('Payroll  Has Been Created');
          return redirect()->route('admin.payrolllist.index');
    }


    public function show($id)
    {
       $page_title = 'Show Payroll Payment';
       $page_description = 'Department Wise Payment';
       $payroll = \App\Models\Payroll::find($id);
       $departments = Department::orderBy('deptname', 'asc')->get();
      
       return view('admin.payrolllist.show',compact('page_title','page_description','payroll','departments','users'));
    }


    public function payrollDownload(Request $request)
    {
       $payroll_id = \Request::get('payroll_id');
       $payroll = \App\Models\Payroll::with('payrollDetails')->find($payroll_id);
       $payrolldetails = $payroll->payrollDetails;
       return \Excel::download(new \App\Exports\PayrollExcelExportFromView($payroll,$payroll->payrollDetails), 'payroll_'.$payroll->date.'.xls');

    }

    
}

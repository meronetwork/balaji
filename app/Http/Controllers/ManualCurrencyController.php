<?php

namespace App\Http\Controllers;

use App\Models\ManualCurrency;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Carbon\Carbon;

class ManualCurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $manualCurrency;
    public function __construct(ManualCurrency $manualCurrency)
    {
        $this->manualCurrency=$manualCurrency;
    }

    public function index()
    {

        $page_title = 'Admin | Manual Currencies';
        $manualCurrencies = $this->manualCurrency->orderBy('date', 'desc')->where(function ($query) {
            if (\Request::get('date'))
                return $query->where('date',\Request::get('date'));
        })->paginate(30);

        return view('admin.manual-currency.index', compact('page_title', 'manualCurrencies','today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Admin| Manual| Currency';
        $page_description = 'Create new job sheet';
        $currencies=\App\Models\Currency::pluck('name','id');

        return view('admin.manual-currency.create', compact('page_title', 'page_description','currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $manualCurrency=$this->manualCurrency->create($request->all());

        Flash::success('Manual Currency SuccessFully Created');
        return redirect()->to('/admin/manual-currency');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Admin|Manual Currency';
        $page_description = 'Create new Manual Currency';
        $manualCurrency = $this->manualCurrency->find($id);
        $currencies=\App\Models\Currency::pluck('name','id');

        return view('admin.manual-currency.edit', compact('page_title', 'page_description','manualCurrency','currencies'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $manualCurrency=$this->manualCurrency->find($id)->update($request->all());



        Flash::success('Manual Currency Update Successfully');
        return redirect()->to('/admin/manual-currency');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $manualCurrency = $this->manualCurrency->find($id);

//        if (! $manualCurrency->isdeletable()) {
//            abort(403);
//        }
        $manualCurrency->delete();


        Flash::success('Manual Currency deleted.');


        return redirect()->back();
    }

    public function deleteModal($id)
    {
        $error = null;

        $manualCurrency = $this->manualCurrency->find($id);

//        if (! $manualCurrency->isdeletable()) {
//            abort(403);
//        }

        $modal_title = 'Delete Manual Currency';

        $manualCurrency = $this->manualCurrency->find($id);
        if (\Request::get('type')) {
            $modal_route = route('admin.manual-currency.delete', $manualCurrency->id).'?type='.\Request::get('type');
        } else {
            $modal_route = route('admin.manual-currency.delete',$manualCurrency->id);
        }

        $modal_body = 'Are you sure you want to delete this Manual Currency ?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }
}

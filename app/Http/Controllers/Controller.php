<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\Attendance;
use App\Models\LeadTransfer;
use App\Models\Organization;
use App\Models\Query;
use App\Models\Audit as Audit;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * @var Audit
     */
    protected $audit;

    /**
     * @var App
     */
    protected $app;

    protected $context;

    protected $context_help_area;

    public function __construct()
    {
        /*$this->audit = $audit;
        $this->global_audits = $this->audit->pushCriteria(new AuditByCreatedDateDescending())->paginate(10);
        View::share('global_audits', $this->global_audits);*/
        //dd(\Carbon\Carbon::now()->addDays(30));

        $this->attendance_log = null;
        $this->org = null;
         $this->announce = null;
       
        if(\Auth::check())
        {
     

    


        $this->attendance_log = Attendance::where('user_id', \Auth::user()->id)->where('clocking_status', '1')->first();

        $this->announce = Announcement::orderBy('announcements_id', 'DESC')->where('org_id',\Auth::user()->org_id)->where('status','published')->first();

  

        $unseen_message = \DB::table('messages')->where('messages.is_seen','!=','1')->where('messages.user_id','!=',\Auth::user()->id)
              ->leftjoin('conversations','messages.conversation_id','=','conversations.id')
              ->where(function($query){
                return  $query->where('conversations.user_one',\Auth::user()->id)->orWhere('conversations.user_two',\Auth::user()->id);
              })->groupBy('messages.conversation_id')
              ->get();
         $this->unseen_message = count($unseen_message);
         \View::share('total_unseen_message', $this->unseen_message);

        }

       // $this->all_projects = Projects::where('enabled', 1)->where('org_id', \Auth::user()->org_id)->lists('name', 'id');
        $this->org = Organization::find($id = 1);

        \View::share('attendance_log', $this->attendance_log);
        \View::share('organization', $this->org);
        \View::share('announce', $this->announce);
    }


    public function getSalesByPaymethod($start_date,$end_date){
      
        $totalSalesCount = \App\Models\Orders::select('fin_orders.*')
                    ->where('fin_orders.bill_date','>=',$start_date)
                    ->where('fin_orders.bill_date','<=',$end_date)
                    ->where(function($query){
                        if(\Request::get('outlet_id')){
                            return $query->where('outlet_id',\Request::get('outlet_id'));
                        }

                    })
                    ->get();

        $totalSales = \App\Models\Orders::with('payments')->select('fin_orders.*','fin_orders_meta.is_bill_active')
            ->where('fin_orders.bill_date','>=',$start_date)
            ->where('fin_orders.bill_date','<=',$end_date)
            ->where(function($query){

                if(\Request::get('outlet_id')){
                    return $query->where('fin_orders.outlet_id',\Request::get('outlet_id'));
                }

            })
            ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
            ->groupBy('fin_orders.id')
            ->get();



        $amountSummary = [
            'totalSalesCount'=>count($totalSalesCount),
            'totalDiscount'=>0,
            'totalSubtotal'=>0,
            'netSalesCount'=>0,
            'service_charge'=>0,
            'taxable_amount'=>0,
            'tax_amount' => 0,
            'total_amount' => 0,
            'coverCounts'=>0
        ];


        $paid_by_total = [
            'cash'=>0,
            'city-ledger'=>0,
            'credit-cards'=>0,
            'check'=>0,
            'travel-agent'=>0,
            'complementry'=>0,
            'staff'=>0,
            'room'=>0,
            'e-sewa'=>0,
            'others'=>0,
            'edm'=>0,
            'mnp'=>0,
        ];

        $totalByPaid =0;

        foreach ($totalSales as $key => $value) {
          
            if($value->is_bill_active == '1'){

                $amountSummary['totalSubtotal'] += (float)$value->subtotal;
                $amountSummary['totalDiscount'] += (float)$value->discount_amount;
                $amountSummary['service_charge'] +=(float) $value->service_charge;
                $amountSummary['taxable_amount'] +=(float) $value->taxable_amount;
                $amountSummary['tax_amount'] += (float)$value->tax_amount;
                $amountSummary['total_amount'] += (float) $value->total_amount;
                $amountSummary['netSalesCount'] ++;
                $amountSummary['coverCounts'] +=  (float) $value->covers;
               
                    $paid_by = $value->payments;
                 
                
                    $paid_by_total['cash'] += $paid_by->where('paid_by','cash')->sum('amount');

                    $paid_by_total['city-ledger'] += $paid_by->where('paid_by','city-ledger')->sum('amount');
                    $paid_by_total['credit-cards'] += $paid_by->where('paid_by','credit-cards')->sum('amount');
                    $paid_by_total['check'] += $paid_by->where('paid_by','check')->sum('amount');
                    $paid_by_total['complementry'] += $paid_by->where('paid_by','complementry')->sum('amount');
                    $paid_by_total['staff'] += $paid_by->where('paid_by','staff')->sum('amount');
                    $paid_by_total['room'] += $paid_by->where('paid_by','room')->sum('amount');
                    $paid_by_total['e-sewa'] += $paid_by->where('paid_by','e-sewa')->sum('amount');
                    $paid_by_total['edm'] += $paid_by->where('paid_by','edm')->sum('amount');
                    $paid_by_total['mnp'] += $paid_by->where('paid_by','mnp')->sum('amount');

                    $otherPayments= array_keys($paid_by_total);

                    $paid_by_total['others'] += $paid_by->whereNotIn('paid_by',$otherPayments)->sum('amount');
                

                    $totalByPaid +=  $value->total_amount;
            }

        }
     
        return ['amountSummary'=>$amountSummary,'paid_by_total'=>$paid_by_total,'totalByPaid'=>$totalByPaid];
    }
}

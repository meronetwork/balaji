<?php

namespace App\Http\Controllers;


use App\Models\Currency;
use Illuminate\Http\Request;
use App\Models\Reservation;
use DB;
use App\Models\FolioPayment;
class FrontCashController extends Controller
{

	public function index(){


		$page_title = 'Front Office Cash';
		$page_description = "All financial transaction from front office staff";
		$dipositHistory = FolioPayment::select('currencies.symbol as currency_symbol','folio_payments.*')->where('is_deposit','1')
						->leftjoin('currencies','folio_payments.currencies','=','currencies.id')
						->orderBy('id','desc')
						->groupBy('folio_payments.id')
						->get();

		return view('admin.front_cash.index',compact('page_title','page_description','dipositHistory'));
	}




	public function create(){

		$rooms = Reservation::select('room_num','id')->whereIn('reservation_status',[2,3])
				->pluck('room_num','id');

		$currencies = Currency::select('symbol as name','id','default_selling')->with(['latestManualCurrency'=>function($q){
            $q->whereDate('date',date('Y-m-d'))->latest();
        }])->get();

		return view('admin.front_cash.create',compact('rooms','currencies'));

	}



	public function getGuest($resid){

		$res = Reservation::with('guest')->find($resid);

		if($res->guest){

			$guest = $res->guest->first_name . $res->guest->last_name;

		}else{

			$guest = $res->guest_name;
		}



		return ['guest'=>$guest];


	}


}

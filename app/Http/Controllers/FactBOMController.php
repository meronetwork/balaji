<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Audit as Audit;
use App\Models\Client;
use App\Models\Contact;
use App\Models\FactBOM;
use App\Models\FactBOMDetail;
use App\Models\FactRouting;
use App\Models\FactRoutingDetail;
use App\Models\Lead;
use App\Models\MasterComments;
use App\Models\OrderDetail;
use App\Models\Orders;
use App\Models\Orders as Order;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use App\Models\Role as Permission;
use App\Models\Sauda;
use App\Models\SaudaDetails;
use App\Models\StockMove;
use App\User;
use Auth;
use DB;
use Flash;
use Illuminate\Http\Request;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER.
 */
class FactBOMController extends Controller
{
    /**
     * @var Client
     */
    private $fact_bom;

    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param Client $bug
     * @param Permission $permission
     * @param User $user
     */
    public function __construct(Permission $permission, FactBOM $fact_bom)
    {
        parent::__construct();
        $this->permission = $permission;
        $this->fact_bom = $fact_bom;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $filterdate = function ($query) {

            $start_date = \Request::get('start_date');

            $end_date = \Request::get('end_date');

            if ($start_date && $end_date) {
                return $query->where('bill_date', '>=', $start_date)
                    ->where('bill_date', '<=', $end_date);
            }

        };

        $filterSupplier = function ($query) {

            $supplier = \Request::get('supplier_id');
            if ($supplier) {

                return $query->where('supplier_id', $supplier);
            }

        };

        $filterCurrency = function ($query) {

            $currency = \Request::get('currency');

            if ($currency) {

                return $query->where('currency', $currency);
            }

        };

        $orders = FactBOM::select('fact_bom.id as id', 'fact_bom.bom_name', 'fact_bom.created_at as created_at', 'products.name as product_name',)
            ->leftjoin('products', 'fact_bom.product_id', '=', 'products.id')
            ->orderBy('id', 'DESC');


        if (\Request::get('search') && \Request::get('search') == 'true') {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate(40);
        }

        $page_title = ' Recipe BOM';
        $page_description = 'Manage Production bill of materials';
        $suppliers = Client::where('relation_type', 'supplier')->pluck('name', 'id')->all();
        $currency = \App\Models\Country::whereEnabled('1')->pluck('currency_name', 'currency_code as id')->all();


        return view('admin.fact_bom.index', compact('orders', 'currency', 'page_title', 'page_description', 'suppliers'));
    }


    /**
     * @return \Illuminate\View\View
     */
    public function show(Request  $request , $id)
    {
        $ord = $this->fact_bom->find($id);
        $page_title = ' Recipe BOM';
        $page_description = 'Manage Production bill of materials';
        $orderDetails = FactBOMDetail::select( 'fact_bom_details.id', 'fact_bom_details.raw_prod_id', 'fact_bom_details.qty','product_units.symbol')
            ->where('bom_id', $ord->id)
            ->leftjoin('products','fact_bom_details.raw_prod_id','=','products.id')
            ->leftjoin('product_units','products.product_unit','=','product_units.id')
            ->get();
//        dd($orderDetails);


        $imagepath = \Auth::user()->organization->logo;
        // dd($imagepath);
        if($request->is('admin/production/fact_bom/print/'.$id)){
            return view('admin.fact_bom.print',compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
        }

        return view('admin.fact_bom.show', compact('ord', 'imagepath', 'page_title', 'page_description', 'orderDetails'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $page_title = ' Recipe BOM';
        $page_description = 'Manage Production bill of materials';
        $order = null;
        $orderDetail = null;
        $final_products = \App\Models\Product::select('id', 'name')->where('assembled_product', 1)->get();

        $products = \App\Models\Product::where('component_product', 1)->get();

        $routing = \App\Models\FactRouting::pluck('name','id');



        $users = \App\User::where('enabled', '1')->where('org_id', \Auth::user()->org_id)->pluck('first_name', 'id');

        $section = \App\Models\ProductsSection::pluck('section_name', 'id');

        return view('admin.fact_bom.create', compact('page_title', 'section', 'final_products', 'users', 'order_count', 'page_description', 'products','routing'));
    }


    public function store(Request $request)
    {
        $attributes = $request->all();
        $fact_bom = FactBOM::create($attributes);

        //  Request Detail Create
        $fact_bom_id = $fact_bom->id;
        $product_id = $request->rproduct_id;
        $quantity = $request->rquantity;


        foreach ($product_id as $key => $value) {
            if ($value != '') {
                $detail = FactBOMDetail::create([
                    "bom_id" => $fact_bom_id,
                    "raw_prod_id" => $value,
                    "qty" => $quantity[$key],
                ]);
            }
        }
        Flash::success('Production BOM created Successfully.');
        return redirect('/admin/production/fact_bom');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $page_title = ' Factory BOM';
        $page_description = 'Manage Production bill of materials';
        $order = FactBOM::where('id', $id)->first();
        $orderDetails = FactBOMDetail::where('bom_id', $id)->get();

        $final_products = \App\Models\Product::select('id', 'name')->where('assembled_product', 1)->get();
     
        $products = \App\Models\Product::where('component_product', 1)->get();
        $users = \App\User::where('enabled', '1')->where('org_id', \Auth::user()->org_id)->pluck('first_name', 'id');

        $section = \App\Models\ProductsSection::pluck('section_name', 'id');
         $routing = \App\Models\FactRouting::pluck('name','id');
     
        return view('admin.fact_bom.edit', compact('page_title', 'section', 'final_products', 'users', 'order_count', 'page_description', 'products', 'order', 'orderDetails','routing'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {


        $fact_bom = $this->fact_bom->find($id);

        $fact_bom->update($request->all());
        $orderDetails = FactBOMDetail::where('bom_id', $id)->delete();

        //  Request Detail Create
        $fact_bom_id = $fact_bom->id;
        $product_id = $request->rproduct_id;
        $quantity = $request->rquantity;


//        dd($request->all());
        //dd($order);
        if ($fact_bom->isEditable()) {
            foreach ($product_id as $key => $value) {
                if ($value != '') {
                    $detail = FactBOMDetail::create([
                        "bom_id" => $fact_bom_id,
                        "raw_prod_id" => $value,
                        "qty" => $quantity[$key],
                    ]);
                }
            }
            Flash::success('Production Routing Updated Successfully.');
            return redirect('/admin/production/fact_bom');
        } else {
            Flash::success('Error in updating Production Routing.');
        }

        return \Redirect::back()->withInput(\Request::all());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $fact_bom = $this->fact_bom->find($id);

        if (!$fact_bom->isdeletable()) {
            abort(403);
        }

        $fact_bom->delete();
        FactRoutingDetail::where('routing_id', $id)->delete();

        Flash::success('Factory Routing successfully deleted.');
        return redirect('/admin/production/fact_bom');
    }


    /**
     * Delete Confirm.
     *
     * @param int $id
     * @return  View
     */
    public function deleteModal($id)
    {
        $error = null;

        $modal_title = 'Delete Factory BOM';

        $fact_bom = $this->fact_bom->find($id);

        $modal_body = 'Are you sure you want to delete this Factory BOM?';
        $modal_route = route('admin.production.fact_bom.delete',  $fact_bom->id
        );
        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }


    public function printInvoice($id)
    {
        $ord = $this->fact_bom->find($id);
        $orderDetails = PurchaseOrderDetail::where('order_no', $id)->get();
        //dd($orderDetails);
        $imagepath = \Auth::user()->organization->logo;

        return view('admin.fact_bom.print', compact('ord', 'imagepath', 'orderDetails'));
    }

    public function generatePDF($id)
    {
        $ord = $this->fact_bom->find($id);
        $orderDetails = PurchaseOrderDetail::where('order_no', $id)->get();
        $imagepath = \Auth::user()->organization->logo;

        $pdf = \PDF::loadView('admin.fact_bom.generateInvoicePDF', compact('ord', 'imagepath', 'orderDetails'));
        $file = $id . '_' . str_replace(' ', '_', $ord->client->name) . '.pdf';

        if (\File::exists('reports/' . $file)) {
            \File::Delete('reports/' . $file);
        }

        return $pdf->download($file);
    }




}

<?php namespace App\Http\Controllers;

use App\Repositories\ContactRepository as Contact;
use App\Repositories\RoleRepository as Permission;
use App\Repositories\AuditRepository as Audit;
use Flash;
use DB;
use Auth;
use App\User;
use Nahid\Talk\Facades\Talk;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;
use View;

/**
 * THIS CONTROLLER IS USED AS PRODUCT CONTROLLER
 */

class TalkController extends Controller
{


    /**
     * @var Permission
     */
    private $permission;

    /**
     * @param contact $contact
     * @param Permission $permission
     * @param User $user
     */
  //   public function __construct( Permission $permission)
  //   {
        // parent::__construct();
  //       $this->permission = $permission;
       

  //         Talk::setAuthUserId(Auth::user()->id);

  //       View::composer('partials.peoplelist', function($view) {
  //           $threads = Talk::threads();
  //           $view->with(compact('threads'));
  //       });
  //   }

    protected $authUser;

     public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            Talk::setAuthUserId(Auth::user()->id);

            return $next($request);
        });
    }
    
    /**
     * @return \Illuminate\View\View
     */
   private function getuserProfilePic($user)
    {
        $profile = $user->image;
        if ($profile) {
            $profile = url('/').'/images/profiles/'.$profile;
        } else {
            $profile = url('/').'/images/logo.png';
        }

        return $profile;
    }
    public function index()
    {
        $page_title = 'Admin | Chat';
        $unseen_message = \DB::table('messages')->where('messages.is_seen','0')->where('messages.user_id','!=',\Auth::user()->id)
              ->leftjoin('conversations','messages.conversation_id','=','conversations.id')
              ->where('conversations.user_one',\Auth::user()->id)->orWhere('conversations.user_two',\Auth::user()->id)
              ->groupBy('messages.conversation_id')
              ->get();

        $users = User::where('enabled','1')->get();

        $total_unseen_message= count($unseen_message);
        $threads = Talk::threads();
        $user_id = null;

        foreach ($threads as $key => $inbox) {
          if(!is_null($inbox->thread)){
            $user_id =$inbox->withUser->id; //getting first user in chat list
            break;
          }
        }

        $messages = $this->getUserMessage($user_id); 
   
        $totalMessage = count($messages);
        $messages = $messages->slice(-20); //get latest 20 messages
        $receiver = $user_id; //message receiver

        $rcv_user = User::find($user_id);  
        $rcv_profile_img = $this->getuserProfilePic($rcv_user);
        $sender_profile_img = $this->getuserProfilePic(Auth::user());
        //dd($sender_profile_img);
        if (count($messages) > 0) {
            $messages = $messages->sortBy('id');
            $this->ajaxSeenMessage($messages[0]->conversation_id);
        }

        return view('admin.talk.chathistory', compact('messages','rcv_user','user_id','totalMessage','threads','users','receiver','sender_profile_img','rcv_profile_img','page_title'));
    }


  
    private function getUserMessage($id){
      $messages =  collect();
      $conversations = Talk::getMessagesByUserId($id,0,1000000000000000); //dont know why always first getting 20 messages so....
      // $user = $conversations->withUser;
      if($conversations)
        $messages = $conversations->messages;
      return $messages;
    }

    public function chatHistory($id)
    {
        $page_title = 'Admin | Chat';
        $messages = $this->getUserMessage($id); 
          // dd($messages);
        $totalMessage = count($messages);
        //dd($totalMessage);
        $messages = $messages->slice(-20); //get latest 20 messages
        $user = User::find($id);  
        $receiver = $id;
        if (count($messages) > 0) {
            $messages = $messages->sortBy('id');
            $this->ajaxSeenMessage($messages[0]->conversation_id);
        }
        $rcv_profile_img = $this->getuserProfilePic($user);
        $sender_profile_img = $this->getuserProfilePic(Auth::user());
        $threads = Talk::threads();
        $users = User::where('enabled','1')->get();
        $rcv_user = User::find($id);

        // return $threads;
        return view('admin.talk.chathistory', compact('messages','rcv_user','user_id','totalMessage','threads','users','receiver','sender_profile_img','rcv_profile_img','page_title'));
    }
   public function ajaxSyncMessage($id){
        $messages = $this->getUserMessage($id);
        $messages = $messages->slice(-20); //get latest 20 messages
        return ['message'=>$messages];
    }

    public function moreMessage($id,$start,$end){
        $messages = $this->getUserMessage($id); 
        $messages =  $messages->slice($start,$end);
        return response()->json(['status'=>'success', 'data'=>$messages], 200);
    }

  
    public function ajaxSendMessage(Request $request)
    {
      //dd("DOEN");
      $pusher = new \Pusher\Pusher("4df5c0b0bf2e03e7c74c", "cee364ffe5af925ad97d", "1007457", array('cluster' => 'ap2'));
      $rules = [
          'message-data'=>'required',
          '_id'=>'required'
      ];
      $attachment = [
        'attachment'=>$request->attachment,
        'att_type'=>$request->att_type,
        'file_name'=>$request->file_name,
        'extension'=>$request->extension,
      ];
//dd($attachment);
      $this->validate($request, $rules);
      $body = $request->input('message-data');
      $userId = $request->input('_id');
      if ($message = Talk::sendMessageByUserId($userId, $body,$attachment)) {
          $pusher->trigger('meronetwork-chat-channel', 'chat-user-'.$userId.'-'.\Auth::user()->id, array('message' => $message));
          return ['sucess'=>true,'id'=>$message->id];
      }
        
    }
    public function ajaxDeleteMessage(Request $request, $id)
    {
        if ($request->ajax()) {
            if(Talk::deleteMessage($id)) {
              return response()->json(['status'=>'success','check'=>$id], 200);
              

            }
            return response()->json(['status'=>'errors', 'msg'=>'something went wrong'], 401);
        }
    }

    public function ajaxSeenMessage($cid){
      $user=DB::table('messages')->where('conversation_id',$cid)->orderBy('created_at', 'desc')->first();
      $checked_user=$user->user_id;  
      if($checked_user != Auth::user()->id){
        DB::table('messages')->where('user_id',$checked_user)->where('conversation_id',$cid)->update(['is_seen'=>true]);
      }
      return 0;
    }
    
    public function getPopoverMessage(){
      $threads = Talk::threads();
      $threads = $threads->slice(0,20);
      $html = view('ajax.popovermessage', compact('threads'))->render();
      return ['message'=>$html];
    }

    public function typing($receiver_id){
      $pusher = new \Pusher\Pusher("4df5c0b0bf2e03e7c74c", "cee364ffe5af925ad97d", "1007457", array('cluster' => 'ap2'));
      $pusher->trigger('meronetwork-chat-channel', 'chat-user-typing-'.$receiver_id.'-'.\Auth::user()->id, array('message' => $message));
      return ['success'=>true];
    }

      public function tests()
    {
       $b = new Broadcast(\Illuminate\Contracts\Config\Repository::class, \Vinkla\Pusher\PusherFactory::class);
      dd($b->tests());
    }

}

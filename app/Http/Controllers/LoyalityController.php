<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loyality;
class LoyalityController extends Controller
{
    public function __construct(Loyality $loyality){

    	$this->loyality = $loyality;
    }

    public function index(){
    	$page_title = 'Admin | Loyality';
    	$loyality = $this->loyality->orderBy('created_at','desc')->paginate(30);

    	return view('admin.hotel.loyality',compact('loyality','page_title'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use App\Models\RecipeStep;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class RecipeStepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $step;
    public function __construct(RecipeStep $step)
    {
        $this->step=$step;
    }

    public function index()
    {
        $page_title = "Admin | Recipe Step";
        $isindex = true;
        $steps = $this->step->orderBy('id','desc')
            ->paginate(30);
        return view('admin.recipeSteps.index',compact('steps','isindex','page_title'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = trans('admin/cases/general.page.create.title'); // "Admin | Client | Create";
        $page_description = trans('admin/cases/general.page.create.description'); // "Creating a new client";
//        $recipes = Recipe::all('name');
        $recipes = Recipe::pluck('name','id');
        return view('admin.recipeSteps.create', compact(  'page_title', 'page_description','recipes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'recipe_id' => 'required',
            'step_num' => 'required',
            'instructions' => 'required',
        ]);
        $attributes = $request->all();
        $this->step->create($attributes);
        Flash::success('Step created Successfully.');

        return redirect('/admin/recipe-step');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Admin | Recipe Step | Create";

        $page_description = "Update Recipe";

        $step=$this->step->find($id);
        $recipes = Recipe::pluck('name','id');

        return view('admin.recipeSteps.edit', compact(  'page_title', 'page_description','step','recipes'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'step_num' => 'required',
            'instructions' => 'required',
            'recipe_id' => 'required',
        ]);
        $attributes = $request->all();
        $this->step->find($id)->update($attributes);
        Flash::success('Recipe Step Updated Successfully.');

        return redirect('/admin/recipe-step');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $step = $this->step->find($id);
//        \TaskHelper::authorizeOrg($supportJob);
        if (! $step->isdeletable()) {
            abort(403);
        }

        $step->delete();

//        MasterComments::where('type', 'orders')->where('master_id', $id)->delete();

        Flash::success('Recipe Step deleted.');

//        if (\Request::get('type')) {
//            return redirect('/admin/orders?type='.\Request::get('type'));
//        }

        return redirect('/admin/recipe-step');
    }

    public function deleteModal($id)
    {
        $error = null;

        $step = $this->step->find($id);

        if (! $step->isdeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Recipe Step';

        $step = $this->step->find($id);
        if (\Request::get('type')) {
            $modal_route = route('admin.recipe-step.delete', ['id' => $step->id]).'?type='.\Request::get('type');
        } else {
            $modal_route = route('admin.recipe-step.delete', ['id' => $step->id]);
        }

        $modal_body = 'Are you sure you want to delete this Recipe Step?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function Search(Request $request){
        $key = $request->search;
        $page_title = "Admin | show";
//        $recipe = Recipe::select('recipes.id','recipes.name','recipes.description','fact_batches.product_id')->leftjoin('products','fact_batches.product_id','=','products.id')->where('fact_batches.id','LIKE','%'.$key.'%')->orWhere('products.name','LIKE','%'.$key.'%')->orWhere('fact_batches.max_qty','LIKE','%'.$key.'%')->orWhere('fact_batches.batch_no','LIKE','%'.$key.'%')->orderBy('fact_batches.id','desc')->paginate(30);
        $recipes = Recipe::select('recipes.id','recipes.name','recipes.description')->where('recipes.id','LIKE','%'.$key.'%')->orWhere('recipes.name','LIKE','%'.$key.'%')->orWhere('recipes.description','LIKE','%'.$key.'%')->orderBy('recipes.id','desc')->paginate(30);
        return view('admin.factoryBatch.index',compact('factoryBatches','page_title'));
    }
}

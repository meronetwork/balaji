<?php

namespace App\Http\Controllers;

use App\Helpers\TaskHelper;
use App\Models\Announcement;
use App\Models\Contact;
use App\Models\Lead;
use App\Models\ProjectTask;
use App\Models\Audit as Audit;
use App\Models\Task;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Reservation;
use App\Helpers\ReservationHelper;
class HomeController extends Controller
{
    /**
     * @param Application $app
     * @param Audit $audit
     */
    public function __construct(Application $app, Audit $audit)
    {
        parent::__construct($app, $audit);
        // Set default crumbtrail for controller.
        session(['crumbtrail.leaf' => 'home']);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    // public function index(Request $request)
    // {
    //     $homeRouteName = 'welcome';

    //     try {
    //         $homeCandidateName =\Config::get('settings.app_home_route');
    //         $homeRouteName = $homeCandidateName;
    //     }
    //     catch (Exception $ex) { } // Eat the exception will default to the welcome route.

    //     $request->session()->reflash();
    //     return Redirect::route($homeRouteName);
    // }

    public function index()
    {

        if (!\Auth::check()) {
            return \Redirect::to('/login')->with('message', 'Login to Start');
        }


        $page_title = "Home";
        $page_description = "Welcome to <strong>" . TaskHelper::GetOrgName(\Auth::user()->org_id)->organization_name . "</strong> Smalloffice";
        $annoucements = Announcement::where('org_id', \Auth::user()->org_id)->orderBy('created_at', 'desc')->take(7)->get();
        $p_tasks = ProjectTask::orderBy('created_at', 'desc')->take(5)->get();
        $leads = Lead::orderBy('created_at', 'desc')->where('org_id', \Auth::user()->org_id)->where('lead_type_id', '2')->take(5)->get();
        $online_leads = Lead::where('org_id', \Auth::user()->org_id)->orderBy('created_at', 'desc')->where('lead_type_id', '1')->take(5)->get();
        $potential = Lead::where('org_id', \Auth::user()->org_id)->orderBy('created_at', 'desc')->where('lead_type_id', '4')->take(5)->get();
        $contacts = Contact::where('org_id', \Auth::user()->org_id)->orderBy('created_at', 'desc')->take(5)->get();
        if (\Auth::user()->hasRole('admins')) {
            $projecttasks = ProjectTask::orderby('id', 'desc')->take(5)->get();
        } else {
            $projecttasks = ProjectTask::orderBy('id', 'desc')->where('peoples', \Auth::user()->username)->orwhere('user_id', \Auth::user()->id)->take(5)->get();
        }
        if (\Auth::user()->hasRole('admins')) {
            $notcompleted = ProjectTask::orderby('id', 'desc')->where('end_date', '<', \Carbon\Carbon::now())->where('status', 'new')->orWhere('status', 'ongoing')->take(5)->get();
        } else {
            $notcompleted = ProjectTask::orderBy('id', 'desc')->where('peoples', \Auth::user()->username)->orwhere('user_id', \Auth::user()->id)->where('end_date', '<', \Carbon\Carbon::now())->where('status', 'new')->orWhere('status', 'ongoing')->take(5)->get();
        }
        if (\Auth::user()->hasRole('admins')) {
            $upcommingtasks = ProjectTask::orderBy(DB::raw('ABS(DATEDIFF(project_tasks.end_date, NOW()))'))->take(5)->get();
            // dd($upcommingtasks);
        } else {
            $upcommingtasks = ProjectTask::orderBy(DB::raw('ABS(DATEDIFF(project_tasks.end_date, NOW()))'))->where('peoples', \Auth::user()->username)->orwhere('user_id', \Auth::user()->id)->take(5)->get();
        }
//        $follow_ups = DB::table('lead_notes')
//            ->selectRaw('users.first_name as name, leads.name as lead, lead_notes.lead_id, lead_notes.note, lead_notes.updated_at')
//            ->join('users', 'users.id', '=', 'lead_notes.user_id')
//            ->join('leads', 'lead_notes.lead_id', '=', 'leads.id')
//            ->where('lead_notes.updated_at', '>=', \Carbon\Carbon::now()->subDays(7))
//            ->where('lead_notes.updated_at', '<=', \Carbon\Carbon::now()->endOfWeek())
//            ->orderBy('lead_notes.id', 'desc')
//            ->limit(5)
//            ->get();
        $my_remaining = \App\Models\Task::whereIn('task_assign_to', [\Auth::user()->id])
            //->where('enabled', '1')
            ->where('task_status', '!=', 'Completed')
            ->where('org_id', \Auth::user()->org_id)
            // ->whereBetween('task_due_date', array(\Carbon\Carbon::today(), \Carbon\Carbon::now()->subDays(60)))
            ->orderBy('id', 'DESC')
            ->take(5)->get();
        //  dd($my_remaining);
        $all_status = \App\Models\Leadstatus::where('enabled', '1')->orderby('ordernum')->get();
        $all_stages = \App\Models\Stage::where('enabled', '1')->orderby('ordernum')->get();





        $hotel_arrival = ReservationHelper::hotel_arrival()->get();

//        $hotel_departure  =ReservationHelper::hotel_departure()->get();
        $room_occupied  =  ReservationHelper::room_occupied()->count();




        $due_out = ReservationHelper::due_out()->count();

        $stay_over = ReservationHelper::stay_over()->count();

        $arrival_confirm = ReservationHelper::arrival_confirm()->count();

        $arrival_unconfirm =ReservationHelper::arrival_unconfirm()->count();

        $arrived =ReservationHelper::arrived()->count();

        $walk_in =ReservationHelper::walk_in()->count();

        $due_in =ReservationHelper::due_in()->count();

        //dd($due_in);

        $check_out = ReservationHelper::check_out()->count();

        $day_use = ReservationHelper::day_use()->count();

        $dirty = \App\Models\Room::where('house_status_id', '1')->count();
        $clean = \App\Models\Room::where('house_status_id', '2')->count();
        $pickup = \App\Models\Room::where('house_status_id', '3')->count();
        $inspected = \App\Models\Room::where('house_status_id', '4')->count();
        $today = \Carbon\Carbon::today();
        $week = \Carbon\Carbon::today()->addDays(7);


        $attendance_log = \App\Models\ShiftAttendance::where('user_id', \Auth::user()->id)->where('date', date('Y-m-d'))->orderBy('attendance_status', 'desc')->first();

        if (!$attendance_log) {
            //check for next day night shift
            $check_night_shift = \App\Models\ShiftAttendance::where('user_id', \Auth::user()->id)
                ->orderBy('date', 'desc')
                ->first();

            if ($check_night_shift && $check_night_shift->shift->is_night) {
                $previous_day = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
                $attendance_log = \App\Models\ShiftAttendance::where('user_id', \Auth::user()->id)->where('date', $previous_day)->orderBy('attendance_status', 'desc')->first();
            }
        }


        $hk_task=Task::latest()->paginate(20);

        return view('home', compact('hk_task','page_title', 'page_description', 'all_status', 'all_stages', 'upcommingtasks', 'notcompleted', 'projecttasks', 'annoucements', 'p_tasks', 'leads', 'online_leads', 'potential', 'contacts', 'follow_ups', 'my_remaining', 'hotel_arrival', 'hotel_departure', 'room_occupied', 'due_out', 'stay_over', 'arrival_confirm', 'arrival_unconfirm', 'arrived', 'walk_in', 'due_in', 'check_out', 'day_use', 'dirty', 'clean', 'pickup', 'inspected', 'attendance_log'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function welcome(Request $request)
    {
        $page_title = trans('general.text.welcome');
        $page_description = 'This is the welcome page';

        $request->session()->reflash();

        return view('welcome', compact('page_title', 'page_description'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notViewedCases(Request $request)
    {
        $page_title = 'Not Viewed Cases';
        $page_description = 'This is the Lists of Not Viewed Cases';

        $cases = \App\Models\Cases::where('status', '!=', 'closed')
            ->where('org_id', Auth::user()->org_id)
            ->where('enabled', '1')
            ->orderBy('id', 'desc')->paginate(20);

        // dd($cases);

        return view('notviewedcases', compact('page_title', 'page_description', 'cases'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function NotViewedLeads(Request $request)
    {
        $page_title = 'Not Viewed Cases';
        $page_description = 'This is the Lists of Not Viewed Cases';

        $leads = \App\Models\Lead::where('viewed', '0')
            ->where('rating', 'active')
            ->where('enabled', '1')
            ->where('org_id', Auth::user()->org_id)
            ->orderBy('id', 'desc')->paginate(20);

        $stages = \App\Models\Stage::where('enabled', '1')->orderby('ordernum')->pluck('name', 'id');
        $lead_status = \App\Models\Leadstatus::where('enabled', '1')->pluck('name', 'id')->all();

        return view('notviewedleads', compact('page_title', 'page_description', 'leads', 'stages', 'lead_status'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function NotViewedMarketingTasks(Request $request)
    {
        $page_title = 'Not Viewed Marketing Tasks';
        $page_description = 'This is the Lists of Not Marketing Tasks';

        $tasks = \App\Models\Task
            ::whereIn('task_assign_to', [\Auth::user()->id])
            ->where('enabled', '1')
            ->where('task_status', '!=', 'Completed')
            ->whereBetween('task_due_date', [\Carbon\Carbon::yesterday(), \Carbon\Carbon::now()->addDays(30)])
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return view('notviewedmarketingtasks', compact('page_title', 'page_description', 'tasks'));
    }
}

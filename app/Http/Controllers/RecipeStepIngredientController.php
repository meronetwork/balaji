<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Recipe;
use App\Models\RecipeStepIngredient;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class RecipeStepIngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $stepIngredient;
    public function __construct(RecipeStepIngredient $stepIngredient)
    {
        $this->stepIngredient=$stepIngredient;
    }

    public function index()
    {
        $page_title = "Admin | Recipe Step Ingredient";
        $isindex = true;
        $stepIngredients = $this->stepIngredient->orderBy('id','desc')
            ->paginate(30);
        return view('admin.recipeStepIngredient.index',compact('steps','isindex','page_title','stepIngredients'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Admin | Recipe Step Ingredient | Create";
        $page_description = "Creating a Recipe Step Ingredient";
//        $recipes = Recipe::all('name');
        $recipes = Recipe::pluck('name','id');
        $ingredients = Product::pluck('name','id');
        return view('admin.recipeStepIngredient.create', compact(  'page_title', 'page_description','recipes','ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'recipe_id' => 'required',
            'ingredient_id' => 'required',
            'sepe_num' => 'required',
            'amount_required' => 'required',
        ]);
        $attributes = $request->all();
        $this->stepIngredient->create($attributes);
        Flash::success('Step ingredient created Successfully.');

        return redirect('/admin/recipe-step-ingredient');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Admin | Recipe Step ingredient | Create";

        $page_description = "Update Recipe ingredient";

        $stepIngredient=$this->stepIngredient->find($id);
        $recipes = Recipe::pluck('name','id');
        $ingredients = Product::pluck('name','id');
        return view('admin.recipeStepIngredient.edit', compact(  'page_title', 'page_description','stepIngredient','recipes','ingredients'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'recipe_id' => 'required',
            'ingredient_id' => 'required',
            'sepe_num' => 'required',
            'amount_required' => 'required',
        ]);
        $attributes = $request->all();
        $this->stepIngredient->find($id)->update($attributes);
        Flash::success('Recipe Step Ingredient Updated Successfully.');

        return redirect('/admin/recipe-step-ingredient');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stepIngredient = $this->stepIngredient->find($id);
//        \TaskHelper::authorizeOrg($supportJob);
        if (!$stepIngredient->isDeletable()) {
            abort(403);
        }

        $stepIngredient->delete();

//        MasterComments::where('type', 'orders')->where('master_id', $id)->delete();

        Flash::success('Recipe Step Ingredient deleted.');

//        if (\Request::get('type')) {
//            return redirect('/admin/orders?type='.\Request::get('type'));
//        }

        return redirect('/admin/recipe-step-ingredient');
    }

    public function deleteModal($id)
    {
        $error = null;

        $stepIngredient = $this->stepIngredient->find($id);

        if (! $stepIngredient->isDeletable()) {
            abort(403);
        }

        $modal_title = 'Delete Recipe Step Ingredient';

        $stepIngredient = $this->stepIngredient->find($id);
        if (\Request::get('type')) {
            $modal_route = route('admin.recipe-step-ingredient.delete', ['id' => $stepIngredient->id]).'?type='.\Request::get('type');
        } else {
            $modal_route = route('admin.recipe-step-ingredient.delete', ['id' => $stepIngredient->id]);
        }

        $modal_body = 'Are you sure you want to delete this Recipe Step Ingredient?';

        return view('modal_confirmation', compact('error', 'modal_route', 'modal_title', 'modal_body'));
    }

    public function Search(Request $request){
        $key = $request->search;
        $page_title = "Admin | show";
//        $recipe = Recipe::select('recipes.id','recipes.name','recipes.description','fact_batches.product_id')->leftjoin('products','fact_batches.product_id','=','products.id')->where('fact_batches.id','LIKE','%'.$key.'%')->orWhere('products.name','LIKE','%'.$key.'%')->orWhere('fact_batches.max_qty','LIKE','%'.$key.'%')->orWhere('fact_batches.batch_no','LIKE','%'.$key.'%')->orderBy('fact_batches.id','desc')->paginate(30);
        $recipes = Recipe::select('recipes.id','recipes.name','recipes.description')->where('recipes.id','LIKE','%'.$key.'%')->orWhere('recipes.name','LIKE','%'.$key.'%')->orWhere('recipes.description','LIKE','%'.$key.'%')->orderBy('recipes.id','desc')->paginate(30);
        return view('admin.factoryBatch.index',compact('factoryBatches','page_title'));
    }
}

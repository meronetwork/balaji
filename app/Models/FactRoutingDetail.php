<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FactRoutingDetail extends Model
{
    /**
     * @var array
     */
    protected $table = 'fact_routing_details';

    protected $fillable = ['id', 'routing_id', 'area_id', 'descr', 'setup_time', 'cycle_time', 'overhead_cost', 'labour_cost', 'to_user_id','process_order'];

    public function routing()
    {
        return $this->belongsTo(FactRouting::class,'routing_id');
    }

    public function section()
    {
        return $this->belongsTo(ProductsSection::class,'area_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'to_user_id');
    }



    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Communication from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Communication from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }
}

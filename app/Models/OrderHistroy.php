<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistroy extends Model
{
    /**
     * @var array
     */

    protected $table = 'fin_orders_histroy';

    /**
     * @var array
     */

    protected $fillable = ['non_taxable_amount',
        'parent_id', 'user_id', 'reservation_id', 'pos_customer_id', 'order_type', 'name', 'position', 'bill_no', 'address',
        'ship_date', 'require_date', 'sales_tax', 'status', 'paid', 'bill_date', 'due_date',
        'subtotal', 'discount_amount', 'discount_percent', 'discount_note', 'total_amount',
        'comment', 'org_id', 'customer_pan', 'tax_amount', 'terms', 'taxable_amount', 'from_stock_location', 'payment_status', 'outlet_id', 'session_id', 'table', 'room_no', 'covers', 'vat_type', 'service_type', 'amount_with_service', 'settlement', 'service_charge', 'is_bill_active', 'void_reason', 'is_bill_printed', 'fiscal_year', 'company_name', 'is_posted', 'sync_with_ird', 'ready_status', 'posting_entry_id', 'settle_entry_id', 'cancel_date', 'credit_note_no', 'bill_to'
    ];

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function client()
    {
        return $this->belongsTo('\App\Models\Client', 'pos_customer_id');
    }

    public function lead()
    {
        return $this->belongsTo('\App\Models\Lead', 'client_id');
    }

    public function leadname()
    {
        return $this->belongsTo('\App\Models\Lead', 'client_id');
    }

    public function organization()
    {
        return $this->belongsTo('\App\Models\organization');
    }

    public function outlet()
    {
        return $this->belongsTo('\App\Models\PosOutlets');
    }

    public function reservation()
    {
        return $this->belongsTo('\App\Models\Reservation');
    }


    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }


    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ($this->perms()->where('id', $perm->id)->first()) {
            return true;
        }
        // Otherwise
        return false;
    }
}

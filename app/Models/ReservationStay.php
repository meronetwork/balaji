<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationStay extends Model
{
     protected $table = 'reservations_stays';
	
	/**
     * @var array
     */
    protected $fillable = ['room_num', 'previous_checkout_date', 'extend_checkout_date','extend_days','user_id','reservation_id'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
	
}

<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FactIssueRouting extends Model
{
   // use HasFactory;


    protected $table = 'fact_issue_routing';

    protected $fillable = ['issue_id', 'area_id', 'description', 'setup_time', 'cycle_time', 'overhead_cost', 'labour_cost', 'to_user_id', 'actual_start', 'actual_end', 'actutal_qty', 'is_paused', 'is_stop','process_order'];

     public function section()
    {
        return $this->belongsTo(ProductsSection::class,'area_id');
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class,'to_user_id');
    }


}

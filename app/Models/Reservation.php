<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

       protected $table = 'reservations';    

    	/**
         * @var array
         */

        protected $fillable = ['room_id', 'check_in', 'check_out','no_of_adult','no_of_child','smoking','rate_id','room_rate','agent_id','number_of_days','advance_payement','guest_id','no_extra_bed','reservation_status','company_id','nationality','gender','doc_type_extra','doc_num_extra','email','phone','mobile','reservation_type','check_in_time','check_out_time','guest_name','room_num','guest_type','user_id','book_in','book_out','source_id','check_out_by','arrival_flight_time','parent_res','linked_to','arrival_time','flight_no','occupancy','settlement','remarks','void_reason','payment_type_id','cc_no','cc_exp_date','name_on_card','card_swiped','booked_by','complementry_by','deposit_amount','ledger_id','title','source_type','bill_to','occupancy_child','ref_reservation'];          



        public function isEditable()
    	{ 
            if ( \Auth::user()->hasRole('admins'))
                return true;
            // Protect the admins and users Leads from editing changes
            if ( \Auth::user()->id != $this->user_id && \Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4 && \Auth::user()->id != 3 ) {
                return false;
            }
            return true;
    	}

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Leads from deletion
        /*if ( (\Auth::user()->id != $this->user_id  && \Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4 && \Auth::user()->id != 3)) {
            return false;
        } */
        
        if ( !\Auth::user()->hasRole('admins'))
            return false;

        return true;
    }
    public function guest(){
        return $this->belongsTo('App\Models\Guest');
    }

    public function room(){
        return $this->belongsTo('App\Models\Room','room_num','room_number');
    }

    public function rateplan(){ 
        return $this->belongsTo('App\Models\Rateplan','rate_id');
    }

    public function agent(){  
        return $this->belongsTo('App\Models\Client','agent_id','id'); 
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function reservation_state(){
        return $this->belongsTo('App\Models\ReservationStatus','reservation_status','id');
    } 

    public function source(){
          return $this->belongsTo('App\Models\Communication','source_id','id');
    }

    public function client(){
         return $this->belongsTo('App\Models\Client','company_id','id');
    }

    

     public function paymenttype(){
         return $this->belongsTo('App\Models\PaymentType','payment_type_id','payment_type_id');
    }

    public function reservationStatus(){
        return $this->belongsTo('App\Models\ReservationStatus','reservation_status','id');
    }

     public function folio(){
        return $this->belongsTo('App\Models\Folio','id','reservation_id');
    }
}

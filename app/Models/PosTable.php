<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PosTable extends Model
{

    protected $table = 'pos_tables';

    /**
     * @var array
     */

    protected $fillable = ['table_number', 'enabled', 'table_area_id', 'seating_capacity', 'is_packing', 'image','sequence'];


    public function isEditable()
    {

        if (!\Auth::user()->hasRole('admins')) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */

    public function isDeletable()
    {
        if (!\Auth::user()->hasRole('admins'))
            return false;

        return true;
    }

    public function outlet()
    {
        return $this->belongsTo('\App\Models\PosOutlets');
    }

    public function tablearea()
    {
        return  $this->belongsTo('\App\Models\TableArea', 'table_area_id');
    }
}

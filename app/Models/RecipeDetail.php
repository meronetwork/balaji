<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeDetail extends Model
{
    /**
     * @var array
     */
    protected $table = 'recipe_details';

    protected $fillable = ['id', 'recipe_id', 'raw_prod_id', 'qty'];

    public function recipe()
    {
        return $this->belongsTo(Recipe::class,'recipe_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'raw_prod_id');
    }



    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Communication from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Communication from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }
}

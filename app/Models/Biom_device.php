<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Biom_device extends Model
{
	protected $fillable = [
        'device_name',
        'ip_address',
        'com_key',
        'serial_number'
    ];
    public $timestamps = true;
    //
}
 
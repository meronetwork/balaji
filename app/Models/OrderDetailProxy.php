<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line
class OrderDetailProxy extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

     use SoftDeletes; //add this line
    protected $table = 'fin_order_detail_proxy';

    protected $fillable = ['discount_amount','id', 'folio_id', 'description', 'client_id', 'order_id', 'product_id', 'price', 'quantity', 'total', 'bill_date', 'date', 'tax', 'tax_amount', 'is_posted', 'posted_to_ledger', 'flag', 'is_inventory', 'is_printed_kot_bot', 'remarks','discount_percent','product_type_id','reference_order'];


    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    public function ptype(){

        return $this->belongsTo('App\Models\ProductTypeMaster', 'product_type_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }


    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ($this->perms()->where('id', $perm->id)->first()) {
            return true;
        }
        // Otherwise
        return false;
    }
}

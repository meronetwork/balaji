<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbbrOrders extends Model
{
    /**
     * @var array
     */

    protected $table = 'abbr_fin_orders';

    /**
     * @var array
     */

    protected $fillable = ['is_converted','bill_type','non_taxable_amount',
        'folio_id', 'user_id', 'reservation_id', 'pos_customer_id', 'order_type', 'name', 'position', 'bill_no', 'address',
        'ship_date', 'require_date', 'sales_tax', 'status', 'paid', 'bill_date', 'due_date',
        'subtotal', 'discount_amount', 'discount_percent', 'discount_note', 'total_amount',
        'comment', 'org_id', 'tax_amount', 'terms', 'taxable_amount', 'from_stock_location', 'payment_status', 'outlet_id', 'session_id', 'table', 'room_no', 'covers', 'vat_type', 'service_type', 'amount_with_service', 'service_charge', 'fiscal_year', 'ready_status', 'bill_to','transaction_date'
    ];

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function client()
    {
        return $this->belongsTo('\App\Models\Client', 'pos_customer_id');
    }

    public function lead()
    {
        return $this->belongsTo('\App\Models\Lead', 'client_id');
    }

    public function leadname()
    {
        return $this->belongsTo('\App\Models\Lead', 'client_id');
    }

    public function organization()
    {
        return $this->belongsTo('\App\Models\organization');
    }

    public function outlet()
    {
        return $this->belongsTo('\App\Models\PosOutlets');
    }

    public function reservation()
    {
        return $this->belongsTo('\App\Models\Reservation');
    }

    public function folio()
    {
        return $this->belongsTo('\App\Models\Folio', 'folio_id');
    }

    public function ordermeta()
    {
        return $this->hasOne('\App\Models\AbbrOrderMeta', 'order_id', 'id');
    }

     public function restauranttable()
    {
        return $this->hasOne('\App\Models\PosTable', 'id','table');
    }

     public function payments()
    {


        if($this->reservation_id){


            return $this->hasMany('\App\Models\FolioPayment', 'reservation_id','reservation_id');
        }
        return $this->hasMany('\App\Models\Payment', 'sale_id','id')->where('type','abbr');
    }


    public function getPayingLedger(){

        if($this->reservation_id){

           return \App\Models\FolioPayment::where('reservation_id',$this->reservation_id)
            ->first()->city_ledger ?? null;

        }

        $payments = $this->payments;
        $ledgerName = [];

        foreach ($payments as $key => $value) {
            $entry = $value->entry;
            if($entry){

                $entry_items = $entry->entry_items->where('dc','D')->first();
                $ledgerName[] =  $entry_items->ledgerdetail->name ?? '';

            }

        }
        return implode("<br> & ",$ledgerName);


    }
    public function getPayingLedgerAttribute(){

        if($this->reservation_id){

           return \App\Models\FolioPayment::where('reservation_id',$this->reservation_id)
            ->first()->city_ledger ?? null;

        }

        $payments = $this->payments;
        $ledgerName = [];

        foreach ($payments as $key => $value) {
            $entry = $value->entry;
            if($entry){

                $entry_items = $entry->entry_items->where('dc','D')->first();
                $ledgerName[] =  $entry_items->ledgerdetail->name ?? '';

            }

        }
        return implode("<br> & ",$ledgerName);


    }






    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }


    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ($this->perms()->where('id', $perm->id)->first()) {
            return true;
        }
        // Otherwise
        return false;
    }

    public function getTotalItemsQuantityAttribute(){
        $cur_year=\App\Models\Fiscalyear::where('current_year', 1)->first();
        $fiscal_year = request()->fiscal_year ? request()->fiscal_year : $cur_year->fiscal_year;
        $prefix = '';
        if ($fiscal_year !=  $cur_year->fiscal_year) {
            $prefix = Fiscalyear::where('fiscal_year', $fiscal_year)->first()->numeric_fiscal_year . '_';
        }
        $order_details = new AbbrOrderDetail();
        $new_detail_table = $prefix . $order_details->getTable();
        $order_details->setTable($new_detail_table);

        $qty= $order_details->where('order_id',$this->id)->sum('quantity');
        return $qty;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NightAuditLog extends Model
{
    protected $table = 'night_audit_logs';
	
	/**
     * @var array
     */
    protected $fillable = ['user_id', 'res_id', 'operation'];
}

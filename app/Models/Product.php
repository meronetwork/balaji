<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var array
     */

    protected $table = 'products';

    /**
     * @var array
     */

    protected $fillable = ['tax_amount','includes_tax','parent_product_id','name', 'cost', 'price', 'enabled', 'public', 'category_id', 'org_id', 'ordernum', 'alert_qty', 'product_code', 'product_unit', 'product_image', 'product_type_id', 'dollar_price', 'outlet_id','created_by','updated_by','is_raw_material'];


    public function category()
    {
        return $this->belongsTo('\App\Models\ProductCategory');
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Courses from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @param $Course
     * @return bool
     */
    public static function isForced($Course)
    {
        if ('users' == $Course->name) {
            return true;
        }

        return false;
    }

    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Course has is assigned the given permission.
        if ($this->perms()->where('id', $perm->id)->first()) {
            return true;
        }
        // Otherwise
        return false;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function producttypemaster()
    {
        return $this->belongsTo('App\Models\ProductTypeMaster', 'product_type_id');
    }
    public function outlet()
    {
        return $this->belongsTo('App\Models\PosOutlets', 'outlet_id');
    }
    public function unit(){
        return $this->belongsTo(ProductsUnit::class,'product_unit');
    }
    public function parentProduct(){
        return $this->belongsTo(Product::class,'parent_product_id');
    }
    public function childProducts(){
        return $this->hasMany(Product::class,'parent_product_id');
    }
}

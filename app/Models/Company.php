<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class Company extends Model
{
	protected $table = 'lead_company';
    /**
     * @var array
     */
    protected $fillable = ['name'];
	
	public function lead()
    {
        return $this->hasMany('App\Models\Lead');
    }
	
}

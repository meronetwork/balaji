<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TransactionGroups extends Model
{

	 /**
     * @var array
     */

	protected $table = 'transaction_groups';
	
	/**
     * @var array
     */
    protected $fillable = ['group_code','group_type','description','enabled'];




}

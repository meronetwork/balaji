<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class POSPrint extends Model
{
    /**
     * @var array
     */

	protected $table = 'bill_reprints_pos';

	protected $fillable = ['order_id','abbr_order_id', 'printed_date','printed_by'];


    public function contact()
    {
        return $this->hasOne('App\Models\Client');
    }

	/**
	 * @return bool
	 */
	public function isEditable()
	{
			// Protect the admins and users Communication from editing changes
			if ( ('admins' == $this->name) || ('users' == $this->name) ) {
					return false;
			}

			return true;
	}

	/**
	 * @return bool
	 */
	public function isDeletable()
	{
			// Protect the admins and users Communication from deletion
			if ( ('admins' == $this->name) || ('users' == $this->name) ) {
					return false;
			}

			return true;
	}

}

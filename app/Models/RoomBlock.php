<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomBlock extends Model
{
    protected $table = 'room_blocked';
	
	/**
     * @var array
     */
    protected $fillable = ['room_id', 'from_date', 'to_date','reason','user_id',];
	
        public function isEditable()
    {
        // Protect the admins and users Leads from editing changes
        if ( \Auth::user()->id != $this->user_id && \Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4 && \Auth::user()->id != 3 ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Leads from deletion
        /*if ( (\Auth::user()->id != $this->user_id  && \Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4 && \Auth::user()->id != 3)) {
            return false;
        } */
        
        if ( !\Auth::user()->hasRole('admins'))
            return false;

        return true;
    }
     public function room(){
     return $this->belongsTo('App\Models\Room','room_id','room_id');
    }
}

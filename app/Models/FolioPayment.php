<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FolioPayment extends Model
{
    /**
     * @var array
     */

	protected $table = 'folio_payments';

	/**
     * @var array
     */
    protected $fillable = ['entry_id','id','date','user_id','reservation_id','reference_no','transaction_id','paid_by','cheque_no','cc_no','cc_holder','cc_month','expiry_date','cc_type','amount','currency','attachment','type','note','pos_paid','pos_balance','approval_code','folio_id','bank_name','cheque_date','travel_agent_ledger','city_ledger','staff_nc_board','sub_type','complementry_staff','remarks','e_sewa_id','room_no','guest_name','room_reservation_id','is_deposit','record_type','guest_name','currencies','currency_amount','exchange_amount','revenue'];


    public function user()
    {
        return $this->belongsTo('\App\User');
    }



    public function client()
    {
        return $this->belongsTo('\App\Models\Client');
    }

    public function paymenttype()
    {

        return $this->belongsTo('\App\Models\PaymentType','paid_by','payment_type_id');

    }



    public function reservation(){

        return $this->belongsTo('\App\Models\Reservation','reservation_id');

    }

     public function leadname()
    {
        return $this->belongsTo('\App\Models\Lead','client_id');
    }

    public function organization()
    {
        return $this->belongsTo('\App\Models\organization');
    }

	/**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }


	public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ( $this->perms()->where('id' , $perm->id)->first() ) {
            return true;
        }
        // Otherwise
        return false;
    }

    public function entry(){
        return $this->belongsTo(Entry::class,'entry_id');
    }
    public function sale()
    {
        return $this->belongsTo(Orders::class, 'reservation_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FactoryLossGain extends Model
{

    protected $table = 'fact_loss_gain_modes';

    protected $fillable = ['loss_gain_mode'];

    public function isEditable()
    {
        // Protect the admins and users Communication from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Communication from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
}


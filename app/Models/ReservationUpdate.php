<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationUpdate extends Model
{
     protected $table = 'reservation_update';
	
	/**
     * @var array
     */
    protected $fillable = ['res_id', 'user_id'];
	
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessDate extends Model
{
    protected $table='business_date';

    protected $guarded=[];
}

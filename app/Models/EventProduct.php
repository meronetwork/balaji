<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventProduct extends Model
{
    protected $table = 'event_products';
    protected $fillable = ['event_id','name'];
}

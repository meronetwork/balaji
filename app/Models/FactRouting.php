<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FactRouting extends Model
{
    /**
     * @var array
     */
    protected $table = 'fact_routing';

    protected $fillable = ['id', 'bom_id', 'product_id', 'name'];

    public function bom()
    {
        return $this->belongsTo(FactBOM::class,'bom_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function routingDetails(){


        return $this->hasMany(\App\Models\FactRoutingDetail::class, 'routing_id', 'id');
    }    




    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Communication from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Communication from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }
}

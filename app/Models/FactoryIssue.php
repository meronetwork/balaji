<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FactoryIssue extends Model
{
    /**
     * @var array
     */
    protected $table = 'fact_issue';

    protected $fillable = ['id',  'production', 'description', 'date', 'return_issue_id', 'can_return','product_id','qty','user_id','start','finish','status','total_cost','cost_per_piece','cost_of_mat','overhead_cost','labour_cost','selected_bom'];


    public function factbom(){
        return $this->belongsTo(\App\Models\FactBOM::class,'product_id','product_id');
    }


    public function issueBomDetail(){


        return $this->hasMany(\App\Models\FactIssueBOM::class, 'issue_id', 'id');
    }  

    public function issueRouteDetail(){


        return $this->hasMany(\App\Models\FactIssueRouting::class, 'issue_id', 'id');
    }   


    public function product(){
        return $this->belongsTo(\App\Models\Product::class,'product_id');
    }


    public function user(){
        return $this->belongsTo(\App\User::class,'user_id');
    }


    public function costInfo(){

        $materialCost = $this->total_cost;
        $factRouteDetails = $this->issueRouteDetail;
        $overheadCost =  $factRouteDetails->sum('overhead_cost') * $this->qty;
        $labourCost = $factRouteDetails->sum('labour_cost') * $this->qty;
        $totalCost = $materialCost + $overheadCost + $labourCost;
        $perUnitCost = $totalCost / $this->qty;


        return ['materialCost'=>$materialCost,'overheadCost'=>$overheadCost,'labourCost'=>$labourCost,'totalCost'=>$totalCost,'perUnitCost'=>$perUnitCost];
    }

    public function overruncost($costInfo){



      $actual =   \StockHelper::getBomCost($this->product_id,$this->qty,$this->selected_bom)[0]['approxCost'];

      $overruncost = $costInfo['labourCost'] + $costInfo['overheadCost'] + $actual ;

      $overruncost = $costInfo['totalCost'] - $overruncost;


      return ['overruncost'=>$overruncost,'perQty'=>$overruncost / $this->qty];
     



    }




    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Communication from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Communication from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class ProjectTask extends Model
{
    /**
     * @var array
     */

	protected $table = 'project_tasks';

	protected $fillable = ['project_id','user_id','category_id','subject', 'description', 'percent_complete', 'actual_duration','schedule','timespan', 'duration', 'estimated_effort', 'milestone', 'order', 'precede_tasks', 'priority', 'peoples', 'attachment', 'status', 'start_date','end_date', 'enabled','task_order','stage_id','color'];


      public function project()
	    {
	        return $this->belongsTo('\App\Models\Projects', 'project_id');
	    }

	    public function tags()
	    {
	        return $this->belongsToMany('\App\User','project_task_user');
	    }

	     public function user()
	    {
	        return $this->belongsTo('\App\User');
	    }
	    public function stage(){
	    	return $this->belongsTo('\App\Models\TaskStage','stage_id');
	    }
	 	public function category(){
	 	return $this->belongsTo('\App\Models\ProjectTaskCategory','category_id');
	 }
	/**
	 * @return bool
	 */
	public function isEditable()
	{
			// Protect the admins and users Communication from editing changes
			if ( ('admins' == $this->full_name) || ('users' == $this->full_name) ) {
					return false;
			}

			return true;
	}

	/**
	 * @return bool
	 */
	public function isDeletable()
	{
			// Protect the admins and users Communication from deletion
			if ( ('admins' == $this->full_name) || ('users' == $this->full_name) ) {
					return false;
			}

			return true;
	}

}

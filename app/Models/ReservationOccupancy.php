<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationOccupancy extends Model
{
    protected $table = 'reservation_occupancy';

    protected $fillable = ['resv_id','occupancy_child','occupancy_adult','guest_type','date'];


}

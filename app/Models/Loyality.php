<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loyality extends Model
{
    protected $table = 'loyalties';
	
	/**
     * @var array
     */
    protected $fillable = ['client_id','outlet_id','invoice_id','invoice_amount','type'];

    public function invoice(){

    	return $this->belongsTo('\App\Models\Orders');
    }
        public function client()
    {
        return $this->belongsTo('\App\Models\Client', 'client_id');
    }

        public function outlet()
    {
        return $this->belongsTo('\App\Models\PosOutlets');
    }
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FolioDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'folio_details';

    protected $fillable = ['id','folio_id','product_id', 'description', 'price', 'quantity', 'total', 'bill_date', 'date', 'tax', 'tax_amount','posted_to_ledger','is_posted','non_chargeable','flag','res_id'];  



    public function article()
    {
        return $this->belongsTo('App\Models\Article', 'product_id');  
    }
      

    public function customer()
    { 
        return $this->belongsTo('App\User', 'customer_id');
    } 

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function folio()
    {
        return $this->belongsTo('App\Models\Folio', 'folio_id');
    }

    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }


    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ( $this->perms()->where('id' , $perm->id)->first() ) {
            return true;
        }
        // Otherwise
        return false;
    }

}

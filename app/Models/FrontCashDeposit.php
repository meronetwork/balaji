<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrontCashDeposit extends Model
{
    use HasFactory;

    protected $table = 'front_cash_deposit';

    protected $fillable =['room_num', 'guest', 'pay_in', 'record_type', 'currency_id', 'currency_amount', 'total_amount', 'notes'];
}

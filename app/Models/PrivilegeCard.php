<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrivilegeCard extends Model
{
    use HasFactory;
    protected $fillable=['image','name', 'address', 'contact_no', 'dob', 'nationality', 'married_status', 'qualification', 'occupation', 'office_name', 'office_address', 'familiar_about_indreni', 'feel_about_service', 'any_suggestion', 'how_would_you_utilize', 'customer_sign', 'date', 'card_no', 'membership_type', 'valid_upto', 'date_of_issue', 'approved_by','phone','email'];
    protected $table='privilege_card';

    public function isEditable()
    {
        // Protect the admins and users Leads from editing changes
        if (\Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        if (! \Auth::user()->hasRole('admins')) {
            return false;
        }

        return true;
    }


}

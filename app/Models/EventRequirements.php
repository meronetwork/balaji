<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventRequirements extends Model
{
    protected $table = 'events_requirements';
    protected $fillable = ['event_id','name'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecipeStepIngredient extends Model
{
    use HasFactory;
    protected $table='recipe_steps_ingredients';
    protected $fillable=['recipe_id','sepe_num','ingredient_id','amount_required'];
    use HasFactory;
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ($this->perms()->where('id', $perm->id)->first()) {
            return true;
        }
        // Otherwise
        return false;
    }
    public function recipe(){
        return $this->belongsTo(Recipe::class,'recipe_id');
    }
    public function ingredient(){
        return $this->belongsTo(Product::class,'ingredient_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsSection extends Model
{
    protected $table = 'product_sections';
    protected $fillable = ['section_name','section_area'];
}

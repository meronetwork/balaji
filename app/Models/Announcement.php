<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Announcement extends Model
{
    /**
     * @var array
     */

	protected $table = 'announcements';
	
	/**
     * @var array
     */
    protected $fillable = ['title', 'description','user_id','status', 'view_status', 'start_date','end_date','all_client','org_id','placement'];
	

    public function user()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }


	/**
     * @return bool
     */
    public function isEditable()
    {
        // Prevent user from deleting his own account.
        if ( Auth::check() && (Auth::user()->id == $this->user_id) ) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Prevent user from deleting his own account.
        if ( Auth::check() && (Auth::user()->id == $this->user_id) ) {
            return true;
        }

        return false;
    }
	
	
	public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ( $this->perms()->where('id' , $perm->id)->first() ) {
            return true;
        }
        // Otherwise
        return false;
    }
	



}
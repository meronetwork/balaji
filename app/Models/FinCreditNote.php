<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinCreditNote extends Model
{
    
    protected $table = 'fin_credit_notes';

    protected $fillable = ['order_id','credit_note_no','credit_user_id','void_reason','cancel_date'];

}

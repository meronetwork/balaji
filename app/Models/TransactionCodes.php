<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TransactionCodes extends Model
{

	 /**
     * @var array
     */

	protected $table = 'transactions_codes';
	
	/**
     * @var array
     */
    protected $fillable = ['code','description','transaction_group_id','default_price','enabled'];


    public function transactiongroups()
    {
        return $this->belongsTo('\App\Models\TransactionGroups','transaction_group_id');
    }

}

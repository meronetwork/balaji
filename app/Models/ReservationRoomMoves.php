<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationRoomMoves extends Model
{
 
  protected $table = 'reservation_room_moves';
	
	/**
     * @var array
     */
    protected $fillable = ['reservation_id', 'from_room','to_room','moved_date'];
	


}

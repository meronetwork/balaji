<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FactIssueBOM extends Model
{
   // use HasFactory;


    protected $table = 'fact_issue_bom';

    protected $fillable = [ 'issue_id', 'raw_product_id', 'qty'];

    
    public function product()
    {
        return $this->belongsTo(Product::class,'raw_product_id');
    }

}

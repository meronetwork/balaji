<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
     protected $fillable = ['event_type', 'event_name', 'venue_id','event_start_date','event_end_date','amount_paid','potential_cost','calculated_cost','edited_cost','extra_cost','event_status','num_participants','user_id','menu_items','other_details','comments','schedule','contact_num','booked_by','bill_to','booking_advance','set_up','bar_info','source','person_name','email','landline','address','city','post_code','setup_type'];


    public function isEditable()
	{
			// Protect the admins and users Communication from editing changes
			if ( ('admins' == $this->name) || ('users' == $this->name) ) {
					return false;
			}

			return true;
	}

	/**
	 * @return bool
	 */
	public function isDeletable()
	{
			// Protect the admins and users Communication from deletion
			if ( ('admins' == $this->name) || ('users' == $this->name) ) {
					return false;
			}

			return true;
	}

    public function venue()
    {
        return $this->belongsTo('App\Models\EventVenues'); 
    }


    public function client()
    {
        return $this->belongsTo('\App\Models\Client','bill_to');
    }

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

}

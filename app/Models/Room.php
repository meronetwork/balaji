<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';

    /**
     * @var array
     */
    protected $fillable = ['roomtype_id', 'room_number', 'floor_number', 'status_id', 'hk_remarks', 'housekeeper_id', 'house_status_id'];

    public function isEditable()
    {
        // Protect the admins and users Leads from editing changes
        if (!\Auth::user()->hasRole('admins')) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Leads from deletion
        /*if ( (\Auth::user()->id != $this->user_id  && \Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4 && \Auth::user()->id != 3)) {
            return false;
        } */

        if (!\Auth::user()->hasRole('admins'))
            return false;

        return true;
    }

    public function room_type()
    {

        return $this->belongsTo('App\Models\RoomType', 'roomtype_id', 'roomtype_id');
    }

    public function house_stat()
    {

        return $this->belongsTo('App\Models\Housestatus', 'house_status_id', 'id');
    }
}

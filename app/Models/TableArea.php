<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TableArea extends Model
{
    /**
     * @var array
     */

	protected $table = 'table_area';     
	
	/**
     * @var array
     */

    protected $fillable = ['floor_id','name'];

    public function posfloor(){
        return  $this->belongsTo('\App\Models\PosFloor','floor_id');
    }
	

}




<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TimeSheet extends Model
{
    /**
     * @var array
     */

	protected $table = 'timesheet';
	
	/**
     * @var array
     */
    protected $fillable = ['employee_id','date','time_from','time_to','comments','activity_id','project_id','date_submitted'];
	

    public function user()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }

     public function employee()
    {
        return $this->belongsTo('\App\User', 'employee_id');
    }

     public function activity()
    {
        return $this->belongsTo('\App\Models\Activity');
    }

/**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }
    
	public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ( $this->perms()->where('id' , $perm->id)->first() ) {
            return true;
        }
        // Otherwise
        return false;
    }
	


}
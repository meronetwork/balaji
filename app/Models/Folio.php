<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Folio extends Model
{
    /**
     * @var array
     */

	protected $table = 'folio';

	/**
     * @var array
     */

    protected $fillable = ['settle_entry_id','posting_entry_id','user_id','folio_type','reservation_id','org_id','require_date','sales_tax','bill_date','bill_no','amount','discount_amount','discount_note','taxable_amount','tax_amount','total_amount','paid_amount','subtotal','discount_percent','from_stock_location','payment_status','vat_type','service_type','amount_with_service','settlement','service_charge','is_bill_active','void_reason','is_bill_printed','fiscal_year','is_final_invoice','last_settled'];

       public function folio_details()
    {
        return $this->hasMany('App\Models\FolioDetail', 'folio_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function client()
    {
        return $this->belongsTo('\App\Models\Client');
    }

    public function lead()
    {
        return $this->belongsTo('\App\Models\Lead','client_id');
    }

     public function leadname()
    {
        return $this->belongsTo('\App\Models\Lead','client_id');
    }

    public function organization()
    {
        return $this->belongsTo('\App\Models\organization');
    }

    public function reservation()
    {
        return $this->belongsTo('\App\Models\Reservation','reservation_id');
    }


	/**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if ( ('admins' == $this->name) || ('users' == $this->name) ) {
            return false;
        }

        return true;
    }


	public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ( $this->perms()->where('id' , $perm->id)->first() ) {
            return true;
        }
        // Otherwise
        return false;
    }




}

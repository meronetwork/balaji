<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FactBOM extends Model
{
    /**
     * @var array
     */
    protected $table = 'fact_bom';

    protected $fillable = ['id', 'product_id', 'bom_name', 'routing_id', ];

    public function routing()
    {
        return $this->belongsTo(FactRouting::class,'routing_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }



    public function factBomDetail(){


        return $this->hasMany(\App\Models\FactBOMDetail::class, 'bom_id', 'id');
    }    


    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Communication from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Communication from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }
}

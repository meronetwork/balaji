<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rateplan extends Model
{
    protected $table = 'roomtype_rate_plans';

    /**
     * @var array
     */
    protected $fillable = ['roomtype_id', 'package_name', 'rate', 'breakfast_rate', 'lunch_rate', 'dinner_rate', 'order_num', 'detail', 'enabled'];

    public function isEditable()
    {
        // Protect the admins and users Leads from editing changes
        if (!\Auth::user()->hasRole('admins')) {
            return false;
        }

        return true;
    }


    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Leads from deletion
        /*if ( (\Auth::user()->id != $this->user_id  && \Auth::user()->id != 1 && \Auth::user()->id != 5 && \Auth::user()->id != 4 && \Auth::user()->id != 3)) {
            return false;
        } */

        if (!\Auth::user()->hasRole('admins'))
            return false;

        return true;
    }

    public function roomtype()
    {
        return $this->belongsTo('App\Models\RoomType', 'roomtype_id', 'roomtype_id');
    }
}

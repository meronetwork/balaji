<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tablebooking extends Model
{
    /**
     * @var array
     */

    protected $table = 'table_bookings';

    /**
     * @var array
     */

    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'no_of_guests', 'reservation', 'table_reservation', 'reservation_type', 'if_other', 'special_request', 'user_id', 'org_id'];

    public function posfloor()
    {
        return  $this->belongsTo('\App\Models\PosFloor', 'floor_id');
    }
}

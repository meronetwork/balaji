<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationBed extends Model
{
    protected $table = 'reservation_beds';
	
	/**
     * @var array
     */
    protected $fillable = ['res_id','no_of_beds','from_date','to_date','user_id'];
}

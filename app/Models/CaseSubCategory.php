<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class CaseSubCategory extends Model
{   
    /**
     * @var array
     */

    protected $table = 'cases_sub_category';

    protected $fillable = ['name','subject','description','parent_id'];
	
	/**
     * @return bool  
     */
    
    public function isEditable()
    {
            // Protect the admins and users Communication from editing changes
            if ( ('admins' == $this->name) || ('users' == $this->name) ) {
                    return false;
            }

            return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
            // Protect the admins and users Communication from deletion
            if ( ('admins' == $this->name) || ('users' == $this->name) ) {
                    return false;
            }

            return true;
    }
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Casecategory','parent_id');
    }
}

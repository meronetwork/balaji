<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PosKitchen extends Model
{

    protected $table = 'pos_kitchen';

    /**
     * @var array
     */

    protected $fillable = ['kitchen_code', 'name', 'short_name', 'enabled'];


    public function isEditable()
    {

        if (!\Auth::user()->hasRole('admins')) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {

        if (!\Auth::user()->hasRole('admins'))
            return false;

        return true;
    }
}

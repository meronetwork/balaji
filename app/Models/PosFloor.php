<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class POSFloor extends Model
{
    /**
     * @var array
     */

	protected $table = 'pos_floors';   
	
	/**
     * @var array
     */

    protected $fillable = ['outlet_id','name'];

    public function outlet(){
        return  $this->belongsTo('\App\Models\PosOutlets');
    }
	

}




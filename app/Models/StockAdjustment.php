<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class StockAdjustment extends Model
{
    /**
     * @var array
     */

	protected $table = 'stock_adjustment';
	
	/**
     * @var array
     */
    protected $fillable = ['date','location_id','reason','status','ledgers_id','vat_type','subtotal','discount_percent','taxable_amount','tax_amount','total_amount','comments','entry_id'];
	

    public function productlocation()
    {
        return $this->belongsTo('\App\Models\ProductLocation','location_id');
    }
  

     public function adjustmentreason()
    {
        return $this->belongsTo('\App\Models\AdjustmentReason','reason');
    }

	



}
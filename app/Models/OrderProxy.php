<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //add this line
class OrderProxy extends Model
{
    /**
     * @var array
     */
    use SoftDeletes; //add this line

    protected $table = 'fin_orders_proxy';


    /**
     * @var array
     */

    protected $fillable = ['invoice_type','non_taxable_amount',
        'folio_id', 'user_id', 'reservation_id', 'pos_customer_id', 'order_type', 'name', 'position', 'bill_no', 'address',
        'ship_date', 'require_date', 'sales_tax', 'status', 'paid', 'bill_date', 'due_date',
        'subtotal', 'discount_amount', 'discount_percent', 'discount_note', 'total_amount',
        'comment', 'org_id', 'tax_amount', 'terms', 'taxable_amount', 'from_stock_location', 'payment_status', 'outlet_id', 'session_id', 'table', 'room_no', 'covers', 'vat_type', 'service_type', 'amount_with_service', 'service_charge', 'fiscal_year', 'ready_status', 'bill_to','transaction_date','is_final_invoice','deleted_at',
        'merged_to','is_edm','reference_order','edm_no','is_splitted','served_waiter'
    ];

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function client()
    {
        return $this->belongsTo('\App\Models\Client', 'pos_customer_id');
    }

    public function lead()
    {
        return $this->belongsTo('\App\Models\Lead', 'client_id');
    }

    public function leadname()
    {
        return $this->belongsTo('\App\Models\Lead', 'client_id');
    }

    public function organization()
    {
        return $this->belongsTo('\App\Models\organization');
    }

    public function outlet()
    {
        return $this->belongsTo('\App\Models\PosOutlets');
    }

    public function reservation()
    {
        return $this->belongsTo('\App\Models\Reservation');
    }

    public function folio()
    {
        return $this->belongsTo('\App\Models\Folio', 'folio_id');
    }


    public function getRoomNumber(){

        if($this->room_no){

            return $this->room_no;
        }else{
            return $this->reservation->room_num;
        }


    }


    public function food_items(){
        return $this->hasMany('App\Models\OrderDetailProxy', 'order_id', 'id');
    }

     public function restauranttable()
    {
        return $this->hasOne('\App\Models\PosTable', 'id','table');
    }

     public function payments()
    {
        return $this->belongsTo('\App\Models\Payment', 'id','sale_id');
    }

       public function merged_order()
    {
        return $this->belongsTo('\App\Models\OrderProxy', 'merged_to','id');
    }



    /**
     * @return bool
     */
    public function isEditable()
    {
        // Protect the admins and users Intakes from editing changes
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        // Protect the admins and users Intakes from deletion
        if (('admins' == $this->name) || ('users' == $this->name)) {
            return false;
        }

        return true;
    }


    public function hasPerm(Permission $perm)
    {
        // perm 'basic-authenticated' is always checked.
        if ('basic-authenticated' == $perm->name) {
            return true;
        }
        // Return true if the Intake has is assigned the given permission.
        if ($this->perms()->where('id', $perm->id)->first()) {
            return true;
        }
        // Otherwise
        return false;
    }
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Article extends Model
{

	 /**
     * @var array
     */

	protected $table = 'articles';
	
	/**
     * @var array
     */
    protected $fillable = ['code','description','transaction_code_id','price','sequence'];


    public function transactioncode()
    {
        return $this->belongsTo('\App\Models\TransactionCodes','transaction_code_id');
    }


}

<?php $readonly = ($producttypemasters->isEditable())? '' : 'readonly'; ?>

<div class="content">

    <div class="form-group">
        <label> Product Type Master Name</label>
          {!! Form::text('name', null, ['class' => 'form-control', $readonly]) !!}
    </div>

    <div class="form-group">
        <label>Ledger ID</label>
          {!! Form::text('ledger_id', null, ['class' => 'form-control', $readonly]) !!}
    </div>
      <div class="form-group">
        <label>Discount Limit</label>
          {!! Form::text('discount_limit', null, ['class' => 'form-control', $readonly]) !!}
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                {!! '<input type="hidden" name="enabled" value="0">' !!}
                {!! Form::checkbox('enabled', '1', $producttypemasters->enabled) !!} {{ trans('general.status.enabled') }}
            </label>
        </div>
    </div>

</div><!-- /.content -->




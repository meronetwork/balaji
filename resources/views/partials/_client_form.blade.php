<?php $readonly = ($client->isEditable())? '' : 'readonly'; ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<div class="content" style="padding-left: 0;">
    <div class="col-md-6" style="padding-left: 0;">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label" style="text-transform: capitalize;">
                {{ $_GET['relation_type']}} Name
            </label>
            <div class="col-sm-10">
                {!! Form::text('name', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('location', trans('admin/clients/general.columns.location')) !!}
            </label>
            <div class="col-sm-10">
                {!! Form::text('location', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('phone', trans('admin/clients/general.columns.phone')) !!}
            </label>
            <div class="col-sm-10">
                {!! Form::text('phone', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('email', trans('admin/clients/general.columns.email')) !!}
            </label>
            <div class="col-sm-10">
                {!! Form::text('email', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                VAT ID (Tax ID)
            </label>
            <div class="col-sm-10">
                {!! Form::text('vat', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group" id="travel_agent_commission" style="display: none;">
            <label for="inputEmail3" class="col-sm-2 control-label">
                Commission
            </label>
            <div class="col-sm-10">
                {!! Form::number('commission', null, ['class' => 'form-control','max'=>'99', $readonly]) !!}
            </div>
        </div>
        @if(\Request::segment(3) != 'create')
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                Ledger ID
            </label>
            <div class="col-sm-10">
                {!! Form::select('ledger_id',$ledger_list,$ledger_id,['class'=>'form-control searchable select2',$readonly]) !!}
            </div>
        </div>
        @endif
            <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                Pos Discount
            </label>
            <div class="col-sm-10">
                <input type="number" name="pos_discount" step="any" class="form-control" placeholder="Enter discount in percentage..." value="{{$client->pos_discount}}">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('website',trans('admin/clients/general.columns.website')) !!}
            </label>
            <div class="col-sm-10">
                {!! Form::text('website', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('industry', trans('admin/clients/general.columns.industry')) !!}
            </label>
            <div class="col-sm-10">
                {!! Form::select('industry', ['Hospitality'=>'Hospitality', 'Travel'=>'Travel', 'Education'=>'Education', 'Health'=>'Health', 'IT' =>'IT', 'ISP'=>'ISP', 'Banking'=>'Banking', 'Brokers'=>'Brokers', 'Trading'=>'Trading', 'Construction'=>'Construction', 'Government'=>'Government', 'Engineering'=>'Engineering', 'Media'=>'Media', 'Finance'=>'Finance', 'Retail'=>'Retail'], null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label" style="text-transform: capitalize;">

                {!! Form::label('customer_group', $_GET['relation_type'].' Groups') !!}
            </label>
            <div class="col-sm-10">
                {!! Form::select('customer_group', $groups, null, ['class' => 'form-control searchable', $readonly,'placeholder'=>'Select Groups']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('type', trans('admin/clients/general.columns.type')) !!}
            </label>
            <div class="col-sm-10">
                <!--  {!! Form::select('type', ['Customer'=>'Customer','Supplier'=>'Supplier', 'Agent'=>'Agent'], null, ['class' => 'form-control', $readonly]) !!} -->

                <?php
           if(\Request::get('relation_type') == 'poscustomer'){
            $selected_ledger = env('CITY_LEDGER_GROUP_ID');
           }
           elseif (\Request::get('relation_type') == 'hotelcustomer') {

              $selected_ledger = env('COMPANY_GROUP_ID');

           }

           ?>

                <select class='form-control searchable select2 ' name="types" id="types">
                    @if(\Request::get('relation_type') == 'supplier')
                    <optgroup label="Supplier">
                        <?php
                    //Sunny_deptors
                    $groups= \App\Models\COAgroups::orderBy('code', 'asc')->where('parent_id','61')->where('org_id',\Auth::user()->org_id)->get();
                    foreach($groups as $grp)
                    {
                        echo '<option value="'.$grp->id.'"'.
                        (($grp->name==$client->type)?'selected="selected"':"").
                        '>'
                        .$grp->name.'</option>';
                    }
                     ?>
                    </optgroup>
                    @endif
                        @if(\Request::get('relation_type') == 'poscustomer')
                        <optgroup label="Customers">
                        <?php
                     //Sunny_creditors
                    $groups= \App\Models\COAgroups::orderBy('code', 'asc')->where('parent_id','20')->where('org_id',\Auth::user()->org_id)->get();
                    foreach($groups as $grp)
                    {

                        if($grp->name == $client->type){
                            echo "<option value='{$grp->id}' selected>{$grp->name}</option>";
                        }
                        else if($grp->id == $selected_ledger && \Request::segment(4) == 'create'){
                             echo "<option value='{$grp->id}' selected>{$grp->name}</option>";
                        }

                        else{
                            echo "<option value='{$grp->id}'>{$grp->name}</option>";

                        }


                    }
                   ?>
                    </optgroup>
                            @endif
                </select>
                <script type="text/javascript">
                     @if(\Request::segment(3) == 'create')

        $('#types').val(`{{$selected}}`);


    @endif
                </script>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">
                {!! Form::label('stock_symbol', trans('admin/clients/general.columns.stock_symbol')) !!}
            </label>
            <div class="col-sm-10">
                {!! Form::text('stock_symbol', null, ['class' => 'form-control', $readonly]) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">
            {!! Form::checkbox('enabled', '1', $client->enabled) !!} {!! trans('general.status.enabled') !!}
        </label>
    </div>
</div><!-- /.content -->
<script type="text/javascript">

    @if(\Request::segment(3) == 'create')

        $('#types').val(`{{$selected}}`);


    @endif
    $(document).ready(function() {
        $('.searchable').select2();
    });

</script>
<script type="text/javascript">
    var env_travel_agent = '{{ env('TRAVEL_AGENT_GROUP_ID') }}';


    $('#types').on('change', function() {
        if (this.value == env_travel_agent) {

            $('#travel_agent_commission').show();

        } else {
            $('#travel_agent_commission').hide();
        }
    });

    $(document).ready(function() {

        var x = $('#types').val();
        if (x == env_travel_agent) {
            $('#travel_agent_commission').show();

        } else {
            $('#travel_agent_commission').hide();
        }

    });

</script>

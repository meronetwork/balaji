<?php 
if (isset($showpage) && $showpage == true)
    $readonly = 'readonly';
else
    $readonly = ($expenses->isEditable())? '' : 'readonly';  
?>

<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>

<link rel="stylesheet" href="/nepali-date-picker/nepali-date-picker.min.css">
<script type="text/javascript" src="/nepali-date-picker/nepali-date-picker.js"> </script>


<div class="content">
    <div class="col-md-6">
        <div class="form-group">
              <label for="inputEmail3" class="col-sm-4 control-label">Date Type</label>
              <div class="col-sm-8">
              <select id="selectdatetype" name="datetype" class="form-control label-success">
                <option value="eng">English</option>
                <option value="nep">Nepali</option>
              </select>
          </div>
         </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
            Date
            </label><div class="col-sm-8" id='dateselectors'>
            {!! Form::text('date', $expenses->date , ['class' => 'form-control datepicker','placeholder'=>'Date', $readonly]) !!}
        </div>
        </div>

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
           Expense Type
            </label><div class="col-sm-8">
               {{--  {!! Form::select('expense_type',array('expense'=>'Indirect Expenses','bill_payment'=>'Bill Payement','advance_payment'=>'Advance Payment','sales_return'=>'Sales Return'),$expenses->expense_type,['class'=>'form-control label-primary','id'=>'expense_type']) !!} --}}
                 {!! Form::select('expense_type',array('expense'=>'Indirect Expenses','bill_payment'=>'Bill Payement','advance_payment'=>'Advance Payment','sales_return'=>'Sales Return'),$expenses->expense_type,['class'=>'form-control label-primary']) !!}
            </div>
        </div>


         <div class="form-group">
                            <label for="priority" class="col-sm-4 control-label">
                            Select Tags
                            </label>
                            <div class="col-sm-8">
                            {!! Form::select('tag_id', [''=>"Select Tag"]+$tags,$expenses->tag_id, ['class' => 'form-control select2','required'=>'required']) !!}
                        </div>
                        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
            Expenses Account <a href="#" data-target="#modal_dialog"  data-toggle="modal" >[+]</a>
            </label>
            <div class="col-sm-8" >
                <select  class="form-control searchable select2 " name="expenses_account" required="" id='expenses_account'>
                  
                </select>
            </div>
        </div>
 


        <div class="form-group">

            <label for="inputEmail3" class="col-sm-4 control-label">
            Amount
            </label>

            <div class="col-sm-8">
           
       

        <div class="input-group">
                <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                 {!! Form::text('amount', $expenses->amount, ['class' => 'form-control','placeholder'=>'Amount', $readonly]) !!}
               
              </div>

               </div>

        </div>

        <?php 
        
        //Sunny_deptors
        $cashgroups= \App\Models\COALedgers::orderBy('code', 'asc')->where('group_id',\FinanceHelper::get_ledger_id('CASH_EQUIVALENTS'))->where('org_id',\Auth::user()->org_id)->get();

        ?>

            

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
            Paid Through
            </label><div class="col-sm-8">
             <select class = 'form-control searchable select2 ' name="paid_through" >
                   @foreach($cashgroups as $gps)
                         <option value="{{$gps->id}}" @if($expenses->paid_through == $gps->id) selected @endif>{{$gps->name}}</option>
                   @endforeach
              
            </select>
        </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
          Supplier <a href="javascript::void()" onclick="openwindow()" >[+]</a>
            </label><div class="col-sm-8" id='customers_id'>
            {!! Form::select('vendor_id',$vendors, $expenses->vendor_id, ['class' => 'form-control searchable select2','placeholder'=>'Select Supplier',$readonly]) !!}
        </div>
        </div>

         <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
           Narration
            </label><div class="col-sm-8">
           {!! Form::text('reference', $expenses->reference, ['class' => 'form-control','placeholder'=>'Narration', $readonly]) !!}
        </div>
        </div>
        
        @if(! (  isset($showpage) ? $showpage : '' ) || $expenses->attachment)
         <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">
            Attachment
            </label><div class="col-sm-8">
          @if(!(  isset($showpage) ? $showpage : '' ))
            {!! Form::file('attachment', null, ['class' => 'form-control']) !!}
          @endif
            @if($expenses->attachment)
            <a href="{{ asset('attachment/'.$expenses->attachment)}}" target="_blank">{{$expenses->attachment}}</a>
            @endif
        </div>
        </div>
        @endif
    </div>
    <div class="col-md-6">
       
    
</div><!-- /.content -->

<div id='expense_account_options' style="display: none">



@foreach ($required_account as $key=>$value)
  <select class="form-control searchable"  id='{{ $key }}-id'>
    <option value="">Select Ledger</option>
    {{ \FinanceHelper::ledgerGroupsOptionshtml($value) }}
  </select>
@endforeach


</div>


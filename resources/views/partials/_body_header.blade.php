<style>
    #notviewed-leads_length {
        float: left;
    }

    #notviewed-leads_filter {
        float: right;
    }



    @media (max-width: 767px) {
        .MobileView {
            display: none;
        }
    }

    @media (min-width: 767px) {

        .DeskView {
            display: none;
        }
    }
    .alert-icon{
        animation: blink 2.5s ease-in infinite;
    }
    @keyframes blink {
        from, to { opacity: 1 }
        50% { opacity: 0 }
    }

</style>
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />

<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script type="text/javascript">
    function getCookie(name) {

        const value = `; ${document.cookie}`;

        const parts = value.split(`; ${name}=`);

        if (parts.length === 2) return parts.pop().split(';').shift();

    }



    if (getCookie(('sidebar_collapse'))) {

        $('.sidebar-mini').addClass('sidebar-collapse');

    }

</script>
<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"> <?php  echo TaskHelper::GetOrgName(\Auth::user()->org_id)->organization_name ?> </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Sidebar toggle button-->
        <ul class="nav navbar-nav MobileView">


            <?php
            $orgs=\App\Models\Organization::pluck('organization_name','id')->all();
            $fiscal_years=\App\Models\Fiscalyear::latest()->where('org_id',\Auth::user()->org_id)->pluck('fiscal_year','numeric_fiscal_year')->take(2);
            ?>
            @if(\Auth::user()->super_manager == 1)

                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        {!! Form::select('org_id', [''=>'Select Org']+$orgs, \Auth::user()->org_id, ['class' => 'form-control input-sm label-default', id => "header_org_id"]) !!}
                        <input type="hidden" name="header_user_id" id="header_user_id" value="{{\Auth::user()->id}}">
                    </div>
                </form>
            @endif
                @if(\Auth::user()->hasRole('admins'))
                    <?php
                    $current_fiscal_year=\App\Models\Fiscalyear::where('current_year',1)->first();
                    session_start();
                    $selected_fiscal_year=\Session::get('selected_fiscal_year')??$current_fiscal_year->numeric_fiscal_year;
                    ?>
                <form class="navbar-form navbar-left" role="search" id="session-fiscal-year" method="GET" action="/admin/set-session">
                <div class="form-group">
                    {!! Form::select('fiscal_year_id',$fiscal_years, $selected_fiscal_year, ['class' => 'form-control input-sm label-default', 'id' => "header_fiscal_year"]) !!}
                </div>
            </form>
                @endif


            @if (Auth::check())
                @if(!\Auth::user()->hasRole(['pos-user','pos-manager','waiter','hrm','accountant']))
                <!-- add some lists here-->
                @endif
            @endif



            <li><a href="/home">Hotel Home</a></li>
            <li><a href="/admin/pos/dashboard">POS Home</a></li>


            <li class="dropdown dropdown-large">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Top Nav <b class="caret"></b></a>

                <ul class="dropdown-menu dropdown-menu-large row">
                    <li class="col-sm-3">
                        <ul>
                            <li class="dropdown-header">Financial Mgmt</li>

                            <li><a href="/admin/finance/dashboard">Finance Board</a></li>
                            <li><a href="/admin/accounts/reports/balancesheet">Balance Sheet</a></li>
                            <li><a href="/admin/accounts/reports/profitloss">Profit Loss</a></li>
                            <li><a href="/admin/accounts/reports/trialbalance">Trial Balance</a></li>
                            <li><a href="/admin/salesboard">Sales Analysis</a></li>

                            <li><a href="/admin/hotel/reservation/reports/index">All Reports</a></li>

                            <li class="divider"></li>
                            <li class="dropdown-header">Restaurant</li>
                            <li><a href="/admin/hotel/openresturantoutlets">Open Outlet</a></li>
                            <li><a href="/admin/recipe">Recipes</a></li>
                            <li><a href="/admin/orders/todaypos/payments">Todays Payment</a></li>
                            <li><a href="/admin/orders/todaypos/invoice">Todays Invoices</a></li>
                            <li><a href="/admin/edm/order">EDM - Free Dines</a></li>

                        </ul>
                    </li>
                    <li class="col-sm-3">
                        <ul>
                            <li class="dropdown-header">Accounting</li>
                            <li><a href="/admin/entries">Voucher Entry</a></li>
                            <li><a href="/admin/bank">Income</a></li>
                            <li><a href="/admin/expenses">Expense</a></li>
                            <li><a href="/admin/cash_in_out">Day Book</a></li>
                            <li><a href="/admin/reconciliations">Reconcile</a></li>
                            <li><a href="/admin/chartofaccounts">COA</a></li>


                            <li class="divider"></li>
                            <li class="dropdown-header">Front Office</li>
                            <li><a href="/admin/hotel/reservation/arrivals">Expected Arrivals</a></li>
                            <li><a href="/admin/hotel/reservation/departures">Expected Departures</a></li>

                            <li><a href="/admin/hotel/reservation/checkins">Checkins</a></li>
                            <li><a href="/admin/hotel/reservation/checkouts">Checkouts</a></li>
                            <li><a href="/admin/hotel/guestlist">Inhouse</a></li>

                        </ul>
                    </li>
                    <li class="col-sm-3">
                        <ul>
                            <li class="dropdown-header">Inventory</li>

                            <li><a href="/admin/products">Product Items</a></li>
                            <li><a href="/admin/requisition">Requisition</a></li>
                            <li><a href="/admin/location/stocktransfer">Transfer Stocks</a></li>
                            <li><a href="/admin/product/stock_adjustment">Stock Adjustment</a></li>
                            <li><a href="/admin/product/statement">Stock Ledger</a></li>
                            <li><a href="/admin/grn">GRN</a></li>

                            <li class="divider"></li>
                            <li class="dropdown-header">Sales</li>
                            <li><a href="/admin/sales-book">Sales Book</a></li>
                            <li><a href="/admin/orders/allpos/invoice">Tax Invoices</a></li>
                            <li><a href="/admin/orders/allpos/payments">Receipts</a></li>
                            <li><a href="/admin/debtors_lists">Debtors List</a></li>
                            <li><a href="/admin/orders/returnpos">Sales Return</a></li>


                        </ul>
                    </li>
                    <li class="col-sm-3">
                        <ul>
                            <li class="dropdown-header">Front Office Cash</li>
                            <li><a href="/admin/hotel/cashiering/billing">Billing</a></li>
                            <li><a href="/admin/front-cash?revenue=deposit">Deposit</a></li>

                            <li><a href="/admin/front-cash">Front Cash</a></li>
                            <li><a href="#">Front Expense</a></li>
                            <li><a href="/admin/manual-currency">Daily Currency</a></li>
                            <li><a href="/admin/hotel/irdposting/index">Invoice List</a></li>



                            <li class="divider"></li>
                            <li class="dropdown-header">Purchase</li>
                            <li><a href="/admin/purchase-book">Purchase Book</a></li>
                            <li><a href="/admin/purchase?type=purchase_orders">PO</a></li>
                            <li><a href="/admin/purchase?type=bills">Bills</a></li>
                            <li><a href="/admin/purchase/paymentslist">Payments</a></li>
                            <li><a href="/admin/creditors_lists">Creditors List</a></li>

                        </ul>
                    </li>



                </ul>

            </li>
                <?php
                $business_date=\App\Models\BusinessDate::first();
                ?>

                <li><a href="#">
                        @if(!$business_date||($business_date&&$business_date->business_date!=date('Y-m-d')))
                            <span data-toggle="tooltip" data-placement="bottom" title="Business Date does not match with realtime date. Actual Date is {{date('Y/m/d')}}. Order will post with business date">
                        <i class="fa fa-exclamation-triangle alert-icon" style="color: yellow;font-size: 15px"></i>
                            {{$business_date?date('Y/m/d',strtotime($business_date->business_date)):'Date Not Set'}}</span>
                        @else
                            <span data-toggle="tooltip" data-placement="bottom" title="Business Date">
                            <i class="fa fa-check-circle" style="font-size: 17px;color: greenyellow;"></i>
                            {{date('Y/m/d',strtotime($business_date->business_date))}}
                            </span>
                        @endif
                        </a></li>
        </ul>
        <div class="navbar-custom-menu DeskView">
            <ul class="nav navbar-nav">

                <li>
                    <a href="/home" class=""><i class="fa fa-home"></i> Home </a>
                </li>
                @if (Auth::check())
                    @if(!\Auth::user()->hasRole('pos-user') && !\Auth::user()->hasRole('pos-manager') && !\Auth::user()->hasRole('waiter'))
                        <li>
                            <a href="/admin/talk"> Chat </a>
                        </li>
                    @endif



                <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->

                            @if(\Auth::user()->image)
                                <img src="/images/profiles/{{\Auth::user()->image}}" class="user-image" alt="User Image" />
                            @else
                                <img src="{{ Gravatar::get(Auth::user()->email , 'tiny') }}" class="user-image" alt="User Image" />
                        @endif


                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->username }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                @if(\Auth::user()->image)
                                    <img src="/images/profiles/{{\Auth::user()->image}}" wi class="img-circle" alt="User Image" />
                                @else
                                    <img src="{{ Gravatar::get(Auth::user()->email , 'tiny') }}" class="img-circle" alt="User Image" />
                                @endif
                                <p>
                                    {{ Auth::user()->full_name }}
                                    <small>
                                        {{ PayrollHelper::getDepartment(\Auth::user()->departments_id) }},
                                        {{ PayrollHelper::getDesignation(\Auth::user()->designations_id) }}
                                    </small>
                                </p>



                            </li>

                        @if( \Config::get('settings.app_extended_user_menu') )
                            <!-- Menu Body -->
                                <!--      <li class="user-body">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">My PIS</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </li> -->
                        @endif

                        <!-- Menu Footer-->
                            <li class="user-footer">

                                @if ( \Config::get('settings.app_user_profile_link') )
                                    <div class="pull-left">
                                        {!! link_to_route('user.profile', 'Profile', [], ['class' => "btn btn-default btn-flat"]) !!}
                                    </div>
                                @endif

                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                        Logout
                                    </a>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                @else
                    <li>{!! link_to_route('login', 'Sign in') !!}</li>
                    @if (\Config::get('settings.app_allow_registration'))
                        <li>{!! link_to_route('register', 'Register') !!}</li>
                    @endif

                @endif


            </ul>
        </div>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu MobileView">


            <ul class="nav navbar-nav">

                @if (Auth::check())


                    <li class="dropdown messages-menu">
                        <div class="margin">
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-plus"></i> New</button>
                                <button type="button" class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/hotel/reservation-create"><i class="fa fa-calendar"></i> New Reservation / Walkin</a></li>
                                    <li><a href="/admin/pos/dashboard"><i class="fa fa-user"> </i> POS Transaction</a></li>
                                    <li><a href="/admin/clients/create?relation_type=agent"><i class="fa fa-black-tie"></i> New Agent</a></li>

                                    <li><a href="/admin/addevent"> <i class="fa  fa-send-o"></i> New Banquet Booking</a></li>

                                    <li><a href="/admin/tasks"> <i class="fa fa-bullseye"></i> New HK Task</a></li>


                                </ul>
                            </div>
                        </div>
                    </li>

                    @if ( \Config::get('settings.app_context_help_area') && (isset($context_help_area)))
                        {!! $context_help_area !!}
                    @endif

                    @if(\Auth::user()->hasRole(['marketing-manager','admins']))

                        @if(\Config::get('settings.app_notification_area'))
                        <!-- Messages: style can be found in dropdown.less-->




                            <!-- Tasks Menu -->
                            <li style="margin-left: -15px;">
                                <a href="/admin/privilege-card" ><i class="material-icons">card_giftcard</i></a>
                            </li>

                            <li style="margin-left: -15px;">
                                <a href="/admin/hotel/guestlist" ><i class="material-icons">people</i></a>
                            </li>

                            <li style="margin-left: -15px;">
                                <a href="/admin/hotel/guestlist" ><i class="material-icons">hotel</i></a>
                            </li>

                            <li style="margin-left: -15px;">
                                <a href="/admin/stickynote" ><i class="material-icons">sticky_note_2</i></a>
                            </li>


                        @endif

                    @endif
                <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->

                            @if(\Auth::user()->image)
                                <img src="/images/profiles/{{\Auth::user()->image}}" class="user-image" alt="User Image" />
                            @else
                                <img src="{{ Gravatar::get(Auth::user()->email , 'tiny') }}" class="user-image" alt="User Image" />
                        @endif

                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->username }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                @if(\Auth::user()->image)
                                    <img src="/images/profiles/{{\Auth::user()->image}}" wi class="img-circle" alt="User Image" />
                                @else
                                    <img src="{{ Gravatar::get(Auth::user()->email , 'tiny') }}" class="img-circle" alt="User Image" />
                                @endif
                                <p>
                                    {{ Auth::user()->full_name }}
                                    <small>
                                        {{ PayrollHelper::getDepartment(\Auth::user()->departments_id) }},
                                        {{ PayrollHelper::getDesignation(\Auth::user()->designations_id) }}
                                    </small>
                                </p>



                            </li>

                        @if( \Config::get('settings.app_extended_user_menu') )
                            <!-- Menu Body -->
                                <!--      <li class="user-body">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">My PIS</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </li> -->
                        @endif

                        <!-- Menu Footer-->
                            <li class="user-footer">

                                @if ( \Config::get('settings.app_user_profile_link') )
                                    <div class="pull-left">
                                        {!! link_to_route('user.profile', 'Profile', [], ['class' => "btn btn-default btn-flat"]) !!}
                                    </div>
                                @endif

                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                        Logout
                                    </a>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    @if ( \Config::get('settings.app_right_sidebar') )
                    <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    @endif
                @else
                    <li>{!! link_to_route('login', 'Sign in') !!}</li>
                    @if (\Config::get('settings.app_allow_registration'))
                        <li>{!! link_to_route('register', 'Register') !!}</li>
                    @endif
                @endif
            </ul>
        </div>
    </nav>
</header>

<script type="text/javascript">

    $(document).on('change', '#header_fiscal_year', function() {
        $('#session-fiscal-year').submit()
    })
        $(document).on('change', '#header_org_id', function() {
        var id = $('#header_user_id').val();
        var org_id = $(this).val();

        $.post("/admin/ajax_org", {
                id: id
                , org_id: org_id
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    location.reload();
                else
                    $("#header_ajax_status").after("<span style='color:red;' id='header_status_update'>Problem in updating org; Please try again.</span>");

                $('#header_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });
    });
    $(document).ready(function() {
        $('#chatpopover').click(function() {
            var e = $(this);
            if (!e.attr('aria-describedby')) {

                $.get(e.data('poload'), function(d) {
                    e.popover({
                        container: 'body'
                        , content: d.message
                    }).popover('show');

                });

            } else {
                e.popover('destroy');
                $('.popover').html('');
                e.attr('aria-describedby', undefined);
            }
        });
    });

    $('.sidebar-toggle').click(function() {


        if ($('body').hasClass('sidebar-collapse')) {

            //user has removed collapse
            document.cookie = "sidebar_collapse=;path=/"

        } else {

            console.log("OK");
            document.cookie = "sidebar_collapse=true;path=/"
        }

        //console.log(document.cookie);

    });

</script>

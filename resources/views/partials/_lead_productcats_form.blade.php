<?php $readonly = ($ProductCategory->isEditable())? '' : 'readonly'; ?>

<div class="content">
<div class="row">
    <div class="form-group col-md-6">
        <label> Category Name</label>
        {!! Form::text('name', null, ['class' => 'form-control', $readonly]) !!}
    </div>
    <div class="col-md-6 form-group">
        <label class="control-label">Menu</label>
        <select name="menu_id" class="form-control searchable" >
            <option value="">Select Menu</option>
            @foreach($menus as $o)
                <option value="{{$o->id}}" {{$o->id==$ProductCategory->menu_id?'selected':''}}>{{ucfirst($o->menu_name)}}</option>
            @endforeach
        </select>
    </div>
</div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                {!! '<input type="hidden" name="enabled" value="0">' !!}
                {!! Form::checkbox('enabled', '1', $ProductCategory->enabled) !!} {{ trans('general.status.enabled') }}
            </label>
        </div>
    </div>


</div><!-- /.content -->




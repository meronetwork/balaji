<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Enquiry Form::</title>
    <meta name="description" content="" />
	<meta name="keywords" content="" />
	<link href="{{ asset ("/img/favicon.png") }}" rel="icon">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Application CSS-->
    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">    
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>    
<style type="text/css">
    .form-control{
        position: relative;
    }
    .bootstrap-datetimepicker-widget {position:absolute; background:#fff !important; z-index: 99; width:100%;}
</style>
  <!-- Body -->
  <body>
  	
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tbc-form">
                    <header class="row">
                        <div class="col-md-12">

                           <div class="col-md-6"> <a href="#">
                            <img src="{!!env('APP_URL') !!}{!! '/org/'. $organization->logo !!}" alt="" class="img-responsive" style="max-width: 200px;">
                        </a> </div>
                           <div class="col-md-6">  <h2 class="pull-right"> Online Enquiry Form <i class="fas fa-headset"></i> </h2> </div>
                        </div>
                        
                    </header> <!-- /header -->
                    <div class="clearfix"></div>
                    
                    <p class="lead">Please fill in this enquiry form completely as this will help speed up your enquiry.</p>
                    
                    @foreach($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p> 
                    @endforeach
        
                    @if(Session::has('msg'))
                    <br/>
                    <p id="ajax-success-msg" class="alert alert-success">{{Session::get('msg')}}</p>
                    @endif
            
                    {!! Form::open( ['route' => 'post_enquiry_events', 'id' => 'form_edit_lead'] ) !!} 
                    <div class="top">
                        
                        <div class="row">   
                              <div class="col-md-3">
                                <label for="inputEmail3" class="control-label">
                                  Event  Type
                                </label>
                                <div class="form-group">
                                    <select name="event_type" class="form-control select2 searchable">
                                        @foreach($events_type as $e)
                                        <option value="{{$e}}">{{ucfirst(trans($e))}}</option>
                                        @endforeach
                                    </select>
                                </div>
                             </div>                        
                        </div>
                    </div> <!-- /top -->
                    <div class="personal"> 
                        <label class="title bg-title">2. Personal  Details</label>
                     
                            <div class="row">
                                   <div class="col-md-4">
                                        <label for="inputEmail3" class=" control-label">
                                            Event Name
                                        </label>
                                        <div class="form-group">
                                              <input type="text" name="event_name" placeholder="Event Name" id="event_name" class="form-control input-sm" required>    
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                            <label for="inputEmail3" class=" control-label">
                                                Venue
                                            </label>
                                            <div class="form-group">
                                               <select name="venue_id" class="form-control">
                                                   @foreach($venue as $v)
                                                      <option value="{{$v->id}}">{{ucfirst(trans($v->venue_name))}}</option>
                                                    @endforeach
                                               </select>   
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                            <label for="inputEmail3" class=" control-label">
                                                Conatct No
                                            </label>
                                            <div class="form-group">
                                                 <input type="text" name="contact_num" placeholder="Contact No" id="company_id" class="form-control input-sm" required>
                                            </div>
                                    </div> 
                            </div>

                            <div class="row">
                                   <div class="col-md-4">
                                        <label for="inputEmail3" class="control-label">
                                            Start Date Time
                                        </label>
                                        <div class="form-group">
                                              <input type="text" name="event_start_date" placeholder="Start Date Time" id="event_start_date" class="form-control input-sm">
                                        </div>
                                    </div> 
                                    
                                    <div class="col-md-4">
                                            <label for="inputEmail3" class=" control-label">
                                                End Date Time
                                            </label>
                                            <div class="form-group">
                                               <input type="text" name="event_end_date" placeholder=" End Date Time" id="event_end_date" class="form-control input-sm">   
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="inputEmail3" class="control-label">
                                            Participants
                                        </label>
                                        <div class="form-group">
                                             <input type="number" name="num_participants" placeholder="Participants" id="num_participants" class="form-control input-sm" required>
                                        </div>
                                    </div> 
                            </div>


                             <div class="row">
                                   <div class="col-md-4">
                                        <label for="inputEmail3" class="control-label">
                                          Person  Name
                                        </label>
                                        <div class="form-group">
                                              <input type="text" name="person_name" placeholder="Person Name" id="person_name" class="form-control input-sm">
                                        </div>
                                    </div> 
                                    
                                    <div class="col-md-4">
                                            <label for="inputEmail3" class=" control-label">
                                                Email
                                            </label>
                                            <div class="form-group">
                                               <input type="email" name="email" placeholder="Email" id="email" class="form-control input-sm">   
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="inputEmail3" class="control-label">
                                            Landline
                                        </label>
                                        <div class="form-group">
                                             <input type="text" name="landline" placeholder="Landline" id="landline" class="form-control input-sm" required>
                                        </div>
                                    </div> 
                            </div>

                             <div class="row">
                                   <div class="col-md-4">
                                        <label for="inputEmail3" class="control-label">
                                          Address
                                        </label>
                                        <div class="form-group">
                                              <input type="text" name="address" placeholder="Address" id="address" class="form-control input-sm">
                                        </div>
                                    </div> 
                                    
                                    <div class="col-md-4">
                                            <label for="inputEmail3" class=" control-label">
                                                City
                                            </label>
                                            <div class="form-group">
                                               <input type="text" name="city" placeholder="City" id="city" class="form-control input-sm">   
                                            </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="inputEmail3" class="control-label">
                                            Post Code
                                        </label>
                                        <div class="form-group">
                                             <input type="text" name="post_code" placeholder="Post Code" id="post_code" class="form-control input-sm" required>
                                        </div>
                                    </div> 
                            </div>
                                   

                    </div> <!-- /Personal -->

                    <h3>Requirements</h3>

                    <div class="row">

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="white board" name="requirements[]">White Board</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="sofa" name="requirements[]">Sofa</label>
                          </div>  

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="chair" name="requirements[]">Chair</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="podium" name="requirements[]">Podium</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="mike" name="requirements[]">Mike</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="panas" name="requirements[]">Panas</label>
                          </div>

                    </div>

                     <div class="row">
                   <div class="col-md-12">
                     
                        <textarea class="form-control" name="schedule" id="schedule" placeholder="Write Schedule" style="display: none;">
                              <table border="1" cellpadding="1" cellspacing="1" style="width:500px">
                                  <tbody>
                                      <tr>
                                        <td>Time</td>
                                        <td>Function Activities</td>
                                        <td>Rep/Host</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                  </tbody>
                              </table>
                              <p>&nbsp;</p>
                        </textarea>
                    </div>
               </div> 

                 
                    <hr style="margin:2px 0; border:none;">  
                    <div class="experience">                        
                        <div class="table-responsive"> 
                            <div class="form-group">
                              <label for="comment">Please Write Menu:</label>
                              <textarea class="form-control" rows="3" name="menu_items" id="menu_items"></textarea>
                            </div>                       
                        </div>
                    </div> <!-- /Experience -->

                     <hr style="margin:2px 0; border:none;">
                    <div class="experience">                        
                        <div class="table-responsive"> 
                            <div class="form-group">
                              <label for="comment">Please write Set up:</label>
                              <textarea class="form-control" rows="3" name="set_up" id="set_up"></textarea>
                            </div>                       
                        </div>
                    </div> <!-- /Experience -->

                     <hr style="margin:2px 0; border:none;">
                    <div class="experience">                        
                        <div class="table-responsive"> 
                            <div class="form-group">
                              <label for="comment">Please write the Description:</label>
                              <textarea class="form-control" rows="3" name="comments" id="comment"></textarea>
                            </div>                       
                        </div>
                    </div> <!-- /Experience -->

                    <!-- <input type="checkbox" id="accept" name="accept"> Send me New Offers and Newsletters.<br><br> -->
                    <input type="hidden" id="communication_id" name="communication_id" value="{{ $communication_id }}"> <br><br>
                    <input type="hidden" name="source" value="Online" id="source">    
                    <input type="hidden" id="org_id" name="org_id" value="1">
                    <input type="submit" class="btn btn-success btn-lg" name="submit" id="submit" value="Submit Enquiry">
                    </form>
                </div>
            </div>
        </div>
    </div>
  
    <!-- jQuery library -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

   

    <script type="text/javascript">
        $('#event_start_date').on('click',function(){
              $('#event_start_date').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    inline: true
                });
         
        });
        $('#event_end_date').on('click',function(){
     
             $('#event_end_date').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss',
                    inline: true
                });
        });
          
    </script>
  
  </body>
</html>

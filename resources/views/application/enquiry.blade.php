<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ env('APP_NAME')}} Enquiry Form::</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link href="{{ asset ("/img/favicon.png") }}" rel="icon">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Application CSS-->
    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />


    <link href="http://hotel.local/bower_components/admin-lte/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons 4.4.0 -->
    <link href="http://hotel.local/bower_components/admin-lte/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.1 -->
    <link href="http://hotel.local/bower_components/admin-lte/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="http://hotel.local/bower_components/admin-lte/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

</head>

<!-- Body -->
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tbc-form">
                    <header class="row">
                        <div class="col-md-12">

                            <div class="col-md-6"> <a href="#">
                                    <img src="{!!env('APP_URL') !!}{!! '/org/'. $organization->logo !!}" alt="{{env('APP_NAME')}}" class="img-responsive">
                                </a> </div>
                            <div class="col-md-6">
                                <h2 class="pull-right"> Online Booking Engine <i class="fas fa-headset"></i> </h2>
                            </div>
                        </div>

                    </header> <!-- /header -->
                    <div class="clearfix"></div>

                    <p class="lead">Pay in advance to make a 100% booking confirmation.</p>


                    @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach

                    @if(Session::has('msg'))
                    <br />
                    <p id="ajax-success-msg" class="alert alert-success">{{Session::get('msg')}}</p>
                    @endif

                    {!! Form::open( ['route' => 'post_reservation', 'id' => 'form_edit_lead'] ) !!}
                    <div class="top">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Guest Type
                                </label>
                                <div class="form-group">
                                    {!! Form::select('guest_type',['TOU'=>'Tourist','BUS'=>'Business Travellers','FAM'=>'Families','STA'=>'Hotel Staffs','DEl'=>'Delegates','VIP'=>'VIP','COR'=>'Corporate','GOV'=>'Government','FIT'=>"FIT",'COM'=>"Complementry"], old('guest_type'), ['class' => 'form-control searchable select2 input-sm'] )!!}
                                </div>
                            </div>

                        </div>
                    </div> <!-- /top -->
                    <div class="personal">
                        <label class="title bg-title">2. Personal or Company Details</label>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Title
                                </label>
                                <div class="form-group">
                                    <select name="title" class="form-control searchable select2 input-sm" id="title">
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Miss">Miss</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Guest Name
                                </label>
                                <div class="form-group">
                                    <input type="text" name="guest_name" placeholder="Guest Name" id="event_name" class="form-control input-sm" required="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Gender
                                </label>
                                <div class="form-group">
                                    <select name="gender" class="form-control   input-sm">
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Email
                                </label>
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Email.." size="6" value="{{old('email')}}" class="form-control input-sm">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Phone
                                </label>
                                <div class="form-group">
                                    <input type="number" name="phone" placeholder="Phone Number.." size="6" value="{{old('phone')}}" class="form-control input-sm">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Mobile
                                </label>
                                <div class="form-group">
                                    <input type="number" name="mobile" placeholder="Mobile Number.." size="6" value="{{old('mobile')}}" class="form-control input-sm">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Smoking
                                </label>
                                <div class="form-group">
                                    <select name="smoking" class="form-control input-sm">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div> <!-- /Personal -->

                    <div class="personal">
                        <label class="title bg-title">3. Dates and Availability</label>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Check in
                                </label>
                                <div class="form-group">
                                    <input required="" type="text" class="form-control occupied_date_from input-sm check_in" value="{{\Carbon\Carbon::now()->toDateString()}}" name="check_in" id="check_in" readonly="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Check Out
                                </label>
                                <div class="form-group">
                                    <input required="" type="text" class="form-control occupied_date_from input-sm" value="{{\Carbon\Carbon::now()->addDays(1)->toDateString()}}" name="check_out" id="check_out" readonly="">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Check in Time
                                </label>
                                <div class="form-group">
                                    <input type="text" class="form-control occupied_date_from input-sm" value="{{\Carbon\Carbon::now()}}" name="check_in_time" id="check_in_time">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Check Out Time
                                </label>
                                <div class="form-group">
                                    <input type="text" class="form-control occupied_date_from input-sm" value="" name="check_out_time" id="check_out_time">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Arrival Time
                                </label>
                                <div class="form-group">
                                    <input type="text" class="form-control occupied_date_from input-sm" value="" name="arrival_time" id="arrival_time">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Arrival Flight Detail
                                </label>
                                <div class="form-group">
                                    <input type="text" class="form-control occupied_date_from input-sm" value="" name="flight_no">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Arrival Flight Time
                                </label>
                                <div class="form-group">
                                    <input type="text" class="form-control occupied_date_from input-sm" value="" name="arrival_flight_time" id="arrival_flight_time">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Occupancy
                                </label>
                                <div class="form-group">
                                    <input type="number" class="form-control occupied_date_from input-sm" value="1" name="occupancy" id="occupancy">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Booked By
                                </label>
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" value="" name="booked_by" id="booked_by">
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="personal">
                        <label class="title bg-title">4. Stay Information</label>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Room
                                </label>
                                <div class="form-group">
                                    <select name="room_num" class="form-control searchable select2 input-sm" id="room_num" required="">
                                        <option value="">Select room</option>
                                        @foreach($room as $rm)
                                        <option value="{{$rm['room_number']}}">{{ucfirst(trans($rm['room_number']))}} ({{$rm['room_name']}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Rate Plan
                                </label>
                                <div class="form-group">
                                    <select name="rate_id" class="form-control searchable select2 input-sm" id="room_plan" required="">
                                        <option value="">Select Rate Plan</option>
                                        application/reservation-getroom-plan
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Number Of Days
                                </label>
                                <div class="form-group">
                                    <input type="number" name="number_of_days" placeholder="days.." id="number_of_days" value="1" class="form-control input-sm" readonly="">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="personal">
                        <label class="title bg-title">5. Other Information</label>

                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Nationality
                                </label>
                                <div class="form-group">
                                    {!! Form::select('nationality', ["Afghanistan"=>"Afghanistan",
                                    "Albania"=>"Albania",
                                    "Algeria"=>"Algeria",
                                    "American Samoa"=>"American Samoa",
                                    "Andorra"=>"Andorra",
                                    "Angola"=>"Angola",
                                    "Anguilla"=>"Anguilla",
                                    "Antigu"=>"Antigu",
                                    "Argentina"=>"Argentina",
                                    "Armenia"=>"Armenia",
                                    "Aruba"=>"Aruba",
                                    "Australia"=>"Australia",
                                    "Austria"=>"Austria",
                                    "Azerbaijan"=>"Azerbaijan",
                                    "Bahamas"=>"Bahamas",
                                    "Bahrain"=>"Bahrain",
                                    "Bangladesh"=>"Bangladesh",
                                    "Barbados"=>"Barbados",
                                    "Belarus"=>"Belarus",
                                    "Belgium"=>"Belgium",
                                    "Belize"=>"Belize",
                                    "Benin"=>"Benin",
                                    "Bermuda"=>"Bermuda",
                                    "Bhutan"=>"Bhutan",
                                    "Bolivia"=>"Bolivia",
                                    "Bosnia And Hercegovina"=>"Bosnia And Hercegovina",
                                    "Botswana"=>"Botswana",
                                    "Brazil"=>"Brazil",
                                    "British Virgin Islands"=>"British Virgin Islands",
                                    "Brunei"=>"Brunei",
                                    "Bulgaria"=>"Bulgaria",
                                    "Burkina Faso"=>"Burkina Faso",
                                    "Burundi"=>"Burundi",
                                    "Cambodia"=>"Cambodia",
                                    "Cameroon"=>"Cameroon",
                                    "Canada"=>"Canada",
                                    "Capeverde"=>"Capeverde",
                                    "Cayman Islands"=>"Cayman Islands",
                                    "Central African Republic"=>"Central African Republic",
                                    "Chad"=>"Chad",
                                    "Chile"=>"Chile",
                                    "China"=>"China",
                                    "Colombia"=>"Colombia",
                                    "Comoros"=>"Comoros",
                                    "Congo"=>"Congo",
                                    "Costa Rica"=>"Costa Rica",
                                    "Croatia"=>"Croatia",
                                    "Cuba"=>"Cuba",
                                    "Cyprus"=>"Cyprus",
                                    "Czech Republic"=>"Czech Republic",
                                    "Denmark"=>"Denmark",
                                    "Djibouti"=>"Djibouti",
                                    "Dominca"=>"Dominca",
                                    "Dominican Republic"=>"Dominican Republic",
                                    "Ecuador"=>"Ecuador",
                                    "Egypt"=>"Egypt",
                                    "El Salvador"=>"El Salvador",
                                    "Equatorial Guinea"=>"Equatorial Guinea",
                                    "Eritrea"=>"Eritrea",
                                    "Estonia"=>"Estonia",
                                    "Ethiopia"=>"Ethiopia",
                                    "Falkland Islands"=>"Falkland Islands",
                                    "Fiji"=>"Fiji",
                                    "Finland"=>"Finland",
                                    "France"=>"France",
                                    "Gabon"=>"Gabon",
                                    "Gambia"=>"Gambia",
                                    "Georgia"=>"Georgia",
                                    "Germany"=>"Germany",
                                    "Ghana"=>"Ghana",
                                    "Greece"=>"Greece",
                                    "Greenland"=>"Greenland",
                                    "Grenada"=>"Grenada",
                                    "Guam"=>"Guam",
                                    "Guatemala"=>"Guatemala",
                                    "Guinea"=>"Guinea",
                                    "Guinea-bissau"=>"Guinea-bissau",
                                    "Guyana"=>"Guyana",
                                    "Haiti"=>"Haiti",
                                    "Honduras"=>"Honduras",
                                    "Hungary"=>"Hungary",
                                    "Iceland"=>"Iceland",
                                    "India"=>"India",
                                    "Indonesia"=>"Indonesia",
                                    "Iran"=>"Iran",
                                    "Iraq"=>"Iraq",
                                    "Ireland"=>"Ireland",
                                    "Israel"=>"Israel",
                                    "Italy"=>"Italy",
                                    "Jamaica"=>"Jamaica",
                                    "Japan"=>"Japan",
                                    "Jordan"=>"Jordan",
                                    "Kazakhstan"=>"Kazakhstan",
                                    "Kenya"=>"Kenya",
                                    "Kiribati"=>"Kiribati",
                                    "Kuwait"=>"Kuwait",
                                    "Laos"=>"Laos",
                                    "Latvia"=>"Latvia",
                                    "Lebanon"=>"Lebanon",
                                    "Lesotho"=>"Lesotho",
                                    "Liberia"=>"Liberia",
                                    "Libya"=>"Libya",
                                    "Liechtenstein"=>"Liechtenstein",
                                    "Lithuania"=>"Lithuania",
                                    "Luxembourg"=>"Luxembourg",
                                    "Macedonia"=>"Macedonia",
                                    "Madagascar"=>"Madagascar",
                                    "Malawi"=>"Malawi",
                                    "Malaysia"=>"Malaysia",
                                    "Maldives"=>"Maldives",
                                    "Mali"=>"Mali",
                                    "Malta"=>"Malta",
                                    "Marshall Islands"=>"Marshall Islands",
                                    "Mauritania"=>"Mauritania",
                                    "Mauritius"=>"Mauritius",
                                    "Mexico"=>"Mexico",
                                    "Micronesia"=>"Micronesia",
                                    "Moldova"=>"Moldova",
                                    "Monaco"=>"Monaco",
                                    "Mongolia"=>"Mongolia",
                                    "Morocco"=>"Morocco",
                                    "Mozambique"=>"Mozambique",
                                    "Myanmar"=>"Myanmar",
                                    "Namibia"=>"Namibia",
                                    "Nauru"=>"Nauru",
                                    "Nepal"=>"Nepal",
                                    "Netherlands"=>"Netherlands",
                                    "New Zealand"=>"New Zealand",
                                    "Nicaragua"=>"Nicaragua",
                                    "Niger"=>"Niger",
                                    "Nigeria"=>"Nigeria",
                                    "North Korea"=>"North Korea",
                                    "Norway"=>"Norway",
                                    "Oman"=>"Oman",
                                    "Pakistan"=>"Pakistan",
                                    "Palau"=>"Palau",
                                    "Panama"=>"Panama",
                                    "Papua New Guinea"=>"Papua New Guinea",
                                    "Paraguay"=>"Paraguay",
                                    "Peru"=>"Peru",
                                    "Philippines" => "Philippines",
                                    "Poland"=>"Poland",
                                    "Portugal"=>"Portugal",
                                    "Qatar"=>"Qatar",
                                    "Romania"=>"Romania",
                                    "Russia"=>"Russia",
                                    "Rwanda"=>"Rwanda",
                                    "San Marino"=>"San Marino",
                                    "Sao Tome And Principe"=>"Sao Tome And Principe",
                                    "Saudi Arabia"=>"Saudi Arabia",
                                    "Senegal"=>"Senegal",
                                    "Serbia"=>"Serbia",
                                    "Seychelles"=>"Seychelles",
                                    "Sierra Leone"=>"Sierra Leone",
                                    "Singapore"=>"Singapore",
                                    "Slovakia"=>"Slovakia",
                                    "Slovenia"=>"Slovenia",
                                    "Solomon Islands"=>"Solomon Islands",
                                    "Somalia"=>'Somalia',
                                    "outh Africa"=>"South Africa",
                                    "South Korea"=>"South Korea",
                                    "Spain"=>"Spain",
                                    "Sri Lanka"=>"Sri Lanka",
                                    "Sudan"=>"Sudan",
                                    "Suriname"=>"Suriname",
                                    "Swaziland"=>"Swaziland",
                                    "Sweden"=>"Sweden",
                                    "Switzerland"=>"Switzerland",
                                    "Syria"=>"Syria",
                                    "Taiwan"=>"Taiwan",
                                    "Tajikistan"=>"Tajikistan",
                                    "Tanzania"=>"Tanzania",
                                    "Thailand"=>"Thailand",
                                    "Togo"=>"Togo",
                                    "Tonga"=>"Tonga",
                                    "Trinidad And Tobago"=>"Trinidad And Tobago",
                                    "Tunisia"=>"Tunisia",
                                    "Turkey"=>"Turkey",
                                    "Turkmenistan"=>"Turkmenistan",
                                    "Tuvalu"=>"Tuvalu",
                                    "Uganda"=>"Uganda",
                                    "Ukraine"=>"Ukraine",
                                    "United Arab Emirates"=>"United Arab Emirates",
                                    "United Kingdom"=>"United Kingdom",
                                    "United States Of America"=>"United States Of America",
                                    "Uruguay"=>"Uruguay",
                                    "Uzbekistan"=>"Uzbekistan",
                                    "Vanuatu"=>"Vanuatu",
                                    "Venezuela"=>"Venezuela",
                                    "Viet Nam"=>"Viet Nam",
                                    "Zambia"=>"Zambia",
                                    "Zimbabwe"=>"Zimbabwe"],'Nepal', ['class' => 'form-control label-default']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Doc Type
                                </label>
                                <div class="form-group">
                                    <input type="number" name="doc_type_extra" placeholder="Doc type.." size="6" value="" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Doc Number
                                </label>
                                <div class="form-group">
                                    <input type="number" name="doc_num_extra" placeholder="Doc number.." size="6" value="" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="personal">
                        <label class="title bg-title">6.Payment Information</label>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="inputEmail3" class=" control-label">
                                    Payment Type
                                </label>
                                <div class="form-group">
                                    <select name="payment_type_id" class="form-control searchable select2 input-sm" id="payment_type_id" required="required">
                                        <option value="">Select Payment Type</option>
                                        @foreach($paymenttype as $pt)
                                        <option value="{{$pt->payment_type_id}}">{{ $pt->payment_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                    <hr style="margin:10px 0; border:none;">
                    <div class="experience">
                        <div class="table-responsive">
                            <div class="form-group">
                                <label for="comment">Remarks:</label>
                                <textarea type="text" name="remarks" placeholder="Write reservation remarks" id="remarks" class="form-control"></textarea>
                            </div>
                        </div>
                    </div> <!-- /Experience -->
                    <!-- <input type="checkbox" id="accept" name="accept"> Send me New Offers and Newsletters.<br><br> -->
                    <input type="hidden" id="reservation_type" name="reservation_type" value="8"> <br><br>
                    <input type="hidden" id="source_id" name="source_id" value="8">
                    <input type="hidden" id="reservation_status" name="reservation_status" value="2">
                    <input type="hidden" id="source_type" name="source_type" value="Online">
                    <input type="submit" class="btn btn-success btn-lg" name="submit" id="submit" value="Submit Enquiry">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery library ALTER TABLE `reservations` ADD `source_type` VARCHAR(255) NOT NULL AFTER `source_id`;-->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>


    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>



    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script type="text/javascript">
        $(function() {
            $('#check_in').datepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d'
                , sideBySide: true
                , minDate: 0


            });

            $('#check_out').datepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d'
                , sideBySide: true
                , minDate: 0
            });
            $('#cc_expiry_date').datepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d'
                , sideBySide: true
                , minDate: 0
            });
            $('#check_in_time').datetimepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                format: 'HH:mm'
                , sideBySide: true
            });
            $('#arrival_flight_time').datetimepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                format: 'HH:mm'
                , sideBySide: true
            });
            $('#check_out_time').datetimepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                format: 'HH:mm'
                , sideBySide: true

            });
            $('#arrival_time').datetimepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                format: 'YYYY-MM-DD HH:mm'
                , sideBySide: true
            });
        });

        function getroom(date) {
            $('#room_num').empty();
            $('#room_plan').empty();
            $('#room_num').append("<option value=''>Select room </option>");
            $.get('/reservation/checkdate/' + date, function(data, status) {
                console.log(data);
                for (let room of data) {
                    $('#room_num').append(("<option value=" + room.room_number + ">" + room.room_number + "(" + room.room_name + ")</option>"));
                }
            });
        }

        $("#check_in,#check_out").on("change", function(event) {

            var check_in = $('#check_in').val();
            var check_out = $('#check_out').val();
            var date1 = new Date(check_in);
            var date2 = new Date(check_out);
            var diffTime = Math.abs(date2 - date1);
            var days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            if (days == 0) days = 1;
            if (!isNaN(days)) {
                $('#number_of_days').val(days);
            }
            let target = event.target.id;
            if (target == 'check_in') {
                getroom(check_in);
            }
        });

        function roompan(id) {
            $('#room_plan').empty();
            $.get('/hotel/application/reservation-getroom-plan/' + id, function(data, status) {
                for (let plan of data) {
                    let total = Number(plan.rate) + Number(plan.breakfast_rate) + Number(plan.lunch_rate) + Number(plan.dinner_rate);
                    $('#room_plan').append(("<option value=" + plan.id + ">" + plan.package_name + "(" + total + ")</option>"));

                }
            });

        }

        $('#room_num').on('change', function() {


            roompan($(this).val());
        });

        $(document).ready(function() {
            $('#complementry_by').css('visibility', 'hidden');
            roompan($('#room_num').val());
            $('.searchable').select2();
        });

        $('#guest_id').on('change', function() {
            if ($(this).val()) {
                $('#guest_name input').val('');
                $('#guest_name').css('visibility', 'hidden');
                $('.guest_info').hide();
            } else {
                $('#guest_name').css('visibility', 'visible');
                $('.guest_info').show();
            }
        })

        $('#reservationform').on('submit', function() {
            if (!($("select[name=guest_id]").val()) && !($('input[name=guest_name]').val())) {
                alert("Please select a guest");
                return false;
            }
            return true;
        });


        $('#guest_type_id').on('change', function() {
            if ($(this).val() != 'COM') {
                $('#complementry_by').css('visibility', 'hidden');
                $('.payment_info').show();
            } else {

                $('#complementry_by').css('visibility', 'visible');
                $('#booked_by').attr("readonly", "readonly");
                $('.payment_info').hide();
                $('#payment_type_id').removeAttr("required", "required");

            }
        });

    </script>

    <script>
        (function() {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function() {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call(document.querySelectorAll('input.input__field')).forEach(function(inputEl) {
                // in case the input is already filled..
                if (inputEl.value.trim() !== '') {
                    classie.add(inputEl.parentNode, 'input--filled');
                }

                // events:
                inputEl.addEventListener('focus', onInputFocus);
                inputEl.addEventListener('blur', onInputBlur);
            });

            function onInputFocus(ev) {
                classie.add(ev.target.parentNode, 'input--filled');
            }

            function onInputBlur(ev) {
                if (ev.target.value.trim() === '') {
                    classie.remove(ev.target.parentNode, 'input--filled');
                }
            }

        })();

    </script>

</body>
</html>

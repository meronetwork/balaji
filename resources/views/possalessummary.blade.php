@extends('layouts.master')

@section('head_extra')
<!-- jVectorMap 1.2.2 -->
<link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />
<style>
	.filter_date { font-size:14px; }
</style>
@endsection
<style type="text/css">
	@media print {
    .pagebreak {
        clear: both;
        page-break-after: always;
        padding-top: 100px;
    }
    .dataTables_scrollBody {
    	position: relative;
  		height: auto !important;
    }
    .box{
    	background: white !important;
    }
    ::-webkit-scrollbar {
    display: none;
}
}
  @media print {

          @page { margin: 0; }

          body { margin: 1.6cm; }

        }
   .dataTables_scrollBody {
   position: relative;
  height: auto !important;
 
    }
</style>


@section('content')
 <div class='row'>
        <div class='col-md-12'>
                 <div class="row no-print">
                <div class="col-md-12" style="margin-top:5px;">
                	<form action="/admin/posSummaryAmount" method="GET">
                    <div class="filter form-inline" style="margin:0 30px 0 0;">

                        <input style="width:120px;" class="form-control input-sm" id="start_date" placeholder="Start Date" name="start_date" type="text"

                        value="{{ Request::get('start_date') ?? date('Y-m-d') }}" autocomplete="off">&nbsp;&nbsp;
                        <!-- <label for="end_date" style="float:left; padding-top:7px;">End Date: </label> -->
                        <input style="width:120px; display:inline-block;" class="form-control input-sm" id="end_date" placeholder="End Date" name="end_date" type="text" value="{{ Request::get('end_date') ?? date('Y-m-d') }}" autocomplete="off">&nbsp;&nbsp;           

                        <select class="form-control" name="outlet_id">
                        	<option value="">Select outlet</option>
                        	@foreach($outlets as $key=>$outl)
                        	<option value="{{$outl->id}}" @if(Request::get('outlet_id') == $outl->id) selected="" @endif>{{$outl->name}} ({{$outl->short_name}})</option>
                        	@endforeach
                        </select>             

                        <button class="btn btn-primary btn-sm" id="btn-submit-filter" type="submit">
                            <i class="fa fa-list"></i> Filter
                        </button>

                        <a href="/admin/posSummaryAmount" class="btn btn-danger btn-sm" id="btn-filter-clear">
                            <i class="fa fa-close"></i> Reset
                        </a>

                         <a href="#" class="btn btn-danger btn-sm">
                            <i class="fa fa-close"></i> Print
                        </a>
                    </div>
                    </form>
                </div>


           </div><br>
            <div class="row">
            	<div class="col-md-6">
		            
		            <div class="box box-default box-solid">
		                <div class="box-header">
		                    <h1 class="box-title" align="center">Amount Summary</h1>
		                </div>
		                <div class="box-body no-padding">
		                    <div id="bank_table_wrapper" class="dataTables_wrapper form-inline" role="grid">
		                    	<div class="row">
		                    		<div class="col-md-6 text-left"></div>
		                    		<div class="col-md-6 text-right"></div>
		                    	</div>
		                    	<div class="dataTables_scroll">
		                    		<div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px; width: 100%;">
		                    			<div class="dataTables_scrollHeadInner" style="width: 418px; padding-right: 17px;">
		                    				<table class="table table-bordered table-condensed dataTable" style="margin-left: 0px; width: 418px;">
		                    					<thead>
		                                            <tr role="row">
		                                            	<th style="display: none; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th>
		                                            	<th style="display: none; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th>
		                                            </tr>
		                                        </thead>
		                                    </table>
		                                </div>
		                            </div>
		                            <div class="dataTables_scrollBody" style="overflow: auto; height: 250px; width: 100%;">
		                            	<table class="table table-bordered table-condensed dataTable" id="bank_table" style="margin-left: 0px; width: 100%;">
		                            		<thead>
		                                         <tr role="row" style="height: 0px;">
		                                         	<th style="display: none; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th>
		                                         	<th style="display: none; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">	
		                                         	</th>
		                                         </tr>
		                                    </thead>  
		                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
		                                    	
		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Covers</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong> &nbsp;&nbsp;{{ number_format($amountSummary['coverCounts'],0) }}</strong></td>
		                                    	</tr>
		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Sales bill count</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong> &nbsp;&nbsp;{{ number_format($amountSummary['totalSalesCount'],0) }}</strong></td>
		                                    	</tr>

		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "><i class="fa fa-building"></i>Bill Count</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong>
		                                    		 &nbsp;&nbsp;{{ number_format($amountSummary['netSalesCount'],0) }}</strong></td>
		                                    	</tr>
		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Total Discount</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong>
		                                    		{{env('APP_CURRENCY')}} {{ number_format($amountSummary['totalDiscount'],2) }}</strong></td>
		                                    	</tr>
		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Total Service Charge</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong>
		                                    		{{env('APP_CURRENCY')}} {{ number_format($amountSummary['service_charge'],2) }}</strong></td>
		                                    	</tr>
		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Total Taxable Amount</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong>
		                                    		{{env('APP_CURRENCY')}} {{ number_format($amountSummary['taxable_amount'],2) }}</strong></td>
		                                    	</tr>
		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Total Tax</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong>
		                                    		{{env('APP_CURRENCY')}} {{ number_format($amountSummary['tax_amount'],2) }}</strong></td>
		                                    	</tr>

		                                    	<tr  class="odd">
		                                    		<td style="font-size: 16.5px" class=" "> <i class="fa fa-building"></i>Total Amount</td>
		                                    		<td style="font-size: 16.5px"class=" "><strong>
		                                    		{{env('APP_CURRENCY')}} {{ number_format($amountSummary['total_amount'],2) }}</strong></td>
		                                    	</tr>
		                                    


		                                    	
		                                    	
		                                    </tbody>
		                                </table>
		                            </div>
		                        </div>

		                        <div class="row">
		                        	<div class="col-md-6 text-left"></div>
		                        	<div class="col-md-6 text-right"></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

		             <div class="box box-solid box-danger">
		                <div class="box-header">
		                    <h1 class="box-title" align="center">Total Payments By groups</h1>
		                </div>
		                <div class="box-body no-padding">
		                    <div id="bank_table_wrapper" class="dataTables_wrapper form-inline" role="grid">
		                    	<div class="row">
		                    		<div class="col-md-6 text-left"></div>
		                    		<div class="col-md-6 text-right"></div>
		                    	</div>
		                    	<div class="dataTables_scroll">
		                    		<div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px; width: 100%;">
		                    			<div class="dataTables_scrollHeadInner" style="width: 418px; padding-right: 17px;">
		                    				<table class="table table-bordered table-condensed dataTable" style="margin-left: 0px; width: 418px;">
		                    					<thead>
		                                            <tr role="row">
		                                            	<th style="display: none; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th>
		                                            	<th style="display: none; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th>
		                                            </tr>

		                                    </table>
		                                </div>
		                            </div>
		                            <div class="dataTables_scrollBody" style="overflow: auto; height: 250px; width: 100%;">
		                            	<table class="table table-bordered table-condensed dataTable" id="bank_table" style="margin-left: 0px; width: 100%;">
		                            		<thead>
		                                         <tr role="row" style="height: 0px;">
		                                         	<th style="display: none; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th>
		                                         	<th style="display: none; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px; width: 0px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">	
		                                         	</th>
		                                         </tr>
		                                    </thead>  

             <tbody role="alert" aria-live="polite" aria-relevant="all">
            	
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total Cash</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['cash'] }}
                      
                    </strong></td>
            	</tr>

            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by City Ledger</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['city-ledger'] }}
                      
                    </strong></td>
            	</tr>

            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Credit card</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['credit-cards'] }}
                      
                    </strong></td>
            	</tr>

            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Check</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['check'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Travel agent</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['travel-agent'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Complementry</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['complementry'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Staff</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['staff'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Room</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['room'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by e-sewa</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['e-sewa'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by MNP</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['mnp'] }}
                      
                    </strong></td>
            	</tr>
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by EDM</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['edm'] }}
                      
                    </strong></td>
            	</tr>
            	
            	<tr  class="odd">
            		<td style="font-size: 16.5px; vertical-align: middle;" class=" "> <i class="fa fa-building"></i>Total by Others</td>
            		<td style="font-size: 16.5px" class=" "><strong>
                  	{{env('APP_CURRENCY')}} 	{{ $paid_by_total['others'] }}
                      
                    </strong></td>
            	</tr>
     
            </tbody>
		                                </table>
		                            </div>
		                        </div>

		                        <div class="row">
		                        	<div class="col-md-6 text-left"></div>
		                        	<div class="col-md-6 text-right"></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

               </div>
               <div class="pagebreak"></div>
                <div class="col-md-6">
                    <div class="box box-info box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Balance Summary By Groups</h3>
                           
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pad">
                                        <!-- get line graph here -->
                                        <div id="amountSummary" width="300" height="250"></div>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div> <!-- col -->


               
            </div><!-- row -->

        
    </div>
</div>
  <script src="{{ asset ("/bower_components/highcharts/highcharts.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/highcharts/funnel.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/highcharts/highcharts-3d.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/highcharts/exporting.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/highcharts/export-data.js") }}" type="text/javascript"></script>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    	

    	// Radialize the colors
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
Highcharts.chart('amountSummary', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '{{env('APP_COMPANY')}} Sales By Groups'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                connectorColor: 'silver'
            }
        }
    },
    credits: false,
    series: [{
        name: 'Amount Groups',
        data: [
            { name: 'Cash', y: {{$paid_by_total['cash']}} },
            { name: 'City Ledger', y: {{$paid_by_total['city-ledger']}} },
            { name: 'Credit Cards', y: {{$paid_by_total['credit-cards']}} },
            { name: 'Check', y: {{$paid_by_total['check']}} },
            { name: 'Travel Agent', y: {{$paid_by_total['travel-agent']}} },
            { name: 'Complementry', y: {{$paid_by_total['complementry']}} },
            { name: 'Staff', y: {{$paid_by_total['staff']}} },
            { name: 'Room', y: {{$paid_by_total['room']}} },
            { name: 'E-sewa', y: {{$paid_by_total['e-sewa']}} },
            { name: 'Other', y: {{$paid_by_total['others']}} },
        ]
    }]
});

  $(function() {
        $('#start_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
        });
        $('#end_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
        });
    });
    </script>

@endsection
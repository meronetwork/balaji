<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 5px;
    }

    table th,
    table td {
        padding: 3px;
        background:;
        text-align: center;
        border: 1px solid #FFFFFF;
    }
    table .foot {
        font-size: 1em;
        background: #d9edf7;
    }
    table thead th {

        font-size: 1em;
        background: #d9edf7;
    }
</style>
<table style="width:100%; text-align:center; padding: 30px 0; box-shadow: 0 1.2rem 1.8rem 0 rgba(0,0,0,0.24),0 1.7rem 5rem 0 rgba(0,0,0,0.19); -webkit-box-shadow: 0 1.2rem 1.8rem 0 rgba(0,0,0,0.24),0 1.7rem 5rem 0 rgba(0,0,0,0.19); font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 10px;">

    <thead>
    <tr>
        <td colspan="2" style="text-align: left;">Company:{{\Auth::user()->organization->organization_name}}</td>
        <td colspan="9" style="text-align: right;">Fiscal Year: {{$fiscal_year}}</td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: left;">Address:{{\Auth::user()->organization->address}}</td>
        <td colspan="9" style="text-align: right;">@if($months) Month: {{$months}}
            @else
                Date: {{ date('dS M y', strtotime($startdate)) }} - {{ date('dS M y', strtotime($enddate)) }}
            @endif</td>
    </tr>
    <tr>
        <td style="text-align: left;">PAN:{{\Auth::user()->organization->vat_id}}</td>
    </tr>
    <tr>
        <td style="text-align: left;">Purchase Book</td>
    </tr>

    </thead>
</table>

<table style="width:100%; text-align:center; padding: 30px 0; box-shadow: 0 1.2rem 1.8rem 0 rgba(0,0,0,0.24),0 1.7rem 5rem 0 rgba(0,0,0,0.19); -webkit-box-shadow: 0 1.2rem 1.8rem 0 rgba(0,0,0,0.24),0 1.7rem 5rem 0 rgba(0,0,0,0.19); font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 10px;">

    <thead>
    <tr class=bg-olive>

        <th colspan="6" style="text-align: center;" >Bill/Invoice</th>
        <th rowspan="2">Total Purchase</th>
        <th rowspan="2">Tax Free Purchase</th>
        <th colspan="2" style="text-align: center; ">
            Taxable Purchase
        </th>
        <th colspan="2" style="text-align: center; ">Taxable Import</th>
        <th colspan="2" style="text-align: center; ">CapitalTaxable Purchase/Import</th>

    </tr>
    <tr class="bg-olive">
        <th>Date</th>
        <th>Bill No</th>
        <th>Supplier’s Name</th>
        <th>Supl. PAN Number</th>
        <th>Purch/Import Service/Product Detail</th>
        <th>Purch/Import Quantity</th>

        <th>Taxable<br>Purchase</th>
        <th>Tax</th>

        <th>Amt</th>
        <th>Tax</th>
        <th>Amt</th>
        <th>Tax</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $taxable_amount = 0;
    $tax_amount = 0;
    $total_purch =0;
    $totalNontaxPurch = 0;
    $totalDiscount =0;
    ?>
    @foreach($purchase_book as $pur_bks)
        <tr>
            <td style="width: 20%"><span style="font-size: 12px"> {{ strtotime($pur_bks->bill_date) > 0 ? date('d M y', strtotime($pur_bks->bill_date)) : '' }}<br/>
                <?php
                $temp_date = explode(" ",$pur_bks->bill_date );
                $temp_date1 = explode("-",$temp_date[0]);
                $cal = new \App\Helpers\NepaliCalendar();
                //nepali date
                $a = $temp_date1[0];
                $b = $temp_date1[1];
                $c = $temp_date1[2];
                $d = $cal->eng_to_nep($a,$b,$c);

                $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                ?>

                 {!! $nepali_date !!}</span>

            </td>
            <td>{{$pur_bks->bill_no}} </td>
            <td>{{$pur_bks->client->name}}</td>
            <td>{{$pur_bks->client->vat}}</td>
            <td>
                Product
            </td>
            <td>
                {{$pur_bks->total_items_quantity}}
            </td>
            <td>

                {{-- {{$pur_bks->taxable_amount + round($pur_bks->tax_amount,2) }} --}}
                {{number_format($pur_bks->total,2)}}


            </td>
            <td>{{ number_format($pur_bks->non_taxable_amount,2) }}</td>

            <td>{{number_format($pur_bks->taxable_amount,2)}}</td>
            <?php
            $taxable_amount = $taxable_amount + $pur_bks->taxable_amount;
            $tax_amount = $tax_amount +  $pur_bks->tax_amount;
            $total_purch  +=  $pur_bks->total;

            $totalNontaxPurch += $pur_bks->non_taxable_amount;
            $totalDiscount += $pur_bks->discount_amount;
            ?>
            <td>{{ number_format($pur_bks->tax_amount,2) }}</td>
            <td style="text-align: center">-</td>
            <td style="text-align: center">-</td>
            <td style="text-align: center">-</td>
            <td style="text-align: center">-</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="6" style="text-align: right">Total Amount</th>

        <th>{{ number_format($total_purch,2) }}</th>
        <th>{{ number_format($totalNontaxPurch,2) }}</th>
        <th>{{number_format($taxable_amount,2)}}</th>
        <th>{{number_format($tax_amount,2)}}</th>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>

    </tbody>
</table>

<!-- <hr> -->
<!-- <p style="text-align: center;">Sent from MEROCRM</p> -->

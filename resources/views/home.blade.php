@extends('layouts.master')
@section('content')
    <style type="text/css">
        .blink_me {
            animation: blinker 1s linear infinite;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }

        .box-body>.nav>li>a {
            padding: 4px 5px !important;
        }

    </style>

    <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>Welcome to Hotelsuite Home <small>Hotel Main Home page</small></h1>


    </section>


    <div class="row">

        <div class="col-md-3">
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <i class="material-icons">query_stats</i>  Todays Stats</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="nav nav-stacked">
                        <li><a href="/admin/hotel/showroomoccupied/"><i class="material-icons">bedroom_parent</i> Room Occupied <span style="font-size: 17px" class="pull-right badge bg-blue">{{$room_occupied}}</span></a></li>
                        <li><a href="/admin/hotel/showdayin"><i class="material-icons">timelapse</i> Due in <span style="font-size: 17px" class="pull-right badge bg-green">{{$due_in}}</span></a></li>
                        <li><a href="/admin/hotel/showdayout"><i class="material-icons">more_time</i> Due Out <span style="font-size: 17px" class="pull-right badge bg-green">{{$due_out}}</span></a></li>
                        <li><a href="/admin/hotel/showstayover"><i class="material-icons">more</i> Stay Over <span style="font-size: 17px" class="pull-right badge bg-red">{{$stay_over}}</span></a></li>
                        <li><a href="/admin/hotel/checked-out"><i class="material-icons">check_circle_outline</i> Checked Out <span style="font-size: 17px" class="pull-right badge bg-red">{{$check_out}}</span></a></li>
                        <li><a href="#"><i class="material-icons">cancel</i> Void<span style="font-size: 17px"  class="pull-right badge bg-red">{{\App\Helpers\ReservationHelper::void()->count()}}</span></a></li>
                        <li><a href="#"><i class="material-icons">panorama_photosphere_select</i> No Show<span style="font-size: 17px" class="pull-right badge bg-red">{{\App\Helpers\ReservationHelper::noShow()->count()}}</span></a></li>
                        <li><a href="/admin/hotel/day-use/"><i class="material-icons">wb_sunny</i> Day use <span style="font-size: 17px" class="pull-right badge bg-red">{{$day_use}}</span></a></li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>


        <div class="col-md-3">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <span class="material-icons">flaky</span> House status</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="nav nav-stacked">
                        <li><a href="/admin/hotel/clean-room"><i class="material-icons md-18">cleaning_services</i> Clean <span class="pull-right badge bg-blue">{{$clean}}</span></a></li>
                        <li><a href="/admin/hotel/dirty-room"><i class="material-icons">dirty_lens</i> Dirty <span class="pull-right badge bg-aqua">{{$dirty}}</span></a></li>
                        <li><a href="/admin/hotel/inspected-room/"><i class="material-icons">checkroom</i> Inspected <span class="pull-right badge bg-green">{{$inspected}}</span></a></li>
                        <li><a href="/admin/hotel/pickup-room/"><i class="material-icons">plus_one</i> Pickup <span class="pull-right badge bg-red">{{$pickup}}</span></a></li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6">

            <a target="_blank" href="/admin/hotel/reservation/reports/index" class="btn btn-app">
                <i class="fa fa-edit"></i> Reports
            </a>

            <a target="_blank" href="/admin/hotel/rate-plans" class="btn btn-app">
                <i class="fa fa-money"></i> Rate Plans
            </a>

            <a target="_blank" href="#" class="btn btn-app">
                <i style="color:red" class="fa fa-repeat"></i> Occupancy
            </a>
            <a target="_blank" href="/admin/posSummaryAnalysis"  class="btn btn-app">
                <span class="material-icons">auto_fix_high</span><br/>  Sales Analysis
            </a>
            <a target="_blank" href="/enquiry"  class="btn btn-app bg-primary">
                <span class="material-icons">public</span><br/> Connect Page
            </a>
            <a target="_blank" href="/admin/hotel/reservation-calender"  class="btn btn-app">
               <i class="material-icons md-18">event_available</i><br/> Res. Calendar
            </a>
            <a target="_blank" href="/admin/hotel/reservation-index"  class="btn btn-app">
                <span class="material-icons">stars</span><br/> Reservations
            </a>
            <a target="_blank" href="/admin/hotel/house-keep-index"  class="btn btn-app">
                <i class="material-icons md-18">room_preferences</i><br/> House Status

            </a>

            <a target="_blank" href="/admin/hotelcustomer" class="btn btn-app">
                <span class="material-icons">people_alt</span><br/> Customers
            </a>
            <a target="_blank" href="/admin/tasks" class="btn btn-app">
                <span class="material-icons">tasks</span><br/> HK Tasks
            </a>
            <a href="/admin/hotel/roomtransfer/item" class="btn btn-app">
                <span class="material-icons">room_service</span><br/> Room Trans.
            </a>
            <a href="/admin/contacts" class="btn btn-app">
                <span class="material-icons">support_agent</span><br/> Phone Book
            </a>
            <a href="/admin/hotel/irdposting/index" class="btn btn-app">
                <span class="material-icons">receipt_long</span><br/> Invoices
            </a>
            <a href="/admin/orders/hotel/payments" class="btn btn-app">
                <span class="material-icons">receipt</span><br/> Receipts
            </a>
            <a href="/admin/posSummaryAmount" class="btn btn-app">
                <span class="material-icons">leaderboard</span><br/>Sales Summary
            </a>
            <a href="/admin/privilege-card" class="btn btn-app">
                <span class="material-icons">style</span><br/> Loyalty
            </a>
        </div>
        <!--   <div class="col-lg-3 col-xs-6">
              <div class="box box-widget widget-user-2">
                 <div class="small-box bg-black">
                <div class="inner">
                  <strong><i class="fa fa-bar-chart-o"></i> Hotel</strong>
                  <p>Inventory</p>
                </div>
              </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Rooms in Property <span class="pull-right badge bg-blue">31</span></a></li>
                    <li><a href="#">Out of order <span class="pull-right badge bg-aqua">5</span></a></li>
                    <li><a href="#">Due Out <span class="pull-right badge bg-green">12</span></a></li>
                    <li><a href="#">Total Rooms Available <span class="pull-right badge bg-red">842</span></a></li>
                  </ul>
                </div>
              </div>
            </div> -->
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <span class="material-icons">
flight_land
</span> Expected Arrival today</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>Resv. #</th>
                            <th>Room #</th>
                            <th>Source</th>
                            <th>Guest Name</th>
                        </tr>
                        @foreach($hotel_arrival as $ha)
                            <tr>
                                <td>{{$ha->id}}.</td>
                                <td><strong> <a href="/admin/hotel/reservation-edit/{{$ha->id}}">{{$ha->room_num}}</a></strong> {{$ha->room->room_type->type_code}}</td>
                                <td>{{$ha->source->name}}</td>
                                <td>
                                    @if($ha->guest_id)
                                        {{$ha->guest->full_name}}
                                    @else
                                        {{$ha->guest_name}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <span class="material-icons">
departure_board
</span> Expected Departure today</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>Resv. #</th>
                            <th>Room #</th>
                            <th>Source</th>
                            <th>Date</th>
                        </tr>
                        @foreach($hotel_departure as $hd)
                            <tr>
                                <td>{{$hd->id}}.</td>
                                <td><strong><a href="/admin/hotel/reservation-edit/{{$hd->id}}">{{$hd->room_num}}</a></strong> {{$hd->room->room_type->type_code}}</td>
                                <td>{{$ha->date}}</td>
                                <td>
                                    @if($hd->guest_id)
                                        {{$hd->guest->full_name}}
                                    @else
                                        {{$hd->guest_name}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <span class="material-icons">
room_service
</span> Latest House Keeping Tasks <small>Serve on priority basis</small></h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>Resv. #</th>
                            <th>Room #</th>
                            <th>Assigned To</th>
                            <th>Priority</th>
                            <th>Status</th>
                        </tr>
                        @if(isset($hk_task))
                            @foreach($hk_task as $ha)
                                <tr>
                                    <td>{{$ha->lead_id}}</td>
                                    <td><strong>
                                            <a href="/admin/hotel/reservation-edit/{{$ha->lead_id}}" target="_blank">
                                                {{$ha->reservation->room_num}}
                                            </a></strong> {{$ha->reservation->room->room_type->type_code}}</td>
                                    <td>{{$ha->assigned_to->full_name}}</td>
                                    <td>
                                        {{$ha->task_priority}}
                                    </td>
                                    <td>{{$ha->task_status}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"> <span class="material-icons">
calendar_view_month
</span> Expected Services this week</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>Resv. #</th>
                            <th>Room #</th>
                            <th>Package</th>
                            <th>Date</th>
                            <td>No of Person</td>
                        </tr>
                        @if(isset($services_this_week))
                            @foreach($services_this_week as $ha)
                                <tr>
                                    <td>{{$ha->reservation_id}}.</td>
                                    <td><strong> <a href="/admin/hotel/extend-package/{{$ha->reservation_id}}">{{$ha->reservation->room_num}}</a></strong> {{$ha->reservation->room->room_type->type_code}}</td>
                                    <td>{{$ha->services->attraction_name}}</td>
                                    <td>
                                        {{$ha->package_date}}
                                    </td>
                                    <td>{{$ha->number_of_person}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <br />

    <script type="text/javascript">
        function getLocation() {



            return new Promise((resolve, reject) => {

                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(function(position) {

                        return resolve(position);

                    }, function(err) {

                        return reject(err);

                    });



                } else {

                    return reject(false);

                }

            })



        }



        function clockin(user_id) {

            getLocation().then(response => {

                let crd = response.coords;

                let location = JSON.stringify({
                    lat: crd.latitude
                    , long: crd.longitude
                });

                console.log(user_id, location);

                window.location = `/clockin?location=${location}`;

            }).catch(err => {

                console.log(err);

                window.location = `/clockin`;

            });

        }



        function clockout(user_id) {

            getLocation().then(response => {

                let crd = response.coords;

                let location = JSON.stringify({
                    lat: crd.latitude
                    , long: crd.longitude
                });

                console.log(user_id, location);

                window.location = `clockout?location=${location}`;

            }).catch(err => {

                window.location = `/clockout`;

            });

        }





        var serverTime = @php echo strtotime(date('Y-m-d H:i:s')) * 1000;
        @endphp;



        var expected = serverTime;

        var date;

        var hours;

        var minutes;

        var seconds;

        var now = performance.now();

        var then = now;

        var dt = 0;

        var nextInterval = interval = 1000; // ms



        setTimeout(step, interval);



        function formatAMPM(date) {

            let hours = date.getHours();

            let minutes = date.getMinutes();

            let seconds = date.getSeconds();

            let ampm = hours >= 12 ? 'PM' : 'AM';

            hours = hours % 12;

            hours = hours ? hours : 12; // the hour '0' should be '12'

            hours = hours < 10 ? '0' + hours : hours

            minutes = minutes < 10 ? '0' + minutes : minutes;

            seconds = seconds < 10 ? '0' + seconds : seconds;

            let strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;

            return strTime;

        }

        const timeZone = '{{date_default_timezone_get()}}';

        function step() {

            then = now;

            now = performance.now();

            dt = now - then - nextInterval; // the drift



            nextInterval = interval - dt;

            serverTime += interval;

            date = new Date(serverTime);

            date = new Date(date.toLocaleString('en-US', {
                timeZone: timeZone
            }));

            let newtime = formatAMPM(date)

            $('#livetime').text(newtime);

            now = performance.now();



            setTimeout(step, Math.max(0, nextInterval)); // take into account drift

        }

    </script>

@endsection

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">{{ $modal_title }}</h4>
</div>
<?php

$housestatus = \App\Models\Housestatus::select('status_name','id')->get();

//dd($housestatus);

?>
<form method="post" action="{{ $modal_route }}">
        {{ csrf_field() }}

<div class="modal-body">
    @if($error)
        <div>{{{ $error }}}</div>
    @else
       <label class="control-label">Select House Status</label>
       <select class="form-control" name="house_status_id">
          @foreach($housestatus as $hs)
           <option value="{{$hs->id}}">{{$hs->status_name}}</option>
           @endforeach
       </select>
    @endif
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('general.button.cancel') }}</button>
    @if(!$error)
     <button type="Submit" name="submit" class="btn btn-primary">Ok</button  >
     <a href="#" target="blank" class="btn btn-success">Print</a>
</form>
    @endif
</div>


@extends('layouts.master')

@section('head_extra')
<!-- jVectorMap 1.2.2 -->
<link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />
<style>
	.filter_date { font-size:14px; }
</style>
@endsection
<style type="text/css">
	@media print {
    .pagebreak {
        clear: both;
        page-break-after: always;
        padding-top: 100px;
    }
    .dataTables_scrollBody {
    	position: relative;
  		height: auto !important;
    }
    .box{
    	background: white !important;
    }
    ::-webkit-scrollbar {
    display: none;
}
}
  @media print {

          @page { margin: 0; }

          body { margin: 1.6cm; }

        }
   .dataTables_scrollBody {
   position: relative;
  height: auto !important;
 
    }
</style>


@section('content')
 <div class='row'>
        <div class='col-md-12'>
                 <div class="row no-print">
                <div class="col-md-12" style="margin-top:5px;">
                	<form action="/admin/posAmountSummaryDateWise" method="GET">
                    <div class="filter form-inline" style="margin:0 30px 0 0;">

                        <input style="width:120px;" class="form-control input-sm" id="start_date" placeholder="Start Date" name="start_date" type="text"

                        value="{{ Request::get('start_date') ?? date('Y-m-d') }}" autocomplete="off">&nbsp;&nbsp;
                        <!-- <label for="end_date" style="float:left; padding-top:7px;">End Date: </label> -->
                        <input style="width:120px; display:inline-block;" class="form-control input-sm" id="end_date" placeholder="End Date" name="end_date" type="text" value="{{ Request::get('end_date') ?? date('Y-m-d') }}" autocomplete="off">&nbsp;&nbsp;           

                        <select class="form-control" name="outlet_id">
                        	<option value="">Select outlet</option>
                        	@foreach($outlets as $key=>$outl)
                        	<option value="{{$outl->id}}" @if(Request::get('outlet_id') == $outl->id) selected="" @endif>{{$outl->name}} ({{$outl->short_name}})</option>
                        	@endforeach
                        </select>             

                        <button class="btn btn-primary btn-sm" id="btn-submit-filter" type="submit">
                            <i class="fa fa-list"></i> Filter
                        </button>

                        <a href="/admin/posAmountSummaryDateWise" class="btn btn-danger btn-sm" id="btn-filter-clear">
                            <i class="fa fa-close"></i> Reset
                        </a>
                      @if(\Request::get('start_date') && \Request::get('end_date'))
                         <a href="/admin/posAmountSummaryDateWiseExcel/?start_date={{ \Request::get('start_date') }}&end_date={{ \Request::get('end_date') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-download"></i> Excel
                        </a>
                        @endif
                    </div>
                    </form>
                </div>


           </div>
     </div>
</div>


@if(\Request::get('start_date') && \Request::get('end_date'))
<div id="EmpprintReport">
    <div class="row">
        <div class="col-sm-12 std_print">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php 
                    $cal = new \App\Helpers\NepaliCalendar();    
                    $start_date = \Request::get('start_date');
                    $nepali_start_date = $cal->formated_nepali_from_eng_date($start_date);
                    $end_date = \Request::get('end_date');
                    $nepali_end_date = $cal->formated_nepali_from_eng_date($end_date);

                        ?>
                        Product Report List of {{ date('d F-Y', strtotime($start_date)) }}  to
                      {{ date('d F-Y', strtotime($end_date)) }} 
                      
                    </h3>
                </div>
                <?php
                  
                    $begin = new DateTime($start_date);
                    $end = new DateTime($end_date);
                    $end->add(new \DateInterval('P1D'));
                    $interval = DateInterval::createFromDateString('1 day');
                    $period = new DatePeriod($begin, $interval, $end);
                    $cal = new \App\Helpers\NepaliCalendar();
                ?>
                <div class="table-responsive">
                <table id="" class="table table-bordered std_table">
                    <thead>
                    <tr class="bg-danger">
                        <th >Heading</th>
                        @foreach ($period as $dt)
                        <?php
                            $engdate = $dt->format("Y-m-d");
                            // $nepdate = $cal->formated_nepali_from_eng_date($engdate);

                        ?>
                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{$dt->format("M-d") }}</td>
                        @endforeach
                        <th>Total</th>
                    </tr>
                    </thead>
                        <tbody>
                                <tr>
                                    <td >Covers</td>
                                    <?php $cover_count = 0; ?>
                                     @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $cover_count  += $amountSummary['coverCounts'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['coverCounts'],0) }}</td>
				                     @endforeach
				                     <td>{{ number_format($cover_count,0) }}</td>
                                </tr>
                                
                                <tr>     
                                    <td >Sales bill count</td>
                                     <?php $sales_bill_count = 0; ?>
                                     @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $sales_bill_count  += $amountSummary['totalSalesCount'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['totalSalesCount'],0) }}</td>
				                     @endforeach
				                      <td>{{ number_format($sales_bill_count,0) }}</td>
                                </tr>
                                
                                <tr>    
                                    <td >Bill Count</td>
                                     <?php $net_sales_count = 0; ?>
                                     @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $net_sales_count  += $amountSummary['netSalesCount'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['netSalesCount'],2) }}</td>
				                     @endforeach
				                      <td>{{ number_format($net_sales_count,0) }}</td>
                                </tr>
                                <tr>    
                                    <td >Total Discount</td>
                                     <?php $discount_count = 0; ?>
                                     @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $discount_count  += $amountSummary['totalDiscount'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['totalDiscount'],2) }}</td>
				                     @endforeach
				                      <td>{{ number_format($discount_count,2) }}</td>
                                </tr>
                                
                                <tr>     
                                    <td >Total Service Charge</td>
                                     <?php $service_charge_count = 0; ?>
                                       @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $service_charge_count  += $amountSummary['service_charge'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['service_charge'],2) }}</td>
				                     @endforeach
                                      <td>{{ number_format($service_charge_count,2) }}</td>
                                </tr>
                                <tr>    
                                    <td >Total Taxable Amount</td>
                                    <?php $taxable_count = 0; ?>
                                     @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $taxable_count  += $amountSummary['taxable_amount'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['taxable_amount'],2) }}</td>
				                     @endforeach
                                       <td>{{ number_format($taxable_count,2) }}</td>
                                </tr>

                                <tr>    
                                    <td >Total Tax</td>
                                    <?php $tax_amount_count = 0; ?>
                                    @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $tax_amount_count  += $amountSummary['tax_amount'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['tax_amount'],2) }}</td>
				                     @endforeach
                                      <td>{{ number_format($tax_amount_count,2) }}</td>
                                </tr>

                                <tr>    
                                    <td >Total Amount</td>
                                      <?php $total_amount_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
				                            $amountSummary  = $amountSummaryAndPaidTotal['amountSummary'];
                                            $total_amount_count  += $amountSummary['total_amount'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{ number_format($amountSummary['total_amount'],2) }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_amount_count,2) }}</td>
                                </tr>


                                  <tr>    
                                    <td >Total Cash</td>
                                     <?php $total_cash_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_cash_count  += $paid_by_total['cash'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['cash'] }}</td>
				                     @endforeach
                                     <td>{{ number_format($total_cash_count,2) }}</td>
                                </tr>


                                   <tr>    
                                    <td >Total by City Ledger</td>
                                     <?php $total_city_ledger_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_city_ledger_count  += $paid_by_total['city-ledger'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['city-ledger'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_city_ledger_count,2) }}</td>
                                </tr>


                                 <tr>    
                                    <td >Total by Credit card</td>
                                      <?php $total_credit_card_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_credit_card_count  += $paid_by_total['credit-cards'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['credit-cards'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_credit_card_count,2) }}</td>
                                </tr>

                                  <tr>    
                                    <td >Total by Check</td>
                                      <?php $total_check_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_check_count  += $paid_by_total['check'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['check'] }}</td>
				                     @endforeach
                                       <td>{{ number_format($total_check_count,2) }}</td>
                                </tr>


                                <tr>    
                                    <td >Total by Travel agent</td>
                                      <?php $total_travel_agent_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_travel_agent_count  += $paid_by_total['travel-agent'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['travel-agent'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_travel_agent_count,2) }}</td>
                                </tr>


                                  <tr>    
                                    <td >Total by Complementry</td>
                                      <?php $total_complementry_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_complementry_count  += $paid_by_total['complementry'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['complementry'] }}</td>
				                     @endforeach
                                    <td>{{ number_format($total_complementry_count,2) }}</td>
                                </tr>

                                <tr>    
                                    <td >Total by Staff</td>
                                      <?php $total_by_staff_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_by_staff_count  += $paid_by_total['staff'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['staff'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_by_staff_count,2) }}</td>
                                </tr>


                                <tr>    
                                    <td >Total by Room</td>
                                      <?php $total_by_room_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_by_room_count  += $paid_by_total['room'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['room'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_by_room_count,2) }}</td>
                                </tr>


                                  <tr>    
                                    <td >Total by e-sewa</td>
                                    <?php $total_by_esewa_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_by_esewa_count  += $paid_by_total['e-sewa'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['e-sewa'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_by_esewa_count,2) }}</td>
                                </tr>


                                <tr>    
                                    <td >Total by MNP</td>
                                     <?php $total_by_mnp_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_by_mnp_count  += $paid_by_total['mnp'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['mnp'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_by_mnp_count,2) }}</td>
                                </tr>


                                <tr>    
                                    <td >Total by EDM</td>
                                      <?php $total_by_edm_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_by_edm_count  += $paid_by_total['edm'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['edm'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_by_edm_count,2) }}</td>
                                </tr>


                                 <tr>    
                                    <td >Total by others</td>
                                      <?php $total_by_others_count = 0; ?>
                                      @foreach ($period as $dt)
				                        <?php
				                            $engdate = $dt->format("Y-m-d");
				                            $amountSummaryAndPaidTotal = \App\Helpers\TaskHelper::getSalesByPaymethod($engdate,$engdate);
                                            $paid_by_total  = $amountSummaryAndPaidTotal['paid_by_total'];
				                            $total_by_others_count  += $paid_by_total['others'];
				                        ?>
				                        <td class="std_p" title="{{ date('l',strtotime($engdate)) }}">{{env('APP_CURRENCY')}} {{ $paid_by_total['others'] }}</td>
				                     @endforeach
                                      <td>{{ number_format($total_by_others_count,2) }}</td>
                                </tr>





                                </tr>     
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
@endif


<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script type="text/javascript">
  $(function() {
        $('#start_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
        });
        $('#end_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
        });
    });
    </script>

@endsection
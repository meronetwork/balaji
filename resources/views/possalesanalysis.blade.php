@extends('layouts.master')

@section('head_extra')
<!-- jVectorMap 1.2.2 -->
<link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />
<style>
   .filter_date { font-size:14px; }

   @media print {
    .pagebreak {
        clear: both;
        page-break-after: always;
    }
}
  @media print {

          @page { margin: 0; }

          body { margin: 1.6cm; }

        }
</style>
@endsection



@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
  <h1>
               Sales Analysis
                <small>Pos Sales Analysis

                  <b><button class="btn btn-xs btn-primary no-loading" onclick="window.print()">
                     <i class="fa fa-print"></i>
                  Print</button></b>
                </small>
            </h1>



</section>

<div class="row no-print">
          <div class="col-md-12" style="margin-top:5px;margin-bottom: 5px;">
                  <form action="/admin/posSummaryAnalysis" method="GET">
                    <div class="filter form-inline" style="margin:0 30px 0 0;">

                        <input type="text" name="date" placeholder="Enter Date"  class="btn btn-sm btn-default datepicker date-toggle" style="cursor: auto;text-align: left;" value="{{ Request::get('date') }}">
                        <select class="form-control input-sm" name="outlet_id">
                           <option value="">Select outlet</option>
                           @foreach($outlets as $key=>$outl)
                           <option value="{{$outl->id}}" @if(Request::get('outlet_id') == $outl->id) selected="" @endif>{{$outl->name}} ({{$outl->short_name}})</option>
                           @endforeach
                        </select>

                        <button class="btn btn-primary btn-sm" id="btn-submit-filter" type="submit">
                            <i class="fa fa-list"></i> Filter
                        </button>

                        <a href="/admin/posSummaryAnalysis" class="btn btn-danger btn-sm" id="btn-filter-clear">
                            <i class="fa fa-close"></i> Reset
                        </a>

                    </div>
                    </form>
                </div>
    <br>
</div>




<div class='row'>
   <div class='col-md-12'>
      <!-- DIRECT CHAT -->
      <div class="box box-warning direct-chat direct-chat-warning">
         <div class="box-header with-border">
            <h3 class="box-title">Sales Analysis  {{$today_date}}  </h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool no-print" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <button type="button" class="btn btn-box-tool no-print" data-widget="remove"><i class="fa fa-times"></i>
               </button>
            </div>
         </div>
         <div class="box-body">
            <table class="table table-striped">
               <thead>
                  <tr class="bg-yellow ">
                     <th class="text-center">Particulars</th>
                     <th class="text-center">{{ $today_date }} </th>
                     <th class="text-center">Month to date</th>
                     <th class="text-center">Year to date</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($foodSalesByMasterArr as $key=>$value)
                  <tr>
                     <td >{{ $value['name'] }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($value['today'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($value['months'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($value['years'],2)  }}</td>
                  </tr>

                 @endforeach

                  <tr>
                     <th>Total Sales</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($foodSalesByMasterArr->sum('today'),2) }}  </th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($foodSalesByMasterArr->sum('months'),2) }}  </th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($foodSalesByMasterArr->sum('years'),2)}}  </th>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>


    <div class='col-md-12'>
      <!-- DIRECT CHAT -->
      <div class="box box-success direct-chat direct-chat-success">
         <div class="box-header with-border">
            <h3 class="box-title">Sales Analysis {{ $today_date }}</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool no-print" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <button type="button" class="btn btn-box-tool no-print" data-widget="remove"><i class="fa fa-times"></i>
               </button>
            </div>
         </div>
         <div class="box-body">
            <table class="table table-striped">
               <thead>
                  <tr class="bg-olive ">
                     <th class="text-center">Particulars</th>
                     <th class="text-center">{{ $today_date }}</th>
                     <th class="text-center">Month to date</th>
                     <th class="text-center">Year to date</th>
                  </tr>
               </thead>
               <tbody>


                  <tr>
                     <td >Cash Sales</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['cash'] ,2) }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['cash'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['cash'],2)  }}</td>
                  </tr>


                  <tr>
                     <td >City Ledger</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['city-ledger'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['city-ledger'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['city-ledger'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Credit Cards</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['credit-cards'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['credit-cards'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['credit-cards'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Check</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['check'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['check'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['check'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Travel agent</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['travel-agent'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['travel-agent'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['travel-agent'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Complementry</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['complementry'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['complementry'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['complementry'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Staff</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['staff'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['staff'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['staff'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Room</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['room'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['room'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['room'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >e-Sewa</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['e-sewa'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['e-sewa'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['e-sewa'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >EDM</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['edm'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['edm'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['edm'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >MNP</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['mnp'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['mnp'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['mnp'],2)  }}</td>
                  </tr>

                  <tr>
                     <td >Others</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($today_paid_by_total['others'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($month_paid_by_total['others'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format($year_paid_by_total['others'],2)  }}</td>
                  </tr>

                  <tr>
                     <th >Total Sales</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format(($today_paid_by_total['cash'] + $today_paid_by_total['city-ledger'] +$today_paid_by_total['credit-cards'] +$today_paid_by_total['check'] +$today_paid_by_total['travel-agent'] +
                        $today_paid_by_total['complementry'] +
                        $today_paid_by_total['staff']  +
                        $today_paid_by_total['room'] +
                        $today_paid_by_total['e-sewa'] +
                        $today_paid_by_total['others'] +
                        $today_paid_by_total['mnp'] +
                        $today_paid_by_total['edm']),2) }}</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }}
                     {{  number_format(($month_paid_by_total['cash'] +$month_paid_by_total['city-ledger'] +$month_paid_by_total['credit-cards'] +$month_paid_by_total['check'] +$month_paid_by_total['travel-agent'] +
                        $month_paid_by_total['complementry'] +
                        $month_paid_by_total['staff']  +
                        $month_paid_by_total['room'] +
                        $month_paid_by_total['e-sewa'] +
                        $month_paid_by_total['others'] +
                        $month_paid_by_total['mnp'] +
                        $month_paid_by_total['edm']),2) }}
                     </th>
                     <th class="text-center">{{ env("APP_CURRENCY") }}
                     {{ number_format(($year_paid_by_total['cash']+$year_paid_by_total['city-ledger'] +$year_paid_by_total['credit-cards'] +$year_paid_by_total['check'] +$year_paid_by_total['travel-agent'] +
                        $year_paid_by_total['complementry'] +
                        $year_paid_by_total['staff']  +
                        $year_paid_by_total['room'] +
                        $year_paid_by_total['e-sewa'] +
                        $year_paid_by_total['others'] +
                        $year_paid_by_total['mnp'] +
                        $year_paid_by_total['edm']),2) }}</th>
                  </tr>


               </tbody>
            </table>
         </div>
      </div>
   </div>



      <div class='col-md-12 '>
         <div style="break-after:page"></div>
      <!-- DIRECT CHAT -->
      <div class="box box-danger direct-chat direct-chat-danger">
         <div class="box-header with-border">
            <h3 class="box-title">Sales Analysis {{ $today_date }}</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool no-print" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <button type="button" class="btn btn-box-tool no-print" data-widget="remove"><i class="fa fa-times"></i>
               </button>
            </div>
         </div>
         <div class="box-body">
            <table class="table table-striped">
               <thead>
                  <tr class="bg-danger ">
                     <th class="text-center">Particulars</th>
                     <th class="text-center">{{$today_date}}</th>
                     <th class="text-center">Month to date</th>
                     <th class="text-center">Year to date</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td >Sales Before Discount(A)</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($today_amountSummary['totalSubtotal'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{number_format($month_amountSummary['totalSubtotal'],2)   }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($year_amountSummary['totalSubtotal'],2)   }}</td>
                  </tr>

                  <tr>
                     <td >Discount(B)</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} ({{ number_format($today_amountSummary['totalDiscount'],2)  }})</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} ({{number_format($month_amountSummary['totalDiscount'],2)   }})</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} ({{ number_format($year_amountSummary['totalDiscount'],2)   }})</td>
                  </tr>

                  @php
                     $c_today = $today_amountSummary['totalSubtotal'] -  $today_amountSummary['totalDiscount'];
                     $c_months = $month_amountSummary['totalSubtotal'] - $month_amountSummary['totalDiscount'];
                     $c_years = $year_amountSummary['totalSubtotal'] - $year_amountSummary['totalDiscount'];


                  @endphp
                  <tr>
                     <th>Sales After Discount</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($c_today,2) }}</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($c_months,2) }}</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($c_years,2) }}</th>
                  </tr>


                  <tr>
                     <td >Service Charge </td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($today_amountSummary['service_charge'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{number_format($month_amountSummary['service_charge'],2)   }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($year_amountSummary['service_charge'],2)   }}</td>
                  </tr>

                  <tr>
                     <td >VAT</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($today_amountSummary['tax_amount'],2)  }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{number_format($month_amountSummary['tax_amount'],2)   }}</td>
                     <td class="text-center">{{ env("APP_CURRENCY") }} {{ number_format($year_amountSummary['tax_amount'],2)   }}</td>
                  </tr>

                  <tr>
                     <th >Gross Sales</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{number_format(($c_today + $today_amountSummary['service_charge'] +  $today_amountSummary['tax_amount']),2)  }}</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{number_format(($c_months + $month_amountSummary['service_charge'] +  $month_amountSummary['tax_amount']),2)  }}</th>
                     <th class="text-center">{{ env("APP_CURRENCY") }} {{number_format(($c_years + $year_amountSummary['service_charge'] +  $year_amountSummary['tax_amount']),2)  }}</th>
                  </tr>

               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>

@include('partials._date-toggle')
@section('body_bottom')
<script type="text/javascript">
   $('.date-toggle').nepalidatetoggle();

    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true,

        });

    });

</script>

@endsection

@endsection

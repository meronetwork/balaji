<html>
<head>
    <title>Sales Book</title>
    <!-- block from searh engines -->
    <meta name="robots" content="noindex">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Set a meta reference to the CSRF token for use in AJAX request -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons 4.7.0 -->
    <link href="{{ asset("/bower_components/admin-lte/font-awesome/css/all.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.1 -->
    <link href="{{ asset("/bower_components/admin-lte/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />

    <!-- Application CSS-->


    <style>
        @media print {
            body {
                -webkit-print-color-adjust: exact!important;
                color-adjust: exact; print-color-adjust: exact;
            }
            .text-center{
                text-align: center;
            }
            .bg-gray{
                background-color: #d2d6de !important;
            }
            div{
                line-height: 18px;
            }
            .item-detail td,th{
                border:1px solid #eee;
            }
        }
        table {
            width: 100%;
        }

        table,


        th,
        td {
            padding: 5px;
            text-align: center;
        }

        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }

        table#t01 tr:nth-child(odd) {
            background-color: #fff;
        }

        table#t01 th {
            background-color: #696969;
            color: white;
        }


        .table>thead>tr>th {
            border-bottom: 1px solid #696969 !important;
        }

        .table>tbody>tr>th {
            border-top: 1px solid #696969 !important;
        }

        .table>tbody>tr>td {
            border-top: 1px solid #696969 !important;
        }
        @page {
            size: auto;
            margin: 0;
        }

        body {
            padding-left: 1.3cm;
            padding-right: 1.3cm;
            padding-top: 1.3cm;
        }

        @media print {
            .pagebreak {
                page-break-before: always;
            }
    </style>
</head>
<body onload="window.print();">
<table style="width:100%; text-align:center; padding: 30px 0; box-shadow: 0 1.2rem 1.8rem 0 rgba(0,0,0,0.24),0 1.7rem 5rem 0 rgba(0,0,0,0.19); -webkit-box-shadow: 0 1.2rem 1.8rem 0 rgba(0,0,0,0.24),0 1.7rem 5rem 0 rgba(0,0,0,0.19); font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 10px;">

    <thead>
    <tr>
        <td colspan="2" style="text-align: left;">Company:{{\Auth::user()->organization->organization_name}}</td>
        <td colspan="8" style="text-align: right;">Fiscal Year: {{$fiscal_year}}</td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: left;">Address:{{\Auth::user()->organization->address}}</td>
        <td colspan="9" style="text-align: right;">@if($months) Month: {{$months}}
            @else
                Date: {{ date('dS M y', strtotime($startdate)) }} - {{ date('dS M y', strtotime($enddate)) }}
            @endif</td>
    </tr>
    <tr>
        <td style="text-align: left;">PAN:{{\Auth::user()->organization->vat_id}}</td>
    </tr>
    <tr>
        <td style="text-align: left;">Sales Book</td>
    </tr>

    </thead>
</table>
<table class="table-bordered">
    <thead>
    <tr class="bg-info">

        <th colspan="7" style="text-align: center; " >Bill/Invoice</th>
        <th rowspan="2">Total Sales</th>
        <th rowspan="2">Tax Free Sales</th>
        <th rowspan="2">Discount</th>
        <th colspan="2" style="text-align: center; ">
            Taxable Sales
        </th>
        <th colspan="4" style="text-align: center;">Export</th>

    </tr>
    <tr class="bg-info">
        <th>Date</th>
        <th>Bill No</th>
        <th>Guest</th>
        <th>Pos Guest</th>
        <th>Guest PAN</th>
        <th>Product/Service Name</th>
        <th>Prod/Serv Quantity</th>

        <th>Taxable Amount</th>
        <th>TAX</th>

        <th>Exp Prod/Serv Amount</th>
        <th>Exp Country</th>
        <th>Exp Certificate No.</th>
        <th>Exp Certificate Date</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $n = 0;
    $pos_total_amount = 0;
    $pos_s_charge=0;
    $pos_taxable_amount = 0;
    $pos_tax_amount = 0;
    $pos_discount_amount = 0;
    $pos_non_taxable_amount = 0;

    $allTotal = [];

    ?>
    @if(isset($orders_print) && !empty($orders_print))
        @foreach($orders_print as $o)
            <tr>
                <td>{{$o->bill_date}}
                    <?php
                    $temp_date = explode(" ",$o->bill_date );
                    $temp_date1 = explode("-",$temp_date[0]);
                    $cal = new \App\Helpers\NepaliCalendar();
                    //nepali date
                    $a = $temp_date1[0];
                    $b = $temp_date1[1];
                    $c = $temp_date1[2];
                    $d = $cal->eng_to_nep($a,$b,$c);
                    $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                    ?><br>
                    <small> {!! $nepali_date !!}</small>
                </td>
                <td>
                {{$invoice_type=='abbr'?'AI-':'TI'}}{{$o->outlet->outlet_code}}{!! $o->bill_no !!}

                <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>

                <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                <td>@if($o->pos_customer_id){{$o->client->vat}}@elseif($o->reservation_id) {{$o->reservation->client->vat??''}} @else{{$o->folio->reservation->client->vat??''}} @endif</td>
                <td>Product/Service</td>
                <td>{{$o->total_items_quantity}}</td>

                <td>{{$o->total_amount}}</td>

                <td>{{$o->non_taxable_amount}}</td>
                <td> {{ $o->discount_amount }}</td>

            <!-- <td> {{ $o->outlet->name}} </td> -->


                <td>{!! number_format($o->taxable_amount,2) !!}</td>
                <td>{!! number_format($o->tax_amount,2) !!}</td>

                <?php
                $pos_total_amount   = $pos_total_amount + $o->total_amount;
                $pos_s_charge       = $pos_s_charge + $o->service_charge;
                $pos_taxable_amount = $pos_taxable_amount+$o->taxable_amount;
                $pos_non_taxable_amount = $pos_non_taxable_amount+$o->non_taxable_amount;
                $pos_tax_amount     = $pos_tax_amount+$o->tax_amount;
                $pos_discount_amount += $o->discount_amount;
                $paidBy =   ReservationHelper::paidByArr($o->payments);

                //$allTotal [$paidBy]  += $o->total_amount;

                ?>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>


            </tr>
            @if($o->is_bill_active == '0')

                <tr class="bg-danger">
                    <td>{{$o->bill_date}}
                        <?php
                        $temp_date = explode(" ",$o->bill_date );
                        $temp_date1 = explode("-",$temp_date[0]);
                        $cal = new \App\Helpers\NepaliCalendar();
                        //nepali date
                        $a = $temp_date1[0];
                        $b = $temp_date1[1];
                        $c = $temp_date1[2];
                        $d = $cal->eng_to_nep($a,$b,$c);
                        $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                        ?><br>
                        <small> {!! $nepali_date !!}</small>
                    </td>
                    <td> Ref of {{$invoice_type=='abbr'?'AI-':'TI-'}}{{$o->outlet->outlet_code}}{!! $o->bill_no !!} CN-{{$invoice_type=='abbr'?'AI-':'TI-'}}{{$o->outlet->outlet_code}}{{$o->credit_note_no}}
                        <input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                    <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>

                    <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                    <td>@if($o->pos_customer_id){{$o->client->vat}}@elseif($o->reservation_id) {{$o->reservation->client->vat}} @else{{$o->folio->reservation->client->vat}} @endif</td>
                    <td>Product/Service</td>
                    <td>-{{$o->total_items_quantity}}</td>

                    <td>-{{$o->total_amount}}</td>

                    <td>-{{$o->non_taxable_amount}}</td>
                    <td> -{{ round($o->service_charge,3) }}
                        @if($o->discount_note == 'percentage')
                            {{ $o->subtotal * ($o->discount_percent / 100) }}
                        @else
                            {{ $o->discount_percent }}
                        @endif
                    </td>

                <!-- <td> {{ $o->outlet->name}} </td> -->


                    <td>-{!! number_format($o->taxable_amount,2) !!}</td>
                    <td>-{!! number_format($o->tax_amount,2) !!}</td>
                    <?php
                    $pos_total_amount       = $pos_total_amount - $o->total_amount;
                    $pos_s_charge           = $pos_s_charge -$o->service_charge;
                    $pos_taxable_amount     = $pos_taxable_amount-$o->taxable_amount;
                    $pos_non_taxable_amount     = $pos_non_taxable_amount-$o->non_taxable_amount;
                    $pos_tax_amount         = $pos_tax_amount-$o->tax_amount;
                    $pos_discount_amount -= $o->discount_amount;

                    $paidBy =   ReservationHelper::paidByArr($o->payments);
                    //$allTotal [$paidBy]  = $allTotal [$paidBy] - $o->total_amount;
                    ?>
                    <td class="text-center">-</td>
                    <td class="text-center">-</td>
                    <td class="text-center">-</td>
                    <td class="text-center">-</td>


                </tr>

            @endif
        @endforeach
    @endif
    <tr>

        <th colspan="7" style="text-align:right">
            Total Amount:
        </th>
        <td style="font-size: 16.5px"> <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_total_amount,2) }} </strong></td>
        <td style="font-size: 16.5px">
            <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_non_taxable_amount,2) }} </strong>
        </td>        <td><strong>{{env('APP_CURRENCY')}}  {{ $pos_discount_amount }}</strong></td>

        <td style="font-size: 16.5px">
            <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_taxable_amount,2) }} </strong>
        </td>
        <td style="font-size: 16.5px">
            <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_tax_amount,2) }} </strong>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
    <!-- <tfoot> -->







    {{--   <!-- <tr style="font-size: 16.5px">
          <td colspan="5"></td>
          <td >Summary</td>
          <td></td>
      </tr>

      @foreach($allTotal as $key=>$value)
          <tr style="font-size: 16.5px">
              <td colspan="5"></td>
              <td>{{ ucfirst($key) }}</td>
              <td>{{env(APP_CURRENCY)}}{{ number_format($value,2) }}</td>
          </tr>
      @endforeach --}}




    <!-- </tfoot> -->
</table>

</body>
</html>

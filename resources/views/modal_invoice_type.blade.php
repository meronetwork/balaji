<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">{{ $modal_title }}</h4>
</div>
<form method="post" action="{{ $modal_route }}">
        {{ csrf_field() }}

<div class="modal-body">
    @if($error)
        <div>{{{ $error }}}</div> 
       
    @else
       <input type="hidden" name="folio_id" value="{{$id}}">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="invoice_type" id="exampleRadios1" value="detailed" checked>
          <label class="form-check-label" for="exampleRadios1">
            Detailed
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="invoice_type" id="exampleRadios1" value="summarized" >
          <label class="form-check-label" for="exampleRadios1">
            Summarized
          </label>
        </div>
    @endif
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('general.button.cancel') }}</button>
    @if(!$error)
     <button type="Submit" name="submit" class="btn btn-primary">Ok</button> 
</form>
    @endif
</div>

 
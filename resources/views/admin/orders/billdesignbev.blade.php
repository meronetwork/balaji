<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ \Config::get('restro.APP_COMPANY', env('APP_COMPANY'))  }} | INVOICE</title>
    <style type="text/css">

        @media print {

            @page { margin: 0; }

            body { margin: 0.6cm;font-size:11px!important; }

        }
        hr.dotted {
            border-top: 1px dashed black;
        }

        .dotted-thead th {
            border-bottom: 1px dashed black;
        }

        #bg-text {
            color: lightgrey;
            position: absolute;
            left: 0;
            right: 0;
            top: 40%;
            text-align: center;
            margin: auto;
            opacity: 0.5;
            z-index: 2;
            font-size: 80px;
            transform: rotate(330deg);
            -webkit-transform: rotate(330deg);
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }
        .amountSummary td{
            white-space: nowrap;
        }
    </style>
</head>
<body onload="window.print();" onafterprint="myFunction()">
<?php
$pagesToPrint =  $print_no  < 1 ? 2 : 1;
$nepaliCalander = new \App\Helpers\NepaliCalendar();
?>
@for($i=0;$i<$pagesToPrint;$i++) <div class="container">
    <address style="text-align: center;margin-top: 20px">
        {{ \Config::get('restro.APP_COMPANY', env('APP_COMPANY'))  }}<br>
        {{ \Config::get('restro.APP_ADDRESS1', env('APP_ADDRESS1'))  }}<br>
        Tel: {{ \Config::get('restro.APP_PHONE1', env('APP_PHONE1'))  }}<br/>
        @if($print_no == 0 && $i == 0) @if($invoice_type=='abbr')Abbreviated @endif TAX Invoice @else Invoice @endif<br>
        @if($print_no > 0) Copy of Original {{$print_no}} @endif<br>

    </address><br>
    <div class="bill-details">
        VAT No: {{ \Config::get('restro.TPID', env('TPID'))  }}<br>
        Bill NO: {{$ord->outlet->outlet_code}}{{$ord->bill_no}}<br>
        Invoice Date (AD): {{$ord->bill_date}}<br>
        Transaction Date: {{ $ord->transaction_date }}<br>
        Name: @if($ord->client){{$ord->client->name}}@elseif($ord->reservation->client){{$ord->reservation->client->name}}@else{{$ord->reservation->guest_name}}@endif<br>
        @if($ord->client || $ord->reservation->client)
            Customer VAT: @if($ord->client){{$ord->client->vat}}@elseif($ord->reservation->client){{$ord->reservation->client->vat}}@endif<br>
        @endif
        Address: @if($ord->client){{$ord->client->location}}@elseif($ord->reservation->client){{$ord->reservation->client->location}}@endif<br>
        Table No: {{$ord->restauranttable->table_number}}<br>
        Payment Mode: {!!  ReservationHelper::paidByArr($ord->payments)  !!}
    </div>
    <hr class="dotted">
    <table>
        <thead>
        <tr class="dotted-thead">
            <th style="text-align: left;">Sn.Particulars</th>
            <th>Qty</th>
            <th>Rate</th>
            <th>Amount({{env('APP_CURRENCY')}})</th>
        </tr>
        </thead>
        <tbody>

        @if($orderDetails)
            <?php  $totalQty  = 0;
            $num=1;
            ?>
            @foreach($orderDetails as $key=>$orddtl)
                <?php
                $n= 0;
                $qty = 0;
                $ordTotal = 0;

                ?>
                @foreach($orddtl as $odv)
                    <?php
                    $catName = $odv->ptype->name;
                    ?>
                    <tr>
                        @if($odv->is_inventory == 1)
                            <td>{{$num}}.{{$odv->product->name}}</td>
                        @else
                            <td>{{$num}}.{{$odv->description}}</td>
                        @endif
                        <td style="text-align: center;">{{$odv->quantity}}</td>
                        <?php
                        $qty = $qty + $odv->quantity;
                        $totalQty += $odv->quantity;
                        $ordTotal += $odv->total;
                        ?>
                        <td>{{$invoice_type=='abbr'?number_format($odv->price,2):($odv->tax!=1?number_format($odv->price,2):number_format($odv->taxable_amount,2))}}</td>
                        <td>{{$invoice_type=='abbr'?number_format($odv->total,2):($odv->tax!=1?number_format($odv->total,2):number_format($odv->total_taxable_amount,2))}}</td>
                    </tr>
                    <?php
                    $num=$num+1;
                    ?>
                @endforeach

                <tr>
                    <td colspan="6" style="border: 1px solid dotted;"></td>
                </tr>

            @endforeach
        @endif
        <tr>

            <td style="border-top: 1px  dashed black;"> Total Qty </td>
            <td style="text-align: center;border-top: 1px  dashed black;">{{$totalQty}}</td>
            <td  style="border-top: 1px  dashed black;"></td>
            <td  style="border-top: 1px  dashed black;"></td>

        </tr>
        </tbody>
        <tfoot class="amountSummary">
        @if($invoice_type!='abbr')
            <tr>
                <td colspan="3" style="border-top: 1px  dashed black;text-align: left;">Gross Amount:</td>
                <td style="border-top: 1px  dashed black;">{{number_format($ord->taxable_amount+$ord->non_taxable_amount+$ord->discount_amount,2)}}</td>
            </tr>
            @if($ord->discount_amount||$ord->discount_amount>0)
{{--                <tr>--}}
{{--                    <td colspan="3" style="text-align: left;">Discount Percentage:</td>--}}
{{--                    <td>{{ ($ord->discount_percent ? ($ord->discount_percent.'%')  : '0') }}</td>--}}
{{--                </tr>--}}
                <tr>

                    <td colspan="3" style="text-align: left;">Discount Amount:</td>
                    <td>{{ ($ord->discount_amount ? number_format($ord->discount_amount,2)  : '0') }}</td>
                </tr>
            @endif
            <tr>
                <td colspan="3" style="border-top: 1px  dashed black;text-align: left;">Tax Free Amount:</td>
                <td style="border-top: 1px  dashed black;">{{number_format($ord->non_taxable_amount,2)}}</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left;">Taxable Amount:</td>
                <td>{{number_format($ord->taxable_amount,2)}}</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left;">Tax Amount:</td>
                <td>{{number_format($ord->tax_amount,2)}}</td>
            </tr>


        @else
            @if($ord->discount_amount||$ord->discount_amount>0)
                <tr>

                    <td colspan="3" style="border-top: 1px  dashed black;text-align: left;">Gross Amount:</td>
                    <td style="border-top: 1px  dashed black;">{{number_format($ord->subtotal,2)}}</td>
                </tr>

{{--                <tr>--}}
{{--                    <td colspan="3" style="text-align: left;border-top: 1px  dashed black;">Discount Percentage:</td>--}}
{{--                    <td style="border-top: 1px  dashed black;">{{ ($ord->discount_percent ? ($ord->discount_percent.'%')  : '0') }}</td>--}}
{{--                </tr>--}}
                <tr>

                    <td colspan="3" style="text-align: left;">Discount Amount:</td>
                    <td>{{ ($ord->discount_amount ?number_format($ord->discount_amount,2)  : '0') }}</td>
                </tr>
            @endif
        @endif
        <tr>
            <td colspan="3" style="text-align: left;border-top: 1px  dashed black;"><strong>Total:</strong></td>
            <td style="border-top: 1px  dashed black;"><strong>{{ number_format($ord->total_amount,2) }}</strong></td>
        </tr>

        </tfoot>
    </table>
    <hr class="dotted">
    <?php
    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
    ?>
    <span>In Words {{env('APP_CURRENCY')}}.{{ucfirst($f->format($ord->total_amount))}} only.</span>
    <hr class="dotted">
    <div class="greeting">
        Thank You for the business. Do not sign the bill, if you have paid cash
    </div>
    <hr class="dotted"><br><br>
    <span style="float: right;">

            <span>-------------------------------</span><br>
            <span style="text-align: center !important;margin-left: 55px;">Signature</span>

        </span><br><br>
    <hr class="dotted">
    <div class="time">
        <?php $bimal = gethostname(); ?>{{\Auth::user()->first_name}} {{\Auth::user()->last_name}} ({{ date('Y-m-d h:i:sa') }})
    </div>
</div>
@if($i == 0 && $pagesToPrint > 1)
    <div class="page-break"></div>
@endif
@endfor
</body>
</html>

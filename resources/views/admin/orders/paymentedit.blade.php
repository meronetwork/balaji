@extends('layouts.master')

@section('head_extra')

@include('partials._head_extra_select2_css')

@endsection
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {!! $page_title !!}

        <small>{!! $page_description !!}</small>
        <a class="btn btn-primary btn-xs" href="javascript::void()" onclick="printBill()" title="In case No Pop Pop Occur"> <i class="fa fa-print"></i> Print</a>
        <a class="btn btn-danger btn-xs"  href="javascript::void()" id='addMorePayments'>Add More Payments</a>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body">

                <form action="/admin/payment/orders/{{$sale_id}}?invoice_type={{request('invoice_type')}}" method="post">

                    {{ csrf_field() }}

                    <div class="content col-md-12">
                        <div id="main_payment" class="paymentsDiv">
                        @foreach($topPayments as $key=>$plist)
                        @php $divid = 'main_payment' @endphp
                          @include('admin.orders._salessettlepartials')
                          <input type="hidden" name="old_payments[]" value="{{ $plist->id }}">
                        @endforeach
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Date</label>
                                        <div class="input-group " style="margin-right: 12px;">
                                            <input type="text" name="date" id="target_date" value="{{$plist->date}}" placeholder="Date" class="form-control input-sm datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Ref #</label>
                                    <div class="input-group ">
                                        <input type="text" name="reference_no" placeholder="Reference No"  value="{{ $plist->reference_no }}" class="form-control input-sm" required>
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-building"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                          <div class="more_payments">
                            <div id='more_payments'></div>
                            @foreach($subPayments as $key=>$plist)
                            @php $divid =rand() .'div'  @endphp
                            <div id='{{$divid}}' class="paymentsDiv">
                                <div class="bg-danger">
                                    @include('admin.orders._salessettlepartials')
                                </div><i class="fa fa-trash remove-this deletable" data-id='{{$divid}}'></i>
                            </div>
                             <input type="hidden" name="old_payments[]" value="{{ $plist->id }}">
                            @endforeach
                        </div>

                        <input type="hidden" name="sale_id" value="{{$sale_id}}">

                         <div class="row"id='given_amount_div'>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Given Amount</label>
                                    <div class="input-group ">
                                        <input type="text" name="given_amount"
                                        placeholder="Given Amount" id="given_amount"  class="form-control input-sm" >
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-money"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>



                    <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Cash to return</label>
                                    <div class="input-group ">
                                        <input type="text" name="return_amount"
                                        placeholder="Return Amount" id="return_amount"  class="form-control input-sm" required readonly="">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-money"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Attachment</label>
                                    <div class="input-group ">
                                        <input type="file" name="attachment" >

                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="row">
                            <div class="col-md-8">
                                <label for="inputEmail3" class="control-label">
                                    Note
                                </label>

                                <textarea class="form-control input-sm" name="note" id="description" placeholder="Write Note">{{ $plist->note }}</textarea>
                            </div>
                        </div>


                    </div><!-- /.content -->

                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="Submit" value="Resettle Now" class="btn btn-primary">
                            <a href="/admin/hotel/orders/outlet/{{\App\Models\Orders::find($sale_id)->outlet_id}}/settlement" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.col -->

</div><!-- /.row -->

@endsection

@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<script type="text/javascript">

    function addAutoComplete(targetDiv){

        $(`${targetDiv} #travel_agent,${targetDiv} #city_ledger,${targetDiv} #staff_name`).autocomplete({

            source: function(request, response) {
                $.getJSON(`/admin/getLedgers?types=${$(targetDiv + ' #colorselector').val()}&term=${request.term}`, response);
            }
            , minLength: 1,
            change: function(event,ui){
                $(this).val((ui.item ? ui.item.value : ""));
            }

        });


    }

    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });
    function appendPaymentDiv(){

        let mainPayments =   $('#main_payment');

      let randomid = Math.random().toString(36).substring(7);

      $('#more_payments').after(`<div id='${randomid}' style='padding:10px;'><br><div class='bg-danger' >${mainPayments.html()}</div><i class='fa fa-trash remove-this deletable' data-id='${randomid}'></i>  </div>`);

      $(`#${randomid}`).find('#colorselector').attr('target-div',randomid);

      $(`#${randomid}`).find('#colorselector').val('cash');

      let amount = $(`#${randomid}`).find('.settle_amount');

      amount.prop('readonly',false);
      amount.val('');
      addAutoComplete(`#${randomid}`);

      $(`#${randomid}`).find('#colorselector').trigger('change');

      return randomid;
    }

    $('#addMorePayments').click(function(){


        appendPaymentDiv();



    });





    $(function() {

        $(document).on('change','.colorselector',function(){

            let id = $(this).val();
            let targetDiv = '#'+$(this).attr('target-div');

            $(targetDiv + ' #city_ledger').prop('required',false);

            if(id == 'cash'){
                $(targetDiv +' #given_amount_div').show()
            }else{
                $(targetDiv +' #given_amount_div').hide()
            }
            if(id =='city-ledger'){
                $(targetDiv +' #city_ledger').prop('required',true);
            }
            $(targetDiv +' .colors').hide();
            $(targetDiv +' #' + $(this).val()).show();



        });
        $('.paymentsDiv').each(function(){


            let id = '#'+$(this).attr('id');
            addAutoComplete(id);
            let parentDiv = $(id);
            parentDiv.find('.colorselector').trigger('change');
            console.log(parentDiv.find('.colorselector'));

        });

    });




    $(document).ready(function() {
        addAutoComplete('#main_payment');
    });

</script>

<script type="text/javascript">
    $('#room_no').keyup(function() {

        $("#room_no").autocomplete({
            source: "/admin/getRooms"
            , minLength: 1
        });

    });

    $('#room_no').change(function() {

        if ($(this).val() != '') {
            $.ajax({
                url: "/admin/users/ajax/getresv"
                , data: {
                    room_num: $(this).val()
                }
                , dataType: "json"
                , success: function(data) {

                    var result = data.data;
                    $('#room_detail').html(result);

                }
            });
        }

    });

    $('textarea').each(function() {
        $(this).val($(this).val().trim());
    });


    const mainTotal = '{{ $payment_remain  }}';
    $('#given_amount').keyup(function(){

        let value = $(this).val();

        let amount = Math.abs(mainTotal);

        let returnamount = Number(value) - amount;

        $('#return_amount').val(returnamount);

    });




    function printBill(){


        window.open( "/admin/order/thermalprint/{{ $sale_id }}",'_blank','toolbar=yes');


    }
    function settlementAmount(){

        var totalAmount = 0;

        $('.more_payments .settle_amount').each(function(){
            totalAmount += Number($(this).val());
        });

        let finalAmount = Number(mainTotal) -totalAmount;

        $('#main_payment .settle_amount').val(finalAmount);

    }


    $(document).on('change','.more_payments .settle_amount',function(){
        let actualtotal = $('#main_payment .settle_amount').val();
        alert(actualtotal);
        if(Number($(this).val()) > actualtotal){
            let exceedAmount =Number($(this).val()) - actualtotal;
            alert("Amount Exceed by "+exceedAmount);
            $(this).val('0');


        }

        settlementAmount();



    });

$(document).on('click','.remove-this',function(){

    let id = $(this).attr('data-id');
    $(`#${id}`).remove();
    settlementAmount();

})

@if(session()->get( 'printbill' ))

    printBill();

@endif




</script>







@endsection

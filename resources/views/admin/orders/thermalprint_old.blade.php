<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ env('APP_COMPANY')}} | INVOICE</title>
        CASHIER: RAMA
    </head>
    <body onload="window.print();">

        <div class="ticket">
              <p class="centered">

                {{ env('APP_COMPANY') }}
                <br> POS Recipt
                <br>{{env('APP_ADDRESS1')}}
                <br>{{env('APP_ADDRESS2')}}
                @if($ord->reservation) 
                <br>{{ $ord->reservation->client->name }}
                <br>Vat Id: {{ $ord->reservation->client->vat }}
                @endif

              </p> 
            <table>
                <thead>
                    <tr>
                        <th class="description">Product</th>
                        <th class="price">Price</th>
                        <th class="price">Qty</th>
                        <th class="price">Total</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  $n= 0;
                  ?>
                  @foreach($orderDetails as $odk => $odv)
                  <tr>
                    @if($odv->is_inventory == 1)
                      <td class="description">{{ $odv->product->name }}</td>
                    @elseif($odv->is_inventory == 0)
                      <td class="description">{{ $odv->description }}</td>
                    @endif
                    <td class="price">{{$odv->price}}</td>
                    <td class="quantity">{{ $odv->quantity }}</td>
                    <td class="price">{{ env('APP_CURRENCY').' '.$odv->total }}</td>
                  </tr>
                   @endforeach
                   <tr>
                        <td class="quantity"></td>
                        <td class="quantity"></td>
                        <td class="description">SubTotal</td> 
                        <td class="price">{{ env('APP_CURRENCY').' '. number_format($ord->subtotal,2) }}</td>
                    </tr>
                    @if($ord->discount_note == 'percentage')
                        <tr>
                            <td class="quantity"></td>
                            <td class="quantity"></td>
                            <td class="description">Discount(%)</td> 
                            <td class="price">{{ ($ord->discount_percent ? $ord->discount_percent  : '0') }}%</td>
                        </tr>
                    @else
                         <tr>
                            <td class="quantity"></td>
                            <td class="quantity"></td>
                            <td class="description">Discount</td> 
                            <td class="price">{{ ($ord->discount_amount ? $ord->discount_amount  : '0') }}</td>
                         </tr>
                    @endif
                    <tr>
                        <td class="quantity"></td>
                        <td class="quantity"></td>
                        <td class="description">Service Charge:</td>
                        <td class="price">{{ env('APP_CURRENCY').' '. number_format($ord->service_charge,2) }}</td>
                    </tr>
                    <tr>
                        <td class="quantity"></td>
                        <td class="quantity"></td>
                        <td class="description">Amount With Service :</td>
                        <td class="price">{{ env('APP_CURRENCY').' '. number_format($ord->amount_with_service,2) }}</td>
                    </tr>
                    <tr>
                        <td class="quantity"></td>
                        <td class="quantity"></td>
                        <td class="description">Taxable Amount:</td>
                        <td class="price">{{ env('APP_CURRENCY').' '. number_format($ord->taxable_amount,2) }}</td>
                    </tr>
                     <tr>
                        <td class="quantity"></td>
                        <td class="quantity"></td>
                        <td class="description">Tax Amount(13%)</td>
                        <td class="price">{{ env('APP_CURRENCY').' '. number_format($ord->tax_amount,2) }}</td>
                    </tr>
                    <tr>
                        <td class="quantity"></td>
                        <td class="quantity"></td>
                        <td class="description">TOTAL</td>
                        <td class="price">{{ env('APP_CURRENCY').' '. number_format($ord->total_amount,2) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
       
  
    </body>
</html>
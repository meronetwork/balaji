<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css">
        hr.dotted {
            border-top: 1px dashed black;
        }

        .dotted-thead th {
            border-bottom: 1px dashed black;
        }

        #bg-text {
            color: lightgrey;
            position: absolute;
            left: 0;
            right: 0;
            top: 40%;
            text-align: center;
            margin: auto;
            opacity: 0.5;
            z-index: 2;
            font-size: 80px;
            transform: rotate(330deg);
            -webkit-transform: rotate(330deg);
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

    </style>
</head>
<body onload="window.print();" onafterprint="myFunction()">
    <?php 
        $pagesToPrint =  $print_no  < 1 ? 2 : 1;
    
    ?>
    @for($i=0;$i<$pagesToPrint;$i++) <div class="container">
        <address style="text-align: center;margin-top: 20px">
            {{env('APP_COMPANY')}}<br>
            {{$ord->outlet->name}}<br>
            {{env('APP_ADDRESS1')}}<br>
            @if(env('APP_ADDRESS2'))
            {{env('APP_ADDRESS2')}}<br>
            @endif
            @if($print_no == 0 && $i == 0) TAX Invoice @else Invoice @endif<br>
            @if($print_no > 0) Copy of Original {{$print_no}} @endif<br>

        </address><br>
        <div class="bill-details">
            Vat No: {{env('TPID')}}<br>
            Bill NO: {{$ord->outlet->outlet_code}}{{$ord->bill_no}}<br>
            Invoice Date: {{$ord->bill_date}}<br>
            Name: @if($ord->client){{$ord->client->name}}@elseif($ord->reservation->client){{$ord->reservation->client->name}}@else{{$ord->reservation->guest_name}}@endif<br>
            @if($ord->client || $ord->reservation->client)
            Customer VAT: @if($ord->client){{$ord->client->vat}}@elseif($ord->reservation->client){{$ord->reservation->client->vat}}@endif<br>
            @endif
            Address: @if($ord->client){{$ord->client->location}}@elseif($ord->reservation->client){{$ord->reservation->client->location}}@endif<br>
            Tel No: <br>
            Payment Mode:
        </div>
        <hr class="dotted">
        <table>
            <thead>
                <tr class="dotted-thead">
                    <th>Sn</th>
                    <th>Particulars</th>
                    <th>Qty</th>
                    <th>Rate</th>
                    <th>Amount({{env('APP_CURRENCY')}})</th>
                </tr>
            </thead>
            <tbody>
                <?php
                   $n= 0;
                   $qty = 0;
                ?>
                @if($orderDetails)
                @foreach($orderDetails as $odv)
                <tr>
                    <td>{{++$n}}</td>
                    @if($odv->is_inventory == 1)
                    <td>{{$odv->product->name}}</td>
                    @else
                    <td>{{$odv->description}}</td>
                    @endif
                    <td>{{$odv->quantity}}</td>
                    <?php 
                            $qty = $qty + $odv->quantity;
                         ?>
                    <td>{{number_format($odv->price,2)}}</td>
                    <td>{{number_format($odv->total,2)}}</td>
                </tr>
                @endforeach

                @endif

            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td colspan="2" style="border-top: 1px  dashed black;">Gross Amount</td>
                    <td style="border-top: 1px  dashed black;">:</td>
                    <td style="border-top: 1px  dashed black;">{{number_format($ord->subtotal,2)}}</td>
                </tr>
                @if($ord->discount_note == 'percentage')
                <tr>
                    <td></td>
                    <td colspan="2">Discount Percent(%):</td>
                    <td>:</td>
                    <td>{{ ($ord->discount_percent ? $ord->discount_percent  : '0') }}%</td>
                </tr>
                @else
                <tr>
                    <td></td>
                    <td colspan="2">Discount Amount:</td>
                    <td>:</td>
                    <td>{{ ($ord->discount_percent ? $ord->discount_percent  : '0') }}</td>
                </tr>
                @endif
                <tr>
                    <td></td>
                    <td colspan="2">Service Charge</td>
                    <td>:</td>
                    <td>{{ number_format($ord->service_charge,2) }}</td>
                </tr>

                <tr>
                    <td></td>
                    <td colspan="2">Vatable Amount:</td>
                    <td>:</td>
                    <td>{{ number_format($ord->taxable_amount,2) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">Vat Amount(13%)</td>
                    <td>:</td>
                    <td>{{ number_format($ord->tax_amount,2) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"><strong>Total</strong></td>
                    <td>:</td>
                    <td><strong>{{ number_format($ord->total_amount,2) }}</strong></td>
                </tr>

                <tr>
                    <td></td>
                    <td colspan="2" style="border-top: 1px  dashed black;">Total Qty</td>
                    <td style="border-top: 1px  dashed black;">:</td>
                    <td style="border-top: 1px  dashed black;">{{$qty}}</td>
                </tr>

            </tfoot>
        </table>
        <hr class="dotted">
        <?php 
            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
         ?>
        <span>In Words {{env('APP_CURRENCY')}}.{{ucfirst($f->format($ord->total_amount))}} only.</span>
        <hr class="dotted">
        <div class="greeting">
            Thank You for the business

        </div>
        <hr class="dotted">
        <div class="time">
            <?php $bimal = gethostname(); ?>{{\Auth::user()->first_name}} {{\Auth::user()->last_name}} ({{ date('Y-m-d h:i:sa') }})
        </div>
        </div>
        @if($i == 0 && $pagesToPrint > 1)
        <div class="page-break"></div>
        @endif
        @endfor
</body>
</html>
<script>
    // var print_no = '{!! $print_no !!}';
    // if (print_no < 1) {

    //     setTimeout(function() {
    //         location.reload();
    //     }, 100);

    // }
    // function myFunction(){
    //     alert("OK MAN");
    // }

</script>

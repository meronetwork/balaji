@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        <i class="fa fa-bars"></i> {{ $outlet->name }}

        <?php
                   $number = \App\Models\Orders::where('outlet_id', \Request::segment(4))->sum('total_amount');         
                  ?>
        <small> Manage {{ ucfirst(\Request::get('type'))}}
        </small>
    </h1>
    <br/>

    @if($outlet->fnb_outlet == "1")
    <a class="btn btn-primary btn-sm" href="/admin/hotel/showtablelists?type=invoice&outlet_id={{\Request::segment(4)}}"> New Order + </a>
    @else
    <a class="btn btn-primary btn-sm" href="/admin/orders/create?type=invoice&outlet_id={{\Request::segment(4)}}"> New Order + </a>

    @endif

   
</section>

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-body">
                <span id="index_lead_ajax_status"></span>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" id="orders-table">
                        <thead>
                            <tr class="bg-blue">
                                <th>Order Date</th>
                                <th>Order No</th>
                                <th>Table</th>
                                <th>Hotel Guest</th>
                                <th>POS Guest</th>
                               
                                <th>Total</th>
                                {{-- <th>Tools</th>  --}}

                                @if($outlet->fnb_outlet == "1")
                                <th> BOT</th>
                                <th> KOT</th>
                                @endif

                                <th>Estimate</th>
                                @if($outlet->fnb_outlet)
                                <th>Ready Status</th>
                                @endif
                                <th>History</th>
                                <th> Post Bill </th>
                                <th> Action </th>
                                <th>Print Bill</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                        $pos_total_amount = 0; 
                                        $pos_taxable_amount = 0; 
                                        $pos_tax_amount = 0;
                                   ?>
                            @if(isset($orders) && !empty($orders))
                            @foreach($orders as $o)
                            <tr>
                                <input type="hidden" name="_id" value="{{$o->id}}" class="order_id">

                                <td>{{$o->bill_date}}</td>
                                <td style="font-size: 16px"><b>

                                    
                                        @if($o->ready_status == 'checkedout' || $o->ordermeta->is_posted)
                                        <i class="fa fa-ban"></i> {{$o->outlet->short_name}}{!! $o->id !!}
                                        @else
                                        <i class="fa fa-edit"></i> <a href="/admin/orders/{{$o->id}}/edit?type=invoice">{{$o->outlet->short_name}}{!! $o->id !!}</a><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}">
                                        @endif

                                    </b></td>
                                    <?php 
                                        $table_name = \App\Models\PosTable::find($o->table);
                                    ?>
                                <td style="font-size: 16px">{{$table_name->tablearea->name }} > {{$table_name->table_number}}</td>
                                
                                <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name}}@elseif($o->folio_id) {{$o->folio->reservation->client->name}} @else {{$o->folio->reservation->guest_name}}</small>@endif</td>

                                <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                            
                                <?php $pos_taxable_amount = $pos_taxable_amount+$o->taxable_amount;
                                                    $pos_tax_amount = $pos_tax_amount+$o->tax_amount;

                                                    $pos_total_amount = $pos_total_amount + $o->total_amount;
                                               ?>
                                {{-- <td>
                                                <a href="/admin/order/generatePDF/{{$o->id}}"><i class="fa fa-download"></i></a>
                                <a href="/admin/order/print/{{$o->id}}" style="color: grey;"><i class="fa fa-print" title="default"></i></a>
                                </td> --}}
                                <td style="font-size: 16px"><b>{{ env('APP_CURRENCY')}} {{$o->total_amount}}</b></td>


                                @if($outlet->fnb_outlet == "1")
                                <td>
                                    @if($o->ready_status == 'checkedout')
                                    <i class="fa fa-ban"></i>
                                    @else
                                    <a href="/admin/order/botprint/{{$o->id}}" style="color: red;"><i class="fa fa-print" title="BOT Print"></i></a>
                                    @endif
                                </td>

                                <td>
                                    @if($o->ready_status == 'checkedout')
                                    <i class="fa fa-ban"></i>
                                    @else
                                    <a href="/admin/order/kitchenprint/{{$o->id}}" style="color: red;"><i class="fa fa-print" title="Kitchen Print"></i></a>
                                    @endif
                                </td>
                                @endif

                                <td>
                                    @if($o->ready_status == 'checkedout')
                                    <i class="fa fa-ban"></i>
                                    @else
                                    <a href="/admin/order/estimateprint/{{$o->id}}" style="color: red;"><i class="fa fa-print" title="Estimate Print"></i></a>
                                    @endif
                                </td>
                                
                                @if($outlet->fnb_outlet)
                                @if($o->ready_status == 'checkedout')

                                <td>{{$o->ready_status}}</td>

                                @else

                                <td>
                                    {!! \Form::select('ready_status', ['ordered'=>'Ordered','cooking'=>'Cooking','cooked'=>'cooked','served'=>'Served'], $o->ready_status, ['class' => 'index_ready_status form-control'])!!}
                                </td>

                                @endif
                                <td>
                                    <a href="{{route('admin.orders.ordershistroy',$o->id)}}" class="text-danger" title="View History"><i class="fa  fa-history"></i></a>
                                </td>
                                @else
                                <td></td>
                                @endif

                                 @if($o->ordermeta->is_posted )

                                 <td>
                                     <a class="btn btn-success btn-xs" href="#">Posted</a>
                                 </td>

                                 @else
                                 <td>
                                     <form method="post" action="/admin/hotel/postbills/outlets/{{$o->id}}">
                                         {{ csrf_field() }}
                                         <button class="btn btn-danger btn-xs" type="submit">Post</button>
                                     </form>
                                 </td>

                                 @endif
                                 <td>
                                     @if($o->ordermeta->settlement == 0)
                                     @if($o->ordermeta->is_posted)
                                     <a href="/admin/payment/orders/{{$o->id}}/create" title="Settle">Settle Now</a>
                                     @endif
                                     @endif
                                 </td>
                                 <td>
                                      @if($o->ordermeta->is_posted && $o->ordermeta->settlement)
                                      <a href="/admin/order/thermalprint/{{$o->id}}" target="_blank"><i class="fa fa-print"></i></a>

                                      @endif
                                    

                                 </td>


                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td colspan="4">
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_taxable_amount,2) }} </strong>
                                </td>
                               {{--  <td>
                                    <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_tax_amount,2) }} </strong>
                                </td>
                                <td>
                                    <strong>{{env('APP_CURRENCY')}} {{number_format($pos_total_amount,2)}}</strong>
                                </td> --}}
                            </tr>
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
            <div style="text-align: center;"> {!! $orders->appends(\Request::except('page'))->render() !!} </div>
        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">

    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }

</script>


<script type="text/javascript">
    $(document).on('change', '#order_status', function() {

        var id = $(this).closest('tr').find('.index_sale_id').val();

        var purchase_status = $(this).val();
        $.post("/admin/ajax_order_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

</script>
<script type="text/javascript">
    $("#btn-submit-filter").on("click", function() {

        status = $("#filter-status").val();
        type = $("#order_type").val();

        window.location.href = "{!! url('/') !!}/admin/orders?status=" + "&type=" + type;
    });

    $("#btn-filter-clear").on("click", function() {

        type = $("#order_type").val();
        window.location.href = "{!! url('/') !!}/admin/orders?type=" + type;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
    });

</script>


<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('#date1').datepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            dateFormat: 'yy-m-d'
            , sideBySide: true
            , beforeShow: function() {
                setTimeout(function() {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });
    });
    // ['ordered'=>'Ordered','cooking'=>'Cooking','cooked'=>'cooked','served'=>'Served']
    const status_color = {
        ordered: 'label-warning'
        , cooking: 'label-info'
        , cooked: 'label-danger'
        , served: 'label-success'
    }

    function changeColor() {

        $('.index_ready_status').each(function() {
            let val = $(this).val();
            $(this).attr('class', `index_ready_status form-control ${status_color[val]}`);
        })
    }
    $(document).on('change', '.index_ready_status', function() {
        let val = $(this).val();
        $(this).attr('class', `index_ready_status form-control ${status_color[val]}`);
        var orderId = $(this).parent().parent().find('.order_id').val();

        let _token = $('meta[name=csrf-token]').attr('content');
        var data = {
            _token: _token
            , id: orderId
            , op: 'status'
            , value: val
        , }
        $.ajax({
            url: `/admin/orders/${orderId}`
            , type: 'PUT'
            , data: data
            , success: function(result) {

            },

        });

    });

    changeColor();

</script>

@endsection

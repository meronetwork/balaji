@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')
<style>
    .panel .mce-panel {
        border-left-color: #fff;
        border-right-color: #fff;
    }

    .panel .mce-toolbar,
    .panel .mce-statusbar {
        padding-left: 20px;
    }

    .panel .mce-edit-area,
    .panel .mce-edit-area iframe,
    .panel .mce-edit-area iframe html {
        padding: 0 10px;
        min-height: 350px;
    }

    .mce-content-body {
        color: #555;
        font-size: 14px;
    }

    .panel.is-fullscreen .mce-statusbar {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 200000;
    }

    .panel.is-fullscreen .mce-tinymce {
        height: 100%;
    }

    .panel.is-fullscreen .mce-edit-area,
    .panel.is-fullscreen .mce-edit-area iframe,
    .panel.is-fullscreen .mce-edit-area iframe html {
        height: 100%;
        position: absolute;
        width: 99%;
        overflow-y: scroll;
        overflow-x: hidden;
        min-height: 100%;
    }
 input.total{
        width: 100% !important;
    }
</style>


@endsection
<script type="text/javascript">
function noenter() {
  return !(window.event && window.event.keyCode == 13); }


</script>
@section('content')
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<div class='row'>
    <div class='col-md-12'>
        <div class="box-body">


            <div id="orderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-tr">
                        <tr>
                            <td>
                                <input type="text" name="product_id_new[]" class="form-control product_id" required="required" value="" id="product_id" onkeyup="myfun(this)" onchange="price(this)" autocomplete="off">
                                <input type="hidden" name="includes_tax_new[]" class="includes_tax">
                                <input type="hidden" name="tax_amount_new[]" class="tax_amount">
                            </td>

                            <td>
                                <div class="input-group">

                                    <input type="text" class="form-control price" name="price_new[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required" autocomplete="off" readonly="">
                                </div>
                            </td>

                            <td>
                                <input type="number" class="form-control quantity" name="quantity_new[]" placeholder="Quantity" min="1" value="1" required="required">
                            </td>
{{--                            <td >--}}
{{--                                <input type="text" name="discount_percent_new[]" class="form-control discount_percent_line " placeholder="Percent" step="any" min="0" max="99">--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <input type="text" name="discount_amount_new[]" class="form-control discount_amount_line" placeholder="Amount" step="any">--}}
{{--                            </td>--}}


                            <td>
                                <input type="text" class="form-control" name="remarks_new[]" placeholder="Remarks">
                            </td>

                            <td>
                                <div class="input-group">
                                   {{--  <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span> --}}
                                    <input type="text" class="form-control total" name="total_new[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly">

                                </div>
                            </td>
                            <td>
                                    <a href="javascript::void(1);" >
                                        <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="color: #fff;"></i>
                                    </a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="CustomOrderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-custom-tr">
                        <tr>
                            <td>
                                <input type="text" class="form-control product" name="custom_items_name_new[]" value="" placeholder="Product" autocomplete="off" required="required">
                            </td>

                            <td>
                                <div class="input-group">

                                    <input type="text" class="form-control price" name="custom_items_price_new[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required" autocomplete="off">
                                </div>
                            </td>

                            <td>
                                <input type="text" class="form-control quantity" name="custom_items_qty_new[]" placeholder="Quantity" min="1" value="1" required="required">
                            </td>

                            <td>
                                <input type="text" class="form-control" name="remarks_custom_new[]" placeholder="Remarks">
                            </td>

                            <td>
                                <div class="input-group">

                                    <input type="text" class="form-control total" name="custom_total_new[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                </div>
                                <a href="javascript::void(1);" style="width: 10%;">
                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    {!! Form::model( $order, ['route' => ['admin.orders.update', $order->id], 'method' => 'PUT','id'=>'updateOrderForm'] ) !!}

                    <div class="panel-body">

                        <div class="col-md-12">
                           {{--  <div class="col-md-3 form-group" style="">
                                <label for="user_id">Select Reservation Customer</label>
                                <select name="reservation_id" class="form-control customer_id" id="reservation_id">
                                    <option class="form-control input input-lg" value="">Select Resv Customer</option>
                                    @if(isset($reservation))
                                    @foreach($reservation as $key => $uk)
                                    <option value="{{ $uk->id }}" @if($order && $uk->id == $order->reservation_id){{ 'selected="selected"' }}@endif>{{ '('.$uk->id.') '.$uk->guest_name.' ('.$uk->room_num.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
 --}}
                            <div class="col-md-3 hidden-sm hidden-md form-group" style="">

                                <label for="user_id">Select POS Customer<a title="Create New POS Customer" href="#" onclick="openwindowposcustomer()">[+]</a></label>
                                <select name="pos_customer_id" class="form-control pos_customer_id input-sm" id="pos_customer_id">
                                    <option class="form-control input input-lg" value="">Select POS Customer</option>
                                    @if(isset($clients))
                                    @foreach($clients as $key => $uk)
                                    <option value="{{ $uk->id }}" @if($uk->id == $order->pos_customer_id){{ 'selected="selected"' }}@endif>{{ '('.env('APP_CODE'). $uk->id.') '.$uk->name.'('.$uk->location.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>

                            <?php

                                $is_fnb = \App\Models\PosOutlets::find($order->outlet_id)
                                        ->fnb_outlet;
                            ?>

                            @if($is_fnb)

                            <div class="col-md-3 form-group" style="">
                                <label for="comment">Covers</label>
                                <input type="text" name="covers" id="name" class="form-control" value="{{$order->covers}}">
                            </div>

                            <div class="col-md-3 form-group" style="">
                                <label for="comment">Table {{$order->outlet->name}} > {{$order->restauranttable->table_number}}</label>
                                <input type="text" name="table" id="name" class="form-control" value="{{$order->table}}" readonly>
                            </div>

                            @endif
{{--                             @if($selectedOutlet->service_charge)--}}
{{--                             <div class="col-md-3">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label>Service Charge: (10 %)</label>--}}
{{--                                    <select type="text" class="form-control pull-right " name="service_type" id="service_type">--}}
{{--                                        <option value="" @if($order->service_type == "") selected="selected"@endif>Select</option>--}}

{{--                                        <option value="no" @if($order->service_type == "no") selected="selected"@endif>No</option>--}}

{{--                                        <option value="yes" @if($order->service_type == "yes") selected="selected"@endif>Yes({{env('SERVICE_CHARGE')}}%)</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            @else--}}
                             <input type="hidden" name="service_type" id='service_type' value = 'no'>

{{--                            @endif--}}
                            <div class="col-md-3 hidden-sm hide_on_tablet hidden-md">
                                <div class="form-group">
                                    <label>Invoice Type:</label>
                                    <select class="form-control pull-right" name="invoice_type" id="invoice_type" required="required">
                                        <option value="">Select</option>
                                        <option value="tax" {{$order->invoice_type=='tax'?'selected':''}}>Tax Invoice</option>
                                        <option value="abbr" {{$order->invoice_type=='abbr'?'selected':''}}>Abbreviated Tax Invoice</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">



                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Menu:</label>
                                    {!! Form::select('menu_id',$menus,$selectedOutlet->default_menu_id, ['class' => 'form-control input-sm customer_id', 'id'=>'menu_id']) !!}
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Product category </label>
                                    <select name="productcategory_id" id="productcategory_id" class="form-control select_box customer_id" />
                                    <option value="">Select Product Category...</option>
                                    @if($productscategory)

                                    @foreach($productscategory as $uk => $uv)
                                        <option value="{{ $uv->id }}"
                                        @if($selectedOutlet->default_category_id == $uv->id)
                                        selected=""@endif

                                            >{{ $uv->name }}</option>
                                            @endforeach
                                                @endif
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="col-md-3">
                                <div class="form-group">
                                    <label> Bill No </label>
                                    <input type="text" name="bill_no" id="name" class="form-control input-sm" value="{{$order->bill_no}}" readonly>
                                </div>
                            </div> --}}
                            <input type="hidden" id="outlet_id" name="outlet_id" value="{{$order->outlet_id}}">
                            <input type="hidden" name="session_id" value="{{$order->session_id}}">
                            <input type="hidden" name="order_id" value="{{ $order->order_id }}">
                            <input type="hidden" name="bill_date" value="{{ $order->bill_date}}">
                                 <div class="col-md-3">
                                <div class="form-group">
                                    <label> Transcation Date </label>
                                    <input type="text" name="transaction_date" id="transaction_date" class="form-control input-sm datepicker" value="{{ $order->transaction_date }}" >
                                </div>
                            </div>

                            @if($is_fnb)

                            <div class="col-md-3 hidden-sm hidden-md">
                                <div class="form-group">
                                    <label>Bill To:</label>
                                    {!! Form::select('bill_to',['normal'=>'Normal','bwstoguestandcompany'=>'BWS To Guest and Bill To Comapny'],$order->bill_to, ['class' => 'form-control customer_id', 'id'=>'menu_id']) !!}
                                </div>
                            </div>

                            @endif
                        </div>




                        <input type="hidden" name="order_type" value="invoice">


                        <div class="clearfix"></div>





                        <style>
                            #produt_list .col-md-3 {
                                padding-left: 5px;
                                padding-right: 5px;
                            }

                            #produt_list .inner {
                                padding: 5px;
                                text-align: center;
                            }

                            #produt_list .inner h4 {
                                font-size: 14px;
                                margin: 0;
                            }

                            #produt_list .small-box {
                                margin-bottom: 10px;
                                min-height: 40px;

                            }

                        </style>
                        <div class="col-md-12 text-right" style="margin-top: 5px;margin-bottom: 5px">
                            <a href="javascript::void(0)" class="btn btn-success btn-sm" id="addMore" style="float: right;">
                                <i class="fa fa-plus"></i><span> Add Products Item</span>
                            </a>
                        </div>
                        <div class="row">
                        <div class="col-md-4">
                            <div class="box-body" style="height: 509px;overflow: auto;">
                                <ul class="products-list product-list-in-box" id="produt_list">

                                </ul>
                            </div>
                            <div class="box-body" id='category-discount-box'>

                            </div>
                        </div>

                        <div class="col-md-8" style="padding: 0">

                            <table class="table">
                                <thead>
                                <tr class="bg-red">
                                    <th style="width: 30%">Products*</th>
                                    <th style="width: 15%">Price*</th>
                                    <th style="width: 12%">Qty*</th>
                                    {{--                                        <th style="width: 11%">Disc. %</th>--}}
                                    {{--                                        <th style="width: 12%">Disc. Amt</th>--}}
                                    <th style="width: 17%">Remarks</th>
                                    <th style="width: 20%">Total</th>
                                    <th style="width: 5%"><i class="fa fa-cog"></i></th>
                                </tr>
                                </thead>

                                <tbody id="kana-items">
                                    @foreach($orderDetails as $odk => $odv)
                                    @if($odv->is_inventory == 1)
                                        <tr id='item-{{ $odv->id }}' >
                                        <td >
                                            <input type="text" name="product_id[]" class="form-control product_id" required="required" id="product_id" title="{{ $odv->product->name }}" value="{{ $odv->product->name }}" onkeyup="myfun(this)" onchange="price(this)" autocomplete="off">
                                            <input type="hidden" name="includes_tax[]" class="includes_tax" value="{{$odv->tax}}">
                                            <input type="hidden" name="tax_amount[]" class="tax_amount" value="{{$odv->tax_amount}}">
                                        </td>

                                        <td >
                                            <div class="input-group">

                                                <input type="text" class="form-control price" name="price[]" placeholder="Price" value="{{ $odv->price }}" required="required" autocomplete="off" readonly="">
                                            </div>
                                        </td>
                                        <td >
                                            <input type="number" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}" required="required">
                                        </td>
{{--                                        <td >--}}
{{--                                            <input type="text" name="discount_percent[]" class="form-control discount_percent_line " value="{{ $odv->discount_percent }}" placeholder="Percent" step="any" min="0" max="99">--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <input type="text" name="discount_amount[]" class="form-control discount_amount_line" value="{{ $odv->discount_amount }}" placeholder="Amount" step="any">--}}
{{--                                        </td>--}}

                                        <td >
                                            <input type="text" class="form-control" name="remarks[]" placeholder="Remarks" value="{{ $odv->remarks }}">
                                        </td>

                                        <input type="hidden" name="is_printed_kot_bot[]" value="{{$odv->is_printed_kot_bot}}">

                                        <td >
                                            <div class="input-group">
                                                <input type="text" class="form-control total" name="total[]" placeholder="Total"
                                                value="{{ $odv->total }}" readonly="readonly">
                                            </div>
                                        </td>
                                            <td>
                                                <a href="javascript::void(1);">
                                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="color: #fff;"></i>
                                                </a>
                                        </td>
                                    </tr>
                                    @elseif($odv->is_inventory == 0)
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control product" name="description_custom[]" title="{{$odv->description}}" value="{{ $odv->description }}" placeholder="Product" autocomplete="off">
                                        </td>

                                        <td>
                                            <div class="input-group">

                                                <input type="text" class="form-control price" name="price_custom[]" placeholder="Fare" value="{{ $odv->price }}" required="required" autocomplete="off">
                                            </div>
                                        </td>

                                        <td>
                                            <input type="text" class="form-control quantity" name="quantity_custom[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}" required="required">
                                        </td>


                                        <td>
                                            <input type="text" class="form-control" name="remarks_custom[]" placeholder="Remarks" value="{{ $odv->remarks }}">
                                        </td>

                                        <input type="hidden" name="is_printed_kot_bot_custom[]" value="{{$odv->is_printed_kot_bot}}">

                                        <td>
                                            <div class="input-group">

                                                <input type="text" class="form-control total" name="total_custom[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly">

                                                <a href="javascript::void(1);">
                                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                                </a>

                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    <tr class="multipleDiv"></tr>
                                </tbody>


                            </table>



                            <table style="width:70%;font-size: 16.5px; text-align:right" class="table table-responsive table-striped">

                                <tr>
                                    <td  style="width:40%;text-align: right;font-size: 16px;">Total Qty</td>
                                    <td id="qty-total" style="font-size: 19px;">0</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td  style="text-align: right;">Subtotal</td>
                                    <td id="sub-total">{{ $order->subtotal }}</td>
                                    <td>&nbsp; <input type="hidden" name="subtotal" id="subtotal" value="{{ $order->subtotal }}"></td>
                                </tr>
                                <tr>
                                    <td  class="text-right">Category Discount</td>
                                    <td><input type="checkbox" name=""  style="transform: scale(2);" id='category-discount'></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">Discount (%)</td>
                                    <td style="text-align:right">
                                        <div class="input-group " style="float:right; width:50%;">
                                            <input style="display: inline" class="form-control" type="number" min="0" step = 'any' name="discount_percent" id="discount_percent" value="{{$order->discount_percent }}">
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td style="text-align: right;">Discount Amount</td>
                                    <td style="text-align:right">
                                        <div class="input-group " style="float:right; width:50%;">
                                            <div class="input-group-addon">{{env('APP_CURRENCY')}}</div>
                                            <input style="display: inline;text-align: right" class="form-control" type="number" min="0" name="discount_amount" id="discount_amount" value="{{$order->discount_amount }}">
                                        </div>
                                        <div style="display: inline-flex"><small class="text-muted">Calculation after %, or Direct Discount</small></div>

                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td  style="text-align: right;">Taxable Amount(Without Tax)</td>
                                    <td id="taxable-amount">{{ $order->taxable_amount }}</td>
                                    <td>&nbsp;
                                        <input type="hidden" name="taxable_amount" id="taxableamount" value="{{ $order->taxable_amount }}"></td>
                                </tr>
                                <tr>
                                    <td  style="text-align: right;">Tax Free Amount</td>
                                    <td id="non-taxable-amount">{{ $order->non_taxable_amount }}</td>
                                    <td>&nbsp;
                                        <input type="hidden" name="non_taxable_amount" id="non_taxable_amount" value="{{ $order->non_taxable_amount }}"></td>
                                </tr>

                                <tr>
                                    <td  style="text-align: right;">Tax Amount (13%)</td>
                                    <td id="taxable-tax">{{ $order->tax_amount }}</td>
                                    <td>&nbsp; <input type="hidden" name="taxable_tax" id="taxabletax" value="{{ $order->tax_amount }}"></td>
                                </tr>



{{--                                <tr>--}}
{{--                                    <td  style="text-align: right;font-size: 16px;">Discount Amount</td>--}}
{{--                                    <td id="discount-amount" style="font-size: 19px;">{{$order->discount_amount }}</td>--}}
{{--                                    <td>&nbsp; <input type="hidden" name="discount_amount_total" id="discount_amount" value="{{$order->discount_amount }}"></td>--}}
{{--                                </tr>--}}


                                <tr style="font-weight: bold; font-size: 22px">
                                    <td ><strong>TOTAL {{env('APP_CURRENCY')}}</strong></td>
                                    <td id="total">{{ $order->total_amount }}</td>
                                    <td>
                                        <input type="hidden" name="total_tax_amount" id="total_tax_amount" value="{{ $order->tax_amount }}">
                                        <input type="hidden" name="final_total" id="total_" value="{{ $order->total_amount }}">
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>

                </div>

                    <div class="panel-footer">


                        {{-- <a href="/admin/order/generatePDF/{{$order->id}}" class="btn btn-social btn-foursquare">
                        <i class="fa fa-download"></i> PDF
                        </a>

                        <a href="/admin/order/print/{{$order->id}}" class="btn btn-social btn-foursquare">
                            <i class="fa fa-print"></i> Print
                        </a> --}}



                        @if($is_fnb)

                        <a class="btn bg-olive hidden-sm hidden-md"
                        href="/admin/order/kitchenprint/{{ $order->id }}?type=proxy" target='_blank'><span class="material-icons" >kitchen</span> KOT</a>

                        <a class="btn bg-olive hidden-sm hidden-md"
                        href="/admin/order/botprint/{{ $order->id }}?type=proxy" target='_blank'><span class="material-icons">kitchen</span> BAR</a>

                        @endif

                         <a class="btn btn-info submit_link hidden-sm hidden-md" href="#"
                            onclick='postToedm()'><span class="material-icons">restaurant</span></i> EDM </a>

                        <a class="btn btn-primary hidden-sm hidden-md"
                        href="{{ route('admin.orders.merge_order',$order->id) }}" data-toggle="modal" data-target="#modal_dialog"><span class="material-icons">call_merge</span> Merge Table</a>

                        <a class="btn btn-primary hidden-sm hidden-md"
                        href="{{ route('admin.orders.split_bill',$order->id) }}" data-toggle="modal" data-target="#modal_dialog"><span class="material-icons">call_split</span>  Split</a>
                        <a href="javascript::void(1);" onclick="saveOrder()" class="btn  btn-foursquare">
                            <span class="material-icons"> save </span> Update Order
                        </a>
                        @if(!($order->ordermeta->is_posted ?? 0) )
{{--                            <?php--}}
{{--                            $business_date=\App\Models\BusinessDate::first();--}}
{{--                            ?>--}}
{{--                                @if(!$business_date||($business_date&&$business_date->business_date!=date('Y-m-d')))--}}
{{--                                <button class="btn btn-danger submit_link" type="button" data-toggle="tooltip"--}}
{{--                                        data-placement="top" title="Business Date is not configured"--}}
{{--                            disabled> <span class="material-icons">payment</span> Pay & Print</button>--}}
{{--                                @else--}}
                                    <button class="btn btn-danger submit_link" type="button"
                            onclick="submitPostbill()"> <span class="material-icons">payment</span> Pay & Print</button>
{{--                                @endif--}}
                        @else

                            @if($order->ordermeta->settlement == 0)
                            @if($order->ordermeta->is_posted)
                                <a href="/admin/payment/orders/{{$order->id}}/create" title="Settle" class="btn btn-danger submit_link">Settle Now</a>
                            @endif
                            @endif

                        @endif
{{--
                        <a class="btn bg-purple submit_link" href="{{ route('orders.transfer.estimate',$order->id) }}"
                            target="_blank"><i class="fa fa-print"></i> Estimate Print </a> --}}




{{--                        <a class="btn btn-primary"--}}
{{--                        href="{{ route('admin.orders.move_to_room',$order->id) }}" data-toggle="modal" data-target="#modal_dialog"><i class="fa  fa-sign-out"></i>Post to Room</a>--}}


                        <div class="pull-right">
                        <a class="btn btn-default"
                        href="/admin/hotel/showtablelists?type=invoice&outlet_id={{ $order->outlet_id }}"><i class="fa fa-ban"></i> Close</a>

                        <a class="btn btn-danger"
                        href="{{ route('admin.orders.confirm-delete',$order->id) }}" data-toggle="modal" data-target="#modal_dialog"><i class="fa fa-trash"></i> Delete order</a>
                    </div>


                    </div>

                    </form>
                </div>
            </div>

        </div><!-- /.box-body -->
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection

@section('body_bottom')
<!-- form submit -->
@include('partials._body_bottom_submit_bug_edit_form_js')
{{--<script type="text/javascript">--}}
{{--    $(document).ready(function() {--}}
{{--        $("body").tooltip({ selector: '[data-toggle=tooltip]' });--}}
{{--    });--}}
{{--    $(function() {--}}
{{--        $('.datepicker').datetimepicker({--}}
{{--            //inline: true,--}}
{{--            format: 'YYYY-MM-DD'--}}
{{--            , sideBySide: true--}}
{{--            , allowInputToggle: true--}}
{{--        });--}}

{{--    });--}}



{{--</script>--}}
{{--<form method="post" action="/admin/hotel/postbills/outlets/{{$order->id}}"--}}
{{--    id="postBillForm">--}}
{{--    {{ csrf_field() }}--}}

{{-- </form>--}}

{{--<script>--}}
{{--    function submitPostbill(){--}}

{{--    let c = confirm("Are you sure");--}}

{{--    if(!c){--}}
{{--        return;--}}
{{--    }--}}
{{--    $('#postBillForm').submit();--}}


{{--}--}}
{{--    function isNumeric(n) {--}}
{{--        return !isNaN(parseFloat(n)) && isFinite(n);--}}
{{--    }--}}

{{--    $(document).on('change', '.product_id', function() {--}}
{{--        var parentDiv = $(this).parent().parent();--}}
{{--        var el = $(this);--}}
{{--        if (this.value != 'NULL') {--}}
{{--            var _token = $('meta[name="csrf-token"]').attr('content');--}}
{{--            $.ajax({--}}
{{--                type: "POST"--}}
{{--                , contentType: "application/json; charset=utf-8"--}}
{{--                ,  url: "/admin/products/GetProductDetailAjax/" + this.value.replace('/','') + '?_token=' + _token+`&term=${this.value}`--}}
{{--                , success: function(result) {--}}
{{--                    var obj = jQuery.parseJSON(result.data);--}}
{{--                    if (obj == null) {--}}
{{--                        el.val('');--}}
{{--                        return;--}}
{{--                    }--}}
{{--                    parentDiv.find('.includes_tax').val(obj.includes_tax);--}}
{{--                    parentDiv.find('.tax_amount').val(obj.tax_amount);--}}
{{--                    parentDiv.find('.price').val(obj.price);--}}

{{--                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--                        var total = parentDiv.find('.quantity').val() * obj.price;--}}
{{--                    } else {--}}
{{--                        var total = obj.price;--}}
{{--                    }--}}

{{--                    // var tax = parentDiv.find('.tax_rate').val();--}}
{{--                    // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--                    //     tax_amount = total * Number(tax) / 100;--}}
{{--                    //     parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--                    //     total = total + tax_amount;--}}
{{--                    // } else--}}
{{--                    //     parentDiv.find('.tax_amount').val('0');--}}

{{--                    parentDiv.find('.total').val(total);--}}
{{--                    calcTotal();--}}
{{--                },--}}
{{--                error: function(xhr, status, error) {--}}
{{--                    parentDiv.find('.product_id').val('');--}}
{{--                    parentDiv.find('.price').val('');--}}
{{--                    parentDiv.find('.total').val('');--}}
{{--                    parentDiv.find('.tax_amount').val('');--}}
{{--                    parentDiv.find('.product_id').removeAttr('title')--}}
{{--                    calcTotal();--}}
{{--                }--}}
{{--            });--}}
{{--        } else {--}}
{{--            parentDiv.find('.price').val('');--}}
{{--            parentDiv.find('.total').val('');--}}
{{--            parentDiv.find('.tax_amount').val('');--}}
{{--            calcTotal();--}}
{{--        }--}}
{{--    });--}}

{{--    $(document).on('change', '.customer_id', function() {--}}
{{--        if (this.value != '') {--}}
{{--            // $(".quantity").each(function(index) {--}}
{{--            //     var parentDiv = $(this).parent().parent();--}}
{{--            //     if (isNumeric($(this).val()) && $(this).val() != '')--}}
{{--            //         var total = $(this).val() * parentDiv.find('.price').val();--}}
{{--            //     else--}}
{{--            //         var total = parentDiv.find('.price').val();--}}
{{--            //--}}
{{--            //     var tax = parentDiv.find('.tax_rate').val();--}}
{{--            //     if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--            //         tax_amount = total * Number(tax) / 100;--}}
{{--            //         parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--            //         total = total + tax_amount;--}}
{{--            //     } else--}}
{{--            //         parentDiv.find('.tax_amount').val('0');--}}
{{--            //--}}
{{--            //     if (isNumeric(total) && total != '') {--}}
{{--            //         parentDiv.find('.total').val(total);--}}
{{--            //         calcTotal();--}}
{{--            //     }--}}
{{--            //     //console.log( index + ": " + $(this).text() );--}}
{{--            // });--}}
{{--        } else {--}}
{{--            // $('.total').val('0');--}}
{{--            // $('.tax_amount').val('0');--}}
{{--            calcTotal();--}}
{{--        }--}}
{{--    });--}}

{{--    $(document).on('change', '.quantity', function() {--}}
{{--        // var parentDiv = $(this).parent().parent();--}}
{{--        // if (isNumeric(this.value) && this.value != '') {--}}
{{--        //     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--        //          var total_amount = parentDiv.find('.price').val() * parentDiv.find('.quantity').val();--}}
{{--        //          var total = total_amount ;--}}
{{--        //     } else--}}
{{--        //         var total = '';--}}
{{--        // } else--}}
{{--        //     var total = '';--}}
{{--        //--}}
{{--        // var tax = parentDiv.find('.tax_rate').val();--}}
{{--        // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--        //     tax_amount = total * Number(tax) / 100;--}}
{{--        //     parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--        //     total = total + tax_amount;--}}
{{--        // } else--}}
{{--        //     parentDiv.find('.tax_amount').val('0');--}}
{{--        //--}}
{{--        // parentDiv.find('.total').val(total);--}}
{{--        let parentDiv = $(this).parent().parent();--}}

{{--// discount percentage calculated--}}
{{--//         var quantity = Number(parentDiv.find('.quantity').val());--}}
{{--//         var price = Number( parentDiv.find('.price').val());--}}
{{--//         var includes_tax = Number( parentDiv.find('.product_id').data('includes_tax'));--}}
{{--//         var tax_amount = Number( parentDiv.find('.product_id').data('tax_amount'));--}}
{{--//         if (includes_tax==1){--}}
{{--//             price=price-tax_amount;--}}
{{--//         }--}}
{{--//         var tot = quantity * price;--}}
{{--//         var discount=Number(parentDiv.find('.discount_amount_line').val())--}}
{{--//         var dis_per = Number(parentDiv.find('.discount_amount_line').val())/tot*100;--}}
{{--//         parentDiv.find('.discount_percent_line').val(dis_per.toFixed(2));--}}
{{--//--}}
{{--//--}}
{{--//--}}
{{--//         var total=''--}}
{{--//         if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--//             total = (price * quantity)-discount;--}}
{{--//         } else--}}
{{--//             total = '';--}}
{{--//--}}
{{--//         if (includes_tax==1 && (total != 0 || total != '')) {--}}
{{--//             tax_amount = quantity * Number(tax_amount);--}}
{{--//             total = total + tax_amount;--}}
{{--//         } else--}}
{{--//             // parentDiv.find('.tax_amount').val('0');--}}
{{--//--}}
{{--//             parentDiv.find('.total').val(total);--}}
{{--        calculateLineTotal(parentDiv);--}}

{{--        calcTotal();--}}
{{--        $('#discount_percent').trigger('change');--}}
{{--    });--}}

{{--    $(document).on('change', '.price', function() {--}}

{{--        // var parentDiv = $(this).parent().parent().parent();--}}
{{--        //--}}
{{--        // // console.log(parentDiv);--}}
{{--        //--}}
{{--        // if (isNumeric(this.value) && this.value != '') {--}}
{{--        //--}}
{{--        //     if (isNumeric(parentDiv.find('.price').val()) && parentDiv.find('.price').val() != '') {--}}
{{--        //--}}
{{--        //        var total_amount = parentDiv.find('.price').val() * parentDiv.find('.quantity').val();--}}
{{--        //         var total = total_amount ;--}}
{{--        //     } else--}}
{{--        //         var total = '';--}}
{{--        // } else--}}
{{--        //     var total = '';--}}
{{--        //--}}
{{--        // var tax = parentDiv.find('.tax_rate').val();--}}
{{--        // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--        //     tax_amount = total * Number(tax) / 100;--}}
{{--        //     parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--        //     total = total + tax_amount;--}}
{{--        // } else--}}
{{--        //     parentDiv.find('.tax_amount').val('0');--}}
{{--        //--}}
{{--        // parentDiv.find('.total').val(total);--}}
{{--        var parentDiv = $(this).parent().parent();--}}


{{--        // var quantity = Number(parentDiv.find('.quantity').val());--}}
{{--        // var price = Number( parentDiv.find('.price').val());--}}
{{--        // var includes_tax = Number( parentDiv.find('.product_id').data('includes_tax'));--}}
{{--        // var tax_amount = Number( parentDiv.find('.product_id').data('tax_amount'));--}}
{{--        // if (includes_tax==1){--}}
{{--        //     price=price-tax_amount;--}}
{{--        // }--}}
{{--        // var tot = quantity * price;--}}
{{--        // var dis_per = Number(parentDiv.find('.discount_percent_line').val())*tot/100;--}}
{{--        // parentDiv.find('.discount_amount_line').val(dis_per.toFixed(2));--}}
{{--        // var discount=Number(dis_per)--}}
{{--        //--}}
{{--        //--}}
{{--        // var total=''--}}
{{--        // if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--        //     total = (price * quantity)-discount;--}}
{{--        // } else--}}
{{--        //     total = '';--}}
{{--        //--}}
{{--        // if (includes_tax==1 && (total != 0 || total != '')) {--}}
{{--        //     tax_amount = quantity * Number(tax_amount);--}}
{{--        //     total = total + tax_amount;--}}
{{--        // } else--}}
{{--        //     // parentDiv.find('.tax_amount').val('0');--}}
{{--        //--}}
{{--        //     parentDiv.find('.total').val(total);--}}
{{--        //alert('done');--}}
{{--        calculateLineTotal(parentDiv);--}}

{{--        calcTotal();--}}
{{--    });--}}

{{--    $(document).on('change', '.price', function() {--}}
{{--       //  var parentDiv = $(this).parent().parent().parent();--}}
{{--       // // console.log(parentDiv.find('.price').val());--}}
{{--       //  if (isNumeric(this.value) && this.value != '') {--}}
{{--       //      if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--       //         var total_amount = parentDiv.find('.price').val() * parentDiv.find('.quantity').val();--}}
{{--       //          var total = total_amount ;--}}
{{--       //--}}
{{--       //      } else--}}
{{--       //          var total = '';--}}
{{--       //  } else--}}
{{--       //      var total = '';--}}
{{--       //--}}
{{--       //  var tax = parentDiv.find('.tax_rate').val();--}}
{{--       //  if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--       //      tax_amount = total * Number(tax) / 100;--}}
{{--       //      parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--       //      total = total + tax_amount;--}}
{{--       //  } else--}}
{{--       //      parentDiv.find('.tax_amount').val('0');--}}
{{--       //  console.log(total);--}}
{{--       //  parentDiv.find('.total').val(total);--}}
{{--        var parentDiv = $(this).parent().parent();--}}


{{--        // var quantity = Number(parentDiv.find('.quantity').val());--}}
{{--        // var price = Number( parentDiv.find('.price').val());--}}
{{--        // var includes_tax = Number( parentDiv.find('.product_id').data('includes_tax'));--}}
{{--        // var tax_amount = Number( parentDiv.find('.product_id').data('tax_amount'));--}}
{{--        // if (includes_tax==1){--}}
{{--        //     price=price-tax_amount;--}}
{{--        // }--}}
{{--        // var tot = quantity * price;--}}
{{--        // var dis_per = Number(parentDiv.find('.discount_percent_line').val())*tot/100;--}}
{{--        // parentDiv.find('.discount_amount_line').val(dis_per.toFixed(2));--}}
{{--        // var discount=Number(dis_per)--}}
{{--        //--}}
{{--        //--}}
{{--        // var total=''--}}
{{--        // if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--        //     total = (price * quantity)-discount;--}}
{{--        // } else--}}
{{--        //     total = '';--}}
{{--        //--}}
{{--        // if (includes_tax==1 && (total != 0 || total != '')) {--}}
{{--        //     tax_amount = quantity * Number(tax_amount);--}}
{{--        //     total = total + tax_amount;--}}
{{--        // } else--}}
{{--        //     // parentDiv.find('.tax_amount').val('0');--}}
{{--        //--}}
{{--        //     parentDiv.find('.total').val(total);--}}
{{--        calculateLineTotal(parentDiv);--}}

{{--        calcTotal();--}}
{{--    });--}}

{{--    // $(document).on('change', '.tax_rate', function() {--}}
{{--    //     var parentDiv = $(this).parent().parent();--}}
{{--    //--}}
{{--    //     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--    //         var total = parentDiv.find('.price').val() * Number(parentDiv.find('.quantity').val());--}}
{{--    //     } else--}}
{{--    //         var total = '';--}}
{{--    //--}}
{{--    //     var tax = $(this).val();--}}
{{--    //     if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--    //         tax_amount = total * Number(tax) / 100;--}}
{{--    //         parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--    //         total = total + tax_amount;--}}
{{--    //     } else--}}
{{--    //         parentDiv.find('.tax_amount').val('0');--}}
{{--    //--}}
{{--    //     parentDiv.find('.total').val(total);--}}
{{--    //     calcTotal();--}}
{{--    // });--}}

{{--    /*$('#discount').on('change', function() {--}}
{{--        if(isNumeric(this.value) && this.value != '')--}}
{{--        {--}}
{{--            if(isNumeric($('#sub-total').val()) && $('#sub-total').val() != '')--}}
{{--                parentDiv.find('.total').val($('#sub-total').val() - this.value).trigger('change');--}}
{{--        }--}}
{{--    });--}}

{{--    $("#sub-total").bind("change", function() {--}}
{{--        if(isNumeric($('#discount').val()) && $('#discount').val() != '')--}}
{{--            parentDiv.find('.total').val($('#sub-total').val() - $('#discount').val());--}}
{{--        else--}}
{{--            parentDiv.find('.total').val($('#sub-total').val());--}}
{{--    });*/--}}

{{--    $("#addMore").on("click", function() {--}}
{{--        //$($('#orderFields').html()).insertBefore(".multipleDiv");--}}
{{--        $(".multipleDiv").before($('#orderFields #more-tr').html());--}}
{{--    });--}}
{{--    $("#addCustomMore").on("click", function() {--}}
{{--        //$($('#orderFields').html()).insertBefore(".multipleDiv");--}}
{{--        $(".multipleDiv").before($('#CustomOrderFields #more-custom-tr').html());--}}
{{--    });--}}

{{--    $(document).on('click', '.remove-this', function() {--}}
{{--        $(this).parent().parent().parent().parent().remove();--}}
{{--        calcTotal();--}}
{{--    });--}}

{{--    $(document).on('change', '#vat_type', function() {--}}
{{--        calcTotal();--}}
{{--    });--}}

{{--    $(document).on('change', '#service_type', function() {--}}
{{--        calcTotal();--}}
{{--    });--}}

{{--    $(document).on('change', '#discount', function() {--}}

{{--        calcTotal();--}}

{{--    });--}}


{{--    function calcTotal() {--}}
{{--        //alert('hi');--}}
{{--        var subTotal = 0;--}}
{{--        // var service_total = 0;--}}
{{--        // var service_charge = 0;--}}
{{--        var taxableAmount = 0;--}}
{{--        var nontaxableAmount = 0;--}}

{{--        //var tax = Number($('#tax').val().replace('%', ''));--}}
{{--        var total = 0;--}}
{{--        var tax_amount = 0;--}}
{{--        var taxableTax = 0;--}}
{{--        var discount_amount = 0;--}}

{{--        $(".total").each(function(index) {--}}
{{--            var parentDiv=$(this).parent().parent().parent()--}}
{{--            if (isNumeric($(this).val())&&$(this).val()!=''){--}}

{{--                var quantity = Number(parentDiv.find('.quantity').val());--}}
{{--                var price = Number( parentDiv.find('.price').val());--}}
{{--                var tax =0;--}}
{{--                var includes_tax =Number(parentDiv.find('.includes_tax').val());--}}
{{--                if (includes_tax==1){--}}
{{--                    var amount_without_tax=price/1.13;--}}
{{--                    tax=price-amount_without_tax;--}}
{{--                    price=price-tax;--}}
{{--                    taxableAmount+=price*quantity--}}
{{--                    tax_amount=tax_amount+tax*quantity--}}
{{--                }else nontaxableAmount+=price*quantity--}}

{{--                debugger--}}
{{--                // var tot = quantity * price;--}}
{{--                // var discount=Number(parentDiv.find('.discount_amount_line').val())--}}


{{--                subTotal = Number(subTotal) + Number($(this).val());--}}
{{--                total= Number(total) + Number($(this).val());--}}
{{--                discount_amount=discount_amount+Number(parentDiv.find('.discount_amount_line').val())--}}

{{--            }--}}
{{--        });--}}

{{--        // $(".tax_amount").each(function(index) {--}}
{{--        //     if (isNumeric($(this).val()))--}}
{{--        //         tax_amount = Number(tax_amount) + Number($(this).val());--}}
{{--        // });--}}




{{--        // if($('#discount_amount').val().trim()){--}}
{{--        //--}}
{{--        //     let amount_after_discount1 = Number($('input#subtotal').val()) - Number($('#discount_amount').val()) ;--}}
{{--        //--}}
{{--        //     $('#amount-after-discount').text(amount_after_discount1);--}}
{{--        //--}}
{{--        // }--}}




{{--        $('#sub-total').html(subTotal.toFixed(2));--}}
{{--        $('#subtotal').val(subTotal.toFixed(2));--}}

{{--        // $('#taxable-amount').html(subTotal);--}}
{{--        // $('#taxableamount').val(subTotal);--}}

{{--        // var service_type = $('#service_type').val();--}}
{{--        // var discount_amount = $('#discount_amount').val();--}}
{{--        // if(discount_amount.trim() == '' || discount_amount == '0'){--}}
{{--        //--}}
{{--        //  discount_amount =    getDiscountAmount();--}}
{{--        // }--}}

{{--        // var vat_type = $('#vat_type').val();--}}
{{--        // let discounttype = $('#discount').val();--}}
{{--        //--}}
{{--        //--}}
{{--        // if (discounttype == 'percentage') {--}}
{{--        //--}}
{{--        //     if (isNumeric(discount_amount) && discount_amount != 0) {--}}
{{--        //         amount_after_discount = subTotal - (Number(discount_amount) / 100 * subTotal);--}}
{{--        //     } else {--}}
{{--        //         amount_after_discount = subTotal;--}}
{{--        //     }--}}
{{--        //--}}
{{--        //     if (service_type == 'no' || service_type == '') {--}}
{{--        //         service_total = amount_after_discount;--}}
{{--        //         service_charge = 0;--}}
{{--        //         taxableAmount = service_total;--}}
{{--        //     } else {--}}
{{--        //         service_total = amount_after_discount + Number(10 / 100 * amount_after_discount);--}}
{{--        //         service_charge = Number(10 / 100 * amount_after_discount);--}}
{{--        //         taxableAmount = service_total;--}}
{{--        //     }--}}
{{--        //     taxableAmount=taxableAmount.toFixed(2)--}}
{{--        //--}}
{{--        //     total = Number(taxableAmount) + Number(13 / 100 * taxableAmount);--}}
{{--        //     taxableTax = Number(13 / 100 * taxableAmount);--}}
{{--        //--}}
{{--        // } else {--}}
{{--        //--}}
{{--        //     if (isNumeric(discount_amount) && discount_amount != 0) {--}}
{{--        //         amount_after_discount = subTotal - (Number(discount_amount));--}}
{{--        //     } else {--}}
{{--        //         amount_after_discount = subTotal;--}}
{{--        //     }--}}
{{--        //--}}
{{--        //     if (service_type == 'no' || service_type == '') {--}}
{{--        //         service_total = amount_after_discount;--}}
{{--        //         service_charge = 0;--}}
{{--        //         taxableAmount = service_total;--}}
{{--        //     } else {--}}
{{--        //         service_total = amount_after_discount + Number(10 / 100 * amount_after_discount);--}}
{{--        //         service_charge = Number(10 / 100 * amount_after_discount);--}}
{{--        //         taxableAmount = service_total;--}}
{{--        //     }--}}
{{--        //     taxableAmount=taxableAmount.toFixed(2)--}}
{{--        //--}}
{{--        //     total = Number(taxableAmount) + Number(13 / 100 * taxableAmount);--}}
{{--        //     taxableTax = Number(13 / 100 * taxableAmount);--}}
{{--        //--}}
{{--        // }--}}
{{--        //--}}
{{--        // $('#service_charge').val(service_charge.toFixed(2));--}}
{{--        // $('#service-charge').html(service_charge.toFixed(2));--}}
{{--        //--}}
{{--        // $('#amount_with_service').val(service_total);--}}
{{--        // $('#amount-with-service').html(service_total);--}}

{{--        $('#discount_amount').val(discount_amount.toFixed(2));--}}
{{--        $('#discount-amount').html(discount_amount.toFixed(2));--}}

{{--        $('#taxableamount').val(taxableAmount.toFixed(2));--}}
{{--        $('#taxable-amount').html(taxableAmount.toFixed(2));--}}

{{--        $('#non_taxable_amount').val(nontaxableAmount.toFixed(2));--}}
{{--        $('#non-taxable-amount').html(nontaxableAmount.toFixed(2));--}}

{{--        $('#total_tax_amount').val(tax_amount.toFixed(2));--}}

{{--        $('#taxabletax').val(tax_amount.toFixed(2));--}}
{{--        $('#taxable-tax').html(tax_amount.toFixed(2));--}}


{{--        $('#total').html(total.toFixed(2));--}}
{{--        $('#total_').val(total.toFixed(2));--}}


{{--        calculateTotalQty();--}}


{{--    }--}}

{{--       function calculateTotalQty()--}}
{{--    {--}}

{{--        var totalQty = 0;--}}

{{--        $('#kana-items .quantity').each(function(){--}}

{{--            totalQty += Number($(this).val());--}}
{{--            console.log(totalQty);--}}

{{--        });--}}

{{--        $('#qty-total').text(totalQty);--}}

{{--    }--}}



{{--  $(document).on('keyup', '#discount_amount', function() {--}}

{{--        let subtotal = Number( $('#sub-total').text() );--}}
{{--        let discount = Number($(this).val());--}}
{{--        if(subtotal > 0){--}}
{{--            let percent = ( discount / subtotal )* 100;--}}
{{--            $('#discount_percent').val(percent.toFixed(3));--}}
{{--        }--}}


{{--        calcTotal();--}}

{{--    });--}}


{{--   function posdisount(pos_customer_id){--}}
{{--         if (pos_customer_id != '') {--}}
{{--            $.get(`/admin/getposdiscount/${pos_customer_id}`,function(response){--}}
{{--                $('#discount_amount').val('');--}}
{{--                $('#discount_percent').val(response.pos_discount);--}}
{{--                $('#discount_percent').trigger('change');--}}
{{--                calcTotal();--}}
{{--            });--}}
{{--        }--}}

{{--    }--}}



{{--    $('#pos_customer_id').change(function() {--}}
{{--        var pos_customer_id = $(this).val();--}}

{{--        if (pos_customer_id != '') {--}}

{{--            $('#reservation_id').val('');--}}

{{--            posdisount(pos_customer_id);--}}


{{--        }--}}
{{--    });--}}



{{--</script>--}}

{{--<script type="text/javascript">--}}
{{--    $(document).ready(function() {--}}

{{--        $('.customer_id').select2();--}}
{{--        $('.pos_customer_id').select2();--}}

{{--        let dtype = $('#discount').val();--}}

{{--        if (dtype == 'percentage') {--}}
{{--            $('.discounttype').html('(%)');--}}
{{--        } else {--}}

{{--            $('.discounttype').html('(Amount)');--}}
{{--        }--}}

{{--    });--}}

{{--    $('#discount').on('change', function() {--}}
{{--        $('#discount_amount').val('');--}}
{{--        type = $(this).val();--}}

{{--        if (type == 'percentage') {--}}
{{--            $('.discounttype').html('(%)');--}}
{{--        } else {--}}

{{--            $('.discounttype').html('(Amount)');--}}
{{--        }--}}
{{--        calcTotal();--}}

{{--    });--}}



{{--</script>--}}


{{--<script type="text/javascript">--}}


{{--</script>--}}

{{--<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />--}}
{{--<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />--}}
{{--<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>--}}
{{--<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>--}}

{{--<style type="text/css">--}}
{{--    .intl-tel-input {--}}
{{--        width: 100%;--}}
{{--    }--}}

{{--    .intl-tel-input .iti-flag .arrow {--}}
{{--        border: none;--}}
{{--    }--}}

{{--</style>--}}

{{--<script type="text/javascript">--}}
{{--    function myfun(value) {--}}
{{--        $(value).autocomplete({--}}
{{--            source: function(request, response) {--}}
{{--                $.ajax({--}}
{{--                    url: "/admin/getProducts"--}}
{{--                    , dataType: "json"--}}
{{--                    , data: {--}}
{{--                        term: request.term--}}
{{--                        , outlet_id: $("#outlet_id").val()--}}
{{--                        , menu_id: $("#menu_id").val()--}}
{{--                    }--}}
{{--                    , success: function(data) {--}}
{{--                        response(data);--}}
{{--                        addproducttype();--}}
{{--                    }--}}
{{--                });--}}
{{--            }--}}
{{--            , minLength: 1--}}
{{--            , select: function(event, ui) {--}}
{{--               setTimeout(()=>{--}}
{{--                    price(value);--}}
{{--                    var text=ui.item.value--}}
{{--                    $(this).parent().find('.product_id').prop('title',text)--}}
{{--                },100);--}}

{{--            }--}}
{{--        , });--}}
{{--    }--}}

{{--    function price(value) {--}}

{{--        var parentDiv = $(value).parent().parent();--}}

{{--        var el = $(value);--}}

{{--        if (value.value != 'NULL') {--}}
{{--            var _token = $('meta[name="csrf-token"]').attr('content');--}}
{{--            $.ajax({--}}
{{--                type: "POST"--}}
{{--                , contentType: "application/json; charset=utf-8"--}}
{{--                ,  url: "/admin/products/GetProductDetailAjax/" + value.value.replace('/','') + '?_token=' + _token+`&term=${value.value}`--}}
{{--                , success: function(result) {--}}


{{--                    var obj = jQuery.parseJSON(result.data);--}}

{{--                     if (obj == null) {--}}
{{--                        el.val('');--}}
{{--                        return;--}}
{{--                    }--}}
{{--                    let prevItem = $(`table #kana-items #item-${obj.id}`);--}}
{{--                    if (prevItem.length > 0) {--}}
{{--                        calcTotal();--}}
{{--                        return;--}}
{{--                    }--}}

{{--                    parentDiv.find('.price').val(obj.price);--}}

{{--                    parentDiv.attr('id', 'item-' + obj.id);--}}

{{--                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--                        var total = parentDiv.find('.quantity').val() * obj.price;--}}
{{--                    } else {--}}
{{--                        var total = obj.price;--}}
{{--                    }--}}

{{--                    var tax = parentDiv.find('.tax_rate').val();--}}
{{--                    if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {--}}
{{--                        tax_amount = total * Number(tax) / 100;--}}
{{--                        parentDiv.find('.tax_amount').val(tax_amount);--}}
{{--                        total = total + tax_amount;--}}
{{--                    } else--}}
{{--                        parentDiv.find('.tax_amount').val('0');--}}

{{--                    parentDiv.find('.total').val(total);--}}
{{--                    calcTotal();--}}
{{--                },--}}
{{--                error: function(xhr, status, error) {--}}
{{--                    parentDiv.find('.product_id').val('');--}}
{{--                    parentDiv.find('.price').val('');--}}
{{--                    parentDiv.find('.total').val('');--}}
{{--                    parentDiv.find('.tax_amount').val('');--}}
{{--                    parentDiv.find('.product_id').removeAttr('title')--}}
{{--                    calcTotal();--}}
{{--                }--}}
{{--            });--}}
{{--        } else {--}}
{{--            parentDiv.find('.price').val('');--}}
{{--            parentDiv.find('.total').val('');--}}
{{--            parentDiv.find('.tax_amount').val('');--}}
{{--            calcTotal();--}}
{{--        }--}}
{{--    }--}}

{{--</script>--}}

{{--<script>--}}
{{--    $(function() {--}}

{{--        var firstTimeMenuAdded = true;--}}

{{--        $('#menu_id').on('change', function() {--}}
{{--            var outlet_id = $('#outlet_id').val();--}}
{{--            if ($(this).val() != '') {--}}
{{--                $.ajax({--}}
{{--                    url: "/admin/users/ajax/getProductcategory"--}}
{{--                    , data: {--}}
{{--                        menu_id: $(this).val()--}}
{{--                        ,outlet_id: outlet_id--}}
{{--                    }--}}
{{--                    , dataType: "json"--}}
{{--                    , success: function(data) {--}}
{{--                        var result = data.data;--}}
{{--                        $('#productcategory_id').html(result);--}}
{{--                        if(firstTimeMenuAdded){--}}
{{--                            $('#productcategory_id').val('{{ $selectedOutlet->default_category_id }}');--}}
{{--                            firstTimeMenuAdded= false;--}}
{{--                        }--}}
{{--                    }--}}
{{--                });--}}
{{--            }--}}
{{--        });--}}
{{--        $('#menu_id').trigger('change');--}}


{{--    });--}}

{{--</script>--}}

{{--<script>--}}
{{--    $(function() {--}}

{{--        $('#productcategory_id').on('change', function() {--}}
{{--            var outlet_id = $('#outlet_id').val();--}}

{{--            if ($(this).val() != '') {--}}

{{--                $.ajax({--}}
{{--                    url: "/admin/users/ajax/getProduct"--}}
{{--                    , data: {--}}
{{--                        category_id: $(this).val()--}}
{{--                        , outlet_id: outlet_id--}}
{{--                    }--}}
{{--                    , dataType: "json"--}}
{{--                    , success: function(data) {--}}

{{--                        var result = data.data;--}}
{{--                        $('#produt_list').html(result);--}}

{{--                    }--}}
{{--                });--}}
{{--            }--}}
{{--        });--}}
{{--        $('#productcategory_id').trigger('change');--}}
{{--    });--}}


{{--    function AddMore(val) {--}}

{{--        let name = $(val).data('name');--}}
{{--        var price = $(val).data('price');--}}
{{--        var includes_tax = $(val).data('includes_tax');--}}
{{--        var tax_amount = $(val).data('tax_amount');--}}
{{--        var app_currency = `{{env('APP_CURRENCY')}}`;--}}
{{--        let itemid = $(val).data('id');--}}
{{--        var prevItem = $(`table #kana-items #item-${itemid}`)--}}
{{--        if (prevItem.length > 0) {--}}
{{--            let quantityEl = prevItem.find('.quantity');--}}
{{--            let priceEl = prevItem.find('.price');--}}
{{--            let prevQty = Number(quantityEl.val());--}}
{{--            let newQty = prevQty + 1;--}}
{{--            let newTotal = newQty * Number(priceEl.val());--}}
{{--            let totalEl = prevItem.find('.total');--}}
{{--            quantityEl.val(newQty);--}}
{{--            totalEl.val(newTotal);--}}
{{--            calcTotal();--}}
{{--            return false;--}}
{{--        }--}}
{{--        let dom = '<tr id="item-' + itemid + '"><td ><input type="text" name="product_id[]" title="'+name+'" class="form-control product_id" value="' + name + '" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)"><input type="hidden" name="includes_tax[]" class="includes_tax" value="'+includes_tax+'"><input type="hidden" name="tax_amount[]" class="tax_amount" value="'+tax_amount+'"></td><td ><div class="input-group"><input type="text" class="form-control price" name="price[]" placeholder="Price" readonly value="' + price + '" required="required"></div></td><td><input type="text" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1" required="required"><td><input type="text" name="discount_percent[]" placeholder="Percent" value="0" class="form-control discount_percent_line"></td><td ><input type="text" name="discount_amount[]" placeholder="Amount" value="0" class="form-control discount_amount_line" ></td></td><td><input type="text" class="form-control" name="remarks[]" placeholder="Remarks"></td><td ><div class="input-group"> <input type="text" class="form-control total" name="total[]" placeholder="Total" value="' + price + '" readonly="readonly"><a href="javascript::void(1);"><i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i></a></div></td></tr>';--}}

{{--        $(".multipleDiv").before(dom);--}}

{{--        calcTotal();--}}
{{--        addproducttype();--}}
{{--        $('#kana-items tr').last().find('.product_id').focus();--}}
{{--    }--}}


{{--    $('#reservation_id').change(function() {--}}

{{--        let reservation_val = $(this).val();--}}
{{--        $.get('/admin/getpanno/fromreservation/' + reservation_val, function(data, status) {--}}
{{--            $('#pan_no').val(data.vat_no);--}}
{{--            $('#company_name').val(data.company_name);--}}
{{--        });--}}

{{--    });--}}

{{--    $(document).on('input','.discount_amount_line',function(){--}}

{{--        let parentDiv = $(this).parent().parent();--}}
{{--        var quantity = Number(parentDiv.find('.quantity').val());--}}
{{--        var price = Number( parentDiv.find('.price').val());--}}
{{--        var includes_tax = Number( parentDiv.find('.includes_tax').val());--}}
{{--        var tax_amount = Number( parentDiv.find('.tax_amount').val());--}}
{{--        if (includes_tax!=0){--}}
{{--            price=quantity*price-tax_amount;--}}
{{--        }--}}
{{--        var tot = quantity * price;--}}
{{--        var discount=Number(parentDiv.find('.discount_amount_line').val())--}}
{{--        var dis_per = Number(parentDiv.find('.discount_amount_line').val())/tot*100;--}}
{{--        parentDiv.find('.discount_percent_line').val(dis_per.toFixed(2));--}}

{{--        var total=''--}}
{{--        if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--            total = (price * quantity)-discount;--}}
{{--        } else--}}
{{--            total = '';--}}

{{--        if (includes_tax==1 && (total != 0 || total != '')) {--}}
{{--            tax_amount = quantity * Number(tax_amount);--}}
{{--            total = total + tax_amount;--}}
{{--        }--}}

{{--        parentDiv.find('.tax_amount').val(tax_amount);--}}


{{--        parentDiv.find('.total').val(total.toFixed(2));--}}

{{--        calcTotal();--}}

{{--    })--}}
{{--    function calculateLineTotal(parentDiv){--}}
{{--        var quantity = Number(parentDiv.find('.quantity').val());--}}
{{--        var price = Number( parentDiv.find('.price').val());--}}
{{--        var includes_tax = Number( parentDiv.find('.includes_tax').val());--}}
{{--        var tax_amount = Number( parentDiv.find('.tax_amount').val());--}}
{{--        if (includes_tax==1){--}}
{{--            var amount_without_tax=price/1.13;--}}
{{--            tax_amount=price-amount_without_tax;--}}
{{--            price=price-tax_amount;--}}
{{--        }--}}


{{--        var tot = quantity * price;--}}
{{--        var dis_amt = Number(parentDiv.find('.discount_percent_line').val())*tot/100;--}}
{{--        parentDiv.find('.discount_amount_line').val(dis_amt.toFixed(2));--}}
{{--        var discount=Number(dis_amt)--}}


{{--        var total=''--}}
{{--        if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {--}}
{{--            total = (price * quantity)-discount;--}}
{{--        } else--}}
{{--            total = '';--}}

{{--        if (includes_tax==1 && (total != 0 || total != '')) {--}}
{{--            tax_amount = quantity * Number(tax_amount);--}}
{{--            total = total + tax_amount;--}}
{{--        }--}}
{{--debugger--}}
{{--        parentDiv.find('.tax_amount').val(tax_amount.toFixed(2));--}}

{{--        parentDiv.find('.total').val(total.toFixed(2));--}}
{{--    }--}}
{{--    $(document).on('input','.discount_percent_line',function(){--}}

{{--        var parentDiv = $(this).parent().parent();--}}
{{--        calculateLineTotal(parentDiv)--}}
{{--        calcTotal();--}}

{{--    })--}}
{{--    function validateDiscount(){--}}


{{--      let   amount = $('#discount_percent');--}}
{{--      let max_discount = Number("{{env('MAX_DISCOUNT_PERCENT',50)}}");--}}
{{--        if (Number(amount.val()) > max_discount) {--}}
{{--            amount.val(max_discount);--}}
{{--        }--}}

{{--        return ;--}}

{{--    }--}}
{{--    $('#discount_percent').on('keyup change',function(){--}}

{{--        let total = $('input#subtotal').val();--}}
{{--        if(total == 0){--}}


{{--            $(this).val(0);--}}
{{--            return;--}}
{{--        }--}}


{{--        validateDiscount();--}}
{{--        if($(this).val().trim() == ''){--}}

{{--            $('input#discount_amount').prop('readonly',false);--}}

{{--        }else{--}}

{{--            $('input#discount_amount').prop('readonly',true);--}}

{{--        }--}}

{{--        let percent = ( $(this).val() /100 ) * total;--}}
{{--        $('input#discount_amount').val(percent.toFixed(2));--}}



{{--        calcTotal();--}}



{{--    });--}}
{{--  function addproducttype(){--}}

{{--        var allitemId = [];--}}
{{--        $('tbody#kana-items tr').each(function(){--}}

{{--            var id = $(this).attr('id');--}}

{{--            if(id){--}}

{{--                let i =  id.replace("item-", "");--}}
{{--                allitemId.push(i);--}}

{{--            }--}}

{{--        });--}}

{{--        let data = {id: allitemId};--}}

{{--        $.get('/admin/getfoodcategorybyid',data,function(response){--}}

{{--            let itemcat = response.itemcat;--}}

{{--            let cattype = response.types;--}}

{{--            for(let item of itemcat){--}}

{{--                $(`#item-${item.id}`).attr('types-id',item.cid);--}}
{{--            }--}}


{{--            var htmlel = `<table class="table">--}}
{{--                        <tr>--}}
{{--                            <th>Category</th>--}}
{{--                            <th>Discount %</th>--}}
{{--                            <td>Total<td>--}}
{{--                        </tr>--}}
{{--                        `;--}}
{{--           for(let cat of cattype){--}}

{{--            htmlel +=  `<tr class='type-discount-parent'>--}}
{{--                    <td> ${cat.name}</td>--}}
{{--                    <td><input type='number' class='form-control line-discount-bytype input-sm' steps='any' data-cid ='${cat.id}'--}}
{{--                        data-dlimt = '${cat.discount_limit}'--}}
{{--                        ></td>--}}
{{--                    <td><input type='number' class='form-control line-total' readonly></td>--}}
{{--            </tr>--}}
{{--            `;--}}


{{--           }--}}
{{--           if($('input#category-discount').is(':checked') ){--}}

{{--             $('#category-discount-box').html(htmlel);--}}
{{--            //console.log(response);--}}
{{--            document.getElementById("category-discount-box").scrollIntoView();--}}


{{--           }--}}

{{--        });--}}



{{--  }--}}




{{--    $('input#category-discount').change(function(){--}}


{{--        if(!$(this).is(':checked')){--}}
{{--            $('#discount_percent').attr('readonly',false);--}}
{{--            $('#discount_amount').attr('readonly',false);--}}
{{--            $('#category-discount-box').html('');--}}
{{--            return ;--}}
{{--        }--}}
{{--        $('#discount_percent').val(0);--}}
{{--        $('#discount_percent').attr('readonly',true);--}}
{{--        $('#discount_amount').attr('readonly',true);--}}

{{--        addproducttype();--}}

{{--    });--}}
{{--    function totaldiscountall(){ //total discount by categotry--}}

{{--        var total = 0;--}}

{{--        $('#category-discount-box input.line-total').each(function(){--}}

{{--            total += Number($(this).val());--}}


{{--        });--}}

{{--        $('#discount_amount').val(Number(total).toFixed(2));--}}

{{--        calcTotal();--}}

{{--    }--}}

{{--    $(document).on('change keyup','.line-discount-bytype',function(){--}}
{{--        let selectdiscount = $(this).val();--}}
{{--        let discount_limit = $(this).attr('data-dlimt');--}}

{{--        if(selectdiscount > discount_limit){--}}
{{--            $(this).val(discount_limit);--}}
{{--            $(this).trigger('change');--}}
{{--            return;--}}
{{--        }--}}



{{--        let cid = $(this).attr('data-cid');--}}
{{--        let parent = $(this).parent().parent();--}}

{{--        var totaldiscount = 0;--}}
{{--        var totalamount = 0;--}}
{{--        $(`tr[types-id=${cid}]`).each(function(){--}}
{{--            let total = $(this).find('input.total').val();--}}
{{--            totalamount += Number(total);--}}
{{--        });--}}
{{--        totaldiscount = ( selectdiscount / 100 ) * totalamount;--}}
{{--        parent.find('.line-total').val(totaldiscount);--}}

{{--        totaldiscountall();--}}

{{--    });--}}


{{--    $(function(){--}}



{{--        calcTotal();--}}
{{--    });--}}
{{--$(document).on('keyup','.product_id',function(e){--}}

{{--    if(e.keyCode==13){--}}


{{--        $('#addMore').trigger('click');--}}

{{--        setTimeout(function(){--}}
{{--           $('#kana-items tr').last().prev().find('.product_id').focus();--}}


{{--        },500)--}}

{{--    }--}}

{{--    e.preventDefault();--}}



{{--});--}}


{{--$(document).ready(function() {--}}
{{--  $(window).keydown(function(event){--}}
{{--    if(event.keyCode == 13) {--}}
{{--      event.preventDefault();--}}
{{--      return false;--}}
{{--    }--}}
{{--  });--}}
{{--});--}}
{{--  function openwindowposcustomer() {--}}
{{--        var win = window.open('/admin/clients/modals/create?relation_type=poscustomer', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');--}}
{{--    }--}}

{{--    function HandlePopupResult(result) {--}}
{{--        if (result) {--}}
{{--            let clients = result.clients;--}}
{{--            let types = $(`input[name=source]:checked`).val();--}}
{{--            if (types == 'lead') {--}}
{{--                lead_clients = clients;--}}
{{--            } else {--}}
{{--                customer_clients = clients;--}}
{{--            }--}}
{{--            var option = '';--}}
{{--            for (let c of clients) {--}}
{{--                option = option + `<option value='${c.id}'>${c.name}</option>`;--}}
{{--            }--}}
{{--            $('#pos_customer_id').html(option);--}}
{{--            setTimeout(function() {--}}
{{--                $('#pos_customer_id').val(result.lastcreated);--}}
{{--                $("#ajax_status").after("<span style='color:green;' id='status_update'>client sucessfully created</span>");--}}
{{--                $('#status_update').delay(3000).fadeOut('slow');--}}
{{--            }, 500);--}}
{{--        } else {--}}
{{--            $("#ajax_status").after("<span style='color:red;' id='status_update'>failed to create clients</span>");--}}
{{--            $('#status_update').delay(3000).fadeOut('slow');--}}
{{--        }--}}
{{--    }--}}
{{--  $(document).on('hidden.bs.modal', '#modal_dialog' , function(e){--}}
{{--        $('#modal_dialog .modal-content').html('');--}}
{{--   });--}}


{{--  function postToedm(){--}}


{{--    let waiter = prompt('Enter Waiter Name');--}}

{{--    if(waiter.trim()){--}}
{{--         let url = `{{ route('admin.order.edm_print_post',$order->id) }}?waiter=${waiter}`;--}}
{{--        location.href = url;--}}

{{--        return;--}}
{{--    }--}}



{{--  }--}}


{{--</script>--}}



<script type="text/javascript">
    $(document).ready(function() {
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    });
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });



</script>
<form method="post" action="/admin/hotel/postbills/outlets/{{$order->id}}"
      id="postBillForm">
    {{ csrf_field() }}

</form>

<script>
    function submitPostbill(){

        let c = confirm("Are you sure");

        if(!c){
            return;
        }
        $('#postBillForm').submit();


    }
    function saveOrder(){
        var total = Number($('#taxableamount').val())
        var invoice_type = $('#invoice_type').val()
        if (invoice_type == '') {
            alert('Please select invoice type')
            return false

        } else if (total > 10000 && invoice_type == 'abbr') {
            alert("Abbreviated Invoice cannot be created. Total taxable amount is greater than Rs. 10,000.");
            return false

        } else {
            $('#updateOrderForm').submit();

        }

    }
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $(document).on('change', '.product_id', function() {
        var parentDiv = $(this).parent().parent();
        var el = $(this);
        if (this.value != 'NULL') {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST"
                , contentType: "application/json; charset=utf-8"
                ,  url: "/admin/products/GetProductDetailAjax/" + this.value.replace('/','') + '?_token=' + _token+`&term=${this.value}`
                , success: function(result) {
                    var obj = jQuery.parseJSON(result.data);
                    if (obj == null) {
                        el.val('');
                        return;
                    }
                    parentDiv.find('.price').val(obj.price);
                    parentDiv.find('.includes_tax').val(obj.includes_tax);
                    // parentDiv.find('.tax_amount').val(obj.tax_amount);
                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                        var total = parentDiv.find('.quantity').val() * obj.price;
                    } else {
                        var total = obj.price;
                    }

                    // var tax = parentDiv.find('.tax_rate').val();
                    // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                    //     tax_amount = total * Number(tax) / 100;
                    //     parentDiv.find('.tax_amount').val(tax_amount);
                    //     total = total + tax_amount;
                    // } else
                    //     parentDiv.find('.tax_amount').val('0');

                    parentDiv.find('.total').val(total);
                    calcTotal();
                    $('#discount_percent').trigger('change');

                },
                error: function(xhr, status, error) {
                    parentDiv.find('.product_id').val('');
                    parentDiv.find('.price').val('');
                    parentDiv.find('.total').val('');
                    parentDiv.find('.tax_amount').val('');
                    parentDiv.find('.product_id').removeAttr('title')
                    calcTotal();
                    $('#discount_percent').trigger('change');

                }
            });
        } else {
            parentDiv.find('.price').val('');
            parentDiv.find('.total').val('');
            // parentDiv.find('.tax_amount').val('');
            calcTotal();
            $('#discount_percent').trigger('change');

        }
    });

    $(document).on('change', '.customer_id', function() {
        // if (this.value != '') {
        //     $(".quantity").each(function(index) {
        //         var parentDiv = $(this).parent().parent();
        //         if (isNumeric($(this).val()) && $(this).val() != '')
        //             var total = $(this).val() * parentDiv.find('.price').val();
        //         else
        //             var total = parentDiv.find('.price').val();
        //
        //         var tax = parentDiv.find('.tax_rate').val();
        //         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
        //             tax_amount = total * Number(tax) / 100;
        //             parentDiv.find('.tax_amount').val(tax_amount);
        //             total = total + tax_amount;
        //         } else
        //             parentDiv.find('.tax_amount').val('0');
        //
        //         if (isNumeric(total) && total != '') {
        //             parentDiv.find('.total').val(total);
        //             calcTotal();
        //         }
        //         //console.log( index + ": " + $(this).text() );
        //     });
        // } else {
        //     // $('.total').val('0');
        //     // $('.tax_amount').val('0');
        //     calcTotal();
        // }
        calcTotal();
        $('#discount_percent').trigger('change');
    });
    function calculateLineTotal(parentDiv){
        var quantity = Number(parentDiv.find('.quantity').val());
        var price = Number( parentDiv.find('.price').val());
        var includes_tax = Number( parentDiv.find('.includes_tax').val());
        var tax_amount = 0;
        if (includes_tax==1){
            var amount_without_tax=price/1.13;
            tax_amount=price-amount_without_tax;
            price=price-tax_amount;
        }
        // var tot = quantity * price;
        // var dis_amt = Number(parentDiv.find('.discount_percent_line').val())*tot/100;
        // parentDiv.find('.discount_amount_line').val(dis_amt.toFixed(2));
        // var discount=Number(dis_amt)


        var total=''
        if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
            // total = (price * quantity)-discount;
            total = (price * quantity);
        } else
            total = '';

        if (includes_tax==1 && (total != 0 || total != '')) {
            tax_amount = quantity * Number(tax_amount);
            total = total + tax_amount;
        }
        // else
        parentDiv.find('.tax_amount').val(tax_amount.toFixed(2));

        parentDiv.find('.total').val(total.toFixed(2));
    }

    $(document).on('input', '.quantity', function() {
        var parentDiv = $(this).parent().parent();
        // if (isNumeric(this.value) && this.value != '') {
        //     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
        //         var total_amount = parentDiv.find('.price').val() * parentDiv.find('.quantity').val();
        //         var total = total_amount ;
        //     } else
        //         var total = '';
        // } else
        //     var total = '';
        //
        // var tax = parentDiv.find('.tax_rate').val();
        // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
        //     tax_amount = total * Number(tax) / 100;
        //     parentDiv.find('.tax_amount').val(tax_amount);
        //     total = total + tax_amount;
        // } else
        //     parentDiv.find('.tax_amount').val('0');

        // parentDiv.find('.total').val(total);
        calculateLineTotal(parentDiv)
        calcTotal();
        $('#discount_percent').trigger('change');
    });

    $(document).on('change', '.price', function() {

        var parentDiv = $(this).parent().parent().parent();

        // console.log(parentDiv);

        // if (isNumeric(this.value) && this.value != '') {
        //
        //     if (isNumeric(parentDiv.find('.price').val()) && parentDiv.find('.price').val() != '') {
        //
        //         var total_amount = parentDiv.find('.price').val() * parentDiv.find('.quantity').val();
        //         var total = total_amount ;
        //     } else
        //         var total = '';
        // } else
        //     var total = '';
        //
        // var tax = parentDiv.find('.tax_rate').val();
        // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
        //     tax_amount = total * Number(tax) / 100;
        //     parentDiv.find('.tax_amount').val(tax_amount);
        //     total = total + tax_amount;
        // } else
        //     parentDiv.find('.tax_amount').val('0');

        // parentDiv.find('.total').val(total);
        calculateLineTotal(parentDiv)

        //console.log(total);

        //alert('done');
        calcTotal();
        $('#discount_percent').trigger('change');

    });

    $(document).on('change', '.price', function() {
        var parentDiv = $(this).parent().parent().parent();
        // console.log(parentDiv.find('.price').val());
        // if (isNumeric(this.value) && this.value != '') {
        //     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
        //         var total_amount = parentDiv.find('.price').val() * parentDiv.find('.quantity').val();
        //         var total = total_amount ;
        //
        //     } else
        //         var total = '';
        // } else
        //     var total = '';
        //
        // var tax = parentDiv.find('.tax_rate').val();
        // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
        //     tax_amount = total * Number(tax) / 100;
        //     parentDiv.find('.tax_amount').val(tax_amount);
        //     total = total + tax_amount;
        // } else
        //     parentDiv.find('.tax_amount').val('0');
        // console.log(total);
        // parentDiv.find('.total').val(total);
        calculateLineTotal(parentDiv)
        //console.log(total);

        //alert('done');
        calcTotal();
        $('#discount_percent').trigger('change');
    });

    // $(document).on('change', '.tax_rate', function() {
    //     var parentDiv = $(this).parent().parent();
    //
    //     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
    //         var total = parentDiv.find('.price').val() * Number(parentDiv.find('.quantity').val());
    //     } else
    //         var total = '';
    //
    //     var tax = $(this).val();
    //     if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
    //         tax_amount = total * Number(tax) / 100;
    //         parentDiv.find('.tax_amount').val(tax_amount);
    //         total = total + tax_amount;
    //     } else
    //         parentDiv.find('.tax_amount').val('0');
    //
    //     parentDiv.find('.total').val(total);
    //     calcTotal();
    // });

    /*$('#discount').on('change', function() {
        if(isNumeric(this.value) && this.value != '')
        {
            if(isNumeric($('#sub-total').val()) && $('#sub-total').val() != '')
                parentDiv.find('.total').val($('#sub-total').val() - this.value).trigger('change');
        }
    });

    $("#sub-total").bind("change", function() {
        if(isNumeric($('#discount').val()) && $('#discount').val() != '')
            parentDiv.find('.total').val($('#sub-total').val() - $('#discount').val());
        else
            parentDiv.find('.total').val($('#sub-total').val());
    });*/

    $("#addMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").before($('#orderFields #more-tr').html());
    });
    $("#addCustomMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").before($('#CustomOrderFields #more-custom-tr').html());
    });

    $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();
        calcTotal();
        $('#discount_percent').trigger('change');

    });

    $(document).on('change', '#vat_type', function() {
        calcTotal();
    });

    $(document).on('change', '#service_type', function() {
        calcTotal();
    });

    $(document).on('change', '#discount', function() {

        calcTotal();

    });


    function calcTotal() {
        //alert('hi');
        var subTotal = 0;
        // var service_total = 0;
        // var service_charge = 0;
        var taxableAmount = 0;
        var nontaxableAmount = 0;

        //var tax = Number($('#tax').val().replace('%', ''));
        var total = 0;
        var tax_amount = 0;
        var taxableTax = 0;
        var discount_amount = 0;

        $(".total").each(function(index) {
            var parentDiv=$(this).parent().parent().parent()
            if (isNumeric($(this).val())&&$(this).val()!=''){

                var quantity = Number(parentDiv.find('.quantity').val());
                var price = Number( parentDiv.find('.price').val());
                var tax =0;
                var includes_tax =Number(parentDiv.find('.includes_tax').val());
                var discount_percent=$('#discount_percent').val()

                if (includes_tax==1){
                    var amount_without_tax=price/1.13;
                    if(discount_percent){
                        amount_without_tax=amount_without_tax-(Number(discount_percent)/100)*amount_without_tax
                    }
                    tax=(13/100)*amount_without_tax;
                    taxableAmount+=amount_without_tax*quantity
                    tax_amount=tax_amount+tax*quantity
                }else {
                    if(discount_percent){
                        price=price-(Number(discount_percent)/100)*price
                    }
                    nontaxableAmount+=price*quantity
                }
                // var tot = quantity * price;
                // var discount=Number(parentDiv.find('.discount_amount_line').val())


                subTotal = Number(subTotal) + Number($(this).val());
                total= Number(total) + Number($(this).val());
                // discount_amount=discount_amount+Number(parentDiv.find('.discount_amount_line').val())

            }
        });

        // $(".tax_amount").each(function(index) {
        //     if (isNumeric($(this).val()))
        //         tax_amount = Number(tax_amount) + Number($(this).val());
        // });




        if($('#discount_amount').val().trim()){

            let amount_after_discount1 = Number(subTotal) - Number($('#discount_amount').val()) ;

            total=amount_after_discount1
            // $('#amount-after-discount').text(amount_after_discount1);

        }




        $('#sub-total').html(subTotal.toFixed(2));
        $('#subtotal').val(subTotal.toFixed(2));

        // $('#taxable-amount').html(subTotal);
        // $('#taxableamount').val(subTotal);

        // var service_type = $('#service_type').val();
        // var discount_amount = $('#discount_amount').val();
        // if(discount_amount.trim() == '' || discount_amount == '0'){
        //
        //  discount_amount =    getDiscountAmount();
        // }

        // var vat_type = $('#vat_type').val();
        // let discounttype = $('#discount').val();
        //
        //
        // if (discounttype == 'percentage') {
        //
        //     if (isNumeric(discount_amount) && discount_amount != 0) {
        //         amount_after_discount = subTotal - (Number(discount_amount) / 100 * subTotal);
        //     } else {
        //         amount_after_discount = subTotal;
        //     }
        //
        //     if (service_type == 'no' || service_type == '') {
        //         service_total = amount_after_discount;
        //         service_charge = 0;
        //         taxableAmount = service_total;
        //     } else {
        //         service_total = amount_after_discount + Number(10 / 100 * amount_after_discount);
        //         service_charge = Number(10 / 100 * amount_after_discount);
        //         taxableAmount = service_total;
        //     }
        //     taxableAmount=taxableAmount.toFixed(2)
        //
        //     total = Number(taxableAmount) + Number(13 / 100 * taxableAmount);
        //     taxableTax = Number(13 / 100 * taxableAmount);
        //
        // } else {
        //
        //     if (isNumeric(discount_amount) && discount_amount != 0) {
        //         amount_after_discount = subTotal - (Number(discount_amount));
        //     } else {
        //         amount_after_discount = subTotal;
        //     }
        //
        //     if (service_type == 'no' || service_type == '') {
        //         service_total = amount_after_discount;
        //         service_charge = 0;
        //         taxableAmount = service_total;
        //     } else {
        //         service_total = amount_after_discount + Number(10 / 100 * amount_after_discount);
        //         service_charge = Number(10 / 100 * amount_after_discount);
        //         taxableAmount = service_total;
        //     }
        //     taxableAmount=taxableAmount.toFixed(2)
        //
        //     total = Number(taxableAmount) + Number(13 / 100 * taxableAmount);
        //     taxableTax = Number(13 / 100 * taxableAmount);
        //
        // }
        //
        // $('#service_charge').val(service_charge.toFixed(2));
        // $('#service-charge').html(service_charge.toFixed(2));
        //
        // $('#amount_with_service').val(service_total);
        // $('#amount-with-service').html(service_total);

        // $('#discount_amount').val(discount_amount.toFixed(2));
        // $('#discount-amount').html(discount_amount.toFixed(2));

        $('#taxableamount').val(taxableAmount.toFixed(2));
        $('#taxable-amount').html(taxableAmount.toFixed(2));

        $('#non_taxable_amount').val(nontaxableAmount.toFixed(2));
        $('#non-taxable-amount').html(nontaxableAmount.toFixed(2));

        $('#total_tax_amount').val(tax_amount.toFixed(2));

        $('#taxabletax').val(tax_amount.toFixed(2));
        $('#taxable-tax').html(tax_amount.toFixed(2));


        $('#total').html(total.toFixed(2));
        $('#total_').val(total.toFixed(2));


        calculateTotalQty();


    }

    function calculateTotalQty()
    {

        var totalQty = 0;

        $('#kana-items .quantity').each(function(){

            totalQty += Number($(this).val());
            console.log(totalQty);

        });

        $('#qty-total').text(totalQty);

    }



    $(document).on('keyup', '#discount_amount', function() {

        let subtotal = Number( $('#sub-total').text() );
        let discount = Number($(this).val());
        if(subtotal > 0){
            let percent = ( discount / subtotal )* 100;
            $('#discount_percent').val(percent.toFixed(3));
        }


        calcTotal();

    });


    function posdisount(pos_customer_id){
        if (pos_customer_id != '') {
            $.get(`/admin/getposdiscount/${pos_customer_id}`,function(response){
                $('#discount_amount').val('');
                $('#discount_percent').val(response.pos_discount);
                $('#discount_percent').trigger('change');
                calcTotal();
            });
        }

    }



    $('#pos_customer_id').change(function() {
        var pos_customer_id = $(this).val();

        if (pos_customer_id != '') {

            $('#reservation_id').val('');

            posdisount(pos_customer_id);


        }
    });



</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('.customer_id').select2();
        $('.pos_customer_id').select2();

        let dtype = $('#discount').val();

        if (dtype == 'percentage') {
            $('.discounttype').html('(%)');
        } else {

            $('.discounttype').html('(Amount)');
        }

    });

    $('#discount').on('change', function() {
        $('#discount_amount').val('');
        type = $(this).val();

        if (type == 'percentage') {
            $('.discounttype').html('(%)');
        } else {

            $('.discounttype').html('(Amount)');
        }
        calcTotal();

    });



</script>


<script type="text/javascript">


</script>

<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>

<style type="text/css">
    .intl-tel-input {
        width: 100%;
    }

    .intl-tel-input .iti-flag .arrow {
        border: none;
    }

</style>

<script type="text/javascript">
    function myfun(value) {
        $(value).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/admin/getProducts"
                    , dataType: "json"
                    , data: {
                        term: request.term
                        , outlet_id: $("#outlet_id").val()
                        , menu_id: $("#menu_id").val()
                    }
                    , success: function(data) {
                        response(data);
                        addproducttype();
                    }
                });
            }
            , minLength: 1
            , select: function(event, ui) {
                setTimeout(()=>{
                    price(value);
                    var text=ui.item.value
                    $(this).parent().find('.product_id').prop('title',text)
                },100);

            }
            , });
    }

    function price(value) {

        var parentDiv = $(value).parent().parent();

        var el = $(value);

        if (value.value != 'NULL') {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST"
                , contentType: "application/json; charset=utf-8"
                ,  url: "/admin/products/GetProductDetailAjax/" + value.value.replace('/','') + '?_token=' + _token+`&term=${value.value}`
                , success: function(result) {


                    var obj = jQuery.parseJSON(result.data);

                    if (obj == null) {
                        el.val('');
                        return;
                    }
                    let prevItem = $(`table #kana-items #item-${obj.id}`);
                    if (prevItem.length > 0) {
                        calcTotal();
                        return;
                    }

                    parentDiv.find('.price').val(obj.price);
                    parentDiv.find('.includes_tax').val(obj.includes_tax);
                    parentDiv.find('.tax_amount').val(obj.tax_amount);

                    parentDiv.attr('id', 'item-' + obj.id);

                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                        var total = parentDiv.find('.quantity').val() * obj.price;
                    } else {
                        var total = obj.price;
                    }

                    // var tax = parentDiv.find('.tax_rate').val();
                    // if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                    //     tax_amount = total * Number(tax) / 100;
                    //     parentDiv.find('.tax_amount').val(tax_amount);
                    //     total = total + tax_amount;
                    // } else
                    //     parentDiv.find('.tax_amount').val('0');

                    parentDiv.find('.total').val(total);
                    calcTotal();
                    $('#discount_percent').trigger('change');

                },
                error: function(xhr, status, error) {
                    parentDiv.find('.product_id').val('');
                    parentDiv.find('.price').val('');
                    parentDiv.find('.total').val('');
                    parentDiv.find('.tax_amount').val('');
                    parentDiv.find('.product_id').removeAttr('title')
                    calcTotal();
                    $('#discount_percent').trigger('change');

                }
            });
        } else {
            parentDiv.find('.price').val('');
            parentDiv.find('.total').val('');
            parentDiv.find('.tax_amount').val('');
            calcTotal();
            $('#discount_percent').trigger('change');

        }
    }

</script>

<script>
    $(function() {

        var firstTimeMenuAdded = true;

        $('#menu_id').on('change', function() {
            var outlet_id = $('#outlet_id').val();
            if ($(this).val() != '') {
                $.ajax({
                    url: "/admin/users/ajax/getProductcategory"
                    , data: {
                        menu_id: $(this).val()
                        ,outlet_id: outlet_id
                    }
                    , dataType: "json"
                    , success: function(data) {
                        var result = data.data;
                        $('#productcategory_id').html(result);
                        if(firstTimeMenuAdded){
                            $('#productcategory_id').val('{{ $selectedOutlet->default_category_id }}');
                            firstTimeMenuAdded= false;
                        }
                    }
                });
            }
        });
        $('#menu_id').trigger('change');


    });

</script>

<script>
    $(function() {

        $('#productcategory_id').on('change', function() {
            var outlet_id = $('#outlet_id').val();

            if ($(this).val() != '') {

                $.ajax({
                    url: "/admin/users/ajax/getProduct"
                    , data: {
                        category_id: $(this).val()
                        , outlet_id: outlet_id
                    }
                    , dataType: "json"
                    , success: function(data) {

                        var result = data.data;
                        $('#produt_list').html(result);

                    }
                });
            }
        });
        $('#productcategory_id').trigger('change');
    });


    function AddMore(val) {

        let name = $(val).data('name');
        var price = $(val).data('price');
        var includes_tax = $(val).data('includes_tax');
        var tax_amount = $(val).data('tax_amount');
        var app_currency = `{{env('APP_CURRENCY')}}`;
        let itemid = $(val).data('id');
        var prevItem = $(`table #kana-items #item-${itemid}`)
        if (prevItem.length > 0) {
            let quantityEl = prevItem.find('.quantity');
            let priceEl = prevItem.find('.price');
            let prevQty = Number(quantityEl.val());
            let newQty = prevQty + 1;
            let newTotal = newQty * Number(priceEl.val());
            let totalEl = prevItem.find('.total');
            quantityEl.val(newQty);
            totalEl.val(newTotal);
            let taxEl = prevItem.find('.tax_amount');

            let newTaxAmount = newQty * Number(tax_amount);
            taxEl.val(newTaxAmount);
            calcTotal();
            $('#discount_percent').trigger('change');
            return false;
        }
        // let dom = '<tr id="item-' + itemid + '"><td ><input type="text" name="product_id[]" title="'+name+'" class="form-control product_id" value="' + name + '" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)"><input type="hidden" name="includes_tax[]" class="includes_tax" value="'+includes_tax+'"><input type="hidden" name="tax_amount[]" class="tax_amount" value="'+tax_amount+'"></td><td ><div class="input-group"><input type="text" class="form-control price" name="price[]" placeholder="Price" readonly value="' + price + '" required="required"></div></td><td><input type="text" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1" required="required"><td><input type="text" name="discount_percent[]" placeholder="Percent" value="0" class="form-control discount_percent_line"></td><td ><input type="text" name="discount_amount[]" placeholder="Amount" value="0" class="form-control discount_amount_line" ></td></td><td><input type="text" class="form-control" name="remarks[]" placeholder="Remarks"></td><td ><div class="input-group"> <input type="text" class="form-control total" name="total[]" placeholder="Total" value="' + price + '" readonly="readonly"><a href="javascript::void(1);"><i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i></a></div></td></tr>';
        let dom = '<tr id="item-' + itemid + '"><td ><input type="text" name="product_id[]" title="'+name+'" class="form-control product_id" value="' + name + '" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)"><input type="hidden" name="includes_tax[]" class="includes_tax" value="'+includes_tax+'"><input type="hidden" name="tax_amount[]" class="tax_amount" value="'+tax_amount+'"></td><td ><div class="input-group"><input type="text" class="form-control price" name="price[]" placeholder="Price" readonly value="' + price + '" required="required"></div></td><td><input type="number" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1" required="required"></td><td><input type="text" class="form-control" name="remarks[]" placeholder="Remarks"></td><td ><div class="input-group"> <input type="text" class="form-control total" name="total[]" placeholder="Total" value="' + price + '" readonly="readonly"></div></td><td><a href="javascript::void(1);"><i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="color: #fff;"></i></a></td></tr>';

        $(".multipleDiv").before(dom);

        addproducttype();
        calcTotal();

        $('#discount_percent').trigger('change');

        $('#kana-items tr').last().find('.product_id').focus();
    }


    $('#reservation_id').change(function() {

        let reservation_val = $(this).val();
        $.get('/admin/getpanno/fromreservation/' + reservation_val, function(data, status) {
            $('#pan_no').val(data.vat_no);
            $('#company_name').val(data.company_name);
        });

    });

    function validateDiscount(){


        let   amount = $('#discount_percent');
        let max_discount = Number("{{env('MAX_DISCOUNT_PERCENT',50)}}");
        if (Number(amount.val()) > max_discount) {
            amount.val(max_discount);
        }

        return ;

    }
    $('#discount_percent').on('keyup change',function(){

        let total = $('input#subtotal').val();
        if(total == 0){


            $(this).val(0);
            return;
        }


        validateDiscount();
        if($(this).val().trim() == ''){

            $('input#discount_amount').prop('readonly',false);

        }else{

            $('input#discount_amount').prop('readonly',true);

        }

        let percent = ( $(this).val() /100 ) * total;
        $('input#discount_amount').val(percent.toFixed(2));


        calcTotal();



    });
    function addproducttype(){

        var allitemId = [];
        $('tbody#kana-items tr').each(function(){

            var id = $(this).attr('id');

            if(id){

                let i =  id.replace("item-", "");
                allitemId.push(i);

            }

        });

        let data = {id: allitemId};

        $.get('/admin/getfoodcategorybyid',data,function(response){

            let itemcat = response.itemcat;

            let cattype = response.types;

            for(let item of itemcat){

                $(`#item-${item.id}`).attr('types-id',item.cid);
            }


            var htmlel = `<table class="table">
                        <tr>
                            <th>Category</th>
                            <th>Discount %</th>
                            <td>Total<td>
                        </tr>
                        `;
            for(let cat of cattype){

                htmlel +=  `<tr class='type-discount-parent'>
                    <td> ${cat.name}</td>
                    <td><input type='number' class='form-control line-discount-bytype input-sm' steps='any' data-cid ='${cat.id}'
                        data-dlimt = '${cat.discount_limit}'
                        ></td>
                    <td><input type='number' class='form-control line-total' readonly></td>
            </tr>
            `;


            }
            if($('input#category-discount').is(':checked') ){

                $('#category-discount-box').html(htmlel);
                //console.log(response);
                document.getElementById("category-discount-box").scrollIntoView();


            }

        });



    }




    $('input#category-discount').change(function(){


        if(!$(this).is(':checked')){
            $('#discount_percent').attr('readonly',false);
            $('#discount_amount').attr('readonly',false);
            $('#category-discount-box').html('');
            return ;
        }
        $('#discount_percent').val(0);
        $('#discount_percent').attr('readonly',true);
        $('#discount_amount').attr('readonly',true);

        addproducttype();

    });
    function totaldiscountall(){ //total discount by categotry

        var total = 0;

        $('#category-discount-box input.line-total').each(function(){

            total += Number($(this).val());


        });

        $('#discount_amount').val(Number(total).toFixed(2));

        calcTotal();

    }

    $(document).on('change keyup','.line-discount-bytype',function(){
        let selectdiscount = $(this).val();
        let discount_limit = $(this).attr('data-dlimt');

        if(selectdiscount > discount_limit){
            $(this).val(discount_limit);
            $(this).trigger('change');
            return;
        }



        let cid = $(this).attr('data-cid');
        let parent = $(this).parent().parent();

        var totaldiscount = 0;
        var totalamount = 0;
        $(`tr[types-id=${cid}]`).each(function(){
            let total = $(this).find('input.total').val();
            totalamount += Number(total);
        });
        totaldiscount = ( selectdiscount / 100 ) * totalamount;
        parent.find('.line-total').val(totaldiscount);

        var subtotal=Number($('#subtotal').val());
        if(subtotal > 0){
            var percent = ( totaldiscount / subtotal )* 100;
            $('#discount_percent').val(percent.toFixed(3));

        }

        totaldiscountall();
        $('#discount_percent').trigger('change');


    });


    $(function(){



        calcTotal();
    });
    $(document).on('keyup','.product_id',function(e){

        if(e.keyCode==13){


            $(this).parent().parent().find('.quantity').focus()

        }

        e.preventDefault();



    });
    $(document).on('keyup','.quantity',function(e){

        if(e.keyCode==13){


            $('#addMore').trigger('click');

            setTimeout(function(){
                $('#kana-items tr').last().prev().find('.product_id').focus();


            },500)

        }

        e.preventDefault();



    });


    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });
    function openwindowposcustomer() {
        var win = window.open('/admin/clients/modals/create?relation_type=poscustomer', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
    }

    function HandlePopupResult(result) {
        if (result) {
            let clients = result.clients;
            let types = $(`input[name=source]:checked`).val();
            if (types == 'lead') {
                lead_clients = clients;
            } else {
                customer_clients = clients;
            }
            var option = '';
            for (let c of clients) {
                option = option + `<option value='${c.id}'>${c.name}</option>`;
            }
            $('#pos_customer_id').html(option);
            setTimeout(function() {
                $('#pos_customer_id').val(result.lastcreated);
                $("#ajax_status").after("<span style='color:green;' id='status_update'>client sucessfully created</span>");
                $('#status_update').delay(3000).fadeOut('slow');
            }, 500);
        } else {
            $("#ajax_status").after("<span style='color:red;' id='status_update'>failed to create clients</span>");
            $('#status_update').delay(3000).fadeOut('slow');
        }
    }
    $(document).on('hidden.bs.modal', '#modal_dialog' , function(e){
        $('#modal_dialog .modal-content').html('');
    });


    function postToedm(){


        let waiter = prompt('Enter Waiter Name');

        if(waiter.trim()){
            let url = `{{ route('admin.order.edm_print_post',$order->id) }}?waiter=${waiter}`;
            location.href = url;

            return;
        }



    }


</script>
@endsection

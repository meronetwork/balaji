@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')

<style>
    .panel .mce-panel {
        border-left-color: #fff;
        border-right-color: #fff;
    }

    .panel .mce-toolbar,
    .panel .mce-statusbar {
        padding-left: 20px;
    }

    .panel .mce-edit-area,
    .panel .mce-edit-area iframe,
    .panel .mce-edit-area iframe html {
        padding: 0 10px;
        min-height: 350px;
    }

    .mce-content-body {
        color: #555;
        font-size: 14px;
    }

    .panel.is-fullscreen .mce-statusbar {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 200000;
    }

    .panel.is-fullscreen .mce-tinymce {
        height: 100%;
    }

    .panel.is-fullscreen .mce-edit-area,
    .panel.is-fullscreen .mce-edit-area iframe,
    .panel.is-fullscreen .mce-edit-area iframe html {
        height: 100%;
        position: absolute;
        width: 99%;
        overflow-y: scroll;
        overflow-x: hidden;
        min-height: 100%;
    }

</style>

@endsection

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<?php

    $is_fnb = \App\Models\PosOutlets::find(\Request::get('outlet_id'))->fnb_outlet;

?>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <div class="row">
        <div class="col-md-4">
            <?php
                   $outlet_details = \App\Models\PosOutlets::find(\Request::get('outlet_id'));      

                   $outlet_code = $outlet_details->outlet_code;
             ?>
            <h2> {{ $outlet_details->name }}</h2>
        </div>


        <div class="col-md-4">
            <span>{{ date('d F, Y', strtotime(date('Y-m-d'))) }}</span>
            <?php 
                $engdate = explode('-', date('Y-m-d'));

                $nepdata =new \App\Helpers\NepaliCalendar();

                $nepdate = $nepdata->eng_to_nep($engdate[0],$engdate[1],$engdate[2]);
               // dd($nepdate);
            ?>
            <br /> <span>{{$nepdate['date']}} {{$nepdate['nmonth']}}, {{$nepdate['year']}}</span>
        </div>

        <div class="col-md-4">
            @if($is_fnb)
            <a href="/admin/hotel/openresturantoutlets" class="btn btn-default btn-sm">Change Outlet</a>
            @else
            <a href="/admin/hotel/openoutlets" class="btn btn-default btn-sm">Change Outlet</a>
            @endif
        </div>
    </div>

</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box box-body">

            <div id="orderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-tr">
                        <tr>
                            <td>
                                <input type="text" name="product_id[]" class="form-control product_id" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)" autocomplete="off">
                            </td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="text" class="form-control price" name="price[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required" autocomplete="off">
                                </div>
                            </td>
                            <td>
                                <input type="number" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1" required="required">
                            </td>

                            <!-- <td>
                                    <select required name="tax[]" class="form-control tax_rate">
                                        @foreach(config('tax.taxes') as $dk => $dv)
                                        <option value="{{ $dk }}" @if(isset($orderDetail->total) && $orderDetail->tax_amount == $dk) selected="selected" @endif>{{ $dv }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control tax_amount" name="tax_amount[]" @if(isset($orderDetail->tax_amount)) value="{{ $orderDetail->tax_amount }}" @else value="0" @endif readonly="readonly">
                                </td> -->

                            <td>

                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="number" class="form-control total" name="total[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                </div>

                                <a href="javascript::void(1);" style="width: 10%;">
                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                </a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="CustomOrderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-custom-tr">
                        <tr>
                            <td>
                                <input type="text" class="form-control product" name="custom_items_name[]" value="" placeholder="Product" autocomplete="off">
                            </td>

                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="text" class="form-control price" name="custom_items_price[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required" autocomplete="off">
                                </div>
                            </td>

                            <td>
                                <input type="number" class="form-control quantity" name="custom_items_qty[]" placeholder="Quantity" min="1" value="1" required="required">
                            </td>


                            <!--  <td>
                                    <select required name="tax_id_custom[]" class="form-control tax_rate">
                                        @foreach(config('tax.taxes') as $dk => $dv)
                                        <option value="{{ $dk }}" @if(isset($orderDetail->total) && $orderDetail->tax_amount == $dk) selected="selected" @endif>{{ $dv }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control tax_amount" name="custom_tax_amount[]" @if(isset($orderDetail->tax_amount)) value="{{ $orderDetail->tax_amount }}" @else value="0" @endif readonly="readonly">
                                </td> -->
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="number" class="form-control total" name="custom_total[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                </div>

                                <a href="javascript::void(1);" style="width: 10%;">
                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <form method="POST" action="/admin/multiple_orders">
                {{ csrf_field() }}
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-3 form-group" style="">
                        <label for="user_id">Select Reservation Customer</label>
                        <select name="reservation_id" class="form-control customer_id input-sm" id="reservation_id">
                            <option class="form-control input input-lg" value="">Select Resv Customer</option>
                            @if(isset($reservation))
                            @foreach($reservation as $key => $uk)
                            <option value="{{ $uk->id }}" @if($orderDetail && $uk->id ==
                                $orderDetail->customer_id){{ 'selected="selected"' }}@endif>{{ '('.env('APP_CODE'). $uk->id.') '.$uk->guest_name.' -('.$uk->room_num.')' }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="col-md-3 form-group" style="">
                        <label for="user_id">Select POS Customer<a title="Create New POS Customer" href="#" onclick="openwindowposcustomer()">[+]</a></label>
                        <select name="pos_customer_id" class="form-control pos_customer_id input-sm" id="pos_customer_id">
                            <option class="form-control input input-lg" value="">Select POS Customer</option>
                            @if(isset($clients))
                            @foreach($clients as $key => $uk)
                            <option value="{{ $uk->id }}" @if($uk->id ==
                                env('CASH_GUEST_CLIENT_ID')){{ 'selected="selected"' }}@endif>{{ '('.env('APP_CODE'). $uk->id.') '.$uk->name.'('.$uk->location.')' }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                    <?php 
                           $table_name = \App\Models\PosTable::find(\Request::get('table_id'));
                        ?>

                    @if($is_fnb)
                    <div class="col-md-3 form-group" style="">
                        <label for="comment">Covers</label>
                        <input type="text" name="covers" id="cover" class="form-control input-sm" value="{{$table_name->seating_capacity}}">
                    </div>

                    <div class="col-md-3 form-group" style="">
                        <label for="comment">Table</label>
                        <input type="text" class="form-control input-sm" value="{{ $table_name->tablearea->posfloor->outlet->outlet_code }} > {{$table_name->tablearea->posfloor->name }} > {{$table_name->tablearea->name }} > {{$table_name->table_number}}">

                        <input type="hidden" name="table" value="{{$table_name->id}}">


                    </div>
                    @endif

                </div>

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Service Charge:</label>
                            <select type="text" class="form-control pull-right " name="service_type" id="service_type">
                                <option value="">Select</option>
                                <option value="no">No</option>
                                <option value="yes" selected>Yes({{env('SERVICE_CHARGE')}}%)</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Discount:</label>
                            <select type="text" class="form-control pull-right " name="discount_note" id="discount">
                                <option value="percentage">Percentage</option>
                                <option value="amount">Amount</option>
                            </select>
                        </div>
                    </div>


                    <?php 
                                $outlet_id = \Request::get('outlet_id');
                                $menus = \App\Models\PosMenu::where('outlet_id',$outlet_id)->pluck('menu_name','id')->all();
                         ?>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Menu:</label>
                            {!! Form::select('menu_id',[''=>'Select']+$menus,null, ['class' => 'form-control customer_id', 'id'=>'menu_id']) !!}
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label> Product category </label>
                            <select name="productcategory_id" id="productcategory_id" class="form-control select_box customer_id">
                                <option value="">Select Product Category...</option>
                                @if($productscategory)
                                @foreach($productscategory as $uk => $uv)
                                <option value="{{ $uv->id }}">{{ $uv->name }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="outlet_id" value="{{\Request::get('outlet_id')}}">
                    <input type="hidden" name="session_id" value="{{\Request::get('session_id')}}">
                    <input type="hidden" name="bill_date" value="{{ \Carbon\Carbon::now() }}">


                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label> Bill No </label>
                            <input type="text" name="bill_no" id="name" class="form-control input-sm" value="{{$bill_no}}" readonly>
                        </div>
                    </div>

                    @if($is_fnb)

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Bill To:</label>
                            {!! Form::select('bill_to',['normal'=>'Normal','bwstoguestandcompany'=>'BWS To Guest and Bill To Comapny'],null, ['class' => 'form-control customer_id', 'id'=>'menu_id']) !!}
                        </div>
                    </div>

                    @endif

                </div>



                <input type="hidden" name="order_type" value="invoice">

                <div class="col-md-12 text-right">
                    <a href="javascript::void(0)" class="btn btn-default btn-xs addMore" id="">
                        <i class="fa fa-plus"></i> <span>Add Products Item</span>
                    </a>

                    &nbsp; &nbsp; &nbsp;
                </div>

                <hr />
                <style>
                    #produt_list .col-md-3 {
                        padding-left: 5px;
                        padding-right: 5px;
                    }

                    #produt_list .inner {
                        padding: 5px;
                        text-align: center;
                    }

                    #produt_list .inner h4 {
                        font-size: 14px;
                        margin: 0;
                    }

                    #produt_list .small-box {
                        margin-bottom: 10px;
                    }

                </style>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-body">
                                <ul class="products-list product-list-in-box" id="produt_list">

                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <table class="table table-bordered">

                                <thead>
                                    <tr>
                                        <th>Products*</th>
                                        <th>Price*</th>
                                        <th>Quantity*</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr class="multipleDiv">
                                    </tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="text-align: right;">Amount</td>
                                        <td id="sub-total">0.00</td>
                                        <td>&nbsp; <input type="hidden" name="subtotal" id="subtotal" value="0"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">POS Discount <span class="discounttype">(%)</span></td>
                                        <td><input type="number" min="0" name="discount_percent" id="discount_amount" value=""></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Service Charge</td>
                                        <td id="service-charge">0.00</td>
                                        <td>&nbsp; <input type="hidden" name="service_charge" id="service_charge" value="0"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Amount With Service Charge</td>
                                        <td id="amount-with-service">0.00</td>
                                        <td>&nbsp; <input type="hidden" name="amount_with_service" id="amount_with_service" value="0"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Taxable Amount</td>
                                        <td id="taxable-amount">0.00</td>
                                        <td>&nbsp; <input type="hidden" name="taxable_amount" id="taxableamount" value="0"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: right;">Tax Amount</td>
                                        <td id="taxable-tax">0.00</td>
                                        <td>&nbsp; <input type="hidden" name="taxable_tax" id="taxabletax" value="0"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: right; font-weight: bold;">Total Amount</td>
                                        <td id="total">0.00</td>
                                        <td>
                                            <input type="hidden" name="total_tax_amount" id="total_tax_amount" value="0">
                                            <input type="hidden" name="final_total" id="total_" value="0">
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="panel-footer text-right">
                                <button type="submit" class="btn btn-social btn-foursquare">
                                    <i class="fa fa-save"></i>Save Order
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </form>
        </div><!-- /.box-body -->
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection

@section('body_bottom')
<!-- form submit -->
@include('partials._body_bottom_submit_bug_edit_form_js')

<script type="text/javascript">
    // $(function() {
    //     $('.datepicker').datetimepicker({
    //       //inline: true,
    //       format: 'YYYY-MM-DD',
    //       sideBySide: true,
    //       allowInputToggle: true
    //     });

    //   });

</script>

<script>
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $(document).on('change', '.product_id', function() {
        var parentDiv = $(this).parent().parent();
        var el = $(this);
        if (this.value != 'NULL') {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST"
                , contentType: "application/json; charset=utf-8"
                , url: "/admin/products/GetProductDetailAjax/" + this.value + '?_token=' + _token
                , success: function(result) {
                    var obj = jQuery.parseJSON(result.data);
                    if (obj == null) {
                        el.val('');
                        return;
                    }
                    parentDiv.find('.price').val(obj.price);

                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                        var total = parentDiv.find('.quantity').val() * obj.price;
                    } else {
                        var total = obj.price;
                    }

                    var tax = parentDiv.find('.tax_rate').val();
                    if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                        tax_amount = total * parseInt(tax) / 100;
                        parentDiv.find('.tax_amount').val(tax_amount);
                        total = total + tax_amount;
                    } else
                        parentDiv.find('.tax_amount').val('0');

                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
            });
        } else {
            parentDiv.find('.price').val('');
            parentDiv.find('.total').val('');
            parentDiv.find('.tax_amount').val('');
            calcTotal();
        }
    });

    $(document).on('change', '.customer_id', function() {
        if (this.value != '') {
            $(".quantity").each(function(index) {
                var parentDiv = $(this).parent().parent();
                if (isNumeric($(this).val()) && $(this).val() != '')
                    var total = $(this).val() * parentDiv.find('.price').val();
                else
                    var total = parentDiv.find('.price').val();

                var tax = parentDiv.find('.tax_rate').val();
                if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                    tax_amount = total * parseInt(tax) / 100;
                    parentDiv.find('.tax_amount').val(tax_amount);
                    total = total + tax_amount;
                } else
                    parentDiv.find('.tax_amount').val('0');

                if (isNumeric(total) && total != '') {
                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
                //console.log( index + ": " + $(this).text() );
            });
        } else {
            $('.total').val('0');
            $('.tax_amount').val('0');
            calcTotal();
        }
    });

    $(document).on('change', '.quantity', function() {
        var parentDiv = $(this).parent().parent();

        //console.log(parentDiv);

        if (isNumeric(this.value) && this.value != '') {
            if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                var total = parentDiv.find('.price').val() * this.value;
            } else
                var total = '';
        } else
            var total = '';

        var tax = parentDiv.find('.tax_rate').val();
        if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
            tax_amount = total * parseInt(tax) / 100;
            parentDiv.find('.tax_amount').val(tax_amount);
            total = total + tax_amount;
        } else
            parentDiv.find('.tax_amount').val('0');

        parentDiv.find('.total').val(total);
        calcTotal();
    });

    $(document).on('change', '.price', function() {

        var parentDiv = $(this).parent().parent().parent();

        // console.log(parentDiv);

        if (isNumeric(this.value) && this.value != '') {

            if (isNumeric(parentDiv.find('.price').val()) && parentDiv.find('.price').val() != '') {
                console.log(parentDiv.find('.quantity').val());
                var total = parentDiv.find('.quantity').val() * this.value;

            } else
                var total = '';
        } else
            var total = '';

        var tax = parentDiv.find('.tax_rate').val();
        if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
            tax_amount = total * parseInt(tax) / 100;
            parentDiv.find('.tax_amount').val(tax_amount);
            total = total + tax_amount;
        } else
            parentDiv.find('.tax_amount').val('0');

        parentDiv.find('.total').val(total);

        //console.log(total);

        //alert('done');
        calcTotal();
    });

    $(document).on('change', '.tax_rate', function() {
        var parentDiv = $(this).parent().parent();

        if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
            var total = parentDiv.find('.price').val() * parseInt(parentDiv.find('.quantity').val());
        } else
            var total = '';

        var tax = $(this).val();
        if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
            tax_amount = total * parseInt(tax) / 100;
            parentDiv.find('.tax_amount').val(tax_amount);
            total = total + tax_amount;
        } else
            parentDiv.find('.tax_amount').val('0');

        parentDiv.find('.total').val(total);
        calcTotal();
    });

    /*$('#discount').on('change', function() {
        if(isNumeric(this.value) && this.value != '')
        {
            if(isNumeric($('#sub-total').val()) && $('#sub-total').val() != '')
                parentDiv.find('.total').val($('#sub-total').val() - this.value).trigger('change');
        }
    });

    $("#sub-total").bind("change", function() {
        if(isNumeric($('#discount').val()) && $('#discount').val() != '')
            parentDiv.find('.total').val($('#sub-total').val() - $('#discount').val());
        else
            parentDiv.find('.total').val($('#sub-total').val());
    });*/

    $(".addMore").on("click", function() {

        $(".multipleDiv").after($('#orderFields #more-tr').html());

    });

    $("#addCustomMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").after($('#CustomOrderFields #more-custom-tr').html());
    });

    $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();
        calcTotal();
    });

    $(document).on('change', '#vat_type', function() {
        calcTotal();
    });

    $(document).on('change', '#service_type', function() {
        calcTotal();
    });

    $(document).on('change', '#discount', function() {
        calcTotal();
    });

    function calcTotal() {
        //alert('hi');
        var subTotal = 0;
        var service_total = 0;
        var service_charge = 0;
        var taxableAmount = 0;

        //var tax = parseInt($('#tax').val().replace('%', ''));
        var total = 0;
        var tax_amount = 0;
        var taxableTax = 0;

        $(".total").each(function(index) {
            if (isNumeric($(this).val()))
                subTotal = parseInt(subTotal) + parseInt($(this).val());
        });

        $(".tax_amount").each(function(index) {
            if (isNumeric($(this).val()))
                tax_amount = parseInt(tax_amount) + parseInt($(this).val());
        });



        $('#sub-total').html(subTotal);
        $('#subtotal').val(subTotal);

        $('#taxable-amount').html(subTotal);
        $('#taxableamount').val(subTotal);

        var service_type = $('#service_type').val();
        var discount_amount = $('#discount_amount').val();
        var vat_type = $('#vat_type').val();
        let discounttype = $('#discount').val();


        if (discounttype == 'percentage') {

            if (isNumeric(discount_amount) && discount_amount != 0) {
                amount_after_discount = subTotal - (parseInt(discount_amount) / 100 * subTotal);
            } else {
                amount_after_discount = subTotal;
            }

            if (service_type == 'no' || service_type == '') {
                service_total = amount_after_discount;
                service_charge = 0;
                taxableAmount = service_total;
            } else {
                service_total = amount_after_discount + parseInt(10 / 100 * amount_after_discount);
                service_charge = parseInt(10 / 100 * amount_after_discount);
                taxableAmount = service_total;
            }

            total = taxableAmount + parseInt(13 / 100 * taxableAmount);
            taxableTax = parseInt(13 / 100 * taxableAmount);

        } else {

            if (isNumeric(discount_amount) && discount_amount != 0) {
                amount_after_discount = subTotal - (parseInt(discount_amount));
            } else {
                amount_after_discount = subTotal;
            }

            if (service_type == 'no' || service_type == '') {
                service_total = amount_after_discount;
                service_charge = 0;
                taxableAmount = service_total;
            } else {
                service_total = amount_after_discount + parseInt(10 / 100 * amount_after_discount);
                service_charge = parseInt(10 / 100 * amount_after_discount);
                taxableAmount = service_total;
            }

            total = taxableAmount + parseInt(13 / 100 * taxableAmount);
            taxableTax = parseInt(13 / 100 * taxableAmount);

        }

        $('#service_charge').val(service_charge);
        $('#service-charge').html(service_charge);

        $('#amount_with_service').val(service_total);
        $('#amount-with-service').html(service_total);

        $('#taxableamount').val(taxableAmount);
        $('#taxable-amount').html(taxableAmount);

        $('#total_tax_amount').val(tax_amount);

        $('#taxabletax').val(taxableTax);
        $('#taxable-tax').html(taxableTax);


        $('#total').html(total);
        $('#total_').val(total);
    }

    $(document).on('keyup', '#discount_amount', function() {

        calcTotal();

    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
        $('.pos_customer_id').select2();
    });

    $('#discount').on('change', function() {

        $('#discount_amount').val('');
        type = $(this).val();

        if (type == 'percentage') {
            $('.discounttype').html('(%)');
        } else {
            $('.discounttype').html('(Amount)');
        }
        calcTotal();

    });

    $('#discount_amount').keyup(function() {
        type = $('#discount').val();
        if (type == 'percentage') {

            $('.discounttype').html('(%)');

            if (this.value > 99) {
                this.value = '99';
            } else if (this.value < 0) {
                this.value = '0';
            }

        } else {

            $('.discounttype').html('(Amount)');
            let max = Number($('#subtotal').val());
            if (this.value > max) {
                this.value = max;
            } else if (this.value < 0) {
                this.value = '0';
            }

        }
    });

</script>

<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>

<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>

<style type="text/css">
    .intl-tel-input {
        width: 100%;
    }

    .intl-tel-input .iti-flag .arrow {
        border: none;
    }

</style>

<script type="text/javascript">
    function myfun(value) {
        $(value).autocomplete({
            source: "/admin/getProducts"
            , minLength: 1
        });
    }

    function price(value) {

        var parentDiv = $(value).parent().parent();
        var el = $(value);
        if (value.value != 'NULL') {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST"
                , contentType: "application/json; charset=utf-8"
                , url: "/admin/products/GetProductDetailAjax/" + value.value + '?_token=' + _token
                , success: function(result) {
                    var obj = jQuery.parseJSON(result.data);
                    if (obj == null) {
                        el.val('');
                        return;
                    }
                    parentDiv.find('.price').val(obj.price);

                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                        var total = parentDiv.find('.quantity').val() * obj.price;
                    } else {
                        var total = obj.price;
                    }

                    var tax = parentDiv.find('.tax_rate').val();
                    if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                        tax_amount = total * parseInt(tax) / 100;
                        parentDiv.find('.tax_amount').val(tax_amount);
                        total = total + tax_amount;
                    } else
                        parentDiv.find('.tax_amount').val('0');

                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
            });
        } else {
            parentDiv.find('.price').val('');
            parentDiv.find('.total').val('');
            parentDiv.find('.tax_amount').val('');
            calcTotal();
        }
    }

</script>

<script>
    $(function() {
        $('#menu_id').on('change', function() {
            if ($(this).val() != '') {
                $.ajax({
                    url: "/admin/users/ajax/getProductcategory"
                    , data: {
                        menu_id: $(this).val()
                    }
                    , dataType: "json"
                    , success: function(data) {
                        var result = data.data;
                        $('#productcategory_id').html(result);
                    }
                });
            }
        });
    });

</script>

<script>
    $(function() {
        $('#productcategory_id').on('change', function() {
            if ($(this).val() != '') {
                $.ajax({
                    url: "/admin/users/ajax/getProduct"
                    , data: {
                        category_id: $(this).val()
                    }
                    , dataType: "json"
                    , success: function(data) {
                        var result = data.data;
                        $('#produt_list').html(result);
                    }
                });
            }
        });
    });

    function AddMore(val) {

        let name = $(val).data('name');
        let price = $(val).data('price');
        let app_currency = `{{env('APP_CURRENCY')}}`;

        let dom = '<tr><td><input type="text" name="product_id[]" class="form-control product_id" value="' + name + '" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)"></td><td> <div class="input-group"><span class="input-group-addon">' + app_currency + '</span> <input type="text" class="form-control price" name="price[]" placeholder="Price" value="' + price + '" required="required"></div></td><td><input type="number" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1" required="required"></td><td><div class="input-group"><span class="input-group-addon">' + app_currency + '</span> <input type="number" class="form-control total" name="total[]" placeholder="Total" value="' + price + '" readonly="readonly" style="float:left; width:80%;"></div><a href="javascript::void(1);" style="width: 10%;"><i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i></a></td></tr>';

        $(".multipleDiv").after(dom);
        calcTotal();
    }


    $('#reservation_id').change(function() {

        let reservation_val = $(this).val();

        if (reservation_val != '') {
            $('.pos_customer_id').select2('destroy');
            $('#pos_customer_id').val(``);
            $('.pos_customer_id').select2();
        } else {
            $('.pos_customer_id').select2('destroy');
            $('#pos_customer_id').val(`{{env('CASH_GUEST_CLIENT_ID')}}`);
            $('.pos_customer_id').select2();
        }

        $.get('/admin/getpanno/fromreservation/' + reservation_val, function(data, status) {
            $('#pan_no').val(data.vat_no);
            $('#company_name').val(data.company_name);
            $('#guest_name').val(data.guest_name);
        });

    });


    $('#pos_customer_id').change(function() {
        var pos_customer_id = $(this).val();

        if (pos_customer_id != '') {
            $('.customer_id').select2('destroy');
            $('#reservation_id').val('');
            $('.customer_id').select2();
        }
    });

    function openwindowposcustomer() {
        var win = window.open('/admin/clients/modals/create?relation_type=poscustomer', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
    }

    function HandlePopupResult(result) {
        if (result) {
            let clients = result.clients;
            let types = $(`input[name=source]:checked`).val();
            if (types == 'lead') {
                lead_clients = clients;
            } else {
                customer_clients = clients;
            }
            var option = '';
            for (let c of clients) {
                option = option + `<option value='${c.id}'>${c.name}</option>`;
            }
            $('#pos_customer_id').html(option);
            setTimeout(function() {
                $('#pos_customer_id').val(result.lastcreated);
                $("#ajax_status").after("<span style='color:green;' id='status_update'>client sucessfully created</span>");
                $('#status_update').delay(3000).fadeOut('slow');
            }, 500);
        } else {
            $("#ajax_status").after("<span style='color:red;' id='status_update'>failed to create clients</span>");
            $('#status_update').delay(3000).fadeOut('slow');
        }
    }

</script>










@endsection


@php $plist = isset($plist) ? $plist : null  @endphp
<div class="row settlementPart">

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label col-sm-4">Settle In</label>
            <div class="col-md-8">

                {!! Form::select('paid_by[]',['cash'=>'Cash','city-ledger'=>'Credit / City ledger',
                    'check'=>'Cheque',
                    'travel-agent'=>'Travel Agent',
                    'complementry'=>'Complementary',
                    'staff'=>'Staff',
                    'credit-cards'=>'Credit Cards',
                    'e-sewa'=>'E-Sewa',
                    'mnp'=>'Marketing & Promotion',
                    ],$plist->paid_by ??null ,['class'=>'form-control input-sm colorselector','id'=>'colorselector','target-div'=>$divid,'required'=>'required','placeholder'=>'Select']
                    ) !!}

            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label col-sm-4">Amount</label>
            <div class="input-group ">
                <input type="text" name="amount[]" placeholder="Amount"
                value="{{ $plist ? $plist->amount : $payment_remain }}"

                class="form-control input-sm settle_amount" required="required" readonly>
                <div class="input-group-addon">
                    <a href="#"><i class="fa fa-credit-card"></i></a>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="cash" class="colors" style="display:none">
    <div class="row">

    </div>
</div>


<div id="check" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Bank Name</label>
                <div class="input-group ">
                    <input type="text" name="bank_name[]" placeholder="Bank Name" id="bank_name" value="{{ $plist->bank_name ?? null}}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Cheque No</label>
                <div class="input-group ">
                    <input type="text" name="cheque_no[]" placeholder="Cheque No" id="cheque_no" value="{{ $plist->cheque_no ?? null }}" class="form-control input-sm ">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Cheque Date</label>
                <div class="input-group ">
                    <input type="text" name="cheque_date[]" placeholder="Cheque Date"  value="{{  $plist ? $plist->cheque_date : date('Y-m-d')}}" class="form-control input-sm datepicker">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="travel-agent" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Travel Agent</label>
                <div class="input-group ">
                    <input type="text" name="travel_agent_ledger[]" placeholder="Travel Agent" id="travel_agent" value="{{ ($plist && $plist->paid_by == 'travel-agent')? $plist->debitLedger():''  }}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div id="city-ledger" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">
                Credit Customer <a target="_blank" href="/admin/poscustomer"> <i class="fa fa-eye"></i> </a>
            </label>
                <div class="input-group ">
                    <input type="text" name="city_ledger[]" placeholder="Type and Select customer name" id="city_ledger" value="{{$plist && $plist->paid_by=='city-ledger'? $plist->debitLedger():''  }}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div id="complementry" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-5">Staff/NC/Board</label>
                <div class="input-group ">
                    <input type="text" name="staff_nc_board[]" placeholder="Staff/NC/Board" id="staff_nc_board" value="{{ $plist && $plist->staff_nc_board }}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-5">Sub Type</label>
                <div class="input-group ">
                    <input type="text" name="sub_type" placeholder="Sub Type" id="sub_type"  value="{{ $plist->sub_type ?? null}}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-5">Staff</label>
                <div class="input-group ">
                    <input type="text" name="complementry_staff[]" placeholder="Staff" id="complementry_staff" value="{{ $plist->complementry_staff ??null }}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-5">Remarks</label>
                <div class="input-group ">
                    <input type="text" name="remarks[]" placeholder="Remarks" id="remarks" value="{{ $plist->remarks ?? null}}"  class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div id="staff" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Staff Name</label>
                <div class="input-group ">
                    <input type="text" name="staff_name[]" placeholder="Staff Name" id="staff_name" value="{{ $plist->staff_name ?? null }}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div id="credit-cards" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Card Type</label>
                <div class="input-group ">
                    <select class="form-control input-sm" name="cc_type[]">
                        <option value="">Select</option>
                        <option value="visa" @if($plist && $plist->cc_type == 'visa') selected="" @endif>Visa</option>
                        <option value="master"  @if($plist && $plist->cc_type == 'master') selected="" @endif>Master</option>
                    </select>
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-5">Name On Card</label>
                <div class="input-group ">
                    <input type="text" name="cc_holder[]" placeholder="Name on card" id="name_on_card" value="{{ $plist->cc_holder ?? null}}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Card No</label>
                <div class="input-group ">
                    <input type="text" name="cc_no[]" placeholder="Card No" id="card_no" value="{{ $plist->cc_no ?? null}}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div id="e-sewa" class="colors" style="display:none">

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">E-Sewa</label>
                <div class="input-group ">
                    <input type="text" name="e_sewa_id[]" placeholder="E-Sewa Id" id="e_sewa_id" value="{{ $plist->e_sewa_id ?? null}}" class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="room" class="colors" style="display:none">

    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-sm-4">Room No</label>
                <div class="input-group ">
                    <input type="text" name="room_no[]" placeholder="Room No" id="room_no" value="{{ $plist->room_no ?? null }}"class="form-control input-sm">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa fa-credit-card"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div id="room_detail">



        </div>



    </div>

</div>


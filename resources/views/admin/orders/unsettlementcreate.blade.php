@extends('layouts.master')

@section('head_extra')

@include('partials._head_extra_select2_css')

@endsection
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {!! $page_title !!}

        <small>{!! $page_description !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body">

                <form action="/admin/unsettlement/orders/{{$sale_id}}" method="post">

                    {{ csrf_field() }}

                    <div class="content col-md-12">

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Settle In</label>
                                    <div class="col-md-8">
                                        <select name="paid_by" class="form-control" id="colorselector" required>
                                            <option value="">Select</option>
                                            <option value="cash" @if($settlement_detail->paid_by == "cash") selected @endif>Cash</option>
                                            <option value="check" @if($settlement_detail->paid_by == "check") selected @endif>Check</option>
                                            <option value="travel-agent" @if($settlement_detail->paid_by == "travel-agent") selected @endif>Travel Agent</option>
                                            <option value="city-ledger" @if($settlement_detail->paid_by == "city-ledger") selected @endif>City ledger</option>
                                            <option value="complementry" @if($settlement_detail->paid_by == "complementry") selected @endif>Complementry</option>
                                            <option value="staff" @if($settlement_detail->paid_by == "staff") selected @endif>Staff</option>
                                            <option value="credit-cards" @if($settlement_detail->paid_by == "credit-cards") selected @endif>Credit Cards</option>
                                            <option value="room" @if($settlement_detail->paid_by == "room") selected @endif>Room</option>
                                            <option value="e-sewa" @if($settlement_detail->paid_by == "e-sewa") selected @endif>E-Sewa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Amount</label>
                                    <div class="input-group ">
                                        <input type="text" name="amount" placeholder="Amount" id="" value="-{{ $payment_remain }}" class="form-control" required="required" readonly>
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-credit-card"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="cash" class="colors" style="display:none">
                            <div class="row">

                            </div>
                        </div>


                        <div id="check" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Bank Name</label>
                                        <div class="input-group ">
                                            <input type="text" name="bank_name" placeholder="Bank Name" id="bank_name" value="{{$settlement_detail->bank_name}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Cheque No</label>
                                        <div class="input-group ">
                                            <input type="text" name="cheque_no" placeholder="Cheque No" id="cheque_no" value="{{$settlement_detail->cheque_no}}" class="form-control ">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Cheque Date</label>
                                        <div class="input-group ">
                                            <input type="text" name="cheque_date" placeholder="Cheque Date" id="" value="{{$settlement_detail->cheque_date}}" class="form-control datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="travel-agent" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Travel Agent</label>
                                        <div class="input-group ">
                                            <input type="text" name="travel_agent_ledger" placeholder="Travel Agent" id="travel_agent" value="{{$settlement_detail->travel_agent_ledger}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="city-ledger" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">City Ledger</label>
                                        <div class="input-group ">
                                            <input type="text" name="city_ledger" placeholder="City Ledger" id="city_ledger" value="{{$settlement_detail->city_ledger}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="complementry" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Staff/NC/Board</label>
                                        <div class="input-group ">
                                            <input type="text" name="staff_nc_board" placeholder="Staff/NC/Board" id="staff_nc_board" value="{{$settlement_detail->staff_nc_board}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Sub Type</label>
                                        <div class="input-group ">
                                            <input type="text" name="sub_type" placeholder="Sub Type" id="sub_type" value="{{$settlement_detail->sub_type}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Staff</label>
                                        <div class="input-group ">
                                            <input type="text" name="complementry_staff" placeholder="Staff" id="complementry_staff" value="{{$settlement_detail->complementry_staff}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Remarks</label>
                                        <div class="input-group ">
                                            <input type="text" name="remarks" placeholder="Remarks" id="remarks" value="{{$settlement_detail->remarks}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="staff" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Staff Name</label>
                                        <div class="input-group ">
                                            <input type="text" name="staff_name" placeholder="Staff Name" id="staff_name" value="{{$settlement_detail->staff_name}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="credit-cards" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Card Type</label>
                                        <div class="input-group ">
                                            <select class="form-control" name="cc_type">
                                                <option value="">Select</option>
                                                <option value="visa" @if($settlement_detail->cc_type == 'visa') selected @endif>Visa</option>
                                                <option value="master" @if($settlement_detail->cc_type == 'master') selected @endif>Master</option>
                                            </select>
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Name On Card</label>
                                        <div class="input-group ">
                                            <input type="text" name="cc_holder" placeholder="Name on card" id="name_on_card" value="{{$settlement_detail->cc_holder}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Card No</label>
                                        <div class="input-group ">
                                            <input type="text" name="cc_no" placeholder="Card No" id="card_no" value="{{$settlement_detail->cc_no}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Expiry Date</label>
                                        <div class="input-group ">
                                            <input type="text" name="expiry_date" placeholder="Expiry Date" id="expiry_date" value="{{$settlement_detail->expiry_date}}" class="form-control datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="e-sewa" class="colors" style="display:none">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">E-Sewa</label>
                                        <div class="input-group ">
                                            <input type="text" name="e_sewa_id" placeholder="E-Sewa Id" id="e_sewa_id" value="{{$settlement_detail->e_sewa_id}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="room" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Room No</label>
                                        <div class="input-group ">
                                            <input type="text" name="room_no" placeholder="Room No" id="room_no" value="{{$settlement_detail->room_no}}" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="room_detail">


                                </div>

                            </div>

                        </div>



                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Date</label>
                                    <div class="input-group ">
                                        <input type="text" name="date" id="target_date" value="{{\Carbon\Carbon::now()->toDateString()}}" placeholder="Date" class="form-control datepicker">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="far fa-calendar-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Ref #</label>
                                    <div class="input-group ">
                                        <input type="text" name="reference_no" placeholder="Reference No" id="" value="{{$settlement_detail->reference_no}}" class="form-control" required>
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-building"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <input type="hidden" name="sale_id" value="{{$sale_id}}">



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Attachment</label>
                                    <div class="input-group ">
                                        <input type="file" name="attachment">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <label for="inputEmail3" class="control-label">
                                    Note
                                </label>

                                <textarea class="form-control" name="note" id="description" placeholder="Write Note">{!! \Request::old('note') !!}</textarea>
                            </div>
                        </div>


                    </div><!-- /.content -->

                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="Submit" value="Unsettle Now" class="btn btn-primary">
                            <a href="/admin/hotel/orders/outlet/{{\App\Models\Orders::find($sale_id)->outlet_id}}/unsettlement" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.col -->

</div><!-- /.row -->

@endsection

@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>




<script type="text/javascript">
    $('#colorselector').change(function() {
        $('.colors').hide();
        $('#' + $(this).val()).show();

    });

    $('#colorselector').trigger('change');

</script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#travel_agent,#city_ledger,#staff_name").autocomplete({

            source: function(request, response) {
                $.getJSON(`/admin/getLedgers?types=${$('#colorselector').val()}&term=${request.term}`, response);
            }
            , minLength: 1,

            //     source: `/admin/getLedgers`,      
            //     minLength: 1,
            //     extraParams: {  //to pass extra parameter in ajax file.
            //           "types": $('#colorselector').val(),
            //     },
        });
    });

</script>

<script type="text/javascript">
    $('#room_no').keyup(function() {

        $("#room_no").autocomplete({
            source: "/admin/getRooms"
            , minLength: 1
        });

    });

    $('#room_no').change(function() {

        if ($(this).val() != '') {
            $.ajax({
                url: "/admin/users/ajax/getresv"
                , data: {
                    room_num: $(this).val()
                }
                , dataType: "json"
                , success: function(data) {

                    var result = data.data;
                    $('#room_detail').html(result);

                }
            });
        }

    });

</script>





@endsection

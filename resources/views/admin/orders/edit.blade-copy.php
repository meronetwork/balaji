@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')

<style>
    .panel .mce-panel {
        border-left-color: #fff;
        border-right-color: #fff;
    }

    .panel .mce-toolbar,
    .panel .mce-statusbar {
        padding-left: 20px;
    }

    .panel .mce-edit-area,
    .panel .mce-edit-area iframe,
    .panel .mce-edit-area iframe html {
        padding: 0 10px;
        min-height: 350px;
    }

    .mce-content-body {
        color: #555;
        font-size: 14px;
    }

    .panel.is-fullscreen .mce-statusbar {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 200000;
    }

    .panel.is-fullscreen .mce-tinymce {
        height: 100%;
    }

    .panel.is-fullscreen .mce-edit-area,
    .panel.is-fullscreen .mce-edit-area iframe,
    .panel.is-fullscreen .mce-edit-area iframe html {
        height: 100%;
        position: absolute;
        width: 99%;
        overflow-y: scroll;
        overflow-x: hidden;
        min-height: 100%;
    }

</style>


@endsection

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<div class='row'>
    <div class='col-md-12'>
        <div class="box-body">


            <div id="orderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-tr">
                        <tr>
                            <td>
                                <input type="text" name="product_id_new[]" class="form-control product_id" value="" id="product_id" onkeyup="myfun(this)" onchange="price(this)" autocomplete="off">
                            </td>

                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="text" class="form-control price" name="price_new[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required" autocomplete="off">
                                </div>
                            </td>

                            <td>
                                <input type="number" class="form-control quantity" name="quantity_new[]" placeholder="Quantity" min="1" value="1" required="required">
                            </td>

                            <!--<td>
                                    <select required name="tax_new[]" class="form-control tax_rate">
                                        @foreach(config('tax.taxes') as $dk => $dv)
                                        <option value="{{ $dk }}" @if(isset($orderDetail->total) && $orderDetail->tax_amount == $dk) selected="selected" @endif>{{ $dv }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control tax_amount" name="tax_amount_new[]" @if(isset($orderDetail->tax_amount)) value="{{ $orderDetail->tax_amount }}" @else value="0" @endif readonly="readonly">
                                </td> -->

                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="number" class="form-control total" name="total_new[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                </div>
                                <a href="javascript::void(1);" style="width: 10%;">
                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                </a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="CustomOrderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-custom-tr">
                        <tr>
                            <td>
                                <input type="text" class="form-control product" name="custom_items_name_new[]" value="" placeholder="Product" autocomplete="off">
                            </td>

                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="text" class="form-control price" name="custom_items_price_new[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required" autocomplete="off">
                                </div>
                            </td>

                            <td>
                                <input type="number" class="form-control quantity" name="custom_items_qty_new[]" placeholder="Quantity" min="1" value="1" required="required">
                            </td>

                            <!--  <td>
                                    <select required name="tax_id_custom_new[]" class="form-control tax_rate">
                                        @foreach(config('tax.taxes') as $dk => $dv)
                                        <option value="{{ $dk }}" @if(isset($orderDetail->total) && $orderDetail->tax_amount == $dk) selected="selected" @endif>{{ $dv }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control tax_amount" name="custom_tax_amount_new[]" @if(isset($orderDetail->tax_amount)) value="{{ $orderDetail->tax_amount }}" @else value="0" @endif readonly="readonly">
                                </td> -->

                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                    <input type="number" class="form-control total" name="custom_total_new[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                </div>
                                <a href="javascript::void(1);" style="width: 10%;">
                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    {!! Form::model( $order, ['route' => ['admin.orders.update', $order->id], 'method' => 'PUT'] ) !!}

                    <div class="panel-body">

                        <div class="col-md-12">
                            <div class="col-md-3 form-group" style="">
                                <label for="user_id">Select Reservation Customer</label>
                                <select name="reservation_id" class="form-control customer_id" id="reservation_id">
                                    <option class="form-control input input-lg" value="">Select Resv Customer</option>
                                    @if(isset($reservation))
                                    @foreach($reservation as $key => $uk)
                                    <option value="{{ $uk->id }}" @if($order && $uk->id == $order->reservation_id){{ 'selected="selected"' }}@endif>{{ '('.$uk->id.') '.$uk->guest_name.' ('.$uk->room_num.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="col-md-3 form-group" style="">
                                <label for="user_id">Select POS Customer</label>
                                <select name="pos_customer_id" class="form-control pos_customer_id input-sm" id="pos_customer_id">
                                    <option class="form-control input input-lg" value="">Select POS Customer</option>
                                    @if(isset($clients))
                                    @foreach($clients as $key => $uk)
                                    <option value="{{ $uk->id }}" @if($uk->id == $order->pos_customer_id){{ 'selected="selected"' }}@endif>{{ '('.env('APP_CODE'). $uk->id.') '.$uk->name.'('.$uk->location.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>

                            <?php

                                       $is_fnb = \App\Models\PosOutlets::find($order->outlet_id)->fnb_outlet;

                                    ?>

                            @if($is_fnb)

                            <div class="col-md-3 form-group" style="">
                                <label for="comment">Covers</label>
                                <input type="text" name="covers" id="name" class="form-control" value="{{$order->covers}}">
                            </div>

                            <div class="col-md-3 form-group" style="">
                                <label for="comment">Table</label>
                                <input type="text" name="table" id="name" class="form-control" value="{{$order->table}}">
                            </div>

                            @endif
                        </div>

                        <div class="col-md-12">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Service Charge:</label>
                                    <select type="text" class="form-control pull-right " name="service_type" id="service_type">
                                        <option value="" @if($order->service_type == "") selected="selected"@endif>Select</option>

                                        <option value="no" @if($order->service_type == "no") selected="selected"@endif>No</option>

                                        <option value="yes" @if($order->service_type == "yes") selected="selected"@endif>Yes({{env('SERVICE_CHARGE')}}%)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Discount:</label>
                                    <select type="text" class="form-control pull-right " name="discount_note" id="discount">
                                        <option value="percentage" @if($order->discount_note == "percentage") selected="selected"@endif>Percentage</option>
                                        <option value="amount" @if($order->discount_note == "amount") selected="selected"@endif>Amount</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Menu:</label>
                                    {!! Form::select('menu_id',[''=>'Select']+$menus,null, ['class' => 'form-control input-sm customer_id', 'id'=>'menu_id']) !!}
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Product category </label>
                                    <select name="productcategory_id" id="productcategory_id" class="form-control select_box customer_id" />
                                    <option value="">Select Product Category...</option>
                                    @if($productscategory)
                                    @foreach($productscategory as $uk => $uv)
                                    <option value="{{ $uv->id }}">{{ $uv->name }}</option>
                                    @endforeach
                                    @endif
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="outlet_id" value="{{$order->outlet_id}}">
                            <input type="hidden" name="session_id" value="{{$order->session_id}}">
                            <input type="hidden" name="order_id" value="{{ $order->order_id }}">
                            <input type="hidden" name="bill_date" value="{{ $order->bill_date}}">
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label> Bill No </label>
                                    <input type="text" name="bill_no" id="name" class="form-control input-sm" value="{{$order->bill_no}}" readonly>
                                </div>
                            </div>
                            @if($is_fnb)

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Bill To:</label>
                                    {!! Form::select('bill_to',['normal'=>'Normal','bwstoguestandcompany'=>'BWS To Guest and Bill To Comapny'],$order->bill_to, ['class' => 'form-control customer_id', 'id'=>'menu_id']) !!}
                                </div>
                            </div>

                            @endif
                        </div>


                        <input type="hidden" name="order_type" value="invoice">


                        <div class="clearfix"></div>

                        <div class="col-md-12 text-right">
                            <a href="javascript::void(0)" class="btn btn-primary btn-xs" id="addMore" style="float: right;">
                                <i class="fa fa-plus"></i> <span>Add Products Item</span>
                            </a>

                        </div>

                        <style>
                            #produt_list .col-md-3 {
                                padding-left: 5px;
                                padding-right: 5px;
                            }

                            #produt_list .inner {
                                padding: 5px;
                                text-align: center;
                            }

                            #produt_list .inner h4 {
                                font-size: 14px;
                                margin: 0;
                            }

                            #produt_list .small-box {
                                margin-bottom: 10px;
                            }

                        </style>

                        <div class="col-md-6">
                            <div class="box-body">
                                <ul class="products-list product-list-in-box" id="produt_list">

                                </ul>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Products *</th>
                                        <th>Price *</th>
                                        <th>Quantity *</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($orderDetails as $odk => $odv)
                                    @if($odv->is_inventory == 1)
                                    <tr>
                                        <td>
                                            <input type="text" name="product_id[]" class="form-control product_id" required="required" id="product_id" value="{{ $odv->product->name }}" onkeyup="myfun(this)" onchange="price(this)" autocomplete="off">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                <input type="text" class="form-control price" name="price[]" placeholder="Price" value="{{ $odv->price }}" required="required" autocomplete="off">
                                            </div>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}" required="required">
                                        </td>

                                        <!--  <td>
                                                <select required name="tax[]" class="form-control tax_rate">
                                                    @foreach(config('tax.taxes') as $dk => $dv)
                                                    <option value="{{ $dk }}" @if(isset($odv->tax) && $odv->tax == $dk) selected="selected" @endif>{{ $dv }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control tax_amount" name="tax_amount[]" @if(isset($odv->tax_amount)) value="{{ $odv->tax_amount }}" @else value="0" @endif readonly="readonly">
                                            </td> -->

                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                <input type="number" class="form-control total" name="total[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly" style="float:left; width:80%;">
                                            </div>
                                            @if($odk != '0')
                                            <a href="javascript::void(1);" style="width: 10%;">
                                                <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @elseif($odv->is_inventory == 0)
                                    <tr>
                                        <td>

                                            <input type="text" class="form-control product" name="description_custom[]" value="{{ $odv->description }}" placeholder="Product" autocomplete="off">

                                        </td>

                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                <input type="text" class="form-control price" name="price_custom[]" placeholder="Fare" value="{{ $odv->price }}" required="required" autocomplete="off">
                                            </div>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control quantity" name="quantity_custom[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}" required="required">
                                        </td>


                                        <!-- <td>
                                                <select required name="tax_custom[]" class="form-control tax_rate">
                                                    @foreach(config('tax.taxes') as $dk => $dv)
                                                    <option value="{{ $dk }}" @if(isset($odv->tax) && $odv->tax == $dk) selected="selected" @endif>{{ $dv }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control tax_amount" name="tax_amount_custom[]" @if(isset($odv->tax_amount)) value="{{ $odv->tax_amount }}" @else value="0" @endif readonly="readonly">
                                            </td> -->

                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                <input type="number" class="form-control total" name="total_custom[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly" style="float:left; width:80%;">
                                            </div>
                                            @if($odk != '0')
                                            <a href="javascript::void(1);" style="width: 10%;">
                                                <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    <tr class="multipleDiv"></tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="text-align: right;">Subtotal</td>
                                        <td id="sub-total">{{ $order->subtotal }}</td>
                                        <td>&nbsp; <input type="hidden" name="subtotal" id="subtotal" value="{{ $order->subtotal }}"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">POS Discount <span class="discounttype">(%)</span></td>
                                        <td><input type="number" min="0" name="discount_percent" id="discount_amount" value="{{$order->discount_percent }}"></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Service Charge</td>
                                        <td id="service-charge">{{ $order->service_charge }}</td>
                                        <td>&nbsp; <input type="hidden" name="service_charge" id="service_charge" value="{{ $order->service_charge }}"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Amount With Service Charge</td>
                                        <td id="amount-with-service">{{ $order->amount_with_service }}</td>
                                        <td>&nbsp; <input type="hidden" name="amount_with_service" id="amount_with_service" value="{{ $order->amount_with_service }}"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Taxable Amount</td>
                                        <td id="taxable-amount">{{ $order->taxable_amount }}</td>
                                        <td>&nbsp;
                                            <input type="hidden" name="taxable_amount" id="taxableamount" value="{{ $order->taxable_amount }}"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;">Tax Amount (13%)</td>
                                        <td id="taxable-tax">{{ $order->tax_amount }}</td>
                                        <td>&nbsp; <input type="hidden" name="taxable_tax" id="taxabletax" value="{{ $order->tax_amount }}"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" style="text-align: right;"><strong>TOTAL</strong></td>
                                        <td id="total">{{ $order->total_amount }}</td>
                                        <td>
                                            <input type="hidden" name="total_tax_amount" id="total_tax_amount" value="{{ $order->tax_amount }}">
                                            <input type="hidden" name="final_total" id="total_" value="{{ $order->total_amount }}">
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-social btn-foursquare">
                            <i class="fa fa-save"></i>Update Order
                        </button>
                        <a class="btn btn-default" href="javascript:history.back()"><i class="fa fa-ban"></i> Cancel</a>
                    </div>


                    </form>
                </div>
            </div>

        </div><!-- /.box-body -->
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection

@section('body_bottom')
<!-- form submit -->
@include('partials._body_bottom_submit_bug_edit_form_js')
<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>

<script>
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $(document).on('change', '.product_id', function() {
        var parentDiv = $(this).parent().parent();
        var el = $(this);
        if (this.value != 'NULL') {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST"
                , contentType: "application/json; charset=utf-8"
                , url: "/admin/products/GetProductDetailAjax/" + this.value + '?_token=' + _token
                , success: function(result) {
                    var obj = jQuery.parseJSON(result.data);
                    if (obj == null) {
                        el.val('');
                        return;
                    }
                    parentDiv.find('.price').val(obj.price);

                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                        var total = parentDiv.find('.quantity').val() * obj.price;
                    } else {
                        var total = obj.price;
                    }

                    var tax = parentDiv.find('.tax_rate').val();
                    if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                        tax_amount = total * parseInt(tax) / 100;
                        parentDiv.find('.tax_amount').val(tax_amount);
                        total = total + tax_amount;
                    } else
                        parentDiv.find('.tax_amount').val('0');

                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
            });
        } else {
            parentDiv.find('.price').val('');
            parentDiv.find('.total').val('');
            parentDiv.find('.tax_amount').val('');
            calcTotal();
        }
    });

    $(document).on('change', '.customer_id', function() {
        if (this.value != '') {
            $(".quantity").each(function(index) {
                var parentDiv = $(this).parent().parent();
                if (isNumeric($(this).val()) && $(this).val() != '')
                    var total = $(this).val() * parentDiv.find('.price').val();
                else
                    var total = parentDiv.find('.price').val();

                var tax = parentDiv.find('.tax_rate').val();
                if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                    tax_amount = total * parseInt(tax) / 100;
                    parentDiv.find('.tax_amount').val(tax_amount);
                    total = total + tax_amount;
                } else
                    parentDiv.find('.tax_amount').val('0');

                if (isNumeric(total) && total != '') {
                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
                //console.log( index + ": " + $(this).text() );
            });
        } else {
            $('.total').val('0');
            $('.tax_amount').val('0');
            calcTotal();
        }
    });

    $(document).on('change', '.quantity', function() {
        var parentDiv = $(this).parent().parent();
        if (isNumeric(this.value) && this.value != '') {
            if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                var total = parentDiv.find('.price').val() * this.value;
            } else
                var total = '';
        } else
            var total = '';

        var tax = parentDiv.find('.tax_rate').val();
        if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
            tax_amount = total * parseInt(tax) / 100;
            parentDiv.find('.tax_amount').val(tax_amount);
            total = total + tax_amount;
        } else
            parentDiv.find('.tax_amount').val('0');

        parentDiv.find('.total').val(total);
        calcTotal();
    });

    $(document).on('change', '.price', function() {
        var parentDiv = $(this).parent().parent().parent();
        if (isNumeric(this.value) && this.value != '') {
            if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                var total = parentDiv.find('.quantity').val() * this.value;
            } else
                var total = '';
        } else
            var total = '';

        var tax = parentDiv.find('.tax_rate').val();
        if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
            tax_amount = total * parseInt(tax) / 100;
            parentDiv.find('.tax_amount').val(tax_amount);
            total = total + tax_amount;
        } else
            parentDiv.find('.tax_amount').val('0');

        parentDiv.find('.total').val(total);
        calcTotal();
    });

    $(document).on('change', '.tax_rate', function() {
        var parentDiv = $(this).parent().parent();

        if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
            var total = parentDiv.find('.price').val() * parseInt(parentDiv.find('.quantity').val());
        } else
            var total = '';

        var tax = $(this).val();
        if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
            tax_amount = total * parseInt(tax) / 100;
            parentDiv.find('.tax_amount').val(tax_amount);
            total = total + tax_amount;
        } else
            parentDiv.find('.tax_amount').val('0');

        parentDiv.find('.total').val(total);
        calcTotal();
    });

    /*$('#discount').on('change', function() {
        if(isNumeric(this.value) && this.value != '')
        {
            if(isNumeric($('#sub-total').val()) && $('#sub-total').val() != '')
                parentDiv.find('.total').val($('#sub-total').val() - this.value).trigger('change');
        }
    });

    $("#sub-total").bind("change", function() {
        if(isNumeric($('#discount').val()) && $('#discount').val() != '')
            parentDiv.find('.total').val($('#sub-total').val() - $('#discount').val());
        else
            parentDiv.find('.total').val($('#sub-total').val());
    });*/

    $("#addMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").after($('#orderFields #more-tr').html());
    });
    $("#addCustomMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").after($('#CustomOrderFields #more-custom-tr').html());
    });

    $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();
        calcTotal();
    });

    $(document).on('change', '#vat_type', function() {
        calcTotal();
    });

    $(document).on('change', '#service_type', function() {
        calcTotal();
    });

    $(document).on('change', '#discount', function() {

        calcTotal();

    });



    function calcTotal() {
        //alert('hi');
        var subTotal = 0;
        var service_total = 0;
        var taxableAmount = 0;
        var service_charge = 0;

        //var tax = parseInt($('#tax').val().replace('%', ''));
        var total = 0;
        var tax_amount = 0;
        var taxableTax = 0;

        $(".total").each(function(index) {
            if (isNumeric($(this).val()))
                subTotal = parseInt(subTotal) + parseInt($(this).val());
        });

        $(".tax_amount").each(function(index) {
            if (isNumeric($(this).val()))
                tax_amount = parseInt(tax_amount) + parseInt($(this).val());
        });



        $('#sub-total').html(subTotal);
        $('#subtotal').val(subTotal);

        $('#taxable-amount').html(subTotal);
        $('#taxableamount').val(subTotal);

        var service_type = $('#service_type').val();
        var discount_amount = $('#discount_amount').val();
        var vat_type = $('#vat_type').val();
        let discounttype = $('#discount').val();


        if (discounttype == 'percentage') {

            if (isNumeric(discount_amount) && discount_amount != 0) {
                amount_after_discount = subTotal - (parseInt(discount_amount) / 100 * subTotal);
            } else {
                amount_after_discount = subTotal;
            }

            if (service_type == 'no' || service_type == '') {
                service_total = amount_after_discount;
                service_charge = 0;
                taxableAmount = service_total;
            } else {

                service_total = amount_after_discount + parseInt(10 / 100 * amount_after_discount);
                service_charge = parseInt(10 / 100 * amount_after_discount);
                taxableAmount = service_total;

            }

            total = taxableAmount + parseInt(13 / 100 * taxableAmount);
            taxableTax = parseInt(13 / 100 * taxableAmount);

        } else {

            if (isNumeric(discount_amount) && discount_amount != 0) {
                amount_after_discount = subTotal - (parseInt(discount_amount));
            } else {
                amount_after_discount = subTotal;
            }

            if (service_type == 'no' || service_type == '') {
                service_total = amount_after_discount;
                service_charge = 0;
                taxableAmount = service_total;
            } else {
                service_total = amount_after_discount + parseInt(10 / 100 * amount_after_discount);
                service_charge = parseInt(10 / 100 * amount_after_discount);
                taxableAmount = service_total;
            }

            total = taxableAmount + parseInt(13 / 100 * taxableAmount);
            taxableTax = parseInt(13 / 100 * taxableAmount);

        }

        $('#service_charge').val(service_charge);
        $('#service-charge').html(service_charge);

        $('#amount_with_service').val(service_total);
        $('#amount-with-service').html(service_total);

        $('#taxableamount').val(taxableAmount);
        $('#taxable-amount').html(taxableAmount);

        $('#total_tax_amount').val(tax_amount);

        $('#taxabletax').val(taxableTax);
        $('#taxable-tax').html(taxableTax);

        $('#total').html(total);
        $('#total_').val(total);

    }

    $(document).on('keyup', '#discount_amount', function() {

        calcTotal();

    });

</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('.customer_id').select2();
        $('.pos_customer_id').select2();

        let dtype = $('#discount').val();

        if (dtype == 'percentage') {
            $('.discounttype').html('(%)');
        } else {

            $('.discounttype').html('(Amount)');
        }

    });

    $('#discount').on('change', function() {
        $('#discount_amount').val('');
        type = $(this).val();

        if (type == 'percentage') {
            $('.discounttype').html('(%)');
        } else {

            $('.discounttype').html('(Amount)');
        }
        calcTotal();

    });

    $('#discount_amount').keyup(function() {
        type = $('#discount').val();
        if (type == 'percentage') {

            $('.discounttype').html('(%)');

            if (this.value > 99) {
                this.value = '99';
            } else if (this.value < 0) {
                this.value = '0';
            }

        } else {

            $('.discounttype').html('(Amount)');
            let max = Number($('#subtotal').val());
            if (this.value > max) {
                this.value = max;
            } else if (this.value < 0) {
                this.value = '0';
            }

        }
    });

</script>


<script type="text/javascript">
    $(function() {
        $('.datepicker').datepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
            , changeMonth: true
            , changeYear: true
            , yearRange: "-2:+5"
        });

    });

</script>

<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>

<style type="text/css">
    .intl-tel-input {
        width: 100%;
    }

    .intl-tel-input .iti-flag .arrow {
        border: none;
    }

</style>

<script type="text/javascript">
    function myfun(value) {
        $(value).autocomplete({
            source: "/admin/getProducts"
            , minLength: 1
        });
    }

    function price(value) {

        console.log(value.value);

        var parentDiv = $(value).parent().parent();
        var el = $(value);
        if (value.value != 'NULL') {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST"
                , contentType: "application/json; charset=utf-8"
                , url: "/admin/products/GetProductDetailAjax/" + value.value + '?_token=' + _token
                , success: function(result) {
                    var obj = jQuery.parseJSON(result.data);
                    if (obj == null) {
                        el.val('');
                        return;
                    }
                    parentDiv.find('.price').val(obj.price);

                    if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                        var total = parentDiv.find('.quantity').val() * obj.price;
                    } else {
                        var total = obj.price;
                    }

                    var tax = parentDiv.find('.tax_rate').val();
                    if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                        tax_amount = total * parseInt(tax) / 100;
                        parentDiv.find('.tax_amount').val(tax_amount);
                        total = total + tax_amount;
                    } else
                        parentDiv.find('.tax_amount').val('0');

                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
            });
        } else {
            parentDiv.find('.price').val('');
            parentDiv.find('.total').val('');
            parentDiv.find('.tax_amount').val('');
            calcTotal();
        }
    }

</script>

<script>
    $(function() {
        $('#menu_id').on('change', function() {
            if ($(this).val() != '') {
                $.ajax({
                    url: "/admin/users/ajax/getProductcategory"
                    , data: {
                        menu_id: $(this).val()
                    }
                    , dataType: "json"
                    , success: function(data) {
                        var result = data.data;
                        $('#productcategory_id').html(result);
                    }
                });
            }
        });
    });

</script>

<script>
    $(function() {
        $('#productcategory_id').on('change', function() {
            if ($(this).val() != '') {
                $.ajax({
                    url: "/admin/users/ajax/getProduct"
                    , data: {
                        category_id: $(this).val()
                    }
                    , dataType: "json"
                    , success: function(data) {
                        var result = data.data;
                        $('#produt_list').html(result);
                    }
                });
            }
        });
    });

    function AddMore(val) {

        let name = $(val).data('name');
        let price = $(val).data('price');
        let app_currency = `{{env('APP_CURRENCY')}}`;

        let dom = '<tr><td><input type="text" name="product_id[]" class="form-control product_id" value="' + name + '" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)"></td><td> <div class="input-group"><span class="input-group-addon">' + app_currency + '</span> <input type="text" class="form-control price" name="price[]" placeholder="Price" value="' + price + '" required="required"></div></td><td><input type="number" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1" required="required"></td><td><div class="input-group"><span class="input-group-addon">' + app_currency + '</span> <input type="number" class="form-control total" name="total[]" placeholder="Total" value="' + price + '" readonly="readonly" style="float:left; width:80%;"></div><a href="javascript::void(1);" style="width: 10%;"><i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i></a></td></tr>';

        $(".multipleDiv").after(dom);
        calcTotal();
    }


    $('#reservation_id').change(function() {

        let reservation_val = $(this).val();
        $.get('/admin/getpanno/fromreservation/' + reservation_val, function(data, status) {
            $('#pan_no').val(data.vat_no);
            $('#company_name').val(data.company_name);
        });

    });

</script>

@endsection

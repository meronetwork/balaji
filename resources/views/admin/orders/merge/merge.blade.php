<div class="modal-content">
  <div class="modal-header bg-primary">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">
      Merge Table {{ $orderproxy->restauranttable->table_number  }}
    </h4>
  </div>
  <form method="POST" action="{{ route('admin.orders.merge_order',$orderproxy->id) }}" onsubmit="return confirm('Are you sure you want to merge')">
    @csrf
  <div class="modal-body">
    <table class="table table-striped table-hover" style="padding: 0;">
      <thead>
        <tr>
          <th>Table</th>
          <th>Choose</th>
        </tr>
      </thead>
      <tbody>
        @forelse($busytable as $key=>$value)
        <tr>
          <td> {{ $value->restauranttable->table_number  }} </td>
          <td>
            <input type="radio" name="order_id" value="{{ $value->id }}" required="" >
          </td>
        </tr>
        @empty
          <td colspan="2" class="text-center"><h3><i class='fa  fa-smile-o'></i> No Any Ordered table</h3></td>
        @endforelse
      </tbody>
    </table>
  </div>
  <div class="modal-footer">
    @if(count($busytable) > 0)
    <button type="submit" class="btn btn-primary submit_link">Merge</button>
    @endif
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</form>
</div>
<style type="text/css">
  .ui-autocomplete { z-index:2147483647; }

  #room_detail .col-md-4{

    width: 500px !important;
    display: block;
  }
</style>

<div class="modal-content">
  <div class="modal-header bg-primary">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">
     Post Order to Room
    </h4>
  </div>
  <form method="POST" action="{{ route('admin.orders.move_to_room',$proxyId) }}" onsubmit="return confirm('Are you sure you want to post to room')">
    @csrf
  <div class="modal-body">
    <div class="form-group">
        <input type="text" name="room_no" class="form-control" id='room_no' placeholder="Room Number.." >

    </div>

    <div id='room_detail'></div>
  </div>
  <div class="modal-footer">
    <br>
    <button type="submit" class="btn btn-primary submit_link" id='move_to_roomFolio' style="display: none;">Post</button>
    
  </div>
</form>
</div>

<script type="text/javascript">


  $(function(){

     $('#room_no').keyup(function() {
     
        $("#room_no").autocomplete({
            source: "/admin/getRooms"
            , minLength: 1
        });

    });
  $('#room_no').keyup(function() {

        $('#move_to_roomFolio').hide();
        $('#room_detail').html("");
        if ($(this).val() != '') {
            $.ajax({
                url: "/admin/users/ajax/getresv"
                , data: {
                    room_num: $(this).val()
                }
                , dataType: "json"
                , success: function(data) {

                    var result = data.data;

                    if(data.success == '1' && data.data ){

                    $('#room_detail').html(result);
                    $('#move_to_roomFolio').show();

                    }



             

                }
            });
        }

    });


  })
 
</script>
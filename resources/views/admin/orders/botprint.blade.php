<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="style.css">
        <title>{{ env('APP_COMPANY')}} | BOT</title>
    </head>
    <body onload="window.print();">
        <div class="ticket">
              <p class="centered">
                {{ env('APP_COMPANY') }}<br>
                {{$ord->outlet->name}}
                <br> Table: {{$table}} 
                <br>{{env('APP_ADDRESS1')}}  
                <br>{{env('APP_ADDRESS2')}}
                <br>{{date('d M yy h:i')}}
                @if($ord->reservation) 
                <br>{{ $ord->reservation->client->name }}
                <br>Vat Id: {{ $ord->reservation->client->vat }}
                @endif
              </p> 
              <p>--------------------------------</p>
            <table>
                <thead>
                    <tr>
                        <th class="description">Particular</th>
                        <th class="price">Qty</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  $n= 0;
                  ?>
                  @foreach($orderDetails as $odk => $odv)
                      @if($odv->product_type_id == 2)
                        <tr>
                            @if($odv->is_inventory == 1)
                            <td class="description">{{ $odv->product->name }}</td>
                            @elseif($odv->is_inventory == 0)
                            <td class="description">{{ $odv->description }}</td>
                            @endif
                            <td class="quantity">{{ $odv->quantity }}</td>
                        </tr>
                        @endif
                   @endforeach
                </tbody>
            </table>
            <p>---------------------------------</p>
        </div>
       
        <script src="script.js"></script>
    </body>
</html>
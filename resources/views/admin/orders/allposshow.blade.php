@extends('layouts.master')

@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title" }}
        <small>{{ $page_description ?? "Page Description" }}
        </small>
    </h1>

</section>

<?php
      $url = \Request::query();
      if($url){
        $url = \Request::getRequestUri() .'&';
      }
      else{
        $url = \Request::getRequestUri() .'?';
      }
    ?>

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">

            <div class="box-header with-border">

                Current Fiscal Year: {{\App\Models\Fiscalyear::where('current_year',1)->first()->fiscal_year}}
                <div class="col-md-12" style="margin-top:5px;">
                    <div class="filter form-inline" style="margin:0 30px 0 0;">

                        {!! Form::text('start_date', \Request::get('start_date'), ['style' => 'width:120px;', 'class' => 'form-control input-sm', 'id'=>'start_date', 'placeholder'=>'Start Date']) !!}&nbsp;&nbsp;
                        <!-- <label for="end_date" style="float:left; padding-top:7px;">End Date: </label> -->
                        {!! Form::text('end_date', \Request::get('end_date'), ['style' => 'width:120px; display:inline-block;', 'class' => 'form-control input-sm', 'id'=>'end_date', 'placeholder'=>'End Date']) !!}&nbsp;&nbsp;

                        {!! Form::select('outlet_id', ['' => 'Select Outlet'] + $outlet , \Request::get('outlet_id'), ['id'=>'filter-outlet', 'class'=>'form-control input-sm searchable', 'style'=>'width:130px; display:inline-block;']) !!}&nbsp;&nbsp;
                            {!! Form::select('fiscal_year',$allFiscalYear,$fiscal_year,['id'=>'fiscal_year_id', 'class'=>'form-control searchable input-sm', 'style'=>'width:110px; display:inline-block;'])  !!}



                        {!! Form::select('user_id', ['' => 'Select User'] + $users , \Request::get('user_id'), ['id'=>'user_id', 'class'=>'form-control searchable input-sm', 'style'=>'width:130px; display:inline-block;']) !!}&nbsp;&nbsp;
                        {!! Form::select('type', ['' => 'Select Invoice Type','abbreviated'=>'Abbreviated','non_abbreviated'=>'Non Abbreviated'] , \Request::get('type'), ['id'=>'bill_type','class'=>'form-control searchable input-sm', 'style'=>'width:130px; display:inline-block;']) !!}&nbsp;&nbsp;



                        <span class="btn btn-primary btn-sm" id="btn-submit-filter">
                            <i class="fa fa-list"></i> Filter
                        </span>

                        <span class="btn btn-danger btn-sm" id="btn-filter-clear">
                            <i class="fa fa-close"></i> Reset
                        </span>

                    </div>
                </div>

                <div class="col-md-12" style="margin-top:5px;">

                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" placeholder="सुरु  मिति " class="form-control input-sm" id="start_date_nep" name="start_date_nep" value="{{\Request::get('start_date_nep')}}" data-single='true'>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" placeholder="अन्तिम मिति" class="form-control input-sm" id="end_date_nep" name="end_date_nep" value="{{\Request::get('end_date_nep')}}" data-single='true'>
                        </div>
                    </div>

                    {!! Form::select('outlet_id', ['' => 'Select Outlet'] + $outlet , \Request::get('outlet_id'), ['id'=>'filter-outlet-nep', 'class'=>'form-control input-sm', 'style'=>'width:150px; display:inline-block;']) !!}&nbsp;&nbsp;



                    <button type="submit" class="btn btn-success btn-sm" id="btn-submit-filter-nep"> <i class="fa fa-list"></i> नतिजा </button>
                    <a href="/admin/orders/alloutlet" class="btn btn-success btn-sm"> Reset</a>
                </div>
                @if(Request::get('search'))
                <a href="{{$url}}op=print" class="btn btn-default float-right" target="_blank">Print</a>
                <a href="{{$url}}op=pdf" class="btn btn-default float-right">PDF</a>
                <a href="{{$url}}op=excel" class="btn btn-default float-right">Excel</a>
                @endif
            </div>

            <div class="box-body">

                <span id="index_lead_ajax_status"></span>

                <div class="table-responsive ">
                    <table class="table table-hover table-bordered table-striped" id="orders-table">
                        <thead>
                        <tr class="bg-info">

                            <th colspan="7" style="text-align: center; " >Bill/Invoice</th>
                            <th rowspan="2">Total Sales</th>
                            <th rowspan="2">Tax Free Sales</th>
                            <th rowspan="2">Discount</th>
                            <th colspan="2" style="text-align: center; ">
                                Taxable Sales
                            </th>
                            <th colspan="4" style="text-align: center;">Export</th>
                            <th rowspan="2">Outlet</th>
                            <th rowspan="2">Paid By</th>
                            <th rowspan="2">Ledger name</th>


                        </tr>
                        <tr class="bg-info">
                            <th>Date</th>
                            <th>Bill No</th>
                            <th>Guest</th>
                            <th>Pos Guest</th>
                            <th>Guest PAN</th>
                            <th>Product/Service Name</th>
                            <th>Product/Service Quantity</th>

                            <th>Taxable Amount</th>
                            <th>TAX</th>

                            <th>Exported Product/Service Amount</th>
                            <th>Export Country</th>
                            <th>Export Certificate No.</th>
                            <th>Export Certificate Date</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $n = 0;
                        $pos_total_amount = 0;
                        $pos_s_charge=0;
                        $pos_taxable_amount = 0;
                        $pos_tax_amount = 0;
                        $pos_discount_amount = 0;
                        $pos_non_taxable_amount = 0;

                        $allTotal = [];

                        ?>
                        @if(isset($orders) && !empty($orders))
                            @foreach($orders as $o)
                                <tr>
                                    <td>{{$o->bill_date}}
                                        <?php
                                        $temp_date = explode(" ",$o->bill_date );
                                        $temp_date1 = explode("-",$temp_date[0]);
                                        $cal = new \App\Helpers\NepaliCalendar();
                                        //nepali date
                                        $a = $temp_date1[0];
                                        $b = $temp_date1[1];
                                        $c = $temp_date1[2];
                                        $d = $cal->eng_to_nep($a,$b,$c);
                                        $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                                        ?><br>
                                        <small> {!! $nepali_date !!}</small>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.orders.show',['orderId'=>$o->id,'fiscal_year'=>$fiscal_year]) }}" data-toggle="modal"
                                           data-target="#modal_dialog" >TI-{{$o->outlet->outlet_code}}{!! $o->bill_no !!} </a>

                                        <input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                    <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>

                                    <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                    <td>@if($o->pos_customer_id){{$o->client->vat}}@elseif($o->reservation_id) {{$o->reservation->client->vat??''}} @else{{$o->folio->reservation->client->vat??''}} @endif</td>
                                    <td>Product/Service</td>
                                    <td>{{$o->total_items_quantity}}</td>

                                    <td>{{$o->total_amount}}</td>

                                    <td>{{ number_format($o->non_taxable_amount,2) }}</td>
                                    <td> {{ $o->discount_amount }}</td>

                                <!-- <td> {{ $o->outlet->name}} </td> -->


                                    <td>{!! number_format($o->taxable_amount,2) !!}</td>
                                    <td>{!! number_format($o->tax_amount,2) !!}</td>

                                    <?php
                                    $pos_total_amount   = $pos_total_amount + $o->total_amount;
                                    $pos_s_charge       = $pos_s_charge + $o->service_charge;
                                    $pos_taxable_amount = $pos_taxable_amount+$o->taxable_amount;
                                    $pos_non_taxable_amount = $pos_non_taxable_amount+$o->non_taxable_amount;
                                    $pos_tax_amount     = $pos_tax_amount+$o->tax_amount;
                                    $pos_discount_amount += $o->discount_amount;
                                    $paidBy =   ReservationHelper::paidByArr($o->payments);

                                    //$allTotal [$paidBy]  += $o->total_amount;

                                    ?>
                                    <td class="text-center">-</td>
                                    <td class="text-center">-</td>
                                    <td class="text-center">-</td>
                                    <td class="text-center">-</td>
                                    <td> {{ $o->outlet->name}} </td>
                                    <td>{!! $paidBy !!}</td>
                                    <td>{!! $o->getPayingLedger() !!}</td>


                                </tr>
                                @if($o->is_bill_active == '0')

                                    <tr class="bg-danger">
                                        <td>{{$o->bill_date}}
                                            <?php
                                            $temp_date = explode(" ",$o->bill_date );
                                            $temp_date1 = explode("-",$temp_date[0]);
                                            $cal = new \App\Helpers\NepaliCalendar();
                                            //nepali date
                                            $a = $temp_date1[0];
                                            $b = $temp_date1[1];
                                            $c = $temp_date1[2];
                                            $d = $cal->eng_to_nep($a,$b,$c);
                                            $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                                            ?><br>
                                            <small> {!! $nepali_date !!}</small>
                                        </td>
                                        <td> Ref of TI-{{$o->outlet->outlet_code}}{!! $o->bill_no !!} <a href='/admin/credit_note/orders/show/{{$o->id}}' data-toggle="modal" data-target="#modal_dialog">
                                                CN-TI-{{$o->outlet->outlet_code}}{{$o->credit_note_no}}</a>
                                            <input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                        <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>

                                        <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                        <td>@if($o->pos_customer_id){{$o->client->vat}}@elseif($o->reservation_id) {{$o->reservation->client->vat}} @else{{$o->folio->reservation->client->vat}} @endif</td>
                                        <td>Product/Service</td>
                                        <td>-{{$o->total_items_quantity}}</td>

                                        <td>-{{$o->total_amount}}</td>

                                        <td>-{{number_format($o->non_taxable_amount,2)}}</td>
                                        <td> -{{ $o->discount_amount }}
{{--                                            {{ round($o->service_charge,3) }}--}}
{{--                                            @if($o->discount_note == 'percentage')--}}
{{--                                                {{ $o->subtotal * ($o->discount_percent / 100) }}--}}
{{--                                            @else--}}
{{--                                                {{ $o->discount_percent }}--}}
{{--                                            @endif--}}
                                        </td>

                                    <!-- <td> {{ $o->outlet->name}} </td> -->


                                        <td>-{!! number_format($o->taxable_amount,2) !!}</td>
                                        <td>-{!! number_format($o->tax_amount,2) !!}</td>
                                        <?php
                                        $pos_total_amount       = $pos_total_amount - $o->total_amount;
                                        $pos_s_charge           = $pos_s_charge -$o->service_charge;
                                        $pos_taxable_amount     = $pos_taxable_amount-$o->taxable_amount;
                                        $pos_non_taxable_amount     = $pos_non_taxable_amount-$o->non_taxable_amount;
                                        $pos_tax_amount         = $pos_tax_amount-$o->tax_amount;
                                        $pos_discount_amount -= $o->discount_amount;

                                        $paidBy =   ReservationHelper::paidByArr($o->payments);
                                        //$allTotal [$paidBy]  = $allTotal [$paidBy] - $o->total_amount;
                                        ?>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td> {{ $o->outlet->name}} </td>
                                        <td>{!! $paidBy !!}</td>
                                        <td>{!! $o->getPayingLedger() !!}</td>

                                    </tr>

                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>




                        <tr>
                            <td colspan="6"></td>
                            <td>
                                Total Amount:
                            </td>
                            <td style="font-size: 16.5px"> <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_total_amount,2) }} </strong></td>
                            <td><strong>{{env('APP_CURRENCY')}} {{$pos_non_taxable_amount}}</strong></td>
                            <td><strong>{{env('APP_CURRENCY')}}  {{ $pos_discount_amount }}</strong></td>

                            <td style="font-size: 16.5px">
                                <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_taxable_amount,2) }} </strong>
                            </td>
                            <td style="font-size: 16.5px">
                                <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_tax_amount,2) }} </strong>
                            </td>
                        </tr>


                        {{--   <tr style="font-size: 16.5px">
                              <td colspan="5"></td>
                              <td >Summary</td>
                              <td></td>
                          </tr>

                          @foreach($allTotal as $key=>$value)
                              <tr style="font-size: 16.5px">
                                  <td colspan="5"></td>
                                  <td>{{ ucfirst($key) }}</td>
                                  <td>{{env(APP_CURRENCY)}}{{ number_format($value,2) }}</td>
                              </tr>
                          @endforeach --}}




                        </tfoot>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
            <div style="text-align: center;">
                {!! $orders->appends(\Request::except('page'))->render() !!} </div>

        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">

    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }

</script>


<script type="text/javascript">
    $(document).on('change', '#order_status', function() {

        var id = $(this).closest('tr').find('.index_sale_id').val();
        var purchase_status = $(this).val();
        $.post("/admin/ajax_order_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

</script>
<script type="text/javascript">
    $("#btn-submit-filter").on("click", function() {

        outlet_id = $("#filter-outlet").val();
        start_date = $("#start_date").val();
        end_date = $("#end_date").val();
        user_id = $("#user_id").val();
        fiscal_year = $("#fiscal_year_id").val();
        type = $("#bill_type").val();
        window.location.href = "{!! url('/') !!}/admin/orders/alloutlet?start_date=" + start_date + "&end_date=" + end_date + "&outlet_id=" + outlet_id+ "&type=" + type+'&search=true&'
        + 'user_id=' + user_id + "&fiscal_year=" + fiscal_year;

    });

    $("#btn-submit-filter-nep").on("click", function() {

        outlet_id = $("#filter-outlet-nep").val();
        start_date = $("#start_date_nep").val();
        end_date = $("#end_date_nep").val();
        user_id = $("#user_id").val();
        fiscal_year = $("#fiscal_year_id").val();
        type = $("#bill_type").val();
        window.location.href = "{!! url('/') !!}/admin/orders/alloutlet?start_date_nep=" + start_date + "&end_date_nep=" + end_date + "&outlet_id=" + outlet_id+'&search=true&' + 'user_id=' + user_id +"&fiscal_year=" + fiscal_year+"&type=" + type;

    });


    $("#btn-filter-clear").on("click", function() {

        window.location.href = "{!! url('/') !!}/admin/orders/alloutlet";
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
    });

</script>

<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/nepali-date-picker/nepali-date-picker.min.css">
<script src="/nepali-date-picker/nepali-date-picker.js"></script>
<script type="text/javascript">
    $(function() {
        $('#date1').datepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            dateFormat: 'yy-m-d'
            , sideBySide: true
            , beforeShow: function() {
                setTimeout(function() {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });
    });

</script>


<script>
    $(function() {
        $('#start_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
        });
        $('#end_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
        });
    });
    $("#start_date_nep").nepaliDatePicker();
    $("#end_date_nep").nepaliDatePicker();

</script>


<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.searchable').select2();

    });

</script>


@endsection

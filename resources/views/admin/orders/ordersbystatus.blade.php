@extends('layouts.master')
@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')

@endsection
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="row">

    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title task-title" data-id='ordered'>
                    Ordered
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" id="ordered">

                @foreach($ordered as $ord)
                <div class="col-lg-2 col-xs-6" draggable="true" id="{{$ord->id}}">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h4>Table: {{$ord->table}}</h4>
                            <span>Cover: {{$ord->covers}}</span>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>

                        {!! \Form::select('ready_status', ['ordered'=>'Ordered','cooking'=>'Cooking','cooked'=>'cooked','served'=>'Served'], $ord->ready_status, ['class' => 'index_ready_status form-control small-box-footer'])!!}

                        <input type="hidden" name="order_id" value="{{$ord->id}}" class="order_id">
                    </div>
                </div>
                @endforeach

            </div>
            <!-- /.box-body -->
        </div>
    </div>


    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title task-title" data-id='cooking'>
                    Cooking
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding table-responsive" id="cooking">

                @foreach($cooking as $ord)
                <div class="col-lg-2 col-xs-6" id="{{$ord->id}}">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h4>Table: {{$ord->table}}</h4>
                            <span>Cover: {{$ord->covers}}</span>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        {!! \Form::select('ready_status', ['ordered'=>'Ordered','cooking'=>'Cooking','cooked'=>'cooked','served'=>'Served'], $ord->ready_status, ['class' => 'index_ready_status form-control small-box-footer'])!!}
                        <input type="hidden" name="order_id" value="{{$ord->id}}" class="order_id">
                    </div>
                </div>
                @endforeach
            </div>
            <!-- /.box-body -->
        </div>
    </div>


    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title task-title" data-id='cooked'>
                    Cooked
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" id="cooked">
                @foreach($cooked as $ord)
                <div class="col-lg-2 col-xs-6" id="{{$ord->id}}">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h4>Table: {{$ord->table}}</h4>
                            <span>Cover: {{$ord->covers}}</span>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        {!! \Form::select('ready_status', ['ordered'=>'Ordered','cooking'=>'Cooking','cooked'=>'cooked','served'=>'Served'], $ord->ready_status, ['class' => 'index_ready_status form-control small-box-footer'])!!}
                    </div>
                    <input type="hidden" name="order_id" value="{{$ord->id}}" class="order_id">
                </div>
                @endforeach
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title task-title" data-id='served'>
                    Served
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" id="served">
                @foreach($served as $ord)
                <div class="col-lg-2 col-xs-6" id="{{$ord->id}}">
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h4>Table: {{$ord->table}}</h4>
                            <span>Cover: {{$ord->covers}}</span>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        {!! \Form::select('ready_status', ['ordered'=>'Ordered','cooking'=>'Cooking','cooked'=>'cooked','served'=>'Served'], $ord->ready_status, ['class' => 'index_ready_status form-control small-box-footer'])!!}
                        <input type="hidden" name="order_id" value="{{$ord->id}}" class="order_id">
                    </div>
                </div>
                @endforeach
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title task-title" data-id='checkedout'>
                    Checkedout
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" id="checkedout">
                @foreach($checkedout as $ord)
                <div class="col-lg-2 col-xs-6" id="{{$ord->id}}">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h4>Table: {{$ord->table}}</h4>
                            <span>Cover: {{$ord->covers}}</span>
                        </div>
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <a class="small-box-footer">ChecKedOut</a>
                        <input type="hidden" name="order_id" value="{{$ord->id}}" class="order_id">
                    </div>
                </div>
                @endforeach
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

<script>
    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(document.getElementById(data));
    }

    const status_color = {
        ordered: 'label-warning'
        , cooking: 'label-info'
        , cooked: 'label-danger'
        , served: 'label-success'
    }

    const div_color = {
        ordered: 'bg-yellow'
        , cooking: 'bg-info'
        , cooked: 'bg-red'
        , served: 'bg-green'
    }

    $(document).on('change', '.index_ready_status', function() {
        let val = $(this).val();

        //console.log(val);
        $(this).attr('class', `index_ready_status form-control small-box-footer`);
        var orderId = $(this).parent().parent().find('.order_id').val();
        var new_class = 'small-box' + ' ' + div_color[val];

        $(`.box-body #${orderId} .small-box`).attr('class', new_class);
        var html = $(`.box-body #${orderId}`).clone();
        $(`.box-body #${orderId}`).remove();

        // $(html).find('small-box').toggleClass()
        // console.log(html);
        let _token = $('meta[name=csrf-token]').attr('content');
        var data = {
            _token: _token
            , id: orderId
            , op: 'status'
            , value: val
        , }
        $.ajax({
            url: `/admin/orders/${orderId}`
            , type: 'PUT'
            , data: data
            , success: function(result) {

                $(`#${val}`).append(html);

                console.log($(`#${val}`).append(html));
                setTimeout(function() {
                    $(`#${orderId} select`).val(val);
                }, 100)

            },

        });

    });

</script>

@endsection

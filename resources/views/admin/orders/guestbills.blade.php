@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">

    <h1>
        Resv ID# {{ $resv->id }} Sales ( {{env('APP_CURRENCY')}}

        <?php
                
                   $number = \App\Models\Orders::where('reservation_id', \Request::segment(4))->sum('total_amount');
                
                  echo number_format($number,2);
                  ?> )
        <small> Manage {{ ucfirst(\Request::get('type'))}}

            <?php

                if(\Request::get('type') == 'quotation')
                  $ids = \App\Models\Orders::where('order_type','quotation')->lists('id')->all();
                elseif(\Request::get('type') == 'invoice')
                  $ids = \App\Models\Orders::where('order_type','proforma_invoice')->lists('id')->all();
                elseif(\Request::get('type') == 'order')
                   $ids = \App\Models\Orders::where('order_type','order')->lists('id')->all();
                else
                   $ids = \App\Models\Orders::lists('id')->all();

                  $paid_amount = \App\Models\Payment::whereIn('sale_id',$ids)->sum('amount');

                  if($paid_amount ==  null)
                    $paid_amount = 0.00;

                  $due_amount = $number-$paid_amount; 
                 ?>

            Paid <span class="label label-warning">{{ number_format($paid_amount,2) }}</span> Due <span class="label label-danger">{{ number_format($due_amount,2)}}</span> so far

        </small>
    </h1>

</section>

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
        <div class="box box-primary">

            <div class="box-header with-border">
                &nbsp;
                <a class="btn btn-social btn-foursquare" href="/admin/hotel/openoutlets" title="Create Order">
                    <i class="fa fa-plus"></i> Create New {!! $_GET['type'] == 'po' ? 'Purchase Order' : ucfirst($_GET['type']) !!}
                </a>
                &nbsp;
                <a class="btn btn-default btn-sm" href="#" onclick="document.forms['frmClientList'].action = '{!! route('admin.orders.enable-selected') !!}';  document.forms['frmClientList'].submit(); return false;" title="{{ trans('general.button.enable') }}">
                    <i class="fa fa-check-circle"></i>
                </a>
                &nbsp;
                <a class="btn btn-default btn-sm" href="#" onclick="document.forms['frmClientList'].action = '{!! route('admin.orders.disable-selected') !!}';  document.forms['frmClientList'].submit(); return false;" title="{{ trans('general.button.disable') }}">
                    <i class="fa fa-ban"></i>
                </a>



                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>



            <div class="box-body">

                <span id="index_lead_ajax_status"></span>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr>
                                <th style="text-align: center">
                                    <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                                        <i class="fa fa-check-square-o"></i>
                                    </a>
                                </th>
                                <th>id</th>
                                <th>Reservation</th>
                                <th>Outlet</th>
                                <th>Status</th>
                                <th>Paid</th>
                                <th>Balance</th>
                                <th>Pay Status</th>
                                <th>Total</th>
                                <th>Tools </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($orders) && !empty($orders))
                            @foreach($orders as $o)
                            <tr>
                                @if($o->viewed == '0')
                                <td class="bg-warning" align="center">{!! Form::checkbox('chkClient[]', $o->id); !!}</td>

                                <td class="bg-warning"><a href="/admin/orders/{{$o->id}}">PORD{!! $o->id !!}</a><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                <td class="bg-warning">
                                    <h4><a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}} </a><small>{{ $o->name }}</small></h4>
                                </td>

                                <td class="bg-warning"> {{ $o->id}} </td>

                                <td class="bg-warning">
                                    {!! \Form::select('order_status',['Active'=>'Active','Canceled'=>'Canceled','Invoiced'=>'Invoiced'], $o->status, ['class' =>'form-control label-default','id' => 'order_status'])!!}
                                </td>

                                <?php
                                               $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                             ?>

                                <td class="bg-warning">{!! mumber_format($paid_amount,2) !!}</td>

                                <td class="bg-warning">{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                @if($o->payment_status == 'Pending')
                                <td><span class="label label-warning">Pending</span></td>
                                @elseif($o->payment_status == 'Partial')
                                <td><span class="label label-info">Partial</span></td>
                                @elseif($o->payment_status == 'Paid')
                                <td><span class="label label-success">Paid</span></td>
                                @else
                                <td><span class="label label-warning">Pending</span></td>
                                @endif

                                <td class="bg-warning">{!! $o->total_amount !!}</td>

                                <td class="bg-warning">
                                    <a href="/admin/order/generatePDF/{{$o->id}}"><i class="fa fa-download"></i></a>
                                    <a href="/admin/order/print/{{$o->id}}"><i class="fa fa-print"></i></a>
                                    @if(\Request::get('type') == 'quotation')
                                    <a href="/admin/order/copy/{{$o->id}}"><i class="fa fa-copy"></i></a>
                                    @endif
                                </td>
                                <td class="bg-warning">
                                    @if($o->status != 'Invoiced' && \Request::get('type') == 'invoice')
                                    @if( $o->isEditable() || $o->canChangePermissions() )
                                    <a href="{!! route('admin.orders.edit', $o->id) !!}?type={{$o->order_type}}" title="{{ trans('general.button.edit') }}"><i class="fa fa-edit"></i></a>
                                    @else
                                    <i class="fa fa-edit text-muted" title="{{ trans('admin/orders/general.error.cant-edit-this-document') }}"></i>
                                    @endif
                                    @if( $o->isDeletable())
                                    <a href="{!! route('admin.orders.confirm-delete', $o->id) !!}?type={{\Request::get('type')}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
                                    @else
                                    <i class="fa fa-trash-alt text-muted" title="{{ trans('admin/orders/general.error.cant-delete-this-document') }}"></i>
                                    @endif
                                    @else
                                    <i class="fa fa-edit text-muted" title="{{ trans('admin/orders/general.error.cant-edit-this-document') }}"></i>
                                    <i class="fa fa-trash-alt text-muted" title="{{ trans('admin/orders/general.error.cant-delete-this-document') }}"></i>
                                    @endif
                                    <a href="{!! route('admin.payment.purchase.list', $o->id) !!}" title="View Payment"><i class="fa fa-credit-card"></i></a>
                                </td>
                                @else
                                <td align="center">{!! Form::checkbox('chkClient[]', $o->id); !!}</td>
                                <td><a href="/admin/orders/{{$o->id}}">PORD{!! $o->id !!}</a><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>
                                <td><a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{ $o->name }}</small> </td>
                                <td class="bg-warning"> {{ $o->outlet->name}} </td>
                                <td>
                                    {!! \Form::select('order_status',['Active'=>'Active','Canceled'=>'Canceled','Invoiced'=>'Invoiced'], $o->status, ['class' =>'form-control label-default','id' => 'order_status'])!!}
                                </td>
                                <?php
                                              $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                             ?>
                                <td>{!! number_format($paid_amount,2) !!}</td>
                                <td>{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                @if($o->payment_status == 'Pending')
                                <td><span class="label label-warning">Pending</span></td>
                                @elseif($o->payment_status == 'Partial')
                                <td><span class="label label-info">Partial</span></td>
                                @elseif($o->payment_status == 'Paid')
                                <td><span class="label label-success">Paid</span></td>
                                @else
                                <td><span class="label label-warning">Pending</span></td>
                                @endif

                                <td>{!! number_format($o->total_amount,2) !!}</td>
                                <td>
                                    <a href="/admin/order/generatePDF/{{$o->id}}"><i class="fa fa-download"></i></a>
                                    <a href="/admin/order/print/{{$o->id}}"><i class="fa fa-print"></i></a>
                                    @if(\Request::get('type') == 'quotation')
                                    <a href="/admin/order/copy/{{$o->id}}"><i class="fa fa-copy"></i></a>
                                    @endif
                                </td>
                                <td>
                                    @if ( $o->isEditable() || $o->canChangePermissions() )
                                    @if($o->order_type == 'proforma_invoice' && \Request::get('type') == 'invoice')
                                    <i class="fa fa-ban text-muted" title=""></i>
                                    @else
                                    <a href="{!! route('admin.orders.edit', $o->id) !!}?type={{\Request::get('type')}}" title="{{ trans('general.button.edit') }}"><i class="fa fa-edit"></i></a>
                                    @endif
                                    @else
                                    <i class="fa fa-edit text-muted" title="{{ trans('admin/orders/general.error.cant-edit-this-document') }}"></i>
                                    @endif
                                    @if ( $o->isDeletable() )
                                    @if($o->order_type == 'proforma_invoice' && \Request::get('type') == 'invoice')
                                    <i class="fa fa-ban text-muted" title=""></i>
                                    @else
                                    <a href="{!! route('admin.orders.confirm-delete', $o->id) !!}?type={{\Request::get('type')}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
                                    @endif
                                    @else
                                    <i class="fa fa-trash text-muted" title="{{ trans('admin/orders/general.error.cant-delete-this-document') }}"></i>
                                    @endif
                                    <a href="{!! route('admin.payment.orders.list', $o->id) !!}" title="View Payment"><i class="fa fa-credit-card"></i></a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
            <div style="text-align: center;"> {!! $orders->appends(\Request::except('page'))->render() !!} </div>
        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }

</script>


<script type="text/javascript">
    $(document).on('change', '#order_status', function() {

        var id = $(this).closest('tr').find('.index_sale_id').val();

        var purchase_status = $(this).val();
        $.post("/admin/ajax_order_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

</script>
<script type="text/javascript">
    $("#btn-submit-filter").on("click", function() {

        status = $("#filter-status").val();
        type = $("#order_type").val();

        window.location.href = "{!! url() !!}/admin/orders?status=" + "&type=" + type;
    });

    $("#btn-filter-clear").on("click", function() {

        type = $("#order_type").val();
        window.location.href = "{!! url() !!}/admin/orders?type=" + type;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
    });

</script>

@endsection

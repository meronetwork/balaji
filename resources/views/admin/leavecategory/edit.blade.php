@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="/bower_components/tags/js/tag-it.js"></script>
    <link href="/bower_components/tags/css/jquery.tagit.css" rel="stylesheet" type="text/css"/>
    <link href="/bower_components/tags/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
        <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              {{ $page_title ?? 'Page Title' }}
                <small>{{$page_description ?? 'Page Description'}}</small>
            </h1>
           
        </section>

     <div class='row'>
       <div class='col-md-12'>
          <div class="box">
		     <div class="box-body ">
		     	<form method="post" action="/admin/leavecategory/{{$leavecategory->leave_category_id}}/update" enctype="multipart/form-data">  
		     	{{ csrf_field() }}                 

                 <div class="row">

				 <div class="col-md-4">
                 	    <label class="control-label">Leave Code</label>
                        <input type="text" name="leave_code" placeholder="Leave Code" id="leave_code" value="{{$leavecategory->leave_code}}" class="form-control" >
                 	</div>

                 	<div class="col-md-4">
                 	    <label class="control-label">Category Name</label>
                        <input type="text" name="leave_category" placeholder="Leave Category" id="leave_category" value="{{$leavecategory->leave_category}}" class="form-control" >
                 	</div>

                    <div class="col-md-4">
                        <label class="control-label">Leave Quota</label>
                        <input type="text" name="leave_quota" placeholder="Leave Quota" id="leave_quota" value="{{$leavecategory->leave_quota}}" class="form-control" >
                    </div>

					<div class="col-md-4">
                 	    <label class="control-label">Leave Type</label>
                        <input type="text" name="leave_type" placeholder="Leave Category" id="leave_type" value="{{$leavecategory->leave_type}}" class="form-control" >
                 	</div>

					 <div class="col-md-4">
                 	    <label class="control-label">Lapse Type</label>
                        <input type="text" name="lapse_type" placeholder="Lapse Type" id="lapse_type" value="{{$leavecategory->lapse_type}}" class="form-control" >
                 	</div>
                      <div class="col-md-4">
                        <label class="control-label">Leave Flow</label>
                        <br>
                        <label class="control-label">
                            <input type="radio" name="leave_flow" checked="" value="static">
                            Static Leave
                            
                        </label>
                 
                        <label class="control-label">
                             <input type="radio" name="leave_flow" value="dynamic" 
                             @if($leavecategory->leave_flow == 'dynamic') checked="" @endif  >
                            Dynamic Leave
                    
                        </label>
                        <label class="control-label">
                            <input type="radio" name="leave_flow" value="hourly"  @if($leavecategory->leave_flow == 'hourly') checked="" @endif >
                            Horuly
                           
                        </label>
                    </div>
                 	
                </div>       
		     </div>
		   </div>

		        <div class="row">
	                <div class="col-md-12">
	                    <div class="form-group">
	                        {!! Form::submit( trans('general.button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
	                        <a href="{!! route('admin.leavecategory') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
	                    </div>
	                 </div>
	            </div>

	        </form>
        @if($leavecategory->leave_flow == 'static')
        <div class="panel">
            <div class="panel-heading bg-danger">
              <h4>Leave User</h4></div>
            <div class="panel-body">
                <table class="table">
                    <tr class="bg-success">
                        <th>S.N</th>
                        <th>Username</th>
                         <th>Action</th>
                    </tr>
                    @foreach($leaveUser as $key=>$value)
                    <tr>
                        <td>#{{ $value->id }}</td>
                        <td>{{ $value->user->first_name }} {{ $value->user->last_name }}</td>
                        <th>
                          <a href="/admin/remove_add_leave_user/{{ $value->id }}" onclick="return confirm('Are you sure you want to remove')">
                            <i class="fa fa-trash deletable"></i>
                          </a>
                        </th>
                    </tr>
                    @endforeach
                </table>
              <h4>Add More +</h4>
            <form method="POST" action="{{ route('admin.add_leave_user.store',$leavecategory->leave_category_id) }}" >
                @csrf
                <div class="row" style="visibility: hidden;" id='select_users'>
                    <div class="col-md-4 form-group">
                        <select class="form-control searchable" multiple="multiple" name="user[]" required="">
                            
                            @foreach($users as $key=>$value)
                                <option value="{{ $value->id }}">{{ $value->first_name }} {{ $value->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 form-group">
                        <button class="btn btn-primary btn-sm" >Submit</button>
                    </div>
                </div>

            </form>

            </div>
        </div>
        @endif



		</div>
	</div>
        
   
@endsection

@section('body_bottom')
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
 <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>






<script type="text/javascript">

    $(document).ready(function() {
        $("#skills").tagit();
    });

</script>

<script type="text/javascript">
	
     $(function() {
			$('.datepicker').datetimepicker({
					//inline: true,
					//format: 'YYYY-MM-DD',
					format: 'YYYY-MM-DD', 
	        sideBySide: true,
	        allowInputToggle: true
				});
		});
     $('#select_users').css({'visibility':'visible'});
  $('.searchable').select2({

    placeholder:"Select Users",
  });
</script>
@endsection

@extends('layouts.master')
@section('content')

<style type="text/css">
    .container{max-width:1170px; margin:auto;}
.chatimg{ 
  border-radius: 50%;
  width: 50px;
  height: 50px;
  padding: 10px;


}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 30%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; padding:
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 40%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}
.deletemessage{
  float: right;
}
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-custom" data-collapsed="0">
           
            <div class="panel-body">
<div class="messaging">
      <div class="inbox_msg">

        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Recent</h4>
            </div>
          </div>
          <div class="inbox_chat">
            @foreach($threads as $inbox)
             @if(!is_null($inbox->thread))
             <a href="{{route('message.read', ['id'=>$inbox->withUser->id])}}">
            <div class="chat_list @if(\Request::segment(3) == $inbox->withUser->id) active_chat @endif">
              <div class="chat_people">
                <div class="chat_img"><img src="{{$inbox->withUser->avatar}}" alt="avatar" /> </div>
                <div class="chat_ib">
                  <h5>{{$inbox->withUser->first_name}}&nbsp;{{$inbox->withUser->last_name}}
                    <span class="chat_date">{{ date('dS M', strtotime($inbox->thread->created_at)) }} {{ $inbox }}</span></h5>
                  <p>{{substr($inbox->thread->message, 0, 20)}}
                    <span class="chat_date"> @if(auth()->user()->id == $inbox->thread->sender->id)
                            <span class="fa fa-reply"></span>
                        @endif
                      </span>
                  </p>
                </div>
              </div>
            </div>
            </a>
            @endif
            @endforeach
          </div>
        </div>

        <div id='userlivechatbox'>
        <div class="mesgs">
          <div class="msg_history" id='chathistory' data-total-message = "{{ $totalMessage }}" data-receiver="{{$user->id}}" >

            <div v-for = '(message,index) in messageobj'  :key='message.id'> 
            <div class="outgoing_msg" v-if='message.sender.id == authuser' v-bind:id='"message-"+message.id'>
              <div class="sent_msg">
                <p class="userMessage">  
                 @{{message.message}}
                 <span class="deletemessage" data-toggle="tooltip" data-placement="top" title="Delete">

                  <a href="#" v-bind:data-message-id="message.id" class="talkDeleteMessage"><i class="fa fa-close"></i></a>

                </span>
                </p>
                <span class="time_date">@{{message.created_at | moment }}   
                  <a href="#" class="text-muted" title="Seen" v-if='index == messageobj.length - 1 && message.is_seen'>&#10004;&#10004;</a>
                </span> 
                </div>
              </div>
           
            <div class="incoming_msg"  v-if='message.sender.id != authuser' v-bind:id='"message-"+message.id'>
              <div class="incoming_msg_img"> <img :src="profile_img" alt="chat" class="rounded-circle chatimg"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>@{{message.message}}
                  <span class="deletemessage" data-toggle="tooltip" data-placement="top" title="Delete">
                  <a href="#" v-bind:data-message-id="message.id" class="talkDeleteMessage"> <i class="fa fa-close"   ></i></a>
                </span>
                  </p>
                  <span class="time_date"> @{{message.created_at | moment }}</span></div>
              </div>
            </div>
          
          </div>
          <div v-if='messageobj.length == 0'>
         <strong> No any Messages Yet !! </strong>
         </div>
          </div>


          <div class="type_msg" >
            <form method="post" id ='talkSendMessages'>
            <div class="input_msg_write">
              <input name="message-data" id="message-data" placeholder ="Type your message" rows="3" class="SeenMessage" data-user-id="{{$message->user->id}}"
              data-conversation-id="{{$messages[0]->conversation_id}}">
            <input type="hidden" name="_id" value="{{@request()->route('id')}}">
              <button class="msg_send_btn" type="submit" ><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </div>
            </form>
          </div>
        </div>
      </div>

        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>User List</h4>
            </div>
          </div>
          <div class="inbox_chat">
           @foreach($users as $user)
             <a href="{{route('message.read', ['id'=>$inbox->withUser->id])}}">
            <div class="chat_list @if(\Request::segment(3) == $inbox->withUser->id) active_chat @endif">
              <div class="chat_people">
                <div class="chat_img"><img src="{{$user->avatar}}" alt="avatar" /> </div>
                <div class="chat_ib">
                  <h5>{{$user->first_name}}&nbsp;{{$user->last_name}}
                </div>
              </div>
            </div>
            </a>
            @endforeach
          </div>
        </div>
      </div>
      
    </div>
</div>
</div>
</div>
</div>

<?php $userid = \Auth::user()->id; ?>
<script>
  var __baseUrl = "{{url('/')}}";
moment.locale('ne');
const segment_user = {{ \Request::segment(3) }};
const auth_users = {{  $userid }};
const profile_img = `{{ $profile_img }}`;
</script>

<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="{{asset('chat/js/talk.js')}}"></script>
​<script>
 
var myObject = new Vue({
  el: '#userlivechatbox',
  data: {
    messageobj: <?php echo json_encode($messages); ?>,
    authuser: auth_users,
    profile_img: profile_img,
  },
  methods:{
        callFunction: function () {
            var v = this;
            $.get('/admin/talk/'+segment_user,function(response){
              v.messageobj = response.message;
              setTimeout(function(){
                v.callFunction()
              },7000);
          });
        },
        loadMessage: function(){
           var v = this;
           $.get('/admin/talk/'+segment_user,function(response){
              v.messageobj = response.message;
          });
        },
        appendmessage: function(message){
          var v = this;
          let obj = v.messageobj[0];
          obj['message'] = message;
          obj['created_at'] = null;
          obj['id']= 'new';
          obj['sender']['id'] = auth_users;
          v.messageobj.push(obj);
          $('#message-data').val('');
          setTimeout(function() {
          $('#chathistory').scrollTop( $('#chathistory')[0].scrollHeight);
         }, 100);
         
        },
    },

    filters: {
      moment: function (date) {
        return moment(date)
              .startOf("minute")
              .fromNow();
      }
    },

    mounted () {

      this.callFunction();
        $('#chathistory').scrollTop( $('#chathistory')[0].scrollHeight);
    }
});



 $('#talkSendMessages').on('submit', function(e) {
        e.preventDefault();
        var url, request, tag, data;
        tag = $(this);
        url = __baseUrl + '/ajax/message/send';
        data = tag.serialize();
        myObject.appendmessage($('#message-data').val());
        request = $.ajax({
            method: "post",
            url: url,
            data: data
        });
        request.done(function (response) {
            if (response.status == 'success') {
                tag[0].reset();
                myObject.loadMessage();
            }
        });
    });


  $('body').on('click', '.talkDeleteMessage', function (e) {
        e.preventDefault();
        var tag, url, id, request;
        tag = $(this);
        id = tag.data('message-id');
        url = __baseUrl + '/ajax/message/delete/' + id;
        if(!confirm('Do you want to delete this message?')) {
            return false;
        }
        request = $.ajax({
            method: "post",
            url: url,
            data: {"_method": "DELETE"}
        });

        request.done(function(response) {
            myObject.loadMessage();
        });
    });





  $('body').on('click', '.talkDeleteMoreMessage', function (e) {
        e.preventDefault();
        var tag, url, id, request;
        tag = $(this);
        id = tag.data('message-id');
        url = __baseUrl + '/ajax/message/delete/' + id;
        if(!confirm('Do you want to delete this message?')) {
            return false;
        }
        request = $.ajax({
            method: "post",
            url: url,
            data: {"_method": "DELETE"}
        });

        request.done(function(response) {
             $('#message-' + id).hide(500, function () {
                    $(this).remove();
                });
        });
    });

</script>
@endsection

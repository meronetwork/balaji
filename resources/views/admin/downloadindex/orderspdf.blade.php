<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | Purchase Order</title>
    <style type="text/css">
      
@font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}

table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223; 
  font-weight: bold;

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
    </style>

  </head><body>
    <header class="clearfix">

     <table>
            <tr>
        <td width="50%" style="float:left">
          <div id="logo">
            <img style="max-width: 150px" src="{{public_path()}}/{{ '/org/'.$organization->logo }}">
          </div>
        </td>
        <td width="50%" style="text-align: right">
        <div  id="company">
          <h4 class="name">{{ env('APP_COMPANY') }} </h4>
          <div>{{ env('APP_ADDRESS1') }}</div>
          <div>{{ env('APP_ADDRESS2') }}</div>
          <div>PAN: {{ \Auth::user()->organization->tpid }}</div>
          <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>
        </div>
        </td></tr>

      </table> 

     
    </header>
 
      <div id="details" class="clearfix">
      </div>
      <h1>Orders List</h1>

      <table >
        <thead>
          <tr>
          <th class="no">id</th>
          <th class="no">Type</th>
          <th class="no">Issue Date</th>
          <th class="no">Client</th>
          <th class="no">Order Status</th>
          <th class="no">Source</th>
          <th class="no">Paid Amount</th> 
          <th class="no">Balance {{ env('APP_CURRENCY')}}</th>
          <th class="no">Pay Status</th>
          <th class="no">Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $n= 0;
          ?>
          @foreach($orders as $odk => $o)
          <tr>
            

              <td>{!! $o->id !!}</td>
              <td>{!! ucwords($o->order_type) !!}</td>

              <td> 
                  {{ $o->bill_date }}
              </td>

              <td > 
                @if($o->source=='client') {{ $o->client->name }} @else {{ $o->lead->name }} @endif
                <small>{{ $o->name }}</small></a>
              </td>
              <td>
                    {{$o->status}}
              </td>
                <td>{{ ucfirst($o->source) }}</td>

               <?php
                $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
               ?>
              <td>{!! number_format($paid_amount,2) !!}</td>
              <td>{!! number_format($o->total_amount-$paid_amount,2)  !!}</td>

              <td>{{$o->payment_status}}</td>
                
              <td>{!! number_format($o->total_amount,2) !!}</td>
          </tr>
         @endforeach
          
        </tbody> 
       
      </table>
     
      <br>
    
    <footer>
      {{ ucwords(str_replace("_", " ", ucfirst($ord->purchase_type)))}} was created on MEROCRM.
    </footer>
  </body></html>
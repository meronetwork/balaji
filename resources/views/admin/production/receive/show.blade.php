@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Received Production
                <small>View Received Production</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="panel panel-custom box">
 	<div class="panel-heading">
	   <div class='row'>
       		<div class='col-md-12'>
            <b><font size="4">Slips no # {{$slips->id}}</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.production.receive-production-print',$slips->id) }}">
                            <i class="fa  fa-print"></i>&nbsp;<strong>PRINT SLIPS</strong>
            </a>
             <a class="btn btn-danger btn-sm"  title="Import/Export Leads" href="javascript:history.back()">
                            <i class="fa  fa-backward"></i>&nbsp;<strong>CANCEL INVOICES</strong>
            </a>
            </div>
        </div>
	</div>
</div>

<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Product location</th>
        <th>Product section</th>
         <th>Date</th>
    </tr>
</thead>
<tbody>
	<tr>
		<td >
			<input type="checkbox" name="event_id" value="{{$pu->id}}">
        </td>
        <td>{{$slips->id}}</td>
        <td>{{ucfirst(trans($slips->location_name))}}</td>
        <td>{{ucfirst(trans($slips->section_name))}}</td>
        <td>{{ date('dS M y', strtotime($slips->date)) }}</td>

    </tr>
</tbody>
</table>
    <div class="row">
     <div class="col-md-12">
        <label for="inputEmail3" class="control-label">
       Description
        </label>
          <textarea class="form-control" name="description" id="general_remarks"  readonly="">{{$slips->description}}</textarea>
        </div>
    </div>
<div class="panel-heading">
       <div class='row'>
            <div class='col-md-12'>
            <b><font size="4">Issued Slips details</font></b>
        </div>
    </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>#</th>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Lost Quantity</th>
        @if($slips->can_return == 0)
            <th>Return Quantity</th>
        @endif
        <th>Stock Quantity</th>
        <th>lost Notes</th>
    </tr>
</thead>
<tbody>
    @foreach($_details as $key=>$d)
    <tr>
        <td >
            {{$key + 1 }}
        </td>
        <td>{{ucfirst(trans($d->name))}}</td>
        <td>{{$d->lost_qty?$d->quantity+$d->lost_qty:$d->quantity}}&nbsp;{{ucfirst(trans($d->symbol))}}</td>
        <td>{{$d->lost_qty}}&nbsp;{{ucfirst(trans($d->lost_qty?$d->symbol:'-'))}}</td>
        @if($slips->can_return == 0)
            <td>{{$d->return_quantity}}&nbsp;{{ucfirst(trans($d->return_quantity?$d->symbol:'-'))}}</td>
            <td>{{$d->quantity - $d->return_quantity}}&nbsp;{{ucfirst(trans($d->symbol))}}</td>
        @else
            <td>{{$d->quantity}}&nbsp;{{ucfirst(trans($d->symbol))}}</td>
        @endif

        <td>{{$d->lost_notes}}</td>

    </tr>
    @endforeach

</tbody>
</table>
</div>
@endsection

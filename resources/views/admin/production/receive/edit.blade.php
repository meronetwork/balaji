@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Received Production
                <small>Add, Edit or Delete Received Production</small>
            </h1>
            <p> Issue Raw Material from Warehouse to Section for Production / Process

 </p>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
<div class="panel panel-custom box">
 	<div class="box-header with-border">
	   <div class='row'>
       		<div class='col-md-12'>
                    <a href="#" class="filter-by-date" title="Filter"><i class="fa  fa-file"></i></a>&nbsp;
            <b><font size="4">Received Production Edit</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-info btn-sm"  title="List" href="{{ route('admin.production.receive-production') }}">
                <i class="fa fa-list"></i>&nbsp;&nbsp;<strong>Received Production</strong>
            </a>

            </div>
        </div>
	</div>
</div>
    <form method="post" action="{{route('admin.production.receive-production-edit',$slips->id)}}" class="issue-slips form">
        {{csrf_field()}}
        <div class="panel panel-custom">
            <div class="panel-heading">
                <div align="center" style="color:red" id="slipserror"></div>
                <br>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input required="" type="text" class="form-control date_in" value="{{\Carbon\Carbon::now()->toDateString()}}" name="date" id="date">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Warehouse</label>
                            <div class="col-sm-8">
                                <select name="product_location_id"
                                        class="form-control">
                                    @foreach($product_location as $key=>$pl)
                                        <option value="{{$pl->id}}" >{{$pl->location_name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group" id="border-none">
                            <label class="col-sm-6  control-label">Section name</label>
                            <div class="col-sm-6">
                                <select name="product_section_id"
                                        class="form-control">
                                    @foreach($product_section as $key=>$ps)
                                        <option value="{{$ps->id}}" >{{$ps->section_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <br>
                <div id="orderFields" >
                    <table class="table">
                        <thead>
                        <tr class="bg-maroon">
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Lost Quantity</th>
                            <th>Stock Quantity</th>
                            <th>Lost Notes</th>
                        </tr>
                        </thead>
                        <tbody id='multipleDiv'>
                        @foreach($_details as $odk => $odv)
                            <tr>
                                <td>
                                    <input type="hidden" value="{{$odv->id}}" name="fact_product_ledger_id[]">
                                    <select class="form-control select2 product_id" name="product_id[]" required="required">
                                        @if(isset($products))
                                            <option value="">Select Products</option>
                                            @foreach($products as $key => $pk)
                                                <option value="{{ $pk->id }}" @if($odv->product_id == $pk->id) selected="selected"@endif>{{ $pk->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="number" class="form-control instock" name="qty_invoiced[]" placeholder="Quantity" min="1" value="{{ $odv->lost_qty?$odv->qty_invoiced+$odv->lost_qty:$odv->qty_invoiced}}" required="required" readonly="readonly" step="any">
                                        <div class="input-group-addon">
                                            {{$odv->symbol}}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="number" class="form-control quantity" name="lost_qty[]" placeholder="Lost Quantity" min="0" value="{{ $odv->lost_qty }}" step="any" >
                                        <div class="input-group-addon">
                                            {{$odv->symbol}}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="number" class="form-control stock" name="credit_qty[]" placeholder="Remaining Quantity" min="0" value="{{ $odv->qty_invoiced }}" required="required" step="any" readonly="readonly">
                                        <div class="input-group-addon">
                                            {{$odv->symbol}}
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" name="lost_notes[]" class="form-control " placeholder="Lost Notes" step="any" value="{{ $odv->lost_notes }}">
                                </td>

                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="control-label">
                            Description
                        </label>
                        <textarea class="form-control" name="description" id="general_remarks" placeholder="Write Description">{{$slips->description}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 1px">
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn-submit-edit" type="submit" ><i class="fa fa-save"></i> Save </button>
                        <a href="{{ route('admin.production.receive-production') }}" class="btn btn-default">Cancel</a>
                    </div>

                </div>
            </div>
        </div>

    </form>

    @section('body_bottom')
        <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
        <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
        <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
        <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

        <!-- Timepicker -->
        <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/timepicker.css") }}" rel="stylesheet" type="text/css" />
        <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/timepicker.js") }}" type="text/javascript"></script>

        <!-- SELECT2-->
        <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2.css") }}">
        <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2-bootstrap.css") }}">
        <script src="{{ asset("/bower_components/admin-lte/select2/js/select2.js") }}"></script>
        <script>
            $(function() {
                $('#date').datetimepicker({
                    //inline: true,
                    format: 'YYYY-MM-DD',
                    //sideBySide: true
                })


            });

            $("#addMore").on("click", function () {
                //$($('#orderFields').html()).insertBefore(".multipleDiv");
                $(".multipleDiv").after($('#orderFields #more-tr').html());
            });
            $(document).on('click', '.remove-this', function () {
                $(this).parent().parent().parent().remove();
            });
            $(document).on('change','.form-control.select2.product_id',function(){
                let id = $(this).val();
                $(this).closest('tr').find('.form-control.total').val("");
                $.get('/admin/production/ajaxget/'+id,(result)=>{
                    let obj = JSON.parse(result);
                    // console.log(obj[0]);
                    if(obj[0].total == null)
                        obj[0].total = 0;
                    $(this).closest('tr').find('.form-control.instock').val(obj[0].total);
                    $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
                    let quantity = $(this).closest('tr').find('.form-control.quantity').val();
                    if(quantity){
                        let calc = Number(obj[0].total) - Number(quantity);
                        $(this).closest('tr').find('.form-control.total').val(calc);
                    }
                });

            });
            $(document).on('keyup','.form-control.quantity',function(){
                let quantity = $(this).val();
                let total = $(this).closest('tr').find('.form-control.instock').val();
                let calc = Number(total) - Number(quantity);
                $(this).closest('tr').find('.form-control.stock').val(calc);
            });
            $(document).on('submit','.issue-slips.form',function(){
                var error = false;
                $('.form-control.total').each(function(){
                    if($(this).val() < 0 ){
                        return error = true;
                    }
                });
                if(error){
                    $('#slipserror').html("<h4>Entered quantity exceeded from product instock</h4>");
                    return false;
                }

                // if($('.form-control.quantity').val()){
                //     return true;
                // }
                // else{
                //     $('#slipserror').html("<h4>Please select at least one product</h4>");
                //     return false;
                // }
            })
        </script>
    @endsection


@endsection

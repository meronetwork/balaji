<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ env('APP_COMPANY')}} | Issue slips report</title>

    <!-- block from searh engines -->
    <meta name="robots" content="noindex">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Set a meta reference to the CSRF token for use in AJAX request -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons 4.7.0 -->
    <link href="{{ asset("/bower_components/admin-lte/font-awesome/css/all.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />

    <!-- Application CSS-->
    <link href="{{ asset(elixir('css/all.css')) }}" rel="stylesheet" type="text/css" />


  </head>

<body onload="window.print();" cz-shortcut-listen="true" class="skin-blue sidebar-mini">

  <div class='wrapper'>

    <table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Product location</th>
        <th>Product section</th>
         <th>Date</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td >
            <input type="checkbox" name="event_id" value="{{$pu->id}}">
        </td>
        <td>{{$slips->id}}</td>
        <td>{{ucfirst(trans($slips->location_name))}}</td>
        <td>{{ucfirst(trans($slips->section_name))}}</td>
        <td>{{ date('dS M y', strtotime($slips->date)) }}</td>
        
    </tr>
</tbody>
</table>
    <div class="row">
     <div class="col-md-12">
        <label for="inputEmail3" class="control-label">
       Description
        </label>
          <textarea class="form-control" name="description" id="general_remarks"  readonly="">{{$slips->description}}</textarea>
        </div>
    </div>
<div class="panel-heading">
       <div class='row'>
            <div class='col-md-12'>
            <b><font size="4">Issued Slips details</font></b>     
        </div>
    </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>#</th>
        <th>Product Name</th>
        <th>Quantity</th>
    </tr>
</thead>
<tbody>
    @foreach($_details as $key=>$d)
    <tr>
        <td >
            {{$key + 1 }}
        </td>
        <td>{{ucfirst(trans($d->name))}}</td>
        <td>{{$d->quantity}}&nbsp;{{ucfirst(trans($d->symbol))}}</td>
        
    </tr>
    @endforeach

</tbody>
</table>
</body>

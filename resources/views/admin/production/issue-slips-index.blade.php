@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Issue Slips
                <small>Add, Edit or Delete Issue Slips</small>
            </h1>
            <p> Issue Raw Material from Warehouse to Section for Production / Process

 </p>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
<div class="panel panel-custom box">
 	<div class="box-header with-border">
	   <div class='row'>
       		<div class='col-md-12'>
                    <a href="#" class="filter-by-date" title="Filter"><i class="fa  fa-file"></i></a>&nbsp;
            <b><font size="4">Issue Slips List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-info btn-sm"  title="Create" href="{{ route('admin.production.issue-slip-create') }}">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Issue new slips</strong>
            </a>

            </div>

            <div class="col-md-3" style="float: right; ">
                <form method="get" action="/admin/production/search-issue/" id="searchform">
                    <div class="input-group ">
                        <input type="text" placeholder="Search......"   class="form-control" style="height: 30px" name="slip" required="">
                        <div class="input-group-addon">
                            <a href="#" type="submit" onclick="document.getElementById('searchform').submit()"><i class="fa fa-search"></i></a>
                        </div>
            </div>
                    </form>
        </div>

        </div>
	</div>
</div>
<div class="filter_form" style="display: none;margin-top: 10px" >
<form method="get" action="/admin/production/filter-issue/">
        <div class="box-header with-border">
      <div class="col-md-4">

<div class="form-group">
<label for="date" class="col-sm-3 control-label">From Date</label>
<div class="col-sm-6">
<div class="input-group">
    <input required="" type="text" class="form-control date_in"  name="from_date" id="from_date">
    <div class="input-group-addon">
        <a href="#"><i class="fa fa-calendar"></i></a>
    </div>
</div>
</div>
</div>

</div>
      <div class="col-md-4">

<div class="form-group">
<label for="date" class="col-sm-3 control-label">To Date</label>
<div class="col-sm-6">
<div class="input-group">
    <input required="" type="text" class="form-control date_in" value="{{ isset($selecteddate) ? $selecteddate : '' }}" name="to_date" id="to_date">
    <div class="input-group-addon">
        <a href="#"><i class="fa fa-calendar"></i></a>
    </div>
</div>
</div>
</div>

</div>

      <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Filter</button>
        </div>
</div>
    </form>
</div>

<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr class="bg-info">

        <th>ID</th>
        <th>Date</th>
        <th>Warehouse</th>
        <th>Product section</th>
         <th>Date</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="myTable">
	@foreach($slips as $key=>$is)
	<tr @if(!$is->can_return) style="background:#ffe5e5 !important;" @endif>

        <td>{{$is->id}}</td>
        <td>{{$is->date}}</td>
        <td>{{ucfirst(trans($is->location_name))}}</td>
        <td>{{ucfirst(trans($is->section_name))}}</td>
        <td>{{ date('dS M y', strtotime($is->date)) }}</td>
        <?php
         $datas = '<a href="'.route('admin.production.issue-slip-confirm-delete', $is->id).'?type='.\Request::get('type').'" data-toggle="modal" data-target="#modal_dialog" title="Delete"><i class="fa fa-trash-o deletable"></i></a>';
         ?>
        <td>@if($is->can_return)
            <a class="btn btn-round btn-xs btn-default" href="{{route('admin.production.issue-slip-edit',$is->id)}}" title="Edit">
                <i class="fa fa-edit"></i></a>&nbsp;&nbsp;
            @endif
            <a class="btn btn-round btn-xs bg-olive" href="{{route('admin.production.issue-slip-show',$is->id)}}" title="View"><i class="fa fa-file-text"></i></a>&nbsp;&nbsp;
            @if($isindex)
            <a class="btn btn-round btn-xs bg-maroon" href="{{route('admin.production.issue-slip-return',$is->id)}}" title="Return"><i class="fa fa-refresh"></i></a>&nbsp;&nbsp;
            <?php
            if($is->can_return)
            echo $datas ?>
        @endif
    </td>
    </tr>
    @endforeach

</tbody>
</table>
<div style="text-align: center;"> {!! $slips->render() !!} </div>
</div>
@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<!-- Timepicker -->
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/timepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/timepicker.js") }}" type="text/javascript"></script>

<!-- SELECT2-->
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2.css") }}">
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2-bootstrap.css") }}">
<script src="{{ asset("/bower_components/admin-lte/select2/js/select2.js") }}"></script>
<script>
    $(function() {
        $('#from_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD',
            //sideBySide: true
        })
            $('#to_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD',
            //sideBySide: true
        })


    });


$('.filter-by-date').on('click',function(){
let ishidden = $('.filter_form').css('display');
if(ishidden == 'none'){
    $('.filter_form').css('display','block');
}
else{
     $('.filter_form').css('display','none');
}
});
</script>
@endsection
@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>
            Final Goods Statement
            <small>Add, Edit or Delete Issue Slips</small>
        </h1>
        <p> Detailed Report from {{date('dS M y', strtotime($start_date))}} to {{date('dS M y', strtotime($end_date))}}

        </p>
        {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
    </section>
    <style>
        select { width:200px !important; }
    </style>
    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function() {

            $('#payment_month').datetimepicker({
                format: 'YYYY-MM',
                sideBySide: true
            });
        });
    </script>
    <div class="panel-heading">
        <div class='row'>
            <div class='col-md-12'>
                <b><font size="4">Slips no # {{$sauda->id}}</font></b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="/admin/production_report/final_goods/print?product_id={{ $product_id }}&start_date={{ $start_date }}&end_date={{ $end_date }}">
                        <i class="fa  fa-print"></i>&nbsp;<strong>PRINT SLIPS</strong>
                    </a>
                    <a class="btn btn-danger btn-sm"  title="Import/Export Leads" href="javascript:history.back()">
                        <i class="fa  fa-backward"></i>&nbsp;<strong>CANCEL INVOICES</strong>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class="box">
                <div class="box-body">
                    <table class="table table-hover table-no-border" id="leads-table">
                        <thead>
                        <tr class="bg-info">
                            <th>Date</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Credit</th>
                            <th>Debit</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody id="myTable">
                        <tr>

                            <td>{{date('dS M y', strtotime($start_date))}}</td>
                            <td>{{ucfirst(trans($data[0]->name))}}</td>
                            <td>Opening Balance</td>
                            <td> - </td>
                            <td> - </td>
                            <td>{{$balance = $pervious_closing}}{{ucfirst(trans($data[0]->symbol))}}</td>
                        </tr>
                        @php($credit = 0)
                        @php($debit = 0)
                        @foreach($data as $key=>$pro)
                            <tr >
                                @if($pro->name != "")
                                <td>{{date('dS M y', strtotime($pro->date_ledger))}}</td>
                                <td>{{ucfirst(trans($pro->name))}}</td>
                                <td>{{$pro->description}}</td>
                                <td>@php($credit += $pro->credit_qty) {{$pro->credit_qty}}{{ucfirst(trans($pro->symbol))}} </td>
                                <td>@php($debit += $pro->debit_qty) {{$pro->debit_qty}}{{$pro->debit_qty?ucfirst(trans($pro->symbol)):'-'}} </td>
                                <td>{{$balance = $balance + $pro->credit_qty - $pro->debit_qty}}{{ucfirst(trans($data[0]->symbol))}}</td>
                                @else
                                    <td colspan="6"> -- </td>
                                @endif
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot class="bg-primary">
                            <tr>
                                <td colspan="3">Total Calculated</td>
                                <td>{{$credit}} {{$credit?ucfirst(trans($data[0]->symbol)):'-'}} </td>
                                <td>{{$debit}} {{$debit?ucfirst(trans($data[0]->symbol)):'-'}} </td>
                                <td>{{$balance}} {{ucfirst(trans($data[0]->symbol))}}</td>
                            </tr>


                        </tfoot>
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection



@section('body_bottom')

@endsection

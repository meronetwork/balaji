<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>

    <!-- block from searh engines -->
    <meta name="robots" content="noindex">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Set a meta reference to the CSRF token for use in AJAX request -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons 4.7.0 -->
    <link href="{{ asset("/bower_components/admin-lte/font-awesome/css/all.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.1 -->
    <link href="{{ asset("/bower_components/admin-lte/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />

    <!-- Application CSS-->


    <style>
        .box-comment {
            margin-bottom: 5px;
            padding-bottom: 5px;
            border-bottom: 1px solid #eee;
        }

        .box-comment img {
            float: left;
            margin-right: 10px;
        }

        .username {
            font-weight: bold;
        }

        .comment-text span {
            display: block;
        }

        #bg-text {
            color: lightgrey;
            position: absolute;
            left: 0;
            right: 0;
            top: 40%;
            text-align: center;
            margin: auto;
            opacity: 0.5;
            z-index: 1000;
            font-size: 80px;
            transform: rotate(330deg);
            -webkit-transform: rotate(330deg);
        }
        @media print {
            .pagebreak { page-break-before: always; } /* page-break-after works, as well */
        }
    </style>


</head>


<body onload="window.print();" cz-shortcut-listen="true" class="skin-blue sidebar-mini">

<div class='wrapper'>
    <div class="pagebreak"></div>
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <div class="col-xs-3">
                        <img src="{{env('APP_URL')}}{{ '/org/'.$organization->logo }}" style="max-width: 200px;">

                    </div>
                    <div class="col-xs-9">
                        <span class="pull-right">
                            <span>Final Good Statement
                            </span>
                        </span>
                    </div>
                    <hr>
                </h2>

            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-12 ">

                <address style="text-align: center;">
                    <h3> <strong>{{ env('APP_COMPANY') }} </strong></h3>
                </address>

            </div>
            <hr>
        </div>
        <hr />
        <!-- Table row -->
        <div class="row">
            <table class="table table-hover table-no-border" id="leads-table">
                <thead>
                <tr class="bg-info">
                    <th>Date</th>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Credit</th>
                    <th>Debit</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody id="myTable">
                <tr>

                    <td>{{date('dS M y', strtotime($start_date))}}</td>
                    <td>{{ucfirst(trans($data[0]->name))}}</td>
                    <td>Opening Balance</td>
                    <td> - </td>
                    <td> - </td>
                    <td>{{$balance = $pervious_closing}}{{ucfirst(trans($data[0]->symbol))}}</td>
                </tr>
                @php($credit = 0)
                @php($debit = 0)
                @foreach($data as $key=>$pro)
                    <tr >

                        @if($pro->name != "")
                            <td>{{date('dS M y', strtotime($pro->date_ledger))}}</td>
                            <td>{{ucfirst(trans($pro->name))}}</td>
                            <td>{{$pro->description}}</td>
                            <td>@php($credit += $pro->credit_qty) {{$pro->credit_qty}}{{ucfirst(trans($pro->symbol))}} </td>
                            <td>@php($debit += $pro->debit_qty) {{$pro->debit_qty}}{{$pro->debit_qty?ucfirst(trans($pro->symbol)):'-'}} </td>
                            <td>{{$balance = $balance + $pro->credit_qty - $pro->debit_qty}}{{ucfirst(trans($data[0]->symbol))}}</td>
                        @else
                            <td colspan="6"> -- </td>
                        @endif
                    </tr>
                @endforeach

                </tbody>
                <tfoot class="bg-primary">
                <tr>
                    <td colspan="3">Total Calculated</td>
                    <td>{{$credit}} {{$credit?ucfirst(trans($data[0]->symbol)):'-'}} </td>
                    <td>{{$debit}} {{$debit?ucfirst(trans($data[0]->symbol)):'-'}} </td>
                    <td>{{$balance}} {{ucfirst(trans($data[0]->symbol))}}</td>
                </tr>


                </tfoot>
            </table>

            <!-- /.col -->
        </div>

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">


            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>

                        <tr>
                            <th colspan="2">
                                <br>______________________<br>
                                <span style="text-indent: 15px;">
                                &nbsp;&nbsp;Signature</span>
                            </th>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <br>If you find errors or desire certain changes, please contact us.

    </section>
</div><!-- /.col -->

</body>

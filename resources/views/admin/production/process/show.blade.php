@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Issue Process
                <small>View Processes</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
<div class="panel panel-custom">
 	<div class="panel-heading">
	   <div class='row'>
       		<div class='col-md-12'>
            <b><font size="4">Issued Slips</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.production.production-print',$process->id) }}">
            <i class="fa  fa-print"></i>&nbsp;<strong>PRINT SLIPS</strong>
            </a>
             <a class="btn btn-danger btn-sm"  title="Import/Export Leads" href="javascript:history.back()">
                            <i class="fa  fa-backward"></i>&nbsp;<strong>CANCEL INVOICES</strong>
            </a>
            </div>
        </div>
	</div>
</div>

<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Product section</th>
         <th>Date</th>
    </tr>
</thead>
<tbody>
	<tr>
		<td >
			<input type="checkbox" name="event_id" value="{{$pu->id}}">
        </td>
        <td>{{$process->id}}</td>
        <td>{{ucfirst(trans($process->section_name))}}</td>
        <td>{{ date('dS M y', strtotime($process->date)) }}</td>

    </tr>
</tbody>
</table>
    <div class="row">
        @if($process->description)
     <div class="col-md-12">
        <label for="inputEmail3" class="control-label">
       Description
        </label>
          <textarea class="form-control" name="description" id="general_remarks" placeholder="Write Description" readonly="">{{$process->description}}</textarea>
        </div>
        @endif
    </div>
<div class="panel-heading">
       <div class='row'>
            <div class='col-md-12'>
            <b><font size="4">New Raw Material</font></b>
        </div>
    </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>#</th>
        <th>Raw Material Name</th>
        <th>Quantity</th>
    </tr>
</thead>
<tbody>
    @foreach($_details1 as $key=>$d)
    <tr>
        <td >
            {{$key + 1 }}
        </td>
        <td>{{ucfirst(trans($d->name))}}</td>
        <td>{{$d->quantity}}&nbsp;{{ucfirst(trans($d->symbol))}}</td>

    </tr>
    @endforeach

</tbody>
</table>

<div class="panel-heading">
       <div class='row'>
            <div class='col-md-12'>
            <b><font size="4">Raw Material Used</font></b>
        </div>
    </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>#</th>
        <th>Raw Meterial Used</th>
        <th>Quantity</th>
    </tr>
</thead>
<tbody>
    @foreach($_details2 as $key=>$d)
    <tr>
        <td >
            {{$key + 1 }}
        </td>
        <td>{{ucfirst(trans($d->name))}}</td>
        <td>{{$d->quantity}}&nbsp;{{ucfirst(trans($d->symbol))}}</td>

    </tr>
    @endforeach

</tbody>
</table>
</div>
@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
    <script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Process</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
<form method="post" action="{{route('admin.production.process-edit',$edit->id)}}" class="issue-slips form">
  {{csrf_field()}}
<div class="panel panel-custom">
 <div class="panel-heading">

  <div  align="left" style="margin-left: 15px !important;font-size: 16px"><span > Edit </span></div>
  <div align="center" style="color:red" id="slipserror"></div>
  <br>
  <div class="row" style="margin-left: 20px">
   <div class="col-md-4">

<div class="form-group">
<label for="date" class="col-sm-3 control-label">Date</label>
<div class="col-sm-6">
    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input type="text" class="form-control pull-right datepicker date-toggle-nep-eng" name="date" value="{{ isset($edit) ? $edit->date : date('Y-m-d')}}" id="date" required="">
    </div>
</div>
</div>
</div>


<div class="col-md-4" >
<div class="form-group" id="border-none">
<label
    class="col-sm-6  control-label">Section name</label>
<div class="col-sm-6">
    <select name="section_id"
            class="form-control">
        @foreach($product_section as $key=>$ps)
        <option value="{{$ps->id}}" @if($ps->id == $edit->section_id) selected @endif >{{$ps->section_name}}</option>
        @endforeach
    </select>
</div>
</div>
</div>
<div class="col-md-4" >
<div class="form-group" id="border-none">
<label
    class="col-sm-6  control-label">Process type</label>
<div class="col-sm-6">
    <select name="process_type"
            class="form-control">
        @foreach(['process','final'] as $p)
        <option value="{{$p}}" @if($p == $edit->process_type) selected @endif >{{$p}}</option>
        @endforeach
    </select>
</div>
</div>
</div>
</div>
<br>

<div class="row">
     <div class="col-md-12">
    <div  align="left" style="margin-left: 15px !important;font-size: 16px"><span ><b> New Raw Material</b></span></div>
         <a href="javascript::void(1)" class="btn btn-default btn-xs" id="addMore1" style="float: right;">
          <i class="fa fa-plus"></i> <span>Add new </span>
        </a>
  <table class="table">


    <tbody>
        <tr class="multipleDiv1">
          @foreach($edit_new_raw as $key=>$edit_raw)
         <tr>
            <td>
              <select class="form-control select2 product_id" name="product_id[]" required="required">
              <option value="">Select Product</option>
                  @foreach($final_products as $key => $pk)
                      <option value="{{ $pk->id }}" @if($edit_raw->product_id == $pk->id) selected @endif> {{ $pk->name }}</option>
                  @endforeach
              </select>
            </td>

            <td>
                <div class="input-group ">
                    <input type="number" class="form-control quantity" name="quantity[]" placeholder="quantity" required="required" value="{{$edit_raw->credit_qty}}">
                    <div class="input-group-addon">
                    </div>
                </div>

            </td>
            <td>
            <a href="javascript::void(1);" style="width: 10%;">
                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
              </a>
            </td>
        </tr>
        </tr>
        @endforeach
    </tbody>

</table>

</div>
    </div>


    <div class="row">
     <div class="col-md-12">
    <div  align="left" style="margin-left: 15px !important;font-size: 16px"><span ><b> Raw Materials Used </b></span></div>

         <a href="javascript::void(1)" class="btn btn-default btn-xs" id="addMore" style="float: right;">
          <i class="fa fa-plus"></i> <span>Add new </span>
                           </a>
  <table class="table">


    <tbody>
        <tr class="multipleDiv">
          @foreach($edit_used_raw as $key=>$used_raw)

    <tr>
        <td>
          <select class="form-control select2 fact_product_id" name="rproduct_id[]" required="required">
          <option value="">Select or Type Raw Material</option>
              @foreach($products as $key => $pk)
                  <option value="{{ $pk->id }}" @if($used_raw->product_id == $pk->id) selected @endif >{{ $pk->name }}</option>
              @endforeach

          </select>
        </td>
        <td>
            <div class="input-group " >
                <input type="number" class="form-control quantity" name="rquantity[]" placeholder="quantity" required="required" value="{{$used_raw->debit_qty}}">
                <div class="input-group-addon">
                </div>

            </div>
        </td>
        <td>
            <a href="javascript::void(1);" style="width: 10%;">
                <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
            </a>

        </td>
    </tr>
    </tr>

        @endforeach
    </tbody>

</table>

</div>
    </div>

  <div class="row">
     <div class="col-md-12">
        <label for="inputEmail3" class="control-label">
       Description
        </label>
          <textarea class="form-control" name="description" id="general_remarks" placeholder="Write Description">{{$edit->description}}</textarea>
        </div>
    </div>
</div>
 <div class="row" style="margin-left: 1px">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update process</button>
        </div>

    </div>
</div>
</div>

</form>
 <div id="orderFields" style="display: none;">
  <table class="table">
      <tbody id="more-tr">
          <tr>
              <td>
                <select class="form-control select2 fact_product_id" name="rproduct_id[]" required="required">
              <option value="">Select or Type Raw Material</option>
                    @foreach($products as $key => $pk)
                        <option value="{{ $pk->id }}" >{{ $pk->name }}</option>
                    @endforeach
                </select>
              </td>
              <td>
                  <div class="input-group ">
                      <input type="number" class="form-control quantity" name="rquantity[]" placeholder="quantity" required="required" step="any">
                      <div class="input-group-addon">
                      </div>
                  </div>
              </td>
              <td>
                  <a href="javascript::void(1);" style="width: 10%;">
                      <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                  </a>
              </td>
          </tr>
          </tbody>
        </table>
        </div>


 <div id="orderFields1" style="display: none;">
<table class="table">
    <tbody id="more-tr1">
        <tr>
            <td>
              <select class="form-control select2 product_id" name="product_id[]" required="required">

                      <option value="">Select Product</option>
                  @foreach($final_products as $key => $pk)
                      <option value="{{ $pk->id }}">{{ $pk->name }}</option>
                  @endforeach

              </select>
            </td>

            <td>
                <div class="input-group ">
                    <input type="number" class="form-control quantity" name="quantity[]" placeholder="quantity" required="required">
                    <div class="input-group-addon">
                    </div>
                </div>
            </td>
            <td>

                <a href="javascript::void(1);" style="width: 10%;">
                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                </a>
            </td>
        </tr>
    </tbody>
</table>
    </div>

@endsection
@section('body_bottom')
    @include('partials._body_bottom_submit_bug_edit_form_js')
    @include('partials._date-toggle')
    <script>
        const dateRange = {
            <?php $currentFiscalyear = FinanceHelper::cur_fisc_yr();?>
            minDate: `{{ $currentFiscalyear->start_date }}`,
            maxDate: `{{ $currentFiscalyear->end_date }}`
        }
        $('.date-toggle-nep-eng').nepalidatetoggle();
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
        $(function() {
            $('.datepicker').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD',
                sideBySide: true,
                allowInputToggle: true,
                minDate: dateRange.minDate,
                maxDate: dateRange.maxDate,
            });
        });
$("#addMore").on("click", function () {
     //$($('#orderFields').html()).insertBefore(".multipleDiv");
     $(".multipleDiv").after($('#orderFields #more-tr').html());
});
$("#addMore1").on("click", function () {
     //$($('#orderFields').html()).insertBefore(".multipleDiv");
     $(".multipleDiv1").after($('#orderFields1 #more-tr1').html());
});
 $(document).on('click', '.remove-this', function () {
    $(this).parent().parent().parent().remove();
});
 $(document).ready(function(){
    $('.form-control.select2.fact_product_id').each(function(){
        console.log('url',);
        let id = $(this).val();
        $(this).closest('tr').find('.form-control.total').val("");
        $.get('/admin/production/ajaxget/'+id,(result)=>{
            let obj = JSON.parse(result);
            $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
        });
      });
    });
 $(document).on('change','.form-control.select2.fact_product_id',function(){
  let id = $(this).val();
  $(this).closest('tr').find('.form-control.total').val("");
  $.get('/admin/production/ajaxget/'+id,(result)=>{
      let obj = JSON.parse(result);
      $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
  });

 });
 $('.form-control.select2.product_id').each(function(){
    let id = $(this).val();
    $(this).closest('tr').find('.form-control.total').val("");
    $.get('/admin/production/ajaxget/'+id,(result)=>{
    let obj = JSON.parse(result);
    $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
    });
});
 $(document).on('change','.form-control.select2.product_id',function(){
  let id = $(this).val();
  $(this).closest('tr').find('.form-control.total').val("");
  $.get('/admin/production/ajaxget/'+id,(result)=>{
  let obj = JSON.parse(result);
  $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
  });

 });

 $(document).on('keyup','.form-control.quantity',function(){
  let quantity = $(this).val();
  let total = $(this).closest('tr').find('.form-control.instock').val();
  let calc = Number(total) - Number(quantity);
  $(this).closest('tr').find('.form-control.total').val(calc);
 });
 $(document).on('submit','.issue-slips.form',function(){
    var error = false;
    $('.form-control.total').each(function(){
        if($(this).val() < 0 ){
            return error = true;
        }
    });
    if(error){
        $('#slipserror').html("<h4>Entered quantity exceeded from product instock</h4>");
        return false;
    }

  if($('.form-control.quantity').val()){
    return true;
  }
  else{
    $('#slipserror').html("<h4>Please select at least one product</h4>");
    return false;
  }
 })
</script>
@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Statistics
                <small>Costs by manufacturing orders</small>
            </h1>
            <p> 

 </p>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
<div class="panel panel-custom box">
 	<div class="box-header with-border">
	   <div class='row'>
       		<div class='col-md-12'>
                    <a href="#" class="filter-by-date" title="Filter"><i class="fa  fa-file"></i></a>&nbsp;
           
            

            <div class="col-md-3" style="float: right; ">
                <form method="get" action="/admin/production/search-warehouse/" id="searchform">
                    <div class="input-group ">
                        <input type="text" placeholder="Search......"   class="form-control" style="height: 30px" name="slip" required="" value="{{request()->slip?request()->slip:''}}">
                        <div class="input-group-addon">
                            <a href="#" type="submit" onclick="document.getElementById('searchform').submit()"><i class="fa fa-search"></i></a>
                        </div>
            </div>
                    </form>
        </div>

        </div>
	</div>
</div>
<div class="filter_form" style="display: none;margin-top: 10px" >
<form method="get" action="/admin/production/filter-warehouse/">
        <div class="box-header with-border">
      <div class="col-md-4">

<div class="form-group">
<label for="date" class="col-sm-3 control-label">From Date</label>
<div class="col-sm-6">
<div class="input-group">
    <input required="" type="text" class="form-control date_in"  name="from_date" id="from_date">
    <div class="input-group-addon">
        <a href="#"><i class="fa fa-calendar"></i></a>
    </div>
</div>
</div>
</div>

</div>
      <div class="col-md-4">

<div class="form-group">
<label for="date" class="col-sm-3 control-label">To Date</label>
<div class="col-sm-6">
<div class="input-group">
    <input required="" type="text" class="form-control date_in" value="{{ isset($selecteddate) ? $selecteddate : '' }}" name="to_date" id="to_date">
    <div class="input-group-addon">
        <a href="#"><i class="fa fa-calendar"></i></a>
    </div>
</div>
</div>
</div>

</div>

      <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Filter</button>
        </div>
</div>
    </form>
</div>

<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr class="bg-danger">

        <th>Number</th>
        <th>Name</th>
        <th> Part No </th>
        <th>Qty</th>
        <th>Total Cost</th>
        <th>Cost of Materials</th>
        <th>Overhead Cost</th>
        <th>Labour Cost </th>
        <th>Cost Overrun</th>
        <th>Cost overrun per item</th>
    </tr>
</thead>
<tbody id="myTable">
	@foreach($slips as $key=>$is)

      @php 
       $costInfo = $is->costInfo();
       $materialCost = $costInfo['materialCost'];
       $overheadCost  = $costInfo['overheadCost'];
       $labourCost= $costInfo['labourCost'];
       $totalCost = $costInfo['totalCost'];
       $perUnitCost = $costInfo['perUnitCost'];
       $overrunCost = $is->overruncost($costInfo);
    @endphp 
        <tr style="font-size: 16.5px">
            <td>M00{{ $is->id }}</td>
            <td>{{ $is->product->name }}</td>
            <td>P0{{ $is->product->id }}</td>
            <td>{{ $is->qty }} {{ $is->product->product_units->symbol }}</td>
            <td>Rs {{ $totalCost }}</td>
            <td>Rs {{ $materialCost }}</td>
            <td>Rs {{ $overheadCost }}</td>
            <td>Rs {{ $labourCost }}</td>
            <td>Rs {{ $overrunCost['overruncost'] }}</td>
            <td>Rs {{ $overrunCost['perQty'] }}</td>
           
        </tr>
    @endforeach
</tbody>
</table>
<div style="text-align: center;"> {!! $slips->render() !!} </div>
</div>
@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<!-- Timepicker -->
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/timepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/timepicker.js") }}" type="text/javascript"></script>

<!-- SELECT2-->
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2.css") }}">
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2-bootstrap.css") }}">
<script src="{{ asset("/bower_components/admin-lte/select2/js/select2.js") }}"></script>
<script>
    $(function() {
        $('#from_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD',
            //sideBySide: true
        })
            $('#to_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD',
            //sideBySide: true
        })


    });


$('.filter-by-date').on('click',function(){
let ishidden = $('.filter_form').css('display');
if(ishidden == 'none'){
    $('.filter_form').css('display','block');
}
else{
     $('.filter_form').css('display','none');
}
});
</script>
@endsection
@endsection

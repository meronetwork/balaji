@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
               Manufacturing Order {{sprintf('%03d',$warehouse->id)}} </h3>
              
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="box box-default ">
    
    <div class="box-header with-border">
        <h3 class="box-title">Manufacturing Order {{sprintf('%03d',$warehouse->id)}} </h3>
        <span class="pull-right">
            <a class="btn   btn-primary" href="/admin/production/issue-warehouse">Back</a>
            <button class="btn   bg-black">Delete</button>
            <button class="btn   btn-default">PDF Download</button>
            <a class="btn   btn-info" href="{{ route('admin.production.myplan',$warehouse->id) }}">Go to production</a>
        </span>
    </div>

    <div class="box-body">
    <div class="row" style="font-size: 14px">
        <div class="col-md-6">
            <dl class="dl-horizontal">
                    <dt>Product</dt>
                    <dd style="font-size: 16.5px">{{ $warehouse->product->name }}
                       <a href="{{ route('admin.production.lot_print',$warehouse->id) }}"> <i class="fa fa-print"></i>
                       </a>
                    </dd>
                    <dt>Quantity</dt>
                    <dd style="font-size: 16.5px">{{ $warehouse->qty }} {{$warehouse->product->product_units->symbol}}</dd>
                   
                    <dt>Due Date</dt>
                    <dd>{{ $warehouse->date }}</dd>
                    <dt>Assigned To</dt>
                    <dd>{{ $warehouse->user->first_name }} {{ $warehouse->user->last_name }}</dd>
                    <dt>Target Lot</dt>
                    <dd>B{{  sprintf('%03d',$warehouse->id) }}<a href="{{ route('admin.production.target_lot_print',$warehouse->id) }}">
                        <i class="fa fa-print"></i></a>
                    </dd>
            </dl>
        </div>
                <div class="col-md-6">
            <dl class="dl-horizontal">
                    <dt>Created</dt>
                    <dd>{{ date('d/m/Y',strtotime( $warehouse->created_at ))}}</dd>
                    <dt>Status</dt>
                    <dd>{{ $warehouse->status }}</dd>
                    <dt>Start</dt>
                    <dd>{{ strtotime($warehouse->start) ? date('d/m/Y H:i',strtotime($warehouse->start)): '--'  }}</dd>
                    <dt>End</dt>
                    <dd>{{ strtotime($warehouse->finish) ? date('d/m/Y H:i',strtotime($warehouse->finish)): '--'  }}</dd>
                    @php 
                       $costInfo = $warehouse->costInfo();
                       $materialCost = $costInfo['materialCost'];
                       $overheadCost  = $costInfo['overheadCost'];
                       $labourCost= $costInfo['labourCost'];
                       $totalCost = $costInfo['totalCost'];
                       $perUnitCost = $costInfo['perUnitCost'];
                    @endphp 
                    <dt>Material Cost</dt>
                    <dd>Rs {{ number_format($materialCost,2)  }}</dd>

                    <dt>Overhead Cost</dt>
                    <dd>Rs {{  number_format($overheadCost,2) }}</dd>

                    <dt>Labor Cost</dt>
                    <dd>Rs {{  number_format($labourCost,2)  }}</dd>

                    <dt>Total Cost</dt>
                    <dd style="font-size: 16.5px">Rs {{  number_format($totalCost,2)  }}</dd>

                    <dt>Per Unit Cost</dt>
                    <dd>Rs {{  number_format($perUnitCost,2)  }}</dd>
                    
            </dl>
        </div>


</div>
</div>
</div>
<form method="POST" action="{{route('admin.production.issue-warehouse-updateProd',$warehouse->id) }}" id='UpdateStocks'>
    @csrf
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Raw Material Details</h3>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="bg-info">
                    <th>Raw Product</th>
                    <th>Price</th>
                    <th>Qty Booked</th>
                    <th>Total</th>
                    <th class='text-center'>Return Stock </th>
                </tr>
            </thead>
            <tbody>
                @foreach($bomDetails as $key=>$value)
                @php $tQty = $value->qty * $warehouse->qty;   @endphp 
                <tr id='items-{{ $value->id }}'>
                    <td>{{ $value->product->name }}

                        <input type="hidden" name="product_id[]" value="{{ $value->raw_product_id }}" class="product_id">
                    </td>
                    <td>{{ env('APP_CURRENCY') }} {{ $value->product->price }}</td>
                    <td title="Routing Qty {{ $value->qty }}">{{  $tQty }} {{$value->product->product_units->symbol}} / 
                        <small>{{ StockHelper::getRemaingStocks($value->raw_product_id) }}</small>
                        <input type="hidden" name="qty[]" value="{{ $value->qty }}" class="qty">
                    </td>
                    <td>{{ env('APP_CURRENCY') }} {{ $value->product->price * $tQty }}
                        
                    </td>
                    <td class='text-center'> 
                        <a href="javascript::void()" onclick="removeItem('{{ $value->id }}')">
                            <i class="fa  fa-rotate-left (alias)"></i>
                        </a>
                    </td>   
                </tr>
                @endforeach
            </tbody>
        </table>


    </div>
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
         <button class="btn btn-danger" id='addMorePro' type="button">Add More Products</button> 

        </h3>
        <button class="btn btn-success" id='addMorePro' style="float: right;">Save Product</button> 
    </div>
    <div class="box-body">
        <table class="table">
            <thead>
                <tr class="bg-success">
                    <th>Products</th>
                    <th>Qty</th>
                    <th>Booked Qty</th>
                </tr>
            </thead>
            <tbody>
                <tr id='addMoreProducts'></tr>

            </tbody>
        </table>


    </div>
</div>
</form>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Operations</h3>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="bg-danger">
                    <th>Step</th>
                    <th>Operation</th>
                    <th>Description</th>
                    <th>Overhead Cost</th>
                    <th>Labor Cost</th>
                    <th>Worker</th>
                </tr>
            </thead>
            <tbody>
                @foreach($factRouteDetails as $key=>$value)
                    <tr>
                        <td>{{ $value->process_order }}</td>
                        <td>{{ $value->section->section_name }}</td>
                        <td>{{ $value->description }}</td>
                        <td>{{ env("APP_CURRENCY") }} {{ $value->overhead_cost }}</td>
                        <td>{{ env("APP_CURRENCY") }} {{ $value->labour_cost }}</td>
                        <td>{{ $value->user->first_name }} {{ $value->user->last_name }}</td>

                    </tr>    
                @endforeach
            </tbody>
        </table>


    </div>
</div>







<div id='moreDiv' style="display: none">

<table class="table">
<tbody id='more-custom-tr'>
    <tr>
        <td>
            {!! Form::select('product_id[]',$products,null,['class'=>'form-control product_id','placeholder'=>'Select Products','required'=>'true'])  !!}
        </td>
        <td>
            <input type="number" name="qty[]" step="any" class="qty form-control input-sm" required="">
        </td>
        <td> 
            <input type="number" name="bqty[]" step="any" readonly=""  class="booked_qty form-control input-sm" style="width: 80%"> 
           <a href="javascript::void(1);" style="width: 10%;">
            <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
        </a>
        </td>
    </tr>
</tbody>
</table>
</div>
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>

<script type="text/javascript">
    
    $('#addMorePro').click(function(){

        $('#addMoreProducts').after($('#moreDiv .table #more-custom-tr').html());
         $("#addMoreProducts").next('tr').find('select').select2({
            width: '100%'
        });

    });

  $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();

    });


$(document).on('keyup','input.qty',function(){

    let parent = $(this).parent().parent();
    tQty = Number($(this).val());
    parent.find('.booked_qty').val( tQty * {{ $warehouse->qty }} );



});

function removeItem(id){

    if(!confirm("Are you Sure")){
        return;

    }
    $(`#items-${id}`).remove();
}
</script>
@endsection

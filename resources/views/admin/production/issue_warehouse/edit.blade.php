@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
    <script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Issue Ware House
                <small>Edit Issue Ware House</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
<form method="post" action="{{route('admin.production.issue-warehouse-edit',$edit->id)}}" class="issue-slips form">
	{{csrf_field()}}
<div class="panel panel-custom">
 <div class="panel-heading">

 	<div  align="left" style="margin-left: 15px !important;font-size: 16px"><span > Edit </span></div>
 	<div align="center" style="color:red" id="slipserror"></div>
 	<br>
 	<div class="row">
   <div class="col-md-4">
    <div class="form-group">
        <label for="date" class="col-sm-4 control-label">Date</label>
        <div class="col-sm-8">
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right datepicker date-toggle-nep-eng" name="date" value="{{isset($edit) ? $edit->date : date('Y-m-d')}}" id="date" required="">
            </div>
        </div>
    </div>
   </div>

<div class="col-md-4">
    <div class="form-group">
        <label class="col-sm-6 control-label">Location</label>
        <div class="col-sm-6">
            <select name="product_location_id"
                    class="form-control">
                @foreach($product_location as $key=>$pl)
                <option value="{{$pl->id}}" @if($edit->id == $pl->id)selected @endif>{{$pl->location_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>



<div class="col-md-4">
    <div class="form-group" id="border-none">
        <label class="col-sm-6  control-label">Section name</label>
        <div class="col-sm-6">
            <select name="product_section_id"
                    class="form-control">
                @foreach($product_section as $key=>$ps)
                <option value="{{$ps->id}}" >{{$ps->section_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

</div>
<br>

<div class="row">
	 <div class="col-md-12">
	 	 <a href="javascript::void(1)" class="btn btn-primary btn-xs" id="addMore" style="float: right;">
          <i class="fa fa-plus"></i> <span>Add Products Item</span>
                           </a>
  <table class="table">


    <tbody>
        <tr class="multipleDiv">
            @foreach($_details as $detail)
      <tr>
  <td>
    <select class="form-control select2 product_id" name="product_id[]" required="required">

            <option value="">Select Product</option>
        @foreach($products as $key => $pk)
            <option value="{{ $pk->id }}" @if($detail->product_id ==  $pk->id)selected @endif>{{ $pk->name }}</option>
        @endforeach

    </select>
  </td>

  <td>
      <input type="hidden" name="units[]" value="0" class="input-units">
      <input type="hidden" name="old_quantity[]" value="{{$detail->quantity}}">
      <input type="number" class="form-control quantity" name="quantity[]" placeholder="quantity" required="required" value="{{$detail->quantity}}">

  </td>

  <td>

 <div class="input-group ">
<input required="" type="number" class="form-control instock"  readonly="readonly" >
  <div class="input-group-addon">

  </div>
  </div>
  </td>

  <td>
      <input type="number" class="form-control total" placeholder="Remaining Stock"  readonly="readonly" style="float:left; width:80%;">
      <a href="javascript::void(1);" style="width: 10%;">
          <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
      </a>
  </td>
</tr>
@endforeach
        </tr>
    </tbody>

</table>

</div>
	</div>

	<div class="row">
     <div class="col-md-12">
        <label for="inputEmail3" class="control-label">
       Description
        </label>
          <textarea class="form-control" name="description" id="general_remarks" placeholder="Write Description">{{$edit->description}}</textarea>
        </div>
    </div>
</div>
 <div class="row" style="margin-left: 1px">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update Issue</button>
        </div>

    </div>
</div>
</div>

</form>

 <div id="orderFields" style="display: none;">
                    <table class="table">
                        <tbody id="more-tr">
                            <tr>
                                <td>
                                  <select class="form-control select2 product_id" name="product_id[]" required="required">

                                          <option value="">Select Product</option>
                                      @foreach($products as $key => $pk)
                                          <option value="{{ $pk->id }}">{{ $pk->name }}</option>
                                      @endforeach

                                  </select>
                                </td>

                                <td>
                                  <input type="hidden" name="old_quantity[]" value="0">
                                    <input type="number" class="form-control quantity" name="quantity[]" placeholder="quantity" required="required" min="1">

                                </td>

                                <td>

                               <div class="input-group ">
                      		<input required="" type="number" class="form-control instock"  readonly="readonly" >
                                <div class="input-group-addon">

                                </div>
                    			</div>
                                </td>

                                <td>
                                    <input type="number" class="form-control total" placeholder="Remaining Stock"  readonly="readonly" style="float:left; width:80%;">
                                    <a href="javascript::void(1);" style="width: 10%;">
                                        <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

@endsection
@section('body_bottom')
    @include('partials._body_bottom_submit_bug_edit_form_js')
    @include('partials._date-toggle')
    <script>
        const dateRange = {
            <?php $currentFiscalyear = FinanceHelper::cur_fisc_yr();?>
            minDate: `{{ $currentFiscalyear->start_date }}`,
            maxDate: `{{ $currentFiscalyear->end_date }}`
        }
        $('.date-toggle-nep-eng').nepalidatetoggle();
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
        $(function() {
            $('.datepicker').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD',
                sideBySide: true,
                allowInputToggle: true,
                minDate: dateRange.minDate,
                maxDate: dateRange.maxDate,
            });
        });
        $("#addMore").on("click", function () {
             //$($('#orderFields').html()).insertBefore(".multipleDiv");
             $(".multipleDiv").after($('#orderFields #more-tr').html());
        });

        $(document).ready(function(){
            var issue_id = window.location.pathname.split("/").pop();

            $('.form-control.select2.product_id').each(function(){
                let id = $(this).val();
                $(this).closest('tr').find('.form-control.total').val("");
                $.get('/admin/production/ajaxget/'+id+'?issue_id='+issue_id,(result)=>{
                    let obj = JSON.parse(result);
                    if(obj[0].total == null)
                      obj[0].total = 0;
                    $(this).closest('tr').find('.form-control.instock').val(obj[0].total);
                    $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
                    $(this).closest('tr').find('.input-units').val(obj[0].product_units_id);
                    let quantity = $(this).closest('tr').find('.form-control.quantity').val();
                    if(quantity){
                        let calc = Number(obj[0].total) - Number(quantity);
                        $(this).closest('tr').find('.form-control.total').val(calc);
                    }
                });
            });
            $(document).on('change','.form-control.select2.product_id',function(){
                let id = $(this).val();
                $(this).closest('tr').find('.form-control.total').val("");
                $.get('/admin/production/ajaxget/'+id+'?issue_id='+issue_id,(result)=>{
                    let obj = JSON.parse(result);
                    if(obj[0].total == null)
                        obj[0].total = 0;
                    $(this).closest('tr').find('.form-control.instock').val(obj[0].total);
                    $(this).closest('tr').find('.input-group-addon').html(obj[0].symbol);
                    $(this).closest('tr').find('.input-units').val(obj[0].product_units_id);
                    let quantity = $(this).closest('tr').find('.form-control.quantity').val();
                    if(quantity){
                        let calc = Number(obj[0].total) - Number(quantity);
                        $(this).closest('tr').find('.form-control.total').val(calc);
                    }
                });
            });
        });

        $(document).on('click', '.remove-this', function () {
            $(this).parent().parent().parent().remove();
        });



        $(document).on('keyup','.form-control.quantity',function(){
            let quantity = $(this).val();
            let total = $(this).closest('tr').find('.form-control.instock').val();
            let calc = Number(total) - Number(quantity);
            $(this).closest('tr').find('.form-control.total').val(calc);
         });
         $('.form-control.select2.product_id').load()
         $(document).on('submit','.issue-slips.form',function(){
     var error = false;
    $('.form-control.total').each(function(){
        if($(this).val() < 0 ){
            return error = true;
        }
    });
    if(error){
        $('#slipserror').html("<h4>Entered quantity exceeded from product instock</h4>");
        return false;
    }
 	if($('.form-control.quantity').val()){
 		return true;
 	}
 	else{
 		$('#slipserror').html("<h4>Please select at least one product</h4>");
 		return false;
 	}
 })
</script>
@endsection

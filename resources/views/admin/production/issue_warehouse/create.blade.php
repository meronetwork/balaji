@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
    <script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Create a recipe order
                <small>ISSUE PRODUCT FROM WAREHOUSE </small>
            </h1>
            <p> Add Required information to issue product </p>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<form method="post" action="{{route('admin.production.issue-warehouse-create')}}" class="issue-slips form">
	{{csrf_field()}}
<div class="panel panel-custom">
 <div class="panel-heading">

 	<div  align="left" style="margin-left: 15px !important;font-size: 16px"><span > ADD </span></div>
 	<div align="center" style="color:red" id="slipserror"></div>
 	<br>
<form method="POST" action="{{ route('admin.production.issue-warehouse-create') }}">
@csrf
 <div class="row">

    <div class="col-md-4">
<div class="form-group">
<label class="col-sm-4 control-label">Product</label>
<div class="col-sm-8">
    <select name="product_id" class="form-control" id='product_id'>
        <option value="">Select Product</option>
        @foreach($final_products as $key=>$pl)
            <option value="{{$pl->id}}" >{{$pl->name}}</option>
        @endforeach
    </select>
</div>
</div>
</div>

<div class="col-md-4">
<div class="form-group">
<label for="date" class="col-sm-4 control-label">Quantity</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-check-circle"></i>
            </div>
            <input type="number" class="form-control " name="qty" value="" id="quantity" required="">
        </div>
    </div>
</div>

</div>

<div class="col-md-4">
<div class="form-group">
<label for="date" class="col-sm-4 control-label">Due Date</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker date-toggle-nep-eng" name="date" value="{{date('Y-m-d')}}" id="date" required="">
        </div>
    </div>
</div>

</div>

</div>
<br/>
<div class="row">


<div class="col-md-4">
<div class="form-group">
<label for="date" class="col-sm-4 control-label">Start Date</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker date-toggle-nep-eng" name="start" value="{{date('Y-m-d')}}" id="start_date" required="">
        </div>
    </div>
</div>

</div>

<div class="col-md-4">
<div class="form-group">
<label for="date" class="col-sm-4 control-label">Finish Date</label>
    <div class="col-sm-8">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right datepicker date-toggle-nep-eng" name="finish" value="{{date('Y-m-d')}}" id="finish_date" required="">
        </div>
    </div>
</div>

</div>

  <div class="col-md-4">
<div class="form-group">
<label class="col-sm-4 control-label">Assign to</label>
<div class="col-sm-8">
    <select name="user_id"
            class="form-control">
        @foreach($users as $key=>$pl)
        <option value="{{$pl->id}}" >{{$pl->username}}</option>
        @endforeach

    </select>
</div>
</div>
</div>





</div>
<br>

Bill of materials*
<table class="table table-responsive">
  <thead>
    <tr>
      <th>#</th>
      <th>Bill of materials</th>
      <th>Approximate cost</th>
      <th>Earliest start date</th>
      <th>Earliest finish date</th>
    </tr>
   </thead>
   <tbody id='bom_cost_details'>
    
  </tbody>
</table>



</div>
 <div class="row" style="margin-left: 1px">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Save</button>
             <a class="btn btn-default" 
             href="{{ route('admin.production.issue-warehouse-index') }}" >Cancel</a>
        </div>

    </div>
</div>
</div>

</form>


@endsection
@section('body_bottom')
    @include('partials._body_bottom_submit_bug_edit_form_js')
    @include('partials._date-toggle')
<script>


    function updateBomCost(response){

      let tr =`
        <tr>
          <td><input type='radio' name='selected_bom' value='${response.id}' ></td>
          <td>${ response.bom_name }</td>
          <td>{{env("APP_CURRENCY")}} ${ response.approxCost }</td>
          <td>${ $('#start_date').val() }</td>
          <td>${ $('#finish_date').val() }</td>

        </tr>
      `;
      $('#bom_cost_details').append(tr);
    }


    $('#product_id,#quantity').change(function(){

        let pid = $('#product_id').val();
        let qty =  $('#quantity').val();
        $('#bom_cost_details').html("");

        if(!pid || !qty){
            return;
        }


        $.get(`/admin/production/${pid}/${qty}/bom`,function(response){
           $('#bom_cost_details').html("");
            for (let r of response){

              updateBomCost(r);


            }
   

        }).fail(function(){


            alert('Failed to load');

        });




    });


</script>
    <script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
          //inline: true,
          format: 'YYYY-MM-DD HH:mm', 
          sideBySide: true,
          allowInputToggle: true
        });

      });
</script>
@endsection

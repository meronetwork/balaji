<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 16px;
  border: 1px solid #ddd;
}
</style>
<body>

	<table>
		<thead>
		<tr>
			<th colspan="2">{{env("APP_COMPANY")}}</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td >
				Part No.:<br>
				<h3>B{{ sprintf('%03d',$warehouse->product->id)  }}</h3>
			</td>
			<td>
				Printed:<br>
				<h3>{{ date('d/m/Y') }}</h3>
			</td>
		</tr>
		</tbody>
		<tfoot>
			<tr>
				<td>Part description</td>
				<th>{{ $warehouse->product->name }}</th>
			</tr>
		</tfoot>
	</table>

</body>
</html>
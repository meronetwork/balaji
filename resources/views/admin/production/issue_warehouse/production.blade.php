@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
               Manufacturing Order {{sprintf('%03d',$warehouse->id)}} </h3>
              
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="box box-success ">
    
    <div class="box-header with-border">
        <h3 class="box-title">Manufacturing Order {{sprintf('%03d',$warehouse->id)}} </h3>
        <span class="pull-right">
            <button class="btn   btn-primary">PDF Download</button>
             <a  href="{{ route('admin.production.issue-warehouse-show',$warehouse->id) }}" class="btn btn-default">Back</a>
            @if($warehouse->status == 'finished')
                 <button class="btn btn-success" type="button" > Finished </button>
            @else
             <button class="btn btn-danger" id='finishProduction'> Finish Production</button>
            @endif
        </span>
    </div>

    <div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <dl class="dl-horizontal">
                    <dt>Product</dt>
                    <dd>{{ $warehouse->product->name }}</dd>
                    <dt>Quantity</dt>
                    <dd>{{ $warehouse->qty }} {{$warehouse->product->product_units->symbol}}</dd>
                   
                    <dt>Due Date</dt>
                    <dd>{{ $warehouse->date }}</dd>
                    <dt>Assigned To</dt>
                    <dd>{{ $warehouse->user->first_name }} {{ $warehouse->user->last_name }}</dd>
                    <dt>Target Lot</dt>
                    <dd>B{{  sprintf('%03d',$warehouse->id) }}</dd>
            </dl>
        </div>
                <div class="col-md-6">
            <dl class="dl-horizontal">
                    <dt>Created</dt>
                    <dd>{{ date('d/m/Y',strtotime( $warehouse->created_at ))}}</dd>
                    <dt>Status</dt>
                    <dd>{{ $warehouse->status }}</dd>
                    <dt>Start</dt>
                    <dd>{{ strtotime($warehouse->start) ? date('d/m/Y H:i',strtotime($warehouse->start)): '--'  }}</dd>
                    <dt>End</dt>
                    <dd>{{ strtotime($warehouse->finish) ? date('d/m/Y H:i',strtotime($warehouse->finish)): '--'  }}</dd>
                     @php 
                       $costInfo = $warehouse->costInfo();
                       $materialCost = $costInfo['materialCost'];
                       $overheadCost  = $costInfo['overheadCost'];
                       $labourCost= $costInfo['labourCost'];
                       $totalCost = $costInfo['totalCost'];
                       $perUnitCost = $costInfo['perUnitCost'];
                    @endphp 
                    <dt>Material Cost</dt>
                    <dd>{{ number_format($materialCost,2)  }}</dd>

                    <dt>Overhead Cost</dt>
                    <dd>{{  number_format($overheadCost,2) }}</dd>

                    <dt>Overhead Cost</dt>
                    <dd>{{  number_format($labourCost,2)  }}</dd>
                    <dt>Total Cost</dt>
                    <dd>{{  number_format($labourCost,2)  }}</dd>
                    <dt>Per Unit Cost</dt>
                    <dd>{{  number_format($perUnitCost,2)  }}</dd>
                    
            </dl>
        </div>


</div>
</div>
</div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">BOM Details</h3>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Raw Product</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($bomDetails as $key=>$value)
                @php $tQty = $value->qty *$warehouse->qty;   @endphp 
                <tr>
                    <td>{{ $value->product->name }}</td>
                    <td>{{ env('APP_CURRENCY') }} {{ $value->product->price }}</td>
                    <td>{{  $tQty }}</td>
                    <td>{{ env('APP_CURRENCY') }} {{ $value->product->price * $tQty }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>


    </div>
</div>
<form method="POST" id='pauseProduction' action="{{ route('admin.pause_production') }}">
    @csrf
    <input type="hidden" name="fact_id" class='factID' value="{{ $warehouse->id }}">
    <input type="hidden" name="route_detail_id" class='routeDetailID'>
    <input type="hidden" name="qty" class='madeQty'>
</form>

<form method="POST" id='stopProduction' action="{{ route('admin.stop_production') }}">
    @csrf
    <input type="hidden" name="fact_id" class='factID' value="{{ $warehouse->id }}">
    <input type="hidden" name="route_detail_id" class='routeDetailID'>
    <input type="hidden" name="qty" class='madeQty'>
</form>

<form method="POST" action="{{ route('admin.start_production') }}" id='startProduction'>
@csrf
<input type="hidden" name="fact_id" value="{{ $warehouse->id  }}">
<input type="hidden" name="route_detail_id" class='routeDetailID'>

</form>
@php $canStopProdcution = true;   @endphp
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Operations</h3>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Steps</th>
                    <th>Operation</th>
                    <th>Description</th>
                    <th>Overhead Cost</th>
                    <th>Labor Cost</th>
                    <th>Worker</th>
                      <th>Actual Start</th>
                       <th>Actual Finish</th>
                       <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($factRouteDetails as $key=>$value)
                    <tr>
                        <td>{{ $value->process_order }}</td>
                        <td>{{ $value->section->section_name }}</td>
                        <td>{{ $value->description }}</td>
                        <td>{{ env("APP_CURRENCY") }} {{ $value->overhead_cost }}</td>
                        <td>{{ env("APP_CURRENCY") }} {{ $value->labour_cost }}</td>
                        <td>{{ $value->user->first_name }} {{ $value->user->last_name }}</td>

                        <td>{{  strtotime($value->actual_start) ? date('Y-m-d H:i',strtotime($value->actual_start)):'-' }}</td>
                        <td>{{  strtotime($value->actual_end) ? date('Y-m-d H:i',strtotime($value->actual_end)):'-' }}</td>
                        <td>{{ $value->actutal_qty }}</td>
                        <td>

                        @if($value->is_stop)


                        @elseif(strtotime($value->actual_start) && $value->is_paused != '1')
                        
                            <button type="button" class="btn" onclick="pauseProduction('{{ $value->id }}')"  >
                                <i class="fa  fa-pause"></i>
                            </button>
                            <button class="btn" type="button" onclick="stopProduction('{{$value->id }}')">
                                <i class="fa   fa-stop"></i>
                            </button>
                             @php $canStopProdcution = false;   @endphp
                       @else
                        <button type="button" class="btn" onclick="startProduction('{{ $value->id }}')"  >
                            <i class="fa  fa-play"></i>
                        </button>
                         @php $canStopProdcution = false;   @endphp
                       @endif
                        </td>

                    </tr>    
                @endforeach
            </tbody>
        </table>


    </div>
</div>
<form method="POST" action="{{route('admin.finish_production',$warehouse->id)}}" 
    id='finishProductionForm'>
    @csrf 
    <input type="hidden" name="route_id" value="{{ $factRoute->id }}">

</form>


<script type="text/javascript">

    const canStopProdcution = '{{ $canStopProdcution }}';
    $('#finishProduction').click(function(){

        if(!canStopProdcution){


            alert("Please Finish all process first");
            return;
        }

        $('#finishProductionForm').submit();



    });

    function pauseProduction(id){

        $('#pauseProduction .routeDetailID').val(id);
        let q = prompt('How many Have you made') ;

        if(isNaN(Number(q)) || q == null ){
            return;
        }


        $('#pauseProduction .madeQty').val(q);
        $('#pauseProduction').submit();


    }

    function stopProduction(id){

        $('#stopProduction .routeDetailID').val(id);
        let q = prompt('How many Have you made') ;

        if(isNaN(Number(q)) || q == null ){
            return;
        }


        $('#stopProduction .madeQty').val(q);
        $('#stopProduction').submit();


    }

    function startProduction(id){

         $('#startProduction .routeDetailID').val(id);

         $('#startProduction').submit();


    }
</script>
@endsection

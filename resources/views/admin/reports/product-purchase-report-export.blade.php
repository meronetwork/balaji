<table>
    <tr>
        <th  align="center">
            <b>{{env('APP_COMPANY')}}</b>
        </th>
    </tr>
    <tr>
        <th  style="text-align: center;" align="center" >
            <b>{{\Auth::user()->organization->address}}</b>
        </th>
    </tr>
    <tr>
        <th style="text-align: center;" align="center"  >
            <b>{{$excel_name}}</b>
        </th>
    </tr>
    <tr></tr>
    <tr>
        <th >
            <b>Date: {{date('d M Y')}}</b>
        </th>
    </tr>
    <tr>
        <th >
            <b>Selected Filters-->&nbsp;&nbsp;</b>
        @if($start_date)
                <b>Start Date:</b>{{$start_date}},
        @endif
        @if($end_date)
                <b>End Date:</b>{{$start_date}},
        @endif
        @if($search)
                <b>Search Term:</b>{{$search}},
        @endif
        @if($product_type)
                <b>Product Type:</b>{{$product_type->name}},
            @endif
            @if($sort_by)
                <b>Sort By:</b>{{$sort_by}}
            @endif
        </th>

    </tr>
</table>


<table class="table table-hover table-bordered">

    <thead>
    <tr>
        <th style="border:1px solid;font-weight: bold;text-align: center;" >SNo.</th>
        <th style="border:1px solid;font-weight: bold" >Product Type</th>
        <th style="border:1px solid;font-weight: bold" >Particulars</th>
        <th style="text-align: center;width: 10%;">Price (Rs.)</th>
        <th  style="border:1px solid;font-weight: bold;text-align: center;" >Quantity</th>
        <th  style="border:1px solid;font-weight: bold;text-align: center;" >Total Amount (Rs.)</th>
    </tr>
    </thead>
    <tbody>
    @if($data)
        @foreach($data as $key=>$ei)
            <tr>
                <td style="border:1px solid;text-align: center" >{{$key+1}}.</td>
                <td style="border:1px solid;">{{$ei->type_name}}</td>
                <td  style="border:1px solid;">{{$ei->name}}</td>
                <td style="text-align: center">{{round($ei->price,2)}}</td>
                <td  style="border:1px solid;text-align: center;" >{{$ei->quantity}}</td>
                <td  style="border:1px solid;text-align: center" >{{round($ei->total_price,2)}}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="12" style="font-size: 16px;
    color: grey;
    font-weight: 600;
    text-align: center;">
                No data available...
            </td>
        </tr>
    @endif
    </tbody>
</table>

@extends('layouts.master')
@section('content')


    <?php
    function CategoryTree($parent_id = null, $sub_mark = '', $ledgers_data)
    {

        $groups = \App\Models\COAgroups::orderBy('code', 'asc')->where('parent_id', $parent_id)->get();

        if (count($groups) > 0) {
            foreach ($groups as $group) {
                echo '<option value="' . $group->id . '" disabled><strong>' . $sub_mark . '[' . $group->code . ']' . ' ' . $group->name . '</strong></option>';

                $ledgers = \App\Models\COALedgers::orderBy('code', 'asc')->where('group_id', $group->id)
                    ->get();
                if (count($ledgers) > 0) {
                    $submark = $sub_mark;
                    $sub_mark = $sub_mark . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    foreach ($ledgers as $ledger) {

                        if (($ledgers_data->id ?? '') == $ledger->id) {
                            echo '<option selected value="' . $ledger->id . '"><strong>' . $sub_mark . '[' . $ledger->code . ']' . ' ' . $ledger->name . '</strong></option>';
                        } else {

                            echo '<option value="' . $ledger->id . '"><strong>' . $sub_mark . '[' . $ledger->code . ']' . ' ' . $ledger->name . '</strong></option>';
                        }

                    }
                    $sub_mark = $submark;

                }
                CategoryTree($group->id, $sub_mark . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $ledgers_data);
            }
        }
    }

    ?>


    <?php
    $mytime = Carbon\Carbon::now();
    $startOfYear = $mytime->copy()->startOfYear();

    $endOfYear = $mytime->copy()->endOfYear();
    ?>
    <style type="text/css">
        #entry_list td, #entry_list th {


            font-size: 14px;

        }
    </style>
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet"/>
    <script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>


    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border" >
                <h3 class="box-title">Products Purchase Report</h3>
                <a href="/admin/reports/products-purchase?startdate={{Request::get('startdate')}}&enddate={{Request::get('enddate')}}&search={{Request::get('search')}}&product_type={{Request::get('product_type')}}&export=true"
                   class="btn btn-success pull-right btn-sm" style="margin-left: 5px;"
                   >Export to Excel</a>
            </div>
            <div style="clear: both;"></div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- <div id="accordion"> -->
                <!-- <h3>Options</h3> -->
                <div class="balancesheet form">
                    <form method="GET" action="{{route('admin.product.purchaseReport')}}">

                        <div class="row">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Search Product</label>
                                    <div class="input-group">
                                        <input type="text" name="search"
                                               class="form-control input-sm" placeholder="Enter Product Name"
                                               value="{{Request::get('search')}}">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                        <input id="ReportStartdate" type="text" name="startdate"
                                               class="form-control input-sm datepicker"
                                               value="{{Request::get('startdate')}}">
                                        <div class="input-group-addon">
                                            <i>
                                                <div class="fa fa-info-circle" data-toggle="tooltip"
                                                     title="Note : Leave start date as empty if you want statement from the start of the financial year.">
                                                </div>
                                            </i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>End Date</label>

                                    <div class="input-group">
                                        <input id="ReportEnddate" type="text" name="enddate"
                                               class="form-control input-sm datepicker"
                                               value="{{Request::get('enddate')}}">
                                        <div class="input-group-addon">
                                            <i>
                                                <div class="fa fa-info-circle" data-toggle="tooltip"
                                                     title="Note : Leave end date as empty if you want statement till the end of the financial year.">
                                                </div>
                                            </i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Product Types</label>
                                    <select class="form-control input-sm customer_id select2" name="product_type"
                                            tabindex="-1" aria-hidden="true">
                                        <option selected value="">Select Product Type</option>
                                        @foreach($product_types as $type)
                                            <option value="{{$type->id}}"
                                                    @if(Request::get('product_type') == $type->id) selected="" @endif>{{$type->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Sort By</label>
                                    <select class="form-control input-sm" name="sort_by"
                                            >
                                        <option selected value="">Select Sort Option</option>
                                        <option value="Amount" @if(Request::get('sort_by') == 'Amount') selected @endif>Amount</option>
                                        <option value="Quantity" @if(Request::get('sort_by') == 'Quantity') selected @endif>Quantity</option>
                                    </select>


                                    </select>

                                </div>
                            </div>
                            <div style="float: left;">
                                <label>
                                    <label></label>
                                </label>
                                <div class="form-group">
                                    <a type="reset" href="/admin/reports/products-purchase"
                                       class="btn btn-danger pull-right btn-sm" style="margin-left: 5px;"
                                       value="Clear">Clear</a>
                                    <input type="submit" class="btn btn-primary pull-right btn-sm" value="Filter">
                                </div>
                            </div>
{{--                            <div class="col-md-3">--}}
{{--                                <input type="checkbox" name="best_seller" value="true" @if(Request::get('best_seller') == true) checked @endif>--}}
{{--                                <label for="">Sort By Qty</label>--}}
{{--                                <input type="checkbox" name="best_seller" value="true" @if(Request::get('best_seller') == true) checked @endif>--}}
{{--                                <label for="">Sort By Price</label>--}}
{{--                            </div>--}}
                        </div>
                    </form>
                </div>
                <!-- </div> -->
                <div class="subtitle">
                    {{--                    Transaction Date &nbsp;from <strong>{{  $startdate ?? date('d-M-Y', strtotime($startOfYear))}}</strong> to <strong>{{$enddate ?? date('d-M-Y', strtotime($endOfYear))}}</strong>--}}
                </div>
                <table class="table table-hover table-bordered">

                    <tbody id='entry_list'>
                    <tr style="background: #3c8dbc;color: #FFFFFF;">
                        <th style="text-align: center;width: 7%;">SNo.</th>
                        <th>Product Type</th>
                        <th>Particulars</th>
                        <th style="text-align: center;width: 10%;">Price (Rs.)</th>
                        <th style="text-align: center;width: 10%;">Quantity</th>
                        <th style="text-align: center;width: 15%;">Total Amount ({{env('APP_CURRENCY')}}.)</th>
                    </tr>
                    @if($product_purchases&&$product_purchases->isNotEmpty())

                        @foreach($product_purchases as $key=>$ei)


                            <tr>
                                <td style="text-align: center">{{$key+1}}.</td>
                                <td style="white-space: nowrap;">{{$ei->type_name}}</td>
                                <td style="white-space: nowrap;">{{$ei->name}}</td>
                                <td style="text-align: center">{{number_format($ei->price,2)}}</td>
                                <td style="white-space: nowrap;text-align: center;">{{$ei->quantity}}</td>
                                <td style="white-space: nowrap;text-align: center">{{number_format($ei->total_price,2)}}</td>
                            </tr>

                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" style="font-size: 16px;
    color: grey;
    font-weight: 600;
    text-align: center;">
                                No data available...
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="text-center">

                    {!! $product_purchases?$product_purchases->appends(\Request::except('page'))->render():'' !!}

                </div>
                <br>


                {{--                <a href="{{route('admin.chartofaccounts.excel',$requestData)}}" class="btn btn-success">Excel</a>--}}

                {{--                <a href="{{route('admin.chartofaccounts.pdf',$requestData)}}" class="btn btn-primary")">PDF</a>--}}

                {{--                <a href="{{route('admin.chartofaccounts.print',$requestData)}}" class="btn btn-primary")">Print</a>--}}

            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function () {
            $('.datepicker').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD',
                sideBySide: true,
                allowInputToggle: true
            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.customer_id').select2();
        });

        function downloadfile(type) {

            let data = $('#ledgerstatementfilterform').serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            let startdate = data.startdate;

            let enddate = data.enddate;

            location.href = `/admin/chartofaccounts/${type}/{{$ledgers_data->id??''}}?startdate=${startdate}&enddate=${enddate}`;

        }


    </script>


@endsection

<!-- Optional bottom section for modals etc... -->
@section('body_bottom')


@endsection

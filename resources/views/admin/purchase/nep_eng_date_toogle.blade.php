
<?php 
  $cal = new \App\Helpers\NepaliCalendar();
?>
<div id = 'datapart1' style="display: none">
<div class="col-md-12 nepalidate">
<input type="text" class="form-control pull-right nepalidatepicker" name="bill_date" 
@if($order->bill_date) value="{{$cal->full_nepali_from_eng_date($order->bill_date)}}" @endif
 id="bill_date" required="">
    
        <!-- /.input group -->
    
</div>

  <div class="col-md-12 englishdate">
                                    

<input type="text" class="form-control pull-right datepicker" name="bill_date" value="{{$order->bill_date}}" id="bill_date" required="">

</div>

</div>

<div id = 'datapart2' style="display: none">
<div class="col-md-12 nepalidate" >
<input type="text" class="form-control pull-right nepalidatepicker" name="due_date" 
@if($order->due_date) value="{{$cal->full_nepali_from_eng_date($order->due_date)}}"  @endif
id="due_date" required="">
</div>

  <div class="col-md-12 englishdate">
                                    

<input type="text" class="form-control pull-right datepicker" name="due_date" value="{{$order->due_date}}" id="due_date" required="">

</div>

</div>

<script type="text/javascript" src="https://nepali-date-picker.herokuapp.com/src/jquery.nepaliDatePicker.js"> </script>
<link rel="stylesheet" href="https://nepali-date-picker.herokuapp.com/src/nepaliDatePicker.css">
  <script type="text/javascript">
    function setnepalidate(){
    $(".nepalidatepicker").nepaliDatePicker({
        dateFormat: "%y-%m-%d",
        closeOnDateSelect: true
    });
}
function setenglishdate(){
   $('.datepicker').datetimepicker({
          //inline: true,
          //format: 'YYYY-MM-DD',
          format: 'YYYY-MM-DD', 
              sideBySide: true,
              allowInputToggle: true
        });
}

 $('#selectdatetype').change(function(){
  let type = $(this).val();
  if(type =='nep'){
    let html = $('#datapart1 .nepalidate').html();
    // console.log(html);
    $('#dateselectors').html(html);

     let html1 = $('#datapart2 .nepalidate').html();
     $('#dateselectors1').html(html1);
    setnepalidate();
  }else{
    let html = $('#datapart1 .englishdate').html();
    $('#dateselectors').html(html);

    let html1 = $('#datapart2 .englishdate').html();
     $('#dateselectors1').html(html1);
    setenglishdate();
  }
 });

  </script>
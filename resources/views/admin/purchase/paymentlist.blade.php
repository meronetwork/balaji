@extends('layouts.master')

@section('head_extra')

 @include('partials._head_extra_select2_css')

@endsection
@section('content')
 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                 {!! $page_title !!}

                <small>{!! $page_description !!}</small>
            </h1>
            {{ TaskHelper::topSubMenu('topsubmenu.purchase')}}
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
 </section>

  <div class='row'> 
        <div class='col-md-12'> 
            <!-- Box -->
            
                <div class="box box-primary">

                    <div class="box-header with-border">
                        &nbsp;
                        @if($purchase_id)
                        <a class="btn btn-social btn-foursquare" href="{!! route('admin.payment.purchase.create', $purchase_id) !!}" title="Create Order">
                            <i class="fa fa-plus"></i>Add Payment
                        </a>
                        @else
                        <div class="wrap" style="margin-top:5px;">
                        <form method="get" action="/admin/purchase/paymentslist">
                    <div class="filter form-inline" style="margin-left: 20px;">
                    

                        {!! Form::select('supplier_id', $suppliers, \Request::get('supplier_id'), ['id'=>'filter-supplier', 'class'=>'form-control searchable', 'style'=>'width:150px; display:inline-block;','placeholder'=>'Select Supplier']) !!}&nbsp;&nbsp;
                        <input type="text" name="start_date" class="form-control datepicker input-sm" placeholder="Start Date..." value="{{ Request::get('start_date') }}">
                         <input type="text" name="end_date" class="form-control datepicker input-sm" placeholder="End Date..." value="{{ Request::get('end_date') }}">
                        <input type="hidden" name="search" value="true">
                        
                        <button class="btn btn-primary btn-sm" id="btn-submit-filter" type="submit">
                            <i class="fa fa-list"></i> Filter
                        </button>
                        <a href="/admin/purchase/paymentslist" class="btn btn-danger  btn-sm" id="btn-filter-clear" >
                            <i class="fa fa-close"></i> Clear
                        </a>
                    </div>
                    </form>
                </div>
                        @endif
                    </div>



{!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}

                   <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="orders-table">

                                <thead>
                                    <tr class="bg-maroon">
                                        <th>Bill</th>
                                        <th>Reference No</th>
                                        <th>Date</th>
                                        <th>Supplier</th>
                                        <th>Amount</th>
                                        <th>Paid Through</th>
                                        <th>Paid By</th>
                                    </tr>
                                </thead>

                                <tbody>
                                   @if(isset($payment_list) && !empty($payment_list)) 
                                     @foreach($payment_list as $o)
                                        <tr>
                                            <td>{{ $o->purchase->bill_no  }}  </td>
                                            <td>{!! $o->reference_no !!}</td>
                                            <td>{!! date('dS M y', strtotime($o->date)) !!}</td>
                                            <td>{{ $o->purchase->client->name  }}  </td>
                                            <td>{{env('APP_CURRENCY')}} {{ number_format($o->amount,2) }}</td>
                                            <td>{!! $o->paidby->name !!}</td>

                                            <td>
                                                {{ $o->createdby->first_name }} {{ $o->createdby->last_name }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div> <!-- table-responsive -->
                    </div><!-- /.box-body -->
                 </div><!-- /.box -->

            {!! Form::close() !!}
        </div><!-- /.col -->

    </div><!-- /.row -->
    @section('body_bottom')
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<script type="text/javascript">
    $('.searchable').select2();
     $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });
</script>

@endsection


@endsection


@extends('layouts.master')
@section('content')

<style type="text/css">


</style>

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Bills {!! $_GET['type'] == 'purchase_orders' ? 'Purchase Order' : ucfirst($_GET['type']) !!}

                <small> Manage purchase {{ ucfirst(\Request::get('type'))}}</small>
            </h1>
             {{ TaskHelper::topSubMenu('topsubmenu.purchase')}}
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->

                <div class="box box-primary">
                    <div class="box-header with-border">

                        &nbsp;
                        <a class="btn btn-social btn-foursquare" href="{!! route('admin.purchase.create') !!}?type={{\Request::get('type')}}" title="Create Order">
                            <i class="fa fa-plus"></i> Create New {!! $_GET['type'] == 'purchase_orders' ? 'Purchase Order' : ucfirst($_GET['type']) !!}
                        </a>
                        &nbsp;
                        <a class="btn btn-default btn-sm" href="#" onclick="document.forms['frmClientList'].action = '{!! route('admin.orders.enable-selected') !!}';  document.forms['frmClientList'].submit(); return false;" title="{{ trans('general.button.enable') }}">
                            <i class="fa fa-check-circle"></i>
                        </a>
                        &nbsp;
                        <a class="btn btn-default btn-sm" href="#" onclick="document.forms['frmClientList'].action = '{!! route('admin.orders.disable-selected') !!}';  document.forms['frmClientList'].submit(); return false;" title="{{ trans('general.button.disable') }}">
                            <i class="fa fa-ban"></i>
                        </a>

                      &nbsp;
                      <a href="/admin/download/purchase/pdf/index?type={{\Request::get('type')}}&start_date={{  \Request::get('start_date') }}&end_date={{  \Request::get('end_date') }}&supplier_id={{  \Request::get('supplier_id') }}" class="btn btn-success btn-xs float-right" style="margin-right: 20px;">PDF Download</a>
                      &nbsp;
                      <a href="/admin/download/purchase/excel/index?type={{\Request::get('type')}}&start_date={{  \Request::get('start_date') }}&end_date={{  \Request::get('end_date') }}&supplier_id={{  \Request::get('supplier_id') }}" class="btn btn-success btn-xs float-right" >Excel Download</a>
                      &nbsp;


                    </div>
                     <div class="wrap" style="margin-top:5px;">
                        <form method="get" action="/admin/purchase">
                    <div class="filter form-inline" style="margin-left: 20px;">
                        {!! Form::text('start_date', \Request::get('start_date'), [ 'class' => 'form-control date-toggle', 'id'=>'start_date', 'placeholder'=>'Start Date','autocomplete' =>'off']) !!}&nbsp;&nbsp;
                        <!-- <label for="end_date" style="float:left; padding-top:7px;">End Date: </label> -->
                        {!! Form::text('end_date', \Request::get('end_date'), [ 'class' => 'form-control date-toggle', 'id'=>'end_date', 'placeholder'=>'End Date','autocomplete' =>'off']) !!}&nbsp;&nbsp;

                        {!! Form::select('supplier_id', ['' => 'Select Suppliers'] + $suppliers, \Request::get('supplier_id'), ['id'=>'filter-supplier', 'class'=>'form-control', 'style'=>'width:150px; display:inline-block;']) !!}&nbsp;&nbsp;

                        <input type="hidden" name="search" value="true">
                        <input type="hidden" name="type" value={{ Request::get('type') }}>
                        <button class="btn btn-primary" id="btn-submit-filter" type="submit">
                            <i class="fa fa-list"></i> Filter
                        </button>
                        <a href="/admin/purchase?type={{ Request::get('type') }}" class="btn btn-danger" id="btn-filter-clear" >
                            <i class="fa fa-close"></i> Clear
                        </a>
                    </div>
                    </form>
                </div>

                {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
                    <div class="box-body">

                        <span id="index_lead_ajax_status"></span>

                        <div class="table-responsive">
                            <table class="table table-hover table-striped" id="orders-table">

                                <thead>
                                    <tr class="bg-maroon">

                                        <th>id</th>
                                        <th>Bill No</th>
                                        <th>Bill Date</th>
                                        <th>Supplier</th>

                                        <th>Purchse Status</th>
                                        <th>Total</th>
                                        <th>Taxable Amt.</th>
                                        <th>Paid</th>
                                        <th>Balance</th>
                                        <th>Pay Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @php
                                    $totalSummaryAmount = 0;
                                    $totalSummaryPaid = 0;
                                    $totalSummaryBalance = 0;
                                    @endphp
                                @if(isset($orders) && !empty($orders))
                                    @foreach($orders as $o)
                                        <tr>


                                            <td>{{ FinanceHelper::getAccountingPrefix('PURCHASE_PRE') }}{!! $o->id !!}<input type="hidden" name="purchase_id" class="index_purchase_id" value="{{$o->id}}"></td>
                                            <td>{!! $o->bill_no !!}<input type="hidden" name="purchase_id" class="index_purchase_id" value="{{$o->id}}"></td>

                                             <td style="white-space: nowrap;">
                                                {{ date('dS M Y', strtotime($o->bill_date)) }}<br>
                                                {{ TaskHelper::getNepaliDate($o->bill_date)  }}
                                            </td>

                                            <td title="{{ $o->comments}}"><span style="font-size: 16.6px"><a href="/admin/purchase/{{$o->id}}?type={{\Request::get('type')}}"> {{ $o->client->name }}</a></span></td>


                                            <td>
                                              {!! \Form::select('purchase_status',['Placed'=>'Placed','Parked'=>'Parked','Recieved'=>'Recieved'], $o->status, ['class' =>'form-control label-default','id' => 'purchse_status'])!!}
                                            </td>

                                            <td>{!! number_format($o->total,2) !!}</td>

                                            <td>{!! number_format($o->taxable_amount,2) !!}</td>
                                            <?php
                                              $paid_amount= \TaskHelper::getPurchasePaymentAmount($o->id);

                                              $totalSummaryAmount  += $o->total;
                                              $totalSummaryPaid += $paid_amount;
                                              $totalSummaryBalance += (  $o->total-$paid_amount );

                                             ?>
                                            <td>{!! number_format($paid_amount,2)!!}</td>
                                            <td>{!! $o->total - $paid_amount !!}</td>

                                            @if($o->payment_status == 'Pending')
                                              <td><span class="label label-warning">Pending</span></td>
                                            @elseif($o->payment_status == 'Partial')
                                              <td><span class="label label-info">Partial</span></td>
                                            @elseif($o->payment_status == 'Paid')
                                              <td><span class="label label-success">Paid</span></td>
                                            @else
                                               <td><span class="label label-warning">Pending</span></td>
                                            @endif

                                            <td>
                                                @if($o->purchase_type == 'bills')
                                                   @if( $o->isEditable())
                                                      <a href="{!! route('admin.purchase.edit', $o->id) !!}?type={{\Request::get('type')}}" title="{{ trans('general.button.edit') }}"><i class="fa fa-edit"></i></a>
                                                      @if($o->supplier_type == 'supplier')

                                                      <a href="{!! route('admin.payment.purchase.list', $o->id) !!}"  title="View Payment"><i class="fa fa-credit-card"></i></a>
                                                      @endif
                                                       <a href="{!! route('admin.purchase.confirm-delete', $o->id) !!}?type={{\Request::get('type')}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
                                                    @else

                                                    <i class="fa fa-edit text-muted" title=""></i>
                                                    <i class="fa fa-trash text-muted" title=""></i>
                                                     <a href="{!! route('admin.payment.purchase.list', $o->id) !!}"  title="View Payment"><i class="fa fa-credit-card"></i></a>

                                                     @endif
                                                @else

                                                @if ( $o->isEditable() )
                                                    @if($o->status == 'paid' && \Request::get('type') == 'invoice')
                                                    <i class="fa fa-edit text-muted" title=""></i>
                                                    @else
                                                        <a href="{!! route('admin.purchase.edit', $o->id) !!}?type={{\Request::get('type')}}" title="{{ trans('general.button.edit') }}"><i class="fa fa-edit"></i></a>

                                                    @endif
                                                @else
                                                    <i class="fa fa-edit text-muted" title="{{ trans('admin/orders/general.error.cant-edit-this-document') }}"></i>
                                                @endif
{{--                                                @if ( $o->isDeletable() )--}}
{{--                                                    @if($o->status == 'paid' && \Request::get('type') == 'invoice')--}}
{{--                                                    <i class="fa fa-trash text-muted" title=""></i>--}}
{{--                                                    @else--}}
                                                    <a href="{!! route('admin.purchase.confirm-delete', $o->id) !!}?type={{\Request::get('type')}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
{{--                                                   <!--  @endif--}}
{{--                                                @else--}}
{{--                                                    <i class="fa fa-trash text-muted" title="{{ trans('admin/orders/general.error.cant-delete-this-document') }}"></i>--}}
{{--                                                @endif--}}

                                                  @endif
                                                  <a href="/admin/purchase/generatePDF/{{ $o->id }}"><i class="fa fa-download"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                                @if(Request::get('search'))
                                <tfoot>
                                    <tr >
                                        <td colspan="5"></td>
                                        <td>Total:</td>
                                        <td>{{ $totalSummaryAmount }}</td>
                                        <td>{{ $totalSummaryPaid }}</td>
                                        <td>{{ $totalSummaryBalance }}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                </tfoot>
                                @endif
                            </table>

                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                @if(!Request::get('search'))
                <div class="center">

                  {!!  $orders->appends(\Request::except('page'))->render() !!}

                </div>
                @endif

            {!! Form::close() !!}
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
@include('partials._date-toggle')
<script type="text/javascript">
$('.date-toggle').nepalidatetoggle();
</script>
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

    <script language="JavaScript">
        function toggleCheckbox() {
            checkboxes = document.getElementsByName('chkClient[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = !checkboxes[i].checked;
            }
        }
    </script>

    <script>
    $(function() {
        $('#orders-table').DataTable({
            pageLength: 25,
            "ordering": false,
        });
    });
    </script>

<script type="text/javascript">


     $(document).on('change', '#purchse_status', function() {

          var id = $(this).closest('tr').find('.index_purchase_id').val();

          var purchase_status = $(this).val();
          $.post("/admin/ajax_purchase_status",
          {id: id, purchase_status:purchase_status, _token: $('meta[name="csrf-token"]').attr('content')},
          function(data, status){
            if(data.status == '1')
                $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
            else
                $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

            $('#index_status_update').delay(3000).fadeOut('slow');
            //alert("Data: " + data + "\nStatus: " + status);
          });

        });
     $(function() {
        $('#start_date').datepicker({
                 //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d',
                sideBySide: true,

            });
        $('#end_date').datepicker({
                 //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d',
                sideBySide: true,

            });
        });
</script>

@endsection

@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
<div class="callout callout-danger">
          <h4>Queues!</h4>

          <?php ?>

          <p>{{ \DB::table('jobs')->count('id') }} mails are in in queues </p>
        </div><br/>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
               {{ $page_title}}
                <small>{!! $page_description ?? "Page description" !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            {!! Form::open( array('route' => 'admin.contacts.enable-selected', 'id' => 'frmContactList') ) !!}
                <div class="box box-primary">
                    <div class="box-header with-border">
                       
                        &nbsp;
                        <a class="btn btn-default btn-sm" href="#" onclick="document.forms['frmContactList'].action = '{!! route('admin.contacts.enable-selected') !!}';  document.forms['frmContactList'].submit(); return false;" title="{{ trans('general.button.enable') }}">
                            <i class="fa fa-check-circle"></i>
                        </a>
                        &nbsp;
                        <a class="btn btn-default btn-sm" href="#" onclick="document.forms['frmContactList'].action = '{!! route('admin.contacts.disable-selected') !!}';  document.forms['frmContactList'].submit(); return false;" title="{{ trans('general.button.disable') }}">
                            <i class="fa fa-ban"></i>
                        </a>
                        &nbsp;
                       

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="contacts-table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Subject</th>
                                        <th>message</th>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>Subject</th>
                                        <th>message</th>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                @if(isset($mails) && !empty($mails))
                                    @foreach($mails as $m)
                                        <tr>
                                            <td><h4><a href="#">{!! $m->title !!}</a></h4></td>
                                            <td>{!! $m->subject !!}</td>
                                            <td>{{ strip_tags(mb_substr($m->message,0,1000))  }}</td>
                                            <td>{!! $m->course->name !!}</td>
                                            <td>{!! $m->status->name !!}</td>
                                            <td>{!! date('dS M y', strtotime($m->created_at)) !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                          
                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            {!! Form::close() !!}
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection
@endif

<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

    <script language="JavaScript">
        function toggleCheckbox() {
            checkboxes = document.getElementsByName('chkContact[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = !checkboxes[i].checked;
            }
        }
    </script>

    <script>
    $(function() {
        $('#contacts-table').DataTable({
            pageLength: 35
        });
    });
    </script>

@endsection

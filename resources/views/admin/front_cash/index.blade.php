@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ ucfirst($page_title)}}
        <small>{!! $page_description ?? "Page description" !!}</small>
    </h1>
    <p><i class="fa fa-money"></i> Receive all kinds of payments from front office. This will be shown in guest folio top left > Deposit section</p>


</section>
<div class="box">
	<div class="box-header">


	<a href="{{  route('admin.front-cash.create') }}" class="btn btn-primary" data-toggle="modal" data-target="#modal_dialog">Receive Payment</a>

	</div>
	<div class="box-body">
		<table class="table table-striped">
			<thead class="bg-info">
                <th>Num</th>
				<th>Date</th>
                <th>By</th>
                <th>Staff</th>
                <th>guest_name</th>
                <th>type</th>
                <th>curr amount</th>
                <th>xch. amount</th>
                <th>Amount</th>
                <th>revenue</th>
                <th><i class="fa fa-cog"></i></th>
			</thead>

			<tbody>
            @foreach($dipositHistory as $key=>$value)
                <tr>
                    <td> <a target="_blank" href="/admin/entries/show/{{\FinanceHelper::get_entry_type_label($value->entry->entrytype_id)}}/{{$value->entry->id}}">{{$value->entry->number}}</a> </td>
                    <td>{{$value->date }}</td>
                    <td>{{$value->paid_by }}</td>
                    <td>{{$value->user->username }}</td>
                    <td>{{ $value->guest_name }}</td>
                    <td>{{ $value->record_type }}</td>
                    <td style="font-size: 16.5px">{{ $value->currency_symbol }}{{ $value->currency_amount }}</td>
                    <td>{{env('APP_CURRENCY')}}{{ $value->exchange_amount }}</td>
                    <td style="font-size: 16.5px">{{env('APP_CURRENCY')}}{{number_format($value->amount) }}</td>
                    <td>{{ $value->revenue }}</td>
                    <td>
                        <a href="{{ route('admin.hotel.receipt.print',$value->id) }}"><i class="fa fa-print"></i></a>
                    </td>
                </tr>
            @endforeach
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">


$(document).on('change','#room_num',function(){

	let room = $(this).val();

	$.get(`/admin/get-guest-info-from-reservation/${room}`,function(res){

		$('#guest_name').val(res.guest);

	});



});

 $(document).on('hidden.bs.modal', '#modal_dialog', function(e) {
        $('#modal_dialog .modal-content').html('');
    });


</script>
@endsection



 <form action="/admin/payment/reservation/from-front/deposit" method="post">
<div class="modal-content">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h3 class="modal-title">Front Office Cash</h3>
</div>

<div class="modal-body">

      {{ csrf_field() }}
  <div class="row">
  <div class="col-md-6 form-group">
    <label >Room:</label>
    {!!  Form::select('room_num',$rooms,null,['class'=>'form-control input-sm','id'=>'room_num','placeholder'=>'Select room','required'=>'required'])  !!}
  </div>
  <div class="col-md-6 form-group">
    <label >Guest:</label>
    <input  class="form-control input-sm" name="guest_name" placeholder="Guest Name" id='guest_name'>
  </div>


  <div class="col-md-6 form-group">
    <label >Pay In:</label>
    <select name="paid_by" class="form-control input-sm" id="colorselector" required>
        <option value="">Select</option>
        <option value="cash">Cash</option>
        <option value="check">Cheque</option>
         <option value="credit-cards">Credit cards</option>
    </select>
  </div>

  <div class="col-md-6 form-group">
    <label>Revenue:</label>
    {!!

      Form::select('revenue',['accommodation'=>'Accommodation',
      'alcohol'=>'Alcohol',
      'beverage'=>'Beverage',
      'food'=>'Food',
      'massage'=>'Massage',
      'noshow'=>'No Show',
      'others'=>'Others'
      ],null,['class'=>'form-control input-sm'])

    !!}
  </div>


  <div id="cash" class="colors" style="display:none">

      <div class="row">
      </div>

  </div>
  <div id="credit-cards" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Card Type</label>
                                        <div class="input-group ">
                                            <select class="form-control" name="cc_type">
                                                <option value="">Select</option>
                                                <option value="visa">Visa</option>
                                                <option value="master">Master</option>
                                            </select>
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Name On Card</label>
                                        <div class="input-group ">
                                            <input type="text" name="cc_holder" placeholder="Name on card" id="name_on_card" value="" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Card No</label>
                                        <div class="input-group ">
                                            <input type="text" name="cc_no" placeholder="Card No" id="card_no" value="" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Expiry Date</label>
                                        <div class="input-group ">
                                            <input type="text" name="expiry_date" placeholder="Expiry Date" id="expiry_date" value="{{\Carbon\Carbon::now()->toDateString()}}" class="form-control datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
     <div id="check" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Bank Name</label>
                                        <div class="input-group ">
                                            <input type="text" name="bank_name" placeholder="Bank Name" id="bank_name" value="" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Cheque No</label>
                                        <div class="input-group ">
                                            <input type="text" name="cheque_no" placeholder="Cheque No" id="cheque_no" value="" class="form-control ">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Cheque Date</label>
                                        <div class="input-group ">
                                            <input type="text" name="cheque_date" placeholder="Cheque Date" id="" value="" class="form-control datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

  <div class="col-md-6 form-group">
    <label >Record Type:</label>
    <select name="record_type" class="form-control input-sm" id="colorselector" required>
        <option value="">Select</option>
        <option value="disount">Discount</option>
        <option value="rebate">Rebate</option>
        <option value="deposit">Deposit</option>
    </select>
  </div>
  <div class="col-md-6 form-group">
    <label >Date:</label>
    <input type="date" name="date" placeholder="Date" class="form-control" required="required">
  </div>

 </div>

<div class="row">


  <div class="col-md-3 form-group">
    <label >Currency:</label>
      <select name="currencies" class="form-control input-sm exchange_currency" required
              >
          <option value="">Select Currency</option>

      @foreach($currencies as $k=>$v)
              <option value="{{$v->id}}">{{$v->name}}</option>
          @endforeach
      </select>

  </div>
  <div class="col-md-3 form-group">
    <label >Currency Amount:</label>
    <input type="number" step="any" required class="form-control input-sm currency_amount" placeholder="Currency Amount.." name="currency_amount" >
  </div>
  <div class="col-md-3 form-group">
    <label >Exchange Rate:</label>
    <input type="number" step="any" readonly class="form-control input-sm exchange_rate" name="exchange_amount" placeholder="Exchange Rate.." >
  </div>
  <div class="col-md-3 form-group">
    <label >Total Amount:</label>
    <input  type="number" step="any" class="form-control input-sm total_amount" readonly name="amount" placeholder="Total Amount.." required="required">
  </div>

   <div class="col-md-12 form-group">
    <label >Note</label>
    <textarea class="form-control input-sm" rows="3" name="note" placeholder="Note.."></textarea>
  </div>


</div>






</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn  btn-primary" >Submit</button>
</div>
</div>
</form>
<script type="text/javascript">
    $(document).on('change','.exchange_currency',function () {
        calcTotal()
    })
    $(document).on('input','.currency_amount',function () {
        calcTotal()
    })
    function calcTotal(){
        let currency_id=$('.exchange_currency').val()
        let currencies={!! json_encode($currencies) !!}

            let selected_currency='';
        currencies.forEach((item)=>{
            if(item.id==currency_id){
                if(item.latest_manual_currency)
                    selected_currency=item.latest_manual_currency.sell_rate
                else
                    selected_currency=item.default_selling
            }
        })
        selected_currency=Number(selected_currency)

        $('.exchange_rate').val(selected_currency)
        let currency_amount=Number($('.currency_amount').val())
        $('.total_amount').val((selected_currency*currency_amount).toFixed(2))
    }



    $(function() {
        $('#colorselector').change(function() {

            $('.colors').hide();
            $('#' + $(this).val()).show();

        });
    });
    $(document).ready(function() {
        $("#travel_agent,#city_ledger").autocomplete({
            source: "/admin/getLedgers"
            , minLength: 1
        });
    });

    $('#room_no').keyup(function() {
        $("#room_no").autocomplete({
            source: "/admin/getRooms"
            , minLength: 1
        });
    });

    $('#room_no').change(function() {
        if ($(this).val() != '') {
            $.ajax({
                url: "/admin/users/ajax/getresv"
                , data: {
                    room_num: $(this).val()
                }
                , dataType: "json"
                , success: function(data) {
                    var result = data.data;
                    $('#room_detail').html(result);
                }
            });
        }
    });


</script>

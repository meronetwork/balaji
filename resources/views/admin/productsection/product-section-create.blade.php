@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Product Section
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 @if($edit)
 <form method="post" action="{{route('admin.production.edit-prodsection',$edit->id)}}">
         	{{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
 	<div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Name</label>
<div class="input-group ">
<input type="text" name="section_name" placeholder="Section name" id="name" value="{{$edit->section_name}}" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Symbol</label>
<div class="input-group ">
<input type="text" name="section_area" placeholder="Enter section area" id="symbol" class="form-control" value="{{$edit->section_area}}" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-lastfm"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update</button>
        </div>
 
    </div>
</div>
</div>
</div>
</form>
 @else
<form method="post" action="{{route('admin.production.product-section-create')}}">
{{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
 	<div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Section name</label>
<div class="input-group ">
<input type="text" name="section_name" placeholder="Name" id="Enter Section name" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Section area</label>
<div class="input-group ">
<input type="text" name="section_area" placeholder="Enter section Name" id="symbol" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-lastfm"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
 
    </div>
</div>
</div>
</div>
</form>
@endif
  @endsection
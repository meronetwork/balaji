@extends('layouts.master')
@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')

    <style>
        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height: 100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #efefef;
            color: white;
            text-align: center;
        }

    </style>
@endsection

@section('content')
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet"/>
    <script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>

    <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>
            Privilege Card
            <small>
                Edit Privilege Card
            </small>
        </h1>
    </section>
    <div class='row'>
        <div class='col-md-12'>
            <div class="box-body">

                <div class="col-md-12">
                    <div class="">
                        <form method="POST" enctype="multipart/form-data" action="{{route('admin.privilege-card.edit',$privilegeCard->id)}}">
                            {{ csrf_field() }}
                            <div class="">




                                <div class="clearfix"></div>
                                <div class="col-md-12">

                                    <div class="col-md-3 form-group">
                                        <label >Image</label>
                                        <input type="file" name="image">
                                        @if($privilegeCard->image != '')
                                            <label>Current Logo: </label><br/>
                                            <img width="150px" height="auto" src="{{ '/images/privilege-card/'.$privilegeCard->image }}">
                                        @endif
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="comment">Name</label>
                                        <input type="text" name="name" id="name" placeholder="Name" class="form-control"
                                               value="{{$privilegeCard->name}}">
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="comment">Address</label>
                                        <textarea class="form-control input-sm" name="address" id=""
                                                  value="" type="text" cols="10"
                                                  placeholder="Address" rows=1">{{$privilegeCard->address}}</textarea>
                                    </div>

                                    <div class="col-md-3 form-group" style="">
                                        <label for="position">Contact No</label>
                                        <input type="number" name="contact_no" class="form-control"
                                               placeholder="Contact" id="position" value="{{$privilegeCard->contact_no}}">
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="position">Date of birth</label>
                                        <input type="text" name="dob" class="form-control date-toggle-nep-eng datepicker" placeholder="D-O-B"
                                               id="position" value="{{$privilegeCard->dob}}">
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="comment">Email</label>
                                        <input type="email" placeholder="Email" name="email" id="driver_name"
                                               class="form-control" value="{{$privilegeCard->email}}">
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="position">Nationality</label>
                                        <input type="text" name="nationality" class="form-control"
                                               placeholder="Nationality" id="position" value="{{$privilegeCard->nationality}}">
                                    </div>

                                    <div class="col-md-3 form-group" style="">
                                        <label for="status">Married Status</label>
                                        <select class="form-control input-sm" id="user_id" name="married_status">
                                            @if($privilegeCard->married_status=='single')
                                            <option selected value="single">Single</option>
                                            @else
                                                <option value="single">Single</option>
                                            @endif
                                            @if($privilegeCard->married_status=='married')
                                            <option  selected value="married">Married</option>
                                                @else
                                                <option value="married">Married</option>
                                                @endif
                                        </select>
                                    </div>

                                    <div class="col-md-6 form-group" style="">
                                        <label for="position">Qualification</label>
                                        <input type="text" name="qualification" class="form-control"
                                               placeholder="Qualification" id="position" value="{{$privilegeCard->qualification}}">
                                    </div>
                                    <div class="col-md-12 form-group" style="">
                                        <label for="position">Occupation</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Business <input class="form-group" name="occupation" @if($privilegeCard->occupation=='Business') checked @endif type="radio" value="Business">&nbsp;&nbsp;&nbsp;
                                        Service <input class="form-group" name="occupation" @if($privilegeCard->occupation=='Service') checked @endif type="radio" value="Service">
                                        &nbsp;&nbsp;&nbsp;
                                        Self Employee <input class="form-group" @if($privilegeCard->occupation=='Self Employee') checked @endif name="occupation" type="radio"
                                                             value="Self Employee"> &nbsp;&nbsp;&nbsp;
                                        Student <input class="form-group" name="occupation" type="radio" @if($privilegeCard->occupation=='Student') checked @endif value="Student">
                                        &nbsp;&nbsp;&nbsp;
                                        other
                                        @if($privilegeCard->occupation=='Student'||$privilegeCard->occupation=='Self Employee'||$privilegeCard->occupation=='Service' ||$privilegeCard->occupation=='Business')
                                            <input class="form-group input-sm" type="text" value="">
                                        @else
                                            <input class="form-group input-sm" type="text" value="{{$privilegeCard->occupation}}">

                                        @endif
                                        {{--                                        <input type="text" name="nationalty" class="form-control" placeholder="nationalty" id="position" value="{{$privilegeCard->name}}">--}}
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="comment">Office Name</label>
                                        <input type="text" placeholder="Office Name" name="office_name" id="driver_name"
                                               class="form-control" value="{{$privilegeCard->office_name}}">
                                    </div>
                                    <div class="col-md-3 form-group" style="">
                                        <label for="comment">Office Address</label>
                                        <textarea class="form-control input-sm" name="office_address" id=""
                                                  value="{{\Request::old('comments')}}" type="text" cols="10"
                                                  placeholder="Office Address" rows=1">{{$privilegeCard->office_address}}</textarea>
                                    </div>

                                    <div class="col-md-3 form-group" style="">
                                        <label for="comment">Phone</label>
                                        <input type="number" placeholder="Phone" name="phone" id="driver_name"
                                               class="form-control" value="{{$privilegeCard->phone}}">
                                    </div>


                                </div>


                                <div class="clearfix"></div>
                                <br/><br/>

                                <div class="col-md-12">
                                    <div class="bg-primary"><h5><strong style="padding: 8px">Other Details</strong></h5>
                                    </div>
                                </div>

                                <hr/>
                                <div class="col-md-12 form-group" style="">
                                    <label for="comment">Since when have you been familiar about the indreni
                                        outlets?</label>
                                    <textarea class="form-control input-sm" name="familiar_about_indreni" id=""
                                              value="" type="text" cols="10" placeholder="Since when have you been familiar about the indreni
                                        outlets?"
                                              rows=1">{{$privilegeCard->familiar_about_indreni}}</textarea><br/>
                                </div>
                                <div class="col-md-12 form-group" style="">
                                    <label for="comment">What do you feel about our service?</label>
                                    <textarea class="form-control input-sm" name="feel_about_service" id=""
                                              value="" type="text" cols="10" placeholder="What do you feel about our service?"
                                              rows=1">{{$privilegeCard->feel_about_service}}</textarea><br/>
                                </div>
                                <div class="col-md-12 form-group" style="">
                                    <label for="comment">Do you have any suggestion as to how we could improve our service in future?</label>
                                    <textarea class="form-control input-sm" name="any_suggestion" id=""
                                              value="" type="text" cols="10" placeholder="Do you have any suggestion as to how we could improve our service in future?"
                                              rows=1">{{$privilegeCard->any_suggestion}}</textarea><br/>
                                </div>
                                <div class="col-md-12 form-group" style="">
                                    <label for="comment">By appling for this privilege card how would you utilize it?</label>
                                    <textarea class="form-control input-sm" name="how_would_you_utilize" id=""
                                              value="" type="text" cols="10" placeholder="By appling for this privilege card how would you utilize it?"
                                              rows=1">{{$privilegeCard->how_would_you_utilize}}</textarea><br/>
                                </div>
                                <div class="col-md-12">
                                    <div class="bg-primary"><h5><strong style="padding: 8px">FOR OFFICIAL USE ONLY</strong></h5>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group" style="">
                                    <label for="comment">Customer's Signature</label>
                                    <input type="text" placeholder="Customer's Signature" name="customer_sign" id="driver_name"
                                           class="form-control" value="{{$privilegeCard->customer_sign}}">
                                </div>
                                <div class="col-md-6 form-group" style="">
                                    <label for="comment">Date</label>
                                    <input type="text" placeholder="Date" name="date" id="driver_name"
                                           class="form-control date-toggle-nep-eng datepicker" value="{{$privilegeCard->date}}">
                                </div>
                                <div class="col-md-4 form-group" style="">
                                    <label for="comment">Card No:</label>
                                    <input type="text" placeholder="Card No" name="card_no" id="driver_name"
                                           class="form-control" value="{{$privilegeCard->card_no}}">
                                </div>
                                <div class="col-md-4 form-group" style="">
                                    <label for="comment">Membership type</label>
                                    <input type="text" placeholder="Membership type" name="membership_type" id="driver_name"
                                           class="form-control" value="{{$privilegeCard->membership_type}}">
                                </div>
                                <div class="col-md-4 form-group" style="">
                                    <label for="comment">Valid up to:</label>
                                    <input type="text" placeholder="Valid up to" name="valid_upto" id="driver_name"
                                           class="form-control date-toggle-nep-eng datepicker" value="{{$privilegeCard->valid_upto}}">
                                </div>
                                <div class="col-md-6 form-group" style="">
                                    <label for="comment">Date of issue</label>
                                    <input type="text" placeholder="Date of issue" name="date_of_issue" id="driver_name"
                                           class="form-control date-toggle-nep-eng datepicker" value="{{$privilegeCard->date_of_issue}}">
                                </div>
                                <div class="col-md-6 form-group" style="">
                                    <label for="comment">Approved By</label>
                                    <input type="text" placeholder="Approved By" name="approved_by" id="driver_name"
                                           class="form-control" value="{{$privilegeCard->approved_by}}">
                                </div>

                            </div>
                            <div class="panel-footer footer">
                                <button type="submit" class="btn btn-social btn-foursquare">
                                    <i class="fa fa-save"></i>Update Privilege Card
                                </button>

                                <a class="btn btn-social btn-foursquare" href="/admin/privilege-card"> <i
                                        class="fa fa-times"></i> Cancel </a>
                            </div>
                        </form>
                    </div>
                </div>


            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@section('body_bottom')
    <!-- form submit -->
    @include('partials._body_bottom_submit_bug_edit_form_js')
    @include('partials._date-toggle')


    <script>
        $(document).on('ready', function () {
            // var parentDiv = $(this).parent().parent();
            $(".quantity").each(function (index) {
                var parentDiv = $(this).parent().parent();
                if (isNumeric($(this).val()) && $(this).val() != '')
                    var total = $(this).val() * parentDiv.find('.quantity').val();
                else
                    var total = parentDiv.find('.quantity').val();

                if (isNumeric(total) && total != '') {
                    parentDiv.find('.total').val(total);
                    calcTotal();
                }
                //console.log( index + ": " + $(this).text() );
            });
        });

        const dateRange = {
            <?php $currentFiscalyear = FinanceHelper::cur_fisc_yr();?>
            minDate: `{{ $currentFiscalyear->start_date }}`,
            maxDate: `{{ $currentFiscalyear->end_date }}`
        }
        $('.date-toggle-nep-eng').nepalidatetoggle();

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }


        $(document).on('keyup', '.quantity', function () {
            var parentDiv = $(this).parent().parent();
            if (isNumeric(this.value) && this.value != '') {
                if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                    var total = parentDiv.find('.quantity').val() * this.value;
                } else
                    var total = '';
            } else
                var total = '';


            parentDiv.find('.total').val(total);
            calcTotal();
        });


        $("#addMore").on("click", function () {
            //$($('#orderFields').html()).insertBefore(".multipleDiv");
            $(".multipleDiv").after($('#orderFields #more-tr').html());
            $(".multipleDiv").next('tr').find('select').select2({
                width: '100%'
            });
        });

        $(document).on('click', '.remove-this', function () {
            $(this).parent().parent().parent().remove();
            calcTotal();
        });
        $(document).on('load', '.remove-this', function () {
            $(this).parent().parent().parent().remove();
            calcTotal();
        });

        function calcTotal() {
            var total = 0;
            $(".quantity").each(function (index) {
                if (isNumeric($(this).val()))
                    total += Number($(this).val());
            });
            $('#total_').val(total);
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.client_id').select2();
            calcTotal()
        });

    </script>

    <script type="text/javascript">
        $(function () {
            $('.datepicker').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD'
                , sideBySide: true
                , allowInputToggle: true,
                minDate: dateRange.minDate,
                maxDate: dateRange.maxDate,
            });

        });

        function openwindow() {
            var win = window.open('/admin/clients/modals?relation_type=customer', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
        }

        function HandlePopupResult(result) {
            if (result) {
                let clients = result.clients;
                var option = '';
                for (let c of clients) {
                    option = option + `<option value='${c.id}'>${c.name}</option>`;
                }
                $('#client_id').html(option);
                setTimeout(function () {
                    $('.client_id').select2('destroy');
                    $('#client_id').val(result.lastcreated);
                    $("#ajax_status").after("<span style='color:green;' id='status_update'>Client sucessfully created</span>");
                    $('#status_update').delay(3000).fadeOut('slow');
                    $('.client_id').select2();
                }, 500);
            } else {
                $("#ajax_status").after("<span style='color:red;' id='status_update'>failed to create clients</span>");
                $('#status_update').delay(3000).fadeOut('slow');
            }
        }

        $('#discount_percent').on('change keyup', function () {

            let val = $(this).val();

            if (val.trim() == '') {
                $('#discount_amount').attr('readonly', false);
            } else {
                $('#discount_amount').attr('readonly', true);
            }

            let subtotal = Number($('#sub-total').text());

            let discountAmount = (val / 100) * subtotal;

            $('#discount_amount').val(discountAmount);

            calcTotal();
        });

    </script>
@endsection

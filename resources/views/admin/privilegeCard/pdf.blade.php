<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | Purchase Order</title>
    <style type="text/css">

        @font-face {
            font-family: SourceSansPro;
            src: url(SourceSansPro-Regular.ttf);
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }
        a {
            color: #0087C3;
            text-decoration: none;
        }
        body {
            position: relative;
            width: 18cm;
            height: 24.7cm;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
        }
        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 5px;
        }

        table th,
        table td {
            padding: 3px;
            background:;
            text-align: left;
            border-bottom: 1px solid #FFFFFF;
        }

        table th {
            white-space: nowrap;
            font-weight: normal;
        }

        table td {
            text-align: left;
        }

        table td h3{
            color: #57B223;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

        table .no {
            color: #FFFFFF;
            font-size: 1em;
            background: #57B223;
        }

        table .desc {
            text-align: left;
        }

        table .unit {
            background: #DDDDDD;
        }

        table .qty {
        }

        table .total {
            background: #57B223;
            color: #FFFFFF;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table tbody tr:last-child td {
            border: none;
        }

        table tfoot td {
            padding: 5px 10px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
        }

        table tfoot tr:first-child td {
            border-top: none;
        }

        table tfoot tr:last-child td {
            color: #57B223;
            font-size: 1em;
            border-top: 1px solid #57B223;
            font-weight: bold;

        }

        table tfoot tr td:first-child {
            border: none;
        }

        #thanks{
            font-size: 2em;
            margin-bottom: 50px;
        }
        .profile-pic {
            top: 60px;
            left: 383px;
            /*width: 300px;*/
            /*height: 300px;*/
            position: relative;
            z-index: 100;
            padding-inline-start: 22px;
        }
        .palatinum {
            width: 407px;
            padding-top: 56px;
            position: absolute;
            top: 20px;
            left: 233px;

        }

        #notices{
            padding-left: 6px;
            border-left: 6px solid #0087C3;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        .brand-logo{
            width: 117px;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }
    </style>

</head><body>
<header class="clearfix">
    <table>
        <tr>
            <td width="20%" style="float:left">
                <div class="col-xs-3 brand-logo">
                    <img class="img-responsive" style="width:200px" src="data:image/png;base64,{{ $brandLogo }}">


                </div>
            </td>
            <td width="100%" >
                <div  class=" profile-pic">
                    <img class="img-responsive" style="width: 123px"  src="data:image/png;base64,{{ $profile }}">

                </div>
            </td>
            <td width="40%" style="float:right">
                <div class="palatinum">
                    <img class="img-responsive" style="width: 407px" src="data:image/png;base64,{{ $banner }}" alt="">
                </div>


            </td>

            </tr>

    </table>

    </div>
</header>
<main>

    <table  >
        <tr>
            <td>Name</td>
            <td colspan="4"><input style="width:90%;" type="text" disabled value="{{$privilegeCard->name}}"></td>
        </tr>
        <tr>
            <td>Address</td>
            <td colspan="4" ><input style="width:90%;" type="text" disabled value="{{$privilegeCard->address}}"></td>
        </tr>
        <tr>
            <td>Contact No</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->contact_no}}">
            </td>
            <td> </td>
            <td>Email</td>
            <td >
                <input type="text" style="width:77%;" disabled   value="{{$privilegeCard->email}}">
            </td>
        </tr>

        <tr>
            <td>Date of Birth</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->dob}}">
            </td>
            <td> </td>
            <td>Nationality</td>
            <td >
                <input type="text" style="width:77%;" disabled   value="{{$privilegeCard->nationality}}">
            </td>
        </tr>
        <tr>
            <td>Marital Status</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->married_status}}">
            </td>
            <td> </td>
            <td>Occupation</td>
            <td >
                <input type="text" style="width:77%;" disabled value="{{$privilegeCard->occupation}}">
            </td>
        </tr>

        <tr>
            <td>Qualification</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->qualification}}">
            </td>
            <td> </td>
            <td>Office Name</td>
            <td>
                <input type="text" style="width:77%;" disabled value="{{$privilegeCard->office_name}}">
            </td>
        </tr>

        <tr>
            <td>Office Address</td>
            <td >
                <input type="text"  disabled    value="{{$privilegeCard->office_address}}">
            </td>
            <td> </td>
            <td>Phone</td>
            <td>
                <input type="text"  disabled   value="{{$privilegeCard->phone}}">
            </td>
        </tr>

    </table>
    <strong style="padding-bottom: 8px;padding-left: 2px">OTHER DETAILS</strong><br>
        <div style=" border-bottom: 1px solid #AAAAAA;" >
        </div>
    <table  >

        <tr>
            <td>Since when have you been familiar about the indreni
                outlets?</td>

        </tr>
        <tr>
            <td colspan="4"><input style="width:90%;" type="text" disabled value="{{$privilegeCard->familiar_about_indreni}}"></td>
        </tr>
        <tr>
            <td>What do you feel about our service?</td>
        </tr>
        <tr>

            <td colspan="4" ><input style="width:90%;" type="text" disabled value="{{$privilegeCard->feel_about_service}}"></td>
        </tr>
        <tr>
            <td>Do you have any suggestion as to how we could improve our service in future?</td>
        </tr>
        <tr>

            <td colspan="4" ><input style="width:90%;" type="text" disabled value="{{$privilegeCard->any_suggestion}}"></td>
        </tr>
        <tr>
            <td>By applying for this privilege card how would you utilize it?</td>
        </tr>
        <tr>

            <td colspan="4" ><input style="width:90%;" type="text" disabled value="{{$privilegeCard->how_would_you_utilize}}"></td>
        </tr>
    </table>
    <div class="bg-primary"><strong style="padding-bottom: 8px;padding-left: 6px">FOR OFFICIAL USE ONLY</strong>

    </div>
    <div style=" border-bottom: 1px solid #AAAAAA;" >
    </div>
    <table>
        <tr>
            <td>Customer's Signature</td>
            <td  width="27%">
                <input type="text"  disabled value="{{$privilegeCard->customer_sign}}">
            </td>
            <td> </td>
            <td>Date</td>
            <td width="27%">
                <input type="text" disabled   value="{{$privilegeCard->date}}">
            </td>
        </tr>

        <tr>
            <td>Card No:</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->card_no}}">
            </td>
            <td> </td>
            <td>Membership type</td>
            <td >
                <input type="text" style="width:77%;" disabled   value="{{$privilegeCard->membership_type}}">
            </td>
        </tr>
        <tr>
            <td>Valid up to:</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->valid_upto}}">
            </td>
            <td> </td>
            <td>Date of issue</td>
            <td >
                <input type="text" style="width:77%;" disabled   value="{{$privilegeCard->date_of_issue}}">
            </td>
        </tr>

        <tr>
            <td>Approved By</td>
            <td >
                <input type="text" style="width:90%;" disabled value="{{$privilegeCard->approved_by}}">
            </td>
            <td> </td>


        </tr>
    </table>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <table>
        <tr >
            <td style=" text-align: center;
    background: #ad4582;
    color: #ffffff;">
                Indreni Complex,New Baneshwor,Kathmandu Nepal <br>
                <strong>Tel.</strong>+977014784107 /4780119
            </td>

        </tr>

    </table>


{{--    <div id=""> ___________________________________</div>--}}
{{--    <div id="">Customer Signature</div>--}}
{{--    <div style="text-align: right" id=""> ___________________________________</div>--}}
{{--    <div style="text-align: right" id="">Authorized Signature</div>--}}


</main>
<footer>
</footer>
</body></html>

@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Payment
        <small>Payement Type Index</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>

                <b>
                    <font size="4">Payement type List</font>
                </b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm" title="Import/Export Leads" href="{{ route('admin.hotel.payment-type-create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new payement </strong>
                    </a>
                </div>
            </div>
        </div>

        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
                <tr>
                    <th style="text-align:center;width:20px !important">
                        <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                            <i class="fa fa-check-square-o"></i>
                        </a>
                    </th>
                    <th>ID</th>
                    <th>Payment name</th>
                    <th>Payment Code</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($payment_type as $key=>$pt)
                <tr>
                    <td>
                        <input type="checkbox" name="event_id" value="{{$pu->id}}">
                    </td>
                    <td>{{$pt->payment_type_id}}</td>
                    <td> {{ucfirst(trans($pt->payment_name))}}</td>
                    <td> {{ $pt->payment_code }}</td>
                    <td>
                        @if( $pt->isEditable())<a href="/admin/hotel/payment-type-edit/{{$pt->payment_type_id}}"><i class="fa fa-edit"></i></a>
                        @else
                        <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
                        @endif
                        &nbsp;&nbsp;
                        <a href="{{route('admin.hotel.payment-type-confirm-delete', $pt->payment_type_id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <div style="text-align: center;"> {!! $payment_type->render() !!} </div>

    </div>

    @endsection

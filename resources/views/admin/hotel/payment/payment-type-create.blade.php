@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Payment
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 @if($edit)
 <form method="post" action="{{route('admin.hotel.payment-type-edit',$edit->payment_type_id)}}">
  {{ csrf_field() }}
  <div class="panel panel-custom">
   <div class="panel-heading">

    <div class="row">
       <div class="col-sm-4">
          <div class="form-group">  
            <label class="control-label col-sm-12">Payment Name</label>
              <div class="input-group ">
                 <input type="text" name="payment_name" placeholder="Name" id="name" value="{{$edit->payment_name}}" class="form-control" required>
                  <div class="input-group-addon">
                    <a href="#"><i class="fa fa-stack-exchange"></i></a>
                  </div>
              </div>
          </div>
       </div>
   </div>

   <div class="row">
       <div class="col-sm-4">
          <div class="form-group">  
            <label class="control-label col-sm-12">Payment Code</label>
              <div class="input-group ">
                 <input type="text" name="payment_code" placeholder="Payment Code" id="name" value="{{$edit->payment_code}}" class="form-control" required>
                  <div class="input-group-addon">
                    <a href="#"><i class="fa fa-stack-exchange"></i></a>
                  </div>
              </div>
          </div>
       </div>
   </div>

   <div class="row">
       <div class="col-md-12">
          <div class="form-group">
              <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update</button>
          </div>
      </div>
  </div>

  </div>
  </div>
</form>
 @else
         <form method="post" action="{{route('admin.hotel.payment-type-create')}}">
            {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">

    <div class="row">
         <div class="col-sm-4">
            <div class="form-group">  
              <label class="control-label col-sm-12">Payment Name</label>
                <div class="input-group ">
                  <input type="text" name="payment_name" placeholder="Payment name" id="name" class="form-control" required>
                    <div class="input-group-addon">
                      <a href="#"><i class="fa fa-stack-exchange"></i></a>
                    </div>
                </div>
            </div>
         </div>
    </div>

     <div class="row">
         <div class="col-sm-4">
            <div class="form-group">  
              <label class="control-label col-sm-12">Payment Code</label>
                <div class="input-group ">
                  <input type="text" name="payment_code" placeholder="Payment Code" id="payment_code" class="form-control" required>
                    <div class="input-group-addon">
                      <a href="#"><i class="fa fa-stack-exchange"></i></a>
                    </div>
                </div>
            </div>
         </div>
    </div>

 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>
@endif
  @endsection
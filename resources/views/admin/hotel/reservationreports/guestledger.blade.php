<!DOCTYPE html><html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css"> 
@font-face {
  font-family: Arial;
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  width: 18cm;  
  height: 24.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 8px; 
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}

table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223; 
  font-weight: bold;

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}



  </style>
  </head><body>
     
    <header class="clearfix">
        <table>
            <tr>
              <td width="20%" style="float:left">
                <div id="logo">
                  <img style="max-width: 150px" src="{{public_path()}}/{{ '/org/'.$organization->logo }}">
                </div>
              </td>
              <td width="50%" style="text-align:center">
                <div>
                 <h2 style="text-align: center;">Guest Ledger Detail</h2>
                </div>
              </td>
              <td width="30%" style="text-align: right">
                <div  id="company">
                  <h4 class="name">{{ env('APP_COMPANY') }} </h4>
                  <div>{{ env('APP_ADDRESS1') }}</div>
                  <div>{{ env('APP_ADDRESS2') }}</div>
                  <div>Seller's PAN: {{ env('TPID') }}</div>
                  <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>
                </div>
              </td>
            </tr>

      </table> 

      </div>
    </header>
    <main>
     
      <table id="customers" >


           <tr style="background-color: #3F51B5; border:1px solid #ccc;color: white;;">
            <th>Room No.</th>
            <th>Name</th> 
            <th>Arr. Date</th>
            <th>Dep. Date</th>
            <th>Persons</th>
            <th>Room Charges</th>
            <th>Other Charges</th>
            <th>Credit</th>
            <th>Balance</th>
            <th>Rate</th>
            <th>Payment Method</th>
            <th>Folio Status</th>
          </tr>
      
        <tbody>
          @if($reservations)
            @foreach($reservations as $rev)
              <tr>
                <td>{{$rev->room_num}}</td>
                <td>{{$rev->guest_name}}</td>
                <td>{{$rev->check_in}}</td>
                <td>{{$rev->check_out}}</td>
                <td>{{$rev->occupancy}}</td>
                <?php 
                   $roomcharges = \TaskHelper::RoomCharges($rev->id);
                ?>
                <td>{{$roomcharges}}</td>  
                <?php 
                    $value = \TaskHelper::OtherCharges($rev->id);  
                 ?>
                <td>{{$value}}</td>  
                <?php  
                    $data = \TaskHelper::CreditAmount($rev->id);
                 ?>

                <td>{{$data['credit']}}</td>
                <td>{{$data['balance']}}</td>
                <td>{{$rev->room_rate}}</td>
                <td>{{$rev->paymenttype->payment_name}}</td>
                <td></td>
              </tr>
            @endforeach
          @endif
         
        
        </tbody>

      </table>
      

    </main>
    <footer>
      Guest Ledgers was created on MEROCRM.
    </footer></body></html>
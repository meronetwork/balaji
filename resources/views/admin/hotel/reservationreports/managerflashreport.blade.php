<!DOCTYPE html><html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css"> 
@font-face {
  font-family: Arial;
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  width: 18cm;  
  height: 24.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}

table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1em; 
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223; 
  font-weight: bold;

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}



  </style>
  </head><body>
     
    <header class="clearfix">
        <table>
            <tr>
              <td width="20%" style="float:left">
                <div id="logo">
                  <img style="max-width: 150px" src="{{public_path()}}/{{ '/org/'.$organization->logo }}">
                </div>
              </td>
              <td width="50%" style="text-align:center">
                <div>
                 <h2 style="text-align:center;">Manager Flash Report
                  <br><small>{{ date( 'Y-m-d') }} {{ date('H:i:s')  }} </small><br>
                  <small>{{ TaskHelper::getNepaliDate(  date( 'Y-m-d'))}} {{ date('H:i:s')  }} </small>

                 </h2>
                </div>
              </td>
              <td width="30%" style="text-align: right">
                <div  id="company">
                  <h4 class="name">{{ env('APP_COMPANY') }} </h4>
                  <div>{{ env('APP_ADDRESS1') }}</div>
                  <div>{{ env('APP_ADDRESS2') }}</div>
                  <div>Seller's PAN: {{ env('TPID') }}</div>
                  <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>
                </div>
              </td>
            </tr>
        </table> 

      </div>
    </header>
    <main>
     
      <table id="customers">

        <thead>
          <tr>
            <th></th>
            <th><b>{{$getFlashReportCurrent['dateInfo']['today_date']['year'] }} Day</b></th>
            <th><b>{{$getFlashReportCurrent['dateInfo']['today_date']['year'] }} Month</b></th>
            <th><b>{{$getFlashReportCurrent['dateInfo']['today_date']['year'] }} Year</b></th>
            <th><b>{{$getFlashReportPrev['dateInfo']['today_date']['year'] }} Day</b></th>
            <th><b>{{$getFlashReportPrev['dateInfo']['today_date']['year'] }} Month</b></th>   
            <th><b>{{$getFlashReportPrev['dateInfo']['today_date']['year'] }} Year</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Total Rooms in Hotel</td> 
            <td>{{ $getFlashReportCurrent['totalRoom']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['totalRoom']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['totalRoom']['year'] }}</td>
            <td>{{ $getFlashReportPrev['totalRoom']['today'] }}</td>
            <td>{{ $getFlashReportPrev['totalRoom']['month'] }}</td>
            <td>{{ $getFlashReportPrev['totalRoom']['year'] }}</td>
          </tr>
          <tr>
            <td>Rooms Occupied</td>
            <td>{{ $getFlashReportCurrent['occupiedRoom']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['occupiedRoom']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['occupiedRoom']['year'] }}</td>
            <td>{{ $getFlashReportPrev['occupiedRoom']['today'] }}</td>
            <td>{{ $getFlashReportPrev['occupiedRoom']['month'] }}</td>
            <td>{{ $getFlashReportPrev['occupiedRoom']['year'] }}</td>
          </tr>
          <tr>
            <td>Available Rooms</td>
            <td>{{ $getFlashReportCurrent['availableRoom']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['availableRoom']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['availableRoom']['year'] }}</td>
            <td>{{ $getFlashReportPrev['availableRoom']['today'] }}</td>
            <td>{{ $getFlashReportPrev['availableRoom']['month'] }}</td>
            <td>{{ $getFlashReportPrev['availableRoom']['year'] }}</td>
          </tr>
          <tr>
            <td>Complimentary Rooms</td>
             <td>{{ $getFlashReportCurrent['roomByGuestType']['COM']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['roomByGuestType']['COM']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['roomByGuestType']['COM']['year'] }}</td>
            <td>{{ $getFlashReportPrev['roomByGuestType']['COM']['today'] }}</td>
            <td>{{ $getFlashReportPrev['roomByGuestType']['COM']['month'] }}</td>
            <td>{{ $getFlashReportPrev['roomByGuestType']['COM']['year'] }}</td>
          </tr>
          <tr>
            <td>In-House Adults</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['today']->adult ?? 0 }}</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['month']->adult ?? 0 }}</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['year']->adult ?? 0 }}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['today']->adult ?? 0 }}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['month']->adult ?? 0 }}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['year']->adult ?? 0 }}</td>
          </tr>
          <tr>
            <td>In-House Children</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['today']->child ?? 0 }}</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['month']->child ?? 0 }}</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['year']->child ?? 0}}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['today']->child ?? 0}}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['month']->child ?? 0}}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['year']->child ?? 0 }}</td>
          </tr>
          <tr>
            <td>Total In-House Persons</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['today']->person ?? 0}}</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['month']->person ?? 0}}</td>
            <td>{{ $getFlashReportCurrent['inHouseperson']['year']->person ?? 0}}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['today']->person ?? 0}}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['month']->person ?? 0}}</td>
            <td>{{ $getFlashReportPrev['inHouseperson']['year']->person ?? 0}}</td>
          </tr>
          <tr>
            <td>Individual Persons In-House</td>
            <td>{{ $getFlashReportCurrent['inHouseIndividualperson']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['inHouseIndividualperson']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['inHouseIndividualperson']['year'] }}</td>
            <td>{{ $getFlashReportPrev['inHouseIndividualperson']['today'] }}</td>
            <td>{{ $getFlashReportPrev['inHouseIndividualperson']['month'] }}</td>
            <td>{{ $getFlashReportPrev['inHouseIndividualperson']['year'] }}</td>
          </tr>
          <tr>
            <td>VIP Persons In-House</td>
            <td>{{ $getFlashReportCurrent['roomByGuestType']['VIP']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['roomByGuestType']['VIP']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['roomByGuestType']['VIP']['year'] }}</td>
            <td>{{ $getFlashReportPrev['roomByGuestType']['VIP']['today'] }}</td>
            <td>{{ $getFlashReportPrev['roomByGuestType']['VIP']['month'] }}</td>
            <td>{{ $getFlashReportPrev['roomByGuestType']['VIP']['year'] }}</td>
          </tr>
           <tr>
            <td>% Room Occupied</td>
            <td>{{ $getFlashReportCurrent['roomOccupiedPercent']['today'] }}%</td>
            <td>{{ $getFlashReportCurrent['roomOccupiedPercent']['month'] }}%</td>
            <td>{{ $getFlashReportCurrent['roomOccupiedPercent']['year'] }}%</td>
            <td>{{ $getFlashReportPrev['roomOccupiedPercent']['today'] }}%</td>
            <td>{{ $getFlashReportPrev['roomOccupiedPercent']['month'] }}%</td>
            <td>{{ $getFlashReportPrev['roomOccupiedPercent']['year'] }}%</td>
          </tr>
          <tr>
            <td>No Show Rooms</td>
            <td>{{ $getFlashReportCurrent['noSHow']['today']->room ?? 0}}</td>
            <td>{{ $getFlashReportCurrent['noSHow']['month']->room ?? 0}}</td>
            <td>{{ $getFlashReportCurrent['noSHow']['year']->room ?? 0}}</td>
            <td>{{ $getFlashReportPrev['noSHow']['today']->room ?? 0}}</td>
            <td>{{ $getFlashReportPrev['noSHow']['month']->room ?? 0}}</td>
            <td>{{ $getFlashReportPrev['noSHow']['year']->room ?? 0}}</td>
          </tr>
          <tr>
            <td>No Show Persons</td>
            <td>{{ $getFlashReportCurrent['noSHow']['today']->person ?? 0 }}</td>
            <td>{{ $getFlashReportCurrent['noSHow']['month']->person  ?? 0}}</td>
            <td>{{ $getFlashReportCurrent['noSHow']['year']->person ?? 0 }}</td>
            <td>{{ $getFlashReportPrev['noSHow']['today']->person ?? 0 }}</td>
            <td>{{ $getFlashReportPrev['noSHow']['month']->person ?? 0 }}</td>
            <td>{{ $getFlashReportPrev['noSHow']['year']->person ?? 0 }}</td>
          </tr>
          <tr>
            <td>Cancelled Reservations for Today</td>
            <td>{{ $getFlashReportCurrent['cancelledReservation']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['cancelledReservation']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['cancelledReservation']['year'] }}</td>
            <td>{{ $getFlashReportPrev['cancelledReservation']['today'] }}</td>
            <td>{{ $getFlashReportPrev['cancelledReservation']['month'] }}</td>
            <td>{{ $getFlashReportPrev['cancelledReservation']['year'] }}</td>
          </tr>
          <tr>
            <td>Reservations Made Today</td>

            <td>{{ $getFlashReportCurrent['reservationMade']['today'] }}</td>
            <td>{{ $getFlashReportCurrent['reservationMade']['month'] }}</td>
            <td>{{ $getFlashReportCurrent['reservationMade']['year'] }}</td>
            <td>{{ $getFlashReportPrev['reservationMade']['today'] }}</td>
            <td>{{ $getFlashReportPrev['reservationMade']['month'] }}</td>
            <td>{{ $getFlashReportPrev['reservationMade']['year'] }}</td>
          </tr>
           <tr>
            <td>Arrival Rooms for Tomorrow</td>
            <td>{{ $getFlashReportCurrent['reservationTomorrow']['tomorrowArrive']->res ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
            <td>{{ $getFlashReportPrev['reservationTomorrow']['tomorrowArrive']->res ?? 0 }}</td>
            <td>-</td>

          </tr>
          <tr>
            <td>Arrival Persons for Tomorrow</td>
            <td>{{ $getFlashReportCurrent['reservationTomorrow']['tomorrowArrive']->person ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
            <td>{{ $getFlashReportPrev['reservationTomorrow']['tomorrowArrive']->person ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
          </tr>
          <tr>
            <td>Departure Rooms for Tomorrow</td>
            <td>{{ $getFlashReportCurrent['reservationTomorrow']['tomorrowDepart']->res ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
            <td>{{ $getFlashReportPrev['reservationTomorrow']['tomorrowDepart']->res ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
          </tr>
          <tr>
            <td>Departure Persons for Tomorrow</td>
           <td>{{ $getFlashReportCurrent['reservationTomorrow']['tomorrowDepart']->person ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
            <td>{{ $getFlashReportPrev['reservationTomorrow']['tomorrowDepart']->person ?? 0 }}</td>
            <td>-</td>
            <td>-</td>
          </tr>
          <tr>
            <th colspan="7" style="font-weight: 800;">F & B OUTLETS ( ALL SALE)</th>
          </tr>
          @foreach($getFlashReportCurrent['outletIncome'] as $key=>$value)
          <tr>
            <td>{{ $key }} ( {{ $value['outletName'] }} )</td>
            <td>{{ $value['today']->total ?? 0 }}</td>
            <td>{{ $value['month']->total ?? 0 }}</td>
            <td>{{ $value['year']->total ?? 0 }}</td>
            <td>{{ $getFlashReportPrev[$key]['today']->total ?? 0 }}</td>
            <td>{{  $getFlashReportPrev[$key]['month']->total ?? 0  }}</td>
            <td>{{  $getFlashReportPrev[$key]['year']->total ?? 0  }}</td>
          </tr>
          @endforeach
          <tr>
            <th colspan="7" style="font-weight: 800;">Hotel OUTLETS</th>
          </tr>

          <tr>
            <td>Room Charge</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['today']['roomCharge'] }}</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['month']['roomCharge'] }}</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['year']['roomCharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['today']['roomCharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['month']['roomCharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['year']['roomCharge'] }}</td>
          </tr>

          <tr>
            <td>Restro Charge</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['today']['restroCharge'] }}</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['month']['restroCharge'] }}</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['year']['restroCharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['today']['restroCharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['month']['restroCharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['year']['restroCharge'] }}</td>
          </tr>

          <tr>
            <td>Extra Charge</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['today']['extracharge'] }}</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['month']['extracharge'] }}</td>
            <td>{{ $getFlashReportCurrent['hotelOutletIncome']['year']['extracharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['today']['extracharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['month']['extracharge'] }}</td>
            <td>{{ $getFlashReportPrev['hotelOutletIncome']['year']['extracharge'] }}</td>
          </tr>
         
        </tbody>

      </table>
      

    </main>
    <footer>
      was created on MEROCRM.
    </footer></body></html>
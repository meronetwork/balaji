<!DOCTYPE html><html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css"> 
@font-face {
  font-family: Arial;
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  width: 18cm;  
  height: 24.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}

table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223; 
  font-weight: bold;

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}



  </style>
  </head><body>
     
    <header class="clearfix">
        <table>
            <tr>
              <td width="20%" style="float:left">
                <div id="logo">
                  <img style="max-width: 150px" src="{{public_path()}}/{{ '/org/'.$organization->logo }}">
                </div>
              </td>
              <td width="50%" style="text-align:center">
                <div>
                 <h2 style="text-align:center;">Revenue Reports Card </h2>
                 <h4>Weekly Report: {{Carbon\Carbon::today()->format('d M Y')}} </h4>
                </div>
              </td>
              <td width="30%" style="text-align: right">
                <div  id="company">
                  <h4 class="name">{{ env('APP_COMPANY') }} </h4>
                  <div>{{ env('APP_ADDRESS1') }}</div>
                  <div>{{ env('APP_ADDRESS2') }}</div>
                  <div>Seller's PAN: {{ env('TPID') }}</div>
                  <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>
                </div>
              </td>
            </tr>
        </table> 

      </div>
    </header>
    <main>
     
      <table id="customers">
        <thead>
          <tr>
            <th>Chanels</th>
            <th><b>Rm Nights</b></th>
            <th><b>ADR</b></th>
            <th><b>GRR</b></th>
            <th><b>Contrib (%)</b></th>
          </tr>
        </thead>
        <?php 
               $total_nights = 0;
               $total_adr = 0;
               $adr_count = 0;
     
           ?>
        <tbody>
            @if($sources)
                @foreach($sources as $source)
                  <tr>
                    <td>{{$source->name}}</td> 
                    <?php 
                       $nights = \TaskHelper::rmNights($source->id);
                       $total_nights = $total_nights + $nights;
                     ?>
                    <td>{{$nights}}</td>
                     <?php 
                        $adr = \TaskHelper::ADR($source->id);
                        $total_adr = $total_adr + $adr;

                        if($adr){
                              $adr_count = $adr_count+1;
                        }
                     ?>
                    <td>{{$adr}}</td>
                    <td>{{\TaskHelper::GRR($source->id)}}</td>
                    <td></td>
                  </tr>
                @endforeach
            @endif
            <tr>
              <td>Total</td>
              <td>{{$total_nights}}</td>
              <td>{{ $total_adr/$adr_count }}</td>
              <td></td>
              <td></td>
            </tr>
        </tbody>

      </table>
      

    </main>
    <footer>
       was created on MEROCRM.
    </footer></body></html>
@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ env('APP_COMPANY')}} Report Centre
        <small>{!! $page_description ?? "Page description" !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>


<div class='row'>

    <div class='col-md-6'>
        <!-- Box -->

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Hotel Reports </h3> 
                
                    <div class="row">
                        <div class="col-md-6">
                            <input type="date" name="date"  placeholder="Date" value="{{date('Y-m-d')}}"
                            id='selected_dates'
                            class="form-control" 
                            >
                        </div>
                        
                    </div>
               
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="cases-table">
                        <thead>
                            <tr>
                                <th>Report Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> {!! link_to_route('admin.hotel.reservation.reports.guestledgers', 'Guest Ledger', [], []) !!}</td>
                                <td>Guest Ledgers</td>
                            </tr>
                            <tr>
                                <td> {!! link_to_route('admin.hotel.reservation.reports.managerflashreport', 'Manager Flash Reports', [], []) !!}</td>
                                <td>Manager Flash Reports</td>
                            </tr>
                            <tr>
                                <td>{!! link_to_route('admin.hotel.reservation.reports.roomflashreport', 'Room Flash Report', [], []) !!}</td>
                                <td>Room Flash Report</td>
                            </tr>

                            <tr>
                                <td> 
                                    <a data-href="{{route('admin.hotelsales.index')}}" 
                                    onclick="dowloadSales(this);" href="#">Sales & Revenue  Report</a>

                              </td>
                                <td>Daily Sales & Revenue Report</td>
                            </tr>

                            <tr>
                                <td> {!! link_to_route('admin.hotel.reservation.reports.revenuereports', 'Revenue Report', [], []) !!}</td>
                                <td>Revenue Report</td>
                            </tr>
                            

                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->

          <!-- /.box -->
        </div>
   

     <div class='col-md-6'>
        <!-- Box -->

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Account Reports </h3> 
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="cases-table">
                        <thead>
                            <tr>
                                <th>Report Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td> <a href="/admin/debtors_lists"> Debtors List</a> </td>
                                <td>Debtors Ledgers</td>
                            </tr>

                            <tr>
                                <td> <a href="/admin/purchase-book"> Purchase book</a> </td>
                                <td>Purchase Book</td>
                            </tr>
                            <tr>
                                <td> <a href="/admin/orders/alloutlet"> Sales book</a> </td>
                                <td>Sales Book</td>
                            </tr>

                            <tr>
                                <td> <a href="/admin/orders/returnpos"> Credit Notes </a> </td>
                                <td>Sales Return Book</td>
                            </tr>


                            <tr>
                                <td> <a href="/admin/cash_in_out"> Day Book </a> </td>
                                <td>Day Book and Cash Flow</td>
                            </tr>
            
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->

          <!-- /.box -->
        </div>
   


</div><!-- /.row -->


<div class='row'>
<div class='col-md-6'>
        <!-- Box -->

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Restaurant Reports </h3> 
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="cases-table">
                        <thead>
                            <tr>
                                <th>Report Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td> <a href="/admin/posSummaryAnalysis"> Sales Analysis</a> </td>
                                <td>Outlets sales analysis</td>
                            </tr>

                            <tr>
                                <td> <a href="/admin/posSummaryAmount">Sales Summary</a> </td>
                                <td>Payment summary datewise</td>
                            </tr>
                            
                            <tr>
                                <td>{!! link_to_route('admin.hotel.reservation.reports.resturantsales', 'Resturant Sales', [], []) !!}</td>
                                <td>Resturant Sales</td>
                            </tr>
            
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->

          <!-- /.box -->
</div>

<div class='col-md-6'>
        <!-- Box -->

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">System Reports </h3> 
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="cases-table">
                        <thead>
                            <tr>
                                <th>Report Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td> <a href="/admin/audit">User Activity Audit</a> </td>
                                <td>Audit Trails</td>
                            </tr>

                            <tr>
                                <td> <a href="/admin/errors">Error Exceptions</a> </td>
                                <td>Exceptions Report</td>
                            </tr>
                            

            
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->

          <!-- /.box -->
</div>

</div>

<div class="row">
<div class='col-md-6'>
        <!-- Box -->

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Inventory Reports </h3> 
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="cases-table">
                        <thead>
                            <tr>
                                <th>Report Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td> <a target="_blank" href="/admin/products">inventory Items</a> </td>
                                <td>Items Report</td>
                            </tr>

                            
                            

            
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->

          <!-- /.box -->
</div>
    </div>


<script type="text/javascript">
    
    function dowloadSales(ev)
    {   

        var url = $(ev).attr('data-href');

        let date = $('#selected_dates').val();
        
        url = `${url}?date=${date}`;
        location.href = url;
        
        return;

    }


</script>
@endsection

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
table {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
}
table, th, td {
  border: 1px solid black;
}
table th{
	text-align: left;
}
</style>
<body>
@php 
function total($arr)
{


	return collect($arr)->sum('total');
}

@endphp


<table>
	<thead>
		<tr>
			<th>{{ env('APP_COMPANY') }}</th>
			<th colspan="6" style="text-align: center;">
				Daily Sales Report<br>
				<small>As of {{ date('Y-m-d',strtotime($start_date)) }}</small>
				<small>Download At {{date('H:i:s')}}</small>
			</th>
		
		</tr>
		<tr>
			<th></th>
			<th colspan="3" style="text-align: center;">----This Year----</th>
			<th colspan="3" style="text-align: center;">----Previous Year----</th>
		</tr>
		<tr>
			<th>Description</th>
			<th>Day</th>
			<th>Month</th>
			<th>Year</th>
			<th>Day</th>
			<th>Month</th>
			<th>Year</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Rooms</td>
			<td>{{ $dataArrCurrent['today']['room_sale'] }}</td>
			<td>{{ $dataArrCurrent['month']['room_sale'] }}</td>
			<td>{{ $dataArrCurrent['year']['room_sale'] }}</td>
			<td>{{ $dataArrPrevious['today']['room_sale'] }}</td>
			<td>{{ $dataArrPrevious['month']['room_sale'] }}</td>
			<td>{{ $dataArrPrevious['year']['room_sale'] }}</td>
		</tr>
		<tr>
			<td>Food</td>
			<td>{{ $dataArrCurrent['today']['food_sales']['total'] }}</td>
			<td>{{ $dataArrCurrent['month']['food_sales']['total'] }}</td>
			<td>{{ $dataArrCurrent['year']['food_sales']['total'] }}</td>
			<td>{{ $dataArrPrevious['today']['food_sales']['total'] }}</td>
			<td>{{ $dataArrPrevious['month']['food_sales']['total'] }}</td>
			<td>{{ $dataArrPrevious['year']['food_sales']['total'] }}</td>
		</tr>
		
		
			@foreach($availableArticles as $key=>$value)
			<tr>
			<td>
				{{ $value->description }}
			</td>
			<td>{{ $dataArrCurrent['today']['article_sale']->where('product_id',$value->id)->first()->total ?? '0' }}</td>
			<td>{{ $dataArrCurrent['month']['article_sale']->where('product_id',$value->id)->first()->total ?? '0' }}</td>
			<td>{{ $dataArrCurrent['year']['article_sale']->where('product_id',$value->id)->first()->total ?? '0' }}</td>
			<td>{{ $dataArrPrevious['today']['article_sale']->where('product_id',$value->id)->first()->total ?? '0' }}</td>
			<td>{{ $dataArrPrevious['today']['article_sale']->where('product_id',$value->id)->first()->total ?? '0' }}</td>
			<td>{{ $dataArrPrevious['year']['article_sale']->where('product_id',$value->id)->first()->total ?? '0' }}</td>
			</tr>
			@endforeach
			<tr>
				<th colspan="7"  style="text-align: left">Food Sales</th>
			</tr>
				@foreach($dataArrCurrent['today']['food_sales_by_outlet'] as $key=>$value)



				<tr>
				<td>{{ $value['name'] }}</td>
				<td>{{  $dataArrCurrent['today']['food_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrCurrent['month']['food_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrCurrent['year']['food_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrPrevious['today']['food_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrPrevious['month']['food_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrPrevious['year']['food_sales_by_outlet'][$key]['total']  }}</td>
				</tr>
				@endforeach
				<tr>
				<td><b>Food Total</b></td>
				<td>{{  total($dataArrCurrent['today']['food_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrCurrent['month']['food_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrCurrent['year']['food_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrPrevious['today']['food_sales_by_outlet']) }}</td>
				<td>{{  total($dataArrPrevious['month']['food_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrPrevious['year']['food_sales_by_outlet'] )}}</td>
				</tr>
			<tr>

				<th colspan="7"  style="text-align: left">Beverage Sales</th>
			</tr>
				@foreach($dataArrCurrent['today']['beverage_sales_by_outlet'] as $key=>$value)
				<tr>
				<td>{{ $value['name'] }}</td>
				<td>{{  $dataArrCurrent['today']['beverage_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrCurrent['month']['beverage_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrCurrent['year']['beverage_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrPrevious['today']['beverage_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrPrevious['month']['beverage_sales_by_outlet'][$key]['total']  }}</td>
				<td>{{  $dataArrPrevious['year']['beverage_sales_by_outlet'][$key]['total']  }}</td>
				</tr>
				@endforeach

				<tr>
				<td><b>Beverage Total</b></td>
				<td>{{  total($dataArrCurrent['today']['beverage_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrCurrent['month']['beverage_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrCurrent['year']['beverage_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrPrevious['today']['beverage_sales_by_outlet']) }}</td>
				<td>{{  total($dataArrPrevious['month']['beverage_sales_by_outlet'])  }}</td>
				<td>{{  total($dataArrPrevious['year']['beverage_sales_by_outlet'] )}}</td>
				</tr>
			<tr>
				<th colspan="7" style="text-align: left">Tax Detail</th>
			</tr>
			<tr>
				<td>Service Charge</td>
				<td>{{ $dataArrCurrent['today']['tax_summary']->t_serviceCharge }}</td>
				<td>{{ $dataArrCurrent['month']['tax_summary']->t_serviceCharge }}</td>
				<td>{{ $dataArrCurrent['year']['tax_summary']->t_serviceCharge }}</td>
				<td>{{ $dataArrPrevious['today']['tax_summary']->t_serviceCharge }}</td>
				<td>{{ $dataArrPrevious['month']['tax_summary']->t_serviceCharge }}</td>
				<td>{{ $dataArrPrevious['year']['tax_summary']->t_serviceCharge }}</td>
			</tr>
			
			<tr>
				<td>Value Added Tax</td>
				<td>{{ $dataArrCurrent['today']['tax_summary']->t_taxAmount }}</td>
				<td>{{ $dataArrCurrent['month']['tax_summary']->t_taxAmount }}</td>
				<td>{{ $dataArrCurrent['year']['tax_summary']->t_taxAmount }}</td>
				<td>{{ $dataArrPrevious['today']['tax_summary']->t_taxAmount }}</td>
				<td>{{ $dataArrPrevious['month']['tax_summary']->t_taxAmount }}</td>
				<td>{{ $dataArrPrevious['year']['tax_summary']->t_taxAmount }}</td>
			</tr>
			<tr>
				<th colspan="7" style="text-align: left">Sales Collection</th>
			</tr>

			@foreach($dataArrCurrent['today']['sales_by_collection']['paid_by_total'] as $key=>$value)
			<tr>
			<td>{{ ucfirst($key) }}</td>
			<td>{{ $dataArrCurrent['today']['sales_by_collection']['paid_by_total'][$key] }}</td>
			<td>{{ $dataArrCurrent['month']['sales_by_collection']['paid_by_total'][$key] }}</td>
			<td>{{ $dataArrCurrent['year']['sales_by_collection']['paid_by_total'][$key] }}</td>
			<td>{{ $dataArrPrevious['today']['sales_by_collection']['paid_by_total'][$key] }}</td>
			<td>{{ $dataArrPrevious['month']['sales_by_collection']['paid_by_total'][$key] }}</td>
			<td>{{ $dataArrPrevious['year']['sales_by_collection']['paid_by_total'][$key] }}</td>
			</tr>
			@endforeach
			<tr>
			<td><b>Sales Collection Total</b></td>
			<td>{{ array_sum($dataArrCurrent['today']['sales_by_collection']['paid_by_total']) }}</td>
			<td>{{ array_sum($dataArrCurrent['month']['sales_by_collection']['paid_by_total']) }}</td>
			<td>{{ array_sum($dataArrCurrent['year']['sales_by_collection']['paid_by_total']) }}</td>
			<td>{{ array_sum($dataArrPrevious['today']['sales_by_collection']['paid_by_total'] )}}</td>
			<td>{{ array_sum($dataArrPrevious['month']['sales_by_collection']['paid_by_total'] ) }}</td>
			<td>{{ array_sum($dataArrPrevious['year']['sales_by_collection']['paid_by_total']) }}</td>
			</tr>
	</tbody>
</table>



</body>
</html>
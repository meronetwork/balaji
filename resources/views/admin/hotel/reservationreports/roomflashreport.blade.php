<!DOCTYPE html>
<html><head>
</head><body>

<style type="text/css">
	table, td, th {
	border: 1px solid #e5e5e5e5;
  padding-left: 2px;
}

table {
  width: 100%;
  border-collapse: collapse;
}
</style>
<div class="container" style="padding-right: -10px;">
	
<table class="table-bordered">
<thead>
<tr>
	<th colspan="11" style="text-align: center;">
		ROOM FLASH REPORT OF
	</th>
</tr>
<tr>
	<th colspan="11" style="text-align: center;">
		{{ date('Y-m-d',strtotime('+1 day')) }} As On {{ date('Y-m-d') }}
	</th>
</tr>
<tr>
	<th>FULL NAME</th>
	<th>MBL</th>
	<th>AGENT</th>
	<th>ROOMS</th>
	<th>DEP DATE</th>
	<th>TIME</th>
	<th>NAT</th>
	<th>PAX</th>
	<th>RESV #</th>
</tr>
</thead>
<tbody>
	<tr>
		<th colspan="11" style="text-align: center;"><u>IN HOUSE GUEST</u></th>	
	</tr>
	@foreach($inHouseGuest as $key=>$res)
	<tr>
		<th>@if($res->guest)  {{ $res->guest->first_name}} {{ $res->guest->last_name}} @else
			{{ strtoupper($res->guest_name)}}
			@endif
		</th>
		<th>{{ $res->rateplan->package_name }}</th>
		<th>{{$res->agent->name ?? ''}}</th>
		<th>{{$res->room->room_type->type_code ?? ''}}{{ $res->room_num  }}</th>
		<th style="white-space: nowrap;">{{ strtotime($res->book_out) ? date('d-M-Y',strtotime($res->book_out)) : '-'  }}</th>
		<th>{{ $res->check_out_time }}</th>
		<th>{{ $res->nationality }}</th>
		<th>{{ $res->occupancy + $res->occupancy_child  }}</th>
		<th>{{ $res->id }}</th>
	</tr>
	@endforeach
		<tr>
		<th colspan="11" style="text-align: center;"><u>EXPECTED ARRIVAL</u></th>	
	</tr>
	@foreach($expectedArrival as $key=>$res)
	<tr>
		<th>@if($res->guest)  {{ $res->guest->first_name}} {{ $res->guest->last_name}} @else
			{{ strtoupper($res->guest_name)}}
			@endif
		</th>
		<th>{{ $res->rateplan->package_name }}</th>
		<th>{{$res->agent->name}}</th>
		<th>{{$res->room->room_type->type_code}}{{ $res->room_num  }}</th>
		<th style="white-space: nowrap;">{{ strtotime($res->book_out) ? date('d-M-Y',strtotime($res->book_out)) : '-'  }}</th>
		<th>{{ $res->check_out_time }}</th>
		<th>{{ $res->nationality }}</th>
		<th>{{ $res->occupancy + $res->occupancy_child  }}</th>
		<th>{{ $res->id }}</th>
	</tr>
	@endforeach
	<tr>
		<th colspan="11" style="text-align: center;"><u>EXPECTED DEPATURE</u></th>	
	</tr>
	@foreach($expectedDepature as $key=>$res)
	<tr>
		<th>@if($res->guest)  {{ $res->guest->first_name}} {{ $res->guest->last_name}} @else
			{{ strtoupper($res->guest_name)}}
			@endif
		</th>
		<th>{{ $res->rateplan->package_name }}</th>
		<th>{{$res->agent->name}}</th>
		<th>{{$res->room->room_type->type_code}}{{ $res->room_num  }}</th>
		<th style="white-space: nowrap;">{{ strtotime($res->book_out) ? date('d-M-Y',strtotime($res->book_out)) : '-'  }}</th>
		<th>{{ $res->check_out_time }}</th>
		<th>{{ $res->nationality }}</th>
		<th>{{ $res->occupancy + $res->occupancy_child  }}</th>
		<th>{{ $res->id }}</th>
	</tr>
	@endforeach

</tbody>
</table>
</div>
</body></html>
@extends('layouts.master')


@section('content')
    <link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset("/bower_components/admin-lte/plugins/datatables/buttons.dataTables.css") }}" rel="stylesheet"
          type="text/css"/>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
            House Keeping Status
                <smallHouse Keeping Status Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
 <div class="box-header with-border">



<table class="table table-hover table-no-border table-striped" id="leads-table">
<thead>
    <tr class="bg-info">
        <th>Room number</th>
        <th>Room type</th>
        <th>Availability </th>
         <th>House status</th>
        <th>HK Remarks</th>
        <th>Housekeeper </th>
        <th>Blocked Status</th>
    </tr>
</thead>


<tbody>
    @foreach($rooms as $k => $st)

    <?php
    $room_id = $st['room_id'];
    $check = \App\Models\RoomBlock::where('room_id',$room_id)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first();
    ?>
    <tr>

      <td><b class="badge bg-primary">{{$st['room_number']}}</b>&nbsp;{{$st['type_code']}}</td>
      <td><a href="{{route('admin.hotel.house-keeping-edit',$room_id)}}">{{ucfirst(trans($st['room_name']))}}</a></td>

      <td>
           <?php echo ($st['status_id'] == '1') ? 'Available' : 'Not Available'; ?>
      </td>

      <td style="cursor:pointer;background-color: {{$st->house_stat->color??''}}">
          <input type="hidden" name="room_id" class="index_room_id"
                 value="{{$st->room_id}}">
          <div class="label label-default house_status" data-type="select"
                data-pk="1" data-title="Select Status"
                data-value="{{$st->house_status_id}}">
              {{$st->house_stat->status_name??'-'}}</div>
      </td>
      <td>{{$st['hk_remarks']}}</td>
      <td>{{ucfirst(trans( $st['housekeeper']['username'] ?? '' ))}}</td>

        @if($check)
        <td style="background-color: red; color: white">Blocked <a href="{{route('admin.hotel.room-block-confirmunblock', $check->id)}}" data-toggle="modal" data-target="#modal_dialog"  title="Unblock room {{$st['room_number']}}">
          <i class="fa  fa-unlock" style="float: right;"></i></a></td>
        @else
        <td class="lalbel label-success"> Unblocked <a href="{{route('admin.hotel.room-block-selected',$room_id)}}" title="Block room {{$st['room_number']}}">
          <i class="fa fa-unlock" style="float: right;"></i></a></td>
        @endif

    </tr>
    @endforeach

</tbody>
</table>
</div>
    </div>
   </div>

@endsection
@section('body_bottom')
    <link href="/x-editable/bootstrap-editable.css" rel="stylesheet"/>
    <script src="/x-editable/bootstrap-editable.min.js"></script>

        <script>
            $('.house_status').each(function () {
                let parent = $(this).parent().parent();
                let room_id = parent.find('.index_room_id').val();
                $(this).editable({
                    source: <?php echo json_encode($house_status) ?>
                    , success: function (response, newValue) {
                        handleChange(room_id, newValue, 'status', $(this));
                    }
                });
            });
            function handleChange(room_id, value, type, element) {
                $.post("/admin/ajax_room_update", {
                        id: room_id
                        , update_value: value
                        , type: type
                        , _token: $('meta[name="csrf-token"]').attr('content')
                    }
                    , function (data) {
                        // if (data.status == '1') {
                        //     makechanges(value, type, element);
                        // }
                    });
            }
            // function makechanges(value, type, element) {
            //     if (type == 'status') {
            //         value == 'Started' ?
            //             element.css('background', '#4B77BE') : value == 'Open' ?
            //                 element.css('background', '#8F1D21') : value == 'Completed' ?
            //                     element.css('background', '#26A65B') : element.css('background', 'pink')
            //     }
            // }
        </script>
@endsection

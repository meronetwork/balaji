@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              House keeping
                <small>Create a house keeping status</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

         <form method="post" action="{{route('admin.hotel.payment-type-create')}}">
            {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Payment name</label>
<div class="input-group ">
<input type="text" name="payment_name" placeholder="Payment name" id="name" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
 </div>

 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>

  @endsection
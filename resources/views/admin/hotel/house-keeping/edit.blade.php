@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Room
                <small>Create a new room </small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 <form method="post" action="{{route('admin.hotel.house-keeping-edit',$room->room_id)}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
  <h3>Basic Info</h3>
  <div class="row">

    <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              Roomtype
                              </label>
                              <div class="col-md-6">
                                <select name="" class="form-control">
                              
                        <option value="{{$room->roomtype_id}}">{{ucfirst(trans($room->room_type->room_name))}}</option>
                             
                                </select>

                              </div>

                            </div>
                        </div>
                            <div class="col-md-6">
              <div class="form-group">  
                <label class="control-label col-sm-6">Room number</label>
                    <div class="input-group col-sm-5">
                        <input type="number" name="" placeholder="Room name" id="calculated_cost" value="{{$room->room_number}}" class="form-control" required="required" readonly="">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>

  </div>
 
 </div>

 <div class="row">
      <div class="col-md-6">
              <div class="form-group">  
                <label class="control-label col-sm-6">Floor number</label>
                    <div class="input-group col-sm-5">
                        <input type="number" name="floor_number" placeholder="Room name" id="calculated_cost" value="{{$room->floor_number}}" class="form-control" required="required" readonly="">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>
  </div>

 </div>

  <div class="row">
      <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              House Keeping status
                              </label>
                              <div class="col-md-6">
                                <select name="house_status_id" class="form-control">
                                   <option value="">Select status</option>
                                  @foreach($housestatus as $hs)
                                    <option value="{{$hs->id}}" @if($room->house_status_id == $hs->id) selected @endif>{{$hs->status_name}}</option>
                                  @endforeach
                                </select>

                              </div>

                            </div>
                        </div>
                              <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              House Keeper
                              </label>
                              <div class="col-md-6">
                                <select name="housekeeper_id" class="form-control">
                                   <option value="">Select housekeeper</option>
                                  @foreach($housekeeper as $hk)
                                    <option value="{{$hk->id}}"  @if($room->housekeeper_id == $hk->id) selected @endif>{{$hk->username}}</option>
                                  @endforeach
                                </select>

                              </div>

                            </div>
                        </div>
 </div>
  <div class="row">

                     <div class="col-md-6">
                        <label for="inputEmail3" class="control-label">
                        HouseKeeper Remark 
                        </label>
                        <textarea class="form-control" name="hk_remarks" id="comments" placeholder="Write Description">
                        </textarea>
                        </div>
                </div><br>


   <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update</button>
            <a href="{!! route('admin.hotel.room-index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
        </div>
    </div>
</div>
</div>
</div>
  @endsection
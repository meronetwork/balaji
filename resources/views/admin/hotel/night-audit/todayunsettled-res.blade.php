@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
           Night Audit
                <small>Settle today res</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">Post Charge</font></b>
            @if(count($activeReservation) == 0)
             <a class="btn btn-default btn-xs" href="/admin/hotel/release-index" title="Release Reservation">
                            <i class="fa fa-forward"></i> Next
              </a>
            @endif            
             <span id="index_res_ajax_status"></span>
            <div style="display: inline; float: right;">
            </div>
        </div>
</div>
  <form method="POST" action="{{ route('admin.hotel.today_unsettled_folio') }}">
  @csrf
  <div class="pull-right" style="padding: 10px;">
    <button class="btn btn-xs btn-default" type="submit">Post All to folio charge <i class="fa fa-forward"></i></button>
    <br/>
  </div>
  </form>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr class="bg-danger">
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>Res. Id</th>
        <th>Check In</th>
        <th>Room #</th>
        <th>Rooms</th>
        
        <th>Guest Name</th>
   
    </tr>
</thead>
<tbody>
    @foreach($activeReservation as $key=>$res)
    <tr>
        <td >
            <input type="checkbox" name="res_id" value="{{$res->id}}" class="reservation_id">
        </td>
        <td>#{{$res->id}}</td>
        <td>{{ $res->check_in }}</td>
        <td> <a href="" title="{{\TaskHelper::ShowRoom($res->id)}}"><strong>{{$res->room_num}}</strong></a> {{$res->room->room_type->type_code}}</td>
                    <?php 
             $total_person = \App\Models\ReservationRoomGuest::where('res_id',$res->id)->count();
          ?>
                    <td> <strong>{{\TaskHelper::ReservationTotalRoom($res->id)}}</strong> ({{$total_person}})-{{$res->occupancy}} </td>
                
         @if($res->guest)
        <td title="{{ $res->remarks }}">
            <strong style="font-size: 16.5px;">
                <a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest->first_name}} {{ $res->guest->last_name}}</a>
            </strong>
        </td>
        @else
        <td title="{{ $res->remarks }}">
            <strong style="font-size: 16.5px;"><a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ strtoupper($res->guest_name)}}</a></strong>
        </td>    
        @endif


    </tr>
    @endforeach
</tbody>
</table>
</div>

@endsection
@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
           Night Audit
                <small>Pending resrvation index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">Relase resrvation List</font></b>
             {{-- <a class="btn btn-default btn-xs" href="/admin/hotel/nightaudit-roomstatus" title="Release Reservation">
                            <i class="fa fa-forward"></i> Next
              </a> --}}
              <a class="btn btn-default btn-xs" 
              href="/admin/hotel/nightaudit-roomstatus" title="Room Status">
                            <i class="fa fa-forward"></i> Next
              </a>
             <span id="index_res_ajax_status"></span>
            <div style="display: inline; float: right;">
            </div>
        </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>Res. Id</th>
        <th>Book out</th>
         <th>Guest Name</th>
        <th>Res. Status</th>
    </tr>
</thead>
<tbody>
    @foreach($release as $key=>$st)
    <tr>
        <td >
            <input type="checkbox" name="res_id" value="{{$st->id}}" class="reservation_id">
        </td>
        <td>#{{$st->id}}</td>
        <td>{{ date('dS M y', strtotime($st->check_out ?? $st->book_out ))}}</td>
        <td>@if($st->guest_id)
            {{$st->guest->full_name}}
            @else
            {{ucfirst(trans($st->guest_name))}}
            @endif
        </td>
         <td style="background-color: {{$st->color}}">
                <select id= "changestatus">
                    @foreach($res_status as $rs)
                    <option value="{{$rs->id}}" @if($st->reservation_status == $rs->id) selected @endif>{{$rs->status_name}}</option>
                    @endforeach
                </select>
         </td>
     </tr>
    @endforeach
</tbody>
</table>
</div>
<script type="text/javascript">
     $(document).on('change','#changestatus',function(){
        let status_id = $(this).val();
         var id = $(this).closest('tr').find('.reservation_id').val();
          $(this).closest('tr').remove();
         $.post('/admin/hotel/nightaudit-release',
            {id: id, status_id: status_id, _token: $('meta[name="csrf-token"]').attr('content')},
            function(data,status){
                if(data.status == 1){
                    $("#index_res_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                }
                 $('#index_status_update').delay(3000).fadeOut('slow');
            });
    })
</script>
@endsection
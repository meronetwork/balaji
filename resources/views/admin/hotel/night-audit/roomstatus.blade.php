@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{$page_title ?? "Page Title"}}
        <small>{{$page_description ?? "Page Dscription" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="box-header with-border">

            <div class='row'>
                <div class='col-md-12'>
                    <b>
                        <font size="4">{{$page_title ?? "Page Title"}}</font>
                    </b>

                    <span id="index_res_ajax_status"></span>
                    <a href="/admin/hotel/nightaudit-unsettledfolios" class="btn btn-default btn-xs">Next &nbsp;&nbsp;<i class="fa fa-forward"></i></a>
                    <div style="display: inline; float: right;">
                    </div>
                </div>
            </div>

            <table class="table table-hover table-no-border" id="leads-table">
                <thead>
                    <tr>

                        <th>Room number</th>
                        <th>Room type</th>
                        <th>Availability </th>
                        <th>House status</th>
                        <th>HK Remarks</th>
                        <th>Housekeeper </th>
                        <th>Blocked Status</th>
                    </tr>
                </thead>


                <tbody>
                    @foreach($rooms as $k => $st)
                    <?php 

    $room_id = $st['room_id'];
    $check = \App\Models\RoomBlock::where('room_id',$room_id)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first();
 
    ?>
                    <tr>
                        <td><b>{{$st['room_number']}}</b>&nbsp;{{$st['type_code']}}</td>
                        <td><a href="{{route('admin.hotel.house-keeping-edit',$room_id)}}">{{ucfirst(trans($st['room_name']))}}</a></td>
                        <td>
                            <?php echo ($st['status_id'] == '1') ? 'Available' : 'Not Available'; ?>
                        </td>
                        <td style="background-color: {{$st->house_stat->color}}">{{$st->house_stat->status_name}}</td>
                        <td>{{$st['hk_remarks']}}</td>
                        <td>{{ucfirst(trans($st['housekeeper']['username']))}}</td>


                        @if($check)
                        <td style="background-color: red; color: white">Blocked <a href="{{route('admin.hotel.room-block-confirmunblock', $check->id)}}" data-toggle="modal" data-target="#modal_dialog" title="Unblock room {{$st['room_number']}}"><i class="fa  fa-unlock" style="float: right;"></i></td></a>
                        @else
                        <td> Unblocked <a href="{{route('admin.hotel.room-block-selected',$room_id)}}" title="Block room {{$st['room_number']}}"><i class="fa fa-unlock-alt" style="float: right;"></i></a></td>
                        @endif




                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        @endsection

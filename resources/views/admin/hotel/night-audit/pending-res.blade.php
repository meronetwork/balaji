@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
           Night Audit
                <small>You can make end of day and get end of day reports by pressing NEXT button.
Then you can close the day by pressing CLOSE DAY button.</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
           
           

                  <a class="btn btn-primary btn-lg" href="/admin/hotel/today_unsettled_folio" title="Release Reservation">
                            <i class="fa fa-forward"></i> Start Night Audit
                    </a>

             <span id="index_res_ajax_status"></span>
            <div style="display: inline; float: right;">
 
            </div>    


           


        </div>
</div>

<table class="table table-hover table-no-border" id="leads-table">
    <br/>
     <div class="row">
        <div class="col-md-4"></div>

            <div class="callout callout-warning" col-md-4">
             
                Cash Folio's Balance is being processed ...<br/>
                Check-Out Folio's Balance is being processed...<br/>
                Reservations without Check-In are being transferred to cancel list...<br/>
                The Stay Date of records without Check-Out are being extended...<br/>
                Information of the Day details are being processed...<br/>
                Room rates are being processed...<br/>
                Check-In and Folio processes are being backed up...<br/>
                The daily management report is being prepared...<br/>
                The End of The Day reports are being prepared...<br/>
                The Guest reports are being transferred...<br/>
                CANCEL unarrived reservation automatically!

             </div>  
             
               <div class="callout callout-default"  col-md-4">
                 
                <b><font size="4">Pending reservation List</font></b>

             </div>  
             </div>

<thead>
    <tr class="bg-info">
        
        <th>Res. Id</th>
        <th>Check out</th>
         <th>Guest Name</th>
        <th>Res. Status</th>
    </tr>
</thead>
<tbody>
    @foreach($pending as $key=>$st)
    <tr>
       
        <td>#{{$st->id}}</td>
        <td> {{ date('dS M y', strtotime($st->check_in ?? $st->book_in  ))}}</td>
        <td>@if($st->guest_id)
            {{$st->guest->full_name}}
            @else
            {{ucfirst(trans($st->guest_name))}}
            @endif

        </td>
         <td style="background-color: {{$st->color}}">
             
                <select class="changestatus">
                    @foreach($res_status as $rs)
                    <option value="{{$rs->id}}" @if($st->reservation_status == $rs->id) selected @endif>{{$rs->status_name}}</option>
                    @endforeach
                </select>
         </td>
     </tr>

    @endforeach

</tbody>
</table>
</div>
<script type="text/javascript">
     $(document).on('change','.changestatus',function(){
        let status_id = $(this).val();
         var id = $(this).closest('tr').find('.reservation_id').val(); 
          $(this).closest('tr').remove();
         $.post('/admin/hotel/nightaudit-pending',      
            {id: id, status_id: status_id, _token: $('meta[name="csrf-token"]').attr('content')},
            function(data,status){

                if(data.status == 1){
                    $("#index_res_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                }
                 $('#index_status_update').delay(3000).fadeOut('slow');
            });
    })
</script>
@endsection

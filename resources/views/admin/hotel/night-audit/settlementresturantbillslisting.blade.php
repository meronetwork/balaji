 @extends('layouts.master')
 @section('content')

 <link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">

     <h1>
         {{ $page_title ?? "Page Title" }}
         <small>{{ $page_description ?? "Page Description" }}
         </small>
     </h1>

 </section>

 <div class='row'>
     <div class='col-md-12'>
         <!-- Box -->

         <div class="box box-default">

             <div class="box-header">
                 <div class='row'>
                     <div class='col-md-12'>

                         @if($posbills->count()>0||$abbrposbills->count()>0)
                             <span style="font-size: 15px;font-weight: bold">List of invoice to settle</span>
                         @endif
                         @if($posbills_to_post->count()>0||$abbrposbills_to_post->count()>0)
                         <form method="post" action="/admin/hotel/postbills/outlets/today">
                             {{ csrf_field() }}
                             <button class="btn btn-primary btn-xs float-right" title="Finish Night Auditing" type="submit">
                                 <i class="fa fa-forward"></i> Post all Today bills
                             </button>
                         </form>
                         @elseif($posbills_to_post->count()<=0&&$abbrposbills_to_post->count()<=0&&$posbills->count()<=0&&$abbrposbills->count()<=0&&$abbr_orders->count()>0)
                         <form method="post" action="/admin/postabbrtotax">
                             {{ csrf_field() }}
                             <span style="font-size: 15px;font-weight: bold">List of Abbreviated bills to post as Tax invoice</span>
                             <button class="btn btn-primary btn-xs float-right" title="Post All Abbreviated Bills To Tax Invoice" type="submit">
                                 <i class="fa fa-forward"></i> Post All Abbreviated Bills To Tax Invoice
                             </button>
                         </form>
                         @endif
                         <span id="index_res_ajax_status"></span>
                         <div style="display: inline; float: right;">
                         </div>
                     </div>
                 </div>

             </div>

             <div class="box-body">
                 <span id="index_lead_ajax_status"></span>
                 @if($posbills_to_post->count()<=0&&$abbrposbills_to_post->count()<=0&&$posbills->count()<=0&&$abbrposbills->count()<=0&&$abbr_orders->count()>0)
                 <div class="table-responsive ">
                     <table class="table table-hover table-bordered table-striped" id="orders-table">
                         <thead>
                         <tr class="bg-danger">
                             <th style="text-align: center">
                                 SN
                             </th>
                             <th>Date</th>
                             <th>Bill No</th>
                             <th>Guest</th>
                             <th>Pos Guest</th>
                             <th>Guest PAN</th>
                             <th>Total Sales</th>
                             <th>Tax Free Amount</th>
                             <th>Taxable Amount</th>
                             <th>Tax</th>
                             <th>Discount</th>
                             <th>Outlet</th>
                             <th>Paid By</th>
                             <th>Ledger name</th>
                         </tr>
                         </thead>
                         <tbody>
                         <?php
                         $n = 0;
                         $pos_total_amount = 0;
                         $pos_taxable_amount = 0;
                         $pos_non_taxable_amount = 0;
                         $pos_tax_amount = 0;
                         $pos_discount_amount = 0;

                         $allTotal = [];

                         ?>
                         @if(isset($abbr_orders) && !empty($abbr_orders))
                             @foreach($abbr_orders as $o)
                                 <tr>
                                     <td align="center">{{++$n}}</td>
                                     <td>{{$o->bill_date}}
                                         <?php
                                         $temp_date = explode(" ",$o->bill_date );
                                         $temp_date1 = explode("-",$temp_date[0]);
                                         $cal = new \App\Helpers\NepaliCalendar();
                                         //nepali date
                                         $a = $temp_date1[0];
                                         $b = $temp_date1[1];
                                         $c = $temp_date1[2];
                                         $d = $cal->eng_to_nep($a,$b,$c);
                                         $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                                         ?><br>
                                         <small> {!! $nepali_date !!}</small>
                                     </td>
                                     <td>
                                         <a href="{{ route('admin.orders.show',['orderId'=>$o->id,'invoice_type'=>'abbr']) }}" data-toggle="modal" data-target="#modal_dialog" >  AI-{{$o->outlet->outlet_code}}{!! $o->bill_no !!} </a>

                                         <input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                     <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>

                                     <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                     <td>@if($o->pos_customer_id){{$o->client->vat}}@elseif($o->reservation_id) {{$o->reservation->client->vat??''}} @else{{$o->folio->reservation->client->vat??''}} @endif</td>

                                     <td>{{number_format($o->total_amount,2)}}</td>

                                     <td>{{number_format($o->non_taxable_amount,2)}}</td>
                                     <td>{{number_format($o->taxable_amount,2)}}</td>
                                     <td>{{number_format($o->tax_amount,2)}}</td>
                                     <td>

                                         {{ number_format($o->discount_amount,2) }}

                                     </td>
                                     <td> {{ $o->outlet->name}} </td>

                                     <?php
                                     $pos_total_amount   = $pos_total_amount + $o->total_amount;
                                     $pos_taxable_amount = $pos_taxable_amount+$o->taxable_amount;
                                     $pos_non_taxable_amount = $pos_non_taxable_amount+$o->non_taxable_amount;
                                     $pos_tax_amount     = $pos_tax_amount+$o->tax_amount;
                                     $pos_discount_amount += $o->discount_amount;
                                     $paidBy =   ReservationHelper::paidByArr($o->payments);

                                     //$allTotal [$paidBy]  += $o->total_amount;

                                     ?>
                                     <td>{!! $paidBy  !!}</td>
                                     <td>{!! $o->getPayingLedger() !!}</td>


                                 </tr>
                                 @if($o->is_bill_active == '0')

                                     <tr>
                                         <td align="center">{{++$n}}</td>
                                         <td>{{$o->bill_date}}
                                             <?php
                                             $temp_date = explode(" ",$o->bill_date );
                                             $temp_date1 = explode("-",$temp_date[0]);
                                             $cal = new \App\Helpers\NepaliCalendar();
                                             //nepali date
                                             $a = $temp_date1[0];
                                             $b = $temp_date1[1];
                                             $c = $temp_date1[2];
                                             $d = $cal->eng_to_nep($a,$b,$c);
                                             $nepali_date = $d['date'].' '.$d['nmonth'] .', '.$d['year'];
                                             ?><br>
                                             <small> {!! $nepali_date !!}</small>
                                         </td>
                                         <td> Ref of AI-{{$o->outlet->outlet_code}}{!! $o->bill_no !!} CN AI-{{$o->credit_note_no}}
                                             <input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                         <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>

                                         <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                         <td>@if($o->pos_customer_id){{$o->client->vat}}@elseif($o->reservation_id) {{$o->reservation->client->vat}} @else{{$o->folio->reservation->client->vat}} @endif</td>

                                         <td>-{{number_format($o->total_amount,2)}}</td>
                                         <td>-{{number_format($o->non_taxable_amount,2)}}</td>
                                         <td>-{{number_format($o->taxable_amount,2)}}</td>
                                         <td>-{{number_format($o->tax_amount,2)}}</td>
                                         <td>-{{number_format($o->discount_amount,2)}}</td>

                                         <td> {{ $o->outlet->name}} </td>

                                         <?php
                                         $pos_total_amount       = $pos_total_amount - $o->total_amount;
                                         $pos_taxable_amount     = $pos_taxable_amount-$o->taxable_amount;
                                         $pos_non_taxable_amount     = $pos_non_taxable_amount-$o->non_taxable_amount;
                                         $pos_tax_amount         = $pos_tax_amount-$o->tax_amount;
                                         $pos_discount_amount -= $o->discount_amount;

                                         $paidBy =   ReservationHelper::paidByArr($o->payments);
                                         //$allTotal [$paidBy]  = $allTotal [$paidBy] - $o->total_amount;
                                         ?>

                                         <td>{!! $paidBy !!}</td>
                                         <td>{!! $o->getPayingLedger() !!}</td>

                                     </tr>


                                 @endif





                             @endforeach
                         @endif
                         </tbody>
                         <tfoot>




                         <tr>
                             <td colspan="5"></td>
                             <td>
                                 Total Amount:
                             </td>
                             <td style="font-size: 16.5px"> <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_total_amount,2) }} </strong></td>
                             <td style="font-size: 16.5px">
                                 <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_non_taxable_amount,2) }} </strong>
                             </td>

                             <td style="font-size: 16.5px">
                                 <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_taxable_amount,2) }} </strong>
                             </td>
                             <td style="font-size: 16.5px">
                                 <strong> {{env('APP_CURRENCY')}} {{ number_format($pos_tax_amount,2) }} </strong>
                             </td>
                             <td><strong>{{env('APP_CURRENCY')}}  {{ number_format($pos_discount_amount,2) }}</strong></td>
                             <td></td>


                         </tr>


                         {{--   <tr style="font-size: 16.5px">
                               <td colspan="5"></td>
                               <td >Summary</td>
                               <td></td>
                           </tr>

                           @foreach($allTotal as $key=>$value)
                               <tr style="font-size: 16.5px">
                                   <td colspan="5"></td>
                                   <td>{{ ucfirst($key) }}</td>
                                   <td>{{env(APP_CURRENCY)}}{{ number_format($value,2) }}</td>
                               </tr>
                           @endforeach --}}




                         </tfoot>
                     </table>

                 </div> <!-- table-responsive -->

                 @elseif($posbills_to_post->count()>0||$abbrposbills_to_post->count()>0||$posbills->count()>0||$abbrposbills->count()>0)



                 <div class="table-responsive">
                     <table class="table table-hover table-bordered table-striped" id="orders-table">
                         <thead>
                             <tr class="bg-info">
                                 <th>#</th>
                                 <th>Bill No.</th>
                                 <th>Date </th>
                                 <th>Staff</th>
                                 <th>Resv #</th>
                                 <th>Outlet</th>
                                 <th>Paid </th>
                                 <th>Balance</th>
                                 <th>Settle Status</th>
                                 <th>Total</th>
                                 <th>Guest</th>
                                 <th>Post Bill</th>

                                 <th>Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             <?php
                                        $folio_total_amount = 0;
                                   ?>
                             @if($posbills && !empty($posbills))
                             @foreach($posbills as $o)
                             <tr>
                                 <td>{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                 <?php
                                            $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                           ?>
                                <td>TI-{{$o->outlet->outlet_code}} {{ $o->bill_no }}</td>
                                 <td>{!! $o->bill_date !!}</td>
                                 <td>{!! $o->user->username !!}</td>
                                 <td>{{ env('RES_CODE')}}{!! $o->reservation_id !!}</td>
                                 <td>{!! $o->outlet->name !!}</td>
                                 <td>{!! number_format($paid_amount,2) !!}</td>

                                 <td style="font-size: 16.5px">{{env('APP_CURRENCY')}}{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                 @if($o->ordermeta->settlement == 1)
                                 <td><span class="label label-success">Settled</span></td>
                                 @else
                                 <td><span class="label label-danger">Not Settled</span></td>
                                 @endif

                                 <td style="font-size: 16.5px">{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                 <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>

                                 <td>@if($o->reservation->guest_id ) {{ $o->reservation->guest->full_name }} @else {{ $o->name }} @endif</td>

                                 @if($o->ordermeta->is_posted)

                                 <td>
                                     <a class="btn btn-success btn-xs" href="#">Posted</a>
                                 </td>

                                 @else

                                 <td>
                                     <form method="post" action="/admin/hotel/postbills/outlets/{{$o->id}}">
                                         {{ csrf_field() }}
                                         <button class="btn btn-success btn-xs" type="submit">Post</button>

                                     </form>
                                 </td>

                                 @endif



                                 <td>
                                     <a target="_blank" href="/admin/payment/orders/{{$o->id}}/create" title="View Payment">Settle</a>
                                     | <a target="_blank" href="/admin/possales/return" title="View Payment"> Return</a>

                                 </td>
                             </tr>
                             @endforeach
                             @endif

                             @if($abbrposbills && !empty($abbrposbills))
                             @foreach($abbrposbills as $o)
                             <tr>
                                 <td>{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                 <?php
                                            $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id,'abbr');
                                           ?>
                                <td>AI-{{$o->outlet->outlet_code}} {{ $o->bill_no }}</td>
                                 <td>{!! $o->bill_date !!}</td>
                                 <td>{!! $o->user->username !!}</td>
                                 <td>{{ env('RES_CODE')}}{!! $o->reservation_id !!}</td>
                                 <td>{!! $o->outlet->name !!}</td>
                                 <td>{!! number_format($paid_amount,2) !!}</td>

                                 <td style="font-size: 16.5px">{{env('APP_CURRENCY')}}{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                 @if($o->ordermeta->settlement == 1)
                                 <td><span class="label label-success">Settled</span></td>
                                 @else
                                 <td><span class="label label-danger">Not Settled</span></td>
                                 @endif

                                 <td style="font-size: 16.5px">{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                 <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>

                                 <td>@if($o->reservation->guest_id ) {{ $o->reservation->guest->full_name }} @else {{ $o->name }} @endif</td>

                                 @if($o->ordermeta->is_posted)

                                 <td>
                                     <a class="btn btn-success btn-xs" href="#">Posted</a>
                                 </td>

                                 @else

                                 <td>
                                     <form method="post" action="/admin/hotel/postbills/outlets/{{$o->id}}">
                                         {{ csrf_field() }}
                                         <button class="btn btn-success btn-xs" type="submit">Post</button>

                                     </form>
                                 </td>

                                 @endif



                                 <td>
                                     <a target="_blank" href="/admin/payment/orders/{{$o->id}}/create?invoice_type=abbr" title="View Payment">Settle</a>
{{--                                     | <a target="_blank" href="/admin/possales/return" title="View Payment"> Return</a>--}}

                                 </td>
                             </tr>
                             @endforeach
                             @endif
                             <tr>
                                 <td colspan="6">
                                 </td>
                                 <td>
                                     Total Amount:
                                 </td>
                                 <td style="font-size: 20.5px">
                                     <strong> {{env(APP_CURRENCY)}} {{ number_format($folio_total_amount,2) }} </strong>
                                 </td>
                             </tr>
{{--                             @else--}}
{{--                             <tr>--}}
{{--                                 <td colspan="9" style="color: grey;font-weight: bold">--}}
{{--                                 Congratulations! All Orders have been posted and settled successfully.--}}
{{--                                 </td>--}}
{{--                             </tr>--}}
{{--                             @endif--}}

                         </tbody>
                     </table>

                 </div> <!-- table-responsive -->
                 @else
                     <form action="/admin/day-end/business-date" method="post">

                         {{ csrf_field() }}

                         <div class="row">
                             <div class="col-md-9">
                                 <div class="form-group" style="align-items: baseline">
                                     <label class="control-label" style="font-size: 18px">Set Business date to complete the process</label>
                                     <br>
                                     <div class="input-group col-sm-4" style="margin-left: 10px">
                                         <input type="text" name="business_date" placeholder="Business Date" id="business_date"
                                                value="{{ date('Y-m-d', strtotime($business_date->business_date. ' + 1 days')) }}" class="form-control datepicker" style="font-size: 17px;"
                                                readonly required="required">
                                         <div class="input-group-addon">
                                             <a href="#"><i class="fa fa-calendar"></i></a>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>

                         <div class="col-md-12" style="margin-top: 10px">
                             <div class="form-group">
                                 <a href="/home" class='btn btn-danger btn-sm'>{{ trans('general.button.cancel') }}</a>

                                 <button type="Submit" class="btn btn-success btn-sm">
                                     Complete day end and jump to next business date <i class="fa fa-forward"></i>
                                 </button>
                             </div>
                         </div>

                     </form>


                     {{--                     <div>Relax and sit back! All Orders have been posted and settled successfully.</div>--}}
@endif
             </div><!-- /.box-body -->

         </div><!-- /.box -->
         <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">

     </div><!-- /.col -->

 </div><!-- /.row -->
 @endsection


 <!-- Optional bottom section for modals etc... -->
 @section('body_bottom')
 <!-- DataTables -->
 <script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

 <script language="JavaScript">
     function toggleCheckbox() {
         checkboxes = document.getElementsByName('chkClient[]');
         for (var i = 0, n = checkboxes.length; i < n; i++) {
             checkboxes[i].checked = !checkboxes[i].checked;
         }
     }

 </script>


 <script type="text/javascript">
     $(document).on('change', '#order_status', function() {

         var id = $(this).closest('tr').find('.index_sale_id').val();

         var purchase_status = $(this).val();
         $.post("/admin/ajax_order_status", {
                 id: id
                 , purchase_status: purchase_status
                 , _token: $('meta[name="csrf-token"]').attr('content')
             }
             , function(data, status) {
                 if (data.status == '1')
                     $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                 else
                     $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                 $('#index_status_update').delay(3000).fadeOut('slow');
                 //alert("Data: " + data + "\nStatus: " + status);
             });

     });

 </script>
 <script type="text/javascript">
     $("#btn-submit-filter").on("click", function() {

         fiscal_id = $("#filter-fiscal").val();
         status = $("#filter-status").val();
         customer_id = $("#filter-customer").val();
         location_id = $("#filter-location").val();
         type = $("#order_type").val();

         window.location.href = "{!! url('/') !!}/admin/orders?fiscal_id=" + fiscal_id + "&status=" + status + "&customer_id=" + customer_id + "&location_id=" + location_id + "&type=" + type;
     });

     $("#btn-filter-clear").on("click", function() {

         type = $("#order_type").val();
         window.location.href = "{!! url('/') !!}/admin/orders?type=" + type;
     });

 </script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('.customer_id').select2();
     });

 </script>

 @endsection

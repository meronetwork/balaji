 @extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

   <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>
           {{ $page_title or "Page Title" }}
            <small>{{ $page_description or "Page Description" }}
            </small>
        </h1>
    </section> 


    <div class='row'>
        <div class='col-md-12'>
            <div class="box">
		        <div class="box-body">
		            <form method="post" action="/admin/hotel/settle/postfolio/{{$folio->id}}">
		                    {{ csrf_field() }}

		                   <div class="row">
			                   	<div class="col-md-12">

			                   		<h5> Party Name: @if($folio->reservation->guest_id != '0' ) {{  $folio->reservation->guest->full_name }} @else {{  $folio->reservation->guest_name }} @endif <input type="hidden" name="party_ledger_id" value="{{ $folio->reservation->guest->client->ledger_id }}"></h5>

			                   		<h5> Ledger No: {{ $folio->reservation->guest->client->ledger_id }} </h5>

			                   		<h5> Folio NO: <a style="color: black" target="_blank" href="/admin/folio/{{$folio->reservation_id}}/edit/{{$folio->id}}">
		                   	         FOL{{ $folio->id }}</a><input type="hidden" name=""></h5>

		                   	        <h5>Sales Amount: {{env('APP_CURRENCY')}} {{$folio->subtotal}}<input type="hidden" name="subtotal" value="{{$folio->subtotal}}"></h5>

		                   	        <h5>Service Charge Amount: {{env('APP_CURRENCY')}} {{$folio->service_charge}}<input type="hidden" name="service_charge" value="{{$folio->service_charge}}"></h5>

		                   	        <h5>VAT Amount: {{env('APP_CURRENCY')}} {{$folio->tax_amount}}<input type="hidden" name="tax_amount"value="{{$folio->tax_amount}}"></h5>

		                   	        <h5>Total Amount: {{env('APP_CURRENCY')}} {{$folio->total_amount}}<input type="hidden" name="total_amount" value="{{$folio->total_amount}}"></h5>

		                   	      @if($folio->reservation->guest->client->ledger_id)

		                   	        <div class="col-sm-3">
					                    <div class="form-group">
					                        <label class="control-label col-sm-12">Settle Type</label>
					                        <div class="input-group ">
					                          <select class="form-control" name="settle_type" required>
					                          	<option value="">Select Type</option>
					                          	<option value="Cash">Cash</option>
					                          	<option value="Crdeit">Credit</option>
					                          </select>
					                            <div class="input-group-addon">
					                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
					                            </div>
					                        </div>
					                    </div>
					                </div>

					                @else

					                <div class="col-sm-3">
					                    <div class="form-group">
					                        <label class="control-label col-sm-12">Settle Type</label>
					                        <div class="input-group ">
					                          <select class="form-control" name="settle_type" required>
					                          	<option value="">Select Type</option>
					                          	<option value="Cash">Cash</option>
					                          </select>
					                            <div class="input-group-addon">
					                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
					                            </div>
					                        </div>
					                    </div>
					                </div>

					                @endif

					                <div class="col-sm-3">
					                    <div class="form-group">
					                        <label class="control-label col-sm-12">Tag</label>
					                        <div class="input-group ">
					                           {!! Form::select('tag_id', [''=>'Select']+$tags , $edit->outlet_id, ['class' => 'form-control outlet_id', required]) !!}
					                            <div class="input-group-addon">
					                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
					                            </div>
					                        </div>
					                    </div>
					                </div>

			                   	</div>	
		                   </div>


		                <div class="col-md-12">
		                    <div class="form-group">
		                    	@if($folio->settlement == 0)
		                        <button class="btn btn-primary" type="submit">Settle</button>
		                        @else
		                         <button class="btn btn-primary" disabled>Settle</button>
		                        @endif

		                        <a href="" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
		                    </div>
		                </div>
		            </form>
		        </div>
            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->



@endsection

@section('body_bottom')


@endsection
@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body ">
                <form method="post" action="/admin/hotel/table/bookings" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">First Name</label>
                            <input type="text" name="first_name" placeholder="First Name" id="first_name" value="" class="form-control" required>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Last Name</label>
                            <input type="text" name="last_name" placeholder="Last Name" id="last_name" value="" class="form-control" required>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Email</label>
                            <input type="text" name="email" placeholder="Email" id="email" value="" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Phone</label>
                            <input type="text" name="phone" placeholder="Phone" id="phone" value="" class="form-control" required>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">No of Guests</label>
                            <input type="number" name="no_of_guests" placeholder="No of Guests" id="no_of_guests" value="" class="form-control" required>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Reservation Date Time</label>
                            <input type="text" name="reservation" placeholder="Reservation Time" id="reservation" value="" class="form-control datetimepicker" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Table Reservation</label>
                            <select name="table_reservation" id="phone" value="" class="form-control">
                                <option value="">Please Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Reservation Type</label>
                            <select name="reservation_type" id="phone" value="" class="form-control">
                                <option value=""> Please Select </option>
                                <option value="Dinner"> Dinner </option>
                                <option value="VIP/Mezzanine"> VIP/Mezzanine </option>
                                <option value="Birthday/ Anniversary"> Birthday/ Anniversary </option>
                                <option value="Nightlife"> Nightlife </option>
                                <option value="Private"> Private </option>
                                <option value="Wedding"> Wedding </option>
                                <option value="Corporate"> Corporate </option>
                                <option value="Holiday"> Holiday </option>
                                <option value="Other"> Other </option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">If Other above, please specify?</label>
                            <input type="text" name="if_other" placeholder="If Other Name" id="if_other" value="" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Any Special Request?</label>
                            <textarea name="special_request" id="special_request" class="form-control">
                           </textarea>
                        </div>
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::submit( trans('general.button.create'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    <a href="{!! route('admin.hotel.table.bookings.index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>
        </div>


        </form>

    </div>
</div>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<script>
    $('.datetimepicker').datetimepicker({
        //inline: true,
        //format: 'YYYY-MM-DD',
        format: 'YYYY-MM-DD HH:mm'
        , sideBySide: true
    });

</script>


@endsection

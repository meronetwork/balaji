@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>


<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>
                <b>
                    <font size="4">{{ $page_title ?? "Page Title"}} </font>
                </b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm" title="Import/Export Leads" href="{{ route('admin.hotel.table.bookings.create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Create</strong>
                    </a>
                </div>
            </div>
        </div>
        <table class="table table-hover table-no-border" id="leads-table">

            <thead>
                <tr>
                    <th style="text-align:center;width:20px !important">
                        <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                            <i class="fa fa-check-square-o"></i>
                        </a>
                    </th>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>No of Guests</th>
                    <th>Reservation</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tablebookings as $key=>$att)
                <tr>
                    <td>
                        <input type="checkbox" name="event_id" value="{{$att->id}}">
                    </td>
                    <td>{{ $att->id }}</td>
                    <td>{{ $att->first_name }}</td>
                    <td>{{ $att->last_name }}</td>
                    <td><a href="/admin/hotel/table/bookings/{{$att->id}}">{{ $att->email }}</a></td>
                    <td>{{ $att->phone }}</td>
                    <td>{{ $att->no_of_guests }}</td>
                    <td>{{ $att->reservation }}</td>
                    <td>
                        <a href="/admin/hotel/table/bookings/{{$att->id}}/edit"><i class="fa fa-edit"></i></a>
                        <a href="{!! route('admin.hotel.table.bookings.delete-confirm', $att->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection

@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body ">


                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">First Name</label>
                        <input type="text" name="first_name" placeholder="First Name" id="first_name" value="{{$tablebookings->first_name}}" class="form-control" readonly>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Last Name</label>
                        <input type="text" name="last_name" placeholder="Last Name" id="last_name" value="{{$tablebookings->last_name}}" class="form-control" readonly>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Email</label>
                        <input type="text" name="email" placeholder="Email" id="email" value="{{$tablebookings->email}}" class="form-control" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">Phone</label>
                        <input type="text" name="phone" placeholder="Phone" id="phone" value="{{$tablebookings->phone}}" class="form-control" readonly>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">No of Guests</label>
                        <input type="number" name="no_of_guests" placeholder="No of Guests" id="no_of_guests" value="{{$tablebookings->no_of_guests}}" class="form-control" readonly>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Reservation Date Time</label>
                        <input type="text" name="reservation" placeholder="Reservation Time" id="reservation" value="{{$tablebookings->reservation}}" class="form-control datetimepicker" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">Table Reservation</label>
                        <select name="table_reservation" id="phone" value="" class="form-control" readonly>
                            <option value="">Please Select</option>
                            <option value="Yes" @if($tablebookings->table_reservation == 'Yes') selected @endif>Yes</option>
                            <option value="No" @if($tablebookings->table_reservation == 'No') selected @endif>No</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Reservation Type</label>
                        <select name="reservation_type" id="phone" value="" class="form-control" readonly>
                            <option value=""> Please Select </option>
                            <option value="Dinner" @if($tablebookings->reservation_type == 'Dinner') selected @endif> Dinner </option>
                            <option value="VIP/Mezzanine" @if($tablebookings->reservation_type == 'VIP/Mezzanine') selected @endif> VIP/Mezzanine </option>
                            <option value="Birthday/Anniversary" @if($tablebookings->reservation_type == 'Birthday/Anniversary') selected @endif> Birthday/Anniversary </option>
                            <option value="Nightlife" @if($tablebookings->reservation_type == 'Nightlife') selected @endif> Nightlife </option>
                            <option value="Private" @if($tablebookings->reservation_type == 'Private') selected @endif>Private </option>
                            <option value="Wedding" @if($tablebookings->reservation_type == 'Wedding') selected @endif> Wedding </option>
                            <option value="Corporate" @if($tablebookings->reservation_type == 'Corporate') selected @endif> Corporate </option>
                            <option value="Holiday" @if($tablebookings->reservation_type == 'Holiday') selected @endif> Holiday </option>
                            <option value="Other" @if($tablebookings->reservation_type == 'Other') selected @endif> Other </option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">If Other above, please specify?</label>
                        <input type="text" name="if_other" placeholder="If Other Name" id="if_other" value="{{$tablebookings->if_other}}" class="form-control" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">Any Special Request?</label>
                        <textarea name="special_request" id="special_request" class="form-control" readonly>{!! $tablebookings->special_request !!}
                            </textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="{!! route('admin.hotel.table.bookings.edit',$tablebookings->id) !!}" class='btn btn-primary'>{{ trans('general.button.edit') }}</a>
                    <a href="{!! route('admin.hotel.table.bookings.index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>
        </div>



    </div>
</div>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<script>
    $('.datetimepicker').datetimepicker({
        //inline: true,
        //format: 'YYYY-MM-DD',
        format: 'YYYY-MM-DD HH:mm'
        , sideBySide: true
    });

</script>


@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')

@endsection

@section('content')
    <div class='row'>
        <div class='col-md-9'>
        	<div class="box box-primary">
            	<div class="box-header with-border">
                	<h3>{!! $guest->title !!}.{!! $guest->first_name !!}, {!! $guest->last_name !!}<small> {{$guest_type[$guest->guest_type][0]}}</small> </h3>
                    <p> Dob: {!! date('dS Y M',strtotime($guest->dob)) !!} </p>
                    <p> Profession: {!! $guest->profession !!} </p>
                    <p> Email: {!! $guest->email !!} </p>
                    <p> Phone: {!! $guest->mobile !!} </p>
                    <p> Country: {!! $guest->country !!} </p>
                    <p> City. : {!! $guest->city !!} </p>
                    <p> Created At. : {!! date('dS Y M',strtotime($guest->created_at) )!!} </p>
                    <p> Created By. : {!! $guest->user->username !!} </p>
                </div>
                <div class="box-body">
    
                 
    
                    <div class="content">
                       
                   <!--      
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    {!! Form::checkbox('enabled', '1', $client->enabled, ['disabled']) !!} {!! trans('general.status.enabled') !!}
                                </label>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <a href="/admin/hotel/guests" class="btn btn-primary">Close</a>
                            @if ( $guest->isEditable() || $client->canChangePermissions() )
                                <a href="/admin/hotel/guests/edit/{{$guest->id}}" title="{{ trans('general.button.edit') }}" class='btn btn-default'>{{ trans('general.button.edit') }}</a>
                            @endif
                              <a href="/admin/chartofaccounts/detail/{{$guest->ledger_id}}/ledgers" title="view ledger" class='btn btn-danger' target="_blank">View Ledger</a>
                        </div>


                        {!! Form::close() !!}

                                <h2>Reservations</h2>
                        <table class="table table-hover table-bordered" id="contacts-table">
                            <thead>
                                <tr>
                                    <th>Res. Id</th>
                                    <th>Guest</th>
                                    <th>Phone</th>
                                    <th>Primary Email</th>
                                    <th>Agent</th>
                                    <th>Company</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($reservation) && !empty($reservation))
                                @foreach($reservation as $res)
                                    <tr>
                                        <td><strong>Res#{{$res->id}}</strong></td>
                                        <td><strong>
                                        <a href="/admin/hotel/guests/show/{{$res->guest->id}}" target="_blank">{{$res->guest->first_name}} {{$res->guest->last_name}}</a>
                                        </strong>
                                        <td>{{$res->phone?$res->phone:$res->mobile}}</td>
                                        <td><a href="mailto::{{$res->email}}">{{$res->email}}</a></td>
                                        <td>
                                        <a href="/admin/clients/{{$res->agent->id}}" target="_blank">{{ $res->agent->name }} </a>
                                        </td>
                                        <td>
                                         <a href="/admin/clients/{{$res->client->id}}" target="_blank">    {!! $res->client->name !!}</a>
                                        </td>
                                        <td>
                                            @if($res->isEditable())
                                          <a href="/admin/hotel/reservation-edit/{{$res->id}}" target="_blank"><i class="fa fa-edit"></i></a>
                                            @else
                                              <i class="fa fa-edit text-muted" title="cannot edit"></i>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        @if(isset($cashbook) && sizeof($cashbook))
                        <h2>Cashbook</h2>
                        <table class="table table-hover table-bordered" id="cashbook-table">
                            <thead>
                                <tr>
                                    <th>Purpose</th>
                                    <th>Method</th>
                                    <th>Category</th>
                                    <th>Date</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total = 0;
                                ?> 
                                @foreach($cashbook as $k)
                                <tr>
                                    <td class="lead"> {!! link_to_route('admin.cashbook.show', $k->descs, [$k->id], ['target' => '_blank']) !!}</td>
                                    <td class="">{{ $k->paymentmethod->name }}</td>  
                                    <td class="">{{ $k->category->name }}</td>
                                    <td class="">{{ date('dS M Y', strtotime($k->date)) }}</td>
                                    <td class="">{{ $k->credit }}</td>
                                    <td class="">{{ $k->debit }}</td>
                                    <td class="">
                                        <?php
                                            if($k->debit != '')
                                                $total = $total + $k->debit;
                                            if($k->credit != '')
                                                $total = $total - $k->credit;
                                        ?>
                                        {{ $total }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif

                        @if(isset($proposal) && sizeof($proposal)) 
                        <h2>Proposal</h2>
                        <table class="table table-hover table-bordered" id="proposal-table">
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Product</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($proposal as $k)
                                    <tr>
                                        <td class="lead"> {!! link_to_route('admin.proposal.show', $k->subject, [$k->id], ['target' => '_blank']) !!}</td>
                                        <td class="">{{ $k->product->name }}</td>
                                        <td class=""><span class="label bg-green">{{ ucfirst($k->status)}}</span></td>
                                        <td class="">{{ ucfirst($k->type)}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        
                    </div><!-- /.content -->
    
                </div><!-- /.box-body -->
            </div>
        </div><!-- /.col -->

    </div><!-- /.row -->

@endsection

@section('body_bottom')
    <!-- Select2 js -->
    @include('partials._body_bottom_select2_js_user_search')
@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<style>
	select { width:200px !important; }
label {
    font-weight: 600 !important;
}

 .intl-tel-input { width: 100%; }
 .intl-tel-input .iti-flag .arrow {border: none;}


.selecticons{
    margin-left: 10px !important;
}
</style>


<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />

<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />

<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
                {{ $page_title or "Page Title" }}
                <small>{!! $page_description or "Page description" !!}</small>
                <small id='ajax_status'></small>
            </h1> {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<?php $readonly = ($guest->isEditable())? '' : 'readonly'; ?>
<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body">

                {!! Form::open( ['route' => ['admin.hotel.guests.edit',$guest->id],'method'=>'POST'] ) !!}
                <h4>Basic Info.</h4>
                <div class="row">
                    <div class="col-md-3 col-sm-12 form-group">
                        <label class="control-label">Title</label>
                        <div class="input-group col-sm-12">

                       {!! Form::select('title', ['Mr'=>'Mr', 'Miss'=>'Miss', 'Mrs'=>'Mrs', 'Ms'=>'Ms','Others'=>'Others'], $guest->title, ['class' => 'form-control searchable select2',$readonly]) !!}

                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label class="control-label">First Name</label>

                        <div class="input-group">
                            {!! Form::text('first_name',$guest->first_name,['class'=>'form-control input-sm','placeholder'=>'First name','required'=>'true',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-user"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 form-group">
                        <label class="control-label">Last Name</label>

                        <div class="input-group">
                            {!! Form::text('last_name',$guest->last_name,['class'=>'form-control input-sm','placeholder'=>'Last name','required'=>'true',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-user"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 form-group">
                        <label class="control-label">Date of Birth</label>

                        <div class="input-group">
                            {!! Form::text('dob',$guest->dob,['class'=>'form-control input-sm datepicker','placeholder'=>'Date of Birth',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-calendar-plus-o"></i></a>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3 form-group">
                    <label class="control-label">Gender</label>
                     <div class="input-group col-sm-12">

                       {!! Form::select('gender', ['M'=>'Male','F'=>'Female'], $guest->gender, ['class' => 'form-control searchable',$readonly]) !!}

                      </div>
                  </div>
                  <div class="col-md-3 form-group">
                        <label class="control-label">Profession</label>

                        <div class="input-group">
                            {!! Form::text('profession',$guest->profession,['class'=>'form-control input-sm','placeholder'=>'Guest Profession',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-user-secret"></i></a>
                            </div>
                        </div>

                    </div>
                      <div class="col-md-3 form-group">
                    <label class="control-label">Guest Type</label>
                     <div class="input-group col-sm-12">
                    {!! Form::select('guest_type',['TOU'=>'Tourist','BUS'=>'Business Travellers','FAM'=>'Families','STA'=>'Hotel Staffs','DEl'=>'Delegates','VIP'=>'VIP','COR'=>'Corporate','GOV'=>'Government','FIT'=>"FIT",'COM'=>"Complementry"],$guest->guest_type, ['class' => 'form-control searchable select2 input-sm',$readonly] )!!}

                      </div>
                  </div>
                </div>
                <h4>Contact Info</h4>
                <div class="row">
                        <div class="col-md-3 form-group">
                        <label class="control-label">Mobile</label>

                        <div class="input-group">
                            {!! Form::text('mobile',$guest->mobile,['class'=>'form-control input-sm','placeholder'=>'Mobile Number',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-phone"></i></a>
                            </div>
                        </div>

                    </div>
                     <div class="col-md-3 form-group">
                        <label class="control-label">Landline</label>

                        <div class="input-group">
                            {!! Form::text('landline',$guest->landline,['class'=>'form-control input-sm','placeholder'=>'Landline Number',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-phone-square"></i></a>
                            </div>
                        </div>

                    </div>
                      <div class="col-md-3 form-group">
                        <label class="control-label">Whatsapp</label>

                        <div class="input-group">
                            {!! Form::text('whatsapp',$guest->whatsapp,['class'=>'form-control input-sm','placeholder'=>'Whatsapp Number',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-whatsapp"></i></a>
                            </div>
                        </div>

                    </div>
                     <div class="col-md-3 form-group">
                        <label class="control-label">Primary email</label>

                        <div class="input-group">
                            {!! Form::text('email',$guest->email,['class'=>'form-control input-sm',
                            'placeholder'=>'Primary Email Address',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-envelope"></i></a>
                            </div>
                        </div>

                    </div>
                      <div class="col-md-3 form-group">
                        <label class="control-label">Secondary email</label>

                        <div class="input-group">
                            {!! Form::text('email2',$guest->email2,['class'=>'form-control input-sm','placeholder'=>'Secondary Email Address',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-envelope"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 form-group">
                        <label class="control-label">Home Address</label>

                        <div class="input-group">
                            {!! Form::text('homeaddress',$guest->homeaddress,['class'=>'form-control input-sm','placeholder'=>'Home Address',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-home"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 form-group">
                        <label class="control-label">City</label>

                        <div class="input-group">
                            {!! Form::text('city',$guest->city,['class'=>'form-control input-sm','placeholder'=>'City',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-building"></i></a>
                            </div>
                        </div>

                    </div>
                      <div class="col-md-3 form-group">
                        <label class="control-label">Country</label>

                        <div class="input-group">
                            {!! Form::text('country',$guest->country,['class'=>'form-control input-sm','placeholder'=>'Search Country','id'=>'country',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-map-o"></i></a>
                            </div>
                        </div>

                    </div>
                     <div class="col-md-3 form-group">
                        <label class="control-label">Postal Code</label>

                        <div class="input-group">
                            {!! Form::text('postalcode',$guest->postalcode,['class'=>'form-control input-sm','placeholder'=>'Postal Code',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-code"></i></a>
                            </div>
                        </div>

                    </div>
                     <div class="col-md-3 form-group">
                        <label class="control-label">Passport No</label>

                        <div class="input-group">
                            {!! Form::text('passport_num',$guest->passport_num,['class'=>'form-control input-sm','placeholder'=>'Passport Number',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-fax"></i></a>
                            </div>
                        </div>

                    </div>
                       <div class="col-md-3 form-group">
                        <label class="control-label">Visa No.</label>

                        <div class="input-group">
                            {!! Form::text('visa_num',$guest->visa_num,['class'=>'form-control input-sm','placeholder'=>'Visa Number',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-cc-visa"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 form-group">
                        <label class="control-label">Visa Exp</label>

                        <div class="input-group">
                            {!! Form::text('visa_exp',$guest->visa_exp,['class'=>'form-control input-sm datepicker','placeholder'=>'Visa Expiry',$readonly]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa   fa-calendar-times-o"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
                <h4>Other Information</h4>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label class="control-label">Business Segment</label>
                        <div class="input-group">
                            {!! Form::text('business_segment',$guest->business_segment,['class'=>'form-control input-sm','placeholder'=>'Business Segment',$readonly]) !!}
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa  fa-briefcase"></i></a>
                                </div>
                        </div>
                    </div>
                      <div class="col-md-3 form-group">
                        <label class="control-label">Last Visit</label>
                        <div class="input-group">
                            {!! Form::text('last_visit',$guest->last_visit,['class'=>'form-control  datepicker input-sm','placeholder'=>'Last Visit Date',$readonly]) !!}
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa   fa-calendar-check-o"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
                <h4>Notes
                <div class="row">
                    <div class="col-md-5 form-group">

                    {!! Form::textarea('notes', $guest->notes, ['class'=>'form-control', 'rows'=>'3','placeholder'=>'Notes',$readonly]) !!}
                    </div>
                </div>
                 <div class="form-group">
                    @if(!$readonly)
                    {!! Form::submit( trans('general.button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    @endif
                    <a href="{!! route('admin.hotel.guests.index') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
                 {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endsection
    @section('body_bottom')
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
              $('.searchable').select2();
            $('.datepicker').datepicker({
                    inline: true,
                    //format: 'YYYY-MM-DD',
                    dateFormat: 'yy-m-d',
                    sideBySide: true,
                });
        $("#country").autocomplete({
        source: function(request, response) {
            var results = $.ui.autocomplete.filter(countryList, request.term);        
            response(results.slice(0, 15));
        }
        });

        });
         const countryList =    [
      "Afghanistan",
      "Albania",
      "Algeria",
      "American Samoa",
      "Andorra",
      "Angola",
      "Anguilla",
      "Antigu",
      "Argentina",
      "Armenia",
      "Aruba",
      "Australia",
      "Austria",
      "Azerbaijan",
      "Bahamas",
      "Bahrain",
      "Bangladesh",
      "Barbados",
      "Belarus",
      "Belgium",
      "Belize",
      "Benin",
      "Bermuda",
      "Bhutan",
      "Bolivia",
      "Bosnia And Hercegovina",
      "Botswana",
      "Brazil",
      "British Virgin Islands",
      "Brunei",
      "Bulgaria",
      "Burkina Faso",
      "Burundi",
      "Cambodia",
      "Cameroon",
      "Canada",
      "Capeverde",
      "Cayman Islands",
      "Central African Republic",
      "Chad",
      "Chile",
      "China",
      "Colombia",
      "Comoros",
      "Congo",
      "Costa Rica",
      "Croatia",
      "Cuba",
      "Cyprus",
      "Czech Republic",
      "Denmark",
      "Djibouti",
      "Dominca",
      "Dominican Republic",
      "Ecuador",
      "Egypt",
      "El Salvador",
      "Equatorial Guinea",
      "Eritrea",
      "Estonia",
      "Ethiopia",
      "Falkland Islands",
      "Fiji",
      "Finland",
      "France",
      "Gabon",
      "Gambia",
      "Georgia",
      "Germany",
      "Ghana",
      "Greece",
      "Greenland",
      "Grenada",
      "Guam",
      "Guatemala",
      "Guinea",
      "Guinea-bissau",
      "Guyana",
      "Haiti",
      "Honduras",
      "Hungary",
      "Iceland",
      "India",
      "Indonesia",
      "Iran",
      "Iraq",
      "Ireland",
      "Israel",
      "Italy",
      "Jamaica",
      "Japan",
      "Jordan",
      "Kazakhstan",
      "Kenya",
      "Kiribati",
      "Kuwait",
      "Laos",
      "Latvia",
      "Lebanon",
      "Lesotho",
      "Liberia",
      "Libya",
      "Liechtenstein",
      "Lithuania",
      "Luxembourg",
      "Macedonia",
      "Madagascar",
      "Malawi",
      "Malaysia",
      "Maldives",
      "Mali",
      "Malta",
      "Marshall Islands",
      "Mauritania",
      "Mauritius",
      "Mexico",
      "Micronesia",
      "Moldova",
      "Monaco",
      "Mongolia",
      "Morocco",
      "Mozambique",
      "Myanmar",
      "Namibia",
      "Nauru",
      "Nepal",
      "Netherlands",
      "New Zealand",
      "Nicaragua",
      "Niger",
      "Nigeria",
      "North Korea",
      "Norway",
      "Oman",
      "Pakistan",
      "Palau",
      "Panama",
      "Papua New Guinea",
      "Paraguay",
      "Peru",
      "Philippines",
      "Poland",
      "Portugal",
      "Qatar",
      "Romania",
      "Russia",
      "Rwanda",
      "San Marino",
      "Sao Tome And Principe",
      "Saudi Arabia",
      "Senegal",
      "Serbia",
      "Seychelles",
      "Sierra Leone",
      "Singapore",
      "Slovakia",
      "Slovenia",
      "Solomon Islands",
      "Somalia",
      "outh Africa",
      "South Korea",
      "Spain",
      "Sri Lanka",
      "Sudan",
      "Suriname",
      "Swaziland",
      "Sweden",
      "Switzerland",
      "Syria",
      "Taiwan",
      "Tajikistan",
      "Tanzania",
      "Thailand",
      "Togo",
      "Tonga",
      "Trinidad And Tobago",
      "Tunisia",
      "Turkey",
      "Turkmenistan",
      "Tuvalu",
      "Uganda",
      "Ukraine",
      "United Arab Emirates",
      "United Kingdom",
      "United States Of America",
      "Uruguay",
      "Uzbekistan",
      "Vanuatu",
      "Venezuela",
      "Viet Nam",
      "Zambia",
      "Zimbabwe"
    ];
    </script>
    @endsection

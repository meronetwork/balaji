@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
             {{$page_title or "page title"}}
                <small>{{ $page_description or "page description"}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
        <div class="row">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-5">
            
        </div>
        <div class="col-md-3">
             {!! Form::open(['route' => 'admin.search.hotel.guests', 'id' => 'guests_search', 'method' => 'GET']) !!}
           
                <div class="input-group"> 
                  <input type="text" name="search" class="form-control" placeholder="Search Guests...">  
                      <span class="input-group-btn">
                        <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> 
                        </button>
                      </span>
                </div>
            
          {!! Form::close() !!}
            
        </div>
    </div>
      
    
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Name</th>
        <th>Phone Num.</th>
        <th>Profession</th>
        <th>Country</th>
        <th>Guest Type</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($guests as $key=>$gst)
    <tr>
        <td>
            <input type="checkbox" name="gst_id" value="{{$gst->id}}">
        </td>
        
        <td>#{{$gst->id}}</td>

        <td><a href="/admin/hotel/guests/show/{{$gst->id}}">{{$gst->first_name}} {{$gst->last_name}}</a></td>

        <td>{{$gst->mobile}}</td>
        <td>{{$gst->profession}}</td>
        <td>{{$gst->country}}</td>
        <td><label class="label {{$guest_type[$gst->guest_type][1]}}">{{$guest_type[$gst->guest_type][0]}}</label></td>
        <td>@if( $gst->isEditable())<a href="/admin/hotel/guests/edit/{{$gst->id}}"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
            &nbsp;&nbsp;
            @if($gst->isDeletable())
             <a href="{{route('admin.hotel.guests-confirm-delete', $gst->id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
             @else
             <i class="fa fa-trash-o deletable text-muted" title="cannot delete"></i>
             @endif
        </td>
   </tr>

    @endforeach
</tbody>

</table>

</div>
@endsection
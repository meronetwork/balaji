@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Admin | Hotel | POS Session
             <small>Admin | hotel | POS Session Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">POS Session List</font></b>
            <div style="display: inline; float: right;">
                <a class="btn btn-success btn-sm"  title="Create" href="{{ route('admin.pos-sessions.create') }}">
                   <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new POS Session</strong>
                </a> 
            </div>      
        </div>
</div>

<table class="table table-hover table-no-border" id="leads-table">

<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
</thead>

<tbody>

    @foreach($sessions as $key=>$att)
    <tr>
        <td>
            <input type="checkbox" name="event_id" value="{{$att->id}}">
        </td>

        <td>{{ $att->id }}</td>
        <td>{{ $att->name }}</td>

        <td>
            @if( $att->isEditable())
            <a href="/admin/hotel/pos-sessions/{{ $att->id }}/edit"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
            <a href="{!! route('admin.hotel.pos-sessions.confirm-delete', $att->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
        </td>
    </tr>
    @endforeach
    
</tbody>

</table>
</div>

@endsection
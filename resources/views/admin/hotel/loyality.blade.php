@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Loyality
                <small>Loyality Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
           
            <b><font size="4">Loyality List</font></b>
            <div style="display: inline; float: right;">
        
            </div>      
        </div>
</div>

<table class="table table-hover table-no-border">

<thead>
    <tr>
        <th>S.N</th>
        <th>Client</th>
        <th>Outlet</th>
        <th>Invoice</th>
        <th>Amount</th>
    </tr>
</thead>
<tbody>
    @foreach($loyality as $key=>$loyal)
    <tr>
        <td>{{++$key}}</td>
        <td>{{$loyal->client->name}}</td>
        <td>{{$loyal->outlet->name}}</td>
        <td>{{$loyal->invoice->name}}</td>
        <td><b>{{env('APP_CURRENCY')}}</b>&nbsp;{{$loyal->invoice_amount}}</td>
    </tr>
    @endforeach
</tbody>
</table>

</div>

@endsection
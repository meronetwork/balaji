@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">

    <h1>
        {{ $page_title ?? "Page Title" }}
        <small>{{ $page_description ?? "Page Description" }}
        </small>
    </h1>

</section>

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
        <div class="box box-primary">
            <div class="box-header with-border">

                &nbsp;

                <a class="btn  btn-xs" href="{!! route('admin.hotel.payment.folio.create',\Request::segment(4)) !!}" title="Create Order">
                    <i class="fa fa-plus"></i> Add Payment
                </a>

                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr>
                                <th style="text-align: center">
                                    <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                                        <i class="fa fa-check-square-o"></i>
                                    </a>
                                </th>

                                <th>Payment No</th>
                                <th>Date</th>
                                <th>Reference No</th>
                                <th>Amount</th>
                                <th>Settled in</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($paymentlist) && !empty($paymentlist))
                            @foreach($paymentlist as $o)
                            <tr>
                                <td align="center">{!! Form::checkbox('chkClient[]', $o->id); !!}</td>
                                <td>{!! sprintf("%04d", $o->id) !!}</td>
                                <td>{!! date('dS M y', strtotime($o->date )) !!}</td>
                                <td>{{ $o->reference_no }}</td>
                                <td>{!! $o->amount !!}</td>
                                <td>{!! $o->paid_by !!}</td>
                                <td><span class="label label-success">Paid</span></td>
                                <td>
                                    @if( $o->isEditable() || $o->canChangePermissions() )
                                    <a href="{!! route('admin.hotel.payment.folio.edit', [$o->reservation_id,$o->id]) !!}" title="{{ trans('general.button.edit') }}"><i class="fa fa-edit"></i></a>
                                    @else
                                    <i class="fa fa-edit text-muted" title="{{ trans('admin/orders/general.error.cant-edit-this-document') }}"></i>
                                    @endif

                                    @if($o->isDeletable())
                                    <a href="{!! route('admin.hotel.payment.folio.confirm-delete', $o->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
                                    @else

                                    <i class="fa fa-trash text-muted" title="{{ trans('admin/orders/general.error.cant-delete-this-document') }}"></i>

                                    @endif

                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }

</script>

<script>
    $(function() {
        $('#orders-table').DataTable({
            pageLength: 25
        });
    });

</script>

@endsection

@extends('layouts.master')

@section('head_extra')

 @include('partials._head_extra_select2_css')

@endsection
@section('content')
 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                 {!! $page_title !!}

                <small>{!! $page_description !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
 </section>

  <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->

                <div class="box box-primary">

                    <!-- <div class="box-header with-border">

                    </div> -->

                   <div class="box-body">
                       <form method="GET" action="/admin/orders/hotel/payments">
                    <div class="  box-header">
                           <h3 class="box-title">
                                @php
                                    $actualAmount = $payment_list->where('is_bill_active','1')->sum('amount');
                                    $returnAmount =  $payment_list->where('is_bill_active','0')->sum('amount');
                                @endphp
                                <b>Total:-</b>
                                {{ $actualAmount  }} - {{ $returnAmount }} = {{ $actualAmount - $returnAmount}}


                           </h3>
                           <span class="pull-right">
                           <small class="text-default">
                           City Ledger = Credit Groups |  Room = In Room Guests |  Complementary = Free Guests
                           </small>
                           </span>
                           <hr/>
                           <div class="row">
                              <div class="col-md-12">
                                <div class="col-md-2">
                                    <input type="text" name="start_date" class="form-control input-sm datepicker date-toogle" placeholder="Start Date"
                                    value="{{ Request::get('start_date') ?? date('Y-m-d') }}">
                                 </div>
                                      <div class="col-md-2">
                                    <input type="text" name="end_date" class="form-control input-sm datepicker date-toogle" placeholder="End Date"
                                    value="{{ Request::get('end_date') ?? date('Y-m-d') }}">
                                 </div>
{{--                                 <div class="col-md-2">--}}
{{--                                    {!! Form::select('outlet_id',$outlets,Request::get('outlet_id'),['class'=>'form-control input-sm','placeholder'=>'Select Outlets']) !!}--}}
{{--                                 </div>--}}
                                 <div class="col-md-1">
                                    {!! Form::select('paid_by',$pay_method,Request::get('paid_by'),
                                    ['class'=>'form-control input-sm','placeholder'=>'Paid By','style'=>'width: 80px;']) !!}
                                 </div>
                                 <div class="col-md-2">
                                     {!!  Form::select('client_id',$clients,
                                     Request::get('client_id'),
                                     ['class'=>'form-control input-sm searchable','placeholder'=>'Select Customer'] )!!}
                                 </div>
                                 <div class="col-md-2">
                                    <input type="text" name="bill_no" class="form-control input-sm" placeholder="Bill No."
                                    value="{{ Request::get('bill_no') }}">
                                 </div>
                                 <div class="col-md-2">
                                    <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                                    <a class="btn btn-danger btn-sm" href="/admin/orders/hotel/payments">Clear</a>
                                 </div>
                              </div>
                           </div>
                    </div>
                    </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-responsive table-striped" id="orders-table">

                                <thead>
                                    <tr style="background: #3c8dbc;color: #ffffff;">


                                        <th>Order Bill</th>
                                        <th>Voucher #</th>
                                        <th>Type</th>
                                        <th>Guest Name </th>
                                        <th>Reference No</th>
                                        <th>Amount</th>
                                        <th>Paid By</th>
                                        <th>Waiter</th>

                                    </tr>
                                </thead>

                                <tbody>
                                   @if(isset($payment_list) && !empty($payment_list))
                                     @foreach($payment_list->groupBy('reservation_id') as $paymentGrp)
                                     <tr>@php $ord = \App\Models\Orders::where('reservation_id',$paymentGrp->first()->reservation_id)->first(); @endphp
                                         <td  class="bg-danger" style="font-size: 16.5px;"> <a href="/admin/orders/{{$ord->id  }}" data-toggle="modal" data-target="#modal_dialog"
                                                 class="font-st" > {{$ord->outlet->outlet_code}}{{ $ord->bill_no }}</a> </td>
                                     </tr>
                                     @foreach($paymentGrp as $o)
                                        <tr @if( $o->is_bill_active == '0' ) class="bg-danger"   @endif>


                                            <td>{!! date('dS M y', strtotime($ord->bill_date)) !!}</td>
                                            <td>
                                                <?php
                                                $entry=\App\Models\Entry::find($o->entry_id)
                                                ?>
                                                <a target="_blank" href="/admin/entries/show/{{\FinanceHelper::get_entry_type_label($entry->entrytype_id)}}/{{$entry->id}}">{{$entry->number}}</a>

                                            </td>
                                            <td> {{ $o->is_deposit==1?'Deposit':'Regular' }} </td>
                                            <td> {{ $o->reservation->guest_name }} </td>
                                            <td>{!! $o->reference_no !!}</td>
                                            <td>{{env('APP_CURRENCY')}} {{ number_format($o->amount,2) }}</td>
                                            <td>{!! ucwords($o->paid_by) !!}</td>
                                            <td> {{$o->user->full_name }}</td>
                                        </tr>
                                    @endforeach
                                    @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div> <!-- table-responsive -->
                    </div><!-- /.box-body -->
                    <div style="text-align: center;">
                        {!! $payment_list->appends(request()->input())->links() !!}
                        {{--  {!! $orders->appends(\Request::except('page'))->render() !!} --}} </div>
                 </div><!-- /.box -->


        </div><!-- /.col -->

    </div><!-- /.row -->
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
@section('body_bottom')
@include('partials._date-toggle')
<script type="text/javascript">

    $('.searchable').select2();

     $('.date-toogle').nepalidatetoggle();


    $('.datepicker').datetimepicker({
        //inline: true,
        //format: 'YYYY-MM-DD',
        format: 'YYYY-MM-DD'
        , sideBySide: true
    });
</script>
@endsection
@endsection


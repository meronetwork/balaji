<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
	.borderless td, .borderless th {
    border: none;

}

 
 @media print {

          @page { margin: 0; }

          body { margin: 1.6cm; }

        }
</style>
<body onload="window.print()">
	<div class="container">

	<table style="width:100%; font-family:Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif';" id="content" class="table">
		<thead>
			<tr>
				<th><img src="{{ '/org/'.$organization->logo }}" style="max-width: 200px;"></th>
				<th>
					<address style="text-align: center;margin-top: 20px">
			            <span style="font-size: 20px;">{{ \Config::get('restro.APP_COMPANY', env('APP_COMPANY'))  }}</span><br>
			            {{ \Config::get('restro.APP_ADDRESS1', env('APP_ADDRESS1'))  }}<br>
			            Tel: {{ \Config::get('restro.APP_PHONE1', env('APP_PHONE1'))  }}<br/>
			            Transfer Estimate
			        </address>
			    </th>
			</tr>

		</thead>

	</table>

	<table style="width:100%; font-family:Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif';" id="content" class="table borderless">
		<thead>
			<tr>
				<th colspan="2">
					<h3>Deposit Receipt</h3>
				</th>
				<th>Date {{ strtotime($payment->date) ? date('Y-m-d',strtotime($payment->date)) : '-' }}</th>
			</tr>
		</thead>
		<tbody >
				<tr>
					<th>Received From</th>
					
                  @if($reservation->guest)
                    <td class="text-left" style="text-align: left;" >
                       
                                {{ $reservation->guest->first_name}} {{ $reservation->guest->last_name}}
                            
                    </td>
                    @else
                    <td >
                    {{ strtoupper($reservation->guest_name)}}
                	</td>
                    @endif
					<td></td>
				</tr>
				<?php 
            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
         ?>
				<tr>
					<th>The Sum of</th>
					<td>{{env('APP_CURRENCY')}}.{{ucfirst($f->format($payment->amount))}} only.</td>
					<td style="font-size: 16.5px;">{{env('APP_CURRENCY')}}  {{ number_format($payment->amount,2) }}</td>
				</tr>
				<tr>
					<th>Deposit For</th>
					<td>Hotel Reservation</td>
					<td><strong>Paid By:</strong>{{ ucfirst($payment->paid_by)  }}</td>
				</tr>
				<tr>
					<th></th>
					<td>Received by</td>
					<td style="text-align: left;">{{ $payment->user->first_name }} {{ $payment->user->last_name }}</td>
				</tr>
		</tbody>

	</table>
</div>
</body>
</html>
@extends('layouts.master')

@section('head_extra')

@include('partials._head_extra_select2_css')

@endsection
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {!! $page_title !!}

        <small>{!! $page_description !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body">

                <form action="/admin/payment/reservation/{{$id}}/deposit" method="post">

                    {{ csrf_field() }}

                    <div class="content col-md-12">

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Settle In</label>
                                    <div class="col-md-8">
                                        <select name="paid_by" class="form-control" id="colorselector" required>
                                            <option value="">Select</option>
                                            <option value="cash">Cash</option>
                                            <option value="check">Cheque</option>
                                             <option value="credit-cards">Credit cards</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Amount</label>
                                    <div class="input-group ">
                                        <input type="text" name="amount" placeholder="Amount" id="" value="{{ $payment_remain ?? 0 }}" class="form-control" required="required">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-credit-card"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="cash" class="colors" style="display:none">

                            <div class="row">
                            </div>

                        </div>
                              <div id="credit-cards" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Card Type</label>
                                        <div class="input-group ">
                                            <select class="form-control" name="cc_type">
                                                <option value="">Select</option>
                                                <option value="visa">Visa</option>
                                                <option value="master">Master</option>
                                            </select>
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-5">Name On Card</label>
                                        <div class="input-group ">
                                            <input type="text" name="cc_holder" placeholder="Name on card" id="name_on_card" value="" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Card No</label>
                                        <div class="input-group ">
                                            <input type="text" name="cc_no" placeholder="Card No" id="card_no" value="" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Expiry Date</label>
                                        <div class="input-group ">
                                            <input type="text" name="expiry_date" placeholder="Expiry Date" id="expiry_date" value="{{\Carbon\Carbon::now()->toDateString()}}" class="form-control datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="check" class="colors" style="display:none">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Bank Name</label>
                                        <div class="input-group ">
                                            <input type="text" name="bank_name" placeholder="Bank Name" id="bank_name" value="" class="form-control">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Cheque No</label>
                                        <div class="input-group ">
                                            <input type="text" name="cheque_no" placeholder="Cheque No" id="cheque_no" value="" class="form-control ">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4">Cheque Date</label>
                                        <div class="input-group ">
                                            <input type="text" name="cheque_date" placeholder="Cheque Date" id="" value="" class="form-control datepicker">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-credit-card"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Date</label>
                                    <div class="input-group ">
                                        <input type="text" name="date" id="target_date" value="{{\Carbon\Carbon::now()->toDateString()}}" placeholder="Date" class="form-control datepicker" required="required">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="far fa-calendar-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Reference No</label>
                                    <div class="input-group ">
                                        <input type="text" name="reference_no" placeholder="Reference No" id="" value="{{ old('company_id') }}" class="form-control" required>
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa fa-building"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="reservation_id" value="{{$id}}">

                        <div class="row">
                            <div class="col-md-8">
                                <label for="inputEmail3" class="control-label">
                                    Note
                                </label>
                                <textarea class="form-control" name="note" id="description" placeholder="Write Note">{!! \Request::old('note') !!}</textarea>
                            </div>
                        </div>


                    </div><!-- /.content -->

                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="btn btn-primary" type="Submit" value="Deposit">
                            <a href="javascript::void()" class="btn btn-default" onclick="closeMe()">Cancel</a>
                           {{--  <a href="/admin/folio/{{$id}}/index" class='btn btn-default'>{{ trans('general.button.cancel') }}</a> --}}
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- /.box-body -->

            <div class="row">
        <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3>Deposit History</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr class="bg-olive">
                                <th>Date</th>
                                <th>Deposit Through</th>
                                <th>Amount</th>
                                <th>Staff</th>
                                <th>Receipt</th>
                            </tr>

                            @foreach($dipositHistory as $key=>$value)
                                <tr>
                                    <td>{{$value->date }}</td>
                                    <td>{{$value->paid_by }}</td>
                                    <td>{{number_format($value->amount) }}</td>
                                    <td>{{$value->user->first_name }} {{$value->user->last_name }}</td>
                                    <td>
                                        <a href="{{ route('admin.hotel.receipt.print',$value->id) }}"><i class="fa fa-print"></i></a>
                                    </td>
                                </tr>
                            @endforeach


                        </thead>
                    </table>

                </div>




            </div>
        </div>
    </div>
</div>
    </div><!-- /.col -->




</div><!-- /.row -->

@endsection

@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>

<script type="text/javascript">
    $(function() {
        $('#colorselector').change(function() {

            $('.colors').hide();
            $('#' + $(this).val()).show();

        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#travel_agent,#city_ledger").autocomplete({
            source: "/admin/getLedgers"
            , minLength: 1
        });
    });

</script>

<script type="text/javascript">
    $('#room_no').keyup(function() {
        $("#room_no").autocomplete({
            source: "/admin/getRooms"
            , minLength: 1
        });
    });

    $('#room_no').change(function() {
        if ($(this).val() != '') {
            $.ajax({
                url: "/admin/users/ajax/getresv"
                , data: {
                    room_num: $(this).val()
                }
                , dataType: "json"
                , success: function(data) {
                    var result = data.data;
                    $('#room_detail').html(result);
                }
            });
        }
    });
function closeMe(){


    if(confirm('Canceling !!! Are you sure ??')){

        window.close();
    }
    return;
}
</script>

@endsection

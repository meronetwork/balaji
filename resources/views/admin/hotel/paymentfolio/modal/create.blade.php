<style type="text/css">
    .ui-autocomplete {
    z-index: 5000000000000000000!important;
}
</style>
<div class="box">

    <div class="box-header with-border">
            <h4>Pay Now & check out</h4>

    </div>

    <div class="box-body">

        <form action="/admin/payment/folio/{{ $id }}" method="post">

            {{ csrf_field() }}

            <div class="content">

                <div class="row">

                    <input type="hidden" name="type" value="modal">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Settle In</label>
                            <div class="col-md-8">
                                <select name="paid_by" class="form-control" id="colorselector" required>
                                    <option value="">Select</option>
                                    <option value="cash" selected>Cash</option>
                                    <option value="check">Check</option>
                                    <option value="travel-agent">Travel Agent</option>
                                    <option value="city-ledger">City ledger</option>
{{--                                    <option value="complementry">Complementry</option>--}}
                                    <option value="staff">Staff</option>
                                    <option value="credit-cards">Credit Cards</option>
                                    <option value="e-sewa">E-Sewa</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Amount</label>
                            <div class="input-group ">
                                <input type="text" name="amount" placeholder="Amount" id="" value="{{ $payment_remain }}" class="form-control" required="required" >
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-credit-card"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="cash" class="colors" style="display:none">

                    <div class="row">


                    </div>

                </div>


                <div id="check" class="colors" style="display:none">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Bank Name</label>
                                <div class="input-group ">
                                    <input type="text" name="bank_name" placeholder="Bank Name" id="bank_name" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Cheque No</label>
                                <div class="input-group ">
                                    <input type="text" name="cheque_no" placeholder="Cheque No" id="cheque_no" value="" class="form-control ">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Cheque Date</label>
                                <div class="input-group ">
                                    <input type="text" name="cheque_date" placeholder="Cheque Date" id="" value="{{\Carbon\Carbon::now()->toDateString()}}" class="form-control datepicker">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="travel-agent" class="colors" style="display:none">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Travel Ledger</label>
                                <div class="input-group ">
                                    <input type="text" name="travel_agent_ledger" placeholder="Travel Agent" id="travel_agent" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="city-ledger" class="colors" style="display:none">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">City Ledger</label>
                                <div class="input-group ">
                                    <input type="text" name="city_ledger" placeholder="City Ledger" id="city_ledger" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="complementry" class="colors" style="display:none">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-5">Staff/NC/Board</label>
                                <div class="input-group ">
                                    <input type="text" name="staff_nc_board" placeholder="Staff/NC/Board" id="staff_nc_board" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-5">Sub Type</label>
                                <div class="input-group ">
                                    <input type="text" name="sub_type" placeholder="Sub Type" id="sub_type" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-5">Staff</label>
                                <div class="input-group ">
                                    <input type="text" name="complementry_staff" placeholder="Staff" id="complementry_staff" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-5">Remarks</label>
                                <div class="input-group ">
                                    <input type="text" name="remarks" placeholder="Remarks" id="remarks" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="staff" class="colors" style="display:none">

                    <div class="row">


                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Staff Name</label>
                                <div class="input-group ">
                                    <input type="text" name="staff_name" placeholder="Staff Name" id="staff_name" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="credit-cards" class="colors" style="display:none">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Card Type</label>
                                <div class="input-group ">
                                    <select class="form-control" name="cc_type">
                                        <option value="">Select</option>
                                        <option value="visa">Visa</option>
                                        <option value="master">Master</option>
                                    </select>
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-5">Name On Card</label>
                                <div class="input-group ">
                                    <input type="text" name="cc_holder" placeholder="Name on card" id="name_on_card" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Card No</label>
                                <div class="input-group ">
                                    <input type="text" name="cc_no" placeholder="Card No" id="card_no" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">Expiry Date</label>
                                <div class="input-group ">
                                    <input type="text" name="expiry_date" placeholder="Expiry Date" id="expiry_date" value="" class="form-control datepicker">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="e-sewa" class="colors" style="display:none">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-sm-4">E-Sewa</label>
                                <div class="input-group ">
                                    <input type="text" name="e_sewa_id" placeholder="E-Sewa Id" id="e_sewa_id" value="" class="form-control">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-credit-card"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>




                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Date</label>
                            <div class="input-group ">
                                <input type="text" name="date" id="target_date" value="{{ old('target_date') ?? date('Y-m-d') }}" placeholder="Date" class="form-control datepicker" required="required">
                                <div class="input-group-addon">
                                    <a href="#"><i class="far fa-calendar-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Reference No</label>
                            <div class="input-group ">
                                <input type="text" name="reference_no" placeholder="Reference No" id="" value="
                                    {{ old('reference_no')?? (random_int(1000, 10000) .'-'.random_int(1000, 9000)) }}" class="form-control" required>
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-building"></i></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <input type="hidden" name="reservation_id" value="{{$reservation->id}}">


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Attachment</label>
                            <div class="input-group ">
                                <input type="file" name="attachment">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <label for="inputEmail3" class="control-label">
                            Note
                        </label>

                        <textarea class="form-control" name="note" id="description" placeholder="Write Note">{!! \Request::old('note') !!}</textarea>
                    </div>
                </div>


                        <div class="row">
                            <div class="col-md-12">
                            <label class="control-label">Select House Status</label>
       <select class="form-control" name="house_status_id">
          @foreach($housestatus as $hs)
           <option value="{{$hs->id}}">{{$hs->status_name}}</option>
           @endforeach
       </select>
                        </div>
                    </div>


            </div><!-- /.content -->

            <div class="col-md-12">
                <div class="form-group">
                    <input class="btn btn-primary submit_link" type="submit" value="Settle">
                    <input class="btn btn-success submit_link" type="submit" formaction="/admin/payment/folio/{{ $id }}?checkout=true" value="Settle and Checkout">
                    <a href="#"  data-dismiss="modal" class='btn btn-danger'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>

        </form>
    </div>
</div><!-- /.box-body -->
</div><!-- /.col -->

</div><!-- /.row -->

<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>

<script type="text/javascript">
    $(function() {
        $('#colorselector').change(function() {

            $('.colors').hide();
            $('#' + $(this).val()).show();

        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#travel_agent,#city_ledger").autocomplete({
            source: "/admin/getLedgers"
            , minLength: 1
        });
    });

</script>

<script type="text/javascript">
    $('#room_no').keyup(function() {
        $("#room_no").autocomplete({
            source: "/admin/getRooms"
            , minLength: 1
        });
    });

    $('#room_no').change(function() {
        if ($(this).val() != '') {
            $.ajax({
                url: "/admin/users/ajax/getresv"
                , data: {
                    room_num: $(this).val()
                }
                , dataType: "json"
                , success: function(data) {
                    var result = data.data;
                    $('#room_detail').html(result);
                }
            });
        }
    });

    $('input').each(function() {
        $(this).val($(this).val().trim());
    });

</script>

@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body ">
                <form method="post" action="/admin/hotel/transaction/groups/{{$transactiongroups->id}}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Group Code</label>
                            <input type="text" name="group_code" placeholder="Group Code" id="group_code" value="{{$transactiongroups->group_code}}" class="form-control ">
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Group Type</label>

                                {!! Form::select('group_type',['revenue'=>'Revenue','payment'=>'Payment','wrapper'=>'Wrapper'], $transactiongroups->group_type, ['class' => 'form-control searchable select2 input-sm'] )!!}

                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Enabled</label>
                            <div class="form-group">
                                <div class="col-sm-10">
                                    {!! Form::checkbox('enabled', '1', $transactiongroups->enabled) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Description</label>
                            <textarea name="description" class="form-control">{{$transactiongroups->description}}</textarea>

                        </div>

                    </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::submit( trans('general.button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    <a href="{!! route('admin.hotel.transaction.groups.index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>
        </div>


        </form>

    </div>
</div>


@endsection

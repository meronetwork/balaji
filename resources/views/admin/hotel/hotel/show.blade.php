@extends('layouts.master')
@section('content')
<style>
[data-letters]:before {
content:attr(data-letters);
display:inline-block;
font-size:1em;
width:2.5em;
height:2.5em;
line-height:2.5em;
text-align:center;
border-radius:50%;
background:red;
vertical-align:middle;
margin-right:0.3em;
color:white;
}
</style>
  <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h2>
             <a href="#" data-toggle="modal" data-target="#saveLogo">
              <small data-letters="{{mb_substr($show->hotel_name,0,3)}}"></small>
            </a>
           			{{$show->hotel_name}}
                </h2>


        </section> 
<div>
        <div class='col-md-4'>
        	    <p> <i class="fa  fa-location-arrow"></i>&nbsp;<b>Street address:</b> 
                    <a style="color: black" >  {{ ucfirst(trans($show->street_address)) }}</a> 
                  </p>
                   <p> <i class="fa  fa-building-o"></i>&nbsp;<b>City:</b> 
                    <a style="color: black" >{{ucfirst(trans($show->city))  }}</a> 
                  </p>
                  
                  <p> <i class="fa    fa-building"></i>&nbsp;<b>State:</b> 
                    <a style="color: black" > {{ ucfirst(trans($show->state)) }}</a> 
                  </p>
                  <p>
                    <i class="fa  fa-bullseye"></i>&nbsp;<b>Attraction:</b> 
                    <a style="color: black" > {{ ucfirst(trans($show->attraction_name)) }} </a> 
                  </p>
               
                   
        </div>
          <div class='col-md-4'>
        	    <p> <i class="fa  fa-location-arrow"></i>&nbsp;<b>Zipcode:</b> 
                    <a style="color: black" > {{ ucfirst(trans($show->zipcode)) }} </a> 
                  </p>
                    <p> <i class="fa    fa-user"></i>&nbsp;<b>Owner Firstname:</b> 
                    <a style="color: black" >{{ ucfirst(trans($show->owner_firstname)) }} </a> 
                  </p>
                   <p> <i class="fa   fa-user"></i>&nbsp;<b>Owner Lastname:</b> 
                    <a style="color: black" > {{ ucfirst(trans($show->owner_lastname)) }} </a> 
                  </p>
                  <p> <i class="fa"></i>
                    
                  </p>

        </div>
</div>
  
<div class="col-md-7" style="margin-left: -5px">
   <div class="form-group">
                        	<a href="javascript:history.back()" class='btn btn-danger'>Close</a>
                        	
	                                <a  href="/admin/hotel/hotel-edit/{{$show->hotel_id}}" class='btn btn-success'>Edit</a>
	                         
                        </div>
</div>
@endsection
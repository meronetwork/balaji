@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Hotel
        <small>Edit hotel</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<form method="post" action="{{route('admin.hotel.edit',$edit->hotel_id)}}">
    {{ csrf_field() }}
    <div class="panel panel-custom">
        <div class="panel-heading">
            <h3>Basic Info</h3>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">Hotel name</label>
                        <div class="input-group ">
                            <input type="text" name="hotel_name" placeholder="Hotel name" id="calculated_cost" value="{{$edit->hotel_name}}" class="form-control" required="required">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-hotel(alias)"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">City</label>
                        <div class="input-group ">
                            <input type="text" name="city" placeholder="City" id="city" class="form-control" value="{{$edit->city}}">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-building"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">Street address</label>
                        <div class="input-group ">
                            <input type="text" name="street_address" placeholder="Street address" id="calculated_cost" value="{{$edit->street_address}}" class="form-control">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-building-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">State</label>
                        <div class="input-group ">
                            <input type="text" name="state" placeholder="State.." id="calculated_cost" class="form-control" value="{{$edit->state}}">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-flag-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">Zip Code</label>
                        <div class="input-group ">
                            <input type="text" name="zipcode" placeholder="Zipcode.." id="zipcode" class="form-control" value="{{$edit->zipcode}}">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-file-zip-o (alias)"></i></a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <h3>Owner Info</h3>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">First Name</label>
                        <div class="input-group ">
                            <input type="text" name="owner_firstname" placeholder="First Name..." id="calculated_cost" class="form-control" required="required" value="{{$edit->owner_firstname}}">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-user"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-sm-6">Last Name</label>
                        <div class="input-group ">
                            <input type="text" name="owner_lastname" placeholder="Last Name..." id="calculated_cost" class="form-control" required="required" value="{{$edit->owner_lastname}}">
                            <div class="input-group-addon">
                                <a href="#"><i class="fa  fa-user"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn-submit-edit" type="submit">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

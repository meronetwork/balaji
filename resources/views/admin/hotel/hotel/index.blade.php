@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Hotel
                <small>Hotel Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
           
            <b><font size="4">Hotel List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.hotel.create') }}">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new hotel</strong>
                        </a> 
            </div>      
        </div>
</div>

<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Hotel name</th>
        <th>Street address</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($hotel as $key=>$hot)
    <tr>
        <td >
            <input type="checkbox" name="event_id" value="{{$pu->id}}">
        </td>
        <td>{{$hot->hotel_id}}</td>
        <td><a href="{{route('admin.hotel.hotel-show',$hot->hotel_id)}}"> {{ucfirst(trans($hot->hotel_name))}}</a></td>
        <td>{{$hot->street_address}}</td>
     
        <td>
            @if( $hot->isEditable())<a href="/admin/hotel/hotel-edit/{{$hot->hotel_id}}"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
            &nbsp;&nbsp;
             <a href="{!! route('admin.hotel.hotel-confirm-delete', $hot->hotel_id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
 <!--        @if ( $hot->isDeletable() )
            <a href="{!! route('admin.hotel.hotel-confirm-delete', $hot->hotel_id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
        @else
            <i class="fa fa-trash-o text-muted" title="{{ trans('admin/permissions/general.error.cant-delete-perm-in-use') }}"></i>
        @endif</td>
 -->    </tr>
    @endforeach

</tbody>
</table>
</div>

@endsection
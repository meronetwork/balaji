@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {!! $page_title !!}
        <small>{!! $page_description !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
    <p> List of all the invoice for reference</p>

</section>
<style type="text/css">
    .total{
        font-size: 16.5px;
    }
</style>
<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->

        <div class="box box-primary">

            <div class="box-body">
                <span id="index_lead_ajax_status"></span>
            <form method="GET" action="/admin/orders/allpos/invoice">
            <div class="row" style="padding: 0px;">

                       <div class="col-md-2">
                          <input type="text" name="start_date" class="form-control input-sm datepicker date-toggle" placeholder="Start Date" autocomplete="false"
                             value="{{ Request::get('start_date') }}">
                       </div>
                       <div class="col-md-2">
                          <input type="text" name="end_date" class="form-control input-sm datepicker date-toggle" placeholder="End Date" autocomplete="false"
                             value="{{ Request::get('end_date') }}">
                       </div>
                       <div class="col-md-1">
                          <input type="text" name="bill_no" class="form-control input-sm" placeholder="Bill no..." value="{{ Request::get('bill_no') }}">
                       </div>
{{--                       <div class="col-md-2">--}}
{{--                          {!! Form::select('outlet_id',$outlets,Request::get('outlet_id'),['class'=>'form-control input-sm ','placeholder'=>'Select Outlets']) !!}--}}
{{--                       </div>--}}
                <div class="col-md-2">
                          {!! Form::select('invoice_type',['tax'=>'Tax Invoice','abbr'=>'Abbreviated Tax Invoice'],Request::get('invoice_type'),['class'=>'form-control input-sm ']) !!}
                       </div>
                       <div class="col-md-2">
                          {!! Form::select('clients_id',$clients,Request::get('clients_id'),['class'=>'form-control input-sm searchable','placeholder'=>'Select Customer']) !!}
                       </div>
                       <div class="col-md-1">
                          {!! Form::select('paid_by',$pay_method,Request::get('paid_by'),
                          ['class'=>'form-control input-sm','placeholder'=>'Paid By']) !!}
                       </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="input-group">
                            {!! Form::select('fiscal_year',$allFiscalYear,$fiscal_year,['id'=>'fiscal_year_id', 'class'=>'form-control searchable input-sm', 'style'=>'width:150px; display:inline-block;'])  !!}

                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                </div>


                       <div class="col-md-2 pull-right text-right">
                          <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                          <a class="btn btn-danger btn-sm" href="/admin/orders/allpos/invoice" >Clear</a>
                       </div>


                <input type="hidden" name="search" value="true">

                </div><br/>
            </form>
            @php
            $allTotal = 0;
            @endphp
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr class="bg-olive">
                                <th>Bill No</th>
                                <th>Bill Date</th>
                                <th>Miti</th>
{{--                                <th>Ho Gst</th>--}}
                                <th>Po Gst</th>
                                <th>Outlet</th>
                                <th>Paid</th>
                                <th>Balance</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th>Paid By</th>
                                <th>Ledger Name</th>
                               {{--  <th>Tools </th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($orders) && !empty($orders))
                            @foreach($orders as $o)
                                @if($o->settlement == 1)

                                    <tr>
                                        <td class="bg-success"><a href="/admin/orders/{{$o->id}}?invoice_type={{request('invoice_type')}}">{{request('invoice_type')=='abbr'?'AI-':'TI-'}}{{$o->outlet->outlet_code}}{!! $o->bill_no !!}</a></td>
                                        <td class="bg-success"> {{ $o->bill_date }} </td>
                                        <td class="bg-success"> {{ \TaskHelper::getNepaliDate($o->bill_date)}}</td>
{{--                                        <td class="bg-success">@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>--}}

                                        <td class="bg-success">@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                        <td class="bg-success"> {{ $o->outlet->name}} </td>



                                        <?php
                                        if($o->folio->reservation_id??''){

                                            $paid_amount= \TaskHelper::getFolioPaymentAmount($o->folio->reservation_id);
                                        }else{

                                            $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id,request('invoice_type'));
                                        }
                                        $total = $o->total_amount;

                                        ?>

                                        <td class="bg-success total">{!! number_format($paid_amount,2) !!}</td>

                                        <td class="bg-success total">{!! number_format($total,2) !!}</td>

                                        @if($o->settlement == 1)
                                            <td class="bg-success"><span class="label label-success">Settled</span></td>
                                        @else
                                            <td class="bg-success "><span class="label label-warning">Not Settled</span></td>
                                        @endif

                                        <td class="bg-success total">{!! $o->total_amount !!}</td>
                                        <td class="bg-success total">{!! ReservationHelper::paidByArr($o->payments) !!}</td>
                                        <td class="bg-success total">{!! $o->getPayingLedger() !!}</td>

                                    </tr>
                                @else
                                    <tr>

                                        <td><a href="/admin/orders/{{$o->id}}?invoice_type={{request('invoice_type')}}">{{request('invoice_type')=='abbr'?'AI-':'TI-'}}{{$o->outlet->outlet_code}}{!! $o->bill_no !!}</a></td>

                                        <td> {{ $o->bill_date }} </td>
                                        <td > {{ \TaskHelper::getNepaliDate($o->bill_date)}}</td>

{{--                                        <td>@if($o->reservation_id)<a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{$o->reservation->guest_name}}</small>@endif</td>--}}

                                        <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                        <td> {{ $o->outlet->name}} </td>

                                        <?php
                                        $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id,request('invoice_type'));

                                        $total = $o->total_amount-$paid_amount;
                                        ?>
                                        <td class="total">{!! number_format($paid_amount,2) !!}</td>
                                        <td class="total">{!! number_format($total,2) !!}</td>

                                        @if($o->settlement == 1)
                                            <td><span class="label label-success">Settled</span></td>
                                        @else
                                            <td><span class="label label-warning">Not Settled</span></td>
                                        @endif

                                        <td class="total">{!! number_format($o->total_amount,2) !!}</td>
                                        <td>{!! ReservationHelper::paidByArr($o->payments) !!}</td>
                                        <td>{!! $o->getPayingLedger() !!}</td>
                                        {{--  <td>
                                             <a href="{{route('admin.orders.ordershistroy',$o->id)}}" class="text-danger" title="View History"><i class="fa  fa-history"></i></a>
                                         </td> --}}

                                    </tr>



                                @endif
                                @if($o->is_bill_active==0)
                            @if($o->settlement == 1)

                            <tr style="background-color: #f2dede;">
                                <td>Ref of {{request('invoice_type')=='abbr'?'AI-':'TI-'}}{{$o->outlet->outlet_code}}{!! $o->bill_no !!} CN TI-{{$o->credit_note_no}}</a></td>
                                <td> {{ $o->bill_date }} </td>
                                <td> {{ \TaskHelper::getNepaliDate($o->bill_date)}}</td>
{{--                                <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name??''}}@elseif($o->folio_id) {{$o->folio->reservation->client->name??''}} @else {{$o->folio->reservation->guest_name??''}}</small>@endif</td>--}}

                                <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                <td> {{ $o->outlet->name}} </td>



                                <?php
                                    if($o->folio->reservation_id??''){

                                        $paid_amount= \TaskHelper::getFolioPaymentAmount($o->folio->reservation_id);
                                    }else{

                                        $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id,request('invoice_type'));
                                    }
                                 $total = $o->total_amount;

                                ?>

                                <td class=" total">-{!! number_format($paid_amount,2) !!}</td>

                                <td class=" total">-{!! number_format($total,2) !!}</td>

                                <td ><span class="label label-danger">Cancelled</span></td>

                                <td class="total">-{!! $o->total_amount !!}</td>
                                <td class="total">{!! ReservationHelper::paidByArr($o->payments) !!}</td>
                                <td class="total">{!! $o->getPayingLedger() !!}</td>

                            </tr>
                            @else
                            <tr style="background-color: #f2dede;">

                                <td>Ref of {{request('invoice_type')=='abbr'?'AI-':'TI-'}}{{$o->outlet->outlet_code}}{!! $o->bill_no !!} CN {{request('invoice_type')=='abbr'?'AI-':'TI'}}{{$o->credit_note_no}}</a></td>

                                <td> {{ $o->bill_date }} </td>
                                <td> {{ \TaskHelper::getNepaliDate($o->bill_date)}}</td>

{{--                                <td>@if($o->reservation_id)<a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{$o->reservation->guest_name}}</small>@endif</td>--}}

                                <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                <td> {{ $o->outlet->name}} </td>

                                <?php
                                              $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id,request('invoice_type'));

                                              $total = $o->total_amount-$paid_amount;
                                ?>
                                <td class="total">-{!! number_format($paid_amount,2) !!}</td>
                                <td class="total">-{!! number_format($total,2) !!}</td>

                                <td ><span class="label label-danger">Cancelled</span></td>


                                <td class="total">-{!! number_format($o->total_amount,2) !!}</td>
                                <td>{!! ReservationHelper::paidByArr($o->payments) !!}</td>
                                <td>{!! $o->getPayingLedger() !!}</td>
                               {{--  <td>
                                    <a href="{{route('admin.orders.ordershistroy',$o->id)}}" class="text-danger" title="View History"><i class="fa  fa-history"></i></a>
                                </td> --}}

                            </tr>



                            @endif
                            @endif
                            @php   $allTotal =$o->is_bill_active==1?($allTotal+$total):($allTotal+0)  @endphp
                            @endforeach


                            @endif
                        </tbody>

                        <tr>
                            <td style="font-size: 16.5px;">Total</td>

                            <td style="font-size: 16.5px;">{{ number_format($allTotal,2) }}</td>
                        </tr>


                    </table>
                </div> <!-- table-responsive -->
            </div><!-- /.box-body -->

         {{--  <!--   @if(!\Request::get('search')) --> --}}
                <div style="text-align: center;">
                {!! $orders->appends(request()->input())->links() !!}
               {{--  {!! $orders->appends(\Request::except('page'))->render() !!} --}} </div>
          {{--   <!-- @endif --> --}}
        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
@include('partials._date-toggle')

<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }
$('.date-toggle').nepalidatetoggle();
</script>


<script type="text/javascript">
    $(document).on('change', '#order_status', function() {

        var id = $(this).closest('tr').find('.index_sale_id').val();

        var purchase_status = $(this).val();
        $.post("/admin/ajax_order_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

</script>
<script type="text/javascript">
    $("#btn-submit-filter").on("click", function() {

        status = $("#filter-status").val();
        type = $("#order_type").val();

        window.location.href = "{!! url('/') !!}/admin/orders?status=" + "&type=" + type;
    });

    $("#btn-filter-clear").on("click", function() {

        type = $("#order_type").val();
        window.location.href = "{!! url('/') !!}/admin/orders?type=" + type;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();

         $('.searchable').select2();
    });

    $('.datepicker').datetimepicker({

        format: 'YYYY-MM-DD',
    })

</script>

@endsection

@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {!! $page_title !!}
        <small>{!! $page_description ??"ROOM TRANSFER" !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
    <p> List of all the invoice for reference</p>

</section>
<style type="text/css">
    .total{
        font-size: 16.5px;
    }
</style>
<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
     
        <div class="box box-primary">
            
            <div class="box-body">
                <span id="index_lead_ajax_status"></span>
            <form method="GET" action="{{ route('admin.hotel.roomtransfer.item')  }}">
            <div class="row">
              <div class="col-md-12">
                        <div class="form-group">
                            
                            <div class="col-md-2">
                                <input type="text" name="start_date" class="form-control input-sm datepicker date-toggle" placeholder="start Date" 
                                    value="{{ Request::get('start_date') }}">
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="end_date" class="form-control input-sm datepicker date-toggle" placeholder="end Date" 
                                value="{{ Request::get('end_date') }}">
                            </div>
                            <div class="col-md-2">
                                
                                {!! Form::select('outlet_id', isset($outlets) ? $outlets : [] ,Request::get('outlet_id'),['class'=>'form-control input-sm ','placeholder'=>'Select Outlets']) !!}


                            </div>

                       

                           
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                            <a class="btn btn-danger btn-sm" type="button" href="/admin/hotel/roomtransfer/item">Clear</a>
                        </div>
                        </div>
                    </div>
                
                    
                </div><br/>
            </form>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr class="bg-purple">
                                <th>Res. No</th>
                                <th>Res. GUEST</th>
                                <th>Room Num.</th>
                                <th>Date</th>
                                <th>Outlet</th>
                                <th>Served By</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $o)
                            <tr>
                                @php   $res =$o->reservation   @endphp 
                                <td>#{{ $res->id }}</td>

                                      @if($res->guest)
                                <td title="{{ $res->remarks }}">
                                <strong style="font-size: 16.5px;">
                                <a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest->first_name}} {{ $res->guest->last_name}}</a>
                                </strong>
                                </td>
                                @else
                                <td title="{{ $res->remarks }}">
                                <strong style="font-size: 16.5px;"><a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ strtoupper($res->guest_name)}}</a></strong>
                                @endif
                                <td>{{ $o->getRoomNumber() }}</td>
                                <td>{{ $o->transaction_date }} {{ date('H:i',strtotime($o->created_at)) }}</td>
                                <td>{{ $o->outlet->name }}</td>
                                <td>{{ $o->user->first_name }} {{ $o->user->last_name }}</td>
                                <td>{!! number_format($o->total_amount,2) !!}</td>
                                <td>
                                    <a href="{{ route('orders.transfer.estimate',$o->id)}}" target="_blank">
                                        <i class="fa fa-print"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div> <!-- table-responsive -->
            </div><!-- /.box-body -->
            <div style="text-align: center;"> {!! $orders->appends(\Request::except('page'))->render() !!} </div>
        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
@include('partials._date-toggle')

<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }
$('.date-toggle').nepalidatetoggle();
</script>


<script type="text/javascript">
    $(document).on('change', '#order_status', function() {

        var id = $(this).closest('tr').find('.index_sale_id').val();

        var purchase_status = $(this).val();
        $.post("/admin/ajax_order_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

</script>
<script type="text/javascript">
    $("#btn-submit-filter").on("click", function() {

        status = $("#filter-status").val();
        type = $("#order_type").val();

        window.location.href = "{!! url('/') !!}/admin/orders?status=" + "&type=" + type;
    });

    $("#btn-filter-clear").on("click", function() {

        type = $("#order_type").val();
        window.location.href = "{!! url('/') !!}/admin/edm/order";
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
    });

    $('.datepicker').datetimepicker({

        format: 'YYYY-MM-DD',
    })

</script>

@endsection

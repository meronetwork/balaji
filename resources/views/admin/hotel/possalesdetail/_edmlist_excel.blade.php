<table class="table table-hover table-bordered" id="orders-table">
    <thead>
        <tr class="bg-maroon">
            <th>EDM No</th>
            <th>Edm Date</th>
            <th>Edm Time</th>
            <th>Outlet</th>
            <th>Served By</th>
            <th>Served For</th>
            <th>Served Waiter</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orders as $o)
        <tr>
            <td>EDM {{ $o->edm_no }}</td>
            <td>{{ $o->transaction_date }} </td>
            <td>{{date('h:i A',strtotime($o->updated_at))}} </td>
            <td>{{ $o->outlet->name }}</td>
            <td>{{ $o->user->first_name }} {{ $o->user->last_name }}</td>
            <td>{{$o->client->name}}</td>
            <td>{{ $o->served_waiter }}</td>
            <td>{!! $o->subtotal !!}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="7">Total</th>
            <th>{{ $orders->sum('subtotal')  }}</th>
        </tr>
    </tfoot>
</table>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{ env('APP_COMPANY')}} | INVOICE</title>
</head>
<body onload="window.print();" cz-shortcut-listen="true" class="skin-blue sidebar-mini">
  <table style="width:100%; font-family:Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif';">
    <tr>
      <td style="text-align: right;" colspan="7">TPIN: 6061616466</td>
    </tr>
    <tr>
      <th style="width: 20%"><img src="https://i.imgur.com/WJS6qw2.png" style="width:100%;" alt=""></th>
      <th style="width:80%;" colspan="5"><span style="font-size:40px; font-family: Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif'; text-align: center; font-weight: bold;">Yellow Pagoda Hotel</span><br>Damside Road, Pokhara, Nepal<br>Tel:+977 061-467991 | Email: info@yellowpagoda.com<br>Website: www.yellowpagoda.com</th>
    </tr>
    <tr>
      <td colspan="7" style="text-align: center; text-transform: uppercase; font-weight: bold; font-size:20px; padding:10px;">TAX Invoice</td>
    </tr>
    <tr>
      <td style="font-weight: bold;">Guest Name</td>
      <td>:</td>
      <td style="text-transform: uppercase;">@if($ord->reservation->guest_id) {{$ord->reservation->guest->full_name}} @else {{$ord->reservation->guest_name}} @endif</td>
      <td style="font-weight: bold;">Bill No.</td>
      <td>:</td>
      <td>#HYP{{$ord->id}}</td>
    </tr>
    <tr>
      <td style="font-weight: bold;">Address</td>
      <td>:</td>
      <td style="text-transform: uppercase;">@if($ord->reservation->guest_id) {{$ord->reservation->guest->address}} @else {{$ord->reservation->client->location}} @endif</td>
      <td style="font-weight: bold;">Room No.</td>
      <td>:</td>
      <td>{{$ord->reservation->room_num}}/Pax:{{$ord->reservation->occupancy}}</td>
    </tr>
    <tr>
      <td style="font-weight: bold;"></td>
      <td></td>
      <td style="text-transform: uppercase;"></td>
      <td style="font-weight: bold;">Arrival Date</td>
      <td>:</td>
      <td>{{$ord->reservation->check_in}} {{$ord->reservation->check_in_time}}</td>
    </tr>
    <tr>
      <td style="font-weight: bold;"></td>
      <td></td>
      <td style="text-transform: uppercase;"></td>
      <td style="font-weight: bold;">Departure Date</td>
      <td>:</td>
      <td>16/04/19 17:05</td>
    </tr>
    <tr>
      <td style="font-weight: bold;"></td>
      <td></td>
      <td style="text-transform: uppercase;"></td>
      <td style="font-weight: bold;">Plan</td>
      <td>:</td>
      <td>{{$ord->reservation->rateplan->package_name}}</td>
    </tr>
    <tr>
      <td style="font-weight: bold;">Company Name</td>
      <td>:</td>
      <td style="text-transform: uppercase;" colspan="4">@if($ord->customer_name)
          {!! $ord->customer_name  !!}
        @else{!! $ord->reservation->client->name  !!}@endif</td>
    </tr>   
    <tr>
      <td style="font-weight: bold;">Buyers PAN</td>
      <td>:</td>
      <td style="text-transform: uppercase;">@if($ord->customer_pan){!! $ord->customer_pan  !!}@else{!! $ord->reservation->client->vat  !!}@endif</td>
      <td style="font-weight: bold;">Group ID</td>
      <td>:</td>
      <td>045</td>
    </tr>
    <tr>
      <td style="font-weight: bold;">Billing Instruction</td>
      <td>:</td>
      <td style="text-transform: uppercase;" colspan="4">Room to company extras direct</td>
    </tr>
    <tr>
      <td colspan="7" style="border-top:1px solid #333;"></td>
    </tr>
    <tr style="font-weight: bold; position:relative;">
      <td style="padding:10px;">Date</td>
      <td>Bill No</td>
      <td>Description</td>
      <td>Rate</td>
      <td>QTY</td>
      <td>Amount</td>
    </tr>
    <tr>
      <td colspan="7" style="border-top:1px solid #333;"></td>
    </tr>
    <?php
          $n= 0;
          ?>
     @foreach($orderDetails as $odk => $odv)
      <tr>
        <td style="padding:10px;">{{ date('Y-m-d', strtotime($odv->date)) }}</td>
        <td>{{++$n}}</td>
           @if($odv->is_inventory == 1)
                     <td>{{ $odv->product->name }}</td>
                     @elseif($odv->is_inventory == 0)
                    <td>{{ $odv->description }}</td>
                    @endif 
        <td>{{$odv->price}}</td>
        <td>{{ $odv->quantity }}</td>
        <td>{{ env('APP_CURRENCY').' '.$odv->total }}</td>
      </tr>
      @endforeach

      <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td >Subtotal:</td>
        <td>{{ env('APP_CURRENCY').' '.number_format($ord->subtotal ,2)}}</td>
      </tr>

         @if($ord->discount_note == 'percentage')
            <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td >Discount Percent(%):</td>
        <td>{{ ($ord->discount_percent ? $ord->discount_percent  : '0') }}%</td>
      </tr>
            @else
            <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td>Discount Amount:</td>
        <td>{{ ($ord->discount_amount ? $ord->discount_amount  : '0') }}</td>
      </tr>
            @endif

            <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td>Service Charge:</td>
        <td>{{ env('APP_CURRENCY').' '. number_format($ord->service_charge,2) }}</td>
      </tr>

      <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td>Amount With Service :</td>
        <td>{{ env('APP_CURRENCY').' '. number_format($ord->amount_with_service,2) }}</td>
      </tr>
      <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td>Taxable Amount:</td>
        <td>{{ env('APP_CURRENCY').' '. number_format($ord->taxable_amount,2) }}</td>
      </tr>

      <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td>Tax Amount(13%)</td>
        <td>{{ env('APP_CURRENCY').' '. number_format($ord->tax_amount,2) }}</td>
      </tr>

      <tr style="font-weight: bold;">
        <td style="padding:10px;"></td>
        <td colspan="3"></td>
        <td>Total:</td>
        <td>{{ env('APP_CURRENCY').' '. number_format($ord->total_amount,2) }}</td>
      </tr>
         <?php
         
             $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

             
          ?>

    <tr style="font-weight: bold;">
      <td colspan="7" style="text-transform: uppercase; text-align: center; padding:10px;">

       In Words: {{ $f->format($ord->total_amount) }} 

      </td>
    </tr>

  </table>  
</body>
</html>

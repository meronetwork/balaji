 @extends('layouts.master')

 @section('head_extra')
 <!-- Select2 css -->
 @include('partials._head_extra_select2_css')

 <style>
     .panel .mce-panel {
         border-left-color: #fff;
         border-right-color: #fff;
     }

     .panel .mce-toolbar,
     .panel .mce-statusbar {
         padding-left: 20px;
     }

     .panel .mce-edit-area,
     .panel .mce-edit-area iframe,
     .panel .mce-edit-area iframe html {
         padding: 0 10px;
         min-height: 350px;
     }

     .mce-content-body {
         color: #555;
         font-size: 14px;
     }

     .panel.is-fullscreen .mce-statusbar {
         position: absolute;
         bottom: 0;
         width: 100%;
         z-index: 200000;
     }

     .panel.is-fullscreen .mce-tinymce {
         height: 100%;
     }

     .panel.is-fullscreen .mce-edit-area,
     .panel.is-fullscreen .mce-edit-area iframe,
     .panel.is-fullscreen .mce-edit-area iframe html {

         height: 100%;
         position: absolute;
         width: 99%;
         overflow-y: scroll;
         overflow-x: hidden;
         min-height: 100%;

     }

 </style>
 @endsection

 @section('content')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
     <h1>
         {{ $page_title ?? "Page Title" }}
         <small>
             {{ $page_description ?? "Page Description" }}
         </small>
     </h1>

     {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
 </section>

 <div class='row'>
     <div class='col-md-12'>
         <div class="box-body">

             <div id="orderFields" style="display: none;">
                 <table class="table">
                     <tbody id="more-tr">
                         <tr>
                             <td>
                                 <input type="hidden" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="1">
                                 <input type="hidden" name="flag[]" value="extracharge">
                             </td>
                             <td>
                                 <input type="text" class="form-control date datepicker" name="date[]" placeholder="Date" value="{{\Carbon\Carbon::now()}}" required="required">
                             </td>
                             <td>
                                 <input type="text" name="product_id[]" class="form-control product_id" required="required" id="product_id" onkeyup="myfun(this)" onchange="price(this)" placeholder="Prticulars" required="">
                             </td>
                             <td>

                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="text" class="form-control price" name="price[]" placeholder="Fare" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required">

                                 </div>
                             </td>

                             <td>

                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="number" class="form-control total" name="total[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                 </div>

                                 <a href="javascript::void(1);" style="width: 10%;">
                                     <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                 </a>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>

             <div id="CustomOrderFields" style="display: none;">
                 <table class="table">
                     <tbody id="more-custom-tr">
                         <tr>
                             <td>
                                 <input type="hidden" class="form-control quantity" name="custom_items_qty[]" placeholder="Quantity" min="1" value="1" required="required">
                             </td>
                             <td>
                                 <input type="text" class="form-control date datepicker" name="custom_items_date[]" placeholder="Date" value="{{\Carbon\Carbon::now()}}" required="required">
                             </td>
                             <td>
                                 <input type="text" class="form-control product" name="custom_items_name[]" value="" placeholder="Custom Prticulars">
                             </td>

                             <td>
                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="text" class="form-control price" name="custom_items_price[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required">

                                 </div>
                             </td>

                             <td>
                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="number" class="form-control total" name="custom_total[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">

                                 </div>

                                 <a href="javascript::void(1);" style="width: 10%;">
                                     <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                 </a>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>

             <div class="col-md-4">

                 <p> <i class="fa fa-user"></i> Guest Name: @if($reservation->guest_id) >{{$reservation->guest->full_name}} @else {{$reservation->guest_name}} @endif</p>

                 @if($reservation->agent->name)
                 <p> <i class="fa fa-user"></i> Agent : {{ $reservation->agent->name }}</p>
                 @endif

                 @if($reservation->room_num)
                 <p> <i class="fa fa-building"></i> Room No : {{ $reservation->room_num }}</p>
                 @endif

                 @if($reservation->check_in)
                 <p> <i class="fa fa-building"></i> Check in date : {{ $reservation->check_in }}</p>
                 @endif

                 @if($reservation->check_out)
                 <p> <i class="fa fa-building"></i> Check Out Date : {{ $reservation->check_out }}</p>
                 @endif


                 <div class="box-body">
                     <span id="index_lead_ajax_status"></span>
                     <div class="table-responsive">
                         <table class="table table-hover table-bordered" id="orders-table">
                             <thead>
                                 <tr>
                                     <th>id</th>
                                     <th>Paid </th>
                                     <th>Balance </th>
                                     <th>Total</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php
                                        $folio_total_amount = 0;
                                   ?>
                                 @if(isset($folios) && !empty($folios))
                                 @foreach($folios as $o)
                                 <tr>
                                     <td>{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                     <?php
                                              $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                             ?>

                                     <td>{!! number_format($paid_amount,2) !!}</td>

                                     <td>{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                     <td>{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                     <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>
                                 </tr>
                                 @endforeach
                                 @endif
                                 <tr>
                                     <td colspan="2">
                                     </td>
                                     <td>
                                         Total Amount:
                                     </td>
                                     <td>
                                         {{env(APP_CURRENCY)}} {{ number_format($folio_total_amount,2) }}
                                     </td>
                                 </tr>
                             </tbody>
                         </table>

                     </div> <!-- table-responsive -->

                 </div><!-- /.box-body -->


             </div>

             <div class="col-md-8">
                 <div class="">
                     <form method="POST" action="/admin/folio/{{ $reservation->id }}">
                         {{ csrf_field() }}

                         <div class="">
                             <div class="col-md-12">

                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Service Charge:</label>
                                         <select type="text" class="form-control pull-right " name="service_type" id="service_type">
                                             <option value="">Select</option>
                                             <option value="no">No</option>
                                             <option value="yes">Yes({{env('SERVICE_CHARGE')}}%)</option>
                                         </select>
                                     </div>
                                 </div>

                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Discount:</label>
                                         <select type="text" class="form-control pull-right " name="discount_note" id="discount">
                                             <option value="percentage">Percentage</option>
                                             <option value="amount">Amount</option>
                                         </select>
                                     </div>
                                 </div>

                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Folio No:</label>
                                         <input type="text" name="bill_no" value="{{$bill_no}}" class="form-control" readonly>
                                     </div>
                                 </div>


                             </div>

                             <!--   <div class="col-md-12"> 
                                     <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Bill No:</label>
                                           <input type="text" name="" value="" class="form-control" >
                                        </div>
                                    </div> 
                                </div> -->


                             <!-- <div class="col-md-12">
                                    <a href="javascript::void(0)" class="btn btn-default btn-xs" id="addMore" style="float: right;">
                                        <i class="fa fa-plus"></i> <span>Add Products Item</span>
                                    </a>
                                  &nbsp; &nbsp; &nbsp;
                                    <a href="javascript::void(0)" class="btn btn-default btn-xs" id="addCustomMore" style="float: right;">
                                        <i class="fa fa-plus"></i> <span> Add Custom Products Item</span>
                                    </a>
                                </div> -->


                             <hr />
                             <table class="table">
                                 <thead>
                                     <tr>
                                         <th></th>
                                         <th>Date</th>
                                         <th>Particulars *</th>
                                         <th>Charges *</th>
                                         <th>Total</th>
                                     </tr>
                                 </thead>

                                 <tbody>
                                     <tr class="multipleDiv">

                                     </tr>
                                 </tbody>

                                 <tfoot>

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Amount</td>
                                         <td id="sub-total">0.00</td>
                                         <td>&nbsp; <input type="hidden" name="subtotal" id="subtotal" value="0"></td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Folio Discount <span class="discounttype">(%)</span></td>
                                         <td><input type="number" min="0" name="discount_percent" id="discount_amount" value=""></td>
                                         <td>&nbsp;</td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right;"> Service Charge</td>
                                         <td id="service-charge">0.00</td>
                                         <td>&nbsp; <input type="hidden" name="service_charge" id="service_charge" value="0"></td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Amount With Service Charge</td>
                                         <td id="amount-with-service">0.00</td>
                                         <td>&nbsp; <input type="hidden" name="amount_with_service" id="amount_with_service" value="0"></td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Taxable Amount</td>
                                         <td id="taxable-amount">0.00</td>
                                         <td>&nbsp; <input type="hidden" name="taxable_amount" id="taxableamount" value="0"></td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Tax Amount (13%)</td>
                                         <td id="taxable-tax">0.00</td>
                                         <td>&nbsp; <input type="hidden" name="taxable_tax" id="taxabletax" value="0"></td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right; font-weight: bold;">Total Amount</td>
                                         <td id="total">0.00</td>
                                         <td>
                                             <input type="hidden" name="total_tax_amount" id="total_tax_amount" value="0">
                                             <input type="hidden" name="final_total" id="total_" value="0">
                                         </td>
                                     </tr>

                                 </tfoot>

                             </table>

                             <br />

                         </div>
                         <div class="panel-footer">
                             <a href="javascript::void(0)" class="btn btn-warning btn-sm" id="addMore" style="float: right;">
                                 <i class="fa fa-plus"></i> <span>POST CHARGES</span>
                             </a>
                             <button type="submit" class="btn btn-social btn-foursquare">
                                 <i class="fa fa-save"></i>Create folio
                             </button>
                         </div>
                     </form>
                 </div>
             </div>

         </div><!-- /.box-body -->
     </div><!-- /.col -->

 </div><!-- /.row -->
 @endsection

 @section('body_bottom')
 <!-- form submit -->
 @include('partials._body_bottom_submit_bug_edit_form_js')

 <script type="text/javascript">
     // $(function() {
     //     $('.datepicker').datetimepicker({
     //       //inline: true,
     //       format: 'YYYY-MM-DD',
     //       sideBySide: true,
     //       allowInputToggle: true
     //     });

     //   });

 </script>

 <script>
     function isNumeric(n) {

         return !isNaN(parseFloat(n)) && isFinite(n);

     }

     // $(document).on('change', '.product_id', function() { 
     //     var parentDiv = $(this).parent().parent();
     //     if(this.value != 'NULL')
     //     {
     //         var _token = $('meta[name="csrf-token"]').attr('content');
     //         $.ajax({
     //               type: "POST",
     //               contentType: "application/json; charset=utf-8",
     //               url: "/admin/products/GetProductDetailAjax/"+this.value+'?_token='+_token,
     //               success: function (result) {
     //                 var obj = jQuery.parseJSON(result.data);
     //                 parentDiv.find('.price').val(obj.price);

     //                 if(isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '')
     //                 {
     //                     var total = parentDiv.find('.quantity').val() * obj.price;
     //                 }
     //                 else
     //                 {
     //                     var total = obj.price;
     //                 }

     //                 var tax = parentDiv.find('.tax_rate').val();
     //                 if(isNumeric(tax) && tax != 0 && (total != 0 || total != ''))
     //                 {
     //                     tax_amount = total * parseInt(tax) / 100;
     //                     parentDiv.find('.tax_amount').val(tax_amount);
     //                     total = total + tax_amount;
     //                 }
     //                 else
     //                     parentDiv.find('.tax_amount').val('0');

     //                 parentDiv.find('.total').val(total);
     //                 calcTotal();
     //               }
     //          });
     //     }
     //     else
     //     {
     //         parentDiv.find('.price').val('');
     //         parentDiv.find('.total').val('');
     //         parentDiv.find('.tax_amount').val('');
     //         calcTotal();
     //     }
     // });

     $(document).on('change', '.customer_id', function() {
         if (this.value != '') {
             $(".quantity").each(function(index) {
                 var parentDiv = $(this).parent().parent();
                 if (isNumeric($(this).val()) && $(this).val() != '')
                     var total = $(this).val() * parentDiv.find('.price').val();
                 else
                     var total = parentDiv.find('.price').val();

                 var tax = parentDiv.find('.tax_rate').val();
                 if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                     tax_amount = total * parseInt(tax) / 100;
                     parentDiv.find('.tax_amount').val(tax_amount);
                     total = total + tax_amount;
                 } else
                     parentDiv.find('.tax_amount').val('0');

                 if (isNumeric(total) && total != '') {
                     parentDiv.find('.total').val(total);
                     calcTotal();
                 }
                 //console.log( index + ": " + $(this).text() );
             });
         } else {
             $('.total').val('0');
             $('.tax_amount').val('0');
             calcTotal();
         }
     });

     $(document).on('change', '.quantity', function() {
         var parentDiv = $(this).parent().parent();
         if (isNumeric(this.value) && this.value != '') {
             if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                 var total = parentDiv.find('.price').val() * this.value;
             } else
                 var total = '';
         } else
             var total = '';

         var tax = parentDiv.find('.tax_rate').val();
         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
             tax_amount = total * parseInt(tax) / 100;
             parentDiv.find('.tax_amount').val(tax_amount);
             total = total + tax_amount;
         } else
             parentDiv.find('.tax_amount').val('0');

         parentDiv.find('.total').val(total);
         calcTotal();
     });

     $(document).on('change', '.price', function() {
         var parentDiv = $(this).parent().parent();
         if (isNumeric(this.value) && this.value != '') {
             if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                 var total = parentDiv.find('.quantity').val() * this.value;
             } else
                 var total = '';
         } else
             var total = '';

         var tax = parentDiv.find('.tax_rate').val();
         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
             tax_amount = total * parseInt(tax) / 100;
             parentDiv.find('.tax_amount').val(tax_amount);
             total = total + tax_amount;
         } else
             parentDiv.find('.tax_amount').val('0');

         parentDiv.find('.total').val(total);
         calcTotal();
     });

     $(document).on('change', '.tax_rate', function() {
         var parentDiv = $(this).parent().parent();

         if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
             var total = parentDiv.find('.price').val() * parseInt(parentDiv.find('.quantity').val());
         } else
             var total = '';

         var tax = $(this).val();
         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
             tax_amount = total * parseInt(tax) / 100;
             parentDiv.find('.tax_amount').val(tax_amount);
             total = total + tax_amount;
         } else
             parentDiv.find('.tax_amount').val('0');

         parentDiv.find('.total').val(total);
         calcTotal();
     });

     /*$('#discount').on('change', function() {
         if(isNumeric(this.value) && this.value != '')
         {
             if(isNumeric($('#sub-total').val()) && $('#sub-total').val() != '')
                 parentDiv.find('.total').val($('#sub-total').val() - this.value).trigger('change');
         }
     });

     $("#sub-total").bind("change", function() {
         if(isNumeric($('#discount').val()) && $('#discount').val() != '')
             parentDiv.find('.total').val($('#sub-total').val() - $('#discount').val());
         else
             parentDiv.find('.total').val($('#sub-total').val());
     });*/

     $("#addMore").on("click", function() {
         //$($('#orderFields').html()).insertBefore(".multipleDiv");
         $(".multipleDiv").after($('#orderFields #more-tr').html());
         $('.datepicker').datetimepicker({
             //inline: true,
             format: 'YYYY-MM-DD'
             , sideBySide: true
             , allowInputToggle: true
         });
     });
     $("#addCustomMore").on("click", function() {
         //$($('#orderFields').html()).insertBefore(".multipleDiv");
         $(".multipleDiv").after($('#CustomOrderFields #more-custom-tr').html());
         $('.datepicker').datetimepicker({
             //inline: true,
             format: 'YYYY-MM-DD'
             , sideBySide: true
             , allowInputToggle: true
         });
     });

     $(document).on('click', '.remove-this', function() {
         $(this).parent().parent().parent().remove();
         calcTotal();
     });

     $(document).on('change', '#vat_type', function() {
         calcTotal();
     });

     $(document).on('change', '#service_type', function() {
         calcTotal();
     });

     $(document).on('change', '#discount', function() {
         calcTotal();
     });


     function calcTotal() {
         //alert('hi');
         var subTotal = 0;
         var service_total = 0;
         var service_charge = 0;
         var taxableAmount = 0;

         //var tax = parseInt($('#tax').val().replace('%', ''));
         var total = 0;
         var tax_amount = 0;
         var taxableTax = 0;
         $(".total").each(function(index) {
             if (isNumeric($(this).val()))
                 subTotal = parseInt(subTotal) + parseInt($(this).val());
         });
         $(".tax_amount").each(function(index) {
             if (isNumeric($(this).val()))
                 tax_amount = parseInt(tax_amount) + parseInt($(this).val());
         });

         $('#sub-total').html(subTotal);
         $('#subtotal').val(subTotal);

         $('#taxable-amount').html(subTotal);
         $('#taxableamount').val(subTotal);

         var service_type = $('#service_type').val();
         var discount_amount = $('#discount_amount').val();
         var vat_type = $('#vat_type').val();
         let discounttype = $('#discount').val();

         if (discounttype == 'percentage') {

             if (isNumeric(discount_amount) && discount_amount != 0) {
                 amount_after_discount = subTotal - (parseInt(discount_amount) / 100 * subTotal);
             } else {
                 amount_after_discount = subTotal;
             }

             if (service_type == 'no' || service_type == '') {
                 service_total = amount_after_discount;
                 service_charge = 0;
                 taxableAmount = service_total;
             } else {
                 service_total = amount_after_discount + parseInt(10 / 100 * amount_after_discount);
                 service_charge = parseInt(10 / 100 * amount_after_discount);
                 taxableAmount = service_total;
             }

             total = taxableAmount + parseInt(13 / 100 * taxableAmount);
             taxableTax = parseInt(13 / 100 * taxableAmount);

         } else {

             if (isNumeric(discount_amount) && discount_amount != 0) {
                 amount_after_discount = subTotal - (parseInt(discount_amount));
             } else {
                 amount_after_discount = subTotal;
             }

             if (service_type == 'no' || service_type == '') {
                 service_total = amount_after_discount;
                 service_charge = 0;
                 taxableAmount = service_total;
             } else {

                 service_total = amount_after_discount + parseInt(10 / 100 * amount_after_discount);
                 service_charge = parseInt(10 / 100 * amount_after_discount);
                 taxableAmount = service_total;

             }

             total = taxableAmount + parseInt(13 / 100 * taxableAmount);
             taxableTax = parseInt(13 / 100 * taxableAmount);

         }

         $('#amount_with_service').val(service_total);
         $('#amount-with-service').html(service_total);

         $('#service_charge').val(service_charge);
         $('#service-charge').html(service_charge);

         $('#taxableamount').val(taxableAmount);
         $('#taxable-amount').html(taxableAmount);

         $('#total_tax_amount').val(tax_amount);

         $('#taxabletax').val(taxableTax);
         $('#taxable-tax').html(taxableTax);

         $('#total').html(total);
         $('#total_').val(total);
     }

     $(document).on('keyup', '#discount_amount', function() {
         calcTotal();
     });

 </script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('.customer_id').select2();
     });

     $('#discount').on('change', function() {
         $('#discount_amount').val('');
         type = $(this).val();
         if (type == 'percentage') {

             $('.discounttype').html('(%)');
         } else {
             $('.discounttype').html('(Amount)');
         }
         calcTotal();
     });

     $('#discount_amount').keyup(function() {
         type = $('#discount').val();
         if (type == 'percentage') {

             $('.discounttype').html('(%)');

             if (this.value > 99) {
                 this.value = '99';
             } else if (this.value < 0) {
                 this.value = '0';
             }

         } else {

             $('.discounttype').html('(Amount)');
             let max = Number($('#subtotal').val());
             if (this.value > max) {
                 this.value = max;
             } else if (this.value < 0) {
                 this.value = '0';
             }

         }
     });

 </script>



 <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
 <link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
 <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
 <script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>

 <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
 <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>


 <script type="text/javascript">
     // $(document).on('click','.datepicker',function(){
     //            $('.datepicker').datetimepicker({
     //           //inline: true,
     //           format: 'YYYY-MM-DD',
     //           sideBySide: true,
     //           allowInputToggle: true
     //         });

     // }) 

 </script>

 <style type="text/css">
     .intl-tel-input {
         width: 100%;
     }

     .intl-tel-input .iti-flag .arrow {
         border: none;
     }

 </style>

 <script type="text/javascript">
     function myfun(value) {
         $(value).autocomplete({
             source: "/admin/getProductsFront"
             , minLength: 1
         });
     }

     function price(value) {

         console.log(value.value);

         var parentDiv = $(value).parent().parent();
         var el = $(value);
         if (value.value != 'NULL') {
             var _token = $('meta[name="csrf-token"]').attr('content');
             $.ajax({
                 type: "POST"
                 , contentType: "application/json; charset=utf-8"
                 , url: "/admin/articles/GetArticleDetailAjax/" + value.value + '?_token=' + _token
                 , success: function(result) {
                     var obj = jQuery.parseJSON(result.data);
                     if (obj == null) {
                         el.val('');
                         return;
                     }
                     parentDiv.find('.price').val(obj.price);

                     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                         var total = parentDiv.find('.quantity').val() * obj.price;
                     } else {
                         var total = obj.price;
                     }

                     var tax = parentDiv.find('.tax_rate').val();
                     if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                         tax_amount = total * parseInt(tax) / 100;
                         parentDiv.find('.tax_amount').val(tax_amount);
                         total = total + tax_amount;
                     } else
                         parentDiv.find('.tax_amount').val('0');

                     parentDiv.find('.total').val(total);
                     calcTotal();
                 }
             });
         } else {
             parentDiv.find('.price').val('');
             parentDiv.find('.total').val('');
             parentDiv.find('.tax_amount').val('');
             calcTotal();
         }
     }

 </script>

 @endsection

<div class="modal-content">
  <div class="modal-header bg-primary">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">
      Merge Folio
    </h4>
  </div>
  <form method="POST" action="{{ route('admin.hotel.merge_folio',$resid) }}" onsubmit="return confirm('Are you sure you want to merge')">
    @csrf
  <div class="modal-body">
    <table class="table table-striped table-hover" style="padding: 0;">
      <thead>
        <tr>
          <th>Res No.</th>
          <th>Room</th>
          <th>Choose</th>
        </tr>
      </thead>
      <tbody>
        @forelse($reservation as $key=>$value)
        <tr>
          <td>.#{{ $value->id }}</td>
          <td> {{ $value->room->room_type->room_name  }} ({{ $value->room->room_number  }}) </td>
          <td>
            <input type="radio" name="res_id" value="{{ $value->id }}" required="" style="cursor: pointer;">
          </td>
        </tr>
        @empty
          <td colspan="2" class="text-center"><h3><i class='fa  fa-smile-o'></i> No Any Booked Room</h3></td>
        @endforelse
      </tbody>
    </table>
  </div>
  <div class="modal-footer">
    @if(count($reservation) > 0)
    <button type="submit" class="btn btn-primary submit_link">Merge</button>
    @endif
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</form>
</div>
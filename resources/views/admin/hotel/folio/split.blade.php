<style type="text/css">
  #splittettingTable td, #splittettingTable td{


    line-height: 0.5 !important;
  }
</style>

<div class="modal-content">
<div class="modal-header">
   <button type="button" class="close" data-dismiss="modal">&times;</button>
   <h4 class="modal-title">Bill Split</h4>
</div>
<span class="" 
style="position: absolute;top: 80;z-index: 100;left: 48.4%;font-size: 20px;">
  <i class="fa  fa-exchange"></i>
</span>
<div class="modal-body" id='splittettingTable'>
   <div class="row">
      <div class="col-sm-6">
         <div class="panel panel-primary">
            <div class="panel-heading">From Folio</div>
            <div class="panel-body table-responsive" style="padding: 0; margin: 2;">
               <table class="table table-striped">
                <thead>
                  
                    <tr>
                      <th>Item</th>
                      <th>Price</th>
                    </tr>
                  
                </thead>
                  <tbody class="SortMe" id='fromOrder'>
                     @foreach($order_details as $key=>$value)
                     @foreach(range(1,$value->quantity) as $i)
                     <tr>
                        <td class="col-md-2">
                           <span class="label label-default">
                            {{ $value->description ?? $value->article->description }}</span>
                           <input type="hidden" class="detail_id" value="{{ $value->id }}">
                        </td>
                        <td>
                        <span class="price">{{  $value->total }}</span>
                        </td>
                     </tr>
                     @endforeach
                     @endforeach
                     <tr>
                       <td colspan="2"></td>
                     </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>Subtotal</td>
                      <td> <span id='sub-total-from'> {{ $order_details->sum('total') }} </span> </td>
                    </tr>
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
      <div class="col-sm-6">
         <div class="panel panel-success">
            <div class="panel-heading">New Folio</div>
            <div class="panel-body table-responsive"  style="padding: 0; margin: 0;">
               <table class="table table-striped ">
                  
                    <tr>
                      <th>Item</th>
                      <th>Price</th>
                    </tr>
                  
                  <tbody class="SortMe" id='toOrder'>
                     <tr>
                        <td colspan="2"></td>
                     </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>Subtotal</td>
                      <td><span id='sub-total-to'>0.00</span> </td>
                    </tr>
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>


<form method="POST" style="display: none;" id='splitBillForm' 
action="{{ route('admin.hotel.split_folio',$id) }}">
  @csrf
  <div id="splitItemArea"></div>
</form>


<div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" id='splitBill'>Submit</button>
</div>

<script type="text/javascript">


  function updatePrice(){

    var totalPrice = {
      from: 0,
      to: 0,
    }

    $('#fromOrder .price').each(function(){
    
      totalPrice.from += Number($(this).text());
    });
    $('#toOrder .price').each(function(){
      totalPrice.to += Number($(this).text());
    });
    $('#sub-total-from').text(totalPrice.from);
    $('#sub-total-to').text(totalPrice.to);

  }


  $('#splitBill').click(function(){

    $('#splitItemArea').html('');

    $('#fromOrder .detail_id').each(function(){

      let id = $(this).val();
      $('#splitItemArea').append(`<input type='hidden' name='from_product[]' value='${id}' />`);

    });

    $('#toOrder .detail_id').each(function(){

      let id = $(this).val();
      $('#splitItemArea').append(`<input type='hidden' name='to_product[]' value='${id}' />`);

    });

    $('#splitBillForm').submit();

  });


  $(document).ready(function() {
    var target = "";
    var colors = {
        new: "#8BC34A",
        ongoing: '#FFEB3B',
        completed: "#FF4081"
    }
    $('.SortMe').sortable({
        disabled: false,
        axis: 'xy',
        forceHelperSize: true,
        start: function(event, ui) {
         
        },
        connectWith: ".SortMe",
        update: function(event, ui) {
      
        },
        stop: function(event, ui) {
            updatePrice();
        }

    }).disableSelection();
    $(function() {
        $('.swapable').resizable();
    });
});
</script>
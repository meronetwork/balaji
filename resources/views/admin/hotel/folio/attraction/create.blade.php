@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Attraction
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 @if($edit)
 <form method="post" action="{{route('admin.hotel.attraction-edit',$edit->attraction_id)}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">

   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Attraction Name</label>
<div class="input-group ">
<input type="text" name="attraction_name" placeholder="Name" id="name" value="{{$edit->attraction_name}}" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>

    <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Price</label>
<div class="input-group ">
<input type="text" name="price" placeholder="Price" id="name" value="{{$edit->price}}" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>


 </div>
 <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Attraction description</label>
<div class="input-group ">
<textarea type="text" name="attraction_description" placeholder="Attraction description" id="attraction_description" class="form-control">{{$edit->attraction_description}}</textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update</button>
        </div>
 
    </div>
</div>
</div>
</div>
</form>
 @else
         <form method="post" action="{{route('admin.hotel.attraction-create')}}">
            {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">

   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Attraction name</label>
<div class="input-group ">
<input type="text" name="attraction_name" placeholder="Attraction name" id="name" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>

      <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Price</label>
<div class="input-group ">
<input type="text" name="price" placeholder="Price" id="name" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-money"></i></a>
</div>
</div>
</div>
   </div>


 </div>
 <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12">Attraction description</label>
<div class="input-group ">
<textarea type="text" name="attraction_description" placeholder="Write attraction description" id="attraction_description" class="form-control" ></textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>
@endif
  @endsection
@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Attractions
                <small>Attractions Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
           
            <b><font size="4">Attraction List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.hotel.attraction-create') }}">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new attractions</strong>
                        </a> 
            </div>      
        </div>
</div>

<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th style="text-align:center;width:20px !important">
            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                <i class="fa fa-check-square-o"></i>
            </a>
        </th>
        <th>ID</th>
        <th>Attraction name</th>
        <th>Price</th>
        <th>Descriptions</th>
           <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($attraction as $key=>$att)
    <tr>
        <td >
            <input type="checkbox" name="event_id" value="{{$pu->id}}">
        </td>
        <td>{{$att->attraction_id}}</td>
        <td>{{ucfirst(trans($att->attraction_name))}}</td>
        <td>{{$att->price}}</td>
        <td>{{$att->attraction_description}}</td>
     
        <td>
            @if( $att->isEditable())<a href="/admin/hotel/attraction-edit/{{$att->attraction_id}}"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
            &nbsp;&nbsp;
             <a href="{!! route('admin.hotel.attraction-confirm-delete', $att->attraction_id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
 <!--        @if ( $att->isDeletable() )
            <a href="{!! route('admin.hotel.attraction-confirm-delete', $att->attraction_id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
        @else
            <i class="fa fa-trash-o text-muted" title="{{ trans('admin/permissions/general.error.cant-delete-perm-in-use') }}"></i>
        @endif</td>
 -->    </tr>
    @endforeach

</tbody>
</table>
</div>

@endsection
 @extends('layouts.master')
 @section('content')

 <link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
     <h1>
         Folio Home #{{\Request::segment(4)}}
         <small>{{ $page_description ?? "Page Description" }}
         </small>
     </h1>
     Click on post item on right side to post additional charges, transfer folio or split folio <br/>
 </section>

 <div class='row'>
     <div class='col-md-12'>
         <!-- Box -->
         {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
         <div class="box box-primary">

             <div class="box-header with-border">
                 &nbsp;

                 {{-- <a class="btn btn-foursquare btn-xs" href="{!! route('admin.hotel.folio.create',$id) !!}" title="Create Folio">
                            <i class="fa fa-plus"></i> Create New Folio
                        </a> --}}

                 <a class="btn btn-warning btn-xs" href="/admin/payment/reservation/{{$id}}/deposit" title="Create Deposit" target="blank">
                     <i class="fa fa-money"></i> Deposit
                 </a>

                 <a class="btn btn-danger btn-xs" href="/admin/hotel/reservation-edit/{{\Request::segment(4)}}" title="Create Folio">
                     <i class="fa fa-times"></i> Reservation
                 </a>

                 <a class="btn bg-teal btn-xs" href="/admin/hotel/cashiering/billingdetail/{{\Request::segment(4)}}" title="Create Folio">
                     <i class="fa fa-times"></i> Billing
                 </a>

                 <a class="btn bg-olive btn-xs" href="/admin/reservation/checkouterror/{{\Request::segment(4)}}" title="Create Folio">
                     <i class="fa fa-check"></i> Checkout
                 </a>

                 {{-- <a class="btn btn-danger btn-xs" href="/admin/hotel/void/groups/confirm/{{$id}}" data-toggle="modal" data-target="#modal_dialog" title="Group Void Folio">
                 <i class="fa fa-times"></i> Void All Folios
                 </a>
                 --}}

             </div>

             <div class="box-body">

                 <div class="table-responsive">
                     <div class="col-md-12">
                         <h4>Folio Details</h4>
                     </div>
                     <table class="table table-hover table-bordered" id="orders-table">
                         <thead>
                             <tr>
                                 <th style="text-align: center">
                                    
                                 </th>
                                 <th>Folio #</th>
                                 <th>Folio Owner</th>
                                 <th>Folio Type</th>
                                 <th>Settlement Status</th>
                                 <th>Total</th>

                                 <th>Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             <?php
                                        $folio_total_amount = 0;
                                   ?>
                             @if(isset($folios) && !empty($folios))
                             @foreach($folios as $o)
                             @if($o->settlement == 1)
                             <tr>
                                 <td align="center" class="bg-success"></td>
                                 <td class="bg-success">{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                 <td class="bg-success">{!! $o->user->username !!}</td>
                                 <td class="bg-success">{!! $o->folio_type !!}</td>

                                 <td class="bg-success"><span class="label label-success">Settled</span></td>


                                 <td class="bg-success">{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                 <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>
                                 <td class="bg-success">
                                     <a href="/admin/folio/generatePDF/{{$o->id}}" title="PDF"><i class="fa fa-download"></i></a>
                                     <a href="/admin/folio/print/{{$o->id}}" title="Print"><i class="fa fa-print"></i></a>

                                 </td>

                                 <td class="bg-success">
                                     <i class="fa fa-edit text-muted" title="Can't Edit"></i>
                                     <i class="fa fa-trash text-muted" title="Can't Delete"></i>
                                     <i class="fa fa-credit-card text-muted" title="Settled"></i>
                                 </td>

                             </tr>
                             @else
                             <tr>
                                 <td align="center"></td>
                                 <td>{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>


                                 <td>{!! $o->user->username !!}</td>
                                 <td class="bg-success">{!! $o->folio_type !!}</td>

                                 <td><span class="label label-warning">Not Settled</span>&nbsp;&nbsp;<a class="label label-success" href="/admin/payment/folio/{{$o->id}}/create">Pay or Settle Now</a></td>


                                 <td>{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                 <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>



                                 <td>
                                     @if ( $o->isEditable() || $o->canChangePermissions() )
                                     <a class="btn btn-xs btn-warning" href="{!! route('admin.hotel.folio.edit', [$o->reservation_id, $o->id]) !!}" title="{{ trans('general.button.edit') }}">POST Item</a>
                                     @else
                                     <i class="fa fa-edit text-muted" title="{{ trans('admin/orders/general.error.cant-edit-this-document') }}"></i>
                                     @endif



                                     <!-- <a href="/admin/hotel/invoice/type/select/{{$o->id}}" class="btn btn-foursquare btn-xs" data-toggle="modal" data-target="#modal_dialog">Generate Invoice</a> -->


                                     <?php /**
                                                @if( $o->isDeletable())
                                                  <a href="{!! route('admin.hotel.folio.confirm-delete', $o->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
                                                @else
                                                    <i class="fa fa-trash text-muted" title="{{ trans('admin/orders/general.error.cant-delete-this-document') }}"></i>
                                                @endif
                                                **/ ?>

                                 </td>
                             </tr>
                             @endif
                             @endforeach
                             @endif

                             <tr>
                                 <td colspan="5">
                                 </td>
                                 <td>
                                     Total Amount:
                                 </td>
                                 <td>
                                     {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount,2) }}
                                 </td>
                             </tr>

                             <?php 

                                  $deposit_amount = \App\Models\FolioPayment::where('reservation_id',$id)->where('is_deposit',1)->sum('amount');
                                 ?>

                             <tr>
                                 <td colspan="5">
                                 </td>
                                 <td>
                                     Deposit Amount:
                                 </td>
                                 <td>
                                     {{env('APP_CURRENCY')}} {{ number_format($deposit_amount,2) }}
                                 </td>
                             </tr>

                             <tr>
                                 <td colspan="5">
                                 </td>
                                 <td>
                                     Remaining Amount:
                                 </td>
                                 <td>
                                     {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount-$deposit_amount,2) }}
                                 </td>
                             </tr>

                         </tbody>
                     </table>

                 </div> <!-- table-responsive -->

             </div><!-- /.box-body -->

         </div><!-- /.box -->
         <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
         {!! Form::close() !!}
     </div><!-- /.col -->
 </div><!-- /.row -->




 @endsection


 <!-- Optional bottom section for modals etc... -->
 @section('body_bottom')
 <!-- DataTables -->
 <script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

 <script language="JavaScript">
     function toggleCheckbox() {
         checkboxes = document.getElementsByName('chkClient[]');
         for (var i = 0, n = checkboxes.length; i < n; i++) {
             checkboxes[i].checked = !checkboxes[i].checked;
         }
     }

 </script>


 <script type="text/javascript">
     $(document).on('change', '#order_status', function() {

         var id = $(this).closest('tr').find('.index_sale_id').val();

         var purchase_status = $(this).val();
         $.post("/admin/ajax_order_status", {
                 id: id
                 , purchase_status: purchase_status
                 , _token: $('meta[name="csrf-token"]').attr('content')
             }
             , function(data, status) {
                 if (data.status == '1')
                     $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                 else
                     $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                 $('#index_status_update').delay(3000).fadeOut('slow');
                 //alert("Data: " + data + "\nStatus: " + status);
             });

     });

 </script>
 <script type="text/javascript">
     $("#btn-submit-filter").on("click", function() {

         fiscal_id = $("#filter-fiscal").val();
         status = $("#filter-status").val();
         customer_id = $("#filter-customer").val();
         location_id = $("#filter-location").val();
         type = $("#order_type").val();

         window.location.href = "{!! url('/') !!}/admin/orders?fiscal_id=" + fiscal_id + "&status=" + status + "&customer_id=" + customer_id + "&location_id=" + location_id + "&type=" + type;
     });

     $("#btn-filter-clear").on("click", function() {

         type = $("#order_type").val();
         window.location.href = "{!! url('/') !!}/admin/orders?type=" + type;
     });

 </script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('.customer_id').select2();
     });

 </script>

 @endsection

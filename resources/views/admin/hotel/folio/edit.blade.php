 @extends('layouts.master')

 @section('head_extra')
 <!-- Select2 css -->
 @include('partials._head_extra_select2_css')

 <style>
     .panel .mce-panel {
         border-left-color: #fff;
         border-right-color: #fff;
     }

     .panel .mce-toolbar,
     .panel .mce-statusbar {
         padding-left: 20px;
     }

     .panel .mce-edit-area,
     .panel .mce-edit-area iframe,
     .panel .mce-edit-area iframe html {
         padding: 0 10px;
         min-height: 350px;
     }

     .mce-content-body {
         color: #555;
         font-size: 14px;
     }

     .panel.is-fullscreen .mce-statusbar {
         position: absolute;
         bottom: 0;
         width: 100%;
         z-index: 200000;
     }

     .panel.is-fullscreen .mce-tinymce {
         height: 100%;
     }

     .panel.is-fullscreen .mce-edit-area,
     .panel.is-fullscreen .mce-edit-area iframe,
     .panel.is-fullscreen .mce-edit-area iframe html {
         height: 100%;
         position: absolute;
         width: 99%;
         overflow-y: scroll;
         overflow-x: hidden;
         min-height: 100%;
     }

 </style>

 @endsection

 @section('content')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
     <h1>
         {{ $page_title ?? "Page Title" }}
         <small>
             {{ $page_description ?? "Page Description" }}
         </small>
     </h1>
     Click on post charges on bottom right to post new charges <br/>
     <span>To check charges list or add charges goto Master Configuration > Front Office configuration > Transaction charges </span>
     {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}

 </section>
 <div class='row'>
     <div class='col-md-12'>
         <div class="box box-header">
             <div id="orderFields" style="display: none;">
                 <table class="table">
                     <tbody id="more-tr">
                         <tr>

                             <td>
                                 <input type="hidden" class="form-control quantity" name="quantity_new[]" placeholder="Quantity" min="1" value="1">
                                 <input type="hidden" name="flag_new[]" value="extracharge">
                                 <input type="hidden" name="is_posted_new[]" value="0">
                             </td>

                             <td>
                                 <input type="text" class="form-control date datepicker" name="date_new[]" placeholder="Date" value="{{Carbon\Carbon::now()}}" required="required">
                             </td>

                             <td>
                                 <input type="text" name="product_id_new[]" class="form-control product_id" placeholder="Particulars" value="" id="product_id" onkeyup="myfun(this)" onchange="price(this)" required="">
                             </td>

                             <td>

                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="text" class="form-control price" name="price_new[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required">

                                 </div>
                             </td>


                             <td>
                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="number" class="form-control total" name="total_new[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">

                                 </div>

                                 <a href="javascript::void(1);" style="width: 10%;">
                                     <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                 </a>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>

             <div id="CustomOrderFields" style="display: none;">
                 <table class="table">
                     <tbody id="more-custom-tr">
                         <tr>
                             <td>
                                 <input type="hidden" class="form-control quantity" name="custom_items_qty_new[]" placeholder="Quantity" min="1" value="1">
                             </td>



                             <td>
                                 <input type="text" class="form-control date datepicker" name="custom_items_date_new[]" placeholder="Date" required="required">
                             </td>


                             <td>
                                 <input type="text" class="form-control product" name="custom_items_name_new[]" value="" placeholder="Custom Particulars">
                             </td>

                             <td>


                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="text" class="form-control price" name="custom_items_price_new[]" placeholder="Price" value="@if(isset($orderDetail->price)){{ $orderDetail->price }}@endif" required="required">

                                 </div>


                             </td>

                             <td>

                                 <div class="input-group">
                                     <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                     <input type="number" class="form-control total" name="custom_total_new[]" placeholder="Total" value="@if(isset($orderDetail->total)){{ $orderDetail->total }}@endif" readonly="readonly" style="float:left; width:80%;">
                                 </div>

                                 <!-- <a href="javascript::void(1);" style="width: 10%;">
                                        <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                    </a> -->
                             </td>

                         </tr>
                     </tbody>
                 </table>
             </div>

             <div  class="col-md-3">
                <div class="row">
                    <div class="col-md-12" style="background-color: #efefef">
                 <h3 style="text-transform: uppercase;"> <strong> @if($reservation->guest_id) >{{$reservation->guest->full_name}} @else {{$reservation->guest_name}} @endif </strong></h3>
                 @if($reservation->agent_name)
                 <p> <i class="fa fa-user"></i> Agent : {{ $reservation->agent->name }}</p>
                 @endif
                 @if($folio->folio_type)
                 <p> <i class="fa fa-user"></i> Folio Type : <span class="label label-warning">{{ $folio->folio_type }}</span></p>
                 @endif
                 @if($reservation->agent->name ?? null)
                 <p> <i class="fa fa-user"></i> Company : {{ $reservation->agent->name }}</p>
                 @endif
                 @if($reservation->rate_id)
                 <p> <i class="fa fa-user"></i> Rate Plan : {{ $reservation->rateplan->package_name }}</p>
                 @endif
                 @if($reservation->rate_id)
                 <p> <i class="fa fa-user"></i> RoomType : {{ $reservation->rateplan->roomtype->room_name }}</p>
                 @endif
                 @if($reservation->room_num)
                 <p> <i class="fa fa-building"></i> Room No : {{ $reservation->room_num }}</p>
                 @endif

                 @if($reservation->check_in)
                 <p> <i class="fa fa-building"></i> Check in : {{ date('d, M yy', strtotime($reservation->check_in)) }}</p>
                 @endif
                 @if($reservation->check_out)
                 <p> <i class="fa fa-building"></i> Check Out : {{ date('d, M yy', strtotime($reservation->check_out)) }}</p>
                 @endif
                 </div> 
             </div><br>
             @if(count($mergedIn) > 0 )
             <div class="row">
                    <div class="col-md-12" style="background-color: #efefef">
                        {{  count($mergedIn)   }} Item Merged From Other Room With Total <b>{{ number_format($mergedIn->sum('total'))   }}</b>
                    </div>
            </div>
            @endif
                 @if(count($mergedOut) > 0 )
                 <div class="row">
                        <div class="col-md-12" style="background-color: #efefef">
                            {{  count($mergedOut)   }} Item Merged to Room Num. <b>{{ $mergedOut->first()->folio->reservation->room_num  }}</b> With Total<b> {{ number_format($mergedOut->sum('total'))   }}</b>
                        </div>
                </div>
                @endif
             </div>

             <div class="col-md-9">
                 <div class="">
                     <form method="POST" action="/admin/folio/{{ $reservation->id }}/update/{{$folio->id}}">
                         {{ csrf_field() }}

                         <div class="">

                             <div class="col-md-12">


                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Service Charge:</label>
                                        <input type="text" name="service_type" value="yes"  class="form-control " id="service_type" readonly="">


                                         {{-- <select type="text" class="form-control pull-right " name="service_type" id="service_type">
                                             <option value="" @if($folio->service_type == "") selected="selected"@endif>Select</option>

                                             <option value="no" @if($folio->service_type == "no") selected="selected"@endif>No</option>

                                             <option value="yes" @if($folio->service_type == "yes") selected="selected"@endif>Yes({{env('SERVICE_CHARGE')}}%)</option>
                                         </select> --}}
                                     </div>
                                 </div>
{{-- 
                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Discount:</label>
                                         <select type="text" class="form-control pull-right " name="discount_note" id="discount">
                                             <option value="percentage" @if($folio->discount_note == "percentage") selected="selected"@endif>Percentage</option>
                                             <option value="amount" @if($folio->discount_note == "amount") selected="selected"@endif>Amount</option>
                                         </select>
                                     </div>
                                 </div> --}}

                                 <div class="col-md-3">
                                     <div class="form-group">
                                         <label>Folio No:</label>
                                         <input type="text" name="bill_no" value="{{$folio->bill_no}}" class="form-control" readonly>
                                     </div>
                                 </div>

                             </div>


                             <div class="col-md-12">

                                 <!--  <a href="javascript::void(0)" class="btn btn-default btn-xs" id="addMore" style="float: right;">
                                        <i class="fa fa-plus"></i> <span>Add Products Item</span>
                                    </a>
                                  &nbsp; &nbsp; &nbsp;
                                    <a href="javascript::void(0)" class="btn btn-default btn-xs" id="addCustomMore" style="float: right;">
                                        <i class="fa fa-plus"></i> <span> Add Custom Products Item</span>
                                    </a> -->
                             </div>

                             <hr />
                             <table class="table">
                                 <thead>
                                     <tr>
                                         <th></th>
                                         <th>Date *</th>
                                         <th>Particulars *</th>
                                         <th>Charge *</th>
                                         <th>Total</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     @foreach($folioDetails as $odk => $odv)
                                     @if($odv->is_inventory == 1 && $odv->posted_to_ledger == 0)

                                     <tr>
                                         <td>
                                             <input type="hidden" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}">
                                             <input type="hidden" name="posted_to_ledger[]" value="{{$odv->posted_to_ledger}}">
                                             <input type="hidden" name="is_posted[]" value="{{$odv->is_posted}}">
                                             <input type="hidden" name="flag[]" value="{{$odv->flag}}">
                                         </td>
                                         <td>
                                             <input type="text" class="form-control date datepicker" name="date[]" placeholder="Date" value="{{ $odv->date }}" readonly>
                                         </td>
                                         <td>
                                             <input type="text" name="product_id[]" placeholder="Particulars" class="form-control product_id" required="" id="product_id" value="{{ $odv->article->description }}" onkeyup="myfun(this)" onchange="price(this)">
                                         </td>

                                         <td>
                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="text" class="form-control price" name="price[]" placeholder="Price" value="{{ $odv->price }}" required="required" readonly>
                                             </div>
                                         </td>

                                         <td>
                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="number" class="form-control total" name="total[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly" style="float:left; width:80%;">
                                             </div>
                                             @if(Auth::user()->hasRole('admins'))

                                             <a href="javascript::void(1);" style="width: 10%;">
                                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                                </a>
                                            @endif

                                         </td>
                                     </tr>
                                     @elseif($odv->is_inventory == 0 && $odv->posted_to_ledger == 0)
                                     <tr>
                                         <td>
                                             <input type="hidden" class="form-control quantity" name="quantity_custom[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}">
                                             <input type="hidden" name="posted_to_ledger_custom[]" value="{{$odv->posted_to_ledger}}">
                                             <input type="hidden" name="is_posted_custom[]" value="{{$odv->is_posted}}">
                                             <input type="hidden" name="flag_custom[]" value="{{$odv->flag}}">
                                         </td>

                                         <td>
                                             <input type="text" class="form-control date datepicker" name="date_custom[]" placeholder="Date" value="{{ $odv->date }}" required="required" readonly>
                                         </td>

                                         <td>
                                             <input type="text" class="form-control product" name="description_custom[]" value="{{ $odv->description }}" placeholder="Custom Particulars" readonly>
                                         </td>

                                         <td>

                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="text" class="form-control price" name="price_custom[]" placeholder="Fare" value="{{ $odv->price }}" required="required" readonly>
                                             </div>
                                         </td>

                                         <td>

                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="number" class="form-control total" name="total_custom[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly" style="float:left; width:80%;">
                                             </div>


                                        @if(Auth::user()->hasRole('admins'))

                                             <a href="javascript::void(1);" style="width: 10%;">
                                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                                </a>
                                            @endif
                                         </td>
                                     </tr>
                                     @elseif($odv->is_inventory == 1 && $odv->posted_to_ledger == 1)
                                     <tr>
                                         <td>
                                             <input type="hidden" class="form-control quantity" name="quantity[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}">
                                             <input type="hidden" name="posted_to_ledger[]" value="{{$odv->posted_to_ledger}}">
                                             <input type="hidden" name="is_posted[]" value="{{$odv->is_posted}}">
                                             <input type="hidden" name="flag[]" value="{{$odv->flag}}">
                                         </td>

                                         <td>
                                             <input type="text" class="form-control date datepicker" name="date[]" placeholder="Date" value="{{ $odv->date }}" readonly>
                                         </td>
                                         <td>
                                             <input type="text" name="product_id[]" placeholder="Particulars" class="form-control product_id" required="required" id="product_id" value="{{ $odv->article->description }}" onkeyup="myfun(this)" onchange="price(this)" readonly required="">
                                         </td>

                                         <td>
                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="text" class="form-control price" name="price[]" placeholder="Price" value="{{ $odv->price }}" required="required" readonly>
                                             </div>
                                         </td>

                                         <td>
                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="number" class="form-control total" name="total[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly" style="float:left; width:80%;">
                                             </div>

                                        @if(Auth::user()->hasRole('admins'))

                                             <a href="javascript::void(1);" style="width: 10%;">
                                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                                </a>
                                            @endif

                                         </td>
                                     </tr>
                                     @elseif($odv->is_inventory == 0 && $odv->posted_to_ledger == 1)
                                     <tr>
                                         <td>
                                             <input type="hidden" class="form-control quantity" name="quantity_custom[]" placeholder="Quantity" min="1" value="{{ $odv->quantity }}">
                                             <input type="hidden" name="posted_to_ledger_custom[]" value="{{$odv->posted_to_ledger}}">
                                             <input type="hidden" name="is_posted_custom[]" value="{{$odv->is_posted}}">
                                             <input type="hidden" name="flag_custom[]" value="{{$odv->flag}}">
                                         </td>

                                         <td>
                                             <input type="text" class="form-control date datepicker" name="date_custom[]" placeholder="Date" value="{{ $odv->date }}" required="required" readonly>
                                         </td>

                                         <td>
                                             <input type="text" class="form-control product" name="description_custom[]" value="{{ $odv->description }}" placeholder="Custom Particulars" readonly>
                                         </td>

                                         <td>

                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="text" class="form-control price" name="price_custom[]" placeholder="Fare" value="{{ $odv->price }}" required="required" readonly>
                                             </div>

                                         </td>

                                         <td>

                                             <div class="input-group">
                                                 <span class="input-group-addon">{{ env('APP_CURRENCY') }}</span>
                                                 <input type="number" class="form-control total" name="total_custom[]" placeholder="Total" value="{{ $odv->total }}" readonly="readonly" style="float:left; width:80%;">
                                             </div>
                                             
                                        @if(Auth::user()->hasRole('admins'))

                                             <a href="javascript::void(1);" style="width: 10%;">
                                                    <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                                </a>
                                            @endif
                                         </td>
                                     </tr>
                                     @endif
                                     @endforeach
                                     <tr class="multipleDiv">
                                     </tr>
                                 </tbody>

                                 <tfoot>
                                     <tr>
                                         <td colspan="3" style="text-align: right;">Subtotal</td>
                                         <td id="sub-total">{{ $folio->subtotal }}</td>
                                         <td>&nbsp; <input type="hidden" name="subtotal" id="subtotal" value="{{ $folio->subtotal }}"></td>
                                     </tr>
                                {{--      <tr>
                                         <td colspan="3" style="text-align: right;">Folio Discount<span class="discounttype">(%)</span></td>
                                         <td><input type="number" min="0" name="discount_percent" id="discount_amount" value="{{$folio->discount_percent }}" onKeyUp="if(this.value>99){this.value='99';}else if(this.value<0){this.value='0';}"></td>
                                         <td>&nbsp;</td>
                                     </tr> --}}
                                     <input type="hidden" min="0" name="discount_percent" id="discount_amount" value="{{$folio->discount_percent }}" onKeyUp="if(this.value>99){this.value='99';}else if(this.value<0){this.value='0';}">

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Service Charge</td>
                                         <td id="service-charge">{{ $folio->service_charge }}</td>
                                         <td>&nbsp; <input type="hidden" name="service_charge" id="service_charge" value="{{ $folio->service_charge }}"></td>
                                     </tr>

                                     <tr>
                                         <td colspan="3" style="text-align: right;">Amount With Service Charge</td>
                                         <td id="amount-with-service">{{ $folio->amount_with_service }}</td>
                                         <td>&nbsp; <input type="hidden" name="amount_with_service" id="amount_with_service" value="{{ $folio->amount_with_service }}"></td>
                                     </tr>
                                     <tr>
                                         <td colspan="3" style="text-align: right;">Taxable Amount</td>
                                         <td id="taxable-amount">{{ $folio->taxable_amount }}</td>
                                         <td>&nbsp;
                                             <input type="hidden" name="taxable_amount" id="taxableamount" value="{{ $folio->taxable_amount }}"></td>
                                     </tr>
                                     <tr>
                                         <td colspan="3" style="text-align: right;">Tax Amount </td>
                                         <td id="taxable-tax">{{ number_format($folio->tax_amount,2) }}</td>
                                         <td>&nbsp; <input type="hidden" name="taxable_tax" id="taxabletax" value="{{ $folio->tax_amount }}"></td>
                                     </tr>
                                     <tr>
                                         <td colspan="3" style="text-align: right;"><strong>TOTAL</strong></td>
                                         <td id="total">{{ $folio->total_amount }}</td>
                                         <td>
                                             <input type="hidden" name="total_tax_amount" id="total_tax_amount" value="{{ $folio->tax_amount }}">
                                             <input type="hidden" name="final_total" id="total_" value="{{ $folio->total_amount }}">
                                         </td>
                                     </tr>
                                 </tfoot>

                             </table>

                             <br />

                         </div>
                         <div class="panel-footer footer">

                             <a href="javascript::void(0)" class="btn btn-warning btn-sm" id="addMore" style="float: right;">
                                 <i class="fa fa-plus"></i> <span>POST CHARGES</span>
                             </a>

                             <button type="submit" class="btn btn-social btn-foursquare">
                                 <i class="fa fa-save"></i>Save Folio
                             </button >
                          {{--    @if($folio->is_bill_active == 1)
                             <a href="/admin/folio/void/confirm/{{$folio->id}}" class="btn btn-primary" data-toggle="modal" data-target="#modal_dialog">Void</a>@endif --}}
                             <a href="/admin/reservation/folio/{{$folio->reservation_id}}/index" class="btn btn-primary">Close</a>

                             <a href="{{ route('admin.hotel.merge_folio',
                             $folio->reservation_id) }}" data-toggle="modal" data-target="#modal_dialog" class="btn btn-danger">Transer Folio</a>


                             <a href="{{ route('admin.hotel.split_folio',$folio->id) }}" data-toggle="modal" data-target="#modal_dialog" class="btn btn-info">Split Folio</a>
                         </div>
                     </form>
                 </div>
             </div>

         </div><!-- /.box-body -->
     </div><!-- /.col -->

 </div><!-- /.row -->
 @endsection

 @section('body_bottom')
 <!-- form submit -->
 @include('partials._body_bottom_submit_bug_edit_form_js')

 <script type="text/javascript">
     // $(function() {
     //     $('.datepicker').datetimepicker({
     //       //inline: true,
     //       format: 'YYYY-MM-DD',
     //       sideBySide: true,
     //       allowInputToggle: true
     //     });

     //   });

 </script>

 <script>
     function isNumeric(n) {
         return !isNaN(parseFloat(n)) && isFinite(n);
     }

     // $(document).on('change', '.product_id', function() {
     //     var parentDiv = $(this).parent().parent();
     //     var el = $(this);
     //     if(this.value != 'NULL')
     //     {
     //         var _token = $('meta[name="csrf-token"]').attr('content');
     //         $.ajax({
     //               type: "POST",
     //               contentType: "application/json; charset=utf-8",
     //               url: "/admin/products/GetProductDetailAjax/"+this.value+'?_token='+_token,
     //               success: function (result) {
     //                 var obj = jQuery.parseJSON(result.data);
     //                 console.log(obj);
     //                 if(obj == null){
     //                     el.val('');
     //                     return false;
     //                 }
     //                 parentDiv.find('.price').val(obj.price);

     //                 if(isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '')
     //                 {
     //                     var total = parentDiv.find('.quantity').val() * obj.price;
     //                 }
     //                 else
     //                 {
     //                     var total = obj.price;
     //                 }

     //                 var tax = parentDiv.find('.tax_rate').val();
     //                 if(isNumeric(tax) && tax != 0 && (total != 0 || total != ''))
     //                 {
     //                     tax_amount = total * Number(tax) / 100;
     //                     parentDiv.find('.tax_amount').val(tax_amount);
     //                     total = total + tax_amount;
     //                 }
     //                 else
     //                     parentDiv.find('.tax_amount').val('0');

     //                 parentDiv.find('.total').val(total);
     //                 calcTotal();
     //               },

     //          });
     //     }
     //     else
     //     {
     //         parentDiv.find('.price').val('');
     //         parentDiv.find('.total').val('');
     //         parentDiv.find('.tax_amount').val('');
     //         calcTotal();
     //     }
     // });

     $(document).on('change', '.customer_id', function() {
         if (this.value != '') {
             $(".quantity").each(function(index) {
                 var parentDiv = $(this).parent().parent();
                 if (isNumeric($(this).val()) && $(this).val() != '')
                     var total = $(this).val() * parentDiv.find('.price').val();
                 else
                     var total = parentDiv.find('.price').val();

                 var tax = parentDiv.find('.tax_rate').val();
                 if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                     tax_amount = total * Number(tax) / 100;
                     parentDiv.find('.tax_amount').val(tax_amount);
                     total = total + tax_amount;
                 } else
                     parentDiv.find('.tax_amount').val('0');

                 if (isNumeric(total) && total != '') {
                     parentDiv.find('.total').val(total);
                     calcTotal();
                 }
                 //console.log( index + ": " + $(this).text() );
             });
         } else {
             $('.total').val('0');
             $('.tax_amount').val('0');
             calcTotal();
         }
     });

     $(document).on('change', '.quantity', function() {
         var parentDiv = $(this).parent().parent();
         if (isNumeric(this.value) && this.value != '') {
             if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                 var total = parentDiv.find('.price').val() * this.value;
             } else
                 var total = '';
         } else
             var total = '';

         var tax = parentDiv.find('.tax_rate').val();
         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
             tax_amount = total * Number(tax) / 100;
             parentDiv.find('.tax_amount').val(tax_amount);
             total = total + tax_amount;
         } else
             parentDiv.find('.tax_amount').val('0');

         parentDiv.find('.total').val(total);
         calcTotal();
     });

     $(document).on('change', '.price', function() {
         var parentDiv = $(this).parent().parent().parent();
         if (isNumeric(this.value) && this.value != '') {
             if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                 var total = parentDiv.find('.quantity').val() * this.value;
             } else
                 var total = '';
         } else
             var total = '';

         var tax = parentDiv.find('.tax_rate').val();
         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
             tax_amount = total * Number(tax) / 100;
             parentDiv.find('.tax_amount').val(tax_amount);
             total = total + tax_amount;
         } else
             parentDiv.find('.tax_amount').val('0');

         parentDiv.find('.total').val(total);
         calcTotal();
     });

     $(document).on('change', '.tax_rate', function() {
         var parentDiv = $(this).parent().parent();

         if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
             var total = parentDiv.find('.price').val() * Number(parentDiv.find('.quantity').val());
         } else
             var total = '';

         var tax = $(this).val();
         if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
             tax_amount = total * Number(tax) / 100;
             parentDiv.find('.tax_amount').val(tax_amount);
             total = total + tax_amount;
         } else
             parentDiv.find('.tax_amount').val('0');

         parentDiv.find('.total').val(total);
         calcTotal();
     });

     /*$('#discount').on('change', function() {
         if(isNumeric(this.value) && this.value != '')
         {
             if(isNumeric($('#sub-total').val()) && $('#sub-total').val() != '')
                 parentDiv.find('.total').val($('#sub-total').val() - this.value).trigger('change');
         }
     });

     $("#sub-total").bind("change", function() {
         if(isNumeric($('#discount').val()) && $('#discount').val() != '')
             parentDiv.find('.total').val($('#sub-total').val() - $('#discount').val());
         else
             parentDiv.find('.total').val($('#sub-total').val());
     });*/

     $("#addMore").on("click", function() {
         //$($('#orderFields').html()).insertBefore(".multipleDiv");
         $(".multipleDiv").after($('#orderFields #more-tr').html());
         $('.datepicker').datetimepicker({
             //inline: true,
             format: 'YYYY-MM-DD'
             , sideBySide: true
             , allowInputToggle: true
         });
     });
     $("#addCustomMore").on("click", function() {
         //$($('#orderFields').html()).insertBefore(".multipleDiv");
         $(".multipleDiv").after($('#CustomOrderFields #more-custom-tr').html());
         $('.datepicker').datetimepicker({
             //inline: true, 
             format: 'YYYY-MM-DD'
             , sideBySide: true
             , allowInputToggle: true
         });
     });

     $(document).on('click', '.remove-this', function() {
         $(this).parent().parent().parent().remove();
         calcTotal();
     });

     $(document).on('change', '#vat_type', function() {
         calcTotal();
     });

     $(document).on('change', '#service_type', function() {
         calcTotal();
     });

     $(document).on('change', '#discount', function() {
         calcTotal();
     });


     function calcTotal() {
         //alert('hi');
         var subTotal = 0;
         var service_total = 0;
         var service_charge = 0;
         var taxableAmount = 0;

         //var tax = Number($('#tax').val().replace('%', ''));
         var total = 0;
         var tax_amount = 0;
         var taxableTax = 0;
         $(".total").each(function(index) {
             if (isNumeric($(this).val()))
                 subTotal = Number(subTotal) + Number($(this).val());
         });
         $(".tax_amount").each(function(index) {
             if (isNumeric($(this).val()))
                 tax_amount = Number(tax_amount) + Number($(this).val());
         });
         $('#sub-total').html(subTotal);
         $('#subtotal').val(subTotal);

         $('#taxable-amount').html(subTotal);
         $('#taxableamount').val(subTotal);

         var service_type = $('#service_type').val();
         var discount_amount = $('#discount_amount').val();
         var vat_type = $('#vat_type').val();

         let discounttype = $('#discount').val();

         if (discounttype == 'percentage') {

             if (isNumeric(discount_amount) && discount_amount != 0) {
                 amount_after_discount = subTotal - (Number(discount_amount) / 100 * subTotal);
             } else {
                 amount_after_discount = subTotal;
             }

             if (service_type == 'no' || service_type == '') {
                 service_total = amount_after_discount;
                 service_charge = 0;
                 taxableAmount = service_total;
             } else {
                 service_total = amount_after_discount + Number(10 / 100 * amount_after_discount);
                 service_charge = Number(10 / 100 * amount_after_discount);
                 taxableAmount = service_total;
             }

             total = taxableAmount + Number(13 / 100 * taxableAmount);
             taxableTax = Number(13 / 100 * taxableAmount);

         } else {

             if (isNumeric(discount_amount) && discount_amount != 0) {
                 amount_after_discount = subTotal - (Number(discount_amount));
             } else {
                 amount_after_discount = subTotal;
             }

             if (service_type == 'no' || service_type == '') {
                 service_total = amount_after_discount;
                 service_charge = 0;
                 taxableAmount = service_total;
             } else {
                 service_total = amount_after_discount + Number(10 / 100 * amount_after_discount);
                 service_charge = Number(10 / 100 * amount_after_discount);
                 taxableAmount = service_total;
             }

             total = taxableAmount + Number(13 / 100 * taxableAmount);
             taxableTax = Number(13 / 100 * taxableAmount);

         }



         $('#service_charge').val(service_charge);
         $('#service-charge').html(service_charge);

         $('#amount_with_service').val(service_total);
         $('#amount-with-service').html(service_total);

         $('#taxableamount').val(taxableAmount);
         $('#taxable-amount').html(taxableAmount);

         $('#total_tax_amount').val(tax_amount);

         $('#taxabletax').val(taxableTax);
         $('#taxable-tax').html(taxableTax.toFixed(2));

         $('#total').html(total);
         $('#total_').val(total);
     }

     $(document).on('keyup', '#discount_amount', function() {
         calcTotal();
     });

 </script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('.customer_id').select2();
         let dtype = $('#discount').val();

         if (dtype == 'percentage') {
             $('.discounttype').html('(%)');
         } else {

             $('.discounttype').html('(Amount)');
         }
     });

     $('#discount').on('change', function() {
         $('#discount_amount').val('');
         type = $(this).val();
         if (type == 'percentage') {
             $('.discounttype').html('(%)');
         } else {

             $('.discounttype').html('(Amount)');
         }
         calcTotal();
     });

     $('#discount_amount').keyup(function() {

         type = $('#discount').val();
         if (type == 'percentage') {

             $('.discounttype').html('(%)');
             if (this.value > 99) {
                 this.value = '99';
             } else if (this.value < 0) {
                 this.value = '0';
             }

         } else {

             $('.discounttype').html('(Amount)');
             let max = Number($('#subtotal').val());
             if (this.value > max) {
                 this.value = max;
             } else if (this.value < 0) {
                 this.value = '0';
             }

         }

     });

 </script>

 <script type="text/javascript">
     $(function() {
         $('.datepicker').datetimepicker({
             //inline: true,
             format: 'YYYY-MM-DD'
             , sideBySide: true
             , allowInputToggle: true
         });

     });

 </script>

 <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
 <link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
 <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
 <script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>

 <style type="text/css">
     .intl-tel-input {
         width: 100%;
     }

     .intl-tel-input .iti-flag .arrow {
         border: none;
     }

 </style>

 <script type="text/javascript">
     function myfun(value) {
         $(value).autocomplete({
             source: "/admin/getProductsFront"
             , minLength: 1
         });
     }

     function price(value) {


         var parentDiv = $(value).parent().parent();
         if (value.value != 'NULL') {
             var _token = $('meta[name="csrf-token"]').attr('content');
             var el = $(value);
             $.ajax({
                 type: "POST"
                 , contentType: "application/json; charset=utf-8"
                 , url: "/admin/articles/GetArticleDetailAjax/" + value.value + '?_token=' + _token
                 , success: function(result) {
                     var obj = jQuery.parseJSON(result.data);
                     console.log(obj);
                     if (obj == null) {

                         el.val('');
                         return false;
                     }
                     parentDiv.find('.price').val(obj.price);

                     if (isNumeric(parentDiv.find('.quantity').val()) && parentDiv.find('.quantity').val() != '') {
                         var total = parentDiv.find('.quantity').val() * obj.price;
                     } else {
                         var total = obj.price;
                     }

                     var tax = parentDiv.find('.tax_rate').val();
                     if (isNumeric(tax) && tax != 0 && (total != 0 || total != '')) {
                         tax_amount = total * Number(tax) / 100;
                         parentDiv.find('.tax_amount').val(tax_amount);
                         total = total + tax_amount;
                     } else
                         parentDiv.find('.tax_amount').val('0');

                     parentDiv.find('.total').val(total);
                     calcTotal();
                 },

             });
         } else {
             parentDiv.find('.price').val('');
             parentDiv.find('.total').val('');
             parentDiv.find('.tax_amount').val('');
             calcTotal();
         }
     }
$(document).on('hidden.bs.modal', '#modal_dialog' , function(e){
        $('#modal_dialog .modal-content').html('');    
   });

 </script>

 @endsection

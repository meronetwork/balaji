
<html>
   <head>
      <meta charset="utf-8">
      <title>{{ env('APP_COMPANY')}} | INVOICE</title>
   </head>
   <style type="text/css">
      #content {
      position: absolute;
      z-index: 1;
      }
      #bg-text {
      color: lightgrey;
      opacity: 0.5;
      font-size: 120px;
      text-align: center;
      margin-top: 40%;
      transform: rotate(330deg);
      -webkit-transform: rotate(330deg);
      }
      @media all {
      .page-break { display: none; }
      }
      @media print {
      .page-break { display: block; page-break-before: always; }
      }
      @media print {
      @page { margin: 0; }
      body { margin: 1.6cm; }
      }
   </style>
   <body onload="window.print();">
      <?php 
         $pagesToPrint =  $print_no  < 1 ? 2 : 1;    
         ?>
      @for($i=0;$i<$pagesToPrint;$i++)   
      <div class="container">
         <table 
            style="width:100%; font-family:Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif';" >
            <tr>
               <td style="text-align: right;padding-right:10px;" colspan="6">TPID: {{env('TPID')}}</td>
            </tr>
            <tr>
               <th style="width: 20%"><img src="{{ '/org/'.$organization->logo }}" style="max-width: 200px;"></th>
               <th style="width:80%;" colspan="5"><span style="font-size:30px; font-family: Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif'; text-align: center; font-weight: bold;">{{env('APP_COMPANY')}} </span><br>{{env('APP_ADDRESS1')}}<br>{{env('APP_ADDRESS2')}}<br>Tel:{{env('APP_PHONE1')}} | Email: {{env('APP_EMAIL')}}<br>Website: {{env('APP_WEBSITE')}}</th>
            </tr>
            <tr>
               <td colspan="7" style="text-align: center; text-transform: uppercase; font-weight: bold; font-size:20px; padding:10px;">
                @if($print_no == 0 && $i == 0) TAX Invoice @else Invoice @endif
               </td>
            </tr>
            <tr>
               <td colspan="7" style="text-align: center; text-transform: uppercase; font-weight: bold;">
                  @if($print_no > 0) Copy of {{$print_no}} @endif
               </td>
            </tr>
            <tr>
               <td style="font-weight: bold";>Guest's Name</td>
               <td>:</td>
               <td style="text-transform: uppercase;">@if($ord->folio->reservation->guest_id) {{$ord->folio->reservation->guest->full_name}} @else {{$ord->folio->reservation->guest_name}} @endif</td>
               <td style="font-weight: bold;">Bill No.</td>
               <td>:</td>
               <td>{{$ord->outlet->outlet_code}}{{$ord->bill_no}}</td>
            </tr>
            <tr>
               <td style="font-weight: bold;">Address</td>
               <td>:</td>
               <td style="text-transform: uppercase;">@if($ord->folio->reservation->guest_id ?? '') {{$ord->folio->reservation->guest->address ?? ''}} @else {{$ord->folio->reservation->client->location ?? ''}} @endif</td>
               <td style="font-weight: bold;">Room No.</td>
               <td>:</td>
               <td>{{$ord->folio->reservation->room_num}}/Pax:{{$ord->folio->reservation->occupancy}}</td>
            </tr>
            <tr>
               <td style="font-weight: bold;"></td>
               <td></td>
               <td style="text-transform: uppercase;"></td>
               <td style="font-weight: bold;">Arrival Date</td>
               <td>:</td>
               <td>{{$ord->folio->reservation->check_in}} {{$ord->folio->reservation->check_in_time}}</td>
            </tr>
            <tr>
               <td style="font-weight: bold;"></td>
               <td></td>
               <td style="text-transform: uppercase;"></td>
               <td style="font-weight: bold;">Departure Date</td>
               <td>:</td>
               <td>{{$ord->folio->reservation->check_out}} {{$ord->folio->reservation->check_out_time}}</td>
            </tr>
            <tr>
               <td style="font-weight: bold;"></td>
               <td></td>
               <td style="text-transform: uppercase;"></td>
               <td style="font-weight: bold;">Plan</td>
               <td>:</td>
               <td>{{$ord->folio->reservation->rateplan->package_name}}</td>
            </tr>
            <tr>
               <td style="font-weight: bold;">Company Name</td>
               <td>:</td>
               <td style="text-transform: uppercase;" colspan="4">
                  {!! $ord->folio->reservation->client->name  ?? '' !!}
               </td>
            </tr>
            <tr>
               <td style="font-weight: bold;">Buyers PAN</td>
               <td>:</td>
               <td style="text-transform: uppercase;"> @if($ord->folio->reservation->client ) {!! $ord->folio->reservation->client->vat ?? '' !!} @else{!! $ord->reservation->client->vat ??'' !!}@endif</td>
               <td style="font-weight: bold;">Group ID</td>
               <td>:</td>
               <td>045</td>
            </tr>
            <tr>
               <td style="font-weight: bold;">Billing Instruction</td>
               <td>:</td>
               <td style="text-transform: uppercase;" colspan="4">{{ $ord->folio->reservation->bill_to }}</td>
            </tr>
            <tr>
               <td colspan="7" style="border-top:1px solid #333;"></td>
            </tr>
            <tr style="font-weight: bold; position:relative;">
               <td style="padding:10px;">Date</td>
               <td> No</td>
               <td>Particulars</td>
               <td>Charge</td>
               <td>Amount</td>
            </tr>
            <tr>
               <td colspan="7" style="border-top:1px solid #333;"></td>
            </tr>
            <?php
             $n= 0;
             $disAmount = 0;
             $discountTotal = 0;
            ?>
            @foreach($orderDetails as $odk => $odv)
             @if($odv->price  > 0 )
            <tr>
               <td >{{ date('Y-m-d', strtotime($odv->date)) }}</td>
               <td>{{++$n}}</td>
               @if($odv->is_inventory == 1)
               <td>{{ $odv->article->description }}</td>
               @elseif($odv->is_inventory == 0)
               <td>{{ $odv ->description }}</td>
               @endif
               <td>{{$odv->price}}</td>
               <td>{{ env('APP_CURRENCY').' '.$odv->total }}</td>
            </tr>
            @else
               @php  $discountTotal +=  abs($odv['price']) @endphp 
            @endif
            @endforeach
            
            @if($ord->discount_note == 'percentage')
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Discount Percent(%):</td>
               <td>{{ ($ord->discount_percent ? $ord->discount_percent  : '0') }}%</td>
            </tr>
            @else
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Discount Amount:</td>
            @php  
            $disAmount = ($ord->discount_amount ? $ord->discount_amount  : '0');
            $disAmount += $discountTotal;
            @endphp 

            <td>{{ $disAmount }}</td>
            </tr>
            @endif

            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Subtotal:</td>
               <td>{{ env('APP_CURRENCY').' '.number_format($ord->subtotal ,2)}}</td>
            </tr>
          
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Service Charge:</td>
               <td>{{ env('APP_CURRENCY').' '. number_format($ord->service_charge,2) }}</td>
            </tr>
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Amount With Service :</td>
               <td>{{ env('APP_CURRENCY').' '. number_format($ord->amount_with_service,2) }}</td>
            </tr>
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Taxable Amount:</td>
               <td>{{ env('APP_CURRENCY').' '. number_format($ord->taxable_amount,2) }}</td>
            </tr>
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Tax Amount(13%)</td>
               <td>{{ env('APP_CURRENCY').' '. number_format($ord->tax_amount,2) }}</td>
            </tr>
            <tr style="font-weight: bold;">
               <td ></td>
               <td colspan="3" style="text-align: right;">Total:</td>
               <td>{{ env('APP_CURRENCY').' '. number_format($ord->total_amount,2) }}</td>
            </tr>
            <?php
               $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);             
               ?>
            <tr style="font-weight: bold;">
               <td colspan="7" style="text-transform: uppercase; text-align: center; ">
                  In Words: {{ $f->format($ord->total_amount) }}
               </td>
            </tr>
            <tr>
               <td colspan="7" >
                  {{ \Auth::user()->first_name }} {{\Auth::user()->last_name}}
               </td>
            </tr>
            <tr>
               <td colspan="7" >
                  {{date('Y-m-d h:i:sa')}}
               </td>
            </tr>
         </table>
      </div>
      @if($i == 0 && $pagesToPrint > 1)
      <div class="page-break"></div>
      @endif
      @endfor
   </body>
</html>

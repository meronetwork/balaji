@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body ">
                <form method="post" action="/admin/hotel/articles/{{$articles->id}}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Code</label>
                            <input type="text" name="code" placeholder="Code" id="code" value="{{$articles->code}}" class="form-control ">
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Transaction Code</label>
                            <select name="transaction_code_id" class="form-control">
                                <option value="">Select</option>
                                @foreach($transactioncodes as $tc)
                                <option value="{{$tc->id}}" @if($tc->id == $articles->transaction_code_id) selected @endif)>{{$tc->code}}</option>

                                @endforeach

                            </select>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Price</label>
                            <input type="text" name="price" placeholder=" Price" id="code" value="{{$articles->price}}" class="form-control ">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Sequence</label>
                            <input type="text" name="sequence" placeholder="Default Price" id="code" value="{{$articles->sequence}}" class="form-control ">
                        </div>


                        <div class="col-md-4">
                            <label class="control-label">Description</label>
                            <textarea name="description" class="form-control">{{$articles->description}}</textarea>
                        </div>

                    </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::submit( trans('general.button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    <a href="{!! route('admin.hotel.articles.index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>
        </div>


        </form>

    </div>
</div>


@endsection

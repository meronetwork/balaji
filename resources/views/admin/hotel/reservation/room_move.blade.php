@extends('layouts.master')
@section('content')

<style>
  tr td { text-align:left !important; }
</style>

    <div class='row'>
        <div class='col-md-6'>
        	<!-- Box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="">
                            <p> Transfer History for Reservation #{{ Request::segment(3) }}</p>
                            <table class="table table-hover table-bordered" id="leads-table">
                                <thead>
                                    <tr>
                                        <th>From Room</th>
                                        <th>To Room</th>
                                        <th>Date</th>
                                    </tr>
                                    @if($history)
                                    @foreach($history as $k => $v)
                                    <tr>
                                        <td>{{ $v->from_room }}
                                        </td>
                                        <td>{{ $v->to_room }} </td>
                                        <td>{{ $v->moved_date }} </td>
                                    </tr>
                                    @endforeach
                                    @endif

                                </thead>
                            </table>

                          <form method="post" action="{{route('admin.hotel.moveroom',$res_id)}}">
                            {{csrF_field()}}
                                       <label>Moving Date</label> <i class="fa fa-calendar"></i> 
                          <div class="row">
                            <div class="col-md-4" >
                        <div class="form-group">  
                    <div class="input-group ">
                      <input required="" type="text" class="form-control occupied_date_from input-sm" value="{{ isset($edit) ? $edit->check_in : null}}" name="moved_date" id="check_in" readonly="" >
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                    </div>
                </div>
            </div>
        </div>
                            <label>Transfer room to</label> <i class="fa fa-hotel (alias)"></i> 
                            <div class="row">

                            <div class="col-md-4">
                            
           
                        <select class = 'form-control input-sm' name="to_room" id="to_room" required="">
                        <option value="">Select Room</option>
                        @foreach( isset($room) ? $room: []  as $rm)
                        <option value="{{$rm['room_number']}}">{{$rm['room_number']}}({{$rm['room_name']}})</option>
                        @endforeach 
                        </select>
                        <input type="hidden" name="reservation_id" value="{{$res_id}}">
                       </div>
                          </div>
                          <br>
               
        <button class="btn btn-primary" type="submit">Move</button>
<div class="row">
                        <div class="col-md-3">
                    
                     </div>
                       
                    
                </div>
            </div>


                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection


@section('body_bottom')
  <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

 <script type="text/javascript">
    $(function() {
            $('#check_in').datepicker({
                dateFormat: 'yy-m-d',
                sideBySide: true,
                minDate : new Date('{{$check_in}}'),
                maxDate:  new Date('{{$check_out}}')
                });
        });
        function getroom(date){  
         $('#to_room').empty();    
         $('#to_room').append("<option value=''>Select room </option>")
         $.get('/admin/hotel/reservation/checkdate/'+date,function(data,status){
          console.log(data);
          for(let room of data){
          $('#to_room').append(("<option value="+room.room_number+">"+room.room_number+"("+room.room_name+")</option>"));
          }
        });
        }

        $('#check_in').on('change',function(){
          getroom($(this).val());
        });
        
</script>
@endsection


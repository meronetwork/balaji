@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Folio
                <small>Unsettled Folio Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">Unsettled Folio List</font></b>
        </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>Res ID</th>
        <th>Room #</th>
        <th>Rooms</th>
        <th>Check in</th>
        <th>Check out</th>
        <th>Guest Name</th>
        <th>Guest Type</th>
        <th>Agent</th>
        <th>Due Amount</th>
        <th>Officer</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
</thead> 
<tbody>
    @foreach($reservation as $key=>$res)
    <tr>
      <td> {{env('RES_CODE')}}{{$res->id}} </td>
      <td> <a href="" title="{{\TaskHelper::ShowRoom($res->id)}}"><strong>{{$res->room_num}}</strong></a> {{$res->room->room_type->type_code}}</td>
      <td> <strong>{{\TaskHelper::ReservationTotalRoom($res->id)}}</strong> ({{$res->occupancy}})</td>
      <td>
       @if($res->check_in)
        {{ date('dS M y', strtotime($res->check_in)) }}
        @else
        --
      @endif
      </td>
      <td>
        @if($res->check_out)
          {{ date('dS M y', strtotime($res->check_out)) }}
          @else
          --
        @endif
      </td>
      @if($res->guest)
      <td>
        <strong>
            <a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest->full_name}}</a>
        </strong>
      </td>
      @else
       <td>
        <strong><a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest_name}}</a></strong></td>
      @endif
      <td>{{$guest_type[$res->guest_type]}}</td>
      <td> {{$res->agent->name}} </td>
      <td> {{env('CUR_CODE')}} {{\TaskHelper::UnsetteledFolio($res->id)}}</td>
      <td> {{ ucfirst(trans($res->user->username)) }}</td>
      <td style="background-color:  {{$res->reservation_state->status_color}}; ">
        {{$res->reservation_state->status_name}}
      </td>
        <td>
          <a href="/admin/hotel/reservation/generateGRCardPDF/{{$res->id}}" title="generateGRCardPDF">
      <i class="fa fa-download"></i></a>
      &nbsp;
        <a href="{{route('admin.hotel.reservation.generateGRCardprint',$res->id)}}" title="print">
      <i class="fa fa-print"></i></a>
      @if($res->reservation_status == '3')
      &nbsp;
        <a href="/admin/reservation/checkouterror/{{$res->id}}" title="Check Out">
      <i class="fa fa-sign-out"></i></a>
      @endif
      @if($res->isDeletable())
        <a href="{!! route('admin.hotel.reservation.confirm-delete', $res->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
      @endif
    </tr>
    @endforeach
</tbody>
</table>
</div>
@endsection

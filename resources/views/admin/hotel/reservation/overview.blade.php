@extends('layouts.master')
@section('content')
<style type="text/css">
    .legend {
        list-style: none;
    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
        margin: 2px;
    }

</style>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Room View
        <small>overviews</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <form method="get" action="{{route('admin.hotel.reservation-overviews')}}">
            <div class="row">
                <div class="form-group">
                    <label for="user_id" class="col-sm-3 control-label">Select date
                        <span class="required">*</span>
                    </label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control occupied_date_from input-sm" value="{{\Request::get('specific_date')}}" name="specific_date" id="date1" style="z-index: 100">
                    </div>

                    <button class="btn btn-primary" id="btn-submit-edit" type="submit" name="submit_method" value="reserv">View</button>
                </div>
            </div>
        </form>
        <br>
        <div class='row'>
            <div class='col-md-12'>
                @foreach($rooms as $k => $res)

                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div style="background-color: {{$room_status[$k][1]}}" class="small-box">
                        <div class="inner">
                            <h3>{{$res->room_number}}</h3>
                            <span>
                                {{ mb_substr($room_status[$k][0],0,15)}}
                            </span>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                            {{$res->room_name}}
                        </a>
                    </div>
                </div>

                @endforeach<br>
            </div>
        </div>


        <br>
        <label>Index</label>
        <ul class="legend">
            @foreach($res_state as $rs)
            <li><span style="background-color: {{$rs->status_color}}"></span>{{$rs->status_name}}</li>
            @endforeach
        </ul>
    </div>
</div>
@endsection
@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $('#date1').datepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            dateFormat: 'yy-m-d'
            , sideBySide: true
            , beforeShow: function() {
                setTimeout(function() {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });
    });

</script>
@endsection

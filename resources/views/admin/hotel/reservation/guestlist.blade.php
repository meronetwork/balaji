@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Guest Lists
        <small>Lists Of All the Guests</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <form method="get" action="{{route('admin.hotel.guestlist')}}">
            <div class="row">
                <div class="form-group">
                    <label for="user_id" class="col-sm-3 control-label">Select date
                        <span class="required">*</span>
                    </label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control occupied_date_from input-sm" value="{{\Request::get('specific_date')}}" name="specific_date" id="date1" style="z-index: 100">
                    </div>

                    <button class="btn btn-primary" id="btn-submit-edit" type="submit" name="submit_method" value="reserv">View</button>
                </div>
            </div>
        </form>
        <br>


        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
                <tr>
                    <th>Res ID</th>
                    <th>Room No</th>
                    <th>Guest Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
             $i = 0;
         ?>
                @foreach($guests as $key=>$res)
                <tr>
                    <?php
                  $i=$i+1;
             ?>
                    <td><a href="/admin/hotel/reservation-edit/{{$res->res_id}}">{{env('RES_CODE')}}{{$res->res_id}}</a></td>
                    <td> {{$res->room_num}} </td>
                    <td> {{ ucfirst($res->guest_name)}} </td>
                </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td>Total Guest:</td>
                    <td><strong>{{$i}}</strong></td>
                </tr>
            </tbody>
        </table>
    </div>
    @endsection

    @section('body_bottom')
    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#date1').datepicker({
                //inline: true,
                //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d'
                , sideBySide: true
                , beforeShow: function() {
                    setTimeout(function() {
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                }
            });
        });

    </script>
    @endsection

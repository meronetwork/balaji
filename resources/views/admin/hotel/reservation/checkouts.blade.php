@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {!! $page_title ?? "Page Title" !!}
        <small>{!! $page_description ?? "Page description" !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>

                @foreach($reservation as $res)
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$res->room_num}}</h3>
                            @if($res->guest)
                            <span>{{$res->guest->full_name}}</span>
                            @else
                            <span>{{$res->guest_name}}</span>
                            @endif
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="/admin/hotel/reservation-edit/{{$res->id}}" class="small-box-footer">
                            {{ucfirst(trans($res->room->room_type->room_name))}}
                        </a>
                    </div>
                </div>
                @endforeach









            </div>





        </div>




    </div>

    @endsection

@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        History
        <small>Guest History</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">

        <div class="wrap" style="margin-top:5px;">
            <div class="filter form-inline" style="margin:0 30px 0 0;">
                {!! Form::text('check_in_date', \Request::get('check_in_date'), ['style' => 'width:120px;', 'class' => 'form-control', 'id'=>'check_in_date', 'placeholder'=>'Check IN Date']) !!}&nbsp;&nbsp;
                <!-- <label for="end_date" style="float:left; padding-top:7px;">End Date: </label> -->
                {!! Form::text('check_out_date', \Request::get('check_out_date'), ['style' => 'width:120px; display:inline-block;', 'class' => 'form-control', 'id'=>'check_out_date', 'placeholder'=>'Check Out Date']) !!}&nbsp;&nbsp;

                {!! Form::select('agent_id', ['' => 'Select Agent'] + $agents, \Request::get('agent_id'), ['id'=>'filter-agent', 'class'=>'form-control', 'style'=>'width:150px; display:inline-block;']) !!}&nbsp;&nbsp;

                {!! Form::select('guest_type', [''=>'Select Guest Type','TOU'=>'Tourist','BUS'=>'Business Travellers','FAM'=>'Families','STA'=>'Hotel Staffs','DEl'=>'Delegates','VIP'=>'VIP','COR'=>'Corporate','GOV'=>'Government','FIT'=>"FIT",'DIP'=>"Diplomats",'COM'=>"Complementry"] , \Request::get('guest_type'), ['id'=>'filter-guest', 'class'=>'form-control', 'style'=>'width:150px; display:inline-block;']) !!}&nbsp;&nbsp;

                {!! Form::select('user_id', ['' => 'Select Officer'] + $users, \Request::get('user_id'), ['id'=>'filter-user', 'class'=>'form-control', 'style'=>'width:150px; display:inline-block;']) !!}
                &nbsp;&nbsp;

                <span class="btn btn-primary" id="btn-submit-filter">
                    <i class="fa fa-list"></i> Filter
                </span>
                <span class="btn btn-danger" id="btn-filter-clear">
                    <i class="fa fa-close"></i> Clear
                </span>
            </div>
        </div>

        <div class='row'>
            <div class='col-md-12'>


            </div>
        </div>
        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
                <tr>
                    <th>Res ID</th>
                    <th>Room #</th>
                    <th>Rooms</th>
                    <th>Check in</th>
                    <th>Check out</th>
                    <th>Guest Name</th>
                    <th>Guest Type</th>
                    <th>Agent</th>
                    <th>Due Amount</th>
                    <th>Officer</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reservation as $key=>$res)
                <tr>
                    <td> {{env('RES_CODE')}}{{$res->id}} </td>
                    <td> <a href="" title="{{\TaskHelper::ShowRoom($res->id)}}"><strong>{{$res->room_num}}</strong></a> {{$res->room->room_type->type_code??''}}</td>
                    <td> <strong>{{\TaskHelper::ReservationTotalRoom($res->id)}}</strong> ({{$res->occupancy}})</td>
                    <td>
                        @if($res->check_in)
                        {{ date('dS M y', strtotime($res->check_in)) }}
                        @else
                        --
                        @endif
                    </td>
                    <td>@if($res->check_out)
                        {{ date('dS M y', strtotime($res->check_out)) }}
                        @else
                        --
                        @endif</td>
                    @if($res->guest)
                    <td>
                        <strong>
                            <a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest->full_name}}</a>
                        </strong>
                    </td>
                    @else
                    <td>
                        <strong><a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest_name}}</a></strong>
                        @endif
                    <td>{{$guest_type[$res->guest_type]}}</td>
                    <td> {{$res->agent->name??''}} </td>
                    <td> {{env('CUR_CODE')}} {{\TaskHelper::ReservationDueAmount($res->id)}}</td>
                    <td> {{ ucfirst(trans($res->user->username)) }}</td>
                    <td style="background-color:  {{$res->reservation_state->status_color}}; ">
                        {{$res->reservation_state->status_name}}
                    </td>
                    <td>
                        <a href="/admin/hotel/reservation/generateGRCardPDF/{{$res->id}}" title="generateGRCardPDF">
                            <i class="fa fa-download"></i></a>
                        &nbsp;
                        <a href="{{route('admin.hotel.reservation.generateGRCardprint',$res->id)}}" title="print">
                            <i class="fa fa-print"></i></a>
                        @if($res->reservation_status == '3')
                        &nbsp;
                        <a href="/admin/reservation/checkouterror/{{$res->id}}" title="Check Out">
                            <i class="fa fa-sign-out"></i></a>
                        @endif


                        {{-- @if($res->isDeletable())
                        <a href="{!! route('admin.hotel.reservation.confirm-delete', $res->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                        @endif --}}
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $reservation->render() !!}
    </div>

    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
    <script>
        $(function() {
            $('#check_in_date').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD'
                , sideBySide: true
            });

            $('#check_out_date').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD'
                , sideBySide: true
            });
        });

    </script>

    <script type="text/javascript">
        $("#btn-submit-filter").on("click", function() {

            user_id = $("#filter-user").val();
            guest_type = $("#filter-guest").val();
            agent_id = $("#filter-agent").val();
            check_in_date = $("#check_in_date").val();
            check_out_date = $("#check_out_date").val();


            window.location.href = "{!! url('/') !!}/admin/hotel/reservation/guesthistory?agent_id=" + agent_id + "&user_id=" + user_id + "&check_in_date=" + check_in_date + "&check_out_date=" + check_out_date + "&guest_type=" + guest_type;
        });

        $("#btn-filter-clear").on("click", function() {
            type = $("#lead_type").val();
            window.location.href = "{!! url('/') !!}/admin/hotel/reservation/guesthistory";
        });

    </script>
    @endsection

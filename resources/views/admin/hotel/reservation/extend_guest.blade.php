@extends('layouts.master')
@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<style>
  tr td { text-align:left !important; }
</style>

    <div class='row'>
        <div class='col-md-6'> 
          <!-- Box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="">
                            <p>  Reservation guest list #{{ Request::segment(4) }}</p>
                            <table class="table table-hover table-bordered" id="leads-table">
                                <thead>
                                    <tr>
                                        <th>Guest Name</th>
                                        <th>Doc Type</th>
                                        <th>Doc num</th>
                                        <th>Guest type</th>
                                    </tr>
                                    @if($history)
                                    @foreach($history as $k => $v)
                                    <tr>
                                        <td>{{ $v->full_name }}</td>
                                        <td>{{ $v->doc_type }}</td>
                                        <td>{{ $v->doc_num }}</td>
                                        <td>{{ $v->guests_type }}</td>
                                    </tr>
                                    @endforeach
                                    @endif

                                </thead>
                            </table>

                          <form method="post" action="{{route('admin.hotel.extend-guest',$res_id)}}">
                            {{csrF_field()}}
                       
                            <div class="row">
                    <div class="col-md-6" id="full_name" >
              <div class="form-group" >  
                <label class="control-label col-md-6">Guest Name</label>

                    <div class="input-group ">
                        <input type="text" name="full_name" placeholder="guest name.."  value="{{ old('guest_name')}}" class="form-control input-sm"  >
                        <div class="input-group-addon">
                            <a href="#"><i class="fa   fa-user"></i></a>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              Guest Type
                              </label>
                              <div class="col-md-6">
                                <select name="guests_type" class="form-control input-sm">
                                    <option value="TOU">Tourist</option>
                                    <option value="BUS">Business Travellers</option>
                                    <option value="FAM">Families</option>
                                    <option value="STA">Hotel Staffs</option>
                                    <option value="DEL">Delegates</option>
                                    <option value="VIP">VIP</option>
                                    <option value="COR">Corporate</option>
                                    <option value="GOV">Government</option>
                                </select>

                              </div>

                            </div>
                        </div>


                            </div>


                                                  <div class="row">
                    <div class="col-md-6" id="guest_name" >
              <div class="form-group" >  
                <label class="control-label col-md-6">Doc. type</label>

                    <div class="input-group ">
                        <input type="text" name="doc_type" placeholder="doc type.."   class="form-control input-sm"  >
                        <div class="input-group-addon">
                            <a href="#"><i class="fa    fa-file-archive-o"></i></a>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              Doc. num
                              </label>
                              <div class="col-md-6">
                                 <div class="input-group ">
                        <input type="text" name="doc_num" placeholder="doc num.."   class="form-control input-sm"  >
                      
                    </div>

                              </div>

                            </div>
                        </div>


                            </div>

        <button class="btn btn-primary" type="submit">Add guest</button>

        <a href="/admin/folio/{{$res_id}}/create" class="btn btn-primary" target="_blank"> Create Folio </a>

<div class="row">
                        <div class="col-md-3">
                    
                     </div>
                       
                    
                </div>
      
            </form>
            </div>


                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection


@section('body_bottom')
  <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

 <script type="text/javascript">
    $(function() {
            $('#package_date').datepicker({
                dateFormat: 'yy-m-d',
                sideBySide: true,
                minDate : 0,
                maxDate:  new Date('{{$check_out}}')
                });
        });
    $(document).ready(function(){
      $('.searchable').select2();
    });

</script>
@endsection


<!DOCTYPE html><html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css">
@font-face {
  font-family: Arial;
}
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  margin: 0 auto;
  color: #555555;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 12px;
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}
table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}
table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}
tr td{
  padding-top: 5px;
}
table th {
  white-space: nowrap;
  font-weight: normal;
}
table td {
  text-align: left;
}
table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}
table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}
table .desc {
  text-align: left;
}
table .unit {
  background: #DDDDDD;
}
table .qty {
}
table .total {
  background: #57B223;
  color: #FFFFFF;
}
table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}
table tbody tr:last-child td {
  border: none;
}
table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap;
  border-top: 1px solid #AAAAAA;
}
table tfoot tr:first-child td {
  border-top: none;
}
table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223;
  font-weight: bold;
}
table tfoot tr td:first-child {
  border: none;
}
#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}
#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;
}
#notices .notice {
  font-size: 1.2em;
}
footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
    </style>
  </head><body onload="window.print();">
    <header class="clearfix">
        <table>
            <tr>
        <td width="50%" style="float:left">
          <div id="logo">
               <h4 class="name">{{ env('APP_COMPANY') }} </h4>
              <div>{{ env('APP_ADDRESS1') }}</div>
              <div>{{ env('APP_ADDRESS2') }}</div>
              <div>Seller's PAN: {{ env('TPID') }}</div>
              <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>
          </div>
        </td>
        <td width="50%" style="text-align: right">
        <div  id="company">
            <img style="max-width: 150px" src="{{ '/org/'.$organization->logo }}">
        </div>
        </td></tr>
      </table>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <table>
   <tr>
    <td>
    GUEST NAME: <u> @if($ord->guest_id) <strong>{{$ord->guest->full_name}} </strong>@else <strong>{{$ord->guest_name}}</strong> @endif</u>
  </td>
  <td>
     CHECK IN DATE: @if($ord->check_in)<u><strong>{{ date('dS M y', strtotime($ord->check_in)) }}</strong> @else  _________________ @endif
  </td> </u>&nbsp;
  </td>
</tr>
<tr>
  <td>
    GENDER: <u><strong>{{$ord->gender}}</strong></u>
  </td>
  <td>
      CHECK OUT DATE: @if($ord->check_out)<u><strong>{{ date('dS M y', strtotime($ord->check_out)) }}</strong></u> @else  _________________ @endif
  </td>
  </tr>
<tr>
  <td>
    EMAIL: @if($ord->email)<u style="min-width: 20px"><strong>{{$ord->email}}</strong></u>@else _________________ @endif
  </td>
  <td> AGENT: @if($ord->agent->name)<u><strong> {{ $ord->agent->name ?? ''}} </strong></u>@else _________________ @endif</td>
</tr>
<tr>
  <td>
      MOBILE NO: @if($ord->mobile)<u><strong>{{$ord->mobile}}</strong></u> @else _________________ @endif
  </td>
  <td>
      PHONE NO: @if($ord->phone)<strong><u>{{$ord->phone}}</u></strong> @else _________________ @endif
  </td>
  </tr>
  <tr >
<td >
  MODE OF PAYMENT
    <br>
    CASH_____<br>
    CREDIT CARD_____
</td>
<td>
 SOURCE OF PAYMENT
  <br>
    DIRECT______<br>
    CREDIT______
  </td>
  </tr>
  <tr>
    <td>
      NATIONALITY: @if($ord->nationality)<u><strong>{{$ord->nationality}}</strong></u> @else _________________ @endif
    </td>
    <td>
      COMPANY NAME: @if($ord->client->name)<u><strong>{{$ord->client->name}}</strong></u> @else _________________ @endif
    </td>
     </tr>
     <tr>
    <td>
      DOC.TYPE: @if($ord->doc_type_extra)<u><strong>{{$ord->doc_type_extra}}</strong></u>@else _________________ @endif
    </td>
    <td>
      DOC.NUMBER:@if($ord->doc_num_extra)<u><strong>{{$ord->doc_num_extra}}</strong></u> @else _________________ @endif
    </td>
  </tr>
  <tr>
      <td>ARRIVAL FLIGHT DETAIL:@if($ord->flight_no)<u><strong>{{$ord->flight_no}}</strong></u>@else _________________ @endif</td>
      <td>
        ARRIVAL FLIGHT TIME: @if($ord->arrival_flight_time)<u><strong>{{$ord->arrival_flight_time}}</strong></u> @else _________________ @endif
      </td>
  </tr>
  <tr>
    <td>
  ARRIVAL TIME:&nbsp;@if($ord->arrival_time)<u> <strong>{{ date('dS M y', strtotime($ord->arrival_time)) }} </strong></u> @else _________________ @endif
    </td>
    <td>
      OCCUPANCY: @if($ord->occupancy)<u><strong>{{$ord->occupancy}}</strong></u> @else _________________ @endif
    </td>
  </tr>
  <tr>
    <td>
    ROOM NO :: <u><strong>{{$ord->room_num}}&nbsp;{{$ord->room->room_type->type_code}}</strong></u>
</td>
<td>
  RATE PLAN: @if($ord->rateplan->package_name)<u><strong>{{$ord->rateplan->package_name}} ({{$ord->rateplan->rate}})</strong></u>  @else _________________ @endif
  </td>
  </tr>
  <tr>
    <td>Res.Type: @if($ord->type->name)<u> {{$ord->type->name}}</u> @else _________________ @endif</td>
  </tr>
  <tr>
    <td> _________________<br>GUEST SIGNATURE</div>
    </td>
    <td>
      <u> <strong>{{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</strong></u>  <br>&nbsp;&nbsp;&nbsp;CASHIER</div>
    </td>
  </tr>
    </table>
    REMARK: <strong>{{$ord->remarks}}</strong>
    <!-- /.row -->
      <p>Thank you for choosing {{ env('APP_COMPANY') }}. If you find errors or desire certain changes, please contact us.</p>
      <br>
    </main>
    <footer>
     GR Card was created on HOTELSUITE.
    </footer></body></html>
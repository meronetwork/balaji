@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        CheckOut Details
        <small>List of Folios and Payments
            Paid Amount {{ \TaskHelper::getFolioPaymentAmount($reservation_id) }}

        </small>
    </h1>
    <i class="fa fa-info"></i> Balance Amount should be made 0 before you can checkout. Scroll down and click checkout once bill is settled
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}


</section>

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->

        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">

                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr class="bg-teal">
                                <th>id</th>
                                <th>Folio Owner</th>
                                <th>Folio Type</th>

                                <th>Total</th>
                                <th>Tools</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $folio_total_amount = 0;
                                $total_balance_amount= 0;

                            ?>
                            @if(isset($folios) && !empty($folios))
                            @foreach($folios as $o)
                            <tr>
                                <td>{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                <td>{!! $o->user->username !!}</td>
                                <td>{!! $o->folio_type !!}</td>

                                @if($o->folio_type == 'master')
                                    @php
                                        $deposit_amount = \App\Models\FolioPayment::where('reservation_id',$o->reservation_id)
                                            ->where('is_deposit',1)->sum('amount');

                                        $o->total_amount -=  $deposit_amount;
                                    @endphp
                                @endif

                                    <td>{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                    <td>
                                        <?php
                                         $not_posted_ledgers = \App\Models\FolioDetail::where('folio_id',$o->id)->where('posted_to_ledger',0)->get();
                                         $not_posted = \App\Models\FolioDetail::where('folio_id',$o->id)->where('is_posted',0)->get();

                                        ?>
                                        @if(count($not_posted)>0)
                                        <form method="post" action="/admin/hotel/postbills/folio/{{$o->id}}">
                                            {{ csrf_field() }}
                                            <button class="btn btn-foursquare btn-xs" type="submit">POST to AR</button>
                                        </form>
                                        @else
                                        @endif

                                        @if(count($not_posted_ledgers)>0 && count($not_posted)==0)
                                        <a class="btn btn-primary bg-olive btn-xs" href="/admin/payment/folio/{{$o->id}}/create/" data-toggle="modal" data-target="#modal_dialog"><i class="fa fa-credit-card"></i> Pay Now</a>
                                        @endif

                                        {{-- <a target="_blank" class="btn bg-teal btn-xs" href="/admin/reservation/folio/{{$o->reservation_id}}/index"> Folio <i class="fa fa-external-link"></i> </a> --}}

                                        @if(!$o->is_final_invoice && count($not_posted_ledgers)==0 && count($not_posted)==0)

                                        {{-- <a href="/admin/hotel/reservation/folio/{{$o->id}}/generate/invoice" class="btn btn-foursquare bg-navy btn-xs" >Generate Invoice</a> --}}
                                        <a href="/admin/hotel/reservation/folio/{{$o->id}}/generate/final/invoice" class="btn btn-foursquare btn-xs">Final Invoice</a>
                                        @endif

                                    </td>

                                    <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>
                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td colspan="2">
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    <b>
                                {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount,2) }}
                                </b>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->

        </div><!-- /.box -->

    </div><!-- /.col -->
</div><!-- /.row -->

<?php if( count($orders) > 0 ){ ?>

<div class='row'>
    <div class='col-md-12'>
        {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">
                    <div class="col-md-12">
                        <h4>POS Details</h4>
                    </div>
                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr>
                                <th style="text-align: center">
                                    <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                                        <i class="fa fa-check-square-o"></i>
                                    </a>
                                </th>
                                <th>id</th>
                                <th>Reservation</th>
                                <th>Outlet</th>
                                <th>Status</th>
                                <th>Paid</th>
                                <th>Balance</th>
                                <th>Pay Status</th>
                                <th>Total</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                        $pos_total_amount = 0;
                                   ?>
                            @if(isset($orders) && !empty($orders))
                            @foreach($orders as $o)
                            <tr>
                                <td align="center">{!! Form::checkbox('chkClient[]', $o->id); !!}</td>
                                <td><a href="/admin/orders/{{$o->id}}">PORD{!! $o->id !!}</a><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>
                                <td><a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{ $o->name }}</small> </td>
                                <td class="bg-warning"> {{ $o->outlet->name}} </td>
                                <td>
                                    {{$o->status}}
                                </td>
                                <?php
                                              $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                 ?>
                                <td>{!! number_format($paid_amount,2) !!}</td>
                                <td>{!! number_format($o->total_amount-$paid_amount,2) !!}</td>
                                <?php
                                              $pos_balance = $o->total_amount-$paid_amount;
                                              $total_balance_amount = $total_balance_amount + $pos_balance;
                                           ?>
                                @if($o->payment_status == 'Pending')
                                <td><span class="label label-warning">Pending</span></td>
                                @elseif($o->payment_status == 'Partial')
                                <td><span class="label label-info">Partial</span></td>
                                @elseif($o->payment_status == 'Paid')
                                <td><span class="label label-success">Paid</span></td>
                                @else
                                <td><span class="label label-warning">Pending</span></td>
                                @endif
                                <td>{!! number_format($o->total_amount,2) !!}</td>

                                <?php
                                              $pos_total_amount = $pos_total_amount + $o->total_amount;
                                            ?>
                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td colspan="7">
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    {{env('APP_CURRENCY')}} {{ number_format($pos_total_amount,2) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- table-responsive -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        {!! Form::close() !!}
    </div><!-- /.col -->
</div><!-- /.row -->

<?php } ?>




</br>


<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
        <div class="box box-primary">

            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="orders-table">
                        <thead>
                            <tr class="bg-info">
                                <th>Invoice #</th>
                                <th>Bill Date</th>
                                <th>Invoice Owner</th>
                                <th>Invoice Type</th>
                                <th>Tools</th>
                                <th>Total</th>
                                <th>Print Invoice</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $folio_total_amount = 0;
                            ?>
                            @if(isset($invoices) && !empty($invoices))
                            @foreach($invoices as $o)
                            <tr>
                                <td>{{$o->outlet->outlet_code}}{!! $o->bill_no !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>
                                <td>{{ $o->bill_date}}</td>
                                <td>{!! $o->user->username !!}</td>
                                <td>{!! $o->order_type !!}</td>
                                <td>History</td>
                                <td>{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>
                                <td>
                                    <a href="/admin/hotel/invoice/type/select/{{$o->id}}" class="btn btn-foursquare btn-xs" data-toggle="modal" data-target="#modal_dialog">Print Invoice</a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td colspan="4">
                                </td>
                                <td>
                                    Total Amount:
                                </td>
                                <td>
                                    {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount,2) }}
                                </td>
                            </tr>

                            <?php
                                  $deposit_amount = \App\Models\FolioPayment::where('reservation_id',$id??null)->where('is_deposit',1)->sum('amount');
                            ?>
                            <tr>
                                <td colspan="4">
                                </td>
                                <td>
                                    Deposit Amount:
                                </td>
                                <td>
                                    {{env('APP_CURRENCY')}} {{ number_format($deposit_amount,2) }}
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                </td>
                                <td>
                                    Remaining Amount:
                                </td>
                                <td>
                                    {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount-$deposit_amount,2) }}
                                </td>
                            </tr>

                        </tbody>
                    </table>

                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->

        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
        {!! Form::close() !!}
    </div><!-- /.col -->
</div><!-- /.row -->




<?php

  $folios = \App\Models\Folio::where('reservation_id',\Request::segment('4'))->where('is_bill_active',1)->get();

    foreach($folios as $folio){

        $folio_detail = \App\Models\FolioDetail::where('folio_id',$folio->id)->where('posted_to_ledger',0)->get();
    }

    $orders = \App\Models\Orders::select('fin_orders.*')
                           ->leftjoin('fin_orders_meta','fin_orders.id','=','fin_orders_meta.order_id')
                           ->where('fin_orders.reservation_id',\Request::segment('4'))
                           ->where('fin_orders_meta.settlement',0)
                           ->where('fin_orders_meta.is_bill_active',1)
                           ->get();
    $notFinalInvoice = $folios->where('is_final_invoice','0');
    $reservation=\App\Models\Reservation::find(\Request::segment('4'));

?>


<div class='row'>
    <div class='col-md-12'>
        @if(count($notFinalInvoice) > 0|| $reservation->reservation_status==6)

        <div class="col-md-12">
            <a href="#" class="btn btn-primary" disabled>Check Out</a>
        </div>

        @else

        <div class="col-md-12">
            <a href="{{route('admin.hotel.checkoutfromedit',\Request::segment(4))}}" class="btn btn-primary" data-toggle="modal" data-target="#modal_dialog">Check Out</a>
        </div>
        @endif


    </div><!-- /.col -->
</div><!-- /.row -->


<script type="text/javascript">
    function openwindowsettle(id) {
        var win = window.open(`/admin/payment/folio/${id}/create/modal`, '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
    }

    $(document).on('hidden.bs.modal', '#modal_dialog', function(e) {
        $('#modal_dialog .modal-content').html('');
    });

</script>



@endsection

<!DOCTYPE html><html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css">
@font-face {
  font-family: Arial;
}
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  margin: 0 auto;
  color: #555555;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 12px;
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}
table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}
table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}
tr td{
  padding-top: 5px;
}
table th {
  white-space: nowrap;
  font-weight: normal;
}
table td {
  text-align: left;
}
table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}
table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}
table .desc {
  text-align: left;
}
table .unit {
  background: #DDDDDD;
}
table .qty {
}
table .total {
  background: #57B223;
  color: #FFFFFF;
}
table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}
table tbody tr:last-child td {
  border: none;
}
table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap;
  border-top: 1px solid #AAAAAA;
}
table tfoot tr:first-child td {
  border-top: none;
}
table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223;
  font-weight: bold;
}
table tfoot tr td:first-child {
  border: none;
}
#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}
#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;
}
#notices .notice {
  font-size: 1.2em;
}
footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
    </style>
  </head><body onload="window.print();">
    <header class="clearfix">
        <table>
            <tr>
        <td width="50%" style="float:left">
          <div id="logo">
               <div style="font-size: 20px">{{ env('APP_COMPANY') }} </div>
               <div style="font-size: 17px">Registration Form</div>
              <div>Resv # {{ $ord->id }}</div>
              <div>{{ date('Y M d h:i') }}</div>
          </div>
        </td>
        <td width="50%" style="text-align: right">
        <div  id="company">
            <img style="max-width: 150px" src="{{public_path()}}/{{ '/org/'.$organization->logo }}">
        </div>
        </td></tr>
      </table>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <table>
   <tr>
    <td>
      GUEST NAME: @if($ord->guest_id) <strong>{{$ord->guest->full_name}} </strong>@else <strong>{{$ord->guest_name}} </strong>@endif
  </td>
  <td>
     CHECK IN DATE: @if($ord->check_in)<strong>{{ date('dS M y', strtotime($ord->check_in)) }} </strong>@else  _________________ @endif
  </td> &nbsp;
  </td>
</tr>
<tr>
  <td>
    GENDER: <u>{{$ord->gender}}</u>
  </td>
  <td>
      CHECK OUT DATE: @if($ord->check_out)<strong>{{ date('dS M y', strtotime($ord->check_out)) }}</strong> @else  _________________ @endif
  </td>
  </tr>
<tr>
  <td>
    EMAIL: @if($ord->email)<strong>{{$ord->email}}</strong>@else _________________ @endif
  </td>
  <td> AGENT: @if($ord->agent->name)<strong>{{$ord->agent->name}}</strong>@else _________________ @endif</td>
</tr>
<tr>
  <td>
      MOBILE NO: @if($ord->mobile)<strong>{{$ord->mobile}}</strong> @else _________________ @endif
  </td>
  <td>
      PHONE NO: @if($ord->phone)<strong>{{$ord->phone}}</strong> @else _________________ @endif
  </td>
  </tr>
  <tr >
<td >
  MODE OF PAYMENT
    <br>
    CASH_____<br>
    CREDIT CARD_____
</td>
<td>
 SOURCE OF PAYMENT
  <br>
    DIRECT______<br>
    CREDIT______
  </td>
  </tr>
  <tr>
    <td>
      NATIONALITY: @if($ord->nationality)<strong>{{$ord->nationality}}</strong> @else _________________ @endif
    </td>
    <td>
      COMPANY NAME: @if($ord->client->name)<strong>{{$ord->client->name}}</strong> @else _________________ @endif
    </td>
     </tr>
     <tr>
    <td>
      DOC.TYPE: @if($ord->doc_type_extra)<strong>{{$ord->doc_type_extra}}</strong>@else _________________ @endif
    </td>
    <td>
      DOC.NUMBER: @if($ord->doc_num_extra)<strong>{{$ord->doc_num_extra}}</strong> @else _________________ @endif
    </td>
  </tr>
  <tr>
      <td>ARRIVAL FLIGHT DETAIL: @if($ord->flight_no)<strong>{{$ord->flight_no}}</strong>@else _________________ @endif</td>
      <td>
        ARRIVAL FLIGHT TIME: @if($ord->arrival_flight_time) <strong>{{$ord->arrival_flight_time}}</strong> @else _________________ @endif
      </td>
  </tr>
  <tr>
    <td>
  ARRIVAL TIME: &nbsp;@if($ord->arrival_time) <strong>{{ date('dS M y', strtotime($ord->arrival_time)) }} </strong> @else _________________ @endif
    </td>
    <td>
      OCCUPANCY: @if($ord->occupancy)<strong>{{$ord->occupancy}}</strong> @else _________________ @endif
    </td>
  </tr>
  <tr>
    <td>
    ROOM NO.: <strong>{{$ord->room_num}}&nbsp;{{$ord->room->room_type->type_code}}</strong>
</td>
<td>
  RATE PLAN: @if($ord->rateplan->package_name)<strong>{{$ord->rateplan->package_name}}</strong>  @else _________________ @endif
  </td>
  </tr>
  <tr>
    <td>Res.Type: @if($ord->type->name)<strong>{{$ord->type->name}}</strong> @else _________________ @endif</td>
  </tr>
  <tr>
    <td> </td>
    <td>
        <u><strong> {{\Auth::user()->first_name}} {{\Auth::user()->last_name}} </strong></u> <br>&nbsp;&nbsp;&nbsp;CASHIER</div>
    </td>
  </tr>
    </table>

    <br/><br/>

    _________________<br>GUEST SIGNATURE

<br/><br/>

  REMARK: <strong>{{$ord->remarks}}</strong>

<br/> <br/>
<strong>Cancellation and No Show:</strong><br/>
Any cancellation will be charged for the first night’s stay.<br/>
Any decrease in length of stay within 3 days before arrival will be 100% charged. Any guest room reservation confirmed and guaranteed through the Reservation Form, but not claimed on the day of arrival (no show), will be cancelled and will only be re-entered into the reservation system subject to space availability. In this case all nights will be charged the calculated room rate plus taxes.

    <!-- /.row -->
      <p>Thank you for choosing {{ env('APP_COMPANY') }}. If you find errors or desire certain changes, please contact us.</p>
      <br>
    </main>
    <footer>
     GR Card was created on HOTELSUITE.  <a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a>
    </footer></body></html>
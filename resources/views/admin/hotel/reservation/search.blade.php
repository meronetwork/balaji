@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
          <h1>
           {{ $page_title or "page_title"}}
              <small>{{ $page_description or "page_description"}}</small>
          </h1>
          {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>
   <div class="box box-primary">
    <div class="box-header with-border">

       
        <hr/>
        
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>Res ID</th>
        <th>Room #</th>
        <th>Rooms</th>
        <th>Check in</th>
        <th>Check out</th>
        <th>Guest Name</th>
        <th>Guest Type</th>
        <th>Group</th>
        <th>Agent</th>
        <th>Due Amount</th>
        <th>Officer</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>  
    @foreach($reservation as $key=>$res)
    <tr @if($res->source_type == 'Online') class="bg-info" @endif>
      <td> {{env('RES_CODE')}}{{$res->id}} </td>
      <td> <a href="" title="{{\TaskHelper::ShowRoom($res->id)}}"><strong>{{$res->room_num}}</strong></a> {{$res->room->room_type->type_code}}</td>
          <?php 
             $total_person = \App\Models\ReservationRoomGuest::where('res_id',$res->id)->count();
          ?>
      <td> <strong>{{\TaskHelper::ReservationTotalRoom($res->id)}}</strong> ({{$total_person}})-{{$res->occupancy}} </td>
      <td>
       @if($res->check_in)
        {{ date('dS M y', strtotime($res->check_in)) }} 
       @else 
        --
      @endif
      </td>
      <td>@if($res->check_out)
        {{ date('dS M y', strtotime($res->check_out)) }}  
        @else
        --
      @endif</td>
      @if($res->guest)
      <td title="{{ $res->remarks }}"> 
        <strong>
       <a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ $res->guest->first_name}} {{ $res->guest->last_name}}</a>
          </strong>
       </td>
      @else
       <td title="{{ $res->remarks }}">
        <strong><a href="/admin/hotel/reservation-edit/{{$res->id}}">{{ ucwords($res->guest_name)}}</a></strong>
      @endif
      <td><label class="label {{$guest_type[$res->guest_type][1]}}">{{$guest_type[$res->guest_type][0]}}</label></td>
      <td> {{$res->parent_res}} </td>
      <td> {{$res->agent->name}} </td>
      <td> {{env('CUR_CODE')}} {{\TaskHelper::ReservationDueAmount($res->id)}}</td>
      <td> {{ ucfirst(trans($res->user->username)) }}</td>
      <td style="background-color:  {{$res->reservation_state->status_color}}; ">
        {{$res->reservation_state->status_name}}
      </td>
        <td>
          <a href="/admin/hotel/reservation/generateGRCardPDF/{{$res->id}}" title="generateGRCardPDF">
      <i class="fa fa-download"></i></a>
      &nbsp;
        <a href="{{route('admin.hotel.reservation.generateGRCardprint',$res->id)}}" title="print">
      <i class="fa fa-print"></i></a>
      @if($res->reservation_status == '3')
      &nbsp;
        <a href="/admin/reservation/checkouterror/{{$res->id}}" title="Check Out">
      <i class="fa fa-sign-out"></i></a>
      @endif


      @if($res->isDeletable())
        <a href="{!! route('admin.hotel.reservation.confirm-delete', $res->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
      @endif
    </tr>
    @endforeach
</tbody>
</table>
{!! $reservation->render() !!}  
</div>

<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script>
$(function() {
  $('#check_in_date').datetimepicker({
      //inline: true,
      format: 'YYYY-MM-DD',
      sideBySide: true
    });

  $('#check_out_date').datetimepicker({
      //inline: true,
      format: 'YYYY-MM-DD',
      sideBySide: true
    });
});
</script>

<script type="text/javascript">
  
  $("#btn-submit-filter").on("click", function () {
  
      user_id = $("#filter-user").val();
      guest_type = $("#filter-guest").val();
      agent_id = $("#filter-agent").val();
      check_in_date = $("#check_in_date").val();
      check_out_date = $("#check_out_date").val();  

     
      window.location.href = "{!! url() !!}/admin/hotel/reservation-index?agent_id="+agent_id+"&user_id="+user_id+"&check_in_date="+check_in_date+"&check_out_date="+check_out_date+"&guest_type="+guest_type;
  });

$("#btn-filter-clear").on("click", function () {
    type = $("#lead_type").val();
  window.location.href = "{!! url() !!}/admin/hotel/reservation-index";
});

</script>
@endsection
@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              POS
                <small>Unsettled POS Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">Unsettled POS List</font></b>
        </div>
</div>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr>
        <th>Order #</th>
        <th>Res #</th>
        <th>Room #</th>
        <th>Guest name</th>
        <th>Bill Date</th>
        <th>Outlet</th>
        <th>Session</th>
        <th>Total</th>
        <th>Due Amount</th>
        <th>POS Officer</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($orders as $key=>$ord)
    <tr>
      <td> {{$ord->id}} </td>
      <td> {{$ord->reservation_id}}</td>
      <td> <strong>{{$ord->reservation->room_num}}</strong></td>
      <td>
        @if($ord->reservation->guest_id)
        {{$ord->reservation->guest->full_name}}
        @else
        {{$ord->name}}
        @endif
      </td>
      <td>
        {{ date('dS M y', strtotime($ord->bill_date)) }}
      </td>
      <td>
       {{$ord->outlet->name}}
       </td>
      <td>{{$ord->session->name}}</td>
      <td> {{env('CUR_CODE')}} {{$ord->total_amount}} </td>
      <td> {{env('CUR_CODE')}} {{\TaskHelper::UnsetteledPOS($ord->reservation_id)}}</td>
      <td> {{$ord->user->username}}</td>
      <td>
        <a href="/admin/payment/orders/{{$ord->id}}/create"  title="View Payment"><i class="fa fa-credit-card"></i></a>
      </td>
    </tr>
    @endforeach
</tbody>
</table>
</div>
@endsection






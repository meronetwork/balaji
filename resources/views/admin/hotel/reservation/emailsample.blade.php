<!DOCTYPE html><html lang="en"><head>
    <meta charset="utf-8">
    <title>{{ env('APP_COMPANY')}} | INVOICE</title>
    <style type="text/css"> 
@font-face {
  font-family: Arial;
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  width: 18cm;  
  height: 24.7cm; 
  margin: 0 auto; 
  color: #555555;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}

table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}

table th {
  white-space: nowrap;        
  font-weight: normal;
}

table td {
  text-align: left;
}

table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}

table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}

table .desc {
  text-align: left;
}

table .unit {
  background: #DDDDDD;
}

table .qty {
}

table .total {
  background: #57B223;
  color: #FFFFFF;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table tbody tr:last-child td {
  border: none;
}

table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap; 
  border-top: 1px solid #AAAAAA; 
}

table tfoot tr:first-child td {
  border-top: none; 
}

table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223; 
  font-weight: bold;

}

table tfoot tr td:first-child {
  border: none;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

#bg-text
{
    color:lightgrey;
    position: absolute;
    left:0;
    right:0;
    top:40%;
    text-align:center;
    margin:auto;
    opacity: 0.5;
    z-index: 2;
    font-size:120px;
    transform:rotate(330deg);
    -webkit-transform:rotate(330deg);
}

  </style>
  </head><body>
     
    <header class="clearfix">
     
        <table>
            <tr>
        <td width="50%" style="float:left">
          <div id="logo">
            <img style="max-width: 150px" src="{{env('APP_URL')}}/{{ '/org/'.$organization->logo }}">
          </div>
        </td>
        <td width="50%" style="text-align: right">
        <div  id="company">
          <h4 class="name">{{ env('APP_COMPANY') }} </h4>
          <div>{{ env('APP_ADDRESS1') }}</div>
          <div>{{ env('APP_ADDRESS2') }}</div>
          <div>Seller's PAN: {{ env('TPID') }}</div>
          <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>

        </div>
        </td></tr>

      </table> 

      </div>
    </header>
    <main>
      <div id="details" class="clearfix">

        
    <!-- /.row -->
      <p>Thank you for Making Reservationat the {{ env('APP_COMPANY') }}. We have reserved The following accommodation for you.</p>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="no">Arrival Date</th>
            <th class="no">Depature Date</th>
            <th class="no">Nightly Rate</th>
            <th class="no">Rate Plan</th>
            <th class="no">Room Type</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $n= 0;
          ?>
          <tr>
            <td>{{++$n}}</td>
            
             <td>{{ $reservation->check_in }}</td>
             
            <td>{{ $reservation->check_out }}</td>
           
            <td>{{ env('APP_CURRENCY').' '.number_format($reservation->room_rate,2) }}</td>
            <td>{{ $reservation->rateplan->package_name }}</td>
            <td>{{ $reservation->rateplan->roomtype->room_name }}</td>
           
          </tr>
          @foreach($reservation_child as $odk => $odv)
          <tr>
            <td>{{++$n}}</td>
            
             <td>{{ $odv->check_in }}</td>
             
            <td>{{ $odv->check_out }}</td>
           
            <td>{{ env('APP_CURRENCY').' '.number_format($odv->room_rate,2) }}</td>
            <td>{{ $odv->rateplan->package_name }}</td>
            <td>{{ $odv->rateplan->roomtype->room_name }}</td>
          </tr>
         @endforeach
        </tbody>
        </table>

       <p>Your Confirmation Number Is , You are Guranteed for late arrival. If you find it necessary to cancel or change plans please inform us by 4.00pm  central standard time on your arrival dateto avoid date to avoid one night's room and tax charge to your credit card.</p>

       <p>Again, ThankYpu For Choosing the {{env('APP_COMPANY')}}. We Look ForWard to having you as a Guest.</p>

      
      
      <div id="">Best Rrgards,</div>
        <div id="">Reservation Office</div>
        <div id="">{{env('APP_COMPANY')}}</div>
    </main>
    <footer> 
      Reservation was created on MEROCRM.
    </footer></body></html>
@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<style>
 .res-form select { width:200px !important; }
.res-form label {
    font-weight: 600 !important;
}

 .intl-tel-input { width: 100%; }
 .intl-tel-input .iti-flag .arrow {border: none;}
.ui-datepicker{
  z-index: 10000 !important;
}


</style>


<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>
<link rel="stylesheet" type="text/css" href="/bootstrap-iso.css">
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        New Reservation /Walkin
        <small>Create a new reservation</small>
    </h1>
    <i class="fa fa-info"></i> Use Profile feature for foreign, special and regular guests {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="bootstrap-iso res-form" style="min-height: 1416.81px;">
<form method="post" action="{{route('admin.hotel.reservation-create')}}"
enctype="multipart/form-data" id="reservationform" onsubmit="return CheckCheckInCheckout(this);" class="spinning-form">
    {{ csrf_field() }}
   <!-- Main content -->
   <div class="row">
      <div class="col-md-8">
         <div class="card card-primary">
            <div class="card-header text-white" style="background-color: #333;">
               <h3 class="card-title"><i class="fa fa-user"></i> Guest Information</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label>  Title </label>
                     <select name="title" class="form-control searchable select2 input-sm" id="title">
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Ms</option>
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label>  Guest Name <small> <a title="Create New Guest" href="#" onClick='openwindowguest()'> [+] <i class="fa fa-external-link"></i> </a></small> </label>
                     <input style=" text-transform: uppercase" type="text" name="guest_name" placeholder="guest name.." value="{{ old('guest_name')}}" class="form-control input-sm res_guests" id="res_guests" required="">
                  </div>
                  <div class="form-group col-sm-4" id=''>
                     <label>Guest Type</label>
                     {!! Form::select('guest_type',['TOU'=>'Tourist','BUS'=>'Business Travellers','FAM'=>'Families','STA'=>'Hotel Staffs','DEl'=>'Delegates','VIP'=>'VIP','COR'=>'Corporate','GOV'=>'Government','FIT'=>"FIT",'DIP'=>"Diplomats",'COM'=>"Complementry"], old('guest_type'), ['class' => 'form-control searchable select2 input-sm'] )!!}
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label for="inputEmail3" class="col-sm-6 control-label">
                     Agent <a title="Create New Agent" href="#" onclick="openwindowagent()">[+]</a>
                     </label>
                     <select name="agent_id" class="form-control searchable select2 input-sm">
                        <option value="">Select Agent</option>
                        @foreach($agent as $ag)
                        <option value="{{$ag->id}}" @if(\Request::old( 'agent_id' )==$ag->id) selected @endif >{{$ag->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label>Source</label>
                     <select name="source_id" class="form-control searchable select2 input-sm" required>
                        <option value="">Select Source</option>
                        @foreach($sources as $ag)
                        <option value="{{$ag->id}}" @if(\Request::old( 'agent_id' )==$ag->id) selected @endif>{{$ag->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label>
                     Company <a title="Create New Company" href="#" onclick="openwindowcompany()">[+]</a>
                     </label>
                     <select name="company_id" class="form-control  searchable  input-sm">
                        <option value="">Select Company</option>
                        @foreach($client as $cl)
                        <option value="{{$cl->id}}" @if(\Request::old( 'company_id' )==$cl->id) selected @endif>{{$cl->name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-info">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-calendar"></i> Dates and Availability</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label >Check in</label>
                      <input required="" type="text" class="form-control occupied_date_from input-sm check_in datepicker" value="{{\Carbon\Carbon::now()->toDateString()}}" name="book_in" id="check_in" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Check out </label>
                     <input required="" type="text" class="form-control occupied_date_from input-sm datepicker" value="{{\Carbon\Carbon::now()->addDays(1)->toDateString()}}" name="book_out" id="check_out" readonly="">
                  </div>
                  <div class="form-group col-sm-4" id='city'>
                      <label >Check in time </label>
                     <input type="text" class="form-control occupied_date_from input-sm timepicker" value="{{\Carbon\Carbon::now()}}" name="check_in_time" id="check_in_time">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label >Book out time </label>
                     <input type="text" class="form-control occupied_date_from input-sm timepicker" value="" name="check_out_time" id="check_out_time">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Arrival time </label>
                     <input type="text" class="form-control occupied_date_from input-sm datetimepicker" value="" name="arrival_time" id="arrival_time">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Arrival <small>Airlines/Bus</small></label>
                     <input type="text" class="form-control occupied_date_from input-sm" value="" name="flight_no">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                      <label> Arrival <small>Flight/Bus time</small></label>
                     <input type="text" class="form-control occupied_date_from input-sm timepicker" value="" name="arrival_flight_time" id="arrival_flight_time">
                  </div>

                  <div class="form-group col-sm-4">
                     <label >Occupancy <small> Adult</small></label>
                      <input type="number" min="1" max="5" class="form-control input-sm" value="1" name="occupancy" id="occupancy" required="">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Occupancy <small> Child</small></label>
                    <input type="number" min="1" max="5" class="form-control input-sm" value="" name="occupancy_child" id="occupancy_child">
                  </div>
                   <div class="form-group col-sm-4">
                     <label >Booked By <a title="New Contact" href="#" onclick="openwindowcontacts()">[+]</a></label>
                     <input type="text" class="form-control input-sm" placeholder="search contact" value="" id="booked_by" name="booked_by">
                  </div>
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-bed"></i> Stay Information</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                    <label class="control-label">Rooms (Group Book)</label>
                    <input type="number" min="1" max="20" name="num_of_rooms" placeholder="" id="num_of_rooms" value="1" class="form-control input-sm">
                  </div>
                  <div class="form-group col-sm-4">
                     <label for="inputEmail3" class="ccontrol-label ">Room</label>
                      <select name="room_num" class="form-control searchable select2 input-sm room_num" onchange="getroomplanforparent(this)" >
                        <option value="">Select room</option>
                        @forelse($room as $rm)
                        <option value="{{$rm['room_number']}}">{{ucfirst(trans($rm['room_number']))}} ({{$rm['room_name']}})</option>
                        @empty
                        <option value="">No Room available</option>
                        @endforelse
                        </select>
                  </div>
                  <div class="form-group col-sm-4" id='city'>
                    <label  class="control-label">Rate Plan</label>
                     <select class="form-control searchable select2 input-sm room_plan" id="room_plan"  onchange="changerateplan(this,'parent')" name="rate_id">
                        <option value="">Select Rate Plan</option>
                      </select>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                    <label class="control-label">Res. type</label>
                    <select name="reservation_type" class="form-control  searchable select2 input-sm">
                      @foreach($rtype as $rt)
                      <option value="{{$rt->id}}" @if(Request::old( 'reservation_type' )==$rt->id) selected @endif>{{$rt->name}}</option>
                      @endforeach
                  </select>
                  </div>
                  <div class="form-group col-sm-4">
                   <label  class="control-label ">Days</label>
                    <input type="number" name="number_of_days" placeholder="days.." id="number_of_days" value="1" class="form-control input-sm" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                      <label >Room Rate</label>
                      <input type="number" name="room_rate" placeholder="" id="parent_room_rate" class="form-control input-sm">
                  </div>
                  <div class="form-group col-sm-4">
                      <label > Smoking</label>
                      <select name="smoking" class="form-control searchable select2">

                        <option value="0">No</option>
                         <option value="1">Yes</option>
                      </select>
                  </div>
               </div>

            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-money"></i> Billing Information</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label  class="control-label ">Payment Type</label>
                     <select name="payment_type_id" class="form-control searchable select2 input-sm" id="payment_type_id" required="required">
                          <option value="">Select Payment Type</option>
                          @foreach($paymenttype as $pt)
                          <option value="{{$pt->payment_type_id}}">{{ $pt->payment_name }}</option>
                          @endforeach
                      </select>
                  </div>
                  <div class="form-group col-sm-4">
                      <label class="control-label">Card No</label>
                     <input type="text" name="cc_no" placeholder="Credit Card No" id="cc_no" value="" class="form-control input-sm">
                  </div>
                  <div class="form-group col-sm-4" id='city'>
                    <label class="control-label">Card Expiry Date</label>
                   <input type="text" name="cc_exp_date" placeholder="Expiry date" id="cc_exp_date" value="" class="form-control input-sm datepicker">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label class="control-label">Name On Card</label>
                     <input type="text" name="name_on_card" placeholder="Name On Card" id="name_on_card" value="" class="form-control input-sm">
                  </div>
                  <div class="form-group col-sm-4">
                     <label class="control-label">Bill To</label>
                     {!! Form::select('bill_to',['company'=>'Company','group'=>'Group Owner','guest'=>'Guest','companyandguest'=>'Room and Tax to Company Extra to Guest','com'=>"Complementry",'groupindividual'=>'Group Individual'], old('guest_type'), ['class' => 'form-control searchable select2 input-sm'] )!!}
                  </div>
                  <div class="form-group col-sm-4" style="margin-top: 23px;">

                     <label class="control-label">Card Swiped&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="hidden" name="card_swiped" value="0">
                      <input type="checkbox" name="card_swiped" value="1" class="form-check-input">
                    </label>
                  </div>
               </div>

            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Remarks</h3>
            </div>
            <div class="card-body" style="display: block;margin: 0;">

               <div class="form-group">
                        <div class="input-group ">
                            <textarea type="text" name="remarks" placeholder="Write reservation remarks" id="remarks" class="form-control" rows="10" ></textarea>

                        </div>
                    </div>

            </div>
            <!-- /.card-body -->
         </div>

           <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Document Information</h3>
            </div>
            <div class="card-body" style="display: block;margin: 0;">
            <a href="javascript::void(1)" class="btn btn-primary btn-xs" id="addDoc" style="float: right;">
                    <i class="fa fa-plus"></i> <span>Add Documents</span>
                </a>
                <table class="table">

                    <tbody>
                        <tr>
                          <td class="moreroomtd">
                              <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-6 control-label">
                                      Document
                                  </label>
                                  <div class="col-md-6">
                                      <input type="file" name="doc[]">
                                  </div>
                              </div>
                          </td>
                          <td>
                              <a href="javascript::void(1);" style="width: 10%;">
                                  <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                              </a>
                          </td>
                      </tr>
                        <tr class="multipleDivDocs">

                        </tr>
                    </tbody>

                </table>

            </div>
            <!-- /.card-body -->
         </div>

         <!-- /.card -->
         <hr>



      </div>
      <div class="col-md-4">
         <div class="card card-secondary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-phone-square"></i> Contact Info</h3>
            </div>
            <div class="card-body">
               <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" placeholder="Email.." size="6" value="{{old('email')}}" class="form-control input-sm" required>
               </div>
               <div class="form-group">
                  <label >Phone</label>
                  <input type="number" name="phone" placeholder="Phone Number.." size="6" value="{{old('phone')}}" class="form-control input-sm">
               </div>
               <div class="form-group">
                  <label >Mobile</label>
                  <input type="number" name="mobile" placeholder="Mobile Number.." size="6" value="{{old('mobile')}}" class="form-control input-sm">
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <br/>
         <div class="card card-secondary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-info-circle"></i> Other Information</h3>
            </div>
            <div class="card-body">
               <div class="form-group">
                <label class="control-label">Nationality</label>
                <div class="input-group ">
                    {!! Form::select('nationality', ["Afghanistan"=>"Afghanistan", "Albania"=>"Albania", "Algeria"=>"Algeria", "American Samoa"=>"American Samoa", "Andorra"=>"Andorra", "Angola"=>"Angola", "Anguilla"=>"Anguilla", "Antigu"=>"Antigu", "Argentina"=>"Argentina", "Armenia"=>"Armenia", "Aruba"=>"Aruba", "Australia"=>"Australia", "Austria"=>"Austria", "Azerbaijan"=>"Azerbaijan", "Bahamas"=>"Bahamas", "Bahrain"=>"Bahrain", "Bangladesh"=>"Bangladesh", "Barbados"=>"Barbados", "Belarus"=>"Belarus", "Belgium"=>"Belgium", "Belize"=>"Belize", "Benin"=>"Benin", "Bermuda"=>"Bermuda", "Bhutan"=>"Bhutan", "Bolivia"=>"Bolivia", "Bosnia And Hercegovina"=>"Bosnia And Hercegovina", "Botswana"=>"Botswana", "Brazil"=>"Brazil", "British Virgin Islands"=>"British Virgin Islands", "Brunei"=>"Brunei", "Bulgaria"=>"Bulgaria", "Burkina Faso"=>"Burkina Faso", "Burundi"=>"Burundi", "Cambodia"=>"Cambodia", "Cameroon"=>"Cameroon", "Canada"=>"Canada", "Capeverde"=>"Capeverde", "Cayman Islands"=>"Cayman Islands", "Central African Republic"=>"Central African Republic", "Chad"=>"Chad", "Chile"=>"Chile", "China"=>"China", "Colombia"=>"Colombia", "Comoros"=>"Comoros", "Congo"=>"Congo", "Costa Rica"=>"Costa Rica", "Croatia"=>"Croatia", "Cuba"=>"Cuba", "Cyprus"=>"Cyprus", "Czech Republic"=>"Czech Republic", "Denmark"=>"Denmark", "Djibouti"=>"Djibouti", "Dominca"=>"Dominca", "Dominican Republic"=>"Dominican Republic", "Ecuador"=>"Ecuador", "Egypt"=>"Egypt", "El Salvador"=>"El Salvador", "Equatorial Guinea"=>"Equatorial Guinea", "Eritrea"=>"Eritrea", "Estonia"=>"Estonia", "Ethiopia"=>"Ethiopia", "Falkland Islands"=>"Falkland Islands", "Fiji"=>"Fiji", "Finland"=>"Finland", "France"=>"France", "Gabon"=>"Gabon", "Gambia"=>"Gambia", "Georgia"=>"Georgia", "Germany"=>"Germany", "Ghana"=>"Ghana", "Greece"=>"Greece", "Greenland"=>"Greenland", "Grenada"=>"Grenada", "Guam"=>"Guam", "Guatemala"=>"Guatemala", "Guinea"=>"Guinea", "Guinea-bissau"=>"Guinea-bissau", "Guyana"=>"Guyana", "Haiti"=>"Haiti", "Honduras"=>"Honduras", "Hungary"=>"Hungary", "Iceland"=>"Iceland", "India"=>"India", "Indonesia"=>"Indonesia", "Iran"=>"Iran", "Iraq"=>"Iraq", "Ireland"=>"Ireland", "Israel"=>"Israel", "Italy"=>"Italy", "Jamaica"=>"Jamaica", "Japan"=>"Japan", "Jordan"=>"Jordan", "Kazakhstan"=>"Kazakhstan", "Kenya"=>"Kenya", "Kiribati"=>"Kiribati", "Kuwait"=>"Kuwait", "Laos"=>"Laos", "Latvia"=>"Latvia", "Lebanon"=>"Lebanon", "Lesotho"=>"Lesotho", "Liberia"=>"Liberia", "Libya"=>"Libya", "Liechtenstein"=>"Liechtenstein", "Lithuania"=>"Lithuania", "Luxembourg"=>"Luxembourg", "Macedonia"=>"Macedonia", "Madagascar"=>"Madagascar", "Malawi"=>"Malawi", "Malaysia"=>"Malaysia", "Maldives"=>"Maldives", "Mali"=>"Mali", "Malta"=>"Malta", "Marshall Islands"=>"Marshall Islands", "Mauritania"=>"Mauritania", "Mauritius"=>"Mauritius", "Mexico"=>"Mexico", "Micronesia"=>"Micronesia", "Moldova"=>"Moldova", "Monaco"=>"Monaco", "Mongolia"=>"Mongolia", "Morocco"=>"Morocco", "Mozambique"=>"Mozambique", "Myanmar"=>"Myanmar", "Namibia"=>"Namibia", "Nauru"=>"Nauru", "Nepal"=>"Nepal", "Netherlands"=>"Netherlands", "New Zealand"=>"New Zealand", "Nicaragua"=>"Nicaragua", "Niger"=>"Niger", "Nigeria"=>"Nigeria", "North Korea"=>"North Korea", "Norway"=>"Norway", "Oman"=>"Oman", "Pakistan"=>"Pakistan", "Palau"=>"Palau", "Panama"=>"Panama", "Papua New Guinea"=>"Papua New Guinea", "Paraguay"=>"Paraguay", "Peru"=>"Peru", "Philippines" => "Philippines", "Poland"=>"Poland", "Portugal"=>"Portugal", "Qatar"=>"Qatar", "Romania"=>"Romania", "Russia"=>"Russia", "Rwanda"=>"Rwanda", "San Marino"=>"San Marino", "Sao Tome And Principe"=>"Sao Tome And Principe", "Saudi Arabia"=>"Saudi Arabia", "Senegal"=>"Senegal", "Serbia"=>"Serbia", "Seychelles"=>"Seychelles", "Sierra Leone"=>"Sierra Leone", "Singapore"=>"Singapore", "Slovakia"=>"Slovakia", "Slovenia"=>"Slovenia", "Solomon Islands"=>"Solomon Islands", "Somalia"=>'Somalia', "outh Africa"=>"South Africa", "South Korea"=>"South Korea", "Spain"=>"Spain", "Sri Lanka"=>"Sri Lanka", "Sudan"=>"Sudan", "Suriname"=>"Suriname", "Swaziland"=>"Swaziland", "Sweden"=>"Sweden", "Switzerland"=>"Switzerland", "Syria"=>"Syria", "Taiwan"=>"Taiwan", "Tajikistan"=>"Tajikistan", "Tanzania"=>"Tanzania", "Thailand"=>"Thailand", "Togo"=>"Togo", "Tonga"=>"Tonga", "Trinidad And Tobago"=>"Trinidad And Tobago", "Tunisia"=>"Tunisia", "Turkey"=>"Turkey", "Turkmenistan"=>"Turkmenistan", "Tuvalu"=>"Tuvalu", "Uganda"=>"Uganda", "Ukraine"=>"Ukraine", "United Arab Emirates"=>"United Arab Emirates", "United Kingdom"=>"United Kingdom", "United States Of America"=>"United States Of America", "Uruguay"=>"Uruguay", "Uzbekistan"=>"Uzbekistan", "Vanuatu"=>"Vanuatu", "Venezuela"=>"Venezuela", "Viet Nam"=>"Viet Nam", "Zambia"=>"Zambia", "Zimbabwe"=>"Zimbabwe"],'Nepal', ['class' => 'form-control searchable label-default']) !!}

                </div>
            </div>
               <div class="form-group">
                  <label  class="control-label">Gender</label>
                  <select name="gender" class="form-control   input-sm w-100">
                      <option value="M">Male</option>
                      <option value="F">Female</option>
                  </select>
               </div>
               <div class="form-group">
                  <label class="control-label">Doc Type</label>
                  <input type="text" name="doc_type_extra" placeholder="Doc type.." size="6" value="" class="form-control input-sm">
               </div>
               <div class="form-group">
                  <label class="control-label">Doc. Number</label>
                  <input type="text" name="doc_num_extra" placeholder="Doc number.." size="6" value="" class="form-control input-sm">
               </div>
            </div>
            <!-- /.card-body -->
         </div>

      </div>
   </div>

     <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">More Guest info</h3>
            </div>
         <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>S.N</th>
                            <th>Room</th>
                            <th>Rate Plan</th>
                            <th>Room Rate</th>
                            <th>Adult</th>
                            <th>Child</th>
                            <th>Guest Name</th>
                            <th>Doc Type</th>
                            <th>Doc Num.</th>
                        </tr>

                        <tbody id='addmorerooms'>
                            <tr class="addmorerooms">

                            </tr>
                        </tbody>

                    </table>

                </div>
            </div>
          </div>
<br>
            <div class="row">
            <div class="col-12">
               <a href="/admin/hotel/reservation-index" class="btn btn-secondary btn-lg">Cancel</a>
              <button class="btn btn-success btn-lg" id="btn-submit-edit" type="submit" name="submit_method" value="reserv">Add Reservation</button>
            </div>
         </div>
   </form>
   <!-- /.content -->
</div>
<div id='more_room_guest_toadd' style="display: none;">
    <table class="table">
        <tbody id='more-tr-room'>
            <tr class="additionroomtd">
                <td class="morerooms-sn"></td>
                <td>
                    <select name="extra_room_num[]" class="form-control room_num " onchange="getroomplanforchild(this)" required="">
                        <option value="">Select room</option>
                        @forelse($room as $rm)
                        <option value="{{$rm['room_number']}}">{{ucfirst(trans($rm['room_number']))}} ({{$rm['room_name']}})</option>
                        @empty
                        <option value="">No Room available</option>
                        @endforelse
                    </select>
                </td>
                <td style="min-width: 100px">
                    <select name="extra_rate_plan[]" class="form-control room_plan " required="" onchange="changerateplan(this,'child')">
                        <option value="">Select Rate Plan</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="form-control room_rate_value" name="extra_room_rate[]" placeholder="Room rate">
                </td>
                <td>
                    <input name="extra_no_of_adult[]" class="form-control" placeholder="No. of Adults">
                </td>
                <td>
                    <input name="extra_no_of_child[]" class="form-control" placeholder="No. of Child">
                </td>
                <td>
                    <input name="extra_guest_name[]" class="form-control res_guests-additional" placeholder="Guest Name" required="">
                    <input type="hidden" name="extra_guest_id[]" class="moreguest_id">
                </td>
                <td>
                    <input name="extra_doc_type[]" class="form-control" placeholder="Document type">
                </td>
                <td>
                    <input name="extra_doc_num[]" class="form-control" placeholder="Document Number">
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div id="addmoredocs" style="display: none;">
    <table class="table">
        <tbody id="more-tr">
            <tr>
                <td class="moreroomtd">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">
                            Document
                        </label>
                        <div class="col-md-6">
                            <input type="file" name="doc[]">
                        </div>
                    </div>
                </td>
                <td>
                    <a href="javascript::void(1);" style="width: 10%;">
                        <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection


@section('body_bottom')
 @section('body_bottom')
<?php $today = date('Y-m-d');
$today = json_encode($today); ?>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $("#addRoom").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDivRoom").after($('#addmoreroom #more-tr').html());

    });


    $("#addDoc").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDivDocs").after($('#addmoredocs #more-tr').html());
    });
    $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();
    });
    $(function() {
        $('.datepicker').datepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            dateFormat: 'yy-mm-dd'
            , sideBySide: true
            , minDate: 0
        });

        $('.timepicker').datetimepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            format: 'HH:mm'
            , sideBySide: true
        });

        $('.datetimepicker').datetimepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            format: 'YYYY-MM-DD HH:mm'
            , sideBySide: true

        });

    });

    function getroom(date) {
        $('.room_plan').each(function() {
            $(this).empty();
        });
        var roomoption = "<option value=''>Select room </option>";
        $.get('/admin/hotel/reservation/checkdate/' + date, function(data, status) {
            for (let room of data) {
                let option = `<option value='${room.room_number}'>${room.room_number}(${room.room_name})`;
                roomoption = roomoption + option;
            }
            $('.room_num').each(function() {
                $(this).html(roomoption);
            });
        });
    }

    $("#check_in,#check_out").on("change", function(event) {

        var check_in = $('#check_in').val();
        var check_out = $('#check_out').val();
        var date1 = new Date(check_in);
        var date2 = new Date(check_out);
        var diffTime = Math.abs(date2 - date1);
        var days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        if (days == 0) days = 1;
        if (!isNaN(days)) {
            $('#number_of_days').val(days);
        }
        let target = event.target.id;
        if (target == 'check_in') {
            getroom(check_in);
        }
    });

    function disableroom() {
        $('.room_num').children().prop('disabled', false);
        $('.room_num').each(function() {
            var val = $(this).val();
            if (val === '') return;
            $('.room_num').not(this).children('[value="' + val + '"]').prop('disabled', true);
        });
    }

    function getroomplanforparent(ev) {
        $('.searchable').select2('destroy');
        $('#room_plan').empty();
        disableroom();
        let option = roomplanapi(ev.value).then(function(result) {
            $('#room_plan').html(result);
        });

        $('.searchable').select2();
    }

    function getroomplanforchild(ev) {
        $('.searchable').select2('destroy');
        var parent = $(ev).parent().parent();
        var roomplan = parent.find('.room_plan');
        roomplan.empty();
        disableroom();
        let option = roomplanapi(ev.value).then(function(result) {
            $(roomplan).html(result);
        });
        $('.searchable').select2();
    }

    //changing rate plan
    function roomplanapi(id) {
        return new Promise(function(resolve, reject) {
            var roomplanoptions = "<option value=''>Select Rate Plan</option>";
            $.get('/admin/hotel/reservation-getroom-plan/' + id, function(data, status) {
                for (let plan of data) {
                    let total = Number(plan.rate) + Number(plan.breakfast_rate) + Number(plan.lunch_rate) + Number(plan.dinner_rate);
                    roomplanoptions = roomplanoptions + "<option value=" + JSON.stringify({
                        id: plan.id
                        , value: total
                    }) + ">" + plan.package_name + "(" + total + ")</option>";
                }
                return resolve(roomplanoptions);
            });
        });
    }

    function changerateplan(ev, type) {
        var value = JSON.parse($(ev).val());
        if (type == 'parent') {
            $('#parent_room_rate').val(value.value);

        } else { //type is child
            var parent = $(ev).parent().parent();
            parent.find('.room_rate_value').val(value.value);
        }
    }


    $('#num_of_rooms').change(function() {
        if ($(this).val() > 20) {
            $(this).val(20);
            alert('Room cannot be extended more than 20');

        }
        $('.searchable').select2('destroy');
        var numberof_roomtoadd = Number($(this).val()) - 1;
        let totalmorerooms = $('#addmorerooms .additionroomtd').length;
        let toadded_or_to_remove = numberof_roomtoadd - Number(totalmorerooms);
        console.log(numberof_roomtoadd, totalmorerooms, toadded_or_to_remove);
        if (toadded_or_to_remove > 0) {
            for (let i = 0; i < toadded_or_to_remove; i++) {
                $("#addmorerooms").append($('#more_room_guest_toadd #more-tr-room').html());
            }
            $('.morerooms-sn').each(function(index) {
                $(this).text((index + 1) + '.');
            });
            $('.res_guests-additional').each(function() {
                var parent = $(this).parent().parent();
                $(this).autocomplete({
                    source: "/admin/hotel/guests/ajaxgetGuest"
                    , minLength: 1
                    , change: function(event, ui) {
                        parent.find('.moreguest_id').val(ui.item.id);
                    }
                });
            });
        } else {
            $('#addmorerooms > .additionroomtd').slice(toadded_or_to_remove).remove();
            disableroom();
        }
        $('.searchable').select2();

    });

    $(document).ready(function() {
        $('#complementry_by').css('visibility', 'hidden');
        $('.searchable').select2();
    });
    $('#booked_by').autocomplete({
        source: "/admin/getContacts"
        , minLength: 1
    , });



    $('.check_in_submit').on('click', function() {
            let check = $('#check_in').val();
            let p = <?php echo $today; ?>;
    if (check == p) {
        return true;
    }
    alert("Check in date should be of today");
    return false;
});


$('#guest_type_id').on('change', function() {
    if ($(this).val() != 'COM') {
        $('#complementry_by').css('visibility', 'hidden');
        $('.payment_info').show();
    } else {

        $('#complementry_by').css('visibility', 'visible');
        $('#booked_by').attr("readonly", "readonly");
        $('.payment_info').hide();
        $('#payment_type_id').removeAttr("required", "required");

    }
});





function populateGuest(guest) {
    $('.searchable').select2('destroy');
    $("input[name='email']").val(guest.email);
    $("input[name='guest_id']").val(guest.id);
    $("input[name='phone']").val(guest.landline);
    $("input[name='mobile']").val(guest.mobile);
    $("select[name='title']").val(guest.title);
    $("select[name='gender']").val(guest.gender);
    $("select[name='nationality']").val(guest.country);
    $("select[name='guest_type']").val(guest.guest_type);
    $('.searchable').select2();
}

$('.res_guests').autocomplete({
    source: "/admin/hotel/guests/ajaxgetGuest",
    minLength: 1,
    change: function(event, ui) {
        if (ui.item) {
            let guest = JSON.parse(ui.item.data);
            populateGuest(guest);
        } else {
            $('.searchable').select2('destroy');
            $(`input[name='guest_id'],
                  input[name='email'],
                  input[name='phone'],
                  input[name='mobile'],
                  select[name='guest_type']
                  `).val('');
            $(`select[name='title']`).val('Mr');
            $(`select[name='gender']`).val('M');
            $(`select[name='nationality']`).val('Nepal');
            $(`select[name='guest_type']`).val('TOU')
            $('.searchable').select2();

        }
        //store in session
    }
});

function openwindowguest() {
    var win = window.open('/admin/hotel/guests/modal', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function opencalendar() {
    var win = window.open('/admin/hotel/reservationcalender/modal', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=50,left=500,width=700, height=700');
}

function openwindowagent() {
    var win = window.open('/admin/clients/modals/create?relation_type=agent', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function openwindowcompany() {
    var win = window.open('/admin/clients/modals/create?relation_type=hotelcustomer', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function openwindowcontacts() {
    var win = window.open('/admin/contacts/create/modals', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function updateform(result) {
    if (result.type == 'guest') {
        $('#res_guests').val(result.guest.first_name + ' ' + result.guest.last_name);
        populateGuest(result.guest);
    } else if (result.type == 'contacts') {
        $('#booked_by').val(result.contacts.full_name)
    } else {
        $('.searchable').select2('destroy');

        let clients = result.clients;
        var oldagent = $('select[name=agent_id]').val();
        var oldcompany = $('select[name=company_id]').val();
        var option = '';
        for (let c of clients) {
            option = option + `<option value='${c.id}'>${c.name}</option>
            `;
        }
        $('select[name=agent_id]').html(` < option value = '' > Select Agent < /option>` + option);
            $('select[name=company_id]').html(`<option value=''>Select Company</option>` + option);

            setTimeout(function() {
                if (result.relation_type == 'agent')
                    $('select[name=agent_id]').val(result.lastcreated);
                else
                    $('select[name=agent_id]').val(oldagent);

                if (result.relation_type == 'hotelcustomer')
                    $('select[name=company_id]').val(result.lastcreated);
                else
                    $('select[name=company_id]').val(oldcompany);

                $('.searchable').select2();
            });
        }
    }

    function HandlePopupResult(result) {
        if (result) {
            updateform(result);
            setTimeout(function() {
                $("#ajax_status").after("<span style='color:green;' id='status_update'>Client sucessfully created</span>");
                $('#status_update').delay(3000).fadeOut('slow');
            }, 500);
        } else {
            $("#ajax_status").after("<span style='color:red;' id='status_update'>failed to create clients</span>");
            $('#status_update').delay(3000).fadeOut('slow');
        }
    }

</script>

<script type="text/javascript">
    function CheckCheckInCheckout(theform) {

        if (document.getElementById("check_in").value > document.getElementById("check_out").value) {
            alert('Check In Should Be Smaller Than Check OUT');
            return false;
        } else {
            return true;
        }
    }

</script>


<!-- form submit -->
@include('partials._body_bottom_submit_lead_edit_form_js')
@endsection

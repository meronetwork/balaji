@extends('layouts.master')
@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
  tr td { text-align:left !important; }
</style>
    <div class='row'>
        <div class='col-md-6'>
          <!-- Box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="">
                            <p> Extend History for Reservation room #{{ Request::segment(4) }}</p>
                            <table class="table table-hover table-bordered" id="leads-table">
                                <thead>
                                    <tr>
                                        <th>Room</th>
                                        <th>Extended by</th>
                                        <th>Add Guest</th>
                                        <th>Action</th> 
                                    </tr>
                                    @if($history)
                                    @foreach($history as $k => $v)
                                    <tr>
                                        <td>{{ $v->room_num }}</td>
                                        <td>{{ ucfirst(trans($v->user->username)) }} </td>
                                         <td><a href="/admin/hotel/room-guest/{{$v->res_id}}/{{$v->room_num}}">Add Guest</a></td>
                                        <td >&nbsp;&nbsp;&nbsp;&nbsp;<a href="{!! route('admin.hotel.extend-room-confirmdel', $v->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    <tr>
                                        <td>{{ $main_res->room_num }}</td>
                                        <td>{{ ucfirst(trans($main_res->user->username)) }} </td>
                                         <td><a href="/admin/hotel/room-guest/{{$main_res->id}}/{{$main_res->room_num}}">Add Guest</a></td>
                                        <td >&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-trash-o text-muted"></i></td>
                                    </tr>
                                </thead>
                            </table>
                          <form method="post" action="{{route('admin.hotel.extend-room',$res_id)}}">
                            {{csrF_field()}}
                            <label>Add Room</label>
                            <div class="row">
                            <div class="col-md-4">
                        <select class = 'form-control input-sm searchable select2' name="room_num" id="to_room" required="">
                       <option value="">Select Room</option>
                        @foreach($rooms as $rm)
                        <option value="{{$rm['room_number']}}">
                        {{$rm['room_number']}} ({{$rm['room_name']}})
                        </option>
                        @endforeach
                        </select>
                       </div>
                    </div>
                <br>
        <button class="btn btn-primary" type="submit">Add Room</button>
         </form>
<div class="row">
                        <div class="col-md-3">
                     </div>
                </div>
            </div>
                        </div> <!-- table-responsive -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('body_bottom')

 <script type="text/javascript">
    
     $(document).ready(function(){
            $('.searchable').select2();
        });
</script>
@endsection  
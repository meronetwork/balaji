@extends('layouts.master')
@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<style>
  tr td { text-align:left !important; }
</style>

    <div class='row'>
        <div class='col-md-6'>
          <!-- Box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="">
                            <p> Extend History for Reservation package #{{ Request::segment(4) }}</p>
                            <table class="table table-hover table-bordered" id="leads-table">
                                <thead>
                                    <tr>
                                        <th>Package</th>
                                        <th>Number of person</th>
                                        <th>Package date</th>
                                    </tr>
                                    @if($history)
                                    @foreach($history as $k => $v)
                                    <tr>
                                        <td>{{ $v->services->attraction_name }}
                                        </td>
                                        <td>{{ $v->number_of_person }} </td>
                                        <td>{{ $v->package_date }} </td>
                                    </tr>
                                    @endforeach
                                    @endif

                                </thead>
                            </table>

                          <form method="post" action="{{route('admin.hotel.extend-package',$res_id)}}">
                            {{csrF_field()}}
                          <label>Package Date</label>
                          <div class="row">
                            <div class="col-md-4" >
                        <div class="form-group">  
                    <div class="input-group ">
                      <input required="" type="text" class="form-control occupied_date_from input-sm" value="{{$edit->check_in}}" name="package_date" id="package_date" readonly="" required="">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                    </div>
                </div>
            </div>
        </div>
                            <label>Package List</label> 
                            <div class="row">

                            <div class="col-md-4">
                            
           
                        <select class = 'form-control input-sm searchable select2' name="package" id="" required="">
                        <option value="">Select Package</option>
                        @foreach($package as $pck)
                        <option value="{{$pck->attraction_id}}">{{$pck->attraction_name}}</option>
                        @endforeach 
                        </select>
                        <input type="hidden" name="reservation_id" value="{{$res_id}}">
                       </div>
                          </div>
                          <br>

                            <label>Number of person</label> 
                            <div class="row">

                            <div class="col-md-4">
                            
            <div class="input-group ">
                      <input required="" type="number" class="form-control occupied_date_from input-sm" value="{{$edit->check_in}}" name="number_of_person" required="">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-users"></i></a>
                                </div>
                    </div>
                       </div>
                          </div>
                          <br>
               
        <button class="btn btn-primary" type="submit">Add package</button>
<div class="row">
                        <div class="col-md-3">
                    
                     </div>
                       
                    
                </div>
            </div>


                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection


@section('body_bottom')
  <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

 <script type="text/javascript">
    $(function() {
            $('#package_date').datepicker({
                dateFormat: 'yy-m-d',
                sideBySide: true,
                minDate : 0,
                maxDate:  new Date('{{$check_out}}')
                });
        });
    $(document).ready(function(){
      $('.searchable').select2();
    });

</script>
@endsection


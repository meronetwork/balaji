@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<style>
 .res-form select { width:200px !important; }
.res-form label {
    font-weight: 600 !important;
}

 .intl-tel-input { width: 100%; }
 .intl-tel-input .iti-flag .arrow {border: none;}
.ui-datepicker{
  z-index: 10000 !important;
}


</style>

<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>
<link rel="stylesheet" type="text/css" href="/bootstrap-iso.css">
<section class="content-heading" style="margin-top: -35px; margin-bottom: 30px">

  <h1> Reservation Card</h1>
    <h4>
        Res # {{env('RES_CODE')}}{{ $edit->id }}

        <?php

              $total_foilo_amount = \App\Models\Folio::where('reservation_id',$edit->id)->sum('total_amount');

              $total_paid_amount = \App\Models\FolioPayment::where('reservation_id',$edit->id)->sum('amount');

              $checkFolio = \App\Models\Folio::where('reservation_id',$edit->id)->first();
            ?>

         To pay: NPR {{ number_format($total_foilo_amount - $total_paid_amount,2 )}}. Total : NPR {{ number_format($total_foilo_amount,2) }}.

        <span class="pull-right">


                 @if($edit->reservation_status != '3' && $edit->reservation_status != '6' )
                    <a class="btn btn-xs btn-success spinning-form submit_link"
                    onclick='checkinfromedit()' href="javascript::void()">
                     <i class="fa fa-calendar"></i> Check In/ Walk In</a>
                     @endif


             <a href="/admin/hotel/reservation/generateConfirmPDF/{{$edit->id}}">
                <button type="button" class="btn btn-xs bg-black">
                    <i class="fa fa-file-pdf-o"></i> Confirmation
                </button>
            </a>

            <a href="/admin/hotel/reservation/generateGRCardPDF/{{$edit->id}}">
                <button type="button" class="btn btn-xs bg-black">
                    <i class="fa fa-file-pdf-o"></i> Registration
                </button>
            </a>

            @if($edit->reservation_status == '9')
            VOID RESERVATION : {{$edit->void_reason}}
            @elseif($edit->reservation_status == '3')


            @if($checkFolio)
            <a href="{{ route('admin.invoice.print_estimate',$edit->id) }}">
                <button type="button" class="btn btn-xs bg-black">
                    <i class="fa fa-print"></i> Estimate
                </button>
            </a>
            @endif

            <a class="btn btn-warning btn-xs" id="" type="submit" href="{{route('admin.hotel.checkouterror.modal',$edit->id)}}" data-toggle="modal" data-target="#modal_dialog"> <i class="fa fa-sign-out"></i> Check Out</a>

            <a href="/admin/reservation/folio/{{$edit->id}}/index" data-toggle="tooltip" data-original-title="Post Additional Charges, Transfer or Split Folio" title="Post Additional Charges, Transfer or Split Folio">
              <button class="btn btn-success btn-xs" id="" type="submit"> <i class="fa fa-file-text-o"></i> Folio </button></a>

            <a class="btn btn-default btn-xs" id="btn-submit-edit" type="submit" href="/admin/reservation/{{$edit->id}}/move_room/"> <i class="fa fa-tv"></i> Room move</a>


            <a class="btn btn-default btn-xs" href="{{route('admin.hotel.extend-stay',$edit->id)}}"> <i class="fa fa-bed"></i> Extend Stay </a>

            <div class="btn-group">
                <div class="btn-group">
                    <button type="button" data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle">Options <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-tasks"></i> Tasks</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-exclamation-triangle"></i>Staff Logs </a> </li>
                    </ul>
                </div>
            </div>



            <a class="btn bg-black btn-xs" href="/admin/payment/reservation/{{$edit->id}}/deposit" title="Create Deposit" target="blank">
                <i class="fa fa-money"></i> Deposit
            </a>

            <!--   <a href="/admin/roomguest/pos/{{$edit->id}}" class="btn btn-default btn-xs" id="btn-submit-edit" type="submit" >  + POS</a> -->

            @elseif($edit->reservation_status == 6)

            <span style="font-size: 14px;"> Checked out by {{ucfirst(trans($edit->user->username))}} </span>

            <a href="/admin/reservation/folio/{{$edit->id}}/index"><button class="btn btn-success btn-xs" id="" type="submit"> <i class="fa fa-file-text-o"></i> Folio </button></a>
            <a href="/admin/payment/folio/{{ $edit->folio->id }}/index"><button class="btn btn-success btn-xs" id="" type="submit"> <i class="fa fa-money"></i> Payments </button></a>
            @else
            <a class="btn btn-default btn-xs" href="#"><i class="fa fa-exclamation"></i> Logs </a>

            <a class="btn btn-default btn-xs" href="/admin/payment/reservation/{{$edit->id}}/deposit" title="Create Deposit" target="_blank">
                <i class="fa fa-money"></i> Deposit
            </a>

            @endif
        </span>
    </h4>
</section>
<div class="bootstrap-iso res-form" style="min-height: 1416.81px;clear: both;">
<form method="post" action="{{route('admin.hotel.reservation-edit',$edit->id)}}"
enctype="multipart/form-data" id="editReservation" onsubmit="return CheckCheckInCheckout(this);" class="spinning-form">
    {{ csrf_field() }}

    <input type="hidden" value="{{$edit->room_num}}" name="currentroom[]">
   <!-- Main content -->
   <div class="row">
      <div class="col-md-8">
         <div class="card card-primary">
            <div class="card-header text-white bg-info" style="">
               <h3 class="card-title"><i class="fa fa-user"></i> Guest Information</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label>  Title </label>
                     <select name="title" class="form-control searchable select2 input-sm" id="title">
                        <option value="Mr" @if($edit->title=='Mr') selected="" @endif>Mr</option>
                        <option value="Mrs" @if($edit->title=='Mrs') selected="" @endif>Mrs</option>
                        <option value="Miss" @if($edit->title=='Miss') selected="" @endif>Ms</option>
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label>  Guest Name <small> <a title="Create New Guest" href="#" onClick='openwindowguest()'> [+] <i class="fa fa-external-link"></i> </a></small> </label>
                     <input style=" text-transform: uppercase" type="text" name="guest_name" placeholder="guest name.." value="{{ old('guest_name') ?? $edit->guest_name }}" class="form-control input-sm res_guests" id="res_guests" required="">
                  </div>
                   <input type="hidden" name="guest_id" value="{{$edit->guest_id}}">
                  <div class="form-group col-sm-4" id=''>
                     <label>Guest Type</label>
                     {!! Form::select('guest_type',['TOU'=>'Tourist','BUS'=>'Business Travellers','FAM'=>'Families','STA'=>'Hotel Staffs','DEl'=>'Delegates','VIP'=>'VIP','COR'=>'Corporate','GOV'=>'Government','FIT'=>"FIT",'DIP'=>"Diplomats",'COM'=>"Complementry"], old('guest_type') ?? $edit->guest_type , ['class' => 'form-control searchable select2 input-sm','disabled'=> $edit->folio ? true : false ] )!!}
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label for="inputEmail3" class="col-sm-6 control-label">
                     Agent <a title="Create New Agent" href="#" onclick="openwindowagent()">[+]</a>
                     </label>
                     <select name="agent_id" class="form-control searchable select2 input-sm">
                        <option value="">Select Agent</option>
                        @foreach($agent as $ag)
                        <option value="{{$ag->id}}" @if($edit->agent_id == $ag->id) selected @endif >{{$ag->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label>Source</label>
                     <select name="source_id" class="form-control searchable select2 input-sm" required>
                        <option value="">Select Source</option>
                        @foreach($sources as $ag)
                        <option value="{{$ag->id}}" @if($edit->source_id  == $ag->id) selected @endif>{{$ag->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group col-sm-4">
                     <label>
                     Company <a title="Create New Company" href="#" onclick="openwindowcompany()">[+]</a>
                     </label>
                     <select name="company_id" class="form-control  searchable  input-sm">
                        <option value="">Select Company</option>
                        @foreach($client as $cl)
                        <option value="{{$cl->id}}" @if($edit->company_id ==$cl->id) selected @endif>{{$cl->name}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-info">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-calendar"></i> Dates and Availability</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label >Check in </label>
                      <input required="" type="text" class="form-control occupied_date_from input-sm check_in datepicker" value="{{ $edit->book_in }}" name="book_in" id="check_in" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Book out </label>
                     <input required="" type="text" class="form-control occupied_date_from input-sm datepicker" value="{{ $edit->book_out }}" name="book_out" id="check_out" readonly="">
                  </div>
                  <div class="form-group col-sm-4" id='city'>
                      <label >Check in time </label>
                     <input type="text" class="form-control occupied_date_from input-sm timepicker" value="{{ $edit->check_in_time }}" name="check_in_time" id="check_in_time">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label >Check out time </label>
                     <input type="text" class="form-control occupied_date_from input-sm timepicker" value="{{ $edit->check_out_time }}"  name="check_out_time" id="check_out_time">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Arrival time </label>
                     <input type="text" class="form-control occupied_date_from input-sm datetimepicker" value="{{ $edit->arrival_time }}" name="arrival_time" id="arrival_time">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Arrival <small>Airlines/Bus</small></label>
                     <input type="text" class="form-control occupied_date_from input-sm"
                     value="{{ $edit->flight_no }}" name="flight_no">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                      <label> Arrival <small>Flight/Bus time</small></label>
                     <input type="text" class="form-control occupied_date_from input-sm timepicker" value="{{ $edit->arrival_flight_time }}" name="arrival_flight_time" id="arrival_flight_time">
                  </div>

                  <div class="form-group col-sm-4">
                     <label >Occupancy <small> Adult</small></label>
                      <input type="number" min="1" max="5" class="form-control input-sm"
                      value="{{ $edit->occupancy }}" name="occupancy" id="occupancy" required="">
                  </div>
                  <div class="form-group col-sm-4">
                     <label >Occupancy <small> Child</small></label>
                    <input type="number" min="0" max="5" class="form-control input-sm"
                    value="{{ $edit->occupancy_child }}" name="occupancy_child" id="occupancy_child">
                  </div>
                   <div class="form-group col-sm-4">
                     <label >Booked By <a title="New Contact" href="#" onclick="openwindowcontacts()">[+]</a></label>
                     <input type="text" class="form-control input-sm" placeholder="search contact"  value="{{ $edit->booked_by }}"  id="booked_by" name="booked_by">
                  </div>
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-bed"></i> Stay Information</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                    <label class="control-label">Rooms (Group Book)</label>
                    <input type="number" min="1" max="20" name="num_of_rooms" placeholder="" id="num_of_rooms" value="{{ $edit->num_of_rooms }}" class="form-control input-sm"
                    @if($checkFolio) readonly="" @endif>
                  </div>
                  <div class="form-group col-sm-4">
                     <label for="inputEmail3" class="ccontrol-label ">Room</label>
                     <select name="room_num" class="form-control searchable select2 input-sm"
                     id="room_num" onchange="getroomplanforparent(this)">

                      @if($edit->reservation_status == '2')
                        <option value="">Room</option>
                      @foreach($room as $em)
                      <option value="{{$em['room_number']}}" @if($edit->room_num  == $em['room_number'] ) selected="" @endif  >{{ucfirst(trans($em['room_number']))}} ({{$em['room_name']}})</option>
                      @endforeach
                      @else
                        <option value="{{$edit->room_num}}">{{$edit->room_num}} ({{ucfirst($edit->room->room_type->room_name)}})</option>
                      @endif
                        </select>
                  </div>
                  <div class="form-group col-sm-4" id='city'>
                    <label  class="control-label">Rate Plan</label>
                     <select name="rate_id" class="form-control searchable select2 input-sm"
                     id="room_plan" >
                        @foreach($rateplan as $rp)
                                <option value="{{$rp->id}}" @if($rp->id == $edit->rate_id) selected @endif>
                                    {{$rp->package_name}}({{ (float)$rp->rate + (float) $rp->breakfast_rate + (float) $rp->lunch_rate + (float) $rp->dinner_rate}})
                                </option>
                                @endforeach
                      </select>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                    <label class="control-label">Res. type</label>
                    <select name="reservation_type" class="form-control searchable select2 input-sm">
                      <option value="{{$rtype->id}}">{{$rtype->name}}</option>
                  </select>
                  </div>
                  <div class="form-group col-sm-4">
                   <label  class="control-label ">Days</label>
                    <input type="number" name="number_of_days" placeholder="days.." id="number_of_days" value="1" class="form-control input-sm" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                      <label >Room Rate</label>
                      <input type="text" class="form-control input-sm" value="{{$edit->room_rate}}" name="room_rate" id="booked_by">
                  </div>
                  <div class="form-group col-sm-4">
                      <label > Smoking</label>
                      <select name="smoking" class="form-control searchable select2">
                        <option value="0">No</option>
                        <option value="1" @if($edit->smoking ==  '1') selected="" @endif >Yes</option>
                      </select>
                  </div>
               </div>

            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-money"></i> Billing Information</h3>
            </div>
            <div class="card-body" style="display: block;">
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label  class="control-label ">Payment Type</label>
                     <select name="payment_type_id" class="form-control searchable select2 input-sm" id="payment_type_id" required="required">
                       <option value="">Select Payment Type</option>
                        @foreach($paymenttype as $pt)
                        <option value="{{$pt->payment_type_id}}" @if($edit->payment_type_id == $pt->payment_type_id) selected @endif)>{{ $pt->payment_name }}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group col-sm-4">
                      <label class="control-label">Card No</label>
                    <input type="text" name="cc_no" placeholder="Credit Card No" id="cc_no" value="{{$edit->cc_no}}" class="form-control input-sm">
                  </div>
                  <div class="form-group col-sm-4" id='city'>
                    <label class="control-label">Card Expiry Date</label>
                 <input type="text" name="cc_exp_date" placeholder="Expiry date" id="cc_exp_date" value="{{$edit->cc_exp_date}}" class="form-control input-sm datepicker">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-sm-4">
                     <label class="control-label">Name On Card</label>
                     <input type="text" name="name_on_card" placeholder="Name On Card" id="name_on_card" value="{{$edit->name_on_card}}" class="form-control input-sm">
                  </div>
                  <div class="form-group col-sm-4">
                     <label class="control-label">Bill To</label>
                     {!! Form::select('bill_to',['company'=>'Company','group'=>'Group Owner','guest'=>'Guest','companyandguest'=>'Room and Tax to Company Extra to Guest','com'=>"Complementry",'groupindividual'=>'Group Individual'],$edit->bill_to, ['class' => 'form-control searchable select2 input-sm'] )!!}
                  </div>
                  <div class="form-group col-sm-4" style="margin-top: 23px;">

                     <label class="control-label">Card Swiped&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <input type="hidden" name="card_swiped" value="0">
                      <input type="checkbox" name="card_swiped" value="1" class="form-check-input"
                      @if($edit->card_swiped == 1) checked="" @endif>
                    </label>
                  </div>
               </div>

            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Remarks</h3>
            </div>
            <div class="card-body" style="display: block;margin: 0;">

               <div class="form-group">
                        <div class="input-group ">
                            <textarea type="text" name="remarks" placeholder="Write reservation remarks" id="remarks" class="form-control" rows="10" >{{$edit->remarks}}</textarea>

                        </div>
                    </div>

            </div>
            <!-- /.card-body -->
         </div>


         <!-- /.card -->
         <hr>



      </div>
      <div class="col-md-4">
         <div class="card card-secondary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-phone-square"></i> Contact Info</h3>
            </div>
            <div class="card-body">
               <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" placeholder="Email.." size="6" value="{{old('email') ?? $edit->email }}" class="form-control input-sm" required>
               </div>
               <div class="form-group">
                  <label >Phone</label>
                  <input type="number" name="phone" placeholder="Phone Number.." size="6" value="{{old('phone') ?? $edit->phone}}" class="form-control input-sm">
               </div>
               <div class="form-group">
                  <label >Mobile</label>
                  <input type="number" name="mobile" placeholder="Mobile Number.." size="6" value="{{old('mobile') ?? $edit->mobile }}" class="form-control input-sm">
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
         <br/>
         <div class="card card-secondary">
            <div class="card-header">
               <h3 class="card-title"><i class="fa fa-info-circle"></i> Other Information</h3>
            </div>
            <div class="card-body">
               <div class="form-group">
                <label class="control-label">Nationality</label>
                <div class="input-group ">
                    {!! Form::select('nationality', ["Afghanistan"=>"Afghanistan", "Albania"=>"Albania", "Algeria"=>"Algeria", "American Samoa"=>"American Samoa", "Andorra"=>"Andorra", "Angola"=>"Angola", "Anguilla"=>"Anguilla", "Antigu"=>"Antigu", "Argentina"=>"Argentina", "Armenia"=>"Armenia", "Aruba"=>"Aruba", "Australia"=>"Australia", "Austria"=>"Austria", "Azerbaijan"=>"Azerbaijan", "Bahamas"=>"Bahamas", "Bahrain"=>"Bahrain", "Bangladesh"=>"Bangladesh", "Barbados"=>"Barbados", "Belarus"=>"Belarus", "Belgium"=>"Belgium", "Belize"=>"Belize", "Benin"=>"Benin", "Bermuda"=>"Bermuda", "Bhutan"=>"Bhutan", "Bolivia"=>"Bolivia", "Bosnia And Hercegovina"=>"Bosnia And Hercegovina", "Botswana"=>"Botswana", "Brazil"=>"Brazil", "British Virgin Islands"=>"British Virgin Islands", "Brunei"=>"Brunei", "Bulgaria"=>"Bulgaria", "Burkina Faso"=>"Burkina Faso", "Burundi"=>"Burundi", "Cambodia"=>"Cambodia", "Cameroon"=>"Cameroon", "Canada"=>"Canada", "Capeverde"=>"Capeverde", "Cayman Islands"=>"Cayman Islands", "Central African Republic"=>"Central African Republic", "Chad"=>"Chad", "Chile"=>"Chile", "China"=>"China", "Colombia"=>"Colombia", "Comoros"=>"Comoros", "Congo"=>"Congo", "Costa Rica"=>"Costa Rica", "Croatia"=>"Croatia", "Cuba"=>"Cuba", "Cyprus"=>"Cyprus", "Czech Republic"=>"Czech Republic", "Denmark"=>"Denmark", "Djibouti"=>"Djibouti", "Dominca"=>"Dominca", "Dominican Republic"=>"Dominican Republic", "Ecuador"=>"Ecuador", "Egypt"=>"Egypt", "El Salvador"=>"El Salvador", "Equatorial Guinea"=>"Equatorial Guinea", "Eritrea"=>"Eritrea", "Estonia"=>"Estonia", "Ethiopia"=>"Ethiopia", "Falkland Islands"=>"Falkland Islands", "Fiji"=>"Fiji", "Finland"=>"Finland", "France"=>"France", "Gabon"=>"Gabon", "Gambia"=>"Gambia", "Georgia"=>"Georgia", "Germany"=>"Germany", "Ghana"=>"Ghana", "Greece"=>"Greece", "Greenland"=>"Greenland", "Grenada"=>"Grenada", "Guam"=>"Guam", "Guatemala"=>"Guatemala", "Guinea"=>"Guinea", "Guinea-bissau"=>"Guinea-bissau", "Guyana"=>"Guyana", "Haiti"=>"Haiti", "Honduras"=>"Honduras", "Hungary"=>"Hungary", "Iceland"=>"Iceland", "India"=>"India", "Indonesia"=>"Indonesia", "Iran"=>"Iran", "Iraq"=>"Iraq", "Ireland"=>"Ireland", "Israel"=>"Israel", "Italy"=>"Italy", "Jamaica"=>"Jamaica", "Japan"=>"Japan", "Jordan"=>"Jordan", "Kazakhstan"=>"Kazakhstan", "Kenya"=>"Kenya", "Kiribati"=>"Kiribati", "Kuwait"=>"Kuwait", "Laos"=>"Laos", "Latvia"=>"Latvia", "Lebanon"=>"Lebanon", "Lesotho"=>"Lesotho", "Liberia"=>"Liberia", "Libya"=>"Libya", "Liechtenstein"=>"Liechtenstein", "Lithuania"=>"Lithuania", "Luxembourg"=>"Luxembourg", "Macedonia"=>"Macedonia", "Madagascar"=>"Madagascar", "Malawi"=>"Malawi", "Malaysia"=>"Malaysia", "Maldives"=>"Maldives", "Mali"=>"Mali", "Malta"=>"Malta", "Marshall Islands"=>"Marshall Islands", "Mauritania"=>"Mauritania", "Mauritius"=>"Mauritius", "Mexico"=>"Mexico", "Micronesia"=>"Micronesia", "Moldova"=>"Moldova", "Monaco"=>"Monaco", "Mongolia"=>"Mongolia", "Morocco"=>"Morocco", "Mozambique"=>"Mozambique", "Myanmar"=>"Myanmar", "Namibia"=>"Namibia", "Nauru"=>"Nauru", "Nepal"=>"Nepal", "Netherlands"=>"Netherlands", "New Zealand"=>"New Zealand", "Nicaragua"=>"Nicaragua", "Niger"=>"Niger", "Nigeria"=>"Nigeria", "North Korea"=>"North Korea", "Norway"=>"Norway", "Oman"=>"Oman", "Pakistan"=>"Pakistan", "Palau"=>"Palau", "Panama"=>"Panama", "Papua New Guinea"=>"Papua New Guinea", "Paraguay"=>"Paraguay", "Peru"=>"Peru", "Philippines" => "Philippines", "Poland"=>"Poland", "Portugal"=>"Portugal", "Qatar"=>"Qatar", "Romania"=>"Romania", "Russia"=>"Russia", "Rwanda"=>"Rwanda", "San Marino"=>"San Marino", "Sao Tome And Principe"=>"Sao Tome And Principe", "Saudi Arabia"=>"Saudi Arabia", "Senegal"=>"Senegal", "Serbia"=>"Serbia", "Seychelles"=>"Seychelles", "Sierra Leone"=>"Sierra Leone", "Singapore"=>"Singapore", "Slovakia"=>"Slovakia", "Slovenia"=>"Slovenia", "Solomon Islands"=>"Solomon Islands", "Somalia"=>'Somalia', "outh Africa"=>"South Africa", "South Korea"=>"South Korea", "Spain"=>"Spain", "Sri Lanka"=>"Sri Lanka", "Sudan"=>"Sudan", "Suriname"=>"Suriname", "Swaziland"=>"Swaziland", "Sweden"=>"Sweden", "Switzerland"=>"Switzerland", "Syria"=>"Syria", "Taiwan"=>"Taiwan", "Tajikistan"=>"Tajikistan", "Tanzania"=>"Tanzania", "Thailand"=>"Thailand", "Togo"=>"Togo", "Tonga"=>"Tonga", "Trinidad And Tobago"=>"Trinidad And Tobago", "Tunisia"=>"Tunisia", "Turkey"=>"Turkey", "Turkmenistan"=>"Turkmenistan", "Tuvalu"=>"Tuvalu", "Uganda"=>"Uganda", "Ukraine"=>"Ukraine", "United Arab Emirates"=>"United Arab Emirates", "United Kingdom"=>"United Kingdom", "United States Of America"=>"United States Of America", "Uruguay"=>"Uruguay", "Uzbekistan"=>"Uzbekistan", "Vanuatu"=>"Vanuatu", "Venezuela"=>"Venezuela", "Viet Nam"=>"Viet Nam", "Zambia"=>"Zambia", "Zimbabwe"=>"Zimbabwe"],$edit->nationality, ['class' => 'form-control searchable label-default']) !!}

                </div>
            </div>
               <div class="form-group">
                  <label  class="control-label">Gender</label>
                  <select name="gender" class="form-control   input-sm w-100">
                      <option value="M">Male</option>
                      <option value="F" @if($edit->gender == 'F') selected="" @endif>Female</option>
                  </select>
               </div>
               <div class="form-group">
                  <label class="control-label">Doc Type</label>
                  <input type="text" name="doc_type_extra" placeholder="Doc type.." size="6"
                  value="{{ $edit->doc_type_extra }}" class="form-control input-sm">
               </div>
               <div class="form-group">
                  <label class="control-label">Doc. Number</label>
                  <input type="text" name="doc_num_extra" placeholder="Doc number.." size="6"
                  value="{{ $edit->doc_num_extra }}" class="form-control input-sm">
               </div>
            </div>
            <!-- /.card-body -->
         </div>

      </div>
   </div>

     <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">More Guest info</h3>
            </div>
         <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <th>S.N</th>
                            <th>Room</th>
                            <th>Rate Plan</th>
                            <th>Room Rate</th>
                            <th>Adult</th>
                            <th>Child</th>
                            <th>Guest Name</th>
                            <th>Doc Type</th>
                            <th>Doc Num.</th>
                            <th> Action </th>
                        </tr>

                        <tbody id='addmorerooms'>
                            <tr class="addmorerooms">

                            </tr>
                             @foreach($extra_reservation as $key=>$er)
                            <tr>
                                <input type="hidden" name="child_res_id[]" value="{{$er->id}}">
                                <td>{{$key + 1}}.{{$er->id  }}</td>
                                <td>
                                  @if($edit->reservation_status == 2)
                              <select name="extra_room_num[]" class="form-control room_num searchable" onchange="getroomplanforchild(this)" required="">
                                <option value="">Select room</option>
                                @foreach($room as $rm)
                                <option value="{{$rm['room_number']}}" @if($er->room_num == $rm['room_number']) selected="" @endif >{{ucfirst(trans($rm['room_number']))}} ({{$rm['room_name']}})</option>
                                @endforeach
                                </select>
                                  @else
                                    <label class="control-label">{{$er->room->room_type->room_name}} {{ $er->room->room_number }}</label>
                                  @endif
                                </td>
                                <td>
                                    <?php
                                    $extrarateplan = \App\Models\Rateplan::orderBy('id','desc')->where('roomtype_id',$er->room->roomtype_id)->get();
                                  ?>
                                    <select name="extra_rate_plan[]" class="searchable select2">
                                        <option value="">Select Rate Plan</option>
                                        @foreach($extrarateplan as $rp)
                                        <option value="{{$rp->id}}" @if($er->rate_id == $rp->id) selected @endif>
                                            {{$rp->package_name}}
                                            ({{ (float) $rp->rate + (float) $rp->breakfast_rate + (float) $rp->lunch_rate + (float) $rp->dinner_rate}})
                                        </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="number" name="extra_room_rate[]" class="form-control" value="{{$er->room_rate}}">
                                </td>
                                <td>
                                    <input name="extra_no_of_adult[]" class="form-control input-sm" placeholder="No. of Adults" value="{{$er->occupancy}}" required="">
                                </td>
                                <td>
                                    <input name="extra_no_of_child[]" class="form-control input-sm" placeholder="No. of Child" value="{{$er->occupancy_child}}">
                                </td>
                                <td>
                                    <input name="extra_guest_name[]" class="form-control res_guests-additional input-sm" placeholder="Guest Name" value="{{$er->guest_name}}" >
                                </td>
                                <td>
                                    <input name="extra_doc_type[]" class="form-control input-sm" placeholder="Document type" value="{{$er->doc_type_extra}}">
                                </td>
                                <td>
                                    <input name="extra_doc_num[]" class="form-control input-sm" placeholder="Document Number" value="{{$er->doc_num_extra}}">
                                </td>
                                <td>
                                  <a href="/admin/reservation/split_res?res_id={{ $er->id }}"
                                    onclick="return confirm('Are you sure')" class="btn btn-sm btn-primary submit_link">Split</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
          </div>
<br>
</form>
<div class="card card-primary">
   <div class="card-header">
      <h3 class="card-title">Document Information</h3>
   </div>
   <div class="card-body" style="display: block;margin: 0;">
      <form method="post" enctype="multipart/form-data"
         action="/admin/hotel/reservation/add-file/{{$edit->id}}">
         {{ csrf_field() }}
         <a href="javascript::void(1)" class="btn btn-danger btn-xs" id="addDoc" style="float: left;">
         <i class="fa fa-plus"></i> <span>Add Documents</span>
         </a>
         <table class="table" style="margin-top: 30px;">
            <tbody>
               <tr class="multipleDivDocs">
               <tr>
                  <td class="moreroomtd">
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label ">
                        Add Document
                        </label>
                     </div>
                  </td>
                  <td>
                     <input type="file" name="moredocuments">
                  </td>
                  <td>
                     <button class="btn btn-primary btn-xs" type="submit">Upload</button>
                  </td>
               </tr>
               @foreach($moredoc as $doc)
               <tr>
                  <td class="moreroomtd">
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label ">
                        Current Document
                        </label>
                     </div>
                  </td>
                  <td>
                     <a href="{{$doc->doc}}" target="blank">{{substr($doc->doc,13)}}</a>
                     <br>
                     <i class="date">{!! \Carbon\Carbon::createFromTimeStamp(strtotime($doc->created_at))->diffForHumans().' by '.$doc->user->first_name !!}</i>
                  </td>
                  <td>
                     <a data-target="#modal_dialog" data-toggle="modal" href="/admin/hotel/reservation/confirm-delete-file/{{$doc->id}}" style="width: 10%;">
                     <i class="btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                     </a>
                  </td>
               </tr>
               @endforeach
               </tr>
            </tbody>
         </table>
      </form>
   </div>
   <!-- /.card-body -->
</div><br/>
  <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button class="btn btn-primary" id="btn-submit-edit" type="submit"
                    form="editReservation">Update Reservation </button>

                    @if(!in_array($edit->reservation_status, ['11']))

                    @if($edit->reservation_status == '9')
                    <a class="btn btn-default" id="btn-submit-edit" type="submit">Marked Void</a>
                    @elseif($edit->reservation_status != '3' && $edit->reservation_status != '6' )
                    <a class="btn btn-success spinning-form submit_link"
                    onclick='checkinfromedit()' href="javascript::void()">
                     <i class="fa fa-calendar"></i> Check In/ Walk In</a>

                    <a href="/admin/hotel/reservation/{{$edit->id}}/cancellation" class='btn btn-danger'>No Show/ Cancellation</a>
                    @endif


                    <a class="btn btn-warning" id="btn-submit-edit" type="submit" href="{{route('admin.reservation.void.confirm',$edit->id)}}" data-toggle="modal" data-target="#modal_dialog">Void</a>
                       @endif
                    <a href="{!! route('admin.hotel.reservation-index') !!}" class='btn btn-info'>Close</a>
                </div>
            </div>
        </div>

   <!-- /.content -->
</div>
<div id='more_room_guest_toadd' style="display: none;">
    <table class="table">
        <tbody id='more-tr-room'>
            <tr class="additionroomtd">
                <td class="morerooms-sn"></td>
                <td>
                    <select name="extra_room_num_new[]" class="form-control room_num " onchange="getroomplanforchild(this)" required="">
                        <option value="">Select room</option>
                        @foreach($room as $rm)
                        <option value="{{$rm['room_number']}}">{{ucfirst(trans($rm['room_number']))}} ({{$rm['room_name']}})</option>
                        @endforeach
                    </select>
                </td>
                <td style="min-width: 100px">
                    <select name="extra_rate_plan_new[]" class="form-control room_plan " required="" onchange="changerateplan(this,'child')">
                        <option value="">Select Rate Plan</option>
                    </select>
                </td>
                <td>
                    <input type="number" class="form-control room_rate_value" name="extra_room_rate_new[]" placeholder="Room rate">
                </td>
                <td>
                    <input name="extra_no_of_adult_new[]" class="form-control" placeholder="No. of Adults">
                </td>
                <td>
                    <input name="extra_no_of_child_new[]" class="form-control" placeholder="No. of Child">
                </td>
                <td>
                    <input name="extra_guest_name_new[]" class="form-control res_guests-additional" placeholder="Guest Name">
                    <input type="hidden" name="extra_guest_id_new[]" class="moreguest_id">
                </td>
                <td>
                    <input name="extra_doc_type_new[]" class="form-control" placeholder="Document type">
                </td>
                <td>
                    <input name="extra_doc_num_new[]" class="form-control" placeholder="Document Number">
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div id="addmoredocs" style="display: none;">
    <table class="table">
        <tbody id="more-tr">
            <tr>
                <td class="moreroomtd">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">
                            Document
                        </label>
                        <div class="col-md-6">
                            <input type="file" name="doc[]">
                        </div>
                    </div>
                </td>
                <td>
                    <a href="javascript::void(1);" style="width: 10%;">
                        <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection


@section('body_bottom')
 @section('body_bottom')
<?php $today = date('Y-m-d');
$today = json_encode($today); ?>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $("#addRoom").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDivRoom").after($('#addmoreroom #more-tr').html());

    });


    $("#addDoc").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDivDocs").after($('#addmoredocs #more-tr').html());
    });
    $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();
    });
    $(function() {
        $('.datepicker').datepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            dateFormat: 'yy-mm-dd'
            , sideBySide: true
            , minDate: 0
        });

        $('.timepicker').datetimepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            format: 'HH:mm'
            , sideBySide: true
        });

        $('.datetimepicker').datetimepicker({
            //inline: true,
            //format: 'YYYY-MM-DD',
            format: 'YYYY-MM-DD HH:mm'
            , sideBySide: true

        });

    });

    function getroom(date) {
        $('.room_plan').each(function() {
            $(this).empty();
        });
        var roomoption = "<option value=''>Select room </option>";
        $.get('/admin/hotel/reservation/checkdate/' + date, function(data, status) {
            for (let room of data) {
                let option = `<option value='${room.room_number}'>${room.room_number}(${room.room_name})`;
                roomoption = roomoption + option;
            }
            $('.room_num').each(function() {
                $(this).html(roomoption);
            });
        });
    }

    $("#check_in,#check_out").on("change", function(event) {

        var check_in = $('#check_in').val();
        var check_out = $('#check_out').val();
        var date1 = new Date(check_in);
        var date2 = new Date(check_out);
        var diffTime = Math.abs(date2 - date1);
        var days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        if (days == 0) days = 1;
        if (!isNaN(days)) {
            $('#number_of_days').val(days);
        }
        let target = event.target.id;
        if (target == 'check_in') {
            getroom(check_in);
        }
    });

    function disableroom() {
        $('.room_num').children().prop('disabled', false);
        $('.room_num').each(function() {
            var val = $(this).val();
            if (val === '') return;
            $('.room_num').not(this).children('[value="' + val + '"]').prop('disabled', true);
        });
    }

    function getroomplanforparent(ev) {
        $('.searchable').select2('destroy');
        $('#room_plan').empty();
        disableroom();
        let option = roomplanapi(ev.value).then(function(result) {

            $('#room_plan').html(result);
        });

        $('.searchable').select2();
    }

    function getroomplanforchild(ev) {
        $('.searchable').select2('destroy');
        var parent = $(ev).parent().parent();
        var roomplan = parent.find('.room_plan');
        roomplan.empty();
        disableroom();
        let option = roomplanapi(ev.value).then(function(result) {
            $(roomplan).html(result);
        });
        $('.searchable').select2();
    }

    //changing rate plan
    function roomplanapi(id) {
        return new Promise(function(resolve, reject) {
            var roomplanoptions = "<option value=''>Select Rate Plan</option>";
            $.get('/admin/hotel/reservation-getroom-plan/' + id, function(data, status) {
                for (let plan of data) {
                    let total = Number(plan.rate) + Number(plan.breakfast_rate) + Number(plan.lunch_rate) + Number(plan.dinner_rate);
                    roomplanoptions = roomplanoptions + "<option value=" + JSON.stringify({
                        id: plan.id
                        , value: total
                    }) + ">" + plan.package_name + "(" + total + ")</option>";
                }
                return resolve(roomplanoptions);
            });
        });
    }

    function changerateplan(ev, type) {
        var value = JSON.parse($(ev).val());
        if (type == 'parent') {
            $('#parent_room_rate').val(value.value);

        } else { //type is child
            var parent = $(ev).parent().parent();
            parent.find('.room_rate_value').val(value.value);
        }
    }


    $('#num_of_rooms').change(function() {
        if ($(this).val() > 20) {
            $(this).val(20);
            alert('Room cannot be extended more than 20');

        }
        $('.searchable').select2('destroy');
        var numberof_roomtoadd = Number($(this).val()) - 1;
        let totalmorerooms = $('#addmorerooms .additionroomtd').length;
        let toadded_or_to_remove = numberof_roomtoadd - Number(totalmorerooms);
        console.log(numberof_roomtoadd, totalmorerooms, toadded_or_to_remove);
        if (toadded_or_to_remove > 0) {
            for (let i = 0; i < toadded_or_to_remove; i++) {
                $("#addmorerooms").append($('#more_room_guest_toadd #more-tr-room').html());
            }
            $('.morerooms-sn').each(function(index) {
                $(this).text((index + 1) + '.');
            });
            $('.res_guests-additional').each(function() {
                var parent = $(this).parent().parent();
                $(this).autocomplete({
                    source: "/admin/hotel/guests/ajaxgetGuest"
                    , minLength: 1
                    , change: function(event, ui) {
                        parent.find('.moreguest_id').val(ui.item.id);
                    }
                });
            });
        } else {
            $('#addmorerooms > .additionroomtd').slice(toadded_or_to_remove).remove();
            disableroom();
        }
        $('.searchable').select2();

    });

    $(document).ready(function() {
        $('#complementry_by').css('visibility', 'hidden');
        $('.searchable').select2();
    });
    $('#booked_by').autocomplete({
        source: "/admin/getContacts"
        , minLength: 1
    , });



    $('.check_in_submit').on('click', function() {
            let check = $('#check_in').val();
            let p = <?php echo $today; ?>;
    if (check == p) {
        return true;
    }
    alert("Check in date should be of today");
    return false;
});


$('#guest_type_id').on('change', function() {
    if ($(this).val() != 'COM') {
        $('#complementry_by').css('visibility', 'hidden');
        $('.payment_info').show();
    } else {

        $('#complementry_by').css('visibility', 'visible');
        $('#booked_by').attr("readonly", "readonly");
        $('.payment_info').hide();
        $('#payment_type_id').removeAttr("required", "required");

    }
});





function populateGuest(guest) {
    $('.searchable').select2('destroy');
    $("input[name='email']").val(guest.email);
    $("input[name='guest_id']").val(guest.id);
    $("input[name='phone']").val(guest.landline);
    $("input[name='mobile']").val(guest.mobile);
    $("select[name='title']").val(guest.title);
    $("select[name='gender']").val(guest.gender);
    $("select[name='nationality']").val(guest.country);
    $("select[name='guest_type']").val(guest.guest_type);
    $('.searchable').select2();
}

$('.res_guests').autocomplete({
    source: "/admin/hotel/guests/ajaxgetGuest",
    minLength: 1,
    change: function(event, ui) {
        if (ui.item) {
            let guest = JSON.parse(ui.item.data);
            populateGuest(guest);
        } else {
            $('.searchable').select2('destroy');
            $(`input[name='guest_id'],
                  input[name='email'],
                  input[name='phone'],
                  input[name='mobile'],
                  select[name='guest_type']
                  `).val('');
            $(`select[name='title']`).val('Mr');
            $(`select[name='gender']`).val('M');
            $(`select[name='nationality']`).val('Nepal');
            $(`select[name='guest_type']`).val('TOU')
            $('.searchable').select2();

        }
        //store in session
    }
});

function openwindowguest() {
    var win = window.open('/admin/hotel/guests/modal', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function opencalendar() {
    var win = window.open('/admin/hotel/reservationcalender/modal', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=50,left=500,width=700, height=700');
}

function openwindowagent() {
    var win = window.open('/admin/clients/modals/create?relation_type=agent', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function openwindowcompany() {
    var win = window.open('/admin/clients/modals/create?relation_type=hotelcustomer', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function openwindowcontacts() {
    var win = window.open('/admin/contacts/create/modals', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650');
}

function updateform(result) {
    if (result.type == 'guest') {
        $('#res_guests').val(result.guest.first_name + ' ' + result.guest.last_name);
        populateGuest(result.guest);
    } else if (result.type == 'contacts') {
        $('#booked_by').val(result.contacts.full_name)
    } else {
        $('.searchable').select2('destroy');

        let clients = result.clients;
        var oldagent = $('select[name=agent_id]').val();
        var oldcompany = $('select[name=company_id]').val();
        var option = '';
        for (let c of clients) {
            option = option + `<option value='${c.id}'>${c.name}</option>
            `;
        }
        $('select[name=agent_id]').html(` < option value = '' > Select Agent < /option>` + option);
            $('select[name=company_id]').html(`<option value=''>Select Company</option>` + option);

            setTimeout(function() {
                if (result.relation_type == 'agent')
                    $('select[name=agent_id]').val(result.lastcreated);
                else
                    $('select[name=agent_id]').val(oldagent);

                if (result.relation_type == 'hotelcustomer')
                    $('select[name=company_id]').val(result.lastcreated);
                else
                    $('select[name=company_id]').val(oldcompany);

                $('.searchable').select2();
            });
        }
    }

    function HandlePopupResult(result) {
        if (result) {
            updateform(result);
            setTimeout(function() {
                $("#ajax_status").after("<span style='color:green;' id='status_update'>Client sucessfully created</span>");
                $('#status_update').delay(3000).fadeOut('slow');
            }, 500);
        } else {
            $("#ajax_status").after("<span style='color:red;' id='status_update'>failed to create clients</span>");
            $('#status_update').delay(3000).fadeOut('slow');
        }
    }

</script>

<script type="text/javascript">
    function CheckCheckInCheckout(theform) {

        if (document.getElementById("check_in").value > document.getElementById("check_out").value) {
            alert('Check In Should Be Smaller Than Check OUT');
            return false;
        } else {
            return true;
        }
    }



    function checkinfromedit(){

      if(!$('select#room_num').val() || !$('select#room_plan').val() ){

        alert("Please Select Room Number & Rate Plan to check in the guest To Update!!");

        return;
      }

      location.href = `/admin/reservation/checkinfromedit?res_id={!! $edit->id !!}`
    }

</script>


<!-- form submit -->
@include('partials._body_bottom_submit_lead_edit_form_js')
@endsection

@extends('layouts.master')

@section('content')

<style>
  tr td { text-align:left !important; }
</style>

    <div class='row'>
        <div class='col-md-6'>
        	<!-- Box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="">
                            <p> Transfer History for Reservation #{{ Request::segment(4) }}</p>
                            <table class="table table-hover table-bordered" id="leads-table">
                                <thead>
                                    <tr>
                                        <th>Room Number</th>
                                        <th>Previous checkout</th>
                                        <th>New checkouts</th>
                                        <th>Extended days</th>
                                        <th>Extended By</th>
                                    </tr>
                                    @if($history)
                                    @foreach($history as $k => $v)
                                    <tr>
                                        <td>{{ $v->room_num }}
                                        </td>
                                        <td>{{ $v->previous_checkout_date }} </td>
                                        <td>{{ $v->extend_checkout_date }} </td>
                                          <td>{{ $v->extend_days }} </td>
                                         <td>{{ $v->user->username }} </td>
                                    </tr>
                                    @endforeach
                                    @endif

                                </thead>
                            </table>

                          <form method="post" action="{{route('admin.hotel.extend-stay',$res_id)}}">
                            {{csrF_field()}}
                               <div class="row">
                                      
                       
                            <div class="col-md-4" >
                                 <label>Extending date</label> <i class="fa fa-calendar"></i> 
                        <div class="form-group">  
                    <div class="input-group ">
                      <input required="" type="text" class="form-control occupied_date_from input-sm" value="{{$edit->check_in}}" name="extend_checkout_date" id="check_in" readonly="" >
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                    </div>
                </div>
            </div>
                      <div class="col-md-4" >
                                 <label>Extended days</label> <i class="fa fa-calendar"></i> 
                        <div class="form-group">  
                    <div class="input-group ">
                      <input required="" type="text" class="form-control occupied_date_from input-sm" readonly="" id="extended_days" name = 'extend_days'>
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                    </div>
                </div>
            </div>
        </div>
                            <label>Transfer room to</label> <i class="fa fa-hotel (alias)"></i> 
                            <div class="row">

                            <div class="col-md-4">
                            
           
                        <select class = 'form-control input-sm' name="room_num" id="to_room" required="">
                        <option value="">Select Room</option>
                        </select>
                       </div>
                          </div>
                          <br>
               
        <button class="btn btn-primary" type="submit">Extend</button>

        <a href="/admin/folio/{{$res_id}}/create" class="btn btn-primary" target="_blank"> Create Folio </a>
        
<div class="row">
                        <div class="col-md-3">
                    
                     </div>
                       
                    
                </div>
            </div>


                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection


@section('body_bottom')
  <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

 <script type="text/javascript">
    $(function() {
            $('#check_in').datepicker({
                dateFormat: 'yy-m-d',
                sideBySide: true,
                minDate : new Date('{{$check_out1}}'),
        });
    });
        function getroom(date){  
         $('#to_room').empty();    
         $('#to_room').append("<option value=''>Select room </option>")
         $.get('/admin/hotel/reservation/checkdate/'+date,function(data,status){
          console.log(data);
          for(let room of data){
          $('#to_room').append(("<option value="+room.room_number+">"+room.room_number+"("+room.room_name+")</option>"));
          }
        });
        }

        $('#check_in').on('change',function(){
            let current_date = $(this).val();
            getroom(current_date);
           let diff1 = new Date('{{$check_out}}');
           console.log(diff1);
           let diff2 = new Date(current_date);
           let diff  = new Date(diff2 - diff1);  
           let days  = Math.ceil(diff/1000/60/60/24);  
            if(!isNaN(days)){

                $('#extended_days').val(days);
            }
        });
        
</script>
@endsection


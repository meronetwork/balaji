@extends('layouts.master')
@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
  tr td { text-align:left !important; }
</style>
    <div class='row'>
        <div class='col-md-6'>
          <!-- Box -->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="">
                            <p> Extend History for Reservation room #{{ Request::segment(4) }}</p>
                            <table class="table table-hover table-bordered" id="leads-table">
                                <thead>
                                    <tr>
                                        <th>Guest Name</th>
                                        <th>Action</th>  
                                    </tr>
                                    @if($guests)
                                    @foreach($guests as $k => $v)
                                    <tr>
                                        <td>{{ $v->guest_name }}</td>
                                        <td >&nbsp;&nbsp;&nbsp;&nbsp;<a href="{!! route('admin.hotel.room.guest.confirmdel', $v->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </thead>
                            </table>
                          <form method="post" action="{{route('admin.hotel.room.guest.store',[$res_id,$room_num])}}">
                            {{csrF_field()}}
                            <label>Add Guest</label>
                            <div class="row">
                            <div class="col-md-4">
                                <input type="text" name="guest_name" class="form-control">
                            </div>
                          </div>
                <br>
                           <button class="btn btn-primary" type="submit">Add Guest</button>
         </form>
<div class="row">
                        <div class="col-md-3">
                     </div>
                </div>
            </div>
                        </div> <!-- table-responsive -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('body_bottom')

 <script type="text/javascript">
    
     $(document).ready(function(){
            $('.searchable').select2();
        });
</script>
@endsection  
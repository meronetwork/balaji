@extends('layouts.master')
@section('content')
<style>
[data-letters]:before {
content:attr(data-letters);
display:inline-block;
font-size:1em;
width:2.5em;
height:2.5em;
line-height:2.5em;
text-align:center;
border-radius:50%;
background:red;
vertical-align:middle;
margin-right:0.3em;
color:white;
}
</style>
  <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h2>
             <a href="#" data-toggle="modal" data-target="#saveLogo">
              <small data-letters="{{mb_substr($show->room_name,0,3)}}"></small>
            </a>
           			{{$show->room_name}}
                </h2>


        </section> 
<div>
        <div class='col-md-4'>
        	    <p> <i class="fa  fa-users"></i>&nbsp;<b>Max guest:</b> 
                    <a style="color: black" >  {{ ucfirst(trans($show->max_guest)) }}</a> 
                  </p>
                   <p> <i class="fa  fa-building-o"></i>&nbsp;<b>Smoking:</b> 
                    <a style="color: black" >@if($show->smoking == 1) Yes @else No @endif</a> 
                  </p>
               
                   
        </div>
          <div class='col-md-4'>
        	    <p> <i class="fa  fa-money"></i>&nbsp;<b>Room price:</b> 
                    <a style="color: black" > {{ ucfirst(trans($show->room_priceUSD)) }} </a> 
                  </p>
                  <p> <i class="fa"></i>
                    
                  </p>

        </div>
</div>

  <div class="col-md-12">
  <textarea class="form-control" readonly="">{{$show->description}}</textarea>
  </div>

  
<div class="col-md-7" style="margin-left: -5px">
  <br>
   <div class="form-group">
                        	<a href="javascript:history.back()" class='btn btn-danger'>Close</a>
                        	
	                                <a  href="/admin/hotel/room-type-edit/{{$show->roomtype_id}}" class='btn btn-success'>Edit</a>
	                         
                        </div>
</div>
@endsection
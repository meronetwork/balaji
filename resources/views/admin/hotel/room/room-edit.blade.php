@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Room
                <small>Create a new room </small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 <form method="post" action="{{route('admin.hotel.room-edit',$edit->room_id)}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
  <h3>Basic Info</h3>
  <div class="row">

    <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              Roomtype
                              </label>
                              <div class="col-md-6">
                                <select name="roomtype_id" class="form-control">
                                  @foreach($roomtype as $rt)
                                    <option value="{{$rt->roomtype_id}}" @if($edit->roomtype_id == $rt->roomtype_id) selected @endif>{{$rt->room_name}}</option>
                                  @endforeach
                                </select>

                              </div>

                            </div>
                        </div>
     <div class="col-md-6">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-6 control-label">
                              Status
                              </label>
                              <div class="col-md-6">
                                <select name="status id" class="form-control">
                              @foreach($roomstatus as $rs)
                              <option value="{{$rs->status_id}}" @if($edit->status_id == $rs->status_id) selected @endif>{{$rs->status_name}}</option>
                              @endforeach
                              </select>

                              </div>

                            </div>
      </div>
 </div>
<br>
 <div class="row">

    <div class="col-md-6">
              <div class="form-group">  
                <label class="control-label col-sm-6">Room number</label>
                    <div class="input-group col-sm-5">
                        <input type="number" name="room_number" placeholder="Room name" id="calculated_cost" value="{{$edit->room_number}}" class="form-control" required="required">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>

  </div>
      <div class="col-md-6">
              <div class="form-group">  
                <label class="control-label col-sm-6">Floor number</label>
                    <div class="input-group col-sm-5">
                        <input type="number" name="floor_number" placeholder="Room name" id="calculated_cost" value="{{$edit->floor_number}}" class="form-control" required="required">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>

  </div>

 </div>

   <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update</button>
            <a href="{!! route('admin.hotel.room-index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
        </div>
    </div>
</div>
</div>
</div>
  @endsection
@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Room Status
        <small>Room Status Index</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>

                <b>
                    <font size="4">Room status List</font>
                </b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm" title="Import/Export Leads" href="{{ route('admin.hotel.status-create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new status</strong>
                    </a>
                </div>
            </div>
        </div>

        <table class="table table-hover table-no-border table-striped" id="leads-table">
            <thead>
                <tr>
                   
                    <th>ID</th>
                    <th>Status name</th>
                    <th>Status description</th>
                    <th>Color</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($status1 as $key=>$st)
                <tr>
                    
                    <td>{{$st->status_id}}</td>
                    <td>{{ucfirst(trans($st->status_name))}}</td>
                    <td>{{$st->status_description}}</td>
                    <td style="background-color: {{$st->status_color}}"></td>

                    <td>
                        @if( $st->isEditable())<a href="/admin/hotel/room-status-edit/{{$st->status_id}}"><i class="fa fa-edit"></i></a>
                        @else
                        <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
                        @endif
                        &nbsp;&nbsp;
                        <a href="{{route('admin.hotel.status-confirm-delete', $st->status_id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    @endsection

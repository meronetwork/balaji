@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
             Room
                <small>{{ucfirst(trans(\Request::segment(3)))}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">Room List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-info btn-sm"  title="Import/Export Leads" href="{{ route('admin.hotel.room-create') }}">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new room </strong>
                        </a>
            </div>
        </div>
</div>
<table class="table table-hover table-no-border table-striped" id="leads-table">
<thead>
    <tr class="bg-info">
       
        <th>Room Number</th>
         <th>Room name</th>
        <th>Status</th>
        
        <th>Floor number</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($room as $key=>$rm)
    <tr>
       
        
       
        <td class="badge bg-aqua" style="text-align: center; align-content: center;">{{$rm->room_number}}</td>
        <td> {{ucfirst(trans($rm->room_type->room_name))}}</td>

        @if($rm->status_id == 1)
          <td class="bg-maroon" style="text-align: center;">Availabile</td>
        @else
          <td class="bg-green" style="text-align: center;">Unavailabile</td>
        @endif
        
        <td>{{$rm->floor_number}}</td>
        <td>
            @if( $rm->isEditable())<a href="/admin/hotel/room-edit/{{$rm->room_id}}"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
            &nbsp;&nbsp;
             <a href="{{route('admin.hotel.room-confirm-delete', $rm->room_id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
        </td>

<!--   @if ( $rm->isDeletable() )
            <a href="{!! route('admin.hotel.room-confirm-delete', $rm->room_id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
        @else
            <i class="fa fa-trash-o text-muted" title="{{ trans('admin/permissions/general.error.cant-delete-perm-in-use') }}"></i>
        @endif</td>
 -->   

   </tr>

    @endforeach
</tbody>

</table>
<div style="text-align: center;"> {!! $room->render() !!} </div>
</div>
@endsection
@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Room Type
        <small>Room type Index</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>

                <b>
                    <font size="4">Room type List</font>
                </b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm" title="Import/Export Leads" href="{{ route('admin.hotel.room-type-create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add room type</strong>
                    </a>
                </div>
            </div>
        </div>

        <table class="table table-hover table-no-border table-striped" id="leads-table">
            <thead>
                <tr>
                    
                    <th>ID</th>
                    <th>Room Type Name</th>
                    <th>Room Code</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roomtype as $key=>$rt)
                <tr>
                    
                    <td>{{$rt->roomtype_id}}</td>
                    <td><a href="{{route('admin.hotel.room-type-show',$rt->roomtype_id)}}"> {{ucfirst(trans($rt->room_name))}}</a></td>
                    <td>{{$rt->type_code}}</td>

                    <td>
                        @if( $rt->isEditable())<a href="/admin/hotel/room-type-edit/{{$rt->roomtype_id}}"><i class="fa fa-edit"></i></a>
                        @else
                        <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
                        @endif
                        &nbsp;&nbsp;
                        <a href="{{route('admin.hotel.room-type-confirm-delete', $rt->roomtype_id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    @endsection

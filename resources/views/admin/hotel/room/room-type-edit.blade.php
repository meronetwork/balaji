@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Room Type
                <small>Create a new room type</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 <form method="post" action="{{route('admin.hotel.room-type-edit',$edit->roomtype_id)}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
  <h3>Basic Info</h3>
  <div class="row">
    <div class="col-md-4">
              <div class="form-group">  
                <label class="control-label col-sm-6">Room type name</label>
                    <div class="input-group ">
                        <input type="text" name="room_name" placeholder="Room name" id="calculated_cost" value="{{$edit->room_name}}" class="form-control" required="required">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>
   </div>

   <div class="col-md-4">
              <div class="form-group">  
                <label class="control-label col-sm-6">Room Type Code</label>
                    <div class="input-group ">
                        <input type="text" name="type_code" placeholder="type code" id="calculated_cost" value="{{$edit->type_code}}" class="form-control" required="required">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>

  </div>

 </div>

<h3>Description</h3>
 <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<div class="input-group ">
<textarea type="text" name="description" placeholder="Write  description...." id="description" class="form-control" >{{ $edit->description }}</textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
   <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Update</button>
            <a href="{!! route('admin.hotel.room-type') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
        </div>
    </div>
</div>
</div>
</div>
  @endsection
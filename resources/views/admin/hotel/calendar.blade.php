@extends('layouts.master')
@section('head_extra')
<!-- jVectorMap 1.2.2 -->
<link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />
<style>
    .filter_date {
        font-size: 14px;
    }

</style>
<link href="{{ asset("/bower_components/admin-lte/plugins/fullcalendar/fullcalendar.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/plugins/fullcalendar/fullcalendar.print.css") }}" rel="stylesheet" media="print" />
@endsection
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Reservations Calendar
        <small>Reservations</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class="box box-default">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>
<div id='calendar'></div>

</div></div></div></div>


@endsection
@section('body_bottom')
<link href='/bower_components/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='/bower_components/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='/bower_components/fullcalendar/moment.min.js'></script>
<script src='/bower_components/fullcalendar/fullcalendar.min.js'></script>

<script>
    $(function() {
        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d = date.getDate()
            , m = date.getMonth()
            , y = date.getFullYear()

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today'
                , center: 'title'
                , right: 'month,agendaWeek,agendaDay'
            }
            , buttonText: {
                today: 'today'
                , month: 'month'
                , week: 'week'
                , day: 'day'
            },
            //Random default events
            events: @php echo $allReservation @endphp,
            /*
            {
            	title          : 'Click for Google',
            	start          : new Date(y, m, 28),
            	end            : new Date(y, m, 29),
            	url            : 'http://google.com/',
            	backgroundColor: '#3C8DBC',
            	borderColor    : '#3C8DBC'
            }
            */
            eventClick: function(event) {
                if (event.url) {
                    window.open(event.url, "_blank");
                    return false;
                }
            }
        })
    })

</script>
@endsection

 @extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

       <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">

            <h1>
               {{ $page_title or "Page Title" }}
                <small>{{ $page_description or "Page Description" }} 
                </small>
            </h1>

        </section>  

    <div class='row'>
        <div class='col-md-12'> 
            <!-- Box -->
           
                <div class="box box-primary">

                    <div class="box-header with-border"> 
                        <div class='row'>
                          <div class='col-md-12'>
                              <b><font size="4">{{ $page_title or "Page Title" }}</font></b>
                              
                               <span id="index_res_ajax_status"></span>
                              <div style="display: inline; float: right;"> 
                              </div>
                          </div>
                         </div>

                    </div>

                    <div class="box-body">
                        <span id="index_lead_ajax_status"></span>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="orders-table"> 
                                <thead>
                                    <tr class="bg-red">
                                        <th>id</th>
                                        <th>Staff</th> 
                                        <th>Hotel Guest</th> 
                                        <th>POS Guest</th> 
                                        <th>Outlet</th>
                                        <th>Paid Amount</th> 
                                        <th>Balance Amount</th>
                                        <th>Settlement Status</th>
                                        <th>Total</th>
                                        <th>Guest</th>
                                        <th>Post Bill</th>
                                       
                                        <th>Action</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                        $folio_total_amount = 0;
                                   ?>
                                @if(isset($posbills) && !empty($posbills)) 
                                    @foreach($posbills as $o) 
                                        <tr>
                                            <td>{{$o->outlet->outlet_code}}{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>

                                           <?php
                                            $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                           ?>

                                            <td>{!!  $o->user->username !!}</td>
                                            <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name}}</small>@endif</td> 

                                            <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>
                                            <td>{!! $o->outlet->name !!}</td>
                                            <td>{!! number_format($paid_amount,2) !!}</td>

                                            <td>{!! number_format($o->total_amount-$paid_amount,2)  !!}</td>

                                            @if($o->settlement == 1)
                                               <td ><span class="label label-success">Settled</span></td>
                                            @else
                                               <td ><span class="label label-warning">Not Settle</span></td>
                                            @endif   

                                            <td>{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                            <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>

                                             <td>@if($o->reservation->guest_id ) {{  $o->reservation->guest->full_name }} @else {{  $o->name }} @endif</td>

                                             @if($o->is_posted)
                                            
                                              <td>
                                                  <a class="btn btn-success btn-xs" href="#">Posted</a>
                                              </td>

                                            @else

                                            <td>
                                              <form method="post" action="/admin/hotel/postbills/outlets/{{$o->id}}">
                                                {{ csrf_field() }}
                                                <button class="btn btn-success btn-xs" type="submit">Post</button>
                                                
                                              </form>
                                            </td>

                                            @endif
                                             
                                          
                                            
                                            <td>
                                                <a href="/admin/unsettlement/orders/{{$o->id}}/create"  title="UnSettle">Unsettle</a>   
                                            </td>  
                                        </tr>
                                    @endforeach
                                @endif
                                <tr>
                                  <td colspan="7">  
                                  </td>
                                  <td>
                                    Total Amount:
                                  </td>
                                  <td>
                                   <strong> {{env(APP_CURRENCY)}} {{ number_format($folio_total_amount,2) }} </strong>
                                  </td>
                                </tr>
                                </tbody>
                            </table>

                        </div> <!-- table-responsive --> 

                    </div><!-- /.box-body -->
       
                </div><!-- /.box -->
                 <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
           
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

    <script language="JavaScript">
        function toggleCheckbox() {
            checkboxes = document.getElementsByName('chkClient[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = !checkboxes[i].checked;
            }
        }
    </script>


    <script type="text/javascript"> 


     $(document).on('change', '#order_status', function() {
        
          var id = $(this).closest('tr').find('.index_sale_id').val(); 
          
          var purchase_status = $(this).val();
          $.post("/admin/ajax_order_status", 
          {id: id, purchase_status:purchase_status, _token: $('meta[name="csrf-token"]').attr('content')},
          function(data, status){
            if(data.status == '1')
                $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
            else
                $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

            $('#index_status_update').delay(3000).fadeOut('slow');
            //alert("Data: " + data + "\nStatus: " + status);
          });

        });

</script>
<script type="text/javascript">
  
  $("#btn-submit-filter").on("click", function () {

   fiscal_id = $("#filter-fiscal").val();
   status = $("#filter-status").val();
   customer_id = $("#filter-customer").val();
   location_id = $("#filter-location").val();
   type = $("#order_type").val();

  window.location.href = "{!! url() !!}/admin/orders?fiscal_id="+fiscal_id+"&status="+status+"&customer_id="+customer_id+"&location_id="+location_id+"&type="+type;
});

$("#btn-filter-clear").on("click", function () {

    type = $("#order_type").val();
    window.location.href = "{!! url() !!}/admin/orders?type="+type;
});

</script>

<script type="text/javascript">
         $(document).ready(function() {
    $('.customer_id').select2();
});
</script>

@endsection

@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

      <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
           <h1>
                {!! $page_title !!}
                <small>{!! $page_description !!}</small>
            </h1>
             {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}

        </section>

    <div class='row'>
        <div class='col-md-12'> 
            <!-- Box -->
            {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
                <div class="box box-primary">

                    <div class="box-header with-border"> 
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">

                        <span id="index_lead_ajax_status"></span>
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="orders-table">
                                <thead>
                                    <tr class="bg-orange">
                                        <th style="text-align: center">
                                            <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                                                
                                            </a> 
                                        </th>
                                        <th>id</th>
                                        <th>Hotel Guest</th>
                                        <th>POS Guest</th>
                                        <th>Outlet</th>
                                        <th>Paid</th> 
                                        <th>Balance</th>
                                        <th>Settlement Status</th>
                                        <th>Total</th>
                                        <th>Void Tools </th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($orders) && !empty($orders)) 
                                    @foreach($orders as $o) 
                                     @if($o->settlement == 1)
                                        <tr>   
                                            <td class="bg-success" align="center">{!! Form::checkbox('chkClient[]', $o->id); !!}</td>

                                            <td class="bg-success"><a href="/admin/orders/{{$o->id}}/edit">{{$o->outlet->outlet_code}}{!! $o->id !!}</a><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>  

                                          <td class="bg-success">@if($o->reservation_id)<a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{$o->reservation->guest_name}}</small>@endif</td> 

                                            <td class="bg-success">@if($o->pos_customer_id){{$o->client->name}}@endif</td>



                                            <td class="bg-success"> {{ $o->outlet->name}} </td>

                                           

                                             <?php
                                               $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                             ?>

                                            <td class="bg-success">{!! number_format($paid_amount,2) !!}</td>

                                            <td class="bg-success">{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                            @if($o->settlement == 1)
                                              <td class="bg-success"><span class="label label-success">Settled</span></td>
                                            @else
                                               <td class="bg-success"><span class="label label-warning">Not Settled</span></td>
                                            @endif

                                            <td class="bg-success">{!! $o->total_amount !!}</td>  

                                            <td class="bg-success">
                                                <a href="/admin/order/generatePDF/{{$o->id}}"><i class="fa fa-download"></i></a>
                                                 <a href="/admin/order/print/{{$o->id}}"><i class="fa fa-print"></i></a>

                                                 <a href="/admin/order/thermalprint/{{$o->id}}"><i class="fa  fa-file-text" title="Thermal Print"></i></a>

                                                  @if(\Input::get('type') == 'quotation') 
                                                 <a href="/admin/order/copy/{{$o->id}}"><i class="fa fa-copy"></i></a>
                                                 @endif
                                            </td>
                                            
                                          </tr>
                                            @else
                                          <tr>
                                            <td align="center">{!! Form::checkbox('chkClient[]', $o->id); !!}</td>
                                            <td><b><a href="/admin/orders/{{$o->id}}/edit">{{$o->outlet->outlet_code}}{!! $o->id !!}</a></b><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>
                                            <td>@if($o->reservation_id)<a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{$o->reservation->guest_name}}</small>@endif</td> 

                                            <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>
                                            <td > {{ $o->outlet->name}} </td>
                                           
                                             <?php
                                              $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                             ?>
                                            <td>{!! number_format($paid_amount,2) !!}</td>
                                            <td>{!! number_format($o->total_amount-$paid_amount,2)  !!}</td>

                                            @if($o->settlement == 1)
                                               <td><span class="label label-success">Settled</span></td>
                                            @else
                                               <td><span class="label label-warning">No Settled</span></td>
                                            @endif   

                                            <td>{!! number_format($o->total_amount,2) !!}</td>
                                            <td>
                                                

                                                  @if($o->is_bill_active == 1)
                                                  
                                                   <a href="/admin/orders/void/confirm/{{$o->id}}" data-toggle="modal" data-target="#modal_dialog">Make Void</a>

                                                  @else

                                                  Already Void

                                                  @endif
                                             
                                                 
                                            </td>
                                            
                                          </tr>
                                          @endif 
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div> <!-- table-responsive --> 
                    </div><!-- /.box-body -->
                      <div style="text-align: center;"> {!! $orders->appends(\Input::except('page'))->render() !!} </div>
                </div><!-- /.box -->
                 <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
            {!! Form::close() !!}
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

    <script language="JavaScript">
        function toggleCheckbox() {
            checkboxes = document.getElementsByName('chkClient[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = !checkboxes[i].checked;
            }
        }
    </script>


    <script type="text/javascript"> 


     $(document).on('change', '#order_status', function() {
        
          var id = $(this).closest('tr').find('.index_sale_id').val(); 
          
          var purchase_status = $(this).val();
          $.post("/admin/ajax_order_status", 
          {id: id, purchase_status:purchase_status, _token: $('meta[name="csrf-token"]').attr('content')},
          function(data, status){
            if(data.status == '1')
                $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
            else
                $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

            $('#index_status_update').delay(3000).fadeOut('slow');
            //alert("Data: " + data + "\nStatus: " + status);
          });

        });

</script>
<script type="text/javascript">
  
  $("#btn-submit-filter").on("click", function () {

   status = $("#filter-status").val();
   type = $("#order_type").val();

  window.location.href = "{!! url() !!}/admin/orders?status="+"&type="+type;
});

$("#btn-filter-clear").on("click", function () {

    type = $("#order_type").val();
    window.location.href = "{!! url() !!}/admin/orders?type="+type;
});

</script>

<script type="text/javascript">
         $(document).ready(function() {
    $('.customer_id').select2();
});
</script>

@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
            {{$page_title ?? "Page Title"}}
                <small>{{$page_description ?? "Page Description"}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>


<style type="text/css"> 
      
        .btn-circle.btn-xl { 
            width: 115px; 
            height: 115px; 
            padding: 24px 30px; 
            border-radius: 60px; 
            font-size: 14px; 
            font-weight: bold;
            text-align: center; 
        } 
    </style> 


<div class='row'>
<form method="post" action="/admin/hotel/orders/outlet/{{$id}}/tabletransfer">
    {{csrf_field()}}
    <div class="col-md-6">
        <div class="box box-warning ">
            <div class="box-header with-border">
                <h3 class="box-title">From</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col=md-6">
                    <div class="form-group">
                        <label>Select</label>
                        <select class="form-control" name="tablefrom" required>
                            <option value="">-Select-</option>
                            @foreach($tablefrom as $tf)
                            <?php 
                              $table = \App\Models\PosTable::find($tf['table_id']);
                            ?>
                              <option value="{{$table->id}}">{{$table->tablearea->posfloor->name}} &gt; {{$table->tablearea->name}} &gt; {{$table->table_number}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="box box-success ">
                <div class="box-header with-border">
                    <h3 class="box-title">To</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col=md-6">
                        <div class="form-group">
                            <label>Select</label>
                            <select class="form-control" name="tableto" required>
                                <option value="">-Select-</option>
                                @foreach($tableto as $tf)
                                <?php 
                                $table = \App\Models\PosTable::find($tf['table_id']);
                                ?>
                                <option value="{{$table->id}}">{{$table->tablearea->posfloor->name}} &gt; {{$table->tablearea->name}} &gt; {{$table->table_number}}</option>
                                @endforeach
                          </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" align="center">
            <button type="submit" class="btn btn-success ">
                <i class="fa fa-retweet"></i> Complete Transfer
            </button>
        </div>
        </form>
</div>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">

 $(document).ready(function() {
   $('.select2').select2();
 });

</script>

<script type="text/javascript">

    $(function() {
        $('.datepicker').datetimepicker({
          //inline: true,
          format: 'YYYY-MM-DD',
          sideBySide: true,
          allowInputToggle: true
        });

      });

</script>


  @endsection
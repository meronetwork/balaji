@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        <i class="fa fa-print"></i> Reprint Bills
        <small>{!! $page_description !!}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}

</section>

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->

        <div class="box box-primary">
            <div class="box-header with-border">
            <form action="/admin/hotel/orders/outlet/{{$outlet_id}}/reprintbills">
                <span style="font-weight: bold;font-size: 16px">List of Tax Invoice</span>

                <div class="box-tools pull-right">
                <input type="text" name="bill_no" placeholder="Enter Bill Number"
                value="{{ Request::get('bill_no')}}">

          {{--       <select class="bg-olive" name="outlet_id">
                    <option value="">Select Outlet</option>
                    @foreach($outlets as $k => $o)
                        <option value="{{ $o->id }}" @if( $o->id == Request::get('outlet_id')) selected="" @endif
                        >{{ $o->name }}</option>
                    @endforeach
                </select> --}}

                <button class="btn btn-primary btn-xs" style="margin-top: -4px;">Filter</button>
                <button class="btn btn-danger btn-xs" style="margin-top: -4px;">Clear</button>
              </div>
            </form>
            </div>
             {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
            <div class="box-body">
                <span id="index_lead_ajax_status"></span>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" id="orders-table">
                        <thead>
                            <tr class="bg-black">

                                <th>Bill No</th>
                                <th>Bill Date</th>
                                <th>Hotel Guest</th>
                                <th>POS Guest</th>
                                <th>Outlet</th>
                                <th>Paid</th>
                                <th>Balance</th>
                                <th>Settlement Status</th>
                                <th>Total</th>
                                <th>Printed Times</th>
                                <th>Table</th>
                                <th>Print </th>

                            </tr>
                        </thead>
                        @php $total_balance_amount = 0; @endphp
                        <tbody>
                            @if(isset($orders) && !empty($orders))
                            @foreach($orders as $o)
                            <tr>
                                <td>TI-{{$o->outlet->outlet_code}}{!! $o->bill_no !!}</td>
                                <td>{!! $o->bill_date !!}</td>

                                <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name }}@elseif($o->folio_id) {{$o->folio->reservation->client->name ??''}} @else {{$o->folio->reservation->guest_name ??''}}</small>@endif</td>
                                <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                <td> {{ $o->outlet->name}} </td>

                                <?php
                                    if($o->folio->reservation_id ??''){

                                        $paid_amount= \TaskHelper::getFolioPaymentAmount($o->folio->reservation_id);
                                    }else{

                                        $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                    }

                                ?>

                                <td>{!! number_format($paid_amount,2) !!}</td>
                                <td>{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                <?php
                                    $balance_amount = $o->total_amount - $paid_amount;
                                    $total_balance_amount = $total_balance_amount + $balance_amount;
                                ?>
                                @if($o->outlet->id == env('HOTEL_OUTLET_ID'))
                                @if($balance_amount == $o->total_amount )
                                <td><span class="label label-warning">Pending</span></td>
                                @elseif($balance_amount < $o->total && $balance_amount > 0)
                                    <td><span class="label label-info">Partial</span></td>
                                    @elseif($balance_amount == 0)
                                    <td><span class="label label-success">Paid</span></td>
                                    @else
                                    <td><span class="label label-warning">Pending</span></td>
                                    @endif

                                    <?php
                                    ?>
                                    @else

                                    @if($o->ordermeta->settlement == 1)
                                    <td><span class="label label-success">Settled</span></td>
                                    @else
                                    <td><span class="label label-warning">No Settled</span></td>
                                    @endif

                                    @endif

                                    <td>{!! number_format($o->total_amount,2) !!}</td>


                                    <?php
                                    $print_no = \App\Models\POSPrint::where('order_id',$o->id)->count();
                                ?>

                                    <td>{{$print_no}}</td>
                                    <?php
                                        $table_name = \App\Models\PosTable::find($o->table);
                                    ?>
                                    <td> {{$table_name->tablearea->name }} > {{$table_name->table_number}}</td>
                                    <td>
                                        <a href="/admin/order/thermalprint/{{$o->id}}">
                                            <i class="fa fa-print"></i>
                                        </a>
                                    </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div> <!-- table-responsive -->
            </div><!-- /.box-body -->
            <div style="text-align: center;"> {!! $orders->appends(\Request::except('page'))->render() !!} </div>
        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->

<div class='row' style="margin-top: 8px">
    <div class='col-md-12'>
        <!-- Box -->

        <div class="box box-primary">
            <div class="box-header with-border">
            <form action="/admin/hotel/orders/outlet/{{$outlet_id}}/reprintbills">
                <span style="font-weight: bold;font-size: 16px">List of Abbreviated Tax Invoice</span>

                <div class="box-tools pull-right">
                <input type="text" name="abbr_bill_no" placeholder="Enter Bill Number"
                value="{{ Request::get('abbr_bill_no')}}">

          {{--       <select class="bg-olive" name="outlet_id">
                    <option value="">Select Outlet</option>
                    @foreach($outlets as $k => $o)
                        <option value="{{ $o->id }}" @if( $o->id == Request::get('outlet_id')) selected="" @endif
                        >{{ $o->name }}</option>
                    @endforeach
                </select> --}}

                <button class="btn btn-primary btn-xs" style="margin-top: -4px;">Filter</button>
                <button class="btn btn-danger btn-xs" style="margin-top: -4px;">Clear</button>
              </div>
            </form>
            </div>
             {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
            <div class="box-body">
                <span id="index_lead_ajax_status"></span>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" id="orders-table">
                        <thead>
                            <tr class="bg-black">

                                <th>Bill No</th>
                                <th>Bill Date</th>
                                <th>Hotel Guest</th>
                                <th>POS Guest</th>
                                <th>Outlet</th>
                                <th>Paid</th>
                                <th>Balance</th>
                                <th>Settlement Status</th>
                                <th>Total</th>
                                <th>Printed Times</th>
                                <th>Table</th>
                                <th>Print </th>

                            </tr>
                        </thead>
                        @php $total_balance_amount = 0; @endphp
                        <tbody>
                            @if(isset($abbrorders) && !empty($abbrorders))
                            @foreach($abbrorders as $o)
                            <tr>
                                <td>AI-{{$o->outlet->outlet_code}}{!! $o->bill_no !!}</td>
                                <td>{!! $o->bill_date !!}</td>

                                <td>@if($o->reservation_id){{env('RES_CODE')}}{{$o->reservation_id}}<small>{{$o->reservation->guest_name }}@elseif($o->folio_id) {{$o->folio->reservation->client->name ??''}} @else {{$o->folio->reservation->guest_name ??''}}</small>@endif</td>
                                <td>@if($o->pos_customer_id){{$o->client->name}}@endif</td>

                                <td> {{ $o->outlet->name}} </td>

                                <?php
                                    if($o->folio->reservation_id ??''){

                                        $paid_amount= \TaskHelper::getFolioPaymentAmount($o->folio->reservation_id);
                                    }else{

                                        $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                    }

                                ?>

                                <td>{!! number_format($paid_amount,2) !!}</td>
                                <td>{!! number_format($o->total_amount-$paid_amount,2) !!}</td>

                                <?php
                                    $balance_amount = $o->total_amount - $paid_amount;
                                    $total_balance_amount = $total_balance_amount + $balance_amount;
                                ?>
                                @if($o->outlet->id == env('HOTEL_OUTLET_ID'))
                                @if($balance_amount == $o->total_amount )
                                <td><span class="label label-warning">Pending</span></td>
                                @elseif($balance_amount < $o->total && $balance_amount > 0)
                                    <td><span class="label label-info">Partial</span></td>
                                    @elseif($balance_amount == 0)
                                    <td><span class="label label-success">Paid</span></td>
                                    @else
                                    <td><span class="label label-warning">Pending</span></td>
                                    @endif

                                    <?php
                                    ?>
                                    @else

                                    @if($o->ordermeta->settlement == 1)
                                    <td><span class="label label-success">Settled</span></td>
                                    @else
                                    <td><span class="label label-warning">No Settled</span></td>
                                    @endif

                                    @endif

                                    <td>{!! number_format($o->total_amount,2) !!}</td>


                                    <?php
                                    $print_no = \App\Models\POSPrint::where('order_id',$o->id)->count();
                                ?>

                                    <td>{{$print_no}}</td>
                                    <?php
                                        $table_name = \App\Models\PosTable::find($o->table);
                                    ?>
                                    <td> {{$table_name->tablearea->name }} > {{$table_name->table_number}}</td>
                                    <td>
                                        <a href="/admin/order/thermalprint/{{$o->id}}?invoice_type=abbr">
                                            <i class="fa fa-print"></i>
                                        </a>
                                    </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div> <!-- table-responsive -->
            </div><!-- /.box-body -->
            <div style="text-align: center;"> {!! $abbrorders->appends(\Request::except('page'))->render() !!} </div>
        </div><!-- /.box -->
        <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }

</script>


<script type="text/javascript">
    $(document).on('change', '#order_status', function() {

        var id = $(this).closest('tr').find('.index_sale_id').val();

        var purchase_status = $(this).val();
        $.post("/admin/ajax_order_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

</script>
<script type="text/javascript">
    $("#btn-submit-filter").on("click", function() {

        status = $("#filter-status").val();
        type = $("#order_type").val();

        window.location.href = "{!! url('/') !!}/admin/orders?status=" + "&type=" + type;
    });

    $("#btn-filter-clear").on("click", function() {

        type = $("#order_type").val();
        window.location.href = "{!! url('/') !!}/admin/orders?type=" + type;
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
    });

</script>

@endsection

@extends('layouts.master')
@section('content')
  <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
          <h1>
            Billing Overview for Reservation # {{ \Request::segment(5) }}
              <small>List Of Folios and Payments</small>
           
          </h1>
          <i class="fa fa-info"></i> Overviews of all charges
          {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
  </section>

    <div class='row'>
        <div class='col-md-12'> 
            <!-- Box -->
            {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                        	 <div class="col-md-12">
                       	<h4>Folio Details</h4>
                       </div>
                            <table class="table table-hover table-bordered" id="orders-table">
                                <thead>
                                    <tr>
                                       
                                        <th>Id</th>
                                        <th>Folio Owner</th> 
                                        <th>Folio Type</th> 
                                        <th>Paid Amount</th> 
                                        <th>Balance Amount</th>
                                        <th>Payment Status</th>
                                        <th>Total</th>
                                        <th>Tools</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                        $folio_total_amount = 0;
                                        $total_balance_amount= 0;
                                   ?>
                                 @if(isset($folios) && !empty($folios))  
                                    @foreach($folios as $o) 
                                        <tr>
                                           
                                            <td>{!! $o->id !!}<input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>                
                                             <?php

                                              $paid_amount= \TaskHelper::getFolioPaymentAmount($o->reservation_id);

                                             ?>  
                                            <td>{!!  $o->user->username !!}</td>
                                            <td>{!!  $o->folio_type !!}</td>
                                            <td>{!! number_format($paid_amount,2) !!}</td>
                                           
                                            <td>{!! number_format($o->total_amount-$paid_amount,2)  !!}</td>
                                                 <?php
                                                    $balance_amount = $o->total_amount - $paid_amount;

                                                    $total_balance_amount = $total_balance_amount + $balance_amount;

                                                  ?>
                                            @if($balance_amount == $o->total_amount )
                                              <td><span class="label label-warning">Pending</span></td>
                                            @elseif($balance_amount < $o->total && $balance_amount > 0)
                                              <td><span class="label label-info">Partial</span></td>
                                            @elseif($balance_amount == 0)
                                              <td><span class="label label-success">Paid</span></td>
                                             @else
                                              <td><span class="label label-warning">Pending</span></td>
                                            @endif   

                                            <td>{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                            <td>
                          

                                            <a class="btn btn-primary btn-xs" href="#" onClick="window.open('/admin/payment/folio/{{$o->id}}/create', '_blank','toolbar=yes, scrollbars=yes, resizable=yes, top=500,left=500,width=600, height=650')"><i class="fa fa-credit-card"></i> Pay Now</a>

                                            <a target="_blank" class="btn btn-warning btn-xs" href="/admin/reservation/folio/{{$o->reservation_id}}/index"> Folio Window <i class="fa fa-external-link"></i> </a>

                                            </td>

                                            <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>
                                        </tr>
                                    @endforeach    
                                @endif
                                <tr>
                                  <td colspan="5">             
                                  </td>
                                  <td>
                                    Total Amount:
                                  </td>
                                  <td>
                                    {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount,2) }}
                                  </td>
                                </tr>
                                </tbody>
                            </table>

                        </div> <!-- table-responsive --> 

                    </div><!-- /.box-body -->
       
                </div><!-- /.box -->
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div><!-- /.row -->

    <div class='row'>
        <div class='col-md-12'> 
            {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive"> 
                        	 <div class="col-md-12">
                       	<h4>POS Details</h4>
                       </div>
                            <table class="table table-hover table-bordered" id="orders-table">
                                <thead>
                                    <tr>
                                       
                                        <th>id</th>
                                        <th>Reservation</th>
                                        <th>Outlet</th>
                                        <th>Status</th>
                                        <th>Paid</th> 
                                        <th>Balance</th>
                                        <th>Pay Status</th>
                                        <th>Total</th>
                                        <th>Pay</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                        $pos_total_amount = 0;
                                   ?>
                                   @if(isset($orders) && !empty($orders))  
                                      @foreach($orders as $o) 
                                          <tr>

                                         
                                            <td><a href="/admin/orders/{{$o->id}}">PORD{!! $o->id !!}</a><input type="hidden" name="sale_id" class="index_sale_id" value="{{$o->id}}"></td>
                                            <td><a href="/admin/hotel/reservation-edit/{{$o->reservation_id}}">{{env('RES_CODE')}}{{$o->reservation_id}}</a><small>{{ $o->name }}</small> </td>
                                            <td class="bg-warning"> {{ $o->outlet->name}} </td>
                                            <td>
                                                 {{$o->status}}
                                            </td>
                                             <?php
                                              $paid_amount= \TaskHelper::getSalesPaymentAmount($o->id);
                                             ?>
                                            <td>{!! number_format($paid_amount,2) !!}</td>
                                            <td>{!! number_format($o->total_amount-$paid_amount,2)  !!}</td>
                                           <?php
                                              $pos_balance = $o->total_amount-$paid_amount;
                                              $total_balance_amount = $total_balance_amount + $pos_balance;
                                           ?>
                                            @if($o->payment_status == 'Pending')
                                              <td><span class="label label-warning">Pending</span></td>
                                            @elseif($o->payment_status == 'Partial')
                                              <td><span class="label label-info">Partial</span></td>
                                            @elseif($o->payment_status == 'Paid')
                                              <td><span class="label label-success">Paid</span></td>
                                            @else
                                               <td><span class="label label-warning">Pending</span></td>
                                            @endif   
                                            <td>{!! number_format($o->total_amount,2) !!}</td>  
                                            <td><a href="/admin/payment/orders/{{$o->id}}" target="_blank" title="View Payment"><i class="fa fa-credit-card"></i></a></td>
                                            <?php 
                                              $pos_total_amount = $pos_total_amount + $o->total_amount;
                                            ?> 
                                            
                                          </tr>  
                                       @endforeach
                                   @endif
                                <tr>
                                  <td colspan="7"> 
                                  </td>
                                  <td>
                                    Total Amount:
                                  </td>
                                  <td>
                                    {{env('APP_CURRENCY')}} {{ number_format($pos_total_amount,2) }}
                                  </td>
                                </tr>
                                </tbody>
                            </table>
                        </div> <!-- table-responsive --> 
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            {!! Form::close() !!}
        </div><!-- /.col -->
    </div><!-- /.row -->
</br>

    <div class='row'>
        <div class='col-md-12'>  
          <?php  /**   @if($total_balance_amount <= 0) **/ ?>
            <div class="col-md-12">
               <a href="/admin/hotel/cashiering/billing" class="btn btn-primary">Close</a>
            </div>
          <?php  /**   @else
              <div class="col-md-12">
                 <a href="#" class="btn btn-primary" disabled >Check Out</a>
              </div>
            @endif  **/ ?>

        </div><!-- /.col -->
    </div><!-- /.row -->



@endsection
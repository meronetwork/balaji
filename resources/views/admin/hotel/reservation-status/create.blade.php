@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Reservation status
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 @if($edit)
 <form method="post" action="{{route('admin.hotel.res-status-edit',$edit->id)}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">
   <div class="col-sm-6">
<div class="form-group">  
<label class="control-label col-sm-12">Status name</label>
<div class="input-group ">
<input type="text" name="status_name" placeholder="Status name.." id="name" class="form-control"  value="{{$edit->status_name}}" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
    <div class="col-md-2">
    
    <div class="form-group">
                <label>Color picker:</label>
                <input type="color" class="form-control my-colorpicker1 colorpicker-element" name="status_color" value="{{$edit->status_color}}">
              </div>
  </div>
 </div>
 <div class="row">
 
</div>
 <div class="row">
   <div class="col-sm-8">
<div class="form-group">  
<label class="control-label col-sm-12">Status description</label>
<div class="input-group ">
<textarea type="text" name="status_description" placeholder="Write status description" id="attraction_description" class="form-control" >{{$edit->status_description}}</textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>
 @else
         <form method="post" action="{{route('admin.hotel.res-status-create')}}">
            {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">
   <div class="col-sm-6">
<div class="form-group">  
<label class="control-label col-sm-12">Status name</label>
<div class="input-group ">
<input type="text" name="status_name" placeholder="Status name.." id="name" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
    <div class="col-md-2">
    
    <div class="form-group">
                <label>Color picker:</label>
                <input type="color" class="form-control my-colorpicker1 colorpicker-element" name="status_color">
              </div>
  </div>
 </div>
 <div class="row">
 
</div>
 <div class="row">
   <div class="col-sm-8">
<div class="form-group">  
<label class="control-label col-sm-12">Status description</label>
<div class="input-group ">
<textarea type="text" name="status_description" placeholder="Write status description" id="attraction_description" class="form-control" ></textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>
@endif
@endsection
@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Admin | Hotel | POS Tables

        <small>Admin | Hotel | POS Tables Index</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>
                <b>
                    <font size="4">Restaurant Tables List</font>
                </b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm" title="Create Outlets" href="{{ route('admin.pos-tables.create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new POS Tables</strong>
                    </a>
                </div>
            </div>
        </div>
        <table class="table table-hover table-no-border table-striped" id="leads-table">

            <thead>
                <tr>
                    
                    <th>ID</th>
                    <th>Table Area</th>
                    <th>Table Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($costcenters as $key=>$att)

                <tr>
                   

                    <td>{{ $att->id }}</td>
                    <td>{{ $att->tablearea->name }}</td>
                    <td class="badge bg-aqua">{{ $att->table_number }}</td>

                    <td>
                        @if( $att->isEditable())
                        <a href="/admin/hotel/pos-tables/{{$att->id}}/edit"><i class="fa fa-edit"></i></a>
                        @else
                        <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
                        @endif

                        @if ( $att->enabled )
                        <i class="fa fa-check-circle enabled"></i>
                        @else
                        <i class="fa fa-ban disabled"></i>
                        @endif

                        <a href="{!! route('admin.hotel.pos-tables.confirm-delete', $att->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                </tr>

                @endforeach

            </tbody>
        </table>
    </div>

    @endsection

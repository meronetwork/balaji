@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              POS Tables 
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

  <form method="post" action="{{route('admin.hotel.pos-tables.store')}}"  enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="panel panel-custom"> 
        <div class="panel-heading">

            <div class="row">

            

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Table Area <small> ( E.g Floors) </small></label>
                        <div class="input-group ">
                           <select name="table_area_id" class="form-control">
                            <option value="">Please Select</option>
                            @foreach($table_area as $ta)
                               <option value="{{$ta->id}}">{{$ta->name}}-({{$ta->posfloor->name}})</option>
                            @endforeach                               
                           </select>
            

                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>

                        </div>
                    </div>
                </div> 

                 <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Table Number</label>
                        <div class="input-group ">
                            <input type="text" name="table_number" placeholder="Table Number" id="name" class="form-control" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

   
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label col-sm-12">Image</label>
                            <input type="file" name="image">
                        </div>
                    </div>



                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Seating Capacity</label>
                        <div class="input-group ">
                            <input type="text" name="seating_capacity" placeholder="Table Number" id="name" class="form-control" value="" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Table Sequence</label>
                        <div class="input-group ">
                            <input type="text" name="sequence" placeholder="Table Sequence" class="form-control" value="" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
           



            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                   <label>Is packing<i class="imp">*</i></label>
                    <input type="hidden" name="is_packing" value="0">
                    <input type="checkbox" name="is_packing" value="1">
                  </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                   <label>Enabled<i class="imp">*</i></label>
                    <input type="hidden" name="enabled" value="0">
                    <input type="checkbox" name="enabled" value="1">   
                  </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn-submit-edit" type="submit">Add</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
 </form>

  @endsection
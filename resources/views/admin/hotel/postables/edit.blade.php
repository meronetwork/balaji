@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              POS Tables 
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

  <form method="post" action="{{route('admin.pos-tables.update',$edit->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="panel panel-custom"> 
        <div class="panel-heading">

            <div class="row">

            
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Table Area <small> ( E.g Floors) </small></label>
                        <div class="input-group ">

                           <select name="table_area_id" class="form-control">
                                <option value="">Please Select</option>
                                @foreach($table_area as $ta)
                                   <option value="{{$ta->id}}" @if($ta->id == $edit->table_area_id) selected @endif>{{$ta->name}}-({{$ta->posfloor->name}})</option>
                                @endforeach                               
                           </select>

                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                   <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Table Number</label>
                        <div class="input-group ">
                            <input type="text" name="table_number" placeholder="Table Number" id="name" value="{{ $edit->table_number }}" class="form-control" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                 
                
            </div>
            <div class="row">


                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Image</label>
                        <input type="file" name="image">
                    </div>

                     @if($edit->image)
                 <label class="control-label col-sm-12">Image:</label>
                 <a href="/tables/{{$edit->image}}" target="blank"><img src="/tables/{{$edit->image}}" width="50px" height="50px"></a>

                @endif
                </div>



             

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Seating Capacity</label>
                        <div class="input-group ">
                            <input type="text" name="seating_capacity" placeholder="Table Number" id="name" class="form-control" value="{{$edit->seating_capacity}}" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Table Sequence</label>
                        <div class="input-group ">
                            <input type="text" name="sequence" placeholder="Table Sequence"  class="form-control" value="{{$edit->sequence}}" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

           
            

            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                   <label>Is packing<i class="imp">*</i></label>
                    <input type="hidden" name="is_packing" value="0">
                      {!! Form::checkbox('is_packing', '1', $edit->is_packing) !!} 
                  </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                   <label>Enabled<i class="imp">*</i></label>
                    <input type="hidden" name="enabled" value="0">
                      {!! Form::checkbox('enabled', '1', $edit->enabled) !!} 
                  </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn-submit-edit" type="submit">Update</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
 </form>

  @endsection
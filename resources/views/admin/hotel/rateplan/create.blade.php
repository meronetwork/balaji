@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Rate Plan 
                <small>Create </small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 <form method="post" action="{{route('admin.hotel.rate-create')}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">

 <div class="row">
    <div class="col-md-6">
      <div class="form-group">
          <label class="control-label col-sm-6">Room Type</label>
            <div class="col-md-6">
              <select name="roomtype_id" class="form-control">
                @foreach($roomtype as $rt)
                  <option value="{{$rt->roomtype_id}}">{{$rt->room_name}}</option>
                @endforeach
              </select>
            </div>
          </div>
      </div>
 </div>
<br>

<div class="row">
    <div class="col-md-6">
     <div class="form-group">  
      <label class="control-label col-sm-6">Package Name</label>
          <div class="input-group col-sm-6">
              <input type="text" name="package_name" placeholder="Package name" id="package_name" value="" class="form-control" required="required">
              <div class="input-group-addon">
                  <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="row">
      <div class="col-md-6">
          <div class="form-group">  
            <label class="control-label col-sm-6">Room Rate</label>
                <div class="input-group col-sm-6">
                    <input type="text" name="rate" placeholder=" Room Rate" id="rate" value="" class="form-control" required="required">
                    <div class="input-group-addon">
                        <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                    </div>
                </div>
            </div>
       </div>
 </div>

<div class="row">
      <div class="col-md-6">
        <div class="form-group">  
          <label class="control-label col-sm-6">Breakfast Rate</label>
              <div class="input-group col-sm-6">
                  <input type="text" name="breakfast_rate" placeholder="Breakfast Rate" id="breakfast_rate" value="" class="form-control" >
                  <div class="input-group-addon">
                      <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                  </div>
              </div>
          </div>
       </div>
 </div>

 <div class="row">
      <div class="col-md-6">
        <div class="form-group">  
          <label class="control-label col-sm-6">Lunch Rate</label>
              <div class="input-group col-sm-6">
                  <input type="text" name="lunch_rate" placeholder="Lunch Rate" id="lunch_rate" value="" class="form-control" >
                  <div class="input-group-addon">
                      <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                  </div>
              </div>
          </div>
       </div>
 </div>


 <div class="row">
      <div class="col-md-6">
        <div class="form-group">  
          <label class="control-label col-sm-6">Dinner Rate</label>
              <div class="input-group col-sm-6">
                  <input type="text" name="dinner_rate" placeholder="Dinner Rate" id="dinner_rate" value="" class="form-control" >
                  <div class="input-group-addon">
                      <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                  </div>
              </div>
          </div>
       </div>
 </div>


 <div class="row">

            <div class="col-md-6">
              <div class="form-group">  
                <label class="control-label col-sm-6">Display Order Number</label>
                    <div class="input-group col-sm-6">
                        <input type="number" name="order_num" placeholder="Order Number" id="order_num" value="" class="form-control" required="required">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa  fa-hotel (alias)"></i></a>
                        </div>
                    </div>
                </div>

        </div>

 </div>

  <div class="row">
   <div class="col-sm-12">
<div class="form-group">  
<label class="control-label col-sm-12"> Details</label>
<div class="input-group ">
<textarea type="text" name="detail" placeholder="Description" id="detail" class="form-control"></textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>


 <div class="row">
   <div class="col-md-6">
 <div class="form-group">
        <label class="control-label col-sm-6">
        Enabled
        </label>
        <div class="input-group col-sm-6">
            {!! Form::checkbox('enabled', '1', $rateplan->enabled ?? 0) !!} 
          </div>
       
    </div>

 </div>
</div>

   <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Create</button>
            <a href="{!! route('admin.hotel.rate-plans') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
        </div>
    </div>
</div>
</div>
</div>
  @endsection
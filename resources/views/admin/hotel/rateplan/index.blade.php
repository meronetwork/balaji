@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Rate Plan
        <small> Index</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>

                <b>
                    <font size="4">Rate Plan List</font>
                </b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-success btn-sm" title="Import/Export Leads" href="{{ route('admin.hotel.rate-create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new Plan </strong>
                    </a>
                </div>
            </div>
        </div>

        <table class="table table-hover table-border table-striped" id="leads-table">
            <thead>
                <tr class="bg-info">
                    
                    <th>ID</th>
                    <th>Room Type</th>
                    <th>Package Name</th>
                    <th>Rate</th>
                    <th>Order</th>
                    <th>Enabled</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($plan as $key=>$rm)
                <tr>
                   
                    <td>{{$rm->id}}</td>
                    <td style="font-size: 16.5px"> {{ $rm->room_name }}</td>
                    <td>{{$rm->package_name}}</td>
                    <td style="font-size: 16.5px">{{ env('APP_CURRENCY')}}{{ (float) $rm->rate + (float) $rm->breakfast_rate + (float) $rm->lunch_rate + (float) $rm->dinner_rate }}</td>
                    <td>{{$rm->order_num}}</td>
                    <td>{{$rm->enabled}}</td>
                    <td>
                        @if( $rm->isEditable())<a href="/admin/hotel/rate-edit/{{$rm->id}}"><i class="fa fa-edit"></i></a>
                        @else
                        <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
                        @endif
                        &nbsp;&nbsp;
                        <a href="{{route('admin.hotel.rate-confirm-delete', $rm->id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
                </tr>
                @endforeach

            </tbody>
        </table>
        <div style="text-align: center;"> {!! $plan->render() !!} </div>
    </div>

    @endsection

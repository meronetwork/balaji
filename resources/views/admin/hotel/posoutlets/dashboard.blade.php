@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        Restaurant and Outlets Transaction Dashboard
        <small>{{$description}}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>


<style type="text/css">
    .btn-circle.btn-xl {
        width: 125px;
        height: 125px;
        padding: 32px 37px;
        border-radius: 76px;
        font-size: 14px;
        text-align: left;
    }

    .btn-circle.btn-xl span {
        margin-left: -15px;
    }

</style>


<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body">


                <div class="row">

                    <div class='col-md-12'>

                        @if(\Auth::user()->hasRole(['waiter','admins']))
                        <div class="col-md-3">
                            <!-- small box -->
                            <a href="/admin/hotel/openresturantoutlets">
                            <div class="small-box bg-olive">
                                <div class="inner">
                                    <h3>Restro</h3>

                                    <p>Take F&B Orders</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="small-box-footer">
                                    Go <i class="fa fa-arrow-circle-right"></i>
                                </div>

                            </div>
                            </a>
                        </div>
                        @endif

                         @if(\Auth::user()->hasRole(['pos-user','admins']))
                        <div class="col-md-3">
                            <!-- small box -->
                            <a href="/admin/hotel/openoutlets">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>Outlets</h3>

                                    <p>Take Service Orders</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="small-box-footer">
                                    Go <i class="fa fa-arrow-circle-right"></i>
                                </div>

                            </div>
                            </a>
                        </div>
                        @endif
{{--
                        <div class="col-md-3">
                            <!-- small box -->
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h3>Check</h3>

                                    <p>Check or Modify Orders</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <a href="/admin/hotel/outletsales" class="small-box-footer">
                                    Go <i class="fa fa-arrow-circle-right"></i>
                                </a>

                            </div>
                        </div>
 --}}


                        <div class="col-md-3">
                            <!-- small box -->
{{--                            <a href="/admin/hotel/settlement/openoutlets" >--}}
                            <a href="/admin/hotel/orders/outlet/1/settlement" >
                            <div class="small-box bg-primary">
                                <div class="inner">
                                    <h3>Settle</h3>

                                    <p>Pay Now and Issue Bills</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money"></i>
                                </div>
                                <div class="small-box-footer">
                                    Go <i class="fa fa-arrow-circle-right"></i>
                                </div>

                            </div>
                            </a>
                        </div>

                                                <div class="col-md-3">
                            <!-- small box -->
{{--                                                    <a href="/admin/hotel/reprintbills/openoutlets">--}}
                                                    <a href="/admin/hotel/orders/outlet/1/reprintbills">
                                                    <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>Print Bill</h3>

                                    <p>Bill Reprints</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-print"></i>
                                </div>
                                <div class="small-box-footer">
                                    Go <i class="fa fa-arrow-circle-right"></i>
                                </div>

                            </div>
                                                    </a>
                        </div>







                        <!--  <button type="button" onclick="window.open('/admin/hotel/unsettlement/openoutlets','_self')" class="btn bg-purple btn-circle btn-xl"><span>Unsettlement</span></button> -->
                        <!-- <button type="button" onclick="window.open('/admin/hotel/modifysettlement/openoutlets','_self')" class="btn btn-success btn-circle btn-xl"><span>Modify Setlmnt.</span></button>  -->
                        <hr />
                        <div class="clearfix"> </div>


              <a href="/admin/poscustomer" class="btn btn-app">
                <span class="material-icons">people_alt</span><br/> Customers
              </a>
              <a href="/admin/hotel/table/bookings" class="btn btn-app">
                <span class="material-icons">table_chart</span><br/> Table Book
              </a>
              <a href="/admin/hotel/roomtransfer/item" class="btn btn-app">
                <span class="material-icons">room_service</span><br/> Room Trans.
              </a>
              <a href="/admin/edm/order" class="btn btn-app">
                <span class="material-icons">assignment_ind</span><br/> EDM
              </a>
              <a href="/admin/orders/allpos/invoice" class="btn btn-app">
                <span class="material-icons">receipt_long</span><br/> Invoices
              </a>
              <a href="/admin/orders/todaypos/invoice" class="btn btn-app">
                <span class="material-icons">receipt_long</span><br/>Todays Invoices
              </a>
              <a href="/admin/orders/allpos/payments" class="btn btn-app">
                <span class="material-icons">receipt</span><br/> Receipts
              </a>

              <a href="/admin/orders/todaypos/payments" class="btn btn-app">
                <span class="material-icons">receipt</span><br/> Todays Receipts
              </a>

              <a href="/admin/posSummaryAmount" class="btn btn-app">
                <span class="material-icons">leaderboard</span><br/>Sales Summary
              </a>
              <a href="/admin/privilege-card" class="btn btn-app">
                <span class="material-icons">style</span><br/> Loyalty
              </a>
              <a href="/admin/hotel/deleted-orders" class="btn btn-app">
                <span class="material-icons">remove_circle</span><br/> Deleted orders
              </a>
              <a href="/admin/select/outlet/table/transfer" class="btn btn-app">
                <span class="material-icons">transfer_within_a_station</span><br/> Table Transfer
              </a>
              <a href="/admin/pos/orders/selectoutlet/forstatus" class="btn btn-app">
                <span class="material-icons">screen_share</span><br/> Order TV
              </a>




                         @if(!\Auth::user()->hasRole(['pos-manager','admins']))
                        <hr />

                        <button type="button" onclick="window.open('/admin/hotel/pos-outlets/index','_self')" class="btn bg-purple btn-circle btn-xl"><span>Outlets Setting</span></button>
                        <button type="button" onclick="window.open('/admin/hotel/pos-menu/index','_self')" class="btn bg-teal btn-circle btn-xl">Add Menu</button>
                        <button type="button" onclick="window.open('/admin/producttypemaster','_self')" class="btn bg-maroon btn-circle btn-xl"><span>Product Type</span></button>
                        <button type="button" onclick="window.open('/admin/products','_self')" class="btn bg-info btn-circle btn-xl">Products</button>
                        <button type="button" onclick="window.open('/admin/hotel/pos-cost-center/index','_self')" class="btn bg-purple btn-circle btn-xl">Cost Center</button>
                        <button type="button" onclick="window.open('/admin/products','_self')" class="btn btn-warning btn-circle btn-xl">Stocks</button>

                      @endif



                    </div>


                </div>





            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>


@endsection

@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Room status
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 @if($edit)
 <form method="post" action="{{route('admin.hotel.res-type-edit',$edit->id)}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">
   <div class="col-sm-6">
<div class="form-group">  
<label class="control-label col-sm-12">Name</label>
<div class="input-group ">
<input type="text" name="name" placeholder="Name.." id="name" class="form-control"  value="{{$edit->name}}" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
    <div class="col-md-2">
    
    <div class="form-group">
                <label>Color picker:</label>
                <input type="color" class="form-control my-colorpicker1 colorpicker-element" name="color" value="{{$edit->color}}">
              </div>
  </div>
 </div>
 <div class="row">
 
</div>
 <div class="row">
   <div class="col-sm-8">
<div class="form-group">  
<label class="control-label col-sm-12">Description</label>
<div class="input-group ">
<textarea type="text" name="description" placeholder="Write description" id="attraction_description" class="form-control" >{{$edit->description}}</textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>
 @else
         <form method="post" action="{{route('admin.hotel.res-type-create')}}">
            {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
    <div class="row">
   <div class="col-sm-6">
<div class="form-group">  
<label class="control-label col-sm-12">Name</label>
<div class="input-group ">
<input type="text" name="name" placeholder="Name.." id="name" class="form-control" required>
<div class="input-group-addon">
  <a href="#"><i class="fa fa-stack-exchange"></i></a>
</div>
</div>
</div>
   </div>
    <div class="col-md-2">
    
    <div class="form-group">
                <label>Color picker:</label>
                <input type="color" class="form-control my-colorpicker1 colorpicker-element" name="color" value="#FFFFFF">
              </div>
  </div>
 </div>
 <div class="row">
 
</div>
 <div class="row">
   <div class="col-sm-8">
<div class="form-group">  
<label class="control-label col-sm-12">Description</label>
<div class="input-group ">
<textarea type="text" name="description" placeholder="Write description" id="attraction_description" class="form-control" ></textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
 <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Add</button>
        </div>
    </div>
</div>
</div>
</div>
</form>
@endif
@endsection
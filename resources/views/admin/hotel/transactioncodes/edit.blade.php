@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body ">
                <form method="post" action="/admin/hotel/transaction/codes/{{$transactioncodes->id}}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Code</label>
                            <input type="text" name="code" placeholder="Code" id="code" value="{{$transactioncodes->code}}" class="form-control ">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">Transaction Group</label>
                            <select name="transaction_group_id" class="form-control">
                                <option value="">Select</option>
                                @foreach($transactiongroups as $tg)
                                <option value="{{$tg->id}}" @if($tg->id == $transactioncodes->transaction_group_id) selected @endif)>{{$tg->group_code}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Enabled</label>
                            <div class="form-group">
                                <div class="col-sm-10">
                                    {!! Form::checkbox('enabled', '1', $transactioncodes->enabled) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <label class="control-label">Default Price</label>
                            <input type="text" name="default_price" placeholder="Default Price" id="code" value="{{$transactioncodes->default_price}}" class="form-control ">
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Description</label>
                            <textarea name="description" class="form-control">{{$transactioncodes->description}}</textarea>
                        </div>

                    </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::submit( trans('general.button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    <a href="{!! route('admin.hotel.transaction.codes.index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>
        </div>


        </form>

    </div>
</div>


@endsection

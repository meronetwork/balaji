@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Room Block
                <small>Block a room</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

 <form method="post" action="{{route('admin.hotel.room-block-create')}}">
  {{ csrf_field() }}
<div class="panel panel-custom">
 <div class="panel-heading">
  <div class="row">
    <div class="col-md-7">
          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                              Select Room
                              </label>
                              <div class="col-md-6">

                    <div class="input-group ">
                                <select name="room_id" class="form-control searchable select2 input-sm"  id="guest_id" >
                                  
                                  @if(isset($selected_room))
                                  <option value="{{$selected_room->room_id}}">{{ucfirst(trans($selected_room->room_type->room_name))}}</option>
                                  @else
                                  <option value="">Select room</option>
                                  @foreach($room as $rm)
                                  <option value="{{$rm->room_id}}" >{{$rm->room_number}} ({{$rm->room_type->room_name}})</option>
                                  @endforeach
                                  @endif
                                </select>
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-building"></i></a>
                                </div>
                              </div>

                              </div>
          </div>
  </div>
 </div><br>
   <div class="row">

    <div class="col-md-6">
 <label class="control-label col-sm-5">From date</label>
                 <div class="form-group">  
               
                    <div class="input-group ">
                      <input required="" type="text" class="form-control occupied_date_from input-sm check_in" 
                      value="{{\Carbon\Carbon::now()->toDateString()
}}" name="from_date" id="from_date" readonly="">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                    </div>
                </div>
  </div>
 </div>
  <div class="row">
<div class="col-md-6">
                 <div class="form-group">  
                <label class="control-label col-sm-5">To date</label>
                    <div class="input-group ">
                      <input required="" type="text" class="form-control occupied_date_from input-sm check_in" 
                      value="{{\Carbon\Carbon::now()->toDateString()
}}" name="to_date" id="to_date" readonly="">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                    </div>
                </div>
  </div>
 </div>
<h3>Reason</h3>
 <div class="row">
   <div class="col-sm-6">
<div class="form-group">  
<div class="input-group ">
<textarea type="text" name="reason" placeholder="Write  reason...." id="description" class="form-control" ></textarea>
<div class="input-group-addon">
  <a href="#"><i class="fa  fa-sort-alpha-desc"></i></a>
</div>
</div>
</div>
   </div>
 </div>
   <div class="row">
 <div class="col-md-12">
        <div class="form-group">
            <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Create</button>
            <a href="/admin/hotel/room-block-index" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
        </div>
    </div>
</div>
</div>
</div>
  @endsection


@section('body_bottom')
  <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>


 <script type="text/javascript">
    $(function() {
            $('#from_date').datepicker({
                dateFormat: 'yy-m-d',
                sideBySide: true,
                minDate : 0,
         
        });
        $('#to_date').datepicker({
                dateFormat: 'yy-m-d',
                sideBySide: true,
                minDate : 0,
             
        });
    });
        function getroom(date){  
         $('#to_room').empty();    
         $('#to_room').append("<option value=''>Select room </option>")
         $.get('/admin/hotel/reservation/checkdate/'+date,function(data,status){
          for(let room of data){
          $('#to_room').append(("<option value="+room.room_number+">"+room.room_number+"("+room.room_name+")</option>"));
          }
        });
        }

        $('#extend_date').on('change',function(){
            let current_date = $(this).val();
            getroom(current_date);
         
        });
        
</script>
@endsection
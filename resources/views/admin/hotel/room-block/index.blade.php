@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
             Room  block
                <small>Blocked Room  Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
           
            <b><font size="4">Room block List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.hotel.room-block-create') }}">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Block new room</strong>
                        </a> 
            </div>      
        </div>
</div>

<table class="table table-hover table-no-border table-striped" id="leads-table">
<thead>
    <tr class="bg-info">
      
        <th>ID</th>
         <th>Room name</th>
        <th>Room Number</th>
        <th>Reason</th>
        <th>From date</th>
        <th>To date</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($blocked as $key=>$rm)
    <tr>
        
        <td>{{$rm->id}}</td>
        <td> {{ucfirst(trans($rm->room->room_type->room_name))}}</td>
        <td><b class="badge bg-primary">{{$rm->room->room_number}}</b> {{$rm->room->room_type->type_code}}</td>
        <td>{!! $rm->reason !!}</td>
        <td>{{date('dS M y', strtotime($rm->from_date))}}</td>
       <td>{{date('dS M y', strtotime($rm->to_date))}}</td>
        <td>
            @if( $rm->isEditable())<a href="{{route('admin.hotel.room-block-edit',$rm->id)}}"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
            &nbsp;&nbsp;
             <a href="{{route('admin.hotel.room-block-confirmdelete', $rm->id)}}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
 <!--        @if ( $rm->isDeletable() )
            <a href="{!! route('admin.hotel.room-block-confirmdelete', $rm->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
        @else
            <i class="fa fa-trash-o text-muted" title="{{ trans('admin/permissions/general.error.cant-delete-perm-in-use') }}"></i>
        @endif</td>
 -->    </tr>
    @endforeach

</tbody>
</table>
</div>

@endsection
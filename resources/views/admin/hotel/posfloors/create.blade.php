@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{ $page_title ?? "Page Title"}}
        <small> {{ $page_description ?? "Page Description" }}</small>
    </h1>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false) !!}
</section>

<div class='row'>
    <div class='col-md-12'>
        <div class="box">
            <div class="box-body ">
                <form method="post" action="/admin/hotel/posfloors" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">

                        <div class="col-md-4">
                            <label class="control-label">Name</label>
                            <input type="text" name="name" placeholder="Name" id="name" value="" class="form-control ">
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Outlet</label>

                                {!! Form::select('outlet_id',[''=>'Please Select']+$posoutlets, old('outlet_id'), ['class' => 'form-control input-sm'] )!!}

                            </div>
                        </div>



                    </div>


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::submit( trans('general.button.create'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    <a href="{!! route('admin.hotel.posfloors.index') !!}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>
            </div>
        </div>


        </form>

    </div>
</div>


@endsection

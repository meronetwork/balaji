@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<style type="text/css">
     @media print {
   
                #printElement {
                    display: block;
                }
            }
</style>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              POS Sales 
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
       <form  action="{{route('admin.pos.products.report')}}" method="post"> 
        {{csrf_field()}}
   	<div class="row">
        		<div class="col-md-6">
   <div class="panel panel-custom"> 

        <div class="panel-heading">
        	

        	<div class="row">
        		<div class="col-md-6">
        			<label >Start Date</label>
        			<div class="form-group">
        				<input type="text" name="start_date" class="form-control datepicker date-toogle" required="" placeholder="Start Date"
                        value="{{$request['start_date']}}">
        			</div> 
        		</div>
        		<div class="col-md-6">
        			<label >Start Time</label>
        			<div class="form-group">
        				<input type="text" name="start_time" class="form-control timepicker" required="" placeholder="Start time">
        			</div>
        		</div>
        	</div>

        		<div class="row">
        		<div class="col-md-6">
        			<label >End Date</label>
        			<div class="form-group">
        				<input type="text" name="end_date" class="form-control datepicker date-toogle" required="" placeholder="End Date"
                        value="{{$request['end_date']}}">
        			</div>
        		</div>
        		<div class="col-md-6">
        			<label >End Time</label>
        			<div class="form-group">
        				<input type="text" name="end_time" class="form-control timepicker" required="" placeholder="End Date">
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-12">
        			<label>Products Category</label>
        			<div class="form-group">
        				<select class="form-control searchable" name="product_type_id" required="">
                            <option value="">--Select Products Category--</option>
                            @foreach($products_cat as $o)
                                <option value="{{$o->id}}" @if($request['product_type_id'] == $o->id) selected=""  @endif>{{$o->name}}</option>
                            @endforeach            
                        </select>
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-12">
        			<label>Users</label>
        			<div class="form-group">
        				{!! Form::select('user_id',$users,$request['user_id'], ['class'=>'form-control searchable','placeholder'=>'Select Users']) !!}
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-12">
        			<label>Bill Type</label>
        			<div class="form-group">
        				<select class="form-control" name='bill_type'>
                            <option value="tax">Tax Bills</option>
                        </select>
        			</div>
        		</div>
        	</div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Load</button>
                    </div>
                </div>
            </div>




        </div>
    </div>
    </div>
  </div>
</form>

@if(count($sales) > 0)

    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Sales By Product 
                <small>
               {{date('dS M Y',strtotime($request['start_date']))}} - {{date('dS M Y',strtotime($request['end_date']))}}
                </small>
            </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-danger" id='printButton'><i class="fa fa-print"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
         @foreach($sales as $key=>$sal)
            
            <div class="col-md-6 printcontent">
                 <h3 style="padding: 5px;">{{$select_cat->name}} > {{$key}} </h3>
                <table class="table table-boardered">
                    <thead>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Amount</th>
                    </thead>
                    <?php 
                        $tqty = 0;
                        $total = 0;
                    ?>
                    @foreach($sal as $k=>$s)
                        <tr>
                            <td>{{ucfirst($s->name)}}</td>
                            <td>{{$s->quantity}}</td>
                            <td>{{$s->total}}</td>
                        </tr>
                        <?php 
                            $tqty += $s->quantity;
                           $total +=$s->total;
                        ?>
                    @endforeach
                    <tfoot>
                        <tr>
                        <th colspan="2" class="pull-right">Total Quantity</th>
                        <th>:</th>
                        <th>{{$tqty}}</th>
                    </tr>
                      <tr>
                        <th colspan="2" class="pull-right">Total Amount</th>
                         <th>:</th>
                        <th>{{$total}}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>

        @endforeach

              </div>
              <!-- /.row -->
            </div>
           
          </div>
       
        </div>
     
      </div>


  <div id='printElement' style="display: none"> 

  </div>


@elseif(count($sales) == 0 && $select_cat) 


    <div class="alert alert-info">
      <strong>Info!</strong> No any Product Found
    </div>
 

@endif



@endsection

@section('body_bottom')
@include('partials._date-toggle')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

 <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script type="text/javascript">
    

	$('.datepicker').datepicker({
		  dateFormat: 'yy-mm-dd',
        sideBySide: true,
	});

    $('.timepicker').datetimepicker({
        //inline: true,
        //format: 'YYYY-MM-DD',
        format: 'HH:mm',
        sideBySide: true
    });
     $('.date-toogle').nepalidatetoggle();


     $('.searchable').select2();
     $(function() {
        $('#filter-table').DataTable({
            pageLength: 25,
             buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });
    });
     function print() {
        $('#printElement').show();
        printJS({
            printable: 'printElement',
            type: 'html',
            targetStyles: ['*']
        });
        $('#printElement').hide();
}

$(function(){
    var printcontent = '<div align="center">Sales By Product <br>{{date('dS M Y',strtotime($request['start_date']))}} - {{date('dS M Y',strtotime($request['end_date']))}}</div>';
    $('.printcontent').each(function(){
        printcontent = printcontent + `<div class='col-md-12'>${$(this).html()}</div>`;

    });
    $('#printElement').html(printcontent);
    
})
document.getElementById('printButton').addEventListener ("click", print);

</script>
    <script src="/bower_components/admin-lte/plugins/datatables/extra/print.min.js"></script>
@endsection
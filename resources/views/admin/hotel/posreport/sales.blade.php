@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              POS Sales 
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
       <form  action="{{route('admin.pos.report')}}" method="post"> 
        {{csrf_field()}}
   	<div class="row">
        		<div class="col-md-6">
   <div class="panel panel-custom"> 

        <div class="panel-heading">
        	

        	<div class="row">
        		<div class="col-md-6">
        			<label >Start Date</label>
        			<div class="form-group">
        				<input type="text" name="start_date" class="form-control datepicker date-toogle" required="" placeholder="Start Date"
                        value="{{$request['start_date']}}">
        			</div> 
        		</div>
        		<div class="col-md-6">
        			<label >Start Time</label>
        			<div class="form-group">
        				<input type="text" name="start_time" class="form-control timepicker" required="" placeholder="Start time"
                        value="{{$request['start_time']}}">
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-6">
        			<label >End Date</label>
        			<div class="form-group">
        				<input type="text" name="end_date" class="form-control datepicker date-toogle" required="" placeholder="End Date"
                         value="{{$request['end_date']}}">
        			</div>
        		</div>
        		<div class="col-md-6">
        			<label >End Time</label>
        			<div class="form-group">
        				<input type="text" name="end_time" class="form-control timepicker" required="" placeholder="End Date"
                        value="{{$request['end_time']}}">
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-12">
        			<label>Outlets</label>
        			<div class="form-group">
        				<select class="form-control searchable" name="outlet_id" required="">
                            <option value="">--Select Outlets--</option>
                            @foreach($outlets as $o)
                                <option value="{{$o->id}}" @if($request['outlet_id'] == $o->id) selected="" @endif>{{$o->name}} #({{$o->outlet_code}} ({{$o->id}}))</option>
                            @endforeach            
                        </select>
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-12">
        			<label>Users</label>
        			<div class="form-group">
        				{!! Form::select('user_id',$users,$request['user_id'], ['class'=>'form-control searchable','placeholder'=>'Select Users']) !!}
        			</div>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-12">
        			<label>Bill Type</label>
        			<div class="form-group">
        				<select class="form-control" name='bill_type'>
                            <option value="tax">Tax Bills</option>
                        </select>
        			</div>
        		</div>
        	</div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Load</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
  </div>
</form>

@if($sales)
 <div class="panel panel-custom"> 
<div class="row">
<div class="col-md-12">
    <table class="table" id='filter-table'>
            <thead>
                <th>#SN</th>
                <th>Bill No</th>
                <th>Table</th>
       
                <th>Taxable Amt</th>
                <th>Tax Amt</th>
                <th>Total Amount</th>
                <th>Customer Pan</th>
                <th>Bill Date</th>

                <th>Customer</th>
                <th>Created By</th>
                <th>Comment</th>
            </thead>
            <?php 
                $ttaxable = 0;
                $ttax = 0;
                $tamount = 0;
            ?>
            <tbody>
                @foreach($sales as $key=>$s)
                    <tr>
                        <td>#{{$key + 1}}</td>
                        <td>{{$s->bill_no}}</td>
                        <td>{{$s->table}}</td>
                       
                        <td>{{$s->taxable_amount}}</td>
                        <td>{{$s->tax_amount}}</td>
                        <td>{{$s->total_amount}}</td>
                        <td>{{$s->customer_pan}}</td>
                        <td>{{ date('dS M Y',strtotime($s->bill_date)) }}</td>
                        <td>{{ $s->client->name }}</td>
                        <td>{{$s->user->username}}</td>
                         <td>{{$s->comment}}</td>
                    </tr>
                    <?php 
                        $ttaxable += $s->taxable_amount;
                        $ttax += $s->tax_amount;
                        $tamount += $s->total_amount;
                    ?>
                @endforeach
                 </tbody>
        <tfoot>
                <tr>
                    <th colspan="2">
                        
                    </th>
                    <th>Total : </th>
                    <th>{{$ttaxable}}</th>
                    <th>{{$ttax}}</th>
                    <th>{{$tamount}}</th>
                    <th colspan="5"></th>
                </tr>
            </tfoot>
           
           
            
    </table>
</div>
</div>
</div>
@endif



@endsection

@section('body_bottom')
@include('partials._date-toggle')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

 <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script type="text/javascript">
    

	$('.datepicker').datepicker({
		  dateFormat: 'yy-mm-dd',
        sideBySide: true,
	});

    $('.timepicker').datetimepicker({
        //inline: true,
        //format: 'YYYY-MM-DD',
        format: 'HH:mm',
        sideBySide: true
    });
     $('.date-toogle').nepalidatetoggle();


     $('.searchable').select2();
    //  $(function() {
    //     $('#filter-table').DataTable({
    //         pageLength: 25,
    //          buttons: [
    //         'copy', 'csv', 'excel', 'pdf', 'print'
    //     ]
    //     });
    // });
</script>

    <script src="/bower_components/admin-lte/plugins/datatables/extra/export.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
  var table = $('#filter-table').DataTable({
  
  dom: 'Bfrtip',
   buttons: [
            {
                extend: 'copyHtml5',
                title: 'Data export',

            },
            {
                extend: 'excelHtml5',
                title: 'Data export'
            },
            {
                extend: 'csvHtml5',
                title: 'Data export'
            },
            {
                extend: 'pdfHtml5',
                title: 'Sales Report \n {{date('dS M Y',strtotime($request['start_date']) )}} - {{date('dS M Y',strtotime($request['end_date']) )}}',
                footer: true

            }
        ],
  });

});
</script>
@endsection
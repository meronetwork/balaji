 @extends('layouts.master')
 @section('content')

 <link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
     <h1>
         Hotel Invoice List
         <small>
             Reprint invoice
         </small>
     </h1>
 </section>

 <div class='row'>
     <div class='col-md-12'>
         <!-- Box -->
         <div class="box box-primary"><br>
            <form method="GET" action="/admin/hotel/irdposting/index">
            <div class="row">
              <div class="col-md-12">
                    <div class="form-group">
                       <div class="col-md-2">
                          <input type="text" name="start_date" class="form-control input-sm datepicker date-toggle" placeholder="start Date"
                             value="{{ Request::get('start_date') }}">
                       </div>
                       <div class="col-md-2">
                          <input type="text" name="end_date" class="form-control input-sm datepicker date-toggle" placeholder="end Date"
                             value="{{ Request::get('end_date') }}">
                       </div>
                       <div class="col-md-1">
                          <input type="text" name="bill_no" class="form-control input-sm" placeholder="Bill no..." value="{{ Request::get('bill_no') }}">
                       </div>

                       <div class="col-md-2">
                          {!! Form::select('clients_id',$clients,Request::get('clients_id'),['class'=>'form-control input-sm searchable','placeholder'=>'Select Customer']) !!}
                       </div>
                       <div class="col-md-2">
                          {!! Form::select('paid_by',$pay_method,Request::get('paid_by'),
                          ['class'=>'form-control input-sm','placeholder'=>'Paid By']) !!}
                       </div>
                       <div class="col-md-2">
                          <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                          <a class="btn btn-danger btn-sm" href="/admin/hotel/irdposting/index" >Clear</a>
                       </div>
                    </div>
                    </div>
                <input type="hidden" name="search" value="true">

                </div><br/>
            </form>
             <div class="box-body">
                 <span id="index_lead_ajax_status"></span>
                 <div class="table-responsive">
                     <table class="table table-hover table-bordered table-striped" id="orders-table">
                         <thead>
                             <tr class="bg-teal">

                                 <th>Voucher # </th>
                                 <th>Bill # </th>
                                 <th>Bill Date</th>
                                 <th>Staff</th>
                                 <th>Resv #</th>
                                 <th>Paid</th>
                                 <th>Settlement</th>
                                 <th>Total</th>
                                 <th>Guest</th>
                                 <th>Realtime </th>
                                 <th>Sync</th>
                                 <th><i class="fa fa-times"></i> Print</th>
                                 <th>Reprint</th>
                                 <th>PaidBy</th>
                                 <th>Ledger Name</th>
                             </tr>
                         </thead>
                         <tbody>
                             <?php
                                        $folio_total_amount = 0;
                                   ?>
                             @if(isset($hotelbills) && !empty($hotelbills))
                             @foreach($hotelbills as $o)
                             <tr>
                                 <td>
                                     <?php
                                     $entry=\App\Models\Entry::find($o->posting_entry_id)
                                     ?>
                                     <a target="_blank" href="/admin/entries/show/{{\FinanceHelper::get_entry_type_label($entry->entrytype_id)}}/{{$entry->id}}">{{$entry->number}}</a>

                                 </td>

                                 <td> <a href="/admin/orders/{{$o->id  }}" data-toggle="modal" data-target="#modal_dialog"
                                    class="font-st" > {{$o->outlet->outlet_code}}{{ $o->bill_no }}</a> </td>

                                 <?php
                                    $paid_amount= \TaskHelper::getFolioPaymentAmount($o->reservation_id);

                                    $not_posted_ledgers = \App\Models\FolioDetail::where('folio_id',$o->folio_id)->where('posted_to_ledger',0)->get();
                                   ?>

                                 <td>{!! $o->bill_date !!}</td>
                                 <td>{!! $o->user->username !!}</td>
                                 <td>{{ env('RES_CODE')}}{!! $o->reservation_id !!}</td>
                                 <td>{!! number_format($paid_amount,2) !!}</td>


                                 @if(count($not_posted_ledgers)>0)
                                 <td><span class="label label-warning">Not Settle</span></td>
                                 @else
                                 <td><span class="label label-success">Settled</span></td>
                                 @endif

                                 <td class="font-st">{{env('APP_CURRENCY')}} {!! number_format($o->total_amount,2) !!}</td>
                                 <?php $folio_total_amount = $folio_total_amount+$o->total_amount; ?>

                                 <td>@if($o->reservation->company_id ?? null) {{ $o->reservation->client->name }}@else {{$o->reservation->guest_name??''}} @endif </td>

                                 <td>
                                     {{ $o->ordermeta->is_realtime }}
                                 </td>

                                 <td>
                                     {{ $o->ordermeta->sync_with_ird }}
                                 </td>
                                 <?php
                                      $print_no = \App\Models\POSPrint::where('order_id',$o->id)->count();
                                   ?>
                                 <td>
                                     {{$print_no}}
                                 </td>
                                 <td> <a href="/admin/hotel/invoice/type/select/{{$o->id}}" class="btn btn-foursquare btn-xs" data-toggle="modal" data-target="#modal_dialog">Print</a> </td>
                                 <td>{!! ReservationHelper::paidByArr($o->payments) !!}</td>
                                 <td>{{ $o->getPayingLedger() }}</td>
                             </tr>
                             @endforeach
                             @endif
                             <tr>
                                 <td colspan="7" class="text-right">
                                     Total Amount:
                                 </td>
                                 <td>
                                     <strong> {{env('APP_CURRENCY')}} {{ number_format($folio_total_amount,2) }} </strong>
                                 </td>
                             </tr>
                         </tbody>
                     </table>

                 </div> <!-- table-responsive -->



             </div><!-- /.box-body -->

             <div style="text-align: center;"> {!! $hotelbills->appends(\Request::except('page'))->render() !!} </div>

         </div><!-- /.box -->
         <input type="hidden" name="order_type" id="order_type" value="{{\Request::get('type')}}">

     </div><!-- /.col -->

 </div><!-- /.row -->
 @endsection


 <!-- Optional bottom section for modals etc... -->
 @section('body_bottom')
 <!-- DataTables -->
 <script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

 <script language="JavaScript">
     function toggleCheckbox() {
         checkboxes = document.getElementsByName('chkClient[]');
         for (var i = 0, n = checkboxes.length; i < n; i++) {
             checkboxes[i].checked = !checkboxes[i].checked;
         }
     }

 </script>
 <script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>


 <script type="text/javascript">
     $(document).on('change', '#order_status', function() {

         var id = $(this).closest('tr').find('.index_sale_id').val();

         var purchase_status = $(this).val();
         $.post("/admin/ajax_order_status", {
                 id: id
                 , purchase_status: purchase_status
                 , _token: $('meta[name="csrf-token"]').attr('content')
             }
             , function(data, status) {
                 if (data.status == '1')
                     $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                 else
                     $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                 $('#index_status_update').delay(3000).fadeOut('slow');
                 //alert("Data: " + data + "\nStatus: " + status);
             });

     });

 </script>
 <script type="text/javascript">
     $("#btn-submit-filter").on("click", function() {

         fiscal_id = $("#filter-fiscal").val();
         status = $("#filter-status").val();
         customer_id = $("#filter-customer").val();
         location_id = $("#filter-location").val();
         type = $("#order_type").val();

         window.location.href = "{!! url('/') !!}/admin/orders?fiscal_id=" + fiscal_id + "&status=" + status + "&customer_id=" + customer_id + "&location_id=" + location_id + "&type=" + type;
     });

     $("#btn-filter-clear").on("click", function() {

         type = $("#order_type").val();
         window.location.href = "{!! url('/') !!}/admin/orders?type=" + type;
     });

 </script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('.customer_id').select2();
         $('.searchable').select2();
     });

 </script>

 @endsection

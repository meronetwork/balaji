@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              Admin | Hotel | POS Menu
                <small>Admin | hotel | POS Menu Index</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">POS Menu List</font></b>
            <div style="display: inline; float: right;">
                <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.pos-menu.create') }}">
                   <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add new POS Menu</strong>
                </a> 
            </div>      
        </div>
</div>
<table class="table table-hover table-no-border table-striped" id="leads-table">

<thead>
    <tr>
       
        <th>ID</th>
        <th>Outlet Name</th>
        <th>Menu</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($posmenu as $key=>$att)

    <tr>
        

        <td>{{ $att->id }}</td>
        <td style="font-size: 16.5px">{{ $att->outlet->name }}</td>
        <td>{{ $att->menu_name }}</td>
        <td>
            @if( $att->isEditable())
            <a href="/admin/hotel/pos-menu/{{ $att->id }}/edit"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif

              @if ( $att->enabled )
            <i class="fa fa-check-circle enabled"></i>
            @else
            <i class="fa fa-ban disabled"></i>
            @endif
            
            <a href="{!! route('admin.hotel.pos-menu.confirm-delete', $att->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash-o deletable"></i></a>
    </tr>

    @endforeach

</tbody>
</table>
</div>

@endsection
@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              POS | Menu | Create 
                <small>{{$description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

  <form method="post" action="{{route('admin.pos-menu.update',$edit->id)}}">
    {{ csrf_field() }}
    <div class="panel panel-custom"> 
        <div class="panel-heading">

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Outlet</label>
                        <div class="input-group ">
                           {!! Form::select('outlet_id', [''=>'Select']+$outlets , $edit->outlet_id, ['class' => 'form-control outlet_id', required]) !!}
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Menu Name</label>
                        <div class="input-group ">
                            <input type="text" name="menu_name" placeholder="Meu Name" id="name" class="form-control" value="{{$edit->menu_name}}" required>
                            <div class="input-group-addon">
                                <a href="#"><i class="fa fa-stack-exchange"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                   <label>Enabled<i class="imp">*</i></label>
                    <input type="hidden" name="enabled" value="0">
                     {!! Form::checkbox('enabled', '1', $edit->enabled) !!} 
                  </div>
                </div>

            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn-submit-edit" type="submit">Update</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
 </form>

<script type="text/javascript">

    $(document).ready(function() {
        $('.outlet_id').select2();
    });

</script>

  @endsection
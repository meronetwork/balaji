@extends('layouts.master')
@section('content')

<style type="text/css">
.select-mini {
  font-size: 10px;
  height: 25px;
  width: 90px;
}

</style>

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
    {{$page_title}}
        <small> Add, Edit or Delete {{$page_title}}</small>
    </h1>
    <p> {{$page_description}}</p>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="panel panel-custom box">
    <div class="box-header with-border">
        <div class='row'>
            <div class='col-md-12'>
                <a href="#" class="filter-by-date" title="Filter"><i class="fa  fa-file"></i></a>&nbsp;
                <b><font size="4">{{$page_title}} List</font></b>
                <div style="display: inline; float: right;">
                    <a class="btn btn-info btn-sm"  title="Create" href="{{ route('admin.production.fact_bom.create') }}">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;<strong>Add {{$page_title}}</strong>
                    </a>

                </div>

                <div class="col-md-3" style="float: right; ">
                    <form method="get" action="/admin/production/fact_bom/search/" id="searchform">
                        <div class="input-group ">
                            <input type="text" placeholder="Search......"   class="form-control" style="height: 30px" name="slip" required="" value="{{request()->slip?request()->slip:''}}">
                            <div class="input-group-addon">
                                <a href="#" type="submit" onclick="document.getElementById('searchform').submit()"><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="filter_form" style="display: none;margin-top: 10px" >
        <form method="get" action="/admin/production/fact_bom/filter/">
            <div class="box-header with-border">
                <div class="col-md-4">

                    <div class="form-group">
                        <label for="date" class="col-sm-3 control-label">From Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input required="" type="text" class="form-control date_in"  name="from_date" id="from_date">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <label for="date" class="col-sm-3 control-label">To Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input required="" type="text" class="form-control date_in" value="{{ isset($selecteddate) ? $selecteddate : '' }}" name="to_date" id="to_date">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <button class="btn btn-primary" id="btn-submit-edit" type="submit" >Filter</button>
                </div>
            </div>
        </form>
    </div>

    <table class="table table-hover table-no-border" id="leads-table">
        <thead>
        <tr class="bg-info">

            <th>ID</th>
            <th>Date</th>
            <th>Name</th>
            <th>Product</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody id="myTable">
        @foreach($orders as $key=>$is)
            <tr>
                <td>{{$is->id}}</td>
                <td>{{ date('dS M Y', strtotime($is->created_at)) }}</td>
                <td>{{ucfirst($is->bom_name)}}</td>
                <td>{{ucfirst($is->product_name)}}</td>
                <?php
                $datas = '<a href="'.route('admin.production.fact_bom.confirm-delete', $is->id).'" data-toggle="modal" data-target="#modal_dialog" title="Delete"><i class="fa fa-trash-o deletable"></i></a>';
                ?>

                <td>

                    <a class="btn btn-round btn-xs btn-default" href="{{route('admin.production.fact_bom.edit',$is->id)}}" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a>&nbsp;&nbsp;


                    <a class="btn btn-round btn-xs bg-olive" href="{{route('admin.production.fact_bom.show',$is->id)}}" title="View"><i class="fa fa-file-text"></i></a>&nbsp;&nbsp;
                        <?php
                            echo $datas ?>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <div style="text-align: center;"> {!! $orders->render() !!} </div>
</div>

@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<!-- DataTables -->

<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkClient[]');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }

</script>



<script type="text/javascript">
    $(document).on('change', '#purchse_status', function() {

        var id = $(this).closest('tr').find('.index_purchase_id').val();

        var purchase_status = $(this).val();
        $.post("/admin/ajax_purchase_status", {
                id: id
                , purchase_status: purchase_status
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data, status) {
                if (data.status == '1')
                    $("#index_lead_ajax_status").after("<span style='color:green;' id='index_status_update'>Status is successfully updated.</span>");
                else
                    $("#index_lead_ajax_status").after("<span style='color:red;' id='index_status_update'>Problem in updating status; Please try again.</span>");

                $('#index_status_update').delay(3000).fadeOut('slow');
                //alert("Data: " + data + "\nStatus: " + status);
            });

    });

     $(function() {
        $('#start_date').datepicker({
                 //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d',
                sideBySide: true,

            });
        $('#end_date').datepicker({
                 //format: 'YYYY-MM-DD',
                dateFormat: 'yy-m-d',
                sideBySide: true,

            });
        });

</script>

@endsection

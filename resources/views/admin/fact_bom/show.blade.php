@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')

@endsection

@section('content')
<style>
    .box-comment {
        margin-bottom: 5px;
        padding-bottom: 5px;
        border-bottom: 1px solid #eee;
    }

    .box-comment img {
        float: left;
        margin-right: 10px;
    }

    .username {
        font-weight: bold;
    }

    .comment-text span {
        display: block;
    }

</style>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{$page_title}}
        <small> Show  {{$page_title}}</small>
    </h1>
    <p> {{$page_description}}</p>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>

<div class="panel panel-custom box">
    <div class="panel-heading">
        <div class='row'>
            <div class='col-md-12'>
                <b><font size="4">Factory BOM no# {{$ord->id}}</font></b>
                <div style="display: inline; float: right;">
                                <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.production.fact_bom.print',$ord->id) }}">
                                                <i class="fa  fa-print"></i>&nbsp;<strong>PRINT</strong>
                                </a>
                                 <a class="btn btn-danger btn-sm"  title="Import/Export Leads" href="javascript:history.back()">
                                                <i class="fa  fa-backward"></i>&nbsp;<strong>Back to list</strong>
                                </a>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-hover table-no-border" id="leads-table">
        <thead>
        <tr>
            <th style="text-align:center;width:20px !important">
                <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                    <i class="fa fa-check-square-o"></i>
                </a>
            </th>
            <th>ID</th>
            <th>Name</th>
            <th>Product</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td >
                <input type="checkbox" name="event_id" value="">
            </td>
            <td>{{$ord->id}}</td>
            <td>{{ucfirst(trans($ord->bom_name))}}</td>
            <td>{{ucfirst(trans($ord->product->name))}}</td>

        </tr>
        </tbody>
    </table>
    <div class="panel-heading">
        <div class='row'>
            <div class='col-md-12'>
                <b><font size="4">Factory BOM details</font></b>
            </div>
        </div>
    </div>
    <table class="table table-hover table-no-border" id="leads-table">
        <thead>
        <tr>
            <th style="text-align: right;" >#</th>
            <th>Raw Material</th>
            <th>Quantity</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orderDetails as $key=>$d)
            <tr>
                <td style="text-align: right;" >
                    {{$key + 1 }} .
                </td>
                <td>{{ucfirst(trans($d->product->name))}}&nbsp;</td>
                <td>{{$d->qty}}&nbsp;{{$d->symbol}}</td>


            </tr>
        @endforeach

        </tbody>
    </table>
</div>


@endsection

@section('body_bottom')
<!-- Select2 js -->
@include('partials._body_bottom_select2_js_user_search')
@endsection

@extends('layouts.master')

@section('content')
<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        {!! Form::open( array('route' => 'admin.audit.purge', 'id' => 'frmUserList') ) !!}
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{$page_title ?? "Page Title"}}</h3>
                &nbsp;
                {{-- <a class="btn btn-default btn-sm" href="{!! route('admin.audit.purge') !!}" title="{{ trans('admin/audit/general.button.purge', ['purge_retention' => $purge_retention]) }}">
                <i class="fa fa-eraser"></i>
                </a> --}}

                <div class="wrap" style="margin-top:5px;">
                    <div class="filter form-inline" style="margin:0 30px 0 0;">
                        {!! Form::text('start_date', \Request::get('start_date'), ['style' => 'width:120px;', 'class' => 'form-control', 'id'=>'start_date', 'placeholder'=>"Start Date"]) !!}&nbsp;&nbsp;
                        <!-- <label for="end_date" style="float:left; padding-top:7px;">End Date: </label> -->
                        {!! Form::text('end_date', \Request::get('end_date'), ['style' => 'width:120px; display:inline-block;', 'class' => 'form-control', 'id'=>'end_date', 'placeholder'=>"End Date"]) !!}&nbsp;&nbsp;

                        {!! Form::text('category', \Request::get('category'), ['style' => 'width:120px; display:inline-block;', 'class' => 'form-control', 'id'=>'category', 'placeholder'=>"Category"]) !!}&nbsp;&nbsp;

                        {!! Form::select('user_id', ['' => 'Select User'] + $users, \Request::get('user_id'), ['id'=>'filter-user', 'class'=>'form-control', 'style'=>'width:110px; display:inline-block;']) !!}
                        &nbsp;&nbsp;

                        <span class="btn btn-primary" id="btn-submit-filter">
                            <i class="fa fa-list"></i> Filter
                        </span>
                        <span class="btn btn-danger" id="btn-filter-clear">
                            <i class="fa fa-close"></i> Clear
                        </span>
                    </div>
                </div>

            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>{{ trans('admin/audit/general.columns.username') }}</th>
                                <th>{{ trans('admin/audit/general.columns.category') }}</th>
                                <th>{{ trans('admin/audit/general.columns.message') }}</th>
                                <th>{{ trans('admin/audit/general.columns.date') }}</th>
                                <th>{{ trans('admin/audit/general.columns.actions') }}</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>{{ trans('admin/audit/general.columns.username') }}</th>
                                <th>{{ trans('admin/audit/general.columns.category') }}</th>
                                <th>{{ trans('admin/audit/general.columns.message') }}</th>
                                <th>{{ trans('admin/audit/general.columns.date') }}</th>
                                <th>{{ trans('admin/audit/general.columns.actions') }}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($audits as $audit)
                            <tr>
                                <td>{{$audit->id}}</td>
                                <td>{{ ($audit->user) ? $audit->user->username : "N/A" }}</td>
                                <td>{{ $audit->category }}</td>
                                <td>{{ $audit->message }}</td>
                                <td>@userTimeZone($audit->created_at)</td>
                                <td>
                                    <a href="{!! route('admin.audit.show', $audit->id) !!}" title="{{ trans('general.button.display') }}"><i id="action-show" class="fa fa-eye"></i></a>
                                    @if ( $audit->replay_route )
                                    <a href="{!! route('admin.audit.replay', $audit->id) !!}" title="{{ trans('general.button.replay') }}"><i id="replay" class="fa fa-refresh spin-on-hover"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $audits->appends(\Request::except('page'))->render() !!}
                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->
        {!! Form::close() !!}
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- Add a spinner to the action replay icon while mouse is hovering. -->
<script language="JavaScript">
    $('.spin-on-hover').hover(
        function() {
            $(this).addClass('fa-spin');
        }
        , function() {
            $(this).removeClass('fa-spin');
        }
    );

</script>
<script>
    $(function() {
        $('#start_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD HH:mm'
            , sideBySide: true
        });
        $('#end_date').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD HH:mm'
            , sideBySide: true
        });
    });

    $("#btn-submit-filter").on("click", function() {

        user_id = $("#filter-user").val();
        start_date = $("#start_date").val();
        end_date = $("#end_date").val();
        category = $("#category").val();

        window.location.href = "{!! url('/') !!}/admin/audit?user_id=" + user_id + "&start_date=" + start_date + "&end_date=" + end_date + "&category=" + category;
    });

    $("#btn-filter-clear").on("click", function() {
        type = $("#lead_type").val();
        window.location.href = "{!! url('/') !!}/admin/audit";
    });

</script>
@endsection

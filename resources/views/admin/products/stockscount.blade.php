@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              {!! $page_title ?? "Page title" !!}
               
                <small>{!! $page_description or "Page description" !!}</small>
            </h1>
            

             <br/>

           {{ TaskHelper::topSubMenu('topsubmenu.purchase')}}

            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

<div class="box box-header">
   
    <div class="">
       
      

        <div style="min-height:200px" class="" id="">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="bg-purple">
                      <th class="text-center">Id</th>
                      <th class="text-center">Product</th>
                        <th class="text-center">Total In</th>
                        <th class="text-center">Total Out</th>
                        <th class="text-center"> <i class="fa fa- fa-hand-paper-o"></i> On Hand</th>
                    </tr>
                </thead>
                <tbody>  
                     <?php
                    $sum = 0;
                    $StockIn = 0;
                    $StockOut = 0;
                    ?>
                    @if(count($transations)>0)
                    @foreach($transations as $result)
                  @php $stockOutLine = $result->where('qty','<','0')->sum('qty');   
                      $stockInLine = $result->where('qty','>','0')->sum('qty');
                  @endphp
                      <tr>
                        <td align="center">{{$result->first()->pid}}</td>
                      <td style="font-size: 16.5px" align="left"><a href="/admin/products/{{$result->first()->pid}}/edit?op=trans" target="_blank"> {{$result->first()->name}}</a></td>
                  
                      <td align="center">
                        {{ abs($stockInLine) }}
                      </td>
                      <td align="center">
                        {{ abs($stockOutLine) }}
                      </td>
                      <td align="center">
                          {{ $stockInLine - abs($stockOutLine)  }}

                      </td>
                    </tr>
                    @endforeach
                  
                    @else
                    <tr>
                        <td colspan="6" class="text-center text-danger">No Transaction Yet</td>
                    </tr>
                   @endif

                </tbody>
            </table>

            
        </div>

      

        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>
@endsection
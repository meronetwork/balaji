@extends('layouts.master')
@section('content')

<style>
  #tasks-table td:first-child{text-align: center !important;}
  #tasks-table td:nth-child(2){font-weight: bold !important;}
  #tasks-table td:last-child a {margin:0 2px;}
  /*td { text-align:center; }*/
</style>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
              {{$page_title}}
                <small>{{$page_description}}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/buttons.dataTables.css") }}" rel="stylesheet" type="text/css" />

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
{{--            {!! Form::open( array('route' => 'admin.lostAndFound.enable-selected', 'id' => 'frmTaskList') ) !!}--}}
                <div class="box box-primary">
                    <div class="box-header with-border">

                        <a class="btn btn-primary btn-sm" href="{!! route('admin.lostAndFound.create') !!}"
                           >
                            <i class="fa fa-plus-square"></i>&nbsp;&nbsp;<strong>Add New Lost/Found</strong>
                        </a>

{{--                        <div class="filter form-inline" style="display:inline-block; float:right;">--}}
{{--                            {!! Form::label('assign_to', trans('admin/tasks/general.columns.assigned_to')) !!}: &nbsp;--}}
{{--                            {!! Form::select('assign_to', ['' => 'Select user'] + $users, \Request::get('assign_to'), ['id'=>'filter-assign_to', 'class'=>'form-control']) !!}--}}
{{--                            &nbsp;&nbsp;--}}
{{--                            {!! Form::button( 'Filter', ['class' => 'btn btn-primary', 'id' => 'btn-submit-filter'] ) !!}--}}
{{--                            {!! Form::button( 'Clear', ['class' => 'btn btn-danger', 'id' => 'btn-filter-clear'] ) !!}--}}
{{--                        </div>--}}
                    </div>
                    <div class="box-body">
                        <span id="index_lead_ajax_status"></span>

                        <div class="">
                            <table class="table table-hover table-bordered" id="tasks-table">
                                <thead>
                                    <tr class="bg-info">
                                        <th class="text-center">SNo</th>
                                        <th>Date</th>
                                        <th>Guest Info</th>
                                        <th>Item</th>
                                        <th class="text-center">Status</th>
                                        <th>Founder</th>
                                        <th>Location</th>
                                        <th>Admin</th>
                                        <th>{{ trans('admin/tasks/general.columns.actions') }}</th>
                                    </tr>
                                </thead>
                                 <tbody>
                                @if(isset($lost_and_founds) && !empty($lost_and_founds))
                                    @foreach($lost_and_founds as $lk => $lost_and_found)
                                    <tr>
                                        <td>{{($lk+1+(\Request::get('page')?\Request::get('page'):1-1)*20)}}.
                                        </td>
                                        <td class="">{{ date('d M Y',strtotime($lost_and_found->date)) }}
                                            <div style="font-size: 13px;color: grey">{{$lost_and_found->time}}</div>
                                         </td>
                                        <td>
                                            {{$lost_and_found->guest_name}}
                                            <div style="font-size: 13px;color: grey">{{$lost_and_found->phone}}</div>
                                        </td>
                                        <td>
                                            {{$lost_and_found->item}}
                                        </td>
                                        <td class="text-center">
                                            <span class="label label-primary item_status" style="font-size: 100%" data-type="select"
                                                  data-pk="1" data-title="Select Status" data-value="{{$lost_and_found->status}}">
                                                {{$lost_and_found->status=='time_out'?'Time Out':ucfirst($lost_and_found->status)}}
                                            </span>
                                        </td>


                                         <td class="">
                                             {{$lost_and_found->founder}}
                                        </td>
                                        <td class="">
                                             {{$lost_and_found->location}}
                                        </td>
                                        <td class="">
                                             {{$lost_and_found->createdBy->full_name}}
                                        </td>
                                        <td>
                                            <?php
                                                $datas = '';
                                                if ( $lost_and_found->isEditable())
                                                    $datas .= '<a href="'.route('admin.lostAndFound.edit', $lost_and_found->id).'" title="{{ trans(\'general.button.edit\') }}"> <i class="fa fa-edit"></i> </a>';
                                                else
                                                    $datas .= '<i class="fa fa-edit text-muted" title="{{ trans(\'admin/leads/general.error.cant-edit-this-lead\') }}"></i>';


                                                if ( $lost_and_found->isDeletable() )
                                                    $datas .= '<a href="'.route('admin.lostAndFound.confirm-delete', $lost_and_found->id).'" data-toggle="modal" data-target="#modal_dialog" title="{{ trans(\'general.button.delete\') }}"><i class="fa fa-trash deletable"></i></a>';
                                                else
                                                    $datas .= '<i class="fa fa-trash text-muted" title="{{ trans(\'admin/leads/general.error.cant-delete-this-lead\') }}"></i>';

                                                echo $datas;
                                            ?>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->

                <div style="text-align: center;"> {!! $lost_and_founds->appends(\Request::except('page'))->render() !!} </div>
          {!! Form::close() !!}
        </div><!-- /.col -->

    </div><!-- /.row -->

@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.buttons.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/buttons.server-side.js") }}"></script>
    <link href="/x-editable/bootstrap-editable.css" rel="stylesheet" />
    <script src="/x-editable/bootstrap-editable.min.js"></script>
<script language="JavaScript">
    function toggleCheckbox() {
        checkboxes = document.getElementsByName('chkTask[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = !checkboxes[i].checked;
        }
    }
    function makechanges(value, type, parent,element) {
       if (type == 'status') {
            let parent_el = parent.find('.status_id');
            console.log(element)
            debugger
            value=='Started'?
            element.css('background','#4B77BE'):value=='Open'?
            element.css('background','#8F1D21'):value=='Completed'?
            element.css('background','#26A65B'):element.css('background','pink')
        }
    }
    function handleChange(task_id, value, type, parent,element) {
        $.post("/admin/ajax_task_update", {
                id: task_id
                , update_value: value
                , type: type
                , _token: $('meta[name="csrf-token"]').attr('content')
            }
            , function(data) {
                if (data.status == '1') {
                    makechanges(value, type, parent,element);
                    $("#ajax_status").after("<span style='color:green;' id='status_update'>" + type + " sucessfully updated</span>");
                    $('#status_update').delay(3000).fadeOut('slow');
                }

                //alert("Data: " + data + "\nStatus: " + status);
            });
    }
    var statuses = <?php echo json_encode(['lost'=>'Lost','found'=>'Found','delivered'=>'Delivered',
        'time_out'=>'Time Out']); ?>;

    $('.item_status').each(function() {
        let parent = $(this).parent().parent();
        let task_id = parent.find('.index_task_id').val();
        $(this).editable({
            source: statuses
            , success : function(response, newValue) {
                handleChange(task_id, newValue, 'priority', parent,$(this));
            }
        });
    });

</script>

<script>

$("#btn-submit-filter").on("click", function () {
    assign_to = $("#filter-assign_to").val();
    window.location.href = "{!! url('/') !!}/admin/lostAndFound?assign_to="+assign_to;
});
$("#btn-filter-clear").on("click", function () {
    window.location.href = "{!! url('/') !!}/admin/lostAndFound";
});
</script>

@endsection

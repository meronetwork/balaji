@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>
            {{$page_title}}
            <small>{{$page_description}}</small>
        </h1>
        {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
    </section>
    <style>
        select { width:200px !important; }
    </style>
    <style>
        .ui-state-active h4,
        .ui-state-active h4:visited {
            color: #000000;
        }

        .ui-menu-item {
            height: 40px;
            border: 1px solid #ececf9;
        }

        .ui-menu-item:hover {
            background-color: lightblue !important;
            border: 1px solid lightblue !important;

        }

        .ui-widget-content .ui-state-active {
            background-color: white !important;
            border: none !important;
        }

        .list_item_container {
            width: 400px;
            height: 40px;
            float: left;
            margin-left: 5px;
        }

        .ui-widget-content .ui-state-active .list_item_container {
            background-color: #f5f5f5;
        }

        .label {
            width: 85%;
            float: left;
            white-space: nowrap;
            overflow: hidden;
            color: #000000;
            text-align: left;
        }

        input:focus {
            background-color: #f5f5f5;
        }

        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height: 100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }

        input.form-control {
            min-width: 55px !important;
        }

        select {
            min-width: 80px !important;

        }

        .p_sn {
            max-width: 3px !important;
        }

        @media only screen and (max-width: 770px) {
            input.total {
                width: 140px !important;
            }
        }
    </style>

    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <div class='row'>
        <div class='col-md-12'>
            <div class="box">
                <div class="box-body">
{{--                    {!! Form::open( ['route'('admin.lostAndFound.update',$lost_and_found->id), 'id' => 'form_edit_task'] ) !!}--}}

                    <form method="post" action="{{route('admin.lostAndFound.update',$lost_and_found->id)}}" enctype="multipart/form-data" id='form_edit_task'>
                        {{ csrf_field() }}
                        {{--                <div class="content col-md-9">--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('room_num', 'Room No.') !!}
                                {!! Form::text('room_no', $lost_and_found->room_no, ['class' => 'form-control', 'id'=>'lead_id']) !!}
                                <input type="hidden" name="leadId" value="0">
                                <span>
                                    <small class="text-muted">
                                        Type in customer name or reservation number or room number
                                    </small>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('name', 'Guest Name') !!}
                                {!! Form::text('guest_name',  $lost_and_found->guest_name, ['class' => 'form-control','id'=>'guest_name', 'placeholder'=>'Guest Name']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('phone', 'Contact No.') !!}
                                {!! Form::text('phone',  $lost_and_found->phone, ['class' => 'form-control','id'=>'phone', 'placeholder'=>'Phone No.']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::text('email',  $lost_and_found->email, ['class' => 'form-control','id'=>'email', 'placeholder'=>'Email']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('item', 'Item') !!}
                                {!! Form::text('item',  $lost_and_found->item, ['class' => 'form-control', 'placeholder'=>'Particular']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('date', 'Date & Time') !!}
                                {!! Form::text('date',  date('d-m-Y',strtotime($lost_and_found->date)).' '. $lost_and_found->time, ['class' => 'form-control', 'id'=>'task_start_date','required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('status', trans('admin/tasks/general.columns.task_status')) !!}
                                {!! Form::select('status', ['lost'=>'Lost','found'=>'Found','delivered'=>'Delivered','time_out'=>'Time Out'],  $lost_and_found->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('description', trans('admin/tasks/general.columns.task_detail')) !!}
                                {!! Form::textarea('description',  $lost_and_found->description, ['class' => 'form-control', 'rows'=>'5']) !!}
                            </div>
                        </div>


                    </div>
                    {{--                </div><!-- /.content -->--}}


                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::button( trans('general.button.update'), ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                            <a href="{!! route('admin.lostAndFound.index') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                        </div>
                    </div>

                    </form>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@section('body_bottom')
    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>

    <script>
        $(function() {
            $('#task_start_date').datetimepicker({
                //inline: true,
                //format: 'YYYY-MM-DD HH:mm',
                format: 'DD-MM-YYYY HH:mm',
                sideBySide: true
            });
            $('#task_due_date').datetimepicker({
                //inline: true,
                //format: 'YYYY-MM-DD HH:mm',
                format: 'DD-MM-YYYY HH:mm',
                sideBySide: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#lead_id").autocomplete({
                source: "/admin/getReservation",
                minLength: 1,
                focus: function (event, ui) {

                    return false;
                },
                select: function (event, ui) {
                    setTimeout(() => {
                        $('#guest_name').val(ui.item.guest_name)
                        $('#phone').val(ui.item.phone)
                        $('#email').val(ui.item.email)
                    }, 1)

                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                // console.log(JSON.stringify(item))
                // debugger
                var inner_html = '<div class="list_item_container" style="cursor:pointer"><div class="label"><h4><span style="color: grey">'+'Room '+item.value+'</span>'+': [Resv # '+item.id+'] ' + item.guest_name + '</h4></div></div>';
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
            };
        });
    </script>
    <!-- form submit -->
    <script type="text/javascript">
        $("#btn-submit-edit").on("click", function () {
            // Post form.
            $("#form_edit_task").submit();
        });
    </script>
@endsection

@extends('layouts.master')

<!-- jVectorMap 1.2.2 -->
<link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet"
      type="text/css"/>
<style>
    .filter_date {
        font-size: 14px;
    }
</style>
@section('content')
    <script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
    <link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet"/>

    <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>
            Creditors List
        </h1>
    </section>
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class='row'>
                <div class='col-md-12 d-flex'>
                    <div class="wrap" style="margin-top:5px;">
                        <form method="get" action="/admin/creditors_lists">
                            <div class="filter form-inline" style="margin:0 30px 0 0;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="">Select Creditor</label>
                                        <br>
                                        <select class="form-control input-sm select2" style="width: 260px;"
                                                name="ledger_id">
                                            <option value="">Select Creditor</option>
                                            @foreach($all_creditors as $key=>$cred)
                                                <option value="{{$cred['id']}}"
                                                        @if(Request::get('ledger_id') == $cred['id']) selected="" @endif>{{$cred['name']}} </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <div class="input-group">
                                                <input id="ReportStartdate" type="text" name="startdate" class="form-control input-sm datepicker" value="{{\Request::get('startdate')}}">
                                                <div class="input-group-addon">
                                                    <i>
                                                        <div class="fa fa-info-circle" data-toggle="tooltip" title="Note : Leave start date as empty if you want statement from the start of the financial year.">
                                                        </div>
                                                    </i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>End Date</label>

                                            <div class="input-group">
                                                <input id="ReportEnddate" type="text" name="enddate" class="form-control input-sm datepicker" value="{{\Request::get('enddate')}}">
                                                <div class="input-group-addon">
                                                    <i>
                                                        <div class="fa fa-info-circle" data-toggle="tooltip" title="Note : Leave end date as empty if you want statement till the end of the financial year.">
                                                        </div>
                                                    </i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <!-- /.form group -->
                                    </div>

                                    {{--                                    <div class="col-md-2" style="margin-left: 5px;">--}}
                                    {{--                                        <label for="">Select Standing Date</label>--}}
                                    {{--                                        <br>--}}
                                    {{--                                        {!! Form::text('date', \Request::get('date'), ['style' => 'width:170px;', 'class' => 'form-control input-sm  datepicker', 'id'=>'start_date', 'placeholder'=>'Select Standing Date','autocomplete' =>'off']) !!}&nbsp;&nbsp;--}}

                                    {{--                                    </div>--}}
                                    <div class="col-md-2">
                                        <label for="">Fiscal Year</label>
                                        <div class="form-group">
                                            <div class="input-group">
                                                {!! Form::select('fiscal_year',$allFiscalYear,$fiscal_year,['id'=>'fiscal_year_id', 'class'=>'form-control searchable input-sm', 'style'=>'width:150px; display:inline-block;'])  !!}

                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                    <div class="col-md-3">
                                        <label></label>
                                        <div class="form-group" style="margin-top: 24px;">
                                            <button class="btn btn-primary btn-sm" id="btn-submit-filter" type="submit">
                                                <i class="fa fa-list"></i> Filter
                                            </button>
                                            <a href="/admin/creditors_lists" class="btn btn-danger btn-sm"
                                               id="btn-filter-clear">
                                                <i class="fa fa-close"></i> Clear
                                            </a>
                                            <a href="/admin/creditors_lists?ledger_id={{Request::get('ledger_id')}}&export=true&fiscal_year={{Request::get('fiscal_year')}}"
                                               class="btn btn-success btn-sm" id="btn-filter-clear">
                                                <i class="fa file-export"></i> Export To Excel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="debtors_list-tabs" style="position: relative;">
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->

                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="{{route('admin.creditors_lists')}}" aria-expanded="true">List View</a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.creditors_lists.ageing')}}" aria-expanded="false">Ageing View
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content bg-white">

                        <div class="tab-pane active" id="my_debtors_list">
                            <div>

                                <span id="index_lead_ajax_status"></span>

                                <div>
                                    <table id="example" class="table table-striped table-hover table-bordered">
                                        <thead>
                                        <tr class="bg-primary">
                                            <th style="width: 5%;text-align: center;">Sno.</th>
                                            <th style="width: 12%;">Customer Code</th>
                                            <th>Customer</th>
                                            <th style="width: 10%; text-align: center">Opening B/c(DR)</th>
                                            <th style="width: 10%; text-align: center">Opening B/c(CR)</th>
                                            <th style="width: 10%; text-align: center">Debit Amt</th>
                                            <th style="width: 10%; text-align: center">Credit Amt</th>
                                            <th style="width: 10%; text-align: center">Closing B/c</th>
                                            @if($current_fiscal->fiscal_year==$fiscal_year)
                                            <th style="width: 10%;text-align: center">Action</th>
                                                @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($creditorsLists as $key=>$entries)

                                            <tr>
                                                <td style="text-align: center">{{ $key+1  }}.</td>
                                                <td style="text-align: center">{{ $entries['code']  }}</td>
                                                <td style="font-size: 16.5px">
                                                    <a href="/admin/accounts/reports/ledger_statement?ledger_id={{ $entries['id'] }}"
                                                       target="_blank">    {{ $entries['name'] }}</a>
                                                </td>
                                                <td style="text-align: center;font-size: 16.5px">
                                                    {{$entries['opening_blc_dc']=='D'?number_format($entries['opening_blc'],2):0 }}</td>
                                                <td style="text-align: center;font-size: 16.5px">
                                                    {{$entries['opening_blc_dc']=='C'?number_format($entries['opening_blc'],2):0 }}</td>
                                                <td style="text-align: center;font-size: 16.5px">
                                                    {{ number_format($entries['dr_amount'],2) }}</td>
                                                <td style="text-align: center;font-size: 16.5px">
                                                    {{ number_format($entries['cr_amount'],2) }}</td>
                                                <td style="text-align: center;font-size: 16.5px">
                                                    {{ number_format($entries['amount'],2) }}</td>
                                                @if($current_fiscal->fiscal_year==$fiscal_year)
                                                <td style="text-align: center">

                                                    <a href="/admin/creditors_pay/{{ $entries['id'] }}"
                                                       class="btn btn-sm btn-primary" data-toggle="modal"
                                                       data-target="#modal_dialog">Pay Now</a>
                                                </td>
                                                    @endif
                                            </tr>
                                            @php $totalSum +=  $entries['amount']; @endphp
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td class="text-right" colspan="3" style="font-size: 20.5px">Total</td>
                                            <td colspan="5" class="text-right" style="font-size: 20.5px;">{{ env('APP_CURRENCY') }} {{ number_format($totalSum,2) }} </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        $('.datepicker').datetimepicker({
//inline: true,
            format: 'YYYY-MM-DD',
            sideBySide: true
        });
        $('.select2').select2();

        $(document).ready(function() {
            $("#example").DataTable();
        });
    </script>
@endsection

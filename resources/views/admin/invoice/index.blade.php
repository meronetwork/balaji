@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                {{ $page_title }}
                <small>{{ $page_description }}</small>
            </h1>
            <p> This is useful if you are registered in VAT</p>
            Current Fiscal Year: <strong>{{ FinanceHelper::cur_fisc_yr()->fiscal_year}}</strong>

            <p> 

                <a href="/admin/invoice1"> Invoices </a> | <a href="/admin/taxinvoice/renewals"> Renewals </a>

            </p>

            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>


    <div class='row'>

        <div class='col-md-12'>

            <!-- Box -->
            {!! Form::open( array('route' => 'admin.orders.enable-selected', 'id' => 'frmClientList') ) !!}
                <div class="box box-primary">
                    <div class="box-header with-border">

                         <a class="btn btn-primary btn-sm pull-right"  title="Create new invoice" href="{{ route('admin.invoice.create') }}">
                            <i class="fa fa-plus"></i>&nbsp;<strong> Create new tax invoice</strong>
                        </a> 

                      

                    </div>
                    <div class="box-body">

                        <div class="table-responsive">
                           
                            <table class="table table-hover table-bordered table-striped" id="orders-table">
                                <thead>
                                    <tr class="bg-danger">
                                        <th>
                                        Bill date 
                                        </th>
                                        <th>Bill No.</th>
                                        <th>Customer name</th>
                                        <th>Due date</th>
                                        <th>Total</th>
                                        <th>Tools</th>
                                        <th>Post To AR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($orders) && !empty($orders)) 
                                    @foreach($orders as $o)
                                        <tr>
                                            <td>{{ date('D dS M y',strtotime($o->bill_date))}}</td>
                                            <td>#{{ $o->bill_no }}</td>
                                            <td><span style="font-size: 16.5px"> <a href="/admin/invoice1/{{$o->id}}"> {{ $o->client->name }}</a> <small>{{ $o->name }}</small> </span></td>
                                            <td>{{ date('D dS M y',strtotime($o->due_date))}}</td>
                                            <td>{!! number_format($o->total_amount,2) !!}</td>
                                            <td>
                                                <a href="/admin/invoice/print/{{$o->id}}" target="_blank" title="print"><i class="fa fa-print"></i></a>
                                                <a href="/admin/invoice/payment/{{$o->id}}" title="download"><i class="fa fa-credit-card"></i></a>
                                                @if(!$o->invoicemeta->sync_with_ird)
                                                  <a href="{{ route('admin.invoice.edit',$o->id) }}" title="edit"><i class="fa fa-edit"></i></a>
                                                @endif
                                            </td>
                                            <td>@if($o->invoicemeta->sync_with_ird != 1)<a href="invoice/posttoird/{{$o->id}}" class="btn btn-primary btn-xs">Post To AR</a>@endif</td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>

                            {!! $orders->render() !!}

                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            {!! Form::close() !!}
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

    <script language="JavaScript">
        function toggleCheckbox() {
            checkboxes = document.getElementsByName('chkClient[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = !checkboxes[i].checked;
            }
        }
    </script>

    <script>
	$(function() {
		$('#orders-table').DataTable({
            pageLength: 25,
            ordering: false
		});
	});
	</script>

@endsection

@extends('layouts.master')
@section('content')

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    

.file-box {
  float: left;
  width: 220px;
  min-height: 220px;
}
.file-manager h5 {
  text-transform: uppercase;
}
.file-manager {
  list-style: none outside none;
  margin: 0;
  padding: 0;
}
.folder-list li a {
  color: #666666;
  display: block;
  padding: 5px 0;
}
.folder-list li {
  border-bottom: 1px solid #e7eaec;
  display: block;
}
.folder-list li i {
  margin-right: 8px;
  color: #3d4d5d;
}
.category-list li a {
  color: #666666;
  display: block;
  padding: 5px 0;
}
.category-list li {
  display: block;
}
.category-list li i {
  margin-right: 8px;
  color: #3d4d5d;
}
.category-list li a .text-navy {
  color: #1ab394;
}
.category-list li a .text-primary {
  color: #1c84c6;
}
.category-list li a .text-info {
  color: #23c6c8;
}
.category-list li a .text-danger {
  color: #EF5352;
}
.category-list li a .text-warning {
  color: #F8AC59;
}
.file-manager h5.tag-title {
  margin-top: 20px;
}
.tag-list li {
  float: left;
}
.tag-list li a {
  font-size: 10px;
  background-color: #f3f3f4;
  padding: 5px 12px;
  color: inherit;
  border-radius: 2px;
  border: 1px solid #e7eaec;
  margin-right: 5px;
  margin-top: 5px;
  display: block;
}
.file {
  border: 1px solid #e7eaec;
  padding: 0;
  background-color: #ffffff;
  position: relative;
  margin-bottom: 20px;
  margin-right: 20px;
}
.file-manager .hr-line-dashed {
  margin: 15px 0;
}
.file .icon,
.file .image {
  height: 100px;
  overflow: hidden;
}
.file .icon {
  padding: 15px 10px;
  text-align: center;
}
.file-control {
  color: inherit;
  font-size: 11px;
  margin-right: 10px;
}
.file-control.active {
  text-decoration: underline;
}
.file .icon i {
  font-size: 70px;
  color: #dadada;
}
.file .file-name {
  padding: 10px;
  background-color: #f8f8f8;
  border-top: 1px solid #e7eaec;
}
.file-name small {
  color: #676a6c;
}
ul.tag-list li {
  list-style: none;
}
.corner {
  position: absolute;
  display: inline-block;
  width: 0;
  height: 0;
  line-height: 0;
  border: 0.6em solid transparent;
  border-right: 0.6em solid #f1f1f1;
  border-bottom: 0.6em solid #f1f1f1;
  right: 0em;
  bottom: 0em;
}
a.compose-mail {
  padding: 8px 10px;
}
.mail-search {
  max-width: 300px;
}
.ibox {
  clear: both;
  margin-bottom: 25px;
  margin-top: 0;
  padding: 0;
}
.ibox.collapsed .ibox-content {
  display: none;
}
.ibox.collapsed .fa.fa-chevron-up:before {
  content: "\f078";
}
.ibox.collapsed .fa.fa-chevron-down:before {
  content: "\f077";
}
.ibox:after,
.ibox:before {
  display: table;
}
.ibox-title {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #ffffff;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 3px 0 0;
  color: inherit;
  margin-bottom: 0;
  padding: 14px 15px 7px;
  min-height: 48px;
}
.ibox-content {
  background-color: #ffffff;
  color: inherit;
  padding: 15px 20px 20px 20px;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 1px 0;
}
.ibox-footer {
  color: inherit;
  border-top: 1px solid #e7eaec;
  font-size: 90%;
  background: #ffffff;
  padding: 10px 15px;
}
a:hover{
text-decoration:none;    
}

</style>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Documents Management
                <small>{!! $page_description or "Page description" !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

    <div class='row'>
        
<div class="">
<div class="row">
    <div class="col-md-3">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="file-manager">
                   
                    <a  href="{!! route('admin.documents.create') !!}" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_dialog">Upload Files</a><hr>
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search Documents.." id='search_inp'>
                      <div class="input-group-btn">
                        <button class="btn btn-default" type="button" id='search'>
                          <i class="glyphicon glyphicon-search"></i>
                        </button>
                      </div>
                    </div><hr>
                    <div class="hr-line-dashed"></div>
                    <h5>Folders</h5>
                    <ul class="folder-list" style="padding: 0">
                      <li @if(!\Request::get('folder'))class="bg-danger" @endif><a href="/admin/documents/"><i class="fa fa-home"></i>Home</a></li>
                      @foreach($folders as $f)
                      @if(\Request::get('folder') == $f->id) 
                        <li class="bg-danger"><a href="/admin/documents/?folder={{ $f->id }}"><i class="fa fa-folder"></i>{{ $f->name }}</a></li>
                      @else
                        <li><a href="/admin/documents/?folder={{ $f->id }}"><i class="fa fa-folder"></i>{{ $f->name }}</a></li>
                      @endif
                      @endforeach
                    </ul>
                    <h5 class="tag-title">Categories</h5>
                    <ul class="tag-list" style="padding: 0">
                      @foreach($category as $c)
                      @if($c->category)
                        <li class="change-category" data-id='{{ $c->category->id }}' ><a href="#">{{ ucfirst($c->category->name) }}</a></li>
                      @endif
                      @endforeach
                    </ul>
                    <h5><button class="btn btn-xs btn-primary btn-block">Doc Type</button></h5>
                      <div class="radio">
                        <label><input type="radio" name="doctype" value="mydoc" checked>My Documents</label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="doctype" value="shared" 
                          @if(\Request::get('type') && \Request::get('type') == 'shared') checked="" 
                          @endif 
                          >
                        Document Shared With Me</label>
                      </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <?php 
        $mime_images = ['png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml'];

        $mime_video =[
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

        ];
        $mime_audio = [ 'mp3' => 'audio/mpeg','audio/x-m4a'];

        $mime_office = [ 'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'ppt' => 'application/vnd.ms-powerpoint'];

        $mime_doc = ['application/msword'];
        $mime_excel = ['application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        $mime_pdf = ['application/pdf','image/vnd.adobe.photoshop','application/postscript','application/postscript','application/postscript']; 

    ?>
    <div class="col-md-9 animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                @foreach($documents as $key=>$doc)
                <div class="file-box contextmenu" data-id='{{ $doc->id }}'>
                    <a href="/documents/{{ $doc->file }}" download='{{$doc->doc_name}}' >
                        <div class="file">
                            <span class="corner"></span>
                            <?php $filetype = mime_content_type(public_path().'/documents/'.$doc->file) ?>
                            @if(in_array($filetype, $mime_images))
                              <div class="image">
                                <img alt="image" class="img-responsive" src="/documents/{{ $doc->file }}" >
                              </div>
                            @else
                              <div class="icon">
                                  @if(in_array($filetype, $mime_office))
                                    <i class="fa fa-bar-chart-o"></i>
                                  @elseif(in_array($filetype, $mime_audio))
                                    <i class="fa  fa-music"></i>
                                  @elseif(in_array($filetype, $mime_video))
                                    <i class="fa  fa-film"></i>
                                  @elseif(in_array($filetype, $mime_doc))
                                    <i class="fa  fa-file-word-o"></i>
                                  @elseif(in_array($filetype, $mime_excel))
                                   <i class="fa  fa-file-excel-o"></i>
                                  @elseif(in_array($filetype, $mime_pdf))
                                    <i class="fa fa-file-pdf-o"></i>
                                  @else
                                    <i class="fa  fa-file"></i>
                                  @endif
                                  
                              </div>
                            @endif
                            <div class="file-name">
                                 <span class="btn btn-xs {{ $doc->category->color }}" style="display: block;">{{ $doc->category->name }}</span>
                                @if(strlen($doc->doc_name) < 15){{ $doc->doc_name }}@else {{ substr($doc->doc_name,0,15) }}.. @endif
                                <br>
                                <small>Added: {{ date('dS M Y',strtotime($doc->created_at)) }}</small>
                                <br>
                                @if(\Request::get('type') && \Request::get('type') == 'shared')
                                  <small>Shared By : {{$doc->user->username}}</small>
                                @endif
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach

            </div>
        </div>
        </div>
    </div>
</div>

    </div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<link href="/contextmenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
<script src="/contextmenu/jquery.contextMenu.js" type="text/javascript"></script>
<script src="/contextmenu/jquery.ui.position.min.js" type="text/javascript"></script>
<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript">
  
   $(document).on('hidden.bs.modal', '#modal_dialog' , function(e){
        $('#modal_dialog .modal-content').html('');    
   });
 $.contextMenu({
            selector: '.contextmenu', 
            callback: function(key, options) {
                var id = $(this).data('id');
                if(key == 'delete'){
                  let url = `/admin/documents/${id}/confirm-delete`;
                    $(`#modal_dialog .modal-content`).load(url,function(result){
                        $('#modal_dialog').modal({show:true});
                    });
                 
                  return
                }
                if(key == 'edit'){
                  let url = `/admin/documents/${id}/edit`;
                    $(`#modal_dialog .modal-content`).load(url,function(result){
                        $('#modal_dialog').modal({show:true});
                    });
                 
                  return
                }
                if(key =='download' ){
                  location.href = $(this).find('a').attr("href");
                return 
                }
                if(key == 'share'){
                 let  url = `/admin/documents/userlist/${id}/`; 
                $(`#modal_dialog .modal-content`).load(url,function(result){
                    $('#modal_dialog').modal({show:true});
                });
                }
            },
            items: {
               "download": {name: "Download", icon: "fa-cloud-download"},
                "sep1": "---------",
                "edit": {name: "Edit", icon: "fa-edit"},
                "sep2": "---------",
                "delete": {name: "Delete", icon: "fa-trash deletable"}, 
                "sep3": "---------",
                "share": {name: "Share", icon: "fa-share"}
            }
        });
 $('.change-category').click(function(){
  let id = $(this).data('id');
  var folder_id = `{{ \Request::get('folder') }}`;
  if(folder_id)
    window.location = `/admin/documents?category=${id}&folders=${folder_id}`;
  else
    window.location = `/admin/documents?category=${id}`;

 });
 $('#search').click(function(){
  let term = $('#search_inp').val();
  window.location = `/admin/documents?term=${term}`;
 });

 $('input[name=doctype]').change(function(){
  let val = $(this).val();
  if(val == 'shared'){
    window.location = `/admin/documents?type=shared`;
  }
  else{
     window.location = `/admin/documents`;
  }
 });
</script>
@endsection

@extends('layouts.master')
@section('content')

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                {{ $page_title }}
                <small>{!! $page_description ?? "Page description" !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Currency list</h3>
                        &nbsp;
                        <a class="btn btn-default btn-sm" href="{!! route('admin.manual-currency.create') !!}" title="{{ trans('admin/organization/general.button.create') }}">
                            <i class="fa fa-plus-square"></i>
                        </a>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="wrap" style="margin-top:5px; margin-left: 20px;">
                            <form method="get" action="{{route('admin.manual-currency.index')}}">
                                <div class="filter form-inline" style="margin:0 30px 0 0;">
                                    <input placeholder="Select Date" name="date" class="form-control" id="date" type="text">
{{--                                    {!! Form::select('date',  $country, \Request::get('country_id'), ['id'=>'filter-country', 'class'=>'form-control select2', 'style'=>'width:150px; display:inline-block;','placeholder' => 'Date']) !!}&nbsp;&nbsp;--}}

                                    <button class="btn btn-primary" type="submit" id="btn-submit-filter"><i class="fa fa-list"></i> Filter</button>

                                    <a href="{{url('/admin/manual-currency')}}" class="btn btn-danger" id="btn-filter-clear"><i class="fa fa-close"></i> Clear</a> &nbsp;&nbsp;&nbsp;&nbsp;


                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="organization-table">
                                <thead>
                                    <tr >
                                        <th style="text-align: center; width:10px">
                                          ID
                                        </th>
                                        <th>Currency</th>
                                        <th>Buy Rate</th>
                                        <th>Sell Rate</th>
                                        <th>Date</th>
                                        <th style="text-align: center; width:11%">Current Rate </th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($manualCurrencies as $o)
                                        <tr>
                                            <td align="center">{!! $o->id !!}</td>
                                            <td>{!! $o->currency->name !!}</td>
                                            <td>{!! $o->buy_rate !!}</td>

                                            <td>{!! $o->sell_rate !!}</td>
                                            <td>{{ $o->date }}</td>
                                            <td align="center">@if($o->IsCurrent) <i class="fa fa-circle fa" style="color: green;font-size: 11px" aria-hidden="true"></i> @else <i class="fa fa-circle fa" style="color: #eaecea;font-size: 11px" aria-hidden="true"></i> @endif</td>
                                            <td>
                                                @if ( $o->isEditable() || $o->canChangePermissions() )
                                                    <a href="{!! route('admin.manual-currency.edit', $o->id) !!}" title="{{ trans('general.button.edit') }}"><i class="fa fa-edit"></i></a>
                                                @else
                                                    <i class="fa fa-edit text-muted" title="{{ trans('admin/communication/general.error.cant-edit-this-organization') }}"></i>
                                                @endif

                                                @if ( $o->isDeletable() )
                                                    <a href="{!! route('admin.manual-currency.confirm-delete', $o->id) !!}" data-toggle="modal" data-target="#modal_dialog" title="{{ trans('general.button.delete') }}"><i class="fa fa-trash deletable"></i></a>
                                                @else
                                                    <i class="fa fa-trash text-muted" title="{{ trans('admin/organization/general.error.cant-delete-this-organization') }}"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>

{{--    <script language="JavaScript">--}}
{{--        function toggleCheckbox() {--}}
{{--            checkboxes = document.getElementsByName('chkOrganization[]');--}}
{{--            for(var i=0, n=checkboxes.length;i<n;i++) {--}}
{{--                checkboxes[i].checked = !checkboxes[i].checked;--}}
{{--            }--}}
{{--        }--}}
{{--    </script>--}}


    <script>

        $(function () {
            $('#date').datetimepicker({
                //inline: true,
                format: 'YYYY-MM-DD',
                // format: 'MM',
                sideBySide: true
            });
            $('#time').datetimepicker({
                //inline: true,
                format: "HH:mm:ss",
                // format: 'MM',
                sideBySide: true
            });
        });
    $(function() {
        $('#organization-table').DataTable({
            "order": [[ 5, "desc" ]]
        });
    });
    </script>

@endsection

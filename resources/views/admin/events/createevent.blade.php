@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
<style>
    select { width:200px !important; }
label {
    font-weight: 600 !important;
}

 .intl-tel-input { width: 100%; }
 .intl-tel-input .iti-flag .arrow {border: none;}



</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset("/bower_components/intl-tel-input/build/css/intlTelInput.css") }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset("/bower_components/intl-tel-input/build/js/intlTelInput-jquery.min.js") }}"></script>



<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                {{ $page_title ?? "Page Title" }} 
                <small>{!! $page_description ?? "Page description" !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

    <div class='row'>
        <div class='col-md-12'>  
            <div class="box">
                <div class="box-body">
                    {!! Form::open( ['route' => 'addevent', 'class' => 'form-horizontal'] ) !!} 
            
                <div class="content col-md-12">

                  <div class="row">
                       <div class="col-md-4">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                Type
                              </label>
                              <div class="col-sm-6">
                                <select style="width: 130px" name="event_type" class="form-control select2 searchable input-sm">
                                    @foreach($events_type as $e)
                                    <option value="{{$e}}">{{ucfirst(trans($e))}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                        </div> 

                      <div class="col-md-4">
                          <div class="form-group">  
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                Name
                              </label>
                              <div class="input-group ">
                                  <input type="text" name="event_name" placeholder="Event Name" id="event_name" class="form-control input-sm" required>
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-building"></i></a>
                                  </div>
                              </div>
                          </div>    
                      </div> 

                      <div class="col-md-4">
                          <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">
                            Venue
                            </label>
                            <div class="col-sm-5">
                              <select name="venue_id" class="form-control select2 searchable">
                                  @foreach($venue as $v)
                                  <option value="{{$v->id}}">{{ucfirst(trans($v->venue_name))}}</option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                      </div>

                  </div>

                  <div class="row">

                        <div class="col-md-4">
                          <div class="form-group">  
                           <label class="control-label col-sm-5">Start date</label>
                              <div class="input-group ">
                                <input required="" type="text" class="form-control event_start_date input-sm" value="{{ isset($event_start_date) ? $event_start_date : '' }}" name="event_start_date" id="event_start_date">
                                      <div class="input-group-addon">
                                          <a href="#"><i class="fa fa-calendar"></i></a>
                                      </div>
                              </div>
                           </div>
                         </div>


                      <div class="col-md-4">
                        <div class="form-group">  
                            <label class="control-label col-sm-4">End date</label>
                            <div class="input-group ">
                              <input required="" type="text" class="form-control event_end_date input-sm" value="{{ isset($event_end_date) ? $event_end_date : '' }}" name="event_end_date" id="event_end_date">
                              <div class="input-group-addon">
                              <a href="#"><i class="fa fa-calendar"></i></a>
                              </div>
                           </div>
                        </div>
                      </div>   

                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">
                             Status
                              </label>
                              <div class="col-sm-5">
                               <select name="event_status" class="form-control select2 searchable">
                                    @foreach($event_status as $s)
                                    <option value="{{$s}}">{{ucfirst(trans($s))}}</option>
                                    @endforeach
                                </select>
                              </div>
                          </div>
                      </div>

                </div>   

                <div class="row">
                    <div class="col-md-4">
                          <div class="form-group">  
                            <label class="control-label col-sm-5">Participants</label>

                                <div class="input-group ">
                                    <input type="number" name="num_participants" placeholder="Participants num" id="num_participants"  class="form-control input-sm" >
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-users"></i></a>
                                    </div>
                                </div>
                            </div>
                    </div>

                          <div class="col-md-4">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">
                                Bill To
                              </label>
                              <div class="col-md-3">
                                <select style="width:130px;" name="bill_to" class="form-control select2 searchable input-sm">
                                  <option value="">Select Company</option>
                                    @foreach($clients as $cl)
                                    <option value="{{$cl->id}}">{{ucfirst($cl->name)}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                        </div> 

                         <div class="col-md-4">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">
                               Source
                              </label>
                              <div class="col-md-5">
                                <select name="source" class="form-control select2 searchable">
                                     <option value="Offline">Offline</option>
                                     <option value="Online">Online</option>
                                </select>
                              </div>
                            </div>
                        </div> 
                </div> 

              <h3>Contact Details</h3> 

              <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5"> Name</label>
                              <div class="input-group ">
                                  <input type="text" name="person_name" placeholder="Person Name" id="person_name"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-user"></i></a>
                                  </div>
                              </div>
                          </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5">Email</label>
                              <div class="input-group ">
                                  <input type="email" name="email" placeholder="Contact Email" id="email"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-at"></i></a>
                                  </div>
                              </div>
                          </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5">Contact No.</label>
                              <div class="input-group ">
                                  <input type="text" name="contact_num" placeholder="Contact Number" id="contact_num"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-mobile"></i></a>
                                  </div>
                              </div>
                          </div>
                    </div>
              </div>

              <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5"> Landline </label>
                              <div class="input-group ">
                                  <input type="text" name="landline" placeholder="Landline" id="landline"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-phone"></i></a> 
                                  </div>
                              </div>
                          </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5"> Address </label>
                              <div class="input-group ">
                                  <input type="text" name="address" placeholder="Address" id="address"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-map"></i></a> 
                                  </div>
                              </div>
                          </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5">City</label>
                              <div class="input-group ">
                                  <input type="text" name="city" placeholder="City" id="city"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-building"></i></a>    
                                  </div>
                              </div>
                          </div>
                    </div>

              </div>  

              <div class="row">
                  <div class="col-md-4">
                        <div class="form-group">  
                          <label class="control-label col-sm-5">Post Code</label>
                              <div class="input-group ">
                                  <input type="text" name="post_code" placeholder="Post Code" id="post_code"  class="form-control input-sm" >
                                  <div class="input-group-addon">
                                      <a href="#"><i class="fa fa-code"></i></a>   
                                  </div>
                              </div>
                          </div>
                    </div>
              </div>

<div style="">
                <h3>Others Details</h3>
                    <div class="row">
                       <div class="col-md-4">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">
                             Owner
                              </label>
                              <div class="col-sm-8">
                             <select style="width: 130px" name="user_id" class="form-control input-sm">
                                    @foreach($users as $u)
                                    <option value="{{$u->id}}">{{ucfirst(trans($u->username))}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                        </div>     

                        <div class="col-md-4"> 
                            <div class="form-group">  
                              <label class="control-label col-sm-6">Catering Package</label>
                                  <div class="input-group ">
                                      <input type="text" name="catering_package_id" placeholder="Select Package" id="other_details"  class="form-control input-sm" >
                                      <div class="input-group-addon">
                                          <a href="#"><i class="fa fa-hourglass"></i></a>
                                      </div>
                                  </div>
                              </div>
                        </div> 

                        <div class="col-md-4">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">
                            Setup Type
                        </label>
                        <div class="col-sm-4">
                            {!! Form::select('setup_type',['Boardroom'=>'Boardroom','Cluster'=>'Cluster','Theatre'=>'Theatre','Usetup'=>'U set up','Classroom'=>'Classroom','Roundseating'=>'Round seating','Fishbone'=>'Fish bone','Round'=>'Round','Lounge'=>"Lounge"], old('guest_type'), ['class' => 'form-control searchable select2 input-sm'] )!!}

                        </div>
                    </div>
                </div>

                    </div>
                  </div>
                    
                      
               
                   
                    <h3>Resource Requirements</h3>

                    <div class="row">

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="white board" name="requirements[]">White Board</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="sofa" name="requirements[]">Sofa</label>
                          </div>  

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="chair" name="requirements[]">Chair</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="podium" name="requirements[]">Podium</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="mike" name="requirements[]">Mike</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="panas" name="requirements[]">Panas</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="av" name="requirements[]">Audio Visual</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="dj" name="requirements[]">DJ</label>
                          </div>

                          <div class="checkbox col-md-4">
                            <label><input type="checkbox" value="projector" name="requirements[]">LED Projector</label>
                          </div>

                   </div>
              
                      <div class="row"> 

                   <div class="col-md-4">
                      <label for="inputEmail3" class="control-label">
                      Others 
                      </label>
                        
                        <textarea class="form-control" name="others_req" id="others_req" placeholder="Other Requirements">{!! \Request::old('others_req') !!}</textarea>
                      </div>
               </div>

               <h3>Products</h3>
               <div class="row">
                @foreach($products as $key=>$pr)
                @if(count($pr) > 0)
                <div class="col-md-6">
                  <h4>{{$key}}</h4>
                  <ol>
                    <tr>

                  @foreach($pr as $p1)
                  <td>
                  <li>
                   <label><input type="checkbox" value="{{$p1['name']}}" name="products[]">
                   &nbsp;{{ucwords($p1['name'])}}
                 </label>
                  </li>
                </td>
                   @endforeach
               </tr>
                 </ol>
                </div>
                @endif
              @endforeach
                 
               </div>
               <h4>Bar</h4>
                  <div class="row"> 

                   <div class="col-md-4">
                    
                        
                        <textarea class="form-control" name="bar_info" id="bar_info" placeholder="Bar products">{!! \Request::old('bar_info') !!}</textarea>
                      </div>
               </div>
               <h3>Amount Details</h3>

                    <div class="row">

                         <div class="col-md-4">

                         <div class="form-group">  
                <label class="control-label col-sm-6">Paid</label>
                    <div class="input-group ">
                        <input type="number" name="amount_paid" placeholder="Amount Paid" id="amount_paid"  class="form-control" >
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-money"></i></a>
                        </div>
                    </div>
                </div>      
            </div> 

                        <div class="col-md-4">
                          <div class="form-group">  
                             <label class="control-label col-sm-6">Potential cost</label>
                             <div class="input-group ">
                                 <input type="number" name="potential_cost" placeholder="Potential Cost" id="potential_cost" class="form-control" >
                                 <div class="input-group-addon">
                                  <a href="#"><i class="fa fa-money"></i></a>
                                 </div>
                             </div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">  
                <label class="control-label col-sm-6">Calculated cost</label>
                    <div class="input-group ">
                        <input type="text" name="calculated_cost" placeholder="Calculated cost" id="calculated_cost" value="" class="form-control">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-money"></i></a>
                        </div>
                    </div>
                </div>
                        </div>
                   </div>


                <div class="row">

                      <div class="col-md-4">
                           <div class="form-group"> 
                           <label class="control-label col-sm-6">Edited cost</label> 
                              <div class="input-group">
                              <input type="number" name="edited_cost" id="edited_cost" class="form-control" placeholder="Edited cost" >
                              <div class="input-group-addon">
                              <a href="#"><i class="fa fa-money"></i></a>
                             </div>
                           </div>
                         </div>
                      </div>


                     
                    <div class="col-md-4">
                           <div class="form-group"> 
                           <label class="control-label col-sm-6">Extra cost</label> 
                              <div class="input-group">
                              <input type="number" name="extra_cost" id="extra_cost" class="form-control" placeholder="Extra cost" >
                              <div class="input-group-addon">
                              <a href="#"><i class="fa fa-money"></i></a>
                             </div>
                           </div>
                         </div>
                       </div>

                     
                    <div class="col-md-4">
                           <div class="form-group"> 
                           <label class="control-label col-sm-6">Booking Advance</label> 
                              <div class="input-group">
                              <input type="number" name="booking_advance" id="extra_cost" class="form-control" placeholder="Extra cost" >
                              <div class="input-group-addon">
                              <a href="#"><i class="fa fa-money"></i></a>
                             </div>
                           </div>
                         </div>
                       </div>
                </div>
                
                </div>


               <div class="row">
                   <div class="col-md-12">
                      <label for="inputEmail3" class="control-label">
                      Schedule 
                      </label>
                        <textarea class="form-control" name="schedule" id="schedule" placeholder="Write Schedule">
                              <table border="1" cellpadding="1" cellspacing="1" style="width:500px">
                                  <tbody>
                                      <tr>
                                        <td>Time</td>
                                        <td>Function Activities</td>
                                        <td>Rep/Host</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                  </tbody>
                              </table>
                              <p>&nbsp;</p>
                        </textarea>
                    </div>
               </div> 

 
               <div class="row"> 

                   <div class="col-md-12">
                      <label for="inputEmail3" class="control-label">
                      Menu 
                      </label>
                        
                        <textarea class="form-control" name="menu_items" id="menu_items" placeholder="Write Menu">{!! \Request::old('menu_items') !!}</textarea>
                      </div>
               </div>

                <div class="row"> 
                   <div class="col-md-12">
                      <label for="inputEmail3" class="control-label">
                      Set up  
                      </label>
                        <textarea class="form-control" name="set_up" id="set_up" placeholder="Write the Set up">{!! \Request::old('set_up') !!}</textarea>
                      </div>
                </div>
            

                <div class="row InputsWrapper">


                </div> 
                <div class="row">

                     <div class="col-md-12">
                        <label for="inputEmail3" class="control-label">
                        Descriptions 
                        </label>
                          <textarea class="form-control" name="comments" id="comments" placeholder="Write Description">{!! \Request::old('description') !!}</textarea>
                        </div>
                 </div>

                <div class="col-md-12"><br>
                    <div class="form-group">
                        <button class="btn btn-primary" id="btn-submit-edit" type="submit">Add Event</button>
                    </div>
                </div>

                </div><!-- /.content -->
                <div class="content col-md-3">

               <!--    //side menu// -->
                </div>



                    {!! Form::close() !!}
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@section('body_bottom')
    <link href="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap-datetimepicker.css") }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
    <script type="text/javascript">

        $(function() {
            $('#event_start_date').datetimepicker({
                    //inline: true,
                    //format: 'YYYY-MM-DD',
                    format: 'YYYY-MM-DD HH:mm:ss',
                    sideBySide: true
                });
             $('#event_end_date').datetimepicker({
                    //inline: true,
                    //format: 'YYYY-MM-DD',
                    format: 'YYYY-MM-DD HH:mm:ss',
                    sideBySide: true
                });
        });

    </script>

    <script type="text/javascript">
       
             $(document).ready(function() {

                  $("#person_name").autocomplete({
                      source: "/admin/getContacts",
                      minLength: 1,
                   });
            });



    $('#person_name').on('change',function(){
        $.ajax(
            { url: "/admin/getcontactinfo",  data: { contact_id: $(this).val() }, dataType: "json", 
            success: function( data ) { 
            //console.log(data);
            var result = data.data;
            $('#person_name').val(result.full_name); 
            $('#contact_num').val(result.phone);  
            $('#email').val(result.email_1);    
            $('#landline').val(result.landline);
            $('#address').val(result.address);
            $('#city').val(result.city);
            $('#post_code').val(result.postcode);

            } 

            }); 
       });


    </script>

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script type="text/javascript" src="/bower_components/tags/js/tag-it.js"></script>
<script>
//CKEDITOR.replace( 'schedule' );
CKEDITOR.config.height='140px'; 
CKEDITOR.replace( 'schedule', {
toolbar: [
{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],  // Defines toolbar group without name.
                                                                 // Line break - next group will be placed in new line.
{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Language' ] },
{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
]
});


     $('.searchable').select2();

</script>
   


    <!-- form submit -->
    @include('partials._body_bottom_submit_lead_edit_form_js')
@endsection

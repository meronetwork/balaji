@extends('layouts.master')
@section('content')
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Banquet Programs
                <small>{!! $page_description ?? "Page description" !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
           
            <b><font size="4">Event List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('addevent') }}">
                            <i class="fa fa-plus"></i>&nbsp;<strong>Create new event</strong>
                        </a> 
            </div>      
        </div>
        <div class="col-md-12" style="padding-bottom: 5px;">
            <form action="/admin/events">
            <table>
                <tr>
                    <th>
                        <input type="date" name="start_date" class="form-control" value="{{Request::get('start_date')}}">
                    </th>
                     <th>
                        <input type="date" name="end_date" class="form-control" value="{{Request::get('end_date')}}">
                    </th>
                     <th>
                        {!! Form::select('venue_id',$eventsVenue,Request::get('venue_id'),['placeholder'=>'Select Venue','class'=>'form-control']) !!}
                       
                    </th>
                    <th>
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </th>
                    <th>
                        <a href="/admin/events" class="btn btn-danger">Reset</a>
                    </th>
                </tr>
            </table>
        </form>
        </div>
</div>
<?php 
$labelcolor= ['concert'=>"btn bg-maroon  margin",'dinner'=>'btn bg-red  margin','lunch'=>'btn bg-navy  margin','hightea'=>'btn bg-red  margin','cocktail'=>'btn bg-olive  margin','picnic'=>'btn bg-maroon margin','party'=>'btn bg-purple margin','seminar'=>'btn bg-blue margin','conference'=>'btn bg-orange margin','workshop'=>'btn bg-green margin','galas'=>'btn bg-yellow  margin'];
?>
<table class="table table-hover table-no-border" id="leads-table">
<thead>
    <tr class="bg-info">
        
        <th>ID</th>
        <th>Event Name</th>
        <th>Event Type</th>
        <th>Venue</th>
        <th>Souce</th>
        <th>Owner</th>
        <th>Event Status</th>
        <th>Participants</th>
        <th>Start Date</th>
        <th>Action</th> 
    </tr>
</thead>   
<tbody>
    @foreach($events as $key=>$event)
    <tr @if($event->source == 'Online') class="bg-info" @endif>  
        
        <td>{{ env('RES_CODE')}}{{$event->eid}}</td>
        <td style="font-size: 16.5px"><strong>{{ucfirst($event->event_name)}}</strong></td>
        <td><label class="label {{$labelcolor[$event->event_type]}}">{{ucfirst(trans($event->event_type))}}</label></td>
        <td >{{$event->venue_name}}</td>  
        <td >{{$event->source}}</td>
        <td>{{ucfirst($event->username)}}</td>
        <td>{{ucfirst($event->event_status)}}</td>
        <td>{{$event->num_participants}}</td>
        <td>{{ date('dS M y', strtotime($event->event_start_date)) }}</td>
        <?php 
        if($event->isDeletable())
         $datas = '<a href="'.route('confirm-delete', $event->eid).'?type='.\Request::get('type').'" data-toggle="modal" data-target="#modal_dialog" title="{{ trans(\'general.button.delete\') }}"><i class="fa fa-trash-o deletable"></i></a>';
        else
            $datas = '<a href=""><i class="fa fa-trash-o text-muted"></i></a>'
         ?>
        <td>
            <a href="/admin/editevent/{{$event->eid}}"><i class="fa fa-edit"></i></a>&nbsp;
            <?php echo $datas  ?>&nbsp;
            <a href="/admin/event/{{$event->eid}}/pdf" title="pdf download"><i class="fa fa-cloud-download"></i></a>




        </td>


    </tr>
    @endforeach
</tbody>
</table>
      <div style="text-align: center;"> {!! $events->appends(\Request::except('page'))->render() !!} </div>
</div>

@endsection
@extends('layouts.master')
@section('head_extra')
<!-- jVectorMap 1.2.2 -->
<link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />
<style>
    .filter_date { font-size:14px; }
</style>
<link href="{{ asset("/bower_components/admin-lte/plugins/fullcalendar/fullcalendar.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset("/bower_components/admin-lte/plugins/fullcalendar/fullcalendar.print.css") }}" rel="stylesheet" media="print" />
@endsection
@section('content')

<style>
    .required { color: red; }
    .panel-custom .panel-heading {
        border-bottom: 2px solid #1797be;
    }
    .panel-custom .panel-heading {
        margin-bottom: 10px;
    }
</style>

<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Events Report
                <small>{!! $page_description or "Report" !!}</small>
            </h1>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Events Report</strong>
                </div>
            </div>
            <div class="panel-body">
                <form id="attendance-form" role="form"  action="{{route('admin.events-calendar')}}" method="post" class="form-horizontal form-groups-bordered">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="user_id" class="col-sm-3 control-label">Venue<span
                                class="required">*</span></label>

                        <div class="col-sm-5">
                            <select required name="venue_id" id="user_id" class="form-control select_box">
                                <option value="">Select Venue</option>
                                @foreach($venue as $uv)
                                <option value="{{$uv->id}}" @if($venue_id == $uv->id) selected @endif>{{ucfirst($uv->venue_name)}}</option>  
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-5 ">
                            <button type="submit" id="sbtn" class="btn btn-primary">View Report</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@if($events_list)
<div id='calendar'></div>

@section('body_bottom')
<link href='/bower_components/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='/bower_components/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='/bower_components/fullcalendar/moment.min.js'></script>
<script src='/bower_components/fullcalendar/fullcalendar.min.js'></script>
<script>
  $(function () {
    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
      //Random default events
      events    : {!! $events_list !!},
                /*
                {
                    title          : 'Click for Google',
                    start          : new Date(y, m, 28),
                    end            : new Date(y, m, 29),
                    url            : 'http://google.com/',
                    backgroundColor: '#3C8DBC',
                    borderColor    : '#3C8DBC'
                }
                */
      eventClick: function(event) {
        if(event.url) {
            window.open(event.url, "_blank");
            return false;
        }
      }
    })
  })
</script>
@endsection
@endif
@endsection
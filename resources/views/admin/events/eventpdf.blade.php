<!DOCTYPE html><html><head>
	<title>{{ env('APP_COMPANY')}} | Event Booking PDF</title>

	<style type="text/css">
@font-face {
  font-family: Arial;
}
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
a {
  color: #0087C3;
  text-decoration: none;
}
body {
  position: relative;
  margin: 0 auto;
  color: #555555;
  background: #FFFFFF;
  font-family: Arial, sans-serif;
  font-size: 12px;
}
header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}
table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 5px;
}
table th,
table td {
  padding: 3px;
  background:;
  text-align: left;
  border-bottom: 1px solid #FFFFFF;
}
tr td{
  padding-top: 5px;
}
table th {
  white-space: nowrap;
  font-weight: normal;
}
table td {
  text-align: left;
}
table td h3{
  color: #57B223;
  font-size: 1.2em;
  font-weight: normal;
  margin: 0 0 0.2em 0;
}
table .no {
  color: #FFFFFF;
  font-size: 1em;
  background: #57B223;
}
table .desc {
  text-align: left;
}
table .unit {
  background: #DDDDDD;
}
table .qty {
}
table .total {
  background: #57B223;
  color: #FFFFFF;
}
table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}
table tbody tr:last-child td {
  border: none;
}
table tfoot td {
  padding: 5px 10px;
  background: #FFFFFF;
  border-bottom: none;
  font-size: 1em;
  white-space: nowrap;
  border-top: 1px solid #AAAAAA;
}
table tfoot tr:first-child td {
  border-top: none;
}
table tfoot tr:last-child td {
  color: #57B223;
  font-size: 1em;
  border-top: 1px solid #57B223;
  font-weight: bold;
}
table tfoot tr td:first-child {
  border: none;
}
#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}
#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;
}
#notices .notice {
  font-size: 1.2em;
}
footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
   </style>

</head><body>
	<header class="clearfix">
        <table>
            <tr>
        <td width="50%" style="float:left">
          <div id="logo">
               <h4 class="name">{{ env('APP_COMPANY') }} </h4>   
              <div>{{ env('APP_ADDRESS1') }}</div>
              <div>{{ env('APP_ADDRESS2') }}</div>
              <div>Seller's PAN: {{ env('TPID') }}</div>
              <div><a href="mailto:{{ env('APP_EMAIL') }}">{{ env('APP_EMAIL') }}</a></div>
          </div>
        </td>
        <td width="50%" style="text-align: right">
        <div  id="company">
            <img style="max-width: 150px" src="{{env('APP_URL')}}/{{ 'org/'.$organization->logo }}">
        </div>
        </td></tr>
      </table>
    </header>
	    <main>	
	    	 <div id="details" class="clearfix">
	    	 	<h3>Basic Details Booking Id:#{{$ord->id}}</h3>
	    	 	 <table>
	    	 	 	<tr>
	    	 	 		<td>Type: <u>@if($ord->event_type) <strong>{{ucwords($ord->event_type)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Event Name: <u>@if($ord->event_name) <strong>{{ucwords($ord->event_name)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Venue: <u>@if($ord->venue_id) <strong>{{ucwords($ord->venue->venue_name)}} </strong>@else _________________ @endif</u></td>
	    	 	 	</tr>
	    	 	 	<tr>
	    	 	 		<td>Event Start Date: <u>@if($ord->event_start_date) <strong>{{ucwords($ord->event_start_date)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Event End Date: <u>@if($ord->event_end_date) <strong>{{ucwords($ord->event_end_date)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Status: <u>@if($ord->event_status) <strong>{{ucwords($ord->event_status)}} </strong>@else _________________ @endif</u></td>
	    	 	 	</tr>
	    	 	 	<tr>
	    	 	 		<td>Participants: <u>@if($ord->num_participants) <strong>{{ucwords($ord->num_participants)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Bill To: <u>@if($ord->bill_to) <strong>{{ucwords($ord->client->name)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Source: <u>@if($ord->source) <strong>{{ucwords($ord->source)}} </strong>@else _________________ @endif</u></td>
	    	 	 	</tr>
	    	 	</table>
	    	 	<h3>Contact Details</h3>
	    	 	<table>
	    	 		<tr>
	    	 			<td>Name: <u>@if($ord->person_name) <strong>{{ucwords($ord->person_name)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Email: <u>@if($ord->email) <strong>{{ucwords($ord->email)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Contact No: <u>@if($ord->contact_num) <strong>{{ucwords($ord->contact_num)}} </strong>@else _________________ @endif</u></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td>Landline: <u>@if($ord->landline) <strong>{{ucwords($ord->landline)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Address: <u>@if($ord->address) <strong>{{ucwords($ord->address)}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>City: <u>@if($ord->city) <strong>{{ucwords($ord->city)}} </strong>@else _________________ @endif</u></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td>Post Code: <u>@if($ord->post_code) <strong>{{ucwords($ord->post_code)}} </strong>@else _________________ @endif</u></td>
	    	 		</tr>
	    	 	</table>
	    	 	<h3>Other Details</h3>
	    	 	<table>
	    	 		<tr>
	    	 			<td>Owner: <u>@if($ord->user_id) <strong>{{$ord->user->username}} </strong>@else _________________ @endif</u></td>
	    	 	 		<td>Other Details: <u>@if($ord->other_details) <strong>{{$ord->other_details}} </strong>@else _________________ @endif</u></td>
	    	 		</tr>
	    	 	</table>
	    	 	<h3>Requirements</h3>
	    	 	<table>
	    	 		<tr>
	    	 			<td><input type="checkbox" name="" @if(in_array('white board',$requirements)) checked @endif> White Board</td>
	    	 			<td><input type="checkbox" name="" @if(in_array('sofa',$requirements)) checked @endif> Sofa</td>
	    	 			<td><input type="checkbox" name="" @if(in_array('chair',$requirements)) checked @endif> Chair</td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td><input type="checkbox" name=""  @if(in_array('podium',$requirements)) checked @endif> Podium</td>
	    	 			<td><input type="checkbox" name="" @if(in_array('mike',$requirements)) checked @endif> Mike</td>
	    	 			<td><input type="checkbox" name="" @if(in_array('panas',$requirements)) checked @endif> Panas</td>
	    	 			
	    	 		</tr>
	    	 		<tr>
	    	 			<td>Others: <u>@if($ord->others_req) <strong>{{$ord->others_req}} </strong>@else _________________ @endif</u></td>
	    	 		</tr>
	    	 	</table>
	    	 	<h3>Products</h3>
	    	 	<table>
	    	 		
	    	 		@foreach($selected_pro  as $key=>$value)
	    	 		<tr><td>{{$value}}</td></tr>
	    	 		@endforeach
	    	      	
	    	 	</table>
	    	 @if($ord->bar_info)
	    	 	<table>
	    	 		<tr>
	    	 			<td>BAR : <u>@if($ord->bar_info) <strong>{{$ord->bar_info}} </strong>@else _________________ @endif</u></td>
	    	 		</tr>
	    	 	</table>
	    	 @endif

	    	 <h4>Amount Details</h4>

	    	 <table>
	    	 <tr>
 			<td>Paid: <u>@if($ord->person_name) <strong>{{ucwords($ord->person_name)}} </strong>@else _________________ @endif</u></td>
 	 		<td>Potential cost: <u>@if($ord->potential_cost) <strong>{{ucwords($ord->potential_cost)}} </strong>@else _________________ @endif</u></td>
 	 		<td>Calculated cost: <u>@if($ord->calculated_cost) <strong>{{ucwords($ord->calculated_cost)}} </strong>@else _________________ @endif</u></td>
	 		</tr>
	 		<tr>
	 			<td>Edited cost: <u>@if($ord->edited_cost) <strong>{{ucwords($ord->edited_cost)}} </strong>@else _________________ @endif</u></td>
	 	 		<td>Extra cost: <u>@if($ord->extra_cost) <strong>{{ucwords($ord->extra_cost)}} </strong>@else _________________ @endif</u></td>
	 	 		<td>Booking Advance: <u>@if($ord->booking_advance) <strong>{{ucwords($ord->booking_advance)}} </strong>@else _________________ @endif</u></td>
	 		</tr>

	 		</table>

	 		<h4>Schedule:</h4>
	 		<p>{!! $ord->schedule !!}</p>
	 		
	 		<h4>Menus</h4>
	 		<p>{!! $ord->menu_items !!}</p>
	 		
	 		<h4>Set up</h4>
	 		<p>{!! $ord->set_up !!}</p>
	 	
	 		<h4>Descriptions</h4>
	 		<p>{!! $ord->comments !!}</p>
                 
	    	 </div>
	    </main>
     <footer>
          Event Card was created on MeroCrm Software.
     </footer>


</body></html>
@extends('layouts.master')
@section('content')

<?php

function CategoryTree($parent_id=null,$sub_mark=''){

  $groups= \App\Models\COAgroups::orderBy('code', 'asc')->where('parent_id',$parent_id)->where('org_id',\Auth::user()->org_id)->get();

    if(count($groups)>0){
      foreach($groups as $group){

        if($group->id<=4){

           echo '<tr>
                <td></td>
                <td><b>'.$sub_mark.$group->code.'</b></td>
                <td><h5><b>'.$group->name.'</b></h5></td>
                <td><b>Group</b></td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
               </tr>';
        }else{

           echo '<tr>
                <td></td>
                <td><b>'.$sub_mark.$group->code.'</b></td>
                <td><h5><b>'.$group->name.'</b></h5></td> 
                <td><b>Group</b></td>
                <td>-</td>
                <td>-</td>

                <td><a href="'.route('admin.chartofaccounts.edit.groups', $group->id).'" title="Edit"><i class="fa fa-edit"></i></a>
                 <a href="'.route('admin.chartofaccounts.groups.confirm-delete', $group->id).'" title="Delete" data-toggle="modal" data-target="#modal_dialog"><i class="fa fa-trash deletable"></i></a>
                </td>
               </tr>';
        }

        $ledgers= \App\Models\COALedgers::orderBy('code', 'asc')->where('group_id',$group->id)->where('org_id',\Auth::user()->org_id)->get(); 
        if(count($ledgers>0)){
            $submark= $sub_mark;
            $sub_mark = $sub_mark."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; 

              foreach($ledgers as $ledger){

                $closing_balance =TaskHelper::getLedgerBalance($ledger->id);

             echo '<tr style="color: #009551;">
                <td></td>
                <td class="bg-warning"><a href="'.route('admin.chartofaccounts.detail.ledgers', $ledger->id).'" style="color:#009551;"><b>'.$sub_mark.$ledger->code.'</b></a></td>
                <td class="bg-warning"><a href="'.route('admin.chartofaccounts.detail.ledgers', $ledger->id).'" style="color:#009551;"><h5><b>'.$ledger->name.'</b></h5></a></td>
                <td class="bg-warning"><b>Ledgers</b></td>
                <td class="bg-warning">'.$ledger->op_balance.'</td>
                <td class="bg-warning">'.$closing_balance.'</td>
                <td class="bg-warning"><a href="'.route('admin.chartofaccounts.edit.ledgers', $ledger->id).'" title="Edit"><i class="fa fa-edit"></i></a>
                <a href="'.route('admin.chartofaccounts.ledgers.confirm-delete', $ledger->id).'" title="Delete" data-toggle="modal" data-target="#modal_dialog"><i class="fa fa-trash deletable"></i></a>
                </td>
 
               </tr>';

           }
           $sub_mark=$submark;


        }


        CategoryTree($group->id,$sub_mark."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
      }
    }


}

 ?>


<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                {!! $page_title ?? "Page description" !!}
                <small>{!! $page_description ?? "Page description" !!}
                    | Current Fiscal Year: <strong>{{ FinanceHelper::cur_fisc_yr()->fiscal_year}}</strong>

                </small>
            </h1>

            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

   <div class="col-xs-12">
          <div class="box">
           
            <!-- /.box-header -->
            <div class="box-body">
                    <!-- <div id="accordion"> -->
                        <!-- <h3>Options</h3> -->
                        <div class="balancesheet form">
                            <form  method="POST" action="/admin/filterbygroups">
                                {{ csrf_field() }}
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Parent Group</label>
                                        <select class="form-control customer_id select2" id="ReportLedgerId" name="parent_id" tabindex="-1" aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @foreach($parentgroups as $pg)
                                            <option value="{{$pg->id}}" @if($pg->id== $parent_id) selected @endif>{{$pg->name}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="reset" name="reset" class="btn btn-primary pull-right" style="margin-left: 5px;" value="Clear">
                                <input type="submit" class="btn btn-primary pull-right" value="Submit">
                            </div>
                            </form>
                        </div>
                    <!-- </div> -->
                 </div>
          </div>
      </div>


      <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            
                    <div class="box box-header box-danger">

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="orders-table">
                                <thead>
                                    <tr>
                                        <th>Ledger Id</th>
                                        <th>Account Code</th>
                                        <th>Account Name</th>
                                        <th>Type</th>
                                        <th>O/P Balance({{env('APP_CURRENCY')}})</th>
                                        <th>C/L Balance({{env('APP_CURRENCY')}})</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @if($maingroup)
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td><b>{{$maingroup->code}}</b>
                                           
                                        </td>
                                        <td>
                                             <b>{{$maingroup->name}}</b>
                                        </td>
                                        <td>
                                            <b>Group</b>
                                        </td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    @endif

                                    @if(count($maingroupledgers)>0)
                                    @foreach($maingroupledgers as $ledger)

                                    <tr style="color: #009551;">
                                        <td></td>
                                        <td class="bg-warning"><a href="{{route('admin.chartofaccounts.detail.ledgers', $ledger->id)}}" style="color:#009551;"><b> {{$ledger->code}}</b></a></td>
                                        <td class="bg-warning"><a href="{{route('admin.chartofaccounts.detail.ledgers', $ledger->id)}}" style="color:#009551;"><h5><b>{{$ledger->name}}</b></h5></a></td>
                                        <td class="bg-warning"><b>Ledgers</b></td>  
                                        <td class="bg-warning">{{ $ledger->op_balance}}</td>  
                                        <td class="bg-warning"></td>
                                        <td class="bg-warning"><a href="{{route('admin.chartofaccounts.edit.ledgers', $ledger->id)}}" title="Edit"><i class="fa fa-edit"></i></a>
                                        <a href="{{route('admin.chartofaccounts.ledgers.confirm-delete', $ledger->id)}}" title="Delete" data-toggle="modal" data-target="#modal_dialog"><i class="fa fa-trash deletable"></i></a>
                                        </td>
                         
                                    </tr>

                                    @endforeach
                                    @endif
                                   {{ CategoryTree($parent_id,'') }}
                                </tbody>
                            </table>

                        </div> <!-- table-responsive -->

                    </div><!-- /.box-body -->
               
        </div><!-- /.col -->

    </div><!-- /.row -->

      




 @endsection

<!-- Optional bottom section for modals etc... -->
@section('body_bottom') 


@endsection  

@extends('layouts.master')
@section('content')
<?php setlocale(LC_MONETARY, 'en_NP'); ?>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
           	Banking and Income
                <small>{!! $page_descriptions !!}</small>
            </h1>
            <p> Create a Bank, Petty Cash and other fund accounts where you can deposit money. Income entries goes here by selecting the bank accounts</p>
            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>
   <div class="box box-primary">
    <div class="box-header with-border">
       <div class='row'>
        <div class='col-md-12'>
            <b><font size="4">Account List</font></b>
            <div style="display: inline; float: right;">
            <a class="btn btn-primary btn-xs"  title="Create New Bank" href="{{route('admin.bank.create')}}">
                            &nbsp;&nbsp;+ Create New Account   
                        </a>
            </div>
        </div>
</div>
<br/>
<table class="table table-hover table-no-border table-striped" id="leads-table">
<thead>
    <tr class="btn-info">
        
        <th>Account Number</th>
        <th>Account Name</th>
        <th>Currency</th>
        <th>Bank Name</th>
        <th>Amount</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($account as $key=>$acc)
    <tr>
       
        
        <td>{{$acc->account_number}}</td>
        <td style="font-size:16px" ><a href="{{route('admin.bank.show',$acc->id)}}" 
        class="text-uppercase"><strong>{{$acc->account_name}}</a></strong></td>

        <td>{{$acc->currency}}</td>
        <td>{{$acc->bank_name}}</td>
        <td style="font-size:16px">{{env('APP_CURRENCY')}} 
                           {{ TaskHelper::getLedgerBalance($acc->ledger_id)}}</td>
        <td>
            @if( $acc->isEditable())<a href="{{route('admin.bank.edit',$acc->id)}}"><i class="fa fa-edit"></i></a>
            @else
             <i class="fa fa-pencil-square-o text-muted" title="{{ trans('admin/permissions/general.error.cant-edit-this-permission') }}"></i>
            @endif
        </td>
   </tr>

    @endforeach
</tbody>

</table>
<div align="center">{!! $account->render() !!}</div>





<div class="row">
                    <div class="col-md-12">

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped" id="clients-table">
                                <caption>Latest Income Transaction</caption>
                                <thead class="bg-maroon">
                                    <tr>
                                        <th>Id</th>
                                        <th>Reference</th>
                                        <th>Tag</th>
                                        <th>Account</th>
                                        <th>Income Type</th>
                                        <th>Amount Deposited</th>
                                        <th>Date</th>
                                        <th>Fiscal Year</th>
                                        <th>Created By</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($income as $i)
                                    <tr>
                                        <td>{{\FinanceHelper::getAccountingPrefix('INCOME_PRE')}}{{$i->id}}</td>
                                        <td title="{{$i->description}}">{{$i->reference_no}}</td>
                                        <td title=>{{$i->tag->name}}</td>
                                        <td title=>{{$i->banckAcc->account_name}}</td>
                                        <td style="font-size:16px !important">
                                            
                                                {{$types[$i->income_type]}}
                                                <small>
                                                    @if(strlen($i->customers->name) > 25) {{ substr($i->customers->name,0,25).'...' }} @else {{ ucfirst($i->customers->name) }} @endif
                                                </small>
                                            
                                        </td>
                                        <td style="font-size:16px !important" >{{ env('APP_CURRENCY') }} 
                                        {{number_format($i->amount,2)}}
                                        
                                        </td>
                                        <td>{{date('dS Y M',strtotime($i->date_received))}}</td>
                                        <td>{{ $i->fiscalyear->fiscal_year }}</td>
                                        <td>{{$i->user->username}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
</div>
@endsection
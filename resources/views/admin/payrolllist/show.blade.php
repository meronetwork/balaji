@extends('layouts.master')
@section('content')

<style>
    .required { color: red; }
    .panel-custom .panel-heading {
        border-bottom: 2px solid #1797be;
        margin-bottom: 10px;
    }

    .btn-purple, .btn-purple:hover {
        color: #ffffff;
        background-color: #7266ba;
        border-color: transparent;
    }

    input.form-control {
     width: 80px; 
    }

    .show_print { display: none; }
    .mr, #DataTables_length { margin-right: 10px !important; }
</style>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                <small>{!! $page_description ?? "Page description" !!}</small>
            </h1>

         {{-- {{ TaskHelper::topSubMenu('topsubmenu.payroll')}} --}}


            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Make Payment @if(\Request::get(payment_month)) of {{date('M Y', strtotime(\Request::get(payment_month)))}} @endif</strong> 
                </div> 
            </div>
            <div class="panel-body">
                <form id="attendance-form" role="form" enctype="multipart/form-data" action="/admin/payroll/create_payroll" method="get" class="form-horizontal form-groups-bordered">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <div class="input-group">
                            <select class="form-control" name="department_id" readonly="">
                                <option value="0">Select Department</option>
                              @foreach($departments as $department)
                               <option value="{{$department->departments_id}}" @if($payroll->departments_id == $department->departments_id) selected="selected" @endif>{{$department->deptname}}</option>
                              @endforeach
                            </select>    
                             
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input required="" type="text" class="form-control payment_month" value="{{$payroll->date }}" name="payment_month" id="payment_month" readonly="">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="EmpprintReport">
    <div class="row">
        <div class="col-sm-12 std_print">
                        <div class="panel panel-custom table-responsive">
                           @csrf
                           <input type="hidden" name="date" value="{{\Request::get('payment_month')}}">
                            <table class="table table-triped DatsaTables  dataTable no-footer dtr-inline" id="DataTables">
                              <thead>
                                 <tr>
                                    <td class="text-bold col-sm-1">EMP ID</td>
                                    <td class="text-bold">Name</td>
                                    <td class="text-bold">Attendance</td>
                                    <td class="text-bold">Annual Leave</td>
                                    <td class="text-bold col-sm-1">Sick Leave</td>
                                    <td class="text-bold">PHL</td>
                                    <td class="text-bold">MOL/ML/PL</td>
                                    <td class="text-bold">LWP</td>
                                    <td class="text-bold">Absent</td>
                                    <td class="text-bold">1/2 Day Late</td>
                                    <td class="text-bold"> Total </td>
                                    <td class="text-bold"> Payble Attendance </td>
                                    <td class="text-bold"> OT Hours </td>
                                    <td class="text-bold"> T.Basic </td>
                                    <td class="text-bold"> Others Allowance </td>
                                    <td class="text-bold"> D.A </td>
                                    <td class="text-bold">Total</td>
                                    <td class="text-bold">2 hrs additional</td>
                                    <td class="text-bold">Total Salary</td>
                                    <td class="text-bold">Salary for the month</td>
                                    <td class="text-bold">Working Days Basic</td>
                                    <td class="text-bold">OT Amount</td>
                                    <td class="text-bold">Errors Adjust</td>
                                    <td class="text-bold">Gratuity</td>
                                    <td class="text-bold">PF</td>
                                    <td class="text-bold">CTC</td>
                                    <td class="text-bold">Adv.</td>
                                    <td class="text-bold">P.F</td>
                                    <td class="text-bold">CIT</td>
                                    <td class="text-bold">Uniform Deduction</td>
                                    <td class="text-bold">Monthly Payable Amount</td>
                                    <td class="text-bold">SST</td>
                                    <td class="text-bold">Net Salary</td>
                                    <td class="text-bold">Remarks</td>
                                 </tr>
                              </thead>
                              <tbody>
                                  <?php
                                    $total_basic_sal = 0;
                                    $total_net_sal = 0;
                                    $total_overtime = 0;  
                                    $total_fine = 0;
                                    $total_sal = 0;
                                ?>
                         
                                @foreach($payroll->payrollDetails as $sk => $sv)
                                 <tr>
                                    <td class="col-sm-1">{{ $sv->user_id }}
                                    </td>
                                    <td>{{ $sv->user->first_name.' '.$sv->user->last_name }}</td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  Attendance"><input type="text" name="attendance[]" class="form-control" value="{{$sv->attendance}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | Annual leave"><input type="text" name="anual_leave[]" class="form-control" value="{{$sv->anual_leave}}" readonly=""></td> 
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | sick leave"><input type="text" name="sick_leave[]" class="form-control" value="{{$sv->sick_leave}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | phl"><input type="text" name="phl[]" class="form-control" value="{{$sv->phl}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  mol ml pl"><input type="text" name="mol_ml_pl[]" class="form-control" value="{{$sv->mol_ml_pl}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | lwp"><input type="text" name="lwp[]" class="form-control" value="{{$sv->lwp}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  absent"><input type="text" name="absent[]" class="form-control" value="{{$sv->absent}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | late half days"><input type="text" name="late_half_days[]" class="form-control" value="{{$sv->late_half_days}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} total attendance"><input type="text" name="total_days[]" class="form-control" value="{{$sv->total_days}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | payable attendance"><input type="text" name="payable_attendance[]" class="form-control" value="{{$sv->payable_attendance}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  ot hours"><input type="text" name="ot_hours[]" class="form-control" value="{{$sv->ot_hours}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  t basic"><input type="text" name="t_basic[]" class="form-control" value="{{$sv->t_basic}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | other allowance"><input type="text" name="other_allowance[]" class="form-control" value="{{$sv->other_allowance}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} da"><input type="text" name="da[]" class="form-control" value="{{$sv->da}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | total after allowannce"><input type="text" name="total_after_allowance[]" class="form-control" value="{{$sv->total_after_allowance}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | additional attendance two hours"><input type="text" name="additional_attendance_two_hours[]" class="form-control" value="{{$sv->additional_attendance_two_hours}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  total salary"><input type="text" name="total_salary[]" class="form-control"  value="{{$sv->total_salary}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  salary for month"><input type="text" name="salary_for_the_month[]" class="form-control" value="{{$sv->salary_for_the_month}}" readonly=""></td>
                                  
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | Working days basic"><input type="text" name="working_days_basic[]" class="form-control" value="{{$sv->working_days_basic}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | ot amount"><input type="text" name="ot_amount[]" class="form-control"  value="{{$sv->ot_amount}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  error adjustment"><input type="text" name="error_adjust[]" class="form-control" value="{{$sv->error_adjust}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | Gratuity"><input type="text" name="gratuity[]" class="form-control"
                                        value="{{$sv->gratuity}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  PF"><input type="text" name="pf[]" class="form-control" value="{{$sv->pf}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | ctc"><input type="text" name="ctc[]" class="form-control"  value="{{$sv->ctc}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | adv"><input type="text" name="adv[]" class="form-control"  value="{{$sv->adv}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}| pf after ctc"><input type="text" name="pf_after_ctc[]" class="form-control"  value="{{$sv->pf_after_ctc}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  cit"><input type="text" name="cit[]" class="form-control"  value="{{$sv->cit}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  Uniform deduction"><input type="text" name="uniform_deduction[]" class="form-control"  value="{{$sv->uniform_deduction}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  Monthly payable amount"><input type="text" name="monthly_payable_amount[]" class="form-control" value="{{$sv->monthly_payable_amount}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} sst"><input type="text" name="sst[]" class="form-control" value="{{$sv->sst}}" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | net salary"><input type="text" name="net_salary[]" class="form-control" value="{{$sv->net_salary}}" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | remarks"><input type="text" name="remarks[]" class="form-control" value="{{$sv->remarks}}" readonly=""></td>
                                 </tr>
                               

                                <?php
                                    $total_basic_sal = $total_basic_sal + $template->basic_salary;
                                    $total_net_sal = $total_net_sal + $net_salary;
                                    $total_overtime =  $total_overtime+$overtime_money;  

                                ?>

                                 @endforeach

                              </tbody>
                                 {{--}} <tr>
                                    <td colspan="2"></td>
                                    <td style="float: right">Total</td>
                                    <td>{{ $total_basic_sal }}</td>
                                    <td>{{ $total_net_sal }}</td>
                                    <td>{{ $total_overtime }}</td>
                                    <td>{{ $total_fine }}</td>
                                    <td>{{ $total_sal }}</td>
                                 </tr> --}}
                           </table>
                        </div>
                        <a href="/admin/payroll/download-payroll?payroll_id={{$payroll->id}}" class="btn btn-primary"><i class="fa fa-download"></i>Download Excel</a>
        </div>
    </div>
</div>

<div class="modal fade" id="payment_show" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">

        </div>
    </div>
</div>
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')

<!-- SELECT2-->
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2.css") }}">
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2-bootstrap.css") }}">
<script src="{{ asset("/bower_components/admin-lte/select2/js/select2.js") }}"></script>



<script type="text/javascript">
    $(function() {

        $('#payment_month').datetimepicker({
            format: 'YYYY-MM',
            sideBySide: true
        });

        $('.select_box').select2({
            theme: 'bootstrap',
        });

        $('[data-toggle="tooltip"]').tooltip();

 
    
    });
</script>
@endsection

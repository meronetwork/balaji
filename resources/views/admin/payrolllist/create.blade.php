@extends('layouts.master')
@section('content')

<style>
    .required { color: red; }
    .panel-custom .panel-heading {
        border-bottom: 2px solid #1797be;
        margin-bottom: 10px;
    }

    .btn-purple, .btn-purple:hover {
        color: #ffffff;
        background-color: #7266ba;
        border-color: transparent;
    }

    input.form-control {
     width: 80px; 
    }

    .show_print { display: none; }
    .mr, #DataTables_length { margin-right: 10px !important; }
</style>

 <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
            <h1>
                Update Check Individual Payroll Payment
                <small>{!! $page_description ?? "Page description" !!}</small>
            </h1>
            <p> Select Department and Month </p>

         {{-- {{ TaskHelper::topSubMenu('topsubmenu.payroll')}} --}}


            {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
        </section>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Make Payment @if(\Request::get(payment_month)) of {{date('M Y', strtotime(\Request::get(payment_month)))}} @endif</strong> 
                </div> 
            </div>
            <div class="panel-body">
                <form id="attendance-form" role="form" enctype="multipart/form-data" action="/admin/payroll/create_payroll" method="get" class="form-horizontal form-groups-bordered">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <div class="input-group">
                            <select class="form-control" name="department_id" required="">
                                <option value="0">Select Department</option>
                              @foreach($departments as $department)
                               <option value="{{$department->departments_id}}" @if(\Request::get('department_id') == $department->departments_id) selected="selected" @endif>{{$department->deptname}}</option>
                              @endforeach
                            </select>    
                             
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input required="" type="text" class="form-control payment_month" value="{{ \Request::get('payment_month') ? \Request::get('payment_month') : '' }}" name="payment_month" id="payment_month">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" id="sbtn" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@if($users)

<div id="EmpprintReport">
    <div class="row">
        <div class="col-sm-12 std_print">
         <form method="post" action="{{route('admin.payrolllist.store')}}"> 
                        <div class="panel panel-custom table-responsive">
                           @csrf
                           <input type="hidden" name="date" value="{{\Request::get('payment_month')}}">
                            <table class="table table-triped DatsaTables  dataTable no-footer dtr-inline" id="DataTables">
                              <thead>
                                 <tr>
                                    <td class="text-bold col-sm-1">EMP ID</td>
                                    <td class="text-bold">Name</td>
                                    <td class="text-bold">Attendance</td>
                                    <td class="text-bold">Annual Leave</td>
                                    <td class="text-bold col-sm-1">Sick Leave</td>
                                    <td class="text-bold">PHL</td>
                                    <td class="text-bold">MOL/ML/PL</td>
                                    <td class="text-bold">LWP</td>
                                    <td class="text-bold">Absent</td>
                                    <td class="text-bold">1/2 Day Late</td>
                                    <td class="text-bold"> Total Days</td>
                                    <td class="text-bold"> Payble Attendance </td>
                                    <td class="text-bold"> OT Hours </td>
                                    <td class="text-bold"> T.Basic </td>
                                    <td class="text-bold"> Others Allowance </td>
                                    <td class="text-bold"> D.A </td>
                                    <td class="text-bold">Total</td>
                                    <td class="text-bold">2 hrs additional</td>
                                    <td class="text-bold">Total Salary</td>
                                    <td class="text-bold">Salary for the month</td>
                                    <td class="text-bold">Working Days Basic</td>
                                    <td class="text-bold">OT Amount</td>
                                    <td class="text-bold">Errors Adjust</td>
                                    <td class="text-bold">Gratuity</td>
                                    <td class="text-bold">PF</td>
                                    <td class="text-bold">CTC</td>
                                    <td class="text-bold">Adv.</td>
                                    <td class="text-bold">P.F</td>
                                    <td class="text-bold">CIT</td>
                                    <td class="text-bold">Uniform Deduction</td>
                                    <td class="text-bold">Monthly Payable Amount</td>
                                    <td class="text-bold">SST</td>
                                    <td class="text-bold">Net Salary</td>
                                    <td class="text-bold">Remarks</td>
                                 </tr>
                              </thead>
                              <tbody>
                                  <?php
                                    $total_basic_sal = 0;
                                    $total_net_sal = 0;
                                    $total_overtime = 0;  
                                    $total_fine = 0;
                                    $total_sal = 0;
                                ?>
                         
                                @foreach($users as $sk => $sv)
                                 <tr>
                                    <td class="col-sm-1">{{ $sv->user_id }}
                                        <input type="hidden" name = "user_id[]" value="{{ $sv->user_id }}">
                                        <input type="hidden" name = "departments_id" value="{{ \Request::get('department_id') }}">
                                    </td>
                                    <td>{{ $sv->first_name.' '.$sv->last_name }}</td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  Attendance"><input type="number" name="attendance[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | Annual leave"><input type="number" name="anual_leave[]" class="form-control"></td> 
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | sick leave"><input type="number" name="sick_leave[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | phl"><input type="number" name="phl[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  mol ml pl"><input type="number" name="mol_ml_pl[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | lwp"><input type="number" name="lwp[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  absent"><input type="number" name="absent[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | late half days"><input type="number" name="late_half_days[]" class="form-control"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} total Working Days"><input type="number" name="total_days[]" value="{{$total_days}}" class="form-control total_days" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | payable attendance"><input type="number" name="payable_attendance[]" class="form-control payable_attendance"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  ot hours"><input type="number" name="ot_hours[]" class="form-control ot_hours"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  t basic"><input type="number" name="t_basic[]" class="form-control t_basic"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | other allowance"><input type="number" name="other_allowance[]" class="form-control other_allowance"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} da"><input type="number" name="da[]" class="form-control da"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | total after allowannce"><input type="number" name="total_after_allowance[]" class="form-control total_after_allowance" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | additional attendance two hours"><input type="number" name="additional_attendance_two_hours[]" class="form-control additional_attendance_two_hours"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  total salary"><input type="number" name="total_salary[]" class="form-control total_salary" readonly=""></td>

                                     <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  total salary"><input type="number" name="salary_for_the_month[]" class="form-control salary_for_the_month" readonly=""></td>

                                  
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | Working days basic"><input type="number" name="working_days_basic[]" class="form-control working_days_basic" readonly=""></td>

                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | ot amount"><input type="number" name="ot_amount[]" class="form-control ot_amount" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  error adjustment"><input type="number" name="error_adjust[]" class="form-control error_adjust"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | Gratuity"><input type="number" name="gratuity[]" class="form-control gratuity" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  PF"><input type="number" name="pf[]" class="form-control pf" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | ctc"><input type="number" name="ctc[]" class="form-control ctc" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | adv"><input type="number" name="adv[]" class="form-control adv"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}| pf after ctc"><input type="number" name="pf_after_ctc[]" class="form-control pf_after_ctc" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  cit"><input type="number" name="cit[]" class="form-control cit"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  Uniform deduction"><input type="number" name="uniform_deduction[]" class="form-control uniform"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }}|  Monthly payable amount"><input type="number" name="monthly_payable_amount[]" class="form-control monthly_payable_amount" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} sst"><input type="number" name="sst[]" class="form-control sst"></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | net salary"><input type="number" name="net_salary[]" class="form-control net_salary" readonly=""></td>
                                    <td title = "{{ $sv->first_name.' '.$sv->last_name }} | remarks"><input type="text" name="remarks[]" class="form-control"></td>
                                 </tr>
                               

                                <?php
                                    $total_basic_sal = $total_basic_sal + $template->basic_salary;
                                    $total_net_sal = $total_net_sal + $net_salary;
                                    $total_overtime =  $total_overtime+$overtime_money;  

                                ?>

                                 @endforeach

                              </tbody>
                                 {{--}} <tr>
                                    <td colspan="2"></td>
                                    <td style="float: right">Total</td>
                                    <td>{{ $total_basic_sal }}</td>
                                    <td>{{ $total_net_sal }}</td>
                                    <td>{{ $total_overtime }}</td>
                                    <td>{{ $total_fine }}</td>
                                    <td>{{ $total_sal }}</td>
                                 </tr> --}}
                           </table>
                        </div>
                <input type="submit" value="submit" class="btn btn-primary">
        </form> 
        </div>
    </div>
</div>
@endif

<div class="modal fade" id="payment_show" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">

        </div>
    </div>
</div>
@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')

<!-- SELECT2-->
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2.css") }}">
<link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/select2/css/select2-bootstrap.css") }}">
<script src="{{ asset("/bower_components/admin-lte/select2/js/select2.js") }}"></script>



<script type="text/javascript">
    $(function() {

        $('#payment_month').datetimepicker({
            format: 'YYYY-MM',
            sideBySide: true
        });

        $('.select_box').select2({
            theme: 'bootstrap',
        });

        $('[data-toggle="tooltip"]').tooltip();


    $(document).on('input', '.ot_hours,.payable_attendance,.t_basic,.other_allowance,.da,.additional_attendance_two_hours,.error_adjust,.adv,.cit,.uniform,.sst', function() {
        var parentDiv=$(this).parent().parent();
        var t_basic=parentDiv.find('.t_basic').val();
        var other_allowance=parentDiv.find('.other_allowance').val();
        var da=parentDiv.find('.da').val();
        var additional_two_hours=parentDiv.find('.additional_attendance_two_hours').val();

        var total = (Number(t_basic)+Number(other_allowance)+Number(da));
        parentDiv.find('.total_after_allowance').val(total);

        var total_salary = (Number(total)+Number(additional_two_hours));
        parentDiv.find('.total_salary').val(total_salary);


       
        var total_days=parentDiv.find('.total_days').val();
        var payable_attendance=parentDiv.find('.payable_attendance').val();

        var salary_for_the_month = Number(total_salary)/Number(total_days) * Number(payable_attendance);
        parentDiv.find('.salary_for_the_month').val(salary_for_the_month);
        var working_days_basic = Number(t_basic)/Number(total_days)*Number(payable_attendance);

        parentDiv.find('.working_days_basic').val(Number(t_basic)/Number(total_days)*Number(payable_attendance));


        var ot_hours = parentDiv.find('.ot_hours').val();
        var ot_amount = t_basic/(365*8)*1.5*ot_hours;
        parentDiv.find('.ot_amount').val(ot_amount);

        var error_adjust = parentDiv.find('.error_adjust').val();
        var gratuity = Number(working_days_basic)*8.33/100;
        parentDiv.find('.gratuity').val(Number(gratuity));
        var pf = Number(t_basic)*10/100;
        parentDiv.find('.pf').val(Number(t_basic)*10/100);

        var ctc = Number(salary_for_the_month) + Number(ot_amount) + Number(error_adjust) + Number(gratuity)+Number(pf);
        parentDiv.find('.ctc').val(ctc);

        var advanced = parentDiv.find('.adv').val();
        parentDiv.find('.pf_after_ctc').val(Number(t_basic)*2*10/100);
        var cit = parentDiv.find('.cit').val();
        var uniform = parentDiv.find('.uniform').val();

        var monthly_payable_amount = Number(ctc)-Number(advanced)-Number(pf*2)-Number(cit)-Number(uniform);
        parentDiv.find('.monthly_payable_amount').val(Number(monthly_payable_amount));
      
        var sst = parentDiv.find('.sst').val();
        parentDiv.find('.net_salary').val(Number(monthly_payable_amount)-Number(sst));


    });

 
    
    });
</script>
@endsection

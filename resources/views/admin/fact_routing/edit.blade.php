@extends('layouts.master')

@section('head_extra')
<!-- Select2 css -->
@include('partials._head_extra_select2_css')

<style>
    .panel .mce-panel {
        border-left-color: #fff;
        border-right-color: #fff;
    }

    .panel .mce-toolbar,
    .panel .mce-statusbar {
        padding-left: 20px;
    }

    .panel .mce-edit-area,
    .panel .mce-edit-area iframe,
    .panel .mce-edit-area iframe html {
        padding: 0 10px;
        min-height: 350px;
    }

    .mce-content-body {
        color: #555;
        font-size: 14px;
    }

    .panel.is-fullscreen .mce-statusbar {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 200000;
    }

    .panel.is-fullscreen .mce-tinymce {
        height: 100%;
    }

    .panel.is-fullscreen .mce-edit-area,
    .panel.is-fullscreen .mce-edit-area iframe,
    .panel.is-fullscreen .mce-edit-area iframe html {
        height: 100%;
        position: absolute;
        width: 99%;
        overflow-y: scroll;
        overflow-x: hidden;
        min-height: 100%;
    }

    .col-md-4 {
        background: skyblue;
        border: 1px solid #ccc;
    }

</style>
@endsection

@section('content')
<link href="/bower_components/admin-lte/select2/css/select2.min.css" rel="stylesheet" />
<script src="/bower_components/admin-lte/select2/js/select2.min.js"></script>
<section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
    <h1>
        {{$page_title}}
        <small> Edit  {{$page_title}}</small>
    </h1>
    <p> {{$page_description}}</p>
    {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
</section>


<div class='row'>
    <div class='col-md-12'>
        <div class="box-body">

            <div id="orderFields" style="display: none;">
                <table class="table">
                    <tbody id="more-tr">
                    <tr>
                        <td>
                            <select class="form-control select2 product_id" name="area_id[]" required="required">

                                <option value="">Select Product</option>
                                @foreach($section as $key => $section_name)
                                    <option value="{{ $key }}" @if(isset($routingDetail->area_id) && $routingDetail->area_id == $key) selected="selected"@endif>{{ $section_name }}</option>
                                @endforeach

                            </select>
                        </td>
                        <td class="col-sm-1">
                            <input type="text" class="form-control description" name="descr[]" placeholder="Description"  required="required" >
                        </td>
                        <td>
                            <input type="time" class="form-control setup_time" name="setup_time[]" placeholder="Setup Time" >
                        </td>
                        <td>
                            <input type="time" class="form-control cycle_time" name="cycle_time[]" placeholder="Cycle Time" >
                        </td>
                        <td>
                            <input type="number" class="form-control overhead_cost" name="overhead_cost[]" placeholder="Overhead Cost"  step="any">
                        </td>

                        <td>
                            <input type="number" class="form-control labour_cost" name="labour_cost[]" placeholder="Labour Cost"  step="any">
                        </td>
                         <td>
                            <input type="number" class="form-control process_order" name="process_order[]" placeholder="process Order"  
                            required="required" step="any">
                        </td>
                        <td>
                            <div style="float:left; width:70%;">
                                <select name='to_user_id[]' class="form-control select2" >
                                    <option value="">Select User</option>
                                    @foreach($users as $id =>$name)
                                        <option value="{{ $id }}">{{ $name }}({{ $id }})</option>
                                    @endforeach
                                </select>
                            </div>

                            <a href="javascript::void(1);" style="width: 10%;">
                                <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    {!! Form::model( $order, ['route' => ['admin.production.fact_routing.update', $order->id], 'method' => 'PUT'] ) !!}
                    <div class="col-md-12 bg-maroon" style="margin-bottom: 15px;">
                    </div>

                    <div class="panel-body">

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Routing Name:</label>
                                    <input type="text" class="form-control pull-right " name="name" value="{{ $order->name}}" id="name">

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Manufacturing Product:</label>
                                    <select type="text" class="customer_id select2 form-control pull-right " name="product_id" id="product_id">
                                        <option value="">Select  Product</option>
                                        @if(isset($final_products))
                                            @foreach($final_products as $key => $fp)
                                                <option value="{{ $fp->id }}" @if($fp->id == $order->product_id) selected="selected" @endif>{{ $fp->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <!-- /.Request group -->
                            </div>



                        </div>
                        <div class="clearfix"></div><br /><br />
                        <div class="col-md-12">
                            <a href="javascript::void(0)" class="btn btn-default btn-xs" id="addMore" style="float: right;">
                                <i class="fa fa-plus"></i> <span>Add Routing Detail</span>
                            </a>
                        </div>
                        <hr />
                        <table class="table">
                            <thead>
                                <tr class="bg-maroon">
                                    <th>Workstation Section * </th>
                                    <th>Description *</th>
                                    <th>Setup Time*</th>
                                    <th>Cycle Time *</th>
                                    <th>Overhead Cost *</th>
                                    <th>Labour Cost *</th>
                                    <th>Proc. Order</th>
                                    <th>User</th>
                                </tr>
                            </thead>

                            <tbody id='multipleDiv'>
                                @foreach($orderDetails as $odk => $odv)
                                    <tr>
                                        <input type="hidden" class="order_id" name="order_id[]" value="{{$odv->id}}">
                                        <td>
                                            <select class="form-control select2 product_id" name="area_id[]" required="required">

                                                <option value="">Select Product</option>
                                                @foreach($section as $key => $section_name)
                                                    <option value="{{ $key }}" @if(isset($odv->area_id) && $odv->area_id == $key) selected="selected"@endif>{{ $section_name }}</option>
                                                @endforeach

                                            </select>
                                        </td>
                                        <td class="col-sm-1">
                                            <input type="text" class="form-control description" name="descr[]" placeholder="Description"  value="{{$odv->descr}}" required="required" >
                                        </td>
                                        <td>
                                            <input type="time" class="form-control setup_time" name="setup_time[]" placeholder="Setup Time"  value="{{$odv->setup_time}}" required="required" >
                                        </td>
                                        <td>
                                            <input type="time" class="form-control cycle_time" name="cycle_time[]" placeholder="Cycle Time" value="{{$odv->cycle_time}}" required="required" >
                                        </td>
                                        <td>
                                            <input type="number" class="form-control overhead_cost" name="overhead_cost[]" placeholder="Overhead Cost" value="{{$odv->overhead_cost}}" required="required" step="any">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control labour_cost" name="labour_cost[]" placeholder="Labour Cost" value="{{$odv->labour_cost}}" required="required" step="any">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control process_order" name="process_order[]" placeholder="process Order" value="{{$odv->process_order}}" required="required" step="any">
                                        </td>

                                        <td>
                                            <div style="float:left; width:70%;">
                                                <select name='to_user_id[]' class="form-control select2" >
                                                    <option value="">Select User</option>
                                                    @foreach($users as $id => $name)
                                                        <option value="{{ $id }}" @if(isset($odv->to_user_id) && $odv->to_user_id == $id) selected="selected"@endif>{{ $name }}({{ $id }})</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <a href="javascript::void(1);" style="width: 10%;">
                                                <i class="remove-this btn btn-xs btn-danger icon fa fa-trash deletable" style="float: right; color: #fff;"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="multipleDiv"></tr>
                            </tbody>
                        </table>





                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-social btn-foursquare">
                            <i class="fa fa-save"></i>Save Routing
                        </button>
                        <a href="/admin/production/fact_routing" class="btn btn-default">Cancel</a>
                    </div>
                    </form>
                </div>
            </div>

        </div><!-- /.box-body -->
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection
<div class='supplier_options' style="display: none;">
    <div id='_supplier'>
        <option value="">Select Supplier</option>
        @if(isset($clients))
        @foreach($clients as $key => $uk)
        <option value="{{ $uk->id }}" @if($order && $uk->id == $order->supplier_id){{ 'selected="selected"' }}@endif>{{ '('.$uk->id.') '.$uk->name.' ('.$uk->vat.')' }}</option>
        @endforeach
        @endif
    </div>
    <div id='_paid_through'>
        <option value="">Select Supplier</option>
        @if(isset($clients))
        @foreach($paid_through as $key => $uk)
        <option value="{{ $uk->id }}" @if($order && $uk->id == $order->supplier_id){{ 'selected="selected"' }}@endif>{{ '('.$uk->id.') '.$uk->name.' ('.$uk->location.')' }}</option>
        @endforeach
        @endif
    </div>
</div>
@section('body_bottom')
<!-- form submit -->
@include('partials._body_bottom_submit_bug_edit_form_js')
<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true
        });

    });

</script>
@include('admin.purchase.nep_eng_date_toogle')
<script>

    $("#addMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").after($('#orderFields #more-tr').html());
        $(".multipleDiv").next('tr').find('select').select2({
            width: '100%'
        });
        $('#addmorProducts').show(300);

    });
    $("#addCustomMore").on("click", function() {
        //$($('#orderFields').html()).insertBefore(".multipleDiv");
        $(".multipleDiv").after($('#CustomOrderFields #more-custom-tr').html());
    });

    $(document).on('click', '.remove-this', function() {
        $(this).parent().parent().parent().remove();
        $("#multipleDiv .product_id").length > 0 ? $('#addmorProducts').show(300) : $('#addmorProducts').hide(300);
    });



</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.customer_id').select2();
        $('.project_id').select2();
    });

</script>

<script type="text/javascript">
    $(function() {
        $('.datepicker').datetimepicker({
            //inline: true,
            format: 'YYYY-MM-DD'
            , sideBySide: true
            , allowInputToggle: true,

        });

    });

</script>
@endsection

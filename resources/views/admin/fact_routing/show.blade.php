@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')

@endsection

@section('content')
    <style>
        .box-comment {
            margin-bottom: 5px;
            padding-bottom: 5px;
            border-bottom: 1px solid #eee;
        }

        .box-comment img {
            float: left;
            margin-right: 10px;
        }

        .username {
            font-weight: bold;
        }

        .comment-text span {
            display: block;
        }

    </style>

    <section class="content-header" style="margin-top: -35px; margin-bottom: 20px">
        <h1>
            {{$page_title}}
            <small> Show  {{$page_title}}</small>
        </h1>
        <p> {{$page_description}}</p>
        {!! MenuBuilder::renderBreadcrumbTrail(null, 'root', false)  !!}
    </section>

    <div class="panel panel-custom box">
        <div class="panel-heading">
            <div class='row'>
                <div class='col-md-12'>
                    <b><font size="4">Factory BOM no# {{$ord->id}}</font></b>
                    <div style="display: inline; float: right;">
                        <a class="btn btn-success btn-sm"  title="Import/Export Leads" href="{{ route('admin.production.fact_routing.print',$ord->id) }}">
                            <i class="fa  fa-print"></i>&nbsp;<strong>PRINT</strong>
                        </a>
                        <a class="btn btn-danger btn-sm"  title="Import/Export Leads" href="javascript:history.back()">
                            <i class="fa  fa-backward"></i>&nbsp;<strong>Back to list</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
            <tr>
                <th style="text-align:center;width:20px !important">
                    <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                        <i class="fa fa-check-square-o"></i>
                    </a>
                </th>
                <th>ID</th>
                <th>Date</th>
                <th>Name</th>
                <th>BOM</th>
                <th>Product</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td >
                    <input type="checkbox" name="event_id" value="">
                </td>
                <td>{{$ord->id}}</td>
                <td>{{ date('dS M Y', strtotime($is->created_at)) }}</td>
                <td>{{ucfirst(trans($ord->name))}}</td>
                <td>{{ucfirst(trans($ord->bom->bom_name))}}</td>
                <td>{{ucfirst(trans($ord->product->name))}}</td>

            </tr>
            </tbody>
        </table>
        <div class="panel-heading">
            <div class='row'>
                <div class='col-md-12'>
                    <b><font size="4">Factory BOM details</font></b>
                </div>
            </div>
        </div>
        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
            <tr>
                <th>#</th>
                <th>Workstation Section  </th>
                <th>Description </th>
                <th>Setup Time</th>
                <th>Cycle Time </th>
                <th>Overhead Cost </th>
                <th>Labour Cost </th>
                <th>User</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orderDetails as $key=>$d)
                <tr>
                    <td >
                        {{$key + 1 }}
                    </td>
                    <td>{{$d->section->section_name}}&nbsp;</td>
                    <td>{{$d->descr}}&nbsp;</td>
                    <td>{{$d->setup_time}}&nbsp;</td>
                    <td>{{$d->cycle_time}}&nbsp;</td>
                    <td>{{$d->overhead_cost}}&nbsp;</td>
                    <td>{{$d->labour_cost}}&nbsp;</td>

                    <td>{{$d->to_user_id ? $d->user->first_name : ' '}} ({{$d->to_user_id}})&nbsp;</td>



                </tr>
            @endforeach

            </tbody>
        </table>
    </div>


@endsection

@section('body_bottom')
    <!-- Select2 js -->
    @include('partials._body_bottom_select2_js_user_search')
@endsection

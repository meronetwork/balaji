<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ env('APP_COMPANY')}} | Factory BOMr</title>

    <!-- block from searh engines -->
    <meta name="robots" content="noindex">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Set a meta reference to the CSRF token for use in AJAX request -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons 4.7.0 -->
    <link href="{{ asset("/bower_components/admin-lte/font-awesome/css/all.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.1 -->
    <link href="{{ asset("/bower_components/admin-lte/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />

    <!-- Application CSS-->
    {{--    <link href="{{ asset(elixir('css/all.css')) }}" rel="stylesheet" type="text/css" />--}}

    <style>
        table {
            border-collapse: separate;
            border-spacing: 0px;
        }

        table td,
        table th {
            background-color: transparent;
            overflow: hidden;
            z-index: 1;
            border-right: 0;
            border-bottom: 0;
        }

        table th:before,
        table td:before {
            content: "";
            padding: 0;
            height: 1px;
            line-height: 1px;
            width: 1px;
            margin: -4px -994px -996px -6px;
            display: block;
            border: 0;
            z-index: -1;
            position: relative;
            top: -500px;
        }

        table th:before {
            border-top: 999px solid #c9c9c9;
            border-left: 999px solid #c9c9c9;
        }

        table td:before {
            border-top: 999px solid #eeeeee;
            border-left: 999px solid #eeeeee;
        }

    </style>


</head>

<body onload="window.print();" cz-shortcut-listen="true" class="skin-blue sidebar-mini">

<div class='wrapper'>

    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <div class="col-xs-3">
                        <img src="/images/logo-mini.png" style="max-width: 200px;">
                    </div>
                    <div class="col-xs-9">
                            <span class="pull-right">

                            </span>
                    </div>
                    <hr>
                </h2>

            </div>

            <!-- /.col -->
        </div>
        <div class="panel-heading">
            <div class='row'>
                <div class='col-md-12'>
                    <b><font size="4">Factory Routing no# {{$ord->id}}</font></b>

                </div>
            </div>
        </div>

        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
            <tr>
                <th style="text-align:center;width:20px !important">
                    <a class="btn" href="#" onclick="toggleCheckbox(); return false;" title="{{ trans('general.button.toggle-select') }}">
                        <i class="fa fa-check-square-o"></i>
                    </a>
                </th>
                <th>ID</th>
                <th>Date</th>
                <th>Name</th>
                <th>BOM</th>
                <th>Product</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td >
                    <input type="checkbox" name="event_id" value="">
                </td>
                <td>{{$ord->id}}</td>
                <td>{{ date('dS M Y', strtotime($is->created_at)) }}</td>
                <td>{{ucfirst(trans($ord->name))}}</td>
                <td>{{ucfirst(trans($ord->bom->bom_name))}}</td>
                <td>{{ucfirst(trans($ord->product->name))}}</td>

            </tr>
            </tbody>
        </table>
        <div class="panel-heading">
            <div class='row'>
                <div class='col-md-12'>
                    <b><font size="4">Factory BOM details</font></b>
                </div>
            </div>
        </div>
        <table class="table table-hover table-no-border" id="leads-table">
            <thead>
            <tr>
                <th>#</th>
                <th>Workstation Section  </th>
                <th>Description </th>
                <th>Setup Time</th>
                <th>Cycle Time </th>
                <th>Overhead Cost </th>
                <th>Labour Cost </th>
                <th>User</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orderDetails as $key=>$d)
                <tr>
                    <td >
                        {{$key + 1 }}
                    </td>
                    <td>{{$d->section->section_name}}&nbsp;</td>
                    <td>{{$d->descr}}&nbsp;</td>
                    <td>{{$d->setup_time}}&nbsp;</td>
                    <td>{{$d->cycle_time}}&nbsp;</td>
                    <td>{{$d->overhead_cost}}&nbsp;</td>
                    <td>{{$d->labour_cost}}&nbsp;</td>

                    <td>{{$d->to_user_id ? $d->user->first_name : ' '}} ({{$d->to_user_id}})&nbsp;</td>



                </tr>
            @endforeach

            </tbody>
        </table>

    </section>

</div><!-- /.col -->

</body>

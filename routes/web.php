<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Authentication routes...
Auth::routes();

// // Password reset link request routes...
Route::get('password/email', ['as' => 'recover_password',        'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'recover_passwordPost',    'uses' => 'Auth\PasswordController@postEmail']);
// Password reset routes...
Route::get('password/reset/{token}', ['as' => 'reset_password',          'uses' => 'Auth\PasswordController@getReset']);
Route::post('password/reset', ['as' => 'reset_passwordPost',      'uses' => 'Auth\PasswordController@postReset']);

// Online Application Form
Route::get('enquiry', ['as' => 'enquiry',          'uses' => 'ApplicationController@form']);
Route::get('hotel/application/reservation-getroom-plan/{id}','ApplicationController@getroomPlan');
Route::post('post_enquiry', ['as' => 'post_enquiry',    'uses' => 'ApplicationController@postEnquiry']);
Route::get('enquiry_thankyou', ['as' => 'enqiry_thankyou',          'uses' => 'ApplicationController@thankyou']);



Route::post('post_reservation',   ['as' => 'post_reservation',    'uses' => 'ApplicationController@postReservation']);


// Online Ticket Form
Route::get('ticket', ['as' => 'ticket',          'uses' => 'ApplicationController@ticket']);
Route::post('post_ticket', ['as' => 'post_ticket',    'uses' => 'ApplicationController@postTicket']);
Route::get('ticket_thankyou', ['as' => 'ticket_thankyou',          'uses' => 'ApplicationController@ticketThankyou']);

// Online Job Application Form

Route::get('getClients', ['as' => 'getclients', 'uses' => 'ClientsController@get_client']);

Route::get('list_job', ['as' => 'list_job',      'uses' => 'ApplicationController@listJob']);
Route::get('apply_job/{id}', ['as' => 'apply_job',     'uses' => 'ApplicationController@applyJob']);
Route::get('apply_form/{designation_id}/{job_circular_id}', ['as' => 'jobenquiry',          'uses' => 'ApplicationController@jobApplyForm']);
Route::post('post_job', ['as' => 'post_job',      'uses' => 'ApplicationController@postJobApplication']);
Route::get('job_thankyou', ['as' => 'job_thankyou',  'uses' => 'ApplicationController@jobThankyou']);

// Registration terms
Route::get('faust', ['as' => 'faust',                   'uses' => 'FaustController@index']);


Route::get('eventsenquiry',   ['as' => 'events_enquiry',          'uses' => 'ApplicationController@events']);
Route::post('post_enquiry_events',   ['as' => 'post_enquiry_events',    'uses' => 'ApplicationController@postEventFromEnquiry']);
Route::get('event_enquiry_thankyou',   ['as' => 'events_enqiry_thankyou',          'uses' => 'ApplicationController@thankyouEvent']);

// Online Leads Application Form
Route::get('leadsenquiry',   ['as' => 'leads_enquiry',          'uses' => 'ApplicationController@leads']);
Route::post('post_enquiry_leads',   ['as' => 'post_enquiry_leads',    'uses' => 'ApplicationController@postLeadFromEnquiry']);
Route::get('lead_enquiry_thankyou',   ['as' => 'leads_enqiry_thankyou',          'uses' => 'ApplicationController@thankyouLead']);


// Application routes...
Route::get('/', ['as' => 'backslash',   'uses' => 'HomeController@index']);
Route::get('home', ['as' => 'home',        'uses' => 'HomeController@index']);
Route::get('welcome', ['as' => 'welcome',     'uses' => 'HomeController@welcome']);

// Clock
Route::get('clockin', ['as' => 'clockin',   'uses' => 'ShiftAttendanceController@clockin']);
Route::get('clockout', ['as' => 'clockout',   'uses' => 'ShiftAttendanceController@clockout']);

Route::group([],function(){


    Route::get('/admin/clients/modals', ['as' => 'admin.clients.modals',            'uses' => 'ClientsController@showmodal']);
    Route::post('/admin/clients/modals', ['as' => 'admin.clients.pomodals',            'uses' => 'ClientsController@postModal']);
    Route::get('/admin/clients/modals/create', ['as' => 'admin.clients.modals.create',            'uses' => 'ClientsController@showmodal']);
    Route::post('/admin/clients/modals/store', ['as' => 'admin.clients.modals.store',            'uses' => 'ClientsController@postModal']);

    Route::get('/admin/clients/create', ['as' => 'admin.clients.create',           'uses' => 'ClientsController@create']);
    Route::post('/admin/clients', ['as' => 'admin.clients.store',            'uses' => 'ClientsController@store']);
    Route::get('/admin/clients/{leadId}', ['as' => 'admin.clients.show',             'uses' => 'ClientsController@show']);
    Route::patch('/admin/clients/{leadId}', ['as' => 'admin.clients.patch',            'uses' => 'ClientsController@update']);
    Route::put('/admin/clients/{leadId}', ['as' => 'admin.clients.update',           'uses' => 'ClientsController@update']);
    Route::delete('/admin/clients/{leadId}', ['as' => 'admin.clients.destroy',          'uses' => 'ClientsController@destroy']);
    Route::get('/admin/clients/{leadId}/edit', ['as' => 'admin.clients.edit',             'uses' => 'ClientsController@edit']);
    Route::get('/admin/clients/{leadId}/confirm-delete', ['as' => 'admin.clients.confirm-delete',   'uses' => 'ClientsController@getModalDelete']);
    Route::get('/admin/clients/{leadId}/delete', ['as' => 'admin.clients.delete',           'uses' => 'ClientsController@destroy']);
    Route::get('/admin/clients/{leadId}/enable', ['as' => 'admin.clients.enable',           'uses' => 'ClientsController@enable']);
    Route::get('/admin/clients/{leadId}/disable', ['as' => 'admin.clients.disable',          'uses' => 'ClientsController@disable']);

    Route::get('/admin/getLedgers',['as' => 'admin.getledger', 'uses' => 'COAController@get_ledger']);

    Route::get('attendace/{id}/calandar',['as'=>'admin.attendace.calandar','uses'=>'HrCalandarController@attendaceCalendar']);


});


// Routes in this group must be authorized.
Route::group(['middleware' => 'authorize'], function () {
    // Application routes...
    Route::get('dashboard', ['as' => 'dashboard',          'uses' => 'DashboardController@salesboard']);
    Route::get('hrboard', ['as' => 'hrboard',          'uses' => 'HrBoardController@hrBoard']);
    Route::get('user/profile', ['as' => 'user.profile',       'uses' => 'UsersController@profile']);
    Route::patch('user/profile', ['as' => 'user.profile.patch', 'uses' => 'UsersController@profileUpdate']);

    // Site administration section
    Route::group(['prefix' => 'admin'], function () {


        Route::get('front-cash',['as'=>'admin.front-cash.index','uses'=>'FrontCashController@index']);

        Route::get('front-cash/create',['as'=>'admin.front-cash.create','uses'=>'FrontCashController@create']);

        Route::post('front-cash/store',['as'=>'admin.front-cash.store','uses'=>'FrontCashController@store']);


        Route::get('get-guest-info-from-reservation/{id}',['as'=>'admin.get-guest-info-from-reservation','uses'=>'FrontCashController@getGuest']);





        Route::get('getpanno/{id}', ['as' => 'admin.getpanno', 'uses' => 'PurchaseController@getPanNUM']);

        Route::get('getposdiscount/{id}', ['as' => 'admin.getposdiscount', 'uses' => 'OrdersController@getposdiscount']);


        Route::get('orders/transfer/estimate/{id}',['as'=>'orders.transfer.estimate','uses'=>'OrdersController@roomtransferEstimate']);

        Route::get('posSummaryAmount',['as'=>'admin.posSummaryAmount','uses'=>'PosAnalysisController@posAmountSummary']);

        Route::get('posAmountSummaryDateWise',['as'=>'admin.posAmountSummaryDateWise','uses'=>'PosAnalysisController@posAmountSummaryDateWise']);


        Route::get('posAmountSummaryDateWiseExcel/{start_date?}/{end_date?}',['as'=>'admin.posAmountSummaryDateWiseExcel','uses'=>'PosAnalysisController@posAmountSummaryDateWiseExcel']);




        Route::get('hotel/sales',['as'=>'admin.hotelsales.index','uses'=>'HotelSalesReportController@index']);




        Route::get('posSummaryAnalysis',['as'=>'admin.posSummaryAnalysis','uses'=>'PosAnalysisController@posSummaryAnalysis']);

        //shift attendance report
        Route::get('shiftAttendance', ['as' => 'admin.shiftAttendance.index', 'uses' => 'ShiftAttendanceController@filter_report']);
        Route::post('shiftAttendance', ['as' => 'admin.shiftAttendance.filter', 'uses' => 'ShiftAttendanceController@filter_reportShow']);

        Route::get('shiftAttendance/{type}/download', ['as' => 'admin.shiftAttendance.downloadrep', 'uses' => 'ShiftAttendanceController@download_report']);

        Route::get('shiftAttendance/{user_id}/{shift}/{start_date}', ['as' => 'admin.shiftAttendance.filter.user', 'uses' => 'ShiftAttendanceController@filter_reportByUser']);

        Route::post('shiftAttendanceFix/{attendance_id}', 'ShiftAttendanceController@fixattendance');

        Route::get('timeHistory', ['as' => 'admin.shiftAttendance.timeHistory', 'uses' => 'ShiftAttendanceReportController@timeHistory']);

        Route::get('timeHistory/{type}/download', ['as' => 'admin.shiftAttendance.timeHistory.download', 'uses' => 'ShiftAttendanceReportController@downloadAttendaceUserWise']);

        Route::post('timeHistory', ['as' => 'admin.shiftAttendance.timeHistorysh', 'uses' => 'ShiftAttendanceReportController@timeHistoryShow']);

        Route::get('attendanceReports', ['as' => 'admin.shiftAttendance.attendanceReports', 'uses' => 'ShiftAttendanceReportController@attendanceReport']);

        Route::post('attendanceReports', ['as' => 'admin.shiftAttendance.attendanceReportsshow', 'uses' => 'ShiftAttendanceReportController@attendanceReportShow']);

        Route::get('attendanceReports/{type}/download', ['as' => 'admin.shiftAttendance.download', 'uses' => 'ShiftAttendanceReportController@download_report']);

        Route::get('shift_mark_attendance', ['as' => 'admin.shiftAttendance.mark_attendance', 'uses' => 'MarkShiftAttendanceController@create']);

        Route::post('shift_mark_attendance', ['as' => 'admin.shiftAttendance.mark_attendancest', 'uses' => 'MarkShiftAttendanceController@store']);
        Route::get('shift_mark_attendance/bulk', ['as' => 'admin.shiftAttendance.mark_attendance.bulk', 'uses' => 'MarkShiftAttendanceController@createBulk']);
        Route::post('shift_mark_attendance/bulk', ['as' => 'admin.shiftAttendance.mark_attendance.bulknxt', 'uses' => 'MarkShiftAttendanceController@createBulkNext']);
        Route::post('shift_mark_attendance/bulk/save', ['as' => 'admin.shiftAttendance.mark_attendance.save', 'uses' => 'MarkShiftAttendanceController@createBulkSave']);
        Route::get('shift_mark_attendance/{empId}/view_status', 'MarkShiftAttendanceController@viewStatus');
        Route::get('loyality',['uses'=>'admin.loyality.index','uses'=>'LoyalityController@index']);
        Route::post('products/multipledelete', ['as' => 'admin.products.multipledelete', 'uses' => 'ProductController@multipledelete']);
        //user Team routes
        Route::get('teams', ['as' => 'admin.teams.index', 'uses' => 'TeamController@index']);
        Route::get('teams/create', ['as' => 'admin.teams.create', 'uses' => 'TeamController@create']);
        Route::post('teams/create', ['as' => 'admin.teams.store', 'uses' => 'TeamController@store']);
        Route::get('teams/edit/{id}', ['as' => 'admin.teams.edit', 'uses' => 'TeamController@edit']);
        Route::post('teams/edit/{id}', ['as' => 'admin.teams.update', 'uses' => 'TeamController@update']);
        Route::get('teams/confirm-delete{id}', ['as' => 'admin.teams.confirm-delete', 'uses' => 'TeamController@getModalDelete']);
        Route::get('teams/delete/{id}', ['as' => 'admin.teams.delete', 'uses' => 'TeamController@destroy']);

        Route::get('users/teams/{id}', ['as' => 'admin.users.teams', 'uses' => 'TeamController@addUser']);
        Route::post('users/teams/{id}', ['as' => 'admin.users.teamspo', 'uses' => 'TeamController@postUser']);

        Route::get('users/teams/confirm-delete/{id}', ['as' =>
        'admin.users.teams.confirm-delete', 'uses' => 'TeamController@removeTeamMemberModal']);
        Route::get('users/teams/destroy/{id}', ['as' =>
        'admin.users.teams.delete', 'uses' => 'TeamController@removeTeamMember']);

        //hrroutes


        Route::get('users/assign_empid/{id}',['as'=>'admin.assign_empid','uses'=>'UsersController@assignid']);

        Route::post('users/assign_empid/{id}',['as'=>'admin.assign_empidst','uses'=>'UsersController@store_emp_id']);

        Route::get('user_reports_gni', ['as' => 'admin.users.report.gni', 'uses' => 'ExcelController@gniUsers']);


        Route::get('add_earned_leave',['as'=>'admin.add_earned_leave.index',
                'uses'=>'LeaveManagementController@addUserEarnedLeave']);

        Route::post('add_leave_user/{id}',['as'=>'admin.add_leave_user.store','uses'=>'LeaveCategoryController@addUser']);

        Route::get('remove_add_leave_user/{id}',['as'=>'admin.remove_add_leave_user.store','uses'=>'LeaveCategoryController@removeUser']);

        Route::post('add_earned_leave',['as'=>'admin.add_earned_leave.next',
                'uses'=>'LeaveManagementController@addUserEarnedLeaveNext']);

        Route::get('hotel/deleted-orders',['as'=>'admin.hotel.deleted-orders','uses'=>'OrdersController@deletedOrders']);



        Route::post('add_earned_leave/store',['as'=>'admin.add_earned_leave.store',
                'uses'=>'LeaveManagementController@addUserEarnedLeaveStore']);

        Route::get('add_earned_leave/{id}/edit',['as'=>'admin.add_earned_leave.edit',
                'uses'=>'LeaveManagementController@editUserEarnedLeave']);

        Route::post('add_earned_leave/{id}/edit',['as'=>'admin.add_earned_leave.up',
                'uses'=>'LeaveManagementController@updateUserEarnedLeave']);

        Route::get('add_earned_leave/{id}/destroy',['as'=>'admin.add_earned_leave.destroy',
                'uses'=>'LeaveManagementController@destroyUserEarnedLeave']);

        Route::get('get_user_leave_status/{id}','LeaveManagementController@getuserLeaveStatus');


        //





        Route::get('getProductsFront',['as' => 'admin.getproducts.front', 'uses' => 'ProductController@get_products_front']);


        //Privilege Card
        Route::get('privilege-card',['as'=>'admin.privilege-card.index','uses'=>'PrivilegeCardController@Index']);
        Route::get('privilege-card/create',['as'=>'admin.privilege-card.create','uses'=>'PrivilegeCardController@Create']);
        Route::post('privilege-card/create',['as'=>'admin.privilege-card.store','uses'=>'PrivilegeCardController@Store']);
        Route::get('privilege-card/edit/{id}',['as'=>'admin.privilege-card.edit','uses'=>'PrivilegeCardController@Edit']);
        Route::post('privilege-card/edit/{id}',['as'=>'admin.privilege-card.up','uses'=>'PrivilegeCardController@Update']);
        Route::get('privilege-card/confirm-delete/{id}',['as'=>'admin.privilege-card.confirm-delete','uses'=>'PrivilegeCardController@deleteModal']);
        Route::get('privilege-card/delete/{id}',['as'=>'admin.privilege-card.delete','uses'=>'PrivilegeCardController@destroy']);
        Route::get('privilege-card/show/{id}',['as'=>'admin.privilege-card.show','uses'=>'PrivilegeCardController@show']);
        Route::get('privilege-card/print/{id}', ['as' => 'admin.privilege-card.print',  'uses' => 'PrivilegeCardController@printCard']);
        Route::get('privilege-card/download/{id}', ['as' => 'admin.privilege-card.download',  'uses' => 'PrivilegeCardController@download']);
        Route::get('event-calendar',['as'=>'admin.events-calendar','uses'=>'EventController@eventscalendar']);


        Route::post('event-calendar',['as'=>'admin.eventscalendar.view','uses'=>'EventController@eventscalendarview']);

        // Recipes
//        Route::get('recipe',['as'=>'admin.recipe.index','uses'=>'RecipeController@Index']);
//        Route::get('recipe/create',['as'=>'admin.recipe.create','uses'=>'RecipeController@Create']);
//        Route::post('recipe/create',['as'=>'admin.recipe.store','uses'=>'RecipeController@Store']);
//        Route::get('recipe/edit/{id}',['as'=>'admin.recipe.edit','uses'=>'RecipeController@Edit']);
//        Route::post('recipe/edit/{id}',['as'=>'admin.recipe.up','uses'=>'RecipeController@Update']);
//        Route::get('recipe/confirm-delete/{id}',['as'=>'admin.recipe.confirm-delete','uses'=>'RecipeController@deleteModal']);
//        Route::get('recipe/delete/{id}',['as'=>'admin.recipe.delete','uses'=>'RecipeController@destroy']);
//        Route::get('recipe/show/{id}',['as'=>'admin.recipe.show','uses'=>'RecipeController@show']);
//        Route::get('recipe/search',['as'=>'admin.recipe.search','uses'=>'RecipeController@Search']);
//        // Recipes-steps
//        Route::get('recipe-step',['as'=>'admin.recipe-step.index','uses'=>'RecipeStepController@Index']);
//        Route::get('recipe-step/create',['as'=>'admin.recipe-step.create','uses'=>'RecipeStepController@Create']);
//        Route::post('recipe-step/create',['as'=>'admin.recipe-step.store','uses'=>'RecipeStepController@Store']);
//        Route::get('recipe-step/edit/{id}',['as'=>'admin.recipe-step.edit','uses'=>'RecipeStepController@Edit']);
//        Route::post('recipe-step/edit/{id}',['as'=>'admin.recipe-step.editup','uses'=>'RecipeStepController@Update']);
//        Route::get('recipe-step/confirm-delete/{id}',['as'=>'admin.recipe-step.confirm-delete','uses'=>'RecipeStepController@deleteModal']);
//        Route::get('recipe-step/delete/{id}',['as'=>'admin.recipe-step.delete','uses'=>'RecipeStepController@destroy']);
//        Route::get('recipe-step/show/{id}',['as'=>'admin.recipe-step.show','uses'=>'RecipeStepController@show']);
//        Route::get('recipe-step/search',['as'=>'admin.recipe-step.search','uses'=>'RecipeStepController@Search']);
//            //recipe-step-ingredient
//        Route::get('recipe-step-ingredient',['as'=>'admin.recipe-step-ingredient.index','uses'=>'RecipeStepIngredientController@Index']);
//        Route::get('recipe-step-ingredient/create',['as'=>'admin.recipe-step-ingredient.create','uses'=>'RecipeStepIngredientController@Create']);
//        Route::post('recipe-step-ingredient/create',['as'=>'admin.recipe-step-ingredient.store','uses'=>'RecipeStepIngredientController@Store']);
//        Route::get('recipe-step-ingredient/edit/{id}',['as'=>'admin.recipe-step-ingredient.edit','uses'=>'RecipeStepIngredientController@Edit']);
//        Route::post('recipe-step-ingredient/edit/{id}',['as'=>'admin.recipe-step-ingredient.up','uses'=>'RecipeStepIngredientController@Update']);
//        Route::get('recipe-step-ingredient/confirm-delete/{id}',['as'=>'admin.recipe-step-ingredient.confirm-delete','uses'=>'RecipeStepIngredientController@deleteModal']);
//        Route::get('recipe-step-ingredient/delete/{id}',['as'=>'admin.recipe-step-ingredient.delete','uses'=>'RecipeStepIngredientController@destroy']);
//        Route::get('recipe-step-ingredient/show/{id}',['as'=>'admin.recipe-step-ingredient.show','uses'=>'RecipeStepIngredientController@show']);
//        Route::get('recipe-step-ingredient/search',['as'=>'admin.recipe-step-ingredient.search','uses'=>'RecipeStepIngredientController@Search']);




        // hotel

        Route::get('hotel/reservation/email/{id}', ['as' => 'admin.hotel.reservation.email', 'uses' => 'Hotel\ReservationController@SendEmail']);

        Route::get('hotel/hotel-index/', ['as' => 'admin.hotel.index', 'uses' => 'Hotel\HotelController@index']);
        Route::get('hotel/hotel-create/', ['as' => 'admin.hotel.create', 'uses' => 'Hotel\HotelController@create']);
        Route::post('hotel/hotel-create/', ['as' => 'admin.hotel.st', 'uses' => 'Hotel\HotelController@store']);
        Route::get('hotel/hotel-edit/{id}', ['as' => 'admin.hotel.edit', 'uses' => 'Hotel\HotelController@edit']);
        Route::post('hotel/hotel-edit/{id}', ['as' => 'admin.hotel.up', 'uses' => 'Hotel\HotelController@update']);
        Route::get('hotel/hotel-confirm-delete/{id}', ['as' => 'admin.hotel.hotel-confirm-delete', 'uses' => 'Hotel\HotelController@getModalDelete']);
        Route::get('hotel/hotel-delete/{id}', ['as' => 'admin.hotel.hotel-delete', 'uses' => 'Hotel\HotelController@destroy']);
        Route::get('hotel/hotel-show/{id}', ['as' => 'admin.hotel.hotel-show', 'uses' => 'Hotel\HotelController@show']);

        Route::get('hotel/room-type', ['as' => 'admin.hotel.room-type', 'uses' => 'Hotel\RoomController@indexRoomType']);
        Route::get('hotel/room-type-create', ['as' => 'admin.hotel.room-type-create', 'uses' => 'Hotel\RoomController@createRoomType']);
        Route::post('hotel/room-type-create', ['as' => 'admin.hotel.room-type-creater', 'uses' => 'Hotel\RoomController@storeRoomType']);
        Route::get('hotel/room-type-edit/{id}', ['as' => 'admin.hotel.room-type-edit', 'uses' => 'Hotel\RoomController@editRoomType']);
        Route::post('hotel/room-type-edit/{id}', ['as' => 'admin.hotel.room-type-editup', 'uses' => 'Hotel\RoomController@updateRoomType']);
        Route::get('hotel/room-type-confirm-delete/{id}', ['as' => 'admin.hotel.room-type-confirm-delete', 'uses' => 'Hotel\RoomController@getModalDeleteRoomType']);
        Route::get('hotel/room-type-delete/{id}', ['as' => 'admin.hotel.room-type-delete', 'uses' => 'Hotel\RoomController@destroyRoomType']);
        Route::get('hotel/room-type-show/{id}', ['as' => 'admin.hotel.room-type-show', 'uses' => 'Hotel\RoomController@showRoomType']);

        //Rooms
        Route::get('hotel/room-index/', ['as' => 'admin.hotel.room-index', 'uses' => 'Hotel\RoomController@indexRoom']);
        Route::get('hotel/room-create/', ['as' => 'admin.hotel.room-create', 'uses' => 'Hotel\RoomController@createRoom']);
        Route::post('hotel/room-create/', ['as' => 'admin.hotel.room-st', 'uses' => 'Hotel\RoomController@storeRoom']);
        Route::get('hotel/room-edit/{id}', ['as' => 'admin.hotel.room-edit', 'uses' => 'Hotel\RoomController@editRoom']);
        Route::post('hotel/room-edit/{id}', ['as' => 'admin.hotel.room-up', 'uses' => 'Hotel\RoomController@updateRoom']);
        Route::get('hotel/room-confirm-delete/{id}', ['as' => 'admin.hotel.room-confirm-delete', 'uses' => 'Hotel\RoomController@getModalDeleteRoom']);
        Route::get('hotel/room-delete1/{id}', ['as' => 'admin.hotel.room-delete', 'uses' => 'Hotel\RoomController@destroyRoom']);

        Route::get('hotel/unsettled-folio/', ['as' => 'admin.hotel.unsettled-folio', 'uses' => 'Hotel\ReservationController@unsettledFolio']);

        Route::get('hotel/unsettled-pos/', ['as' => 'admin.hotel.unsettled-pos', 'uses' => 'Hotel\ReservationController@unsettledPos']);

        Route::get('hotel/reservation/{id}/cancellation', ['as' => 'admin.hotel.reservation.cancelation', 'uses' => 'Hotel\ReservationController@cancelReservation']);
        Route::get('getfoodcategorybyid','OrdersController@getcategorybyid');

        //Rate Plans
        Route::get('hotel/rate-plans', ['as' => 'admin.hotel.rate-plans', 'uses' => 'Hotel\RateplanController@index']);
        Route::get('hotel/rate-create/', ['as' => 'admin.hotel.rate-create', 'uses' => 'Hotel\RateplanController@create']);
        Route::post('hotel/rate-create/', ['as' => 'admin.hotel.rate-st', 'uses' => 'Hotel\RateplanController@store']);
        Route::get('hotel/rate-edit/{id}', ['as' => 'admin.hotel.rate-edit', 'uses' => 'Hotel\RateplanController@edit']);
        Route::post('hotel/rate-edit/{id}', ['as' => 'admin.hotel.rate-update', 'uses' => 'Hotel\RateplanController@update']);
        Route::get('hotel/rate-confirm-delete/{id}', ['as' => 'admin.hotel.rate-confirm-delete', 'uses' => 'Hotel\RateplanController@getModalDelete']);
        Route::get('hotel/room-delete/{id}', ['as' => 'admin.hotel.rate-delete', 'uses' => 'Hotel\RateplanController@destroy']);


        Route::get('hotel/room-status-index/', ['as' => 'admin.hotel.room-status-index', 'uses' => 'Hotel\RoomController@indexRoomStatus']);
        Route::get('hotel/room-status-create/', ['as' => 'admin.hotel.status-create', 'uses' => 'Hotel\RoomController@createRoomStatus']);
        Route::post('hotel/room-status-create/', ['as' => 'admin.hotel.status-st', 'uses' => 'Hotel\RoomController@storeRoomStatus']);
        Route::get('hotel/room-status-edit/{id}', ['as' => 'admin.hotel.status-edit', 'uses' => 'Hotel\RoomController@editRoomStatus']);
        Route::post('hotel/room-status-edit/{id}', ['as' => 'admin.hotel.status-up', 'uses' => 'Hotel\RoomController@updateRoomStatus']);
        Route::get('hotel/room-status-confirm-delete/{id}', ['as' => 'admin.hotel.status-confirm-delete', 'uses' => 'Hotel\RoomController@getModalDeleteRoomStatus']);
        Route::get('hotel/room-status-delete/{id}', ['as' => 'admin.hotel.status-delete', 'uses' => 'Hotel\RoomController@destroyRoomStatus']);


        Route::get('products/{id}/trans/excel/{type}',['as'=>'admin.products.trans.excel','uses'=>'ProductController@transExcel']);


        Route::get('hotel/payment-type-index/', ['as' => 'admin.hotel.payment-type-index', 'uses' => 'Hotel\PaymentController@indexpaymentType']);
        Route::get('hotel/payment-type-create/', ['as' => 'admin.hotel.payment-type-create', 'uses' => 'Hotel\PaymentController@createpaymentType']);
        Route::post('hotel/payment-type-create/', ['as' => 'admin.hotel.payment-type-st', 'uses' => 'Hotel\PaymentController@storepaymentType']);
        Route::get('hotel/payment-type-edit/{id}', ['as' => 'admin.hotel.payment-type-edit', 'uses' => 'Hotel\PaymentController@editpaymentType']);
        Route::post('hotel/payment-type-edit/{id}', ['as' => 'admin.hotel.payment-type-up', 'uses' => 'Hotel\PaymentController@updatepaymentType']);
        Route::get('hotel/payment-type-confirm-delete/{id}', ['as' => 'admin.hotel.payment-type-confirm-delete', 'uses' => 'Hotel\PaymentController@getModalDeletePaymentType']);
        Route::get('hotel/payment-type-delete/{id}', ['as' => 'admin.hotel.payment-type-delete', 'uses' => 'Hotel\PaymentController@destroyPaymentType']);


        Route::get('hotel/guests/', ['as' => 'admin.hotel.guests.index', 'uses' => 'Hotel\GuestController@index']);

        Route::get('hotel/guests/create/', ['as' => 'admin.hotel.guests.create', 'uses' => 'Hotel\GuestController@create']);

        Route::post('hotel/guests/store/', ['as' => 'admin.hotel.guests.store', 'uses' => 'Hotel\GuestController@store']);
        Route::get('hotel/guests/edit/{id}', ['as' => 'admin.hotel.guests.edit', 'uses' => 'Hotel\GuestController@edit']);
        Route::post('hotel/guests/edit/{id}', ['as' => 'admin.hotel.guests.up', 'uses' => 'Hotel\GuestController@update']);
        Route::get('hotel/guests/show/{id}', ['as' => 'admin.hotel.guests.show', 'uses' => 'Hotel\GuestController@show']);
        Route::get('hotel/guests/confirm-delete/{id}', ['as' => 'admin.hotel.guests-confirm-delete', 'uses' => 'Hotel\GuestController@getModalDelete']);
        Route::get('hotel/guests/delete/{id}', ['as' => 'admin.hotel.guests-delete', 'uses' => 'Hotel\GuestController@destroy']);
        Route::get('hotel/guests/ajaxgetGuest', 'Hotel\GuestController@ajaxgetGuest');
        Route::get('hotel/guests/modal', ['as' => 'admin.hotel.modal', 'uses' => 'Hotel\GuestController@openModal']);

        Route::get('hotel/reservation-index/', ['as' => 'admin.hotel.reservation-index', 'uses' => 'Hotel\ReservationController@index']);

        Route::get('hotel/reservation/guesthistory', ['as' => 'admin.hotel.reservation.guesthistory', 'uses' => 'Hotel\ReservationController@guesthistory']);



        Route::get('hotel/reservation/checkdate/{date}', ['as' => 'admin.hotel.reservation-checkdate', 'uses' => 'Hotel\ReservationController@checkRoomForDate']);

        Route::get('hotel/reservation-create/', ['as' => 'admin.hotel.reservation-create', 'uses' => 'Hotel\ReservationController@create']);
        Route::post('hotel/reservation-create/', ['as' => 'admin.hotel.reservation-st', 'uses' => 'Hotel\ReservationController@store']);

        Route::get('hotel/reservation-getroom-plan/{id}', ['as' => 'admin.hotel.reservation.getroom-plan', 'uses' => 'Hotel\ReservationController@getroomPlan']);

        Route::get('hotel/reservation-edit/{id}', ['as' => 'admin.hotel.reservation-edit', 'uses' => 'Hotel\ReservationController@edit']);
        Route::post('hotel/reservation-edit/{id}', ['as' => 'admin.hotel.reservation-up', 'uses' => 'Hotel\ReservationController@update']);
        Route::get('hotel/reservation/confirm-delete/{id}', ['as' => 'admin.hotel.reservation.confirm-delete', 'uses' => 'Hotel\ReservationController@getModalDelete']);

        Route::get('hotel/reservation/delete/{id}', ['as' => 'admin.hotel.reservation.delete', 'uses' => 'Hotel\ReservationController@destroy']);

        Route::get('reservation/checkoutfromeditmodel/{id}', ['as' => 'admin.hotel.checkoutfromeditmodel', 'uses' => 'Hotel\ReservationController@checkoutfromeditModel']);

        Route::get('reservation/checkoutfromedit/{id}', ['as' => 'admin.hotel.checkoutfromedit', 'uses' => 'Hotel\ReservationController@checkoutfromedit']);

        Route::get('reservation/checkouterror/{id}', ['as' => 'admin.hotel.checkouterror', 'uses' => 'Hotel\ReservationController@checkouterror']);

        Route::get('reservation/checkouterror/{id}/modal', ['as' => 'admin.hotel.checkouterror.modal', 'uses' => 'Hotel\ReservationController@checkouterrorModal']);


        Route::get('reservation/earlycheckout/{id}', ['as' => 'admin.hotel.earlycheckout', 'uses' => 'Hotel\ReservationController@earlycheckout']);


        Route::get('hotel/reservation/confirm-delete-file/{id}', ['as' => 'admin.hotel.reservation-confirm-delete-file', 'uses' => 'Hotel\ReservationController@confirmDeleteFile']);
        Route::get('hotel/reservation/delete-file/{id}', ['as' => 'admin.hotel.reservation-delete-file', 'uses' => 'Hotel\ReservationController@DeleteFile']);
        Route::post('hotel/reservation/add-file/{id}', ['as' => 'admin.hotel.reservation-add-file', 'uses' => 'Hotel\ReservationController@AddFile']);

        Route::get('reservation/checkinfromedit', ['as' => 'admin.hotel.checkinfromedit', 'uses' => 'Hotel\ReservationController@checkinfromedit']);


        Route::get('reservation/split_res', ['as' => 'admin.hotel.split_res', 'uses' => 'Hotel\NightAuditController@splitReservation']);

        Route::get('hotel/reservation/generateGRCardPDF/{id}',   ['as' => 'admin.hotel.reservation.generateGRCardPDF',   'uses' => 'Hotel\ReservationController@generateGRCardPDF']);

        Route::get('hotel/reservation/generateGRCardprint/{id}',   ['as' => 'admin.hotel.reservation.generateGRCardprint',   'uses' => 'Hotel\ReservationController@generateGRCardprint']);

        Route::get('hotel/reservation/generateConfirmPDF/{id}',   ['as' => 'admin.hotel.reservation.generateConfirmPDF',   'uses' => 'Hotel\ReservationController@generateConfirmPDF']);


        // calender reservation
        Route::get('hotel/reservation-calender/', ['as' => 'admin.hotel.reservation-calender', 'uses' => 'Hotel\ReservationCalenderController@index']);

        //search reservation

        Route::get('search/reservations', ['as' => 'admin.search.reservations', 'uses' => 'Hotel\ReservationController@search']);

        //search guests
        Route::get('search/guests', ['as' => 'admin.search.hotel.guests', 'uses' => 'Hotel\GuestController@search']);

        //Reservation Reports


        Route::get('hotel/reservation/reports/index', ['as' => 'admin.hotel.reservation.reports.index', 'uses' => 'Hotel\ReservationReportController@index']);

        Route::get('hotel/reservation/reports/guestledgers', ['as' => 'admin.hotel.reservation.reports.guestledgers', 'uses' => 'Hotel\ReservationReportController@GuestLedger']);

        Route::get('hotel/reservation/reports/managerflashreport', ['as' => 'admin.hotel.reservation.reports.managerflashreport', 'uses' => 'Hotel\ReservationReportController@ManagerFlashReport']);

        Route::get('hotel/reservation/reports/roomflashreport', ['as' => 'admin.hotel.reservation.reports.roomflashreport', 'uses' => 'Hotel\ReservationReportController@RoomFlashReport']);

        Route::get('hotel/reservation/reports/revenuesales', ['as' => 'admin.hotel.reservation.reports.revenuereports', 'uses' => 'Hotel\ReservationReportController@RevenueReport']);

        Route::get('hotel/reservation/reports/resturantsales', ['as' => 'admin.hotel.reservation.reports.resturantsales', 'uses' => 'Hotel\ReservationReportController@ResturantSales']);




        //  Transaction Groups

        Route::post('hotel/transaction/groups',  ['as' => 'admin.hotel.transaction.groups.store', 'uses' => 'Hotel\TransactionGroupsController@store']);

        Route::get('hotel/transaction/groups',  ['as' => 'admin.hotel.transaction.groups.index', 'uses' => 'Hotel\TransactionGroupsController@index']);

        Route::get('hotel/transaction/groups/create',  ['as' => 'admin.hotel.transaction.groups.create', 'uses' => 'Hotel\TransactionGroupsController@create']);

        Route::post('hotel/transaction/groups/{id}',  ['as' => 'admin.hotel.transaction.groups.update', 'uses' => 'Hotel\TransactionGroupsController@update']);

        Route::get('hotel/transaction/groups/{id}/edit',  ['as' => 'admin.hotel.transaction.groups.edit', 'uses' => 'Hotel\TransactionGroupsController@edit']);

        Route::get('hotel/transaction/groups/delete-confirm/{id}',  ['as' => 'admin.hotel.transaction.groups.delete-confirm', 'uses' => 'Hotel\TransactionGroupsController@getModalDelete']);

        Route::get('hotel/transaction/groups/delete{id}',  ['as' => 'admin.hotel.transaction.groups.delete', 'uses' => 'Hotel\TransactionGroupsController@destroy']);


        // Transaction Code

        Route::post('hotel/transaction/codes',  ['as' => 'admin.hotel.transaction.codes.store', 'uses' => 'Hotel\TransactionCodesController@store']);

        Route::get('hotel/transaction/codes',  ['as' => 'admin.hotel.transaction.codes.index', 'uses' => 'Hotel\TransactionCodesController@index']);

        Route::get('hotel/transaction/codes/create',  ['as' => 'admin.hotel.transaction.codes.create', 'uses' => 'Hotel\TransactionCodesController@create']);

        Route::post('hotel/transaction/codes/{id}',  ['as' => 'admin.hotel.transaction.codes.update', 'uses' => 'Hotel\TransactionCodesController@update']);

        Route::get('hotel/transaction/codes/{id}/edit',  ['as' => 'admin.hotel.transaction.codes.edit', 'uses' => 'Hotel\TransactionCodesController@edit']);

        Route::get('hotel/transaction/codes/delete-confirm/{id}',  ['as' => 'admin.hotel.transaction.codes.delete-confirm', 'uses' => 'Hotel\TransactionCodesController@getModalDelete']);

        Route::get('hotel/transaction/codes/delete{id}',  ['as' => 'admin.hotel.transaction.codes.delete', 'uses' => 'Hotel\TransactionCodesController@destroy']);


        // Articles


        Route::post('hotel/articles',  ['as' => 'admin.hotel.articles.store', 'uses' => 'Hotel\ArticlesController@store']);

        Route::get('hotel/articles',  ['as' => 'admin.hotel.articles.index', 'uses' => 'Hotel\ArticlesController@index']);

        Route::get('hotel/articles/create',  ['as' => 'admin.hotel.articles.create', 'uses' => 'Hotel\ArticlesController@create']);

        Route::post('hotel/articles/{id}',  ['as' => 'admin.hotel.articles.update', 'uses' => 'Hotel\ArticlesController@update']);

        Route::get('hotel/articles/{id}/edit',  ['as' => 'admin.hotel.articles.edit', 'uses' => 'Hotel\ArticlesController@edit']);

        Route::get('hotel/articles/delete-confirm/{id}',  ['as' => 'admin.hotel.articles.delete-confirm', 'uses' => 'Hotel\ArticlesController@getModalDelete']);

        Route::get('hotel/articles/delete{id}',  ['as' => 'admin.hotel.articles.delete', 'uses' => 'Hotel\ArticlesController@destroy']);

        Route::post('articles/GetArticleDetailAjax/{productId}', ['uses' => 'Hotel\ArticlesController@getArticleDetailAjax', 'as' => 'admin.products.getArticleDetailAjax']);


        // pos floors

        Route::get('hotel/posfloors', ['as' => 'admin.hotel.posfloors.index', 'uses' => 'Hotel\PosFloorController@index']);
        Route::post('hotel/posfloors', ['as' => 'admin.hotel.posfloors.store', 'uses' => 'Hotel\PosFloorController@store']);
        Route::get('hotel/posfloors/create', ['as' => 'admin.hotel.posfloors.create', 'uses' => 'Hotel\PosFloorController@create']);
        Route::post('hotel/posfloors/{id}', ['as' => 'admin.hotel.posfloors.update', 'uses' => 'Hotel\PosFloorController@update']);
        Route::get('hotel/posfloors/{id}/edit', ['as' => 'admin.hotel.posfloors.edit', 'uses' => 'Hotel\PosFloorController@edit']);

        Route::get('hotel/posfloors/delete-confirm/{id}',  ['as' => 'admin.hotel.posfloors.delete-confirm', 'uses' => 'Hotel\PosFloorController@getModalDelete']);

        Route::get('hotel/posfloors/delete{id}',  ['as' => 'admin.hotel.posfloors.delete', 'uses' => 'Hotel\PosFloorController@destroy']);

        // table area
        Route::get('hotel/tablearea', ['as' => 'admin.hotel.tablearea.index', 'uses' => 'Hotel\TableAreaController@index']);
        Route::post('hotel/tablearea', ['as' => 'admin.hotel.tablearea.store', 'uses' => 'Hotel\TableAreaController@store']);
        Route::get('hotel/tablearea/create', ['as' => 'admin.hotel.tablearea.create', 'uses' => 'Hotel\TableAreaController@create']);
        Route::post('hotel/tablearea/{id}', ['as' => 'admin.hotel.tablearea.update', 'uses' => 'Hotel\TableAreaController@update']);
        Route::get('hotel/tablearea/{id}/edit', ['as' => 'admin.hotel.tablearea.edit', 'uses' => 'Hotel\TableAreaController@edit']);

        Route::get('hotel/tablearea/delete-confirm/{id}',  ['as' => 'admin.hotel.tablearea.delete-confirm', 'uses' => 'Hotel\TableAreaController@getModalDelete']);

        Route::get('hotel/tablearea/delete/{id}',  ['as' => 'admin.hotel.tablearea.delete', 'uses' => 'Hotel\TableAreaController@destroy']);


        // tablebookings

        Route::get('hotel/table/bookings', ['as' => 'admin.hotel.table.bookings.index', 'uses' => 'Hotel\TableBookingController@index']);
        Route::post('hotel/table/bookings', ['as' => 'admin.hotel.table.bookings.store', 'uses' => 'Hotel\TableBookingController@store']);
        Route::get('hotel/table/bookings/create', ['as' => 'admin.hotel.table.bookings.create', 'uses' => 'Hotel\TableBookingController@create']);
        Route::get('hotel/table/bookings/{id}', ['as' => 'admin.hotel.table.bookings.show', 'uses' => 'Hotel\TableBookingController@show']);
        Route::post('hotel/table/bookings/{id}', ['as' => 'admin.hotel.table.bookings.update', 'uses' => 'Hotel\TableBookingController@update']);
        Route::get('hotel/table/bookings/{id}/edit', ['as' => 'admin.hotel.table.bookings.edit', 'uses' => 'Hotel\TableBookingController@edit']);
        Route::get('hotel/table/bookings/delete-confirm/{id}',  ['as' => 'admin.hotel.table.bookings.delete-confirm', 'uses' => 'Hotel\TableBookingController@getModalDelete']);
        Route::get('hotel/table/bookings/delete/{id}',  ['as' => 'admin.hotel.table.bookings.delete', 'uses' => 'Hotel\TableBookingController@destroy']);


        Route::post('event/type',  ['as' => 'admin.event.type.store', 'uses' => 'EventTypeController@store']);
        Route::get('event/type',  ['as' => 'admin.event.type.index', 'uses' => 'EventTypeController@index']);
        Route::get('event/type/create',  ['as' => 'admin.event.type.create', 'uses' => 'EventTypeController@create']);
        Route::post('event/type/{id}',  ['as' => 'admin.event.type.update', 'uses' => 'EventTypeController@update']);
        Route::get('event/type/{id}/edit',  ['as' => 'admin.event.type.edit', 'uses' => 'EventTypeController@edit']);
        Route::get('event/type/delete-confirm/{id}',  ['as' => 'admin.event.type.delete-confirm', 'uses' => 'EventTypeController@getModalDelete']);
        Route::get('event/type/delete{id}',  ['as' => 'admin.event.type.delete', 'uses' => 'EventTypeController@destroy']);

        //

        Route::get('reservation/{res_id}/move_room/', ['as' => 'admin.hotel.moveroom', 'uses' => 'Hotel\ReservationController@moveroom']);
        Route::post('reservation/{id}/move_room/', ['as' => 'admin.hotel.moveroomst', 'uses' => 'Hotel\ReservationController@moveroomstore']);

        //checkins
        Route::get('hotel/reservation/checkins', ['as' => 'admin.hotel.reservation-checkins', 'uses' => 'Hotel\ReservationController@checkins']);

        Route::get('hotel/reservation/checkouts', ['as' => 'admin.hotel.reservation-checkot', 'uses' => 'Hotel\ReservationController@checkouts']);

        Route::get('hotel/reservation/room-overviews', ['as' => 'admin.hotel.reservation-overviews', 'uses' => 'Hotel\RoomController@rooms_overview']);

        Route::get('hotel/reservation/arrivals', ['as' => 'admin.hotel.reservation-arrivals', 'uses' => 'Hotel\ReservationController@arrivals']);
        Route::get('hotel/reservation/departures', ['as' => 'admin.hotel.reservation-departures', 'uses' => 'Hotel\ReservationController@departures']);


        Route::get('hotel/reservation-confirm-delete/{id}', ['as' => 'admin.hotel.reservation-confirm-delete', 'uses' => 'Hotel\AgentController@getModalDelete']);
        Route::get('hotel/reservation-delete/{id}', ['as' => 'admin.hotel.reservation-delete', 'uses' => 'Hotel\AgentController@destroy']);

        Route::get('hotel/res-status-index/', ['as' => 'admin.hotel.res-status-index', 'uses' => 'Hotel\ReservationStatusController@index']);
        Route::get('hotel/res-status-create/', ['as' => 'admin.hotel.res-status-create', 'uses' => 'Hotel\ReservationStatusController@create']);
        Route::post('hotel/res-status-create/', ['as' => 'admin.hotel.res-status-st', 'uses' => 'Hotel\ReservationStatusController@store']);
        Route::get('hotel/res-status-edit/{id}', ['as' => 'admin.hotel.res-status-edit', 'uses' => 'Hotel\ReservationStatusController@edit']);
        Route::post('hotel/res-status-edit/{id}', ['as' => 'admin.hotel.res-status-up', 'uses' => 'Hotel\ReservationStatusController@update']);
        Route::get('hotel/res-status-confirm-delete/{id}', ['as' => 'admin.hotel.res-status-confirm-delete', 'uses' => 'Hotel\ReservationStatusController@getModalDelete']);
        Route::get('hotel/res-status-delete/{id}', ['as' => 'admin.hotel.res-status-delete', 'uses' => 'Hotel\ReservationStatusController@destroy']);

        Route::get('hotel/res-type-index/', ['as' => 'admin.hotel.res-type-index', 'uses' => 'Hotel\ReservationTypeController@index']);
        Route::get('hotel/res-type-create/', ['as' => 'admin.hotel.res-type-create', 'uses' => 'Hotel\ReservationTypeController@create']);
        Route::post('hotel/res-type-create/', ['as' => 'admin.hotel.res-type-st', 'uses' => 'Hotel\ReservationTypeController@store']);
        Route::get('hotel/res-type-edit/{id}', ['as' => 'admin.hotel.res-type-edit', 'uses' => 'Hotel\ReservationTypeController@edit']);
        Route::post('hotel/res-type-edit/{id}', ['as' => 'admin.hotel.res-type-up', 'uses' => 'Hotel\ReservationTypeController@update']);
        Route::get('hotel/res-type-confirm-delete/{id}', ['as' => 'admin.hotel.res-type-confirm-delete', 'uses' => 'Hotel\ReservationTypeController@getModalDelete']);
        Route::get('hotel/res-type-delete/{id}', ['as' => 'admin.hotel.res-type-delete', 'uses' => 'Hotel\ReservationTypeController@destroy']);


        Route::get('hotel/extend-stay/{id}', ['as' => 'admin.hotel.extend-stay', 'uses' => 'Hotel\ReservationController@extendstayshow']);

        Route::post('hotel/extend-stay/{id}', ['as' => 'admin.hotel.extend-stayst', 'uses' => 'Hotel\ReservationController@extendstaystore']);

        Route::get('hotel/extend-package/{id}', ['as' => 'admin.hotel.extend-package', 'uses' => 'Hotel\ReservationController@extendpackageshow']);

        Route::post('hotel/extend-package/{id}', ['as' => 'admin.hotel.extend-packagest', 'uses' => 'Hotel\ReservationController@extendpackagestore']);


        Route::get('hotel/extend-guest/{id}', ['as' => 'admin.hotel.extend-guest', 'uses' => 'Hotel\ReservationController@extendguestshow']);

        Route::post('hotel/extend-guest/{id}', ['as' => 'admin.hotel.extend-guestst', 'uses' => 'Hotel\ReservationController@extendgueststore']);


        Route::get('hotel/extend-bed/{id}', ['as' => 'admin.hotel.extend-bed', 'uses' => 'Hotel\ReservationController@extendbedshow']);

        Route::post('hotel/extend-bed/{id}', ['as' => 'admin.hotel.extend-bedst', 'uses' => 'Hotel\ReservationController@extendbedstore']);

        Route::get('hotel/extend-room/{id}', ['as' => 'admin.hotel.extend-room', 'uses' => 'Hotel\ReservationController@extendroomshow']);

        Route::post('hotel/extend-room/{id}', ['as' => 'admin.hotel.extend-roomst', 'uses' => 'Hotel\ReservationController@extendroomstore']);

        Route::get('hotel/extend-room-confirmdel/{id}', ['as' => 'admin.hotel.extend-room-confirmdel', 'uses' => 'Hotel\ReservationController@extendroomconfirmdelete']);

        Route::get('hotel/extend-room-del/{id}', ['as' => 'admin.hotel.extend-room-del', 'uses' => 'Hotel\ReservationController@extendroomdelete']);

        /// add room guest

        Route::get('hotel/room-guest/{res_id}/{room_num}', ['as' => 'admin.hotel.room.guest', 'uses' => 'Hotel\ReservationController@roomguest']);

        Route::post('hotel/room-guest/{res_id}/{room_num}', ['as' => 'admin.hotel.room.guest.store', 'uses' => 'Hotel\ReservationController@storeroomguest']);

        Route::get('hotel/room-guest-confirmdel/{id}', ['as' => 'admin.hotel.room.guest.confirmdel', 'uses' => 'Hotel\ReservationController@roomguestconfirmdelete']);

        Route::get('hotel/room-guest-del/{id}', ['as' => 'admin.hotel.room.guest.del', 'uses' => 'Hotel\ReservationController@roomguestdelete']);

        // guestlist

        Route::get('hotel/guestlist', ['as' => 'admin.hotel.guestlist', 'uses' => 'Hotel\ReservationController@guestlist']);


        Route::get('hotel/reservation/status/{id}', ['as' => 'admin.hotel.reservation.status', 'uses' => 'Hotel\ReservationController@ReservationByStatus']);

        //home links show

        Route::get('hotel/showroomoccupied', ['as' => 'admin.hotel.showroomoccupied', 'uses' => 'Hotel\ActivityDetailsController@showroomoccupied']);


        Route::get('hotel/showdayin', ['as' => 'admin.hotel.showdayin', 'uses' => 'Hotel\ActivityDetailsController@showdayin']);

        Route::get('hotel/showdayout', ['as' => 'admin.hotel.showdayout', 'uses' => 'Hotel\ActivityDetailsController@showdayout']);


        //CASHIERING FUNCTIONS

        // Billing Index
        Route::get('hotel/cashiering/billing', ['as' => 'admin.hotel.cashiering.billing', 'uses' => 'Hotel\CashieringController@guestsin']);

        Route::get('hotel/cashiering/billingdetail/{id}', ['as' => 'admin.hotel.cashiering.billingdetail', 'uses' => 'Hotel\CashieringController@billingdetail']);


        Route::get('hotel/showstayover', ['as' => 'admin.hotel.showstayover', 'uses' => 'Hotel\ActivityDetailsController@showstayover']);
        Route::get('hotel/showarrivalconfirmed', ['as' => 'admin.hotel.showarrivalconfirmed', 'uses' => 'Hotel\ActivityDetailsController@showarrivalconfirmed']);
        Route::get('hotel/showarrivalunconfirmed', ['as' => 'admin.hotel.showarrivalunconfirmed', 'uses' => 'Hotel\ActivityDetailsController@showarrivalunconfirmed']);
        Route::get('hotel/showarrived', ['as' => 'admin.hotel.showarrived', 'uses' => 'Hotel\ActivityDetailsController@showarrived']);
        Route::get('hotel/showwalkin', ['as' => 'admin.hotel.showwalkin', 'uses' => 'Hotel\ActivityDetailsController@showwalkin']);
        Route::get('hotel/due-to-arrival', ['as' => 'admin.hotel.due-to-arrival', 'uses' => 'Hotel\ActivityDetailsController@duearrival']);
        Route::get('hotel/checked-out', ['as' => 'admin.hotel.checked-out', 'uses' => 'Hotel\ActivityDetailsController@chekedOut']);
        Route::get('hotel/day-use', ['as' => 'admin.hotel.day-use', 'uses' => 'Hotel\ActivityDetailsController@dayuse']);
        Route::get('hotel/clean-room', ['as' => 'admin.hotel.clean-room', 'uses' => 'Hotel\ActivityDetailsController@houseclean']);
        Route::get('hotel/dirty-room', ['as' => 'admin.hotel.inspected-room', 'uses' => 'Hotel\ActivityDetailsController@housedirty']);
        Route::get('hotel/inspected-room', ['as' => 'admin.hotel.clean-roomins', 'uses' => 'Hotel\ActivityDetailsController@houseinspected']);
        Route::get('hotel/pickup-room', ['as' => 'admin.hotel.pickup-room', 'uses' => 'Hotel\ActivityDetailsController@housepickup']);

        Route::post('hotel/makevoid/{id}', ['as' => 'admin.reservation.voidfromedit', 'uses' => 'Hotel\ReservationController@voidfromedit']);

        Route::get('hotel/void/confirm/{id}', ['as' => 'admin.reservation.void.confirm', 'uses' => 'Hotel\ReservationController@voidConfirm']);


        // Pos cost center

        Route::get('hotel/pos-cost-center/index', ['as' => 'admin.hotel.pos-cost-center.index', 'uses' => 'Hotel\POSCostCenterController@index']);
        Route::get('hotel/pos-cost-center/create', ['as' => 'admin.hotel.pos-cost-center.create', 'uses' => 'Hotel\POSCostCenterController@create']);
        Route::post('hotel/pos-cost-center', ['as' => 'admin.hotel.pos-cost-center.store', 'uses' => 'Hotel\POSCostCenterController@store']);
        Route::get('hotel/pos-cost-center/{id}/edit', ['as' => 'admin.hotel.pos-cost-center.edit', 'uses' => 'Hotel\POSCostCenterController@edit']);
        Route::post('hotel/pos-cost-center/{id}/update', ['as' => 'admin.hotel.pos-cost-center.update', 'uses' => 'Hotel\POSCostCenterController@update']);
        Route::get('hotel/pos-cost-center/{id}', ['as' => 'admin.hotel.pos-cost-center.confirm-delete', 'uses' => 'Hotel\POSCostCenterController@getModalDelete']);
        Route::get('hotel/pos-cost-center-delete/{id}', ['as' => 'admin.hotel.pos-cost-center.delete', 'uses' => 'Hotel\POSCostCenterController@destroy']);

        // Pos Kitchen

        Route::get('hotel/pos-kitchen/index', ['as' => 'admin.pos-kitchen.index', 'uses' => 'Hotel\POSKitchenController@index']);
        Route::get('hotel/pos-kitchen/create', ['as' => 'admin.pos-kitchen.create', 'uses' => 'Hotel\POSKitchenController@create']);
        Route::post('hotel/pos-kitchen', ['as' => 'admin.hotel.pos-kitchen.store', 'uses' => 'Hotel\POSKitchenController@store']);
        Route::get('hotel/pos-kitchen/{id}/edit', ['as' => 'admin.pos-kitchen.edit', 'uses' => 'Hotel\POSKitchenController@edit']);
        Route::post('hotel/pos-kitchen/{id}/update', ['as' => 'admin.pos-kitchen.update', 'uses' => 'Hotel\POSKitchenController@update']);
        Route::get('hotel/pos-kitchen/{id}', ['as' => 'admin.hotel.pos-kitchen.confirm-delete', 'uses' => 'Hotel\POSKitchenController@getModalDelete']);
        Route::get('hotel/pos-kitchen-delete/{id}', ['as' => 'admin.hotel.pos-kitchen.delete', 'uses' => 'Hotel\POSKitchenController@destroy']);


        // Pos Outlets

        Route::get('hotel/pos-outlets/index', ['as' => 'admin.pos-outlets.index', 'uses' => 'Hotel\PosOutletsController@index']);
        Route::get('hotel/pos-outlets/create', ['as' => 'admin.pos-outlets.create', 'uses' => 'Hotel\PosOutletsController@create']);
        Route::post('hotel/pos-outlets', ['as' => 'admin.hotel.pos-outlets.store', 'uses' => 'Hotel\PosOutletsController@store']);
        Route::get('hotel/pos-outlets/{id}/edit', ['as' => 'admin.pos-outlets.edit', 'uses' => 'Hotel\PosOutletsController@edit']);
        Route::post('hotel/pos-outlets/{id}/update', ['as' => 'admin.pos-outlets.update', 'uses' => 'Hotel\PosOutletsController@update']);
        Route::get('hotel/pos-outlets/{id}', ['as' => 'admin.hotel.pos-outlets.confirm-delete', 'uses' => 'Hotel\PosOutletsController@getModalDelete']);
        Route::get('hotel/pos-outlets-delete/{id}', ['as' => 'admin.hotel.pos-outlets.delete', 'uses' => 'Hotel\PosOutletsController@destroy']);

        /// add user

        Route::get('/hotel/pos-outlets/{id}/adduser', ['as' => 'admin.hotel.pos-outlets.adduser', 'uses' => 'Hotel\PosOutletsController@addUser']);
        Route::post('/hotel/pos-outlets/{id}/adduser', ['as' => 'admin.hotel.pos-outlets.postuser', 'uses' => 'Hotel\PosOutletsController@postUser']);

        Route::get('hotel/pos-outlets/{id}/confirm-delete/adduser', ['as' => 'admin.hotel.pos-outlets.confirm-delete.adduser', 'uses' => 'Hotel\PosOutletsController@getModalDeleteUser']);
        Route::get('hotel/pos-outlet/{id}/delete/adduser', ['as' => 'admin.hotel.pos-outlets.adduser.delete', 'uses' => 'Hotel\PosOutletsController@destroyUser']);

        // Pos Tables

        Route::get('hotel/pos-tables/index', ['as' => 'admin.pos-tables.index', 'uses' => 'Hotel\PosTableController@index']);
        Route::get('hotel/pos-tables/create', ['as' => 'admin.pos-tables.create', 'uses' => 'Hotel\PosTableController@create']);
        Route::post('hotel/pos-tables', ['as' => 'admin.hotel.pos-tables.store', 'uses' => 'Hotel\PosTableController@store']);
        Route::get('hotel/pos-tables/{id}/edit', ['as' => 'admin.pos-tables.edit', 'uses' => 'Hotel\PosTableController@edit']);
        Route::post('hotel/pos-tables/{id}/update', ['as' => 'admin.pos-tables.update', 'uses' => 'Hotel\PosTableController@update']);
        Route::get('hotel/pos-tables/{id}', ['as' => 'admin.hotel.pos-tables.confirm-delete', 'uses' => 'Hotel\PosTableController@getModalDelete']);
        Route::get('hotel/pos-tables-delete/{id}', ['as' => 'admin.hotel.pos-tables.delete', 'uses' => 'Hotel\PosTableController@destroy']);


        // Pos Sessions

        Route::get('hotel/pos-sessions/index', ['as' => 'admin.pos-sessions.index', 'uses' => 'Hotel\POSSessionController@index']);
        Route::get('hotel/pos-sessions/create', ['as' => 'admin.pos-sessions.create', 'uses' => 'Hotel\POSSessionController@create']);
        Route::post('hotel/pos-sessions', ['as' => 'admin.hotel.pos-sessions.store', 'uses' => 'Hotel\POSSessionController@store']);
        Route::get('hotel/pos-sessions/{id}/edit', ['as' => 'admin.pos-sessions.edit', 'uses' => 'Hotel\POSSessionController@edit']);
        Route::post('hotel/pos-sessions/{id}/update', ['as' => 'admin.pos-sessions.update', 'uses' => 'Hotel\POSSessionController@update']);
        Route::get('hotel/pos-sessions/{id}', ['as' => 'admin.hotel.pos-sessions.confirm-delete',   'uses' => 'Hotel\POSSessionController@getModalDelete']);
        Route::get('hotel/pos-sessions-delete/{id}', ['as' => 'admin.hotel.pos-sessions.delete', 'uses' => 'Hotel\POSSessionController@destroy']);

        // open outlets

        Route::get('hotel/openoutlets', ['as' => 'admin.hotel.pos-outlets.openoutlets', 'uses' => 'Hotel\PosOutletsController@openoutlets']);

        Route::get('hotel/openresturantoutlets', ['as' => 'admin.hotel.resturant-outlets.openresturantoutlets', 'uses' => 'Hotel\PosOutletsController@resturantoutlets']);

        Route::get('hotel/showtablelists', ['as' => 'admin.hotel.showtablelists', 'uses' => 'Hotel\PosOutletsController@showtablelists']);






        Route::get('hotel/outletsales', ['as' => 'admin.hotel.pos-outlets.outletsales', 'uses' => 'Hotel\PosOutletsController@outletsales']);
        Route::get('pos/dashboard', ['as' => 'admin.hotel.pos.dashboard', 'uses' => 'Hotel\PosOutletsController@posDashboard']);


        Route::get('hotel/roomtransfer/item',['as'=>'admin.hotel.roomtransfer.item','uses'=>'OrdersController@roomtransferItem']);


        Route::get('hotel/deposit/list',['as'=>'admin.hotel.deposit.list','uses'=>'Hotel\PaymentFolioController@depositList']);


        Route::post('hotel/pos-openoutlets/outlettake', ['as' => 'admin.hotel.pos-outlets.takeopenoutlets', 'uses' => 'Hotel\PosOutletsController@takeopenoutlets']);
        Route::get('roomguest/pos/{id}', ['as' => 'admin.roomguest.pos.list', 'uses' => 'OrdersController@roomResvGuestPosBills']);


        // pos menu

        Route::get('hotel/pos-menu/index', ['as' => 'admin.pos-menu.index', 'uses' => 'Hotel\POSMenuController@index']);
        Route::get('hotel/pos-menu/create', ['as' => 'admin.pos-menu.create', 'uses' => 'Hotel\POSMenuController@create']);
        Route::post('hotel/pos-menu', ['as' => 'admin.hotel.pos-menu.store', 'uses' => 'Hotel\POSMenuController@store']);
        Route::get('hotel/pos-menu/{id}/edit', ['as' => 'admin.pos-menu.edit', 'uses' => 'Hotel\POSMenuController@edit']);
        Route::post('hotel/pos-menu/{id}/update', ['as' => 'admin.pos-menu.update', 'uses' => 'Hotel\POSMenuController@update']);
        Route::get('hotel/pos-menu/{id}', ['as' => 'admin.hotel.pos-menu.confirm-delete',   'uses' => 'Hotel\POSMenuController@getModalDelete']);
        Route::get('hotel/pos-menu-delete/{id}', ['as' => 'admin.hotel.pos-menu.delete', 'uses' => 'Hotel\POSMenuController@destroy']);


        //room block and night audit
        Route::get('hotel/house-keep-index/', ['as' => 'admin.hotel.house-keep-index', 'uses' => 'Hotel\HouseKeepingController@index']);

        Route::get('hotel/house-keep-create/', ['as' => 'admin.hotel.house-keep-create', 'uses' => 'Hotel\HouseKeepingController@create']);

        Route::post('hotel/house-keep-create/', ['as' => 'admin.hotel.house-keep-st', 'uses' => 'Hotel\HouseKeepingController@store']);


        // change house status after checkout

        Route::post('hotel/reservation/checkout/{id}/house-status', ['as' => 'admin.hotel.reservation.checkout.house-status', 'uses' => 'Hotel\HouseKeepingController@ChangeHouseStatus']);




        Route::get('hotel/room-block-index/', ['as' => 'admin.hotel.room-block', 'uses' => 'Hotel\RoomBlockController@index']);
        Route::get('hotel/room-block-create/', ['as' => 'admin.hotel.room-block-create', 'uses' => 'Hotel\RoomBlockController@create']);
        Route::post('hotel/room-block-create/', ['as' => 'admin.hotel.room-block-st', 'uses' => 'Hotel\RoomBlockController@store']);
        Route::get('hotel/room-block-edit/{id}', ['as' => 'admin.hotel.room-block-edit', 'uses' => 'Hotel\RoomBlockController@edit']);
        Route::post('hotel/room-block-store/{id}', ['as' => 'admin.hotel.room-block-store', 'uses' => 'Hotel\RoomBlockController@update']);
        Route::get('hotel/room-block-confirmdelete/{id}', ['as' => 'admin.hotel.room-block-confirmdelete', 'uses' => 'Hotel\RoomBlockController@confirmDelete']);
        Route::get('hotel/room-block-delete/{id}', ['as' => 'admin.hotel.room-block-delete', 'uses' => 'Hotel\RoomBlockController@destroy']);

        Route::get('hotel/room-block-confirmunblock/{id}', ['as' => 'admin.hotel.room-block-confirmunblock', 'uses' => 'Hotel\HouseKeepingController@confirmDelete']);
        Route::get('hotel/room-block-unblock/{id}', ['as' => 'admin.hotel.room-block-unblock', 'uses' => 'Hotel\HouseKeepingController@destroy']);
        Route::get('hotel/room-block-selected/{id}', ['as' => 'admin.hotel.room-block-selected', 'uses' => 'Hotel\HouseKeepingController@blockroom']);

        Route::get('hotel/house-keeping-edit/{id}', ['as' => 'admin.hotel.house-keeping-edit', 'uses' => 'Hotel\HouseKeepingController@edit']);
        Route::post('hotel/house-keeping-edit/{id}', ['as' => 'admin.hotel.house-keeping-up', 'uses' => 'Hotel\HouseKeepingController@update']);


        Route::get('hotel/pending-index', ['as' => 'admin.hotel.pending-index', 'uses' => 'Hotel\NightAuditController@pendingres']);

        Route::post('hotel/nightaudit-pending/', ['as' => 'admin.hotel.nightaudit-pending/', 'uses' => 'Hotel\NightAuditController@pendingresupdate']);

        Route::get('/hotel/release-index', ['as' => 'admin.hotel.release-index', 'uses' => 'Hotel\NightAuditController@releaseres']);
        Route::post('/hotel/nightaudit-release', ['as' => 'admin.hotel.nightaudit-release', 'uses' => 'Hotel\NightAuditController@releaseresupdate']);


        Route::get('/hotel/today_unsettled_folio',['as'=>'admin.hotel.today_unsettled_folio','uses'=>"Hotel\NightAuditController@todayUnsettledFolio"]);

        Route::post('/hotel/today_unsettled_folio',['as'=>'admin.hotel.today_unsettled_foliopo','uses'=>"Hotel\NightAuditController@todayUnsettledFolioPost"]);


        Route::get('/hotel/nightaudit-roomstatus', ['as' => 'admin.hotel.nightaudit-roomstatus', 'uses' => 'Hotel\NightAuditController@roomStatus']);

        Route::get('/hotel/nightaudit-unsettledfolios', ['as' => 'admin.hotel.nightaudit-unsettledfolios', 'uses' => 'Hotel\NightAuditController@unsettledFolios']);

        Route::get('/hotel/nightaudit-unsettledposbills', ['as' => 'admin.hotel.nightaudit-unsettledfoliospo', 'uses' => 'Hotel\NightAuditController@unsettledPOSbills']);


        Route::get('/hotel/settle/folio/{id}', ['as' => 'admin.hotel.settle.folio', 'uses' => 'Hotel\NightAuditController@settleFolio']);
        Route::get('/hotel/settle/pos/{id}', ['as' => 'admin.hotel.settle.pos', 'uses' => 'Hotel\NightAuditController@settlePOS']);



        Route::get('/hotel/nepalirdsync/posModal/{id}', ['as' => 'admin.nepal.ird.pos.sync.modal', 'uses' => 'Hotel\NightAuditController@nepalIrdPOSSyncModal']);

        Route::get('/hotel/nepalirdsync/pos/{id}', ['as' => 'admin.nepal.ird.pos.sync', 'uses' => 'Hotel\NightAuditController@nepalIrdPOSSync']);


        Route::get('/hotel/nepalirdsync/posBillReturnModal/{id}', ['as' => 'admin.nepal.irdsync.posBillReturnModal.modal', 'uses' => 'Hotel\NightAuditController@nepalIrdPOSSyncBillReturnModal']);

        Route::get('/hotel/nepalirdsync/posBillReturn/{id}', ['as' => 'admin.nepal.ird.posBillReturn.sync', 'uses' => 'Hotel\NightAuditController@nepalIrdPOSSyncBillReturn']);







        Route::post('/hotel/settle/postfolio/{id}', ['as' => 'admin.hotel.settle.postfolio', 'uses' => 'Hotel\NightAuditController@settlePostFolio']);
        Route::post('/hotel/settle/postpos/{id}', ['as' => 'admin.hotel.settle.postpos', 'uses' => 'Hotel\NightAuditController@settlePostPOS']);


        Route::get('/hotel/create/folio/tomorrow', ['as' => 'admin.hotel.create.folio.tomorrow', 'uses' => 'Hotel\NightAuditController@FolioTomorrow']);



        // night audit post outlet  bills

        Route::post('/hotel/postbills/outlets/today', ['as' => 'admin.hotel.postbills.outlets.today', 'uses' => 'Hotel\NightAuditController@OutletPostBillsToday']);

        Route::post('/hotel/postbills/outlets/{id}', ['as' => 'admin.hotel.outlets.postbills', 'uses' => 'Hotel\NightAuditController@OutletPostBills']);


        // night audit post folio bills

        Route::post('/hotel/postbills/folio/today', ['as' => 'admin.hotel.postbills.folio.today', 'uses' => 'Hotel\NightAuditController@FolioPostBillsToday']);

        Route::post('/hotel/postbills/folio/{id}', ['as' => 'admin.hotel.outlets.folio', 'uses' => 'Hotel\NightAuditController@FolioPostBills']);


        //end







        Route::get('production/product-unit-index', ['as' => 'admin.production.product-unit-index', 'uses' => 'ProductUnitController@productUnitIndex']);
        Route::get('production/product-unit', ['as' => 'admin.production.product-unit', 'uses' => 'ProductUnitController@productUnit']);
        Route::post('production/product-unit', ['as' => 'admin.production.products-unit', 'uses' => 'ProductUnitController@storeProductUnit']);
        Route::get('production/edit-produnit/{id}', ['as' => 'admin.production.edit-produnit', 'uses' => 'ProductUnitController@editproductUnit']);
        Route::post('production/edit-produnit/{id}', ['as' => 'admin.production.edit-produnitst', 'uses' => 'ProductUnitController@updateprodUnit']);
        Route::get('production/confirm-delete-produnit/{id}', ['as' => 'admin.production.confirm-delete-produnit', 'uses' => 'ProductUnitController@deleteproductUnitModal']);
        Route::get('production/delete-produnit/{id}', ['as' => 'admin.productunit.delete-produnit', 'uses' => 'ProductUnitController@deleteproductUnit']);

        Route::get('product-location/index', ['as' => 'admin.product-location.index', 'uses' => 'ProductsLocationController@index']);
        Route::get('product-location/create', ['as' => 'admin.product-location.create', 'uses' => 'ProductsLocationController@create']);
        Route::post('product-location/create', ['as' => 'admin.product-location.st', 'uses' => 'ProductsLocationController@store']);
        Route::get('product-location/show/{id}', ['as' => 'admin.product-location.show', 'uses' => 'ProductsLocationController@show']);
        Route::get('product-location/edit/{id}', ['as' => 'admin.product-location.edit', 'uses' => 'ProductsLocationController@edit']);
        Route::post('product-location/edit/{id}', ['as' => 'admin.product-location.up', 'uses' => 'ProductsLocationController@update']);
        Route::get('product-location/delete-confirm/{id}', ['as' => 'admin.product-location.delete-confirm', 'uses' => 'ProductsLocationController@deleteModal']);
        Route::get('product-location/delete/{id}', ['as' => 'admin.product-location.delete', 'uses' => 'ProductsLocationController@destroy']);

        //performace route
        Route::get('performance/indicator', ['as' => 'admin.performance.indicator', 'uses' => 'PerformanceController@index']);
        Route::get('performance/create-performance-indicator', ['as' => 'admin.performance.create-performance-indicator', 'uses' => 'PerformanceController@createIndicator']);
        Route::post('performance/performance-indicator', ['as' => 'admin.performance.performance-indicator', 'uses' => 'PerformanceController@store']);
        Route::get('performance/show-performance-indicator/{id}', ['as' => 'admin.performance.show-performance-indicator', 'uses' => 'PerformanceController@showIndicator']);
        Route::get('performance/edit-performance-indicator/{id}', ['as' => 'admin.performance.edit-performance-indicatorst', 'uses' => 'PerformanceController@editIndicator']);
        Route::post('performance/edit-performance-indicator/{id}', ['as' => 'admin.performance.edit-performance-indicator', 'uses' => 'PerformanceController@updateIndicator']);
        Route::get('performance/confirm-delete-indicator/{id}', ['as' => 'admin.performance.confirm-delete-indicator', 'uses' => 'PerformanceController@getIndicatorDelete']);
        Route::get('performance/delete-indicator/{id}', ['as' => 'admin.performance.delete-indicator', 'uses' => 'PerformanceController@deleteIndicator']);

        Route::get('performance/appraisal', ['as' => 'admin.performance.appraisal', 'uses' => 'PerformanceController@appraisalIndex']);
        Route::get('performance/giveappraisal', ['as' => 'admin.performance.giveappraisal', 'uses' => 'PerformanceController@userAppraisal']);
        Route::post('performance/create-appeaisal', ['as' => 'admin.performance.create-appeaisal', 'uses' => 'PerformanceController@userAppraisalCreate']);
        Route::get('performance/show-appeaisal/{id}', ['as' => 'admin.performance.show-appeaisal', 'uses' => 'PerformanceController@showAppraisal']);
        Route::get('performance/edit-appeaisal/{id}', ['as' => 'admin.performance.edit-appeaisal', 'uses' => 'PerformanceController@editAppraisal']);
        Route::post('performance/edit-appeaisal/{id}', ['as' => 'admin.performance.edit-appeaisalup', 'uses' => 'PerformanceController@updateAppeasial']);
        Route::get('performance/confirm-delete-appeaisal/{id}', ['as' => 'admin.performance.confirm-delete-appeaisal', 'uses' => 'PerformanceController@getappeaisalDelete']);
        Route::get('performance/delete-appeaisal/{id}', ['as' => 'admin.performance.delete-appeaisal', 'uses' => 'PerformanceController@deleteAppeaisal']);

        Route::get('performance/report', ['as' => 'admin.performance.report', 'uses' => 'PerformanceController@ReportIndex']);
        Route::get('performance/report', ['as' => 'admin.performance.reportind', 'uses' => 'PerformanceController@ReportIndex']);
        Route::get('performance/report1/{id}', ['as' => 'admin.performance.reportpt', 'uses' => 'PerformanceController@Report']);
        //performance route ends here

        //event controller

        Route::get('events', 'EventController@index');
        Route::get('addevent', ['as' => 'addevent', 'uses' => 'EventController@create']);
        Route::post('addevent', ['as' => 'addeventst', 'uses' => 'EventController@store']);
        Route::get('editevent/{id}', ['as' => 'editevent', 'uses' => 'EventController@edit']);
        Route::post('editevent/{id}', ['as' => 'editeventup', 'uses' => 'EventController@update']);
        Route::get('confirm-delete/{id}', ['as' => 'confirm-delete', 'uses' => 'EventController@getModalDelete']);
        Route::get('delete-event/{id}', ['as' => 'delete-event', 'uses' => 'EventController@destroy']);
        Route::get('event-venues', 'EventController@showVenue');
        Route::get('add-venue', ['as' => 'add-venue', 'uses' => 'EventController@createVenues']);
        Route::post('add-venue', ['as' => 'add-venue-st', 'uses' => 'EventController@storeVenues']);
        Route::get('edit-venue/{id}', ['as' => 'edit-venue', 'uses' => 'EventController@editVenues']);
        Route::post('edit-venue/{id}', ['as' => 'edit-venueup', 'uses' => 'EventController@updateVenues']);
        Route::get('confirm-delete-venue/{id}', ['as' => 'confirm-delete-venue', 'uses' => 'EventController@getVenueDelete']);
        Route::get('delete-venue/{id}', ['as' => 'delete-venue', 'uses' => 'EventController@destroyVenue']);
        Route::get('event-space', 'EventController@showSpace');
        Route::get('add-event-space', ['as' => 'add-event-space', 'uses' => 'EventController@createSpace']);
        Route::post('add-event-space', ['as' => 'add-event-space-st', 'uses' => 'EventController@storeSpace']);
        Route::get('edit-event-space/{id}', ['as' => 'edit-event-space', 'uses' => 'EventController@editSpace']);
        Route::post('edit-event-space/{id}', ['as' => 'edit-event-spacest', 'uses' => 'EventController@updateSpace']);
        Route::get('confirm-delete-space/{id}', ['as' => 'confirm-delete-space', 'uses' => 'EventController@getspaceDelete']);
        Route::get('delete-space/{id}', ['as' => 'delete-space', 'uses' => 'EventController@destroySpace']);

        // For Activity Time Sheet

        Route::get('activity', ['as' => 'admin.activity.index', 'uses' => 'TimeSheetController@activityIndex']);
        Route::get('activity/create', ['as' => 'admin.activity.create', 'uses' => 'TimeSheetController@activityCreate']);
        Route::post('activity', ['as' => 'admin.activity.save', 'uses' => 'TimeSheetController@activitySave']);
        Route::get('activity/{id}/edit', ['as' => 'admin.activity.edit', 'uses' => 'TimeSheetController@activityEdit']);
        Route::post('activity/{id}', ['as' => 'admin.activity.update', 'uses' => 'TimeSheetController@activityUpdate']);
        Route::get('activity/{id}/confirm-delete', ['as' => 'admin.activity.confirm-delete',   'uses' => 'TimeSheetController@getModalDeleteActivity']);
        Route::get('activity/{id}/delete', ['as' => 'admin.activity.delete',           'uses' => 'TimeSheetController@destroyActivity']);

        // For Time Sheet

        Route::get('timesheet', ['as' => 'admin.timesheet.index', 'uses' => 'TimeSheetController@timesheetIndex']);
        Route::get('timesheet/create', ['as' => 'admin.timesheet.create', 'uses' => 'TimeSheetController@timesheetCreate']);
        Route::post('timesheet', ['as' => 'admin.timesheet.save', 'uses' => 'TimeSheetController@timesheetSave']);
        Route::get('timesheet/{id}/edit', ['as' => 'admin.timesheet.edit', 'uses' => 'TimeSheetController@timesheetEdit']);
        Route::post('timesheet/{id}', ['as' => 'admin.timesheet.update', 'uses' => 'TimeSheetController@timesheetUpdate']);
        Route::get('timesheet/{id}/confirm-delete', ['as' => 'admin.timesheet.confirm-delete',   'uses' => 'TimeSheetController@getModalDeleteTimeSheet']);
        Route::get('timesheet/{id}/delete', ['as' => 'admin.timesheet.delete',           'uses' => 'TimeSheetController@destroyTimeSheet']);

        Route::get('bulkadd/timesheet', ['as' => 'admin.bulkadd.timesheet', 'uses' => 'TimeSheetController@bulkindex']);
        Route::post('bulkadd/timesheet/create', ['as' => 'admin.bulkadd.timesheet.create', 'uses' => 'TimeSheetController@bulkcreate']);
        Route::post('bulkadd/timesheet/save', ['as' => 'admin.bulkadd.timesheet.save', 'uses' => 'TimeSheetController@bulkstore']);

        Route::get('timesheet/{emp_id}/show', ['as' => 'admin.timesheet.show', 'uses' => 'TimeSheetController@timesheetShow']);

        Route::get('timesheet/attendancereport/view', ['as' => 'admin.timesheet.attendancereport', 'uses' => 'TimeSheetController@attendanceReport']);

        Route::post('timesheet/attendancereport/view', ['as' => 'admin.timesheet.attendancereportdet', 'uses' => 'TimeSheetController@attendanceReportDetails']);

        //timesheetsalary
        Route::get('timesheetsalary', ['as' => 'admin.timesheetsalary.index', 'uses' => 'TimeSheetSalaryController@index']);

        Route::get('timesheetsalary/create', ['as' => 'admin.timesheetsalary.create', 'uses' => 'TimeSheetSalaryController@create']);
        Route::post('timesheetsalary/create', ['as' => 'admin.timesheetsalary.store', 'uses' => 'TimeSheetSalaryController@store']);
        Route::get('timesheetsalary/enable/{id}', ['as' => 'admin.timesheetsalary.enable', 'uses' => 'TimeSheetSalaryController@enabledisable']);
        Route::get('timesheetsalary/edit/{id}', ['as' => 'admin.timesheetsalary.edit', 'uses' => 'TimeSheetSalaryController@edit']);
        Route::post('timesheetsalary/update/{id}', ['as' => 'admin.timesheetsalary.update', 'uses' => 'TimeSheetSalaryController@update']);

        Route::get('timesheetsalary/delete/{id}', ['as' => 'admin.timesheetsalary.dext', 'uses' => 'TimeSheetSalaryController@destroy']);
        Route::get('assign_timesheet_salary/', ['as' => 'admin.assign_timesheet_salary.show', 'uses' => 'TimeSheetSalaryController@managesalary']);
        Route::post('assign_timesheet_salary/', ['as' => 'admin.assign_timesheet_salary.det', 'uses' => 'TimeSheetSalaryController@managesalary_details']);
        Route::post('assign_timesheet_salary/store', ['as' => 'admin.assign_timesheet_salary.store', 'uses' => 'TimeSheetSalaryController@managesalary_details_post']);

        // For Cost Center

        Route::get('costcenter', ['as' => 'admin.costcenter.index', 'uses' => 'TimeSheetController@costcenterIndex']);
        Route::get('costcenter/create', ['as' => 'admin.costcenter.create', 'uses' => 'TimeSheetController@costcenterCreate']);
        Route::post('costcenter', ['as' => 'admin.costcenter.save', 'uses' => 'TimeSheetController@costcenterSave']);
        Route::get('costcenter/{id}/edit', ['as' => 'admin.costcenter.edit', 'uses' => 'TimeSheetController@costcenterEdit']);
        Route::post('costcenter/{id}', ['as' => 'admin.costcenter.update', 'uses' => 'TimeSheetController@costcenterUpdate']);
        Route::get('costcenter/{id}/confirm-delete', ['as' => 'admin.costcenter.confirm-delete',   'uses' => 'TimeSheetController@getModalDeleteCostCenter']);
        Route::get('costcenter/{id}/delete', ['as' => 'admin.costcenter.delete',           'uses' => 'TimeSheetController@destroyCostCenter']);

        //BiometricController
        Route::get('biometric', 'BiometricController@index');
        Route::get('biometricmachine', 'BiometricController@machineList');
        Route::get('/biometricattendence', 'BiometricController@showAttendence');
        Route::get('/ImportEmployee', ['as' => 'ImportEmployee', 'uses' => 'BiometricController@ImportEmployee']);
        Route::get('/importdevice', ['as' => 'importdevice', 'uses' => 'BiometricController@ImportDevice']);
        Route::post('/ImportDevice1', ['as' => 'ImportDevice1', 'uses' => 'BiometricController@ImportDevice1']);
        Route::get('/ImportAttendence', ['as' => 'ImportAttendence', 'uses' => 'BiometricController@ImportAttendence']);
        Route::get('/ActivateDevice', ['as' => 'ActivateDevice', 'uses' => 'BiometricController@ActivateDevice']);
        Route::get('/showAllAttendence', ['as' => 'showAllAttendence', 'uses' => 'BiometricController@showAllAttendence']);
        Route::get('/biometrictimehistory', ['as' => 'biometrictimehistory', 'uses' => 'BiometricController@TimeHistory']);
        Route::get('attendancereport', ['as' => 'attendancereportind', 'uses' => 'BiometricController@AttendanceReportIndex']);

        Route::post('attendancereport', ['as' => 'attendancereport', 'uses' => 'BiometricController@AttendanceReport']);
        //

        //salesboard
        Route::get('salesboard', ['as' => 'sls-dashboard', 'uses' => 'SalesBoardController@index']);
        Route::get('product-sales-report/show', ['as' => 'admin.product.sales.report.show', 'uses' => 'SalesBoardController@show']);
        Route::get('reports/products-purchase', ['as' => 'admin.product.purchaseReport', 'uses' => 'purchaseController@productPurchaseReport']);

        // User routes
        Route::post('users/enableSelected', ['as' => 'admin.users.enable-selected',  'uses' => 'UsersController@enableSelected']);
        Route::post('users/disableSelected', ['as' => 'admin.users.disable-selected', 'uses' => 'UsersController@disableSelected']);
        Route::get('users/search', ['as' => 'admin.users.search',           'uses' => 'UsersController@searchByName']);
        Route::get('users/list', ['as' => 'admin.users.list',             'uses' => 'UsersController@listByPage']);
        Route::post('users/getInfo', ['as' => 'admin.users.get-info',         'uses' => 'UsersController@getInfo']);
        Route::post('users', ['as' => 'admin.users.store',            'uses' => 'UsersController@store']);
        Route::get('users', ['as' => 'admin.users.index',            'uses' => 'UsersController@index']);
        Route::get('users/create', ['as' => 'admin.users.create',           'uses' => 'UsersController@create']);
        Route::get('users/{userId}', ['as' => 'admin.users.show',             'uses' => 'UsersController@show']);
        Route::patch('users/{userId}', ['as' => 'admin.users.patch',            'uses' => 'UsersController@update']);
        Route::put('users/{userId}', ['as' => 'admin.users.update',           'uses' => 'UsersController@update']);
        Route::delete('users/{userId}', ['as' => 'admin.users.destroy',          'uses' => 'UsersController@destroy']);
        Route::get('users/{userId}/edit', ['as' => 'admin.users.edit',             'uses' => 'UsersController@edit']);
        Route::get('users/{userId}/confirm-delete', ['as' => 'admin.users.confirm-delete',   'uses' => 'UsersController@getModalDelete']);
        Route::get('users/{userId}/delete', ['as' => 'admin.users.delete',           'uses' => 'UsersController@destroy']);
        Route::get('users/{userId}/enable', ['as' => 'admin.users.enable',           'uses' => 'UsersController@enable']);
        Route::get('users/{userId}/disable', ['as' => 'admin.users.disable',          'uses' => 'UsersController@disable']);
        Route::get('users/{userId}/replayEdit', ['as' => 'admin.users.replay-edit',      'uses' => 'UsersController@replayEdit']);

        Route::get('users/ajax/GetDesignation', ['as' => 'admin.users.ajaxGetDesignation',      'uses' => 'UsersController@ajaxGetDesignation']);

        Route::post('users/ajax_user_update', ['as' => 'admin.users.ajax_user_update',      'uses' => 'UsersController@ajaxUserUpdate']);

        Route::get('usersbydep/{depid}', 'UsersController@getuserByDep');
        //user import export
        Route::get('downloadExcelusers', ['as' => 'admin.users.importexport.index', 'uses' => 'ExcelController@userindex']);
        Route::post('downloadExcelusers', ['as' => 'admin.users.importexport.store', 'uses' => 'ExcelController@importusers']);
        Route::get('downloadExcelusers/{type}', ['as' => 'admin.users.importexport.export', 'uses' => 'ExcelController@exportusers']);

        //User Directory
        Route::get('employee/directory', ['as' => 'admin.users.user-directory',            'uses' => 'UsersController@directory']);

        // User Details Routes

        Route::get('users/{id}/detail', ['as' => 'admin.users.detail.create',      'uses' => 'UserDetailController@create']);
        Route::post('users/{id}/detail/store', ['as' => 'admin.user.detail.store',      'uses' => 'UserDetailController@store']);
        Route::get('users/{id}/detail/{detail_id}/edit', ['as' => 'admin.user.detail.edit',      'uses' => 'UserDetailController@edit']);

        Route::post('users/{id}/detail/{detail_id}/update', ['as' => 'admin.user.detail.update',      'uses' => 'UserDetailController@update']);

        Route::post('usersdocument/{id}/detail/{detail_id}/update', ['as' => 'admin.user.document.update',      'uses' => 'UserDetailController@userdocument']);

        Route::get('userdocument/confirm-delete-file/{id}', ['as' => 'admin.userdocument.confirm-delete-file', 'uses' => 'UserDetailController@confirmDeleteFile']);

        Route::get('userdocument/delete-file/{id}', ['as' => 'admin.userdocument.delete-file', 'uses' => 'UserDetailController@DeleteFile']);

        Route::get('userdetail/{id}/pdf', ['as' => 'admin.userdetail.pdf', 'uses' => 'UserDetailController@pdf']);

        // Role routes
        Route::post('roles/enableSelected', ['as' => 'admin.roles.enable-selected',  'uses' => 'RolesController@enableSelected']);
        Route::post('roles/disableSelected', ['as' => 'admin.roles.disable-selected', 'uses' => 'RolesController@disableSelected']);
        Route::get('roles/search', ['as' => 'admin.roles.search',           'uses' => 'RolesController@searchByName']);
        Route::post('roles/getInfo', ['as' => 'admin.roles.get-info',         'uses' => 'RolesController@getInfo']);
        Route::post('roles', ['as' => 'admin.roles.store',            'uses' => 'RolesController@store']);
        Route::get('roles', ['as' => 'admin.roles.index',            'uses' => 'RolesController@index']);
        Route::get('roles/create', ['as' => 'admin.roles.create',           'uses' => 'RolesController@create']);
        Route::get('roles/{roleId}', ['as' => 'admin.roles.show',             'uses' => 'RolesController@show']);
        Route::patch('roles/{roleId}', ['as' => 'admin.roles.patch',            'uses' => 'RolesController@update']);
        Route::put('roles/{roleId}', ['as' => 'admin.roles.update',           'uses' => 'RolesController@update']);
        Route::delete('roles/{roleId}', ['as' => 'admin.roles.destroy',          'uses' => 'RolesController@destroy']);
        Route::get('roles/{roleId}/edit', ['as' => 'admin.roles.edit',             'uses' => 'RolesController@edit']);
        Route::get('roles/{roleId}/confirm-delete', ['as' => 'admin.roles.confirm-delete',   'uses' => 'RolesController@getModalDelete']);
        Route::get('roles/{roleId}/delete', ['as' => 'admin.roles.delete',           'uses' => 'RolesController@destroy']);
        Route::get('roles/{roleId}/enable', ['as' => 'admin.roles.enable',           'uses' => 'RolesController@enable']);
        Route::get('roles/{roleId}/disable', ['as' => 'admin.roles.disable',          'uses' => 'RolesController@disable']);
        // Menu routes
        Route::post('menus', ['as' => 'admin.menus.save',             'uses' => 'MenusController@save']);
        Route::get('menus', ['as' => 'admin.menus.index',            'uses' => 'MenusController@index']);
        Route::get('menus/getData/{menuId}', ['as' => 'admin.menus.get-data',         'uses' => 'MenusController@getData']);
        Route::get('menus/{menuId}/confirm-delete', ['as' => 'admin.menus.confirm-delete',   'uses' => 'MenusController@getModalDelete']);
        Route::get('menus/{id}/delete', ['as' => 'admin.menus.delete',           'uses' => 'MenusController@destroy']);
        // Modules routes
        Route::get('modules', ['as' => 'admin.modules.index',                'uses' => 'ModulesController@index']);
        Route::get('modules/{slug}/initialize', ['as' => 'admin.modules.initialize',           'uses' => 'ModulesController@initialize']);
        Route::get('modules/{slug}/confirm-uninitialize', ['as' => 'admin.modules.confirm-uninitialize', 'uses' => 'ModulesController@getModalUninitialize']);
        Route::get('modules/{slug}/uninitialize', ['as' => 'admin.modules.uninitialize',         'uses' => 'ModulesController@uninitialize']);
        Route::get('modules/{slug}/enable', ['as' => 'admin.modules.enable',               'uses' => 'ModulesController@enable']);
        Route::get('modules/{slug}/disable', ['as' => 'admin.modules.disable',              'uses' => 'ModulesController@disable']);
        Route::post('modules/enableSelected', ['as' => 'admin.modules.enable-selected',      'uses' => 'ModulesController@enableSelected']);
        Route::post('modules/disableSelected', ['as' => 'admin.modules.disable-selected',     'uses' => 'ModulesController@disableSelected']);
        Route::get('modules/optimize', ['as' => 'admin.modules.optimize',             'uses' => 'ModulesController@optimize']);
        // Permission routes
        Route::get('permissions/generate', ['as' => 'admin.permissions.generate',         'uses' => 'PermissionsController@generate']);
        Route::post('permissions/enableSelected', ['as' => 'admin.permissions.enable-selected',  'uses' => 'PermissionsController@enableSelected']);
        Route::post('permissions/disableSelected', ['as' => 'admin.permissions.disable-selected', 'uses' => 'PermissionsController@disableSelected']);
        Route::post('permissions', ['as' => 'admin.permissions.store',            'uses' => 'PermissionsController@store']);
        Route::get('permissions', ['as' => 'admin.permissions.index',            'uses' => 'PermissionsController@index']);
        Route::get('permissions/create', ['as' => 'admin.permissions.create',           'uses' => 'PermissionsController@create']);
        Route::get('permissions/{permissionId}', ['as' => 'admin.permissions.show',             'uses' => 'PermissionsController@show']);
        Route::patch('permissions/{permissionId}', ['as' => 'admin.permissions.patch',            'uses' => 'PermissionsController@update']);
        Route::put('permissions/{permissionId}', ['as' => 'admin.permissions.update',           'uses' => 'PermissionsController@update']);
        Route::delete('permissions/{permissionId}', ['as' => 'admin.permissions.destroy',          'uses' => 'PermissionsController@destroy']);
        Route::get('permissions/{permissionId}/edit', ['as' => 'admin.permissions.edit',             'uses' => 'PermissionsController@edit']);
        Route::get('permissions/{permissionId}/confirm-delete', ['as' => 'admin.permissions.confirm-delete',   'uses' => 'PermissionsController@getModalDelete']);
        Route::get('permissions/{permissionId}/delete', ['as' => 'admin.permissions.delete',           'uses' => 'PermissionsController@destroy']);
        Route::get('permissions/{permissionId}/enable', ['as' => 'admin.permissions.enable',           'uses' => 'PermissionsController@enable']);
        Route::get('permissions/{permissionId}/disable', ['as' => 'admin.permissions.disable',          'uses' => 'PermissionsController@disable']);
        // Route routes
        Route::get('routes/load', ['as' => 'admin.routes.load',             'uses' => 'RoutesController@load']);
        Route::post('routes/enableSelected', ['as' => 'admin.routes.enable-selected',  'uses' => 'RoutesController@enableSelected']);
        Route::post('routes/disableSelected', ['as' => 'admin.routes.disable-selected', 'uses' => 'RoutesController@disableSelected']);
        Route::post('routes/savePerms', ['as' => 'admin.routes.save-perms',       'uses' => 'RoutesController@savePerms']);
        Route::get('routes/search', ['as' => 'admin.routes.search',           'uses' => 'RoutesController@searchByName']);
        Route::post('routes/getInfo', ['as' => 'admin.routes.get-info',         'uses' => 'RoutesController@getInfo']);
        Route::post('routes', ['as' => 'admin.routes.store',            'uses' => 'RoutesController@store']);
        Route::get('routes', ['as' => 'admin.routes.index',            'uses' => 'RoutesController@index']);
        Route::get('routes/create', ['as' => 'admin.routes.create',           'uses' => 'RoutesController@create']);
        Route::get('routes/{routeId}', ['as' => 'admin.routes.show',             'uses' => 'RoutesController@show']);
        Route::patch('routes/{routeId}', ['as' => 'admin.routes.patch',            'uses' => 'RoutesController@update']);
        Route::put('routes/{routeId}', ['as' => 'admin.routes.update',           'uses' => 'RoutesController@update']);
        Route::delete('routes/{routeId}', ['as' => 'admin.routes.destroy',          'uses' => 'RoutesController@destroy']);
        Route::get('routes/{routeId}/edit', ['as' => 'admin.routes.edit',             'uses' => 'RoutesController@edit']);
        Route::get('routes/{routeId}/confirm-delete', ['as' => 'admin.routes.confirm-delete',   'uses' => 'RoutesController@getModalDelete']);
        Route::get('routes/{routeId}/delete', ['as' => 'admin.routes.delete',           'uses' => 'RoutesController@destroy']);
        Route::get('routes/{routeId}/enable', ['as' => 'admin.routes.enable',           'uses' => 'RoutesController@enable']);
        Route::get('routes/{routeId}/disable', ['as' => 'admin.routes.disable',          'uses' => 'RoutesController@disable']);
        // Audit routes
        Route::get('audit', ['as' => 'admin.audit.index',             'uses' => 'AuditsController@index']);
        Route::get('audit/purge', ['as' => 'admin.audit.purge',             'uses' => 'AuditsController@purge']);
        Route::get('audit/{auditId}/replay', ['as' => 'admin.audit.replay',            'uses' => 'AuditsController@replay']);
        Route::get('audit/{auditId}/show', ['as' => 'admin.audit.show',              'uses' => 'AuditsController@show']);
        // Error routes
        Route::get('errors', ['as' => 'admin.errors.index',             'uses' => 'ErrorsController@index']);
        Route::get('errors/purge', ['as' => 'admin.errors.purge',             'uses' => 'ErrorsController@purge']);
        Route::get('errors/{errorId}/show', ['as' => 'admin.errors.show',              'uses' => 'ErrorsController@show']);
        // Settings routes
        Route::post('settings', ['as' => 'admin.settings.store',            'uses' => 'SettingsController@store']);
        Route::get('settings', ['as' => 'admin.settings.index',            'uses' => 'SettingsController@index']);
        Route::get('settings/load', ['as' => 'admin.settings.load',             'uses' => 'SettingsController@load']);
        Route::get('settings/create', ['as' => 'admin.settings.create',           'uses' => 'SettingsController@create']);
        Route::get('settings/{settingKey}', ['as' => 'admin.settings.show',             'uses' => 'SettingsController@show']);
        Route::patch('settings/{settingKey}', ['as' => 'admin.settings.patch',            'uses' => 'SettingsController@update']);
        Route::put('settings/{settingKey}', ['as' => 'admin.settings.update',           'uses' => 'SettingsController@update']);
        Route::delete('settings/{settingKey}', ['as' => 'admin.settings.destroy',          'uses' => 'SettingsController@destroy']);
        Route::get('settings/{settingKey}/edit', ['as' => 'admin.settings.edit',             'uses' => 'SettingsController@edit']);
        Route::get('settings/{settingKey}/confirm-delete', ['as' => 'admin.settings.confirm-delete',   'uses' => 'SettingsController@getModalDelete']);
        Route::get('settings/{settingKey}/delete', ['as' => 'admin.settings.delete',           'uses' => 'SettingsController@destroy']);


        //imported rootes

        // Projects routes
        Route::post('projects/enableSelected', ['as' => 'admin.projects.enable-selected',  'uses' => 'ProjectsController@enableSelected']);
        Route::post('projects/disableSelected', ['as' => 'admin.projects.disable-selected', 'uses' => 'ProjectsController@disableSelected']);
        Route::get('projects', ['as' => 'admin.projects.index',            'uses' => 'ProjectsController@index']);
        Route::get('/old_projects', ['as' => 'admin.projects.old_projects',            'uses' => 'ProjectsController@old_projects']);

        Route::get('projects/tasks/{userId}', ['as' => 'admin.projects.projectTaskByUser',  'uses' => 'ProjectsController@projectTaskByUser']);

        Route::get('projects/tasks-status/{status}', ['as' => 'admin.projects.projectTaskByStatus', 'uses' => 'ProjectsController@projectTaskByStatus']);

        Route::resource('datatables', 'ProjectsController', [
            'anyData'  => 'datatables.data',
            'index' => 'datatables',
        ]);

        Route::get('projects/create', ['as' => 'admin.projects.create',           'uses' => 'ProjectsController@create']);
        Route::post('projects', ['as' => 'admin.projects.store',            'uses' => 'ProjectsController@store']);
        Route::get('projects/{leadId}', ['as' => 'admin.projects.show',             'uses' => 'ProjectsController@show']);
        Route::patch('projects/{leadId}', ['as' => 'admin.projects.patch',            'uses' => 'ProjectsController@update']);
        Route::put('projects/{leadId}', ['as' => 'admin.projects.update',           'uses' => 'ProjectsController@update']);
        Route::delete('projects/{leadId}', ['as' => 'admin.projects.destroy',          'uses' => 'ProjectsController@destroy']);
        Route::get('projects/{leadId}/edit', ['as' => 'admin.projects.edit',             'uses' => 'ProjectsController@edit']);
        Route::get('projects/{leadId}/confirm-delete', ['as' => 'admin.projects.confirm-delete',   'uses' => 'ProjectsController@getModalDelete']);
        Route::get('projects/{leadId}/delete', ['as' => 'admin.projects.delete',           'uses' => 'ProjectsController@destroy']);
        Route::get('projects/{leadId}/enable', ['as' => 'admin.projects.enable',           'uses' => 'ProjectsController@enable']);
        Route::get('projects/{leadId}/disable', ['as' => 'admin.projects.disable',          'uses' => 'ProjectsController@disable']);

        Route::post('ajaxTaskUpdate', ['as' => 'admin.ajaxTaskUpdate', 'uses' => 'ProjectTaskController@ajaxTaskUpdate']);
        Route::post('ajaxTaskPeopleUpdate', ['as' => 'admin.ajaxTaskPeopleUpdate', 'uses' => 'ProjectTaskController@ajaxTaskPeopleUpdate']);

        Route::get('projectboard', ['as' => 'projectboard', 'uses' => 'ProjectboardController@index']);
        Route::get('calendar', ['as' => 'calendar', 'uses' => 'CalendarController@index']);

        //Project Calendar
        Route::get('/project/calendar', ['as' => 'admin.project.calendar', 'uses' => 'ProjectCalendarController@index']);
        Route::get('/projects/filter/monthly', ['as' => 'admin.project.filter', 'uses' => 'ProjectsController@filterbydate']);
        Route::get('backlogs/tasks/{id}/', ['as' => 'admin.backlogs.tasks',          'uses' => 'ProjectTaskController@backlogsTasks']);

        //tasks modal
        Route::get('project_tasks/create/modals/{projectid}', ['as' => 'admin.project_task.modals',           'uses' => 'ProjectTaskController@openmodal']);
        Route::post('project_tasks/store/modals', ['as' => 'admin.project_task.modalspo',           'uses' => 'ProjectTaskController@postmodal']);

        //LEADS ROUTES

        Route::get('leads/SMSReport', ['as' => 'leads.receive-sms-report', 'uses' => 'LeadsController@receiveSMSReport']);

        // Search
        Route::get('search/leads', ['as' => 'admin.search.leads', 'uses' => 'LeadsController@search']);

        // Import Export Excel Files - Leads
        Route::get('importExportLeads', ['as' => 'admin.import-export.leads',            'uses' => 'ExcelController@leads']);

        Route::get('downloadExcelLeads/{type}', ['as' => 'admin.import-export.downloadExcelLeads', 'uses' => 'ExcelController@downloadExcelLeads']);
        Route::post('importExcelLeads', ['as' => 'admin.import-export.importExcelLeads', 'uses' => 'ExcelController@importExcelLeads']);

        Route::get('downloadexcelfilter', ['as' => 'admin.import-export.downloadexcelfilter', 'uses' => 'LeadsController@DownloadExcelFilter']);

        // For autocomplete of company
        Route::get('getdata', ['as' => 'admin.getdata', 'uses' => 'LeadsController@get_company']);
        // For autocomplete of company
        Route::get('getLeads', ['as' => 'admin.getLeads', 'uses' => 'TasksController@getLeads']);
        Route::get('getRooms', ['as' => 'admin.getrooms', 'uses' => 'Hotel\RoomController@get_rooms']);
        Route::get('users/ajax/getresv', ['as' => 'admin.getResv', 'uses' => 'Hotel\ReservationController@get_resv']);

        // report routes
        Route::post('reports/enableSelected', ['as' => 'admin.contacts.enable-selected',  'uses' => 'ContactsController@enableSelected']);
        Route::post('contacts/disableSelected', ['as' => 'admin.contacts.disable-selected', 'uses' => 'ContactsController@disableSelected']);

        Route::get('reports', ['as' => 'admin.reports.index',            'uses' => 'ReportsController@index']);

        Route::get('reports/leads_today', ['as' => 'admin.reports.reports_leads_today',      'uses' => 'ReportsController@leadsToday']);

        Route::get('reports/leads_by_status', ['as' => 'admin.reports.reports_leads_by_status',           'uses' => 'ReportsController@leadsByStatus']);
        Route::post('reports/post_leads_by_status', ['as' => 'admin.reports.reports_post_leads_by_status',           'uses' => 'ReportsController@postLeadsByStatus']);

        Route::get('reports/converted_leads', ['as' => 'admin.reports.reports_converted_leads',           'uses' => 'ReportsController@convertedLeads']);
        Route::post('reports/post_converted_leads', ['as' => 'admin.reports.reports_post_converted_leads',           'uses' => 'ReportsController@postConvertedLeads']);

        Route::get('reports/all_activities', ['as' => 'admin.reports.reports_all_activities',           'uses' => 'ReportsController@allActivities']);
        Route::get('reports/today_call_activities', ['as' => 'admin.reports.reports_today_calls',           'uses' => 'ReportsController@todayCallActivities']);
        Route::get('reports/all_contacts', ['as' => 'admin.reports.reports_all_contacts',           'uses' => 'ReportsController@allContacts']);
        Route::get('reports/all_clients', ['as' => 'admin.reports.reports_all_clients',           'uses' => 'ReportsController@allClients']);
        Route::get('payrollreports/monthly_report', ['as' => 'admin.payrollreports.monthly_report',           'uses' => 'ReportsController@payrollMonthlyReport']);

        Route::post('payrollreports/post_monthly_report', ['as' => 'admin.payrollreports.post_monthly_report', 'uses' => 'ReportsController@postPayrollMonthlyReport']);

        //pay frequency
        Route::get('payroll/payfrequency', ['as' => 'admin.payfrequency.index', 'uses' => 'PayFrequencyController@index']);
        Route::get('payroll/payfrequency/create', ['as' => 'admin.payfrequency.create', 'uses' => 'PayFrequencyController@create']);
        Route::post('payroll/payfrequency/create', ['as' => 'admin.payfrequency.store', 'uses' => 'PayFrequencyController@store']);
        Route::get('payroll/payfrequency/{id}', ['as' => 'admin.payfrequency.edit', 'uses' => 'PayFrequencyController@edit']);
        Route::post('payroll/payfrequency/{id}', ['as' => 'admin.payfrequency.update', 'uses' => 'PayFrequencyController@update']);
        Route::get('payroll/payfrequency/confirm-delete/{id}', ['as' => 'admin.payfrequency.confirmdelete', 'uses' => 'PayFrequencyController@getModalDelete']);
        Route::get('payroll/payfrequency/delete/{id}', ['as' => 'admin.payfrequency.delete', 'uses' => 'PayFrequencyController@destroy']);

        Route::get('payroll/payfrequency/view_timecard/{id}', ['as' => 'admin.payfrequency.view_timecard', 'uses' => 'PayFrequencyController@view_timecard']);
        Route::get('payroll/payfrequency/view_salarylist/{id}', ['as' => 'admin.payfrequency.view_salarylist', 'uses' => 'PayFrequencyController@view_salarylist']);

        // Send SMS
        Route::get('sendsms', ['as' => 'admin.sendsms', 'uses' => 'LeadsController@sendsms']);
        Route::get('sms/send', ['as' => 'admin.send_sms',      'uses' => 'SmsController@index']);
        Route::post('sms/postSend', ['as' => 'admin.post_send_sms',      'uses' => 'SmsController@postSend']);

        // Send SMS
        Route::post('leads/sendSMS', ['as' => 'admin.leads.send-sms', 'uses' => 'LeadsController@sendSMS']);
        Route::post('leads/sendLeadSMS', ['as' => 'admin.leads.send-lead-sms', 'uses' => 'LeadsController@sendLeadSMS']);

        // Lead routes
        Route::post('leads/enableSelected', ['as' => 'admin.leads.enable-selected',  'uses' => 'LeadsController@enableSelected']);
        Route::post('leads/disableSelected', ['as' => 'admin.leads.disable-selected', 'uses' => 'LeadsController@disableSelected']);
        Route::get('leads/search', ['as' => 'admin.leads.search',           'uses' => 'LeadsController@searchByName']);
        Route::post('leads/getInfo', ['as' => 'admin.leads.get-info',         'uses' => 'LeadsController@getInfo']);
        Route::post('leads', ['as' => 'admin.leads.store',            'uses' => 'LeadsController@store']);
        Route::get('leads', ['as' => 'admin.leads.index',            'uses' => 'LeadsController@index']);
        Route::get('today_followed', ['as' => 'admin.leads.today_followed',   'uses' => 'LeadsController@today_followed']);
        Route::get('leads/create', ['as' => 'admin.leads.create',           'uses' => 'LeadsController@create']);
        Route::get('leads/create/modal', ['as' => 'admin.leads.modal',           'uses' => 'LeadsController@createModal']);
        Route::post('leads/store/modal', ['as' => 'admin.leads.postmodal',           'uses' => 'LeadsController@postModal']);

        Route::get('leads/{leadId}', ['as' => 'admin.leads.show',             'uses' => 'LeadsController@show']);

        Route::post('leads/{leadId}/storelogo', ['as' => 'admin.leads.storelogo',             'uses' => 'LeadsController@storeLogo']);

        Route::patch('leads/{leadId}', ['as' => 'admin.leads.patch',            'uses' => 'LeadsController@update']);
        Route::put('leads/{leadId}', ['as' => 'admin.leads.update',           'uses' => 'LeadsController@update']);
        Route::delete('leads/{leadId}', ['as' => 'admin.leads.destroy',          'uses' => 'LeadsController@destroy']);
        Route::get('leads/{leadId}/edit', ['as' => 'admin.leads.edit',             'uses' => 'LeadsController@edit']);
        Route::get('leads/{leadId}/confirm-delete', ['as' => 'admin.leads.confirm-delete',   'uses' => 'LeadsController@getModalDelete']);
        Route::get('leads/{leadId}/delete', ['as' => 'admin.leads.delete',           'uses' => 'LeadsController@destroy']);
        Route::get('leads/{leadId}/enable', ['as' => 'admin.leads.enable',           'uses' => 'LeadsController@enable']);
        Route::get('leads/{leadId}/disable', ['as' => 'admin.leads.disable',          'uses' => 'LeadsController@disable']);

        Route::get('getcontactinfo', ['as' => 'admin.leads.getcontactinfo', 'uses' => 'LeadsController@getcontactinfo']);

        Route::get('todays_action', ['as' => 'admin.todays_action.disable',          'uses' => 'LeadsController@todays_action']);
        Route::get('overdue_leads', ['as' => 'admin.overdue_leads.disable',          'uses' => 'LeadsController@overdue_leads']);

        Route::post('ajax_lead_status', ['as' => 'admin.ajax_lead_status', 'uses' => 'LeadsController@ajaxLeadStatus']);
        Route::post('ajax_task_status', ['as' => 'admin.ajax_task_status', 'uses' => 'TasksController@ajaxTaskStatus']);
        Route::post('ajax_task_update', ['as' => 'admin.ajaxTaskUpdate', 'uses' => 'TasksController@ajaxTaskUpdate']);
        Route::post('ajax_room_update', ['as' => 'admin.ajaxRoomUpdate', 'uses' => 'Hotel\HouseKeepingController@ajaxRoomUpdate']);

        Route::post('ajax_lead_rating', ['as' => 'admin.ajax_lead_rating', 'uses' => 'LeadsController@ajaxLeadRating']);

        // For autocomplete of Cities
        Route::get('getCities', ['as' => 'admin.getCities', 'uses' => 'LeadsController@getCities']);

        Route::get('mail/{taskId}/show-offerlettermodal', ['as' => 'admin.mail.show-offerlettermodal',   'uses' => 'MailController@getOfferLetterModalMail']);

        Route::post('mail/{taskId}/send-offerlettermodal', ['as' => 'admin.mail.send-offerlettermodal',           'uses' => 'MailController@postOfferLetterModalMail']);

        Route::get('mail/{taskId}/show-unsuccessfulapplicationmodal', ['as' => 'admin.mail.show-unsuccessfulapplicationmodal',   'uses' => 'MailController@getUnsuccessAppModalMail']);
        Route::post('mail/{taskId}/send-unsuccessfulapplicationmodal', ['as' => 'admin.mail.send-unsuccessfulapplicationmodal',           'uses' => 'MailController@postUnsuccessAppModalMail']);

        Route::get('mail/{taskId}/show-pendingmodal', ['as' => 'admin.mail.show-pendingmodal',   'uses' => 'MailController@getPendingModalMail']);
        Route::post('mail/{taskId}/send-pendingmodal', ['as' => 'admin.mail.send-pendingmodal',           'uses' => 'MailController@postPendingModalMail']);

        //campaigns
        Route::get('campaigns/index', ['as' => 'admin.campaigns.index',             'uses' => 'CampaignsController@index']);
        Route::get('campaigns/create', ['as' => 'admin.campaigns.create',             'uses' => 'CampaignsController@create']);
        Route::post('campaigns/create', ['as' => 'admin.campaigns.store',             'uses' => 'CampaignsController@store']);
        Route::get('campaigns/edit/{id}', ['as' => 'admin.campaigns.edit',             'uses' => 'CampaignsController@edit']);
        Route::post('campaigns/edit/{id}', ['as' => 'admin.campaigns.up',             'uses' => 'CampaignsController@update']);
        Route::get('campaigns/show/{id}', ['as' => 'admin.campaigns.show',             'uses' => 'CampaignsController@show']);
        Route::get('campaigns/confirm-delete/{id}', ['as' => 'admin.campaigns.confirm-delete',             'uses' => 'CampaignsController@getModalDelete']);
        Route::get('campaigns/delete/{id}', ['as' => 'admin.campaigns.delete', 'uses' => 'CampaignsController@destroy']);

        //onboarding
        Route::get('onboard/tasktype', ['as' => 'admin.onboard.tasktype', 'uses' => 'Onboarding\TaskTypeController@index']);
        Route::get('onboard/tasktype/create', ['as' => 'admin.onboard.tasktype.create', 'uses' => 'Onboarding\TaskTypeController@create']);
        Route::post('onboard/tasktype/store', ['as' => 'admin.onboard.tasktype.store', 'uses' => 'Onboarding\TaskTypeController@store']);
        Route::get('onboard/tasktype/edit/{id}', ['as' => 'admin.onboard.tasktype.edit', 'uses' => 'Onboarding\TaskTypeController@edit']);
        Route::post('onboard/tasktype/edit/{id}', ['as' => 'admin.onboard.tasktype.upd', 'uses' => 'Onboarding\TaskTypeController@update']);
        Route::get('onboard/tasktype/show/{id}', ['as' => 'admin.onboard.tasktype.show', 'uses' => 'Onboarding\TaskTypeController@show']);
        Route::get('onboard/tasktype/confirm-delete/{id}', ['as' => 'admin.onboard.tasktype.confirm-delete', 'uses' => 'Onboarding\TaskTypeController@getModalDelete']);
        Route::get('onboard/tasktype/delete/{id}', ['as' => 'admin.onboard.tasktype.delete', 'uses' => 'Onboarding\TaskTypeController@destroy']);

        Route::get('onboard/events', ['as' => 'admin.onboard.events', 'uses' => 'Onboarding\EventsController@index']);
        Route::get('onboard/events/create', ['as' => 'admin.onboard.events.create', 'uses' => 'Onboarding\EventsController@create']);
        Route::post('onboard/events/store', ['as' => 'admin.onboard.events.store', 'uses' => 'Onboarding\EventsController@store']);
        Route::get('onboard/events/edit/{id}', ['as' => 'admin.onboard.events.edit', 'uses' => 'Onboarding\EventsController@edit']);
        Route::post('onboard/events/edit/{id}', ['as' => 'admin.onboard.events.upd', 'uses' => 'Onboarding\EventsController@update']);
        Route::get('onboard/events/show/{id}', ['as' => 'admin.onboard.events.show', 'uses' => 'Onboarding\EventsController@show']);
        Route::get('onboard/events/confirm-delete/{id}', ['as' => 'admin.onboard.events.confirm-delete', 'uses' => 'Onboarding\EventsController@getModalDelete']);
        Route::get('onboard/events/delete/{id}', ['as' => 'admin.onboard.events.delete', 'uses' => 'Onboarding\EventsController@destroy']);

        Route::get('onboard/task', ['as' => 'admin.onboard.task', 'uses' => 'Onboarding\TaskController@index']);
        Route::get('onboard/task/create', ['as' => 'admin.onboard.task.create', 'uses' => 'Onboarding\TaskController@create']);
        Route::post('onboard/task/store', ['as' => 'admin.onboard.task.store', 'uses' => 'Onboarding\TaskController@store']);
        Route::get('onboard/task/edit/{id}', ['as' => 'admin.onboard.task.edit', 'uses' => 'Onboarding\TaskController@edit']);
        Route::post('onboard/task/edit/{id}', ['as' => 'admin.onboard.task.upd', 'uses' => 'Onboarding\TaskController@update']);
        Route::get('onboard/task/show/{id}', ['as' => 'admin.onboard.task.show', 'uses' => 'Onboarding\TaskController@show']);
        Route::get('onboard/task/confirm-delete/{id}', ['as' => 'admin.onboard.task.confirm-delete', 'uses' => 'Onboarding\TaskController@getModalDelete']);
        Route::get('onboard/task/delete/{id}', ['as' => 'admin.onboard.task.delete', 'uses' => 'Onboarding\TaskController@destroy']);
        Route::get('onboard/task/tasktype/{id}', 'Onboarding\TaskController@getTaskinfo');

        // Task routes
        Route::post('tasks/enableSelected', ['as' => 'admin.tasks.enable-selected',  'uses' => 'TasksController@enableSelected']);
        Route::post('tasks/disableSelected', ['as' => 'admin.tasks.disable-selected', 'uses' => 'TasksController@disableSelected']);
        Route::get('tasks/search', ['as' => 'admin.tasks.search',           'uses' => 'TasksController@searchByName']);
        Route::post('tasks/getInfo', ['as' => 'admin.tasks.get-info',         'uses' => 'TasksController@getInfo']);
        Route::post('tasks', ['as' => 'admin.tasks.store',            'uses' => 'TasksController@store']);
        Route::get('tasks', ['as' => 'admin.tasks.index',            'uses' => 'TasksController@index']);

        Route::get('tasks/getAllTasks', ['as' => 'admin.tasks.allData',            'uses' => 'TasksController@allData']);

        Route::get('tasks/create', ['as' => 'admin.tasks.create',           'uses' => 'TasksController@create']);
        Route::get('tasks/{taskId}', ['as' => 'admin.tasks.show',             'uses' => 'TasksController@show']);
        Route::patch('tasks/{taskId}', ['as' => 'admin.tasks.patch',            'uses' => 'TasksController@update']);
        Route::put('tasks/{taskId}', ['as' => 'admin.tasks.update',           'uses' => 'TasksController@update']);
        Route::delete('tasks/{taskId}', ['as' => 'admin.tasks.destroy',          'uses' => 'TasksController@destroy']);
        Route::get('tasks/{taskId}/edit', ['as' => 'admin.tasks.edit',             'uses' => 'TasksController@edit']);
        Route::get('tasks/{taskId}/confirm-delete', ['as' => 'admin.tasks.confirm-delete',   'uses' => 'TasksController@getModalDelete']);
        Route::get('tasks/{taskId}/delete', ['as' => 'admin.tasks.delete',           'uses' => 'TasksController@destroy']);
        Route::get('tasks/{taskId}/enable', ['as' => 'admin.tasks.enable',           'uses' => 'TasksController@enable']);
        Route::get('tasks/{taskId}/disable', ['as' => 'admin.tasks.disable',          'uses' => 'TasksController@disable']);

        // For autocomplete of contact
        Route::get('getContacts', ['as' => 'admin.getcontacts', 'uses' => 'ContactsController@get_contact']);
        // For autocomplete of Clients
        Route::get('getClients', ['as' => 'admin.getclients', 'uses' => 'ClientsController@get_client']);

        Route::get('getProducts', ['as' => 'admin.getproducts', 'uses' => 'ProductController@get_products']);

        // Send mail from Lead Detail
        Route::get('mail/{taskId}/show-mailmodal', ['as' => 'admin.mail.show-mailmodal',   'uses' => 'MailController@getModalMail']);
        Route::post('mail/{taskId}/send-mail-modal', ['as' => 'admin.mail.send-mail-modal',           'uses' => 'MailController@postModalMail']);
        // Send mail from Quotation Detail
        Route::get('mail/quotation/{taskId}/show-mailmodal', ['as' => 'admin.mail.quotation.show-mailmodal',   'uses' => 'MailController@getModalMailQuotation']);
        Route::post('mail/quotation/{taskId}/send-mail-modal', ['as' => 'admin.mail.quotation.send-mail-modal',           'uses' => 'MailController@postModalMailQuotation']);

        Route::get('mail/inbox', ['as' => 'admin.mail.inbox',   'uses' => 'MailController@inbox']);
        Route::get('mail/reloadinbox', ['as' => 'admin.mail.reloadinbox',   'uses' => 'MailController@reloadinbox']);
        Route::get('mail/sent', ['as' => 'admin.mail.sent',   'uses' => 'MailController@sent']);
        Route::get('mail/show/{mailid}', ['as' => 'admin.mail.show',   'uses' => 'MailController@show']);
        Route::post('mail/reply', ['as' => 'admin.mail.reply',   'uses' => 'MailController@reply']);
        Route::get('mail/attachment/{filename}', ['as' => 'admin.mail.attachment',   'uses' => 'MailController@attachment']);
        Route::get('mail/{mailId}/confirm-delete', ['as' => 'admin.mail.confirm-delete',   'uses' => 'MailController@getModalDelete']);
        Route::get('mail/{mailId}/delete', ['as' => 'admin.mail.delete',           'uses' => 'MailController@destroy']);
        Route::get('mail/sent_attachment/{filename}', ['as' => 'admin.mail.sentattachment',   'uses' => 'MailController@sent_attachment']);
        Route::get('mail/from_lead/{mailId}', ['as' => 'admin.mail.mail_from_lead',           'uses' => 'MailController@showModalLeadMail']);

        // Send the sms to all product/lead by course and status
        Route::get('mail/send-bulk-sms', ['as' => 'admin.mail.send-bulk-sms',           'uses' => 'SmsController@getBulkSMSForm']);
        Route::post('mail/post-send-bulk-sms', ['as' => 'admin.mail.post-send-bulk-sms',           'uses' => 'SmsController@postBulkSMS']);

        // Send the email to all product/lead by course and status
        Route::get('mail/send-bulk-email', ['as' => 'admin.mail.send-bulk-email',           'uses' => 'MailController@getBulkMailForm']);
        Route::post('mail/post-send-bulk-email', ['as' => 'admin.mail.post-send-bulk-email',           'uses' => 'MailController@postBulkMail']);

        Route::get('mail/send-bulk-email-all', ['as' => 'admin.mail.send-bulk-email-all',           'uses' => 'MailController@getBulkMailAllForm']);
        Route::post('mail/post-send-bulk-email-all', ['as' => 'admin.mail.post-send-bulk-email-all',           'uses' => 'MailController@postBulkMailAll']);

        Route::get('mail/send-bulk-email-all', ['as' => 'admin.mail.send-bulk-email-all',           'uses' => 'MailController@getBulkMailAllForm']);
        Route::post('mail/post-send-bulk-email-all', ['as' => 'admin.mail.post-send-bulk-email-all',           'uses' => 'MailController@postBulkMailAll']);

        Route::post('post-send-bulk-email/loadTemplate', ['as' => 'admin.mail.bulk-template',           'uses' => 'MailController@loadTemplate']);

        Route::get('email/getLeadsTotal', ['as' => 'admin.email.getLeadsTotal',           'uses' => 'MailController@getLeadsTotal']);

        //bulkmail logs
        Route::get('bulkmail/logs', ['as' => 'admin.mail.bulklogs',           'uses' => 'MailController@mailLogIndex']);

        // Send bulk email to all contacts
        Route::get('mail/send-bulk-email-contact', ['as' => 'admin.mail.send-bulk-email-contact',           'uses' => 'MailController@getBulkEmailContact']);
        Route::post('mail/post-send-bulk-email-contact', ['as' => 'admin.mail.post-send-bulk-email-contact',           'uses' => 'MailController@postBulkMailContact']);

        // Send the sms to all contacts
        Route::get('mail/send-bulk-sms-contact', ['as' => 'admin.send-bulk-sms-contact',           'uses' => 'SmsController@getBulkSMSContact']);
        Route::post('mail/post-send-bulk-sms-contact', ['as' => 'admin.post-send-bulk-sms-contact',           'uses' => 'SmsController@postBulkSMSContact']);

        // Product routes
        Route::post('products/enableSelected', ['as' => 'admin.products.enable-selected',  'uses' => 'ProductController@enableSelected']);
        Route::post('products/disableSelected', ['as' => 'admin.products.disable-selected', 'uses' => 'ProductController@disableSelected']);
        Route::get('products/search', ['as' => 'admin.products.search',           'uses' => 'ProductController@searchByName']);
        Route::post('products/getInfo', ['as' => 'admin.products.get-info',         'uses' => 'ProductController@getInfo']);
        Route::post('products', ['as' => 'admin.products.store',            'uses' => 'ProductController@store']);
        Route::get('products', ['as' => 'admin.products.index',            'uses' => 'ProductController@index']);
        Route::get('products/create', ['as' => 'admin.products.create',           'uses' => 'ProductController@create']);
        Route::get('products/{courseId}', ['as' => 'admin.products.show',             'uses' => 'ProductController@show']);
        Route::patch('products/{courseId}', ['as' => 'admin.products.patch',            'uses' => 'ProductController@update']);
        Route::put('products/{courseId}', ['as' => 'admin.products.update',           'uses' => 'ProductController@update']);
        Route::delete('products/{courseId}', ['as' => 'admin.products.destroy',          'uses' => 'ProductController@destroy']);
        Route::get('products/{courseId}/edit', ['as' => 'admin.products.edit',             'uses' => 'ProductController@edit']);
        Route::get('products/{courseId}/confirm-delete', ['as' => 'admin.products.confirm-delete',   'uses' => 'ProductController@getModalDelete']);
        Route::get('products/{courseId}/delete', ['as' => 'admin.products.delete',           'uses' => 'ProductController@destroy']);
        Route::get('products/{courseId}/enable', ['as' => 'admin.products.enable',           'uses' => 'ProductController@enable']);
        Route::get('products/{courseId}/disable', ['as' => 'admin.products.disable',          'uses' => 'ProductController@disable']);

        Route::get('product/stocks_count', ['as' => 'admin.products.stocks_count', 'uses' => 'ProductController@stocks_count']);

        Route::get('product/stock_adjustment', ['as' => 'admin.products.stock_adjustment', 'uses' => 'ProductController@stock_adjustment']);
        Route::get('product/statement', ['as' => 'admin.products.statement', 'uses' => 'ProductController@product_statement']);

        // Cases routes
        Route::post('cases/enableSelected', ['as' => 'admin.cases.enable-selected',  'uses' => 'CasesController@enableSelected']);
        Route::post('cases/disableSelected', ['as' => 'admin.cases.disable-selected', 'uses' => 'CasesController@disableSelected']);
        Route::get('cases/search', ['as' => 'admin.cases.search',           'uses' => 'CasesController@searchByName']);
        Route::post('cases/getInfo', ['as' => 'admin.cases.get-info',         'uses' => 'CasesController@getInfo']);
        Route::post('cases', ['as' => 'admin.cases.store',            'uses' => 'CasesController@store']);

        Route::get('cases', ['as' => 'admin.cases.index',            'uses' => 'CasesController@index']);

        Route::get('cases/create', ['as' => 'admin.cases.create',           'uses' => 'CasesController@create']);
        Route::get('cases/{caseId}', ['as' => 'admin.cases.show',             'uses' => 'CasesController@show']);
        Route::patch('case/{caseId}', ['as' => 'admin.cases.patch',            'uses' => 'CasesController@update']);
        Route::put('cases/{caseId}', ['as' => 'admin.cases.update',           'uses' => 'CasesController@update']);
        Route::delete('case/{caseId}', ['as' => 'admin.cases.destroy',          'uses' => 'CasesController@destroy']);
        Route::get('cases/{caseId}/edit', ['as' => 'admin.cases.edit',             'uses' => 'CasesController@edit']);
        Route::get('cases/{caseId}/confirm-delete', ['as' => 'admin.cases.confirm-delete',   'uses' => 'CasesController@getModalDelete']);
        Route::get('cases/{caseId}/delete', ['as' => 'admin.cases.delete',           'uses' => 'CasesController@destroy']);
        Route::get('cases/{caseId}/enable', ['as' => 'admin.cases.enable',           'uses' => 'CasesController@enable']);
        Route::get('cases/{caseId}/disable', ['as' => 'admin.cases.disable',          'uses' => 'CasesController@disable']);

        // casses template category

        Route::get('casescategory', ['as' => 'admin.casescategory.index',            'uses' => 'CasesCategoryController@index']);
        Route::get('casescategory/create', ['as' => 'admin.casescategory.create',            'uses' => 'CasesCategoryController@create']);
        Route::post('casescategory/store', ['as' => 'admin.casescategory.store',            'uses' => 'CasesCategoryController@store']);
        Route::get('casescategory/{id}/edit', ['as' => 'admin.casescategory.edit',            'uses' => 'CasesCategoryController@edit']);
        Route::post('casescategory/{id}/update', ['as' => 'admin.casescategory.update',            'uses' => 'CasesCategoryController@update']);

        Route::get('casescategory/{caseId}/confirm-delete', ['as' => 'admin.casescategory.confirm-delete',   'uses' => 'CasesCategoryController@getModalDelete']);
        Route::get('casescategory/{caseId}/delete', ['as' => 'admin.casescategory.delete',           'uses' => 'CasesCategoryController@destroy']);

        Route::get('casecategorytemplate', ['as' => 'admin.casecategorytemplate.info', 'uses' => 'CasesController@casecategorytemplate']);

        Route::get('casesdashboard', ['as' => 'admin.cases.dashboard', 'uses' => 'CasesDashBoardController@index']);
        // casses sub template category

        Route::get('casessubcategory', ['as' => 'admin.casessubcategory.index',            'uses' => 'CasesSubCategoryController@index']);
        Route::get('casessubcategory/create', ['as' => 'admin.casessubcategory.create',            'uses' => 'CasesSubCategoryController@create']);
        Route::post('casessubcategory/store', ['as' => 'admin.casessubcategory.store',            'uses' => 'CasesSubCategoryController@store']);
        Route::get('casessubcategory/{id}/edit', ['as' => 'admin.casessubcategory.edit',            'uses' => 'CasesSubCategoryController@edit']);
        Route::post('casessubcategory/{id}/update', ['as' => 'admin.casessubcategory.update',            'uses' => 'CasesSubCategoryController@update']);
        Route::get('casessubcategory/{caseId}/confirm-delete', ['as' => 'admin.casessubcategory.confirm-delete',   'uses' => 'CasesSubCategoryController@getModalDelete']);
        Route::get('casessubcategory/{caseId}/delete', ['as' => 'admin.casessubcategory.delete',           'uses' => 'CasesSubCategoryController@destroy']);



        //folders
        Route::get('folders', ['as' => 'admin.folders.index', 'uses' => 'FolderController@index']);
        Route::get('folders/create', ['as' => 'admin.folders.create', 'uses' => 'FolderController@create']);
        Route::post('folders/create', ['as' => 'admin.folders.store', 'uses' => 'FolderController@store']);
        Route::get('folders/edit/{id}', ['as' => 'admin.folders.edit', 'uses' => 'FolderController@edit']);
        Route::post('folders/edit/{id}', ['as' => 'admin.folders.update', 'uses' => 'FolderController@update']);
        Route::get('folders/confirm-delete{id}', ['as' => 'admin.folders.confirm-delete', 'uses' => 'FolderController@getModalDelete']);
        Route::get('folders/delete/{id}', ['as' => 'admin.folders.delete', 'uses' => 'FolderController@destroy']);
        //Doc category
        Route::get('doc_category', ['as' => 'admin.doc_category.index', 'uses' => 'DocCategoriesController@index']);
        Route::get('doc_category/create', ['as' => 'admin.doc_category.create', 'uses' => 'DocCategoriesController@create']);
        Route::post('doc_category/create', ['as' => 'admin.doc_category.store', 'uses' => 'DocCategoriesController@store']);
        Route::get('doc_category/edit/{id}', ['as' => 'admin.doc_category.edit', 'uses' => 'DocCategoriesController@edit']);
        Route::post('doc_category/edit/{id}', ['as' => 'admin.doc_category.update', 'uses' => 'DocCategoriesController@update']);
        Route::get('doc_category/confirm-delete{id}', ['as' => 'admin.doc_category.confirm-delete', 'uses' => 'DocCategoriesController@getModalDelete']);
        Route::get('doc_category/delete/{id}', ['as' => 'admin.doc_category.delete', 'uses' => 'DocCategoriesController@destroy']);
        //sticky notes
        Route::get('stickynote', ['as' => 'admin.stickynote.index', 'uses' => 'StickyNoteController@index']);
        Route::post('stickynote/store', ['as' => 'admin.stickynote.store', 'uses' => 'StickyNoteController@store']);
        Route::post('stickynote/destroy/{id}', ['as' => 'admin.stickynote.destroy', 'uses' => 'StickyNoteController@destroy']);
        // Proposal routes
        Route::post('proposal/enableSelected', ['as' => 'admin.proposal.enable-selected',  'uses' => 'ProposalController@enableSelected']);
        Route::post('proposal/disableSelected', ['as' => 'admin.proposal.disable-selected', 'uses' => 'ProposalController@disableSelected']);
        Route::get('proposal/search', ['as' => 'admin.proposal.search',           'uses' => 'ProposalController@searchByName']);
        Route::post('proposal/getInfo', ['as' => 'admin.proposal.get-info',         'uses' => 'ProposalController@getInfo']);
        Route::post('proposal', ['as' => 'admin.proposal.store',            'uses' => 'ProposalController@store']);

        Route::get('proposal', ['as' => 'admin.proposal.index',            'uses' => 'ProposalController@index']);

        Route::get('proposal/create', ['as' => 'admin.proposal.create',           'uses' => 'ProposalController@create']);
        Route::get('proposal/{proposalId}', ['as' => 'admin.proposal.show',             'uses' => 'ProposalController@show']);
        Route::patch('proposal/{proposalId}', ['as' => 'admin.proposal.patch',            'uses' => 'ProposalController@update']);
        Route::put('proposal/{proposalId}', ['as' => 'admin.proposal.update',           'uses' => 'ProposalController@update']);
        Route::delete('proposal/{proposalId}', ['as' => 'admin.proposal.destroy',          'uses' => 'ProposalController@destroy']);
        Route::get('proposal/{proposalId}/edit', ['as' => 'admin.proposal.edit',             'uses' => 'ProposalController@edit']);
        Route::get('proposal/{proposalId}/confirm-delete', ['as' => 'admin.proposal.confirm-delete',   'uses' => 'ProposalController@getModalDelete']);
        Route::get('proposal/{proposalId}/delete', ['as' => 'admin.proposal.delete',           'uses' => 'ProposalController@destroy']);
        Route::get('proposal/{proposalId}/enable', ['as' => 'admin.proposal.enable',           'uses' => 'ProposalController@enable']);
        Route::get('proposal/{proposalId}/disable', ['as' => 'admin.proposal.disable',          'uses' => 'ProposalController@disable']);

        Route::get('proposal/print/{id}', ['as' => 'admin.proposal.print',  'uses' => 'ProposalController@printInvoice']);
        Route::get('proposal/generatePDF/{id}', ['as' => 'admin.proposal.generatePDF',   'uses' => 'ProposalController@generatePDF']);
        // Send Email of Proposal from Detail Page
        Route::get('proposalMail/{proposalId}/show-mailmodal', ['as' => 'admin.proposalMail.show-mailmodal', 'uses' => 'ProposalController@getModalMail']);
        Route::post('proposalMail/{proposalId}/send-mail-modal', ['as' => 'admin.proposalMail.send-mail-modal', 'uses' => 'ProposalController@postModalMail']);
        // Ajax upload the template
        Route::post('proposal/loadTemplate', ['as' => 'admin.loadTemplate', 'uses' => 'ProposalController@loadTemplate']);
        Route::get('proposal/copy/{id}', ['as' => 'admin.proposal.copy',  'uses' => 'ProposalController@copyDoc']);

        Route::get('order/copy/{id}', ['as' => 'admin.proposal.copydoc',  'uses' => 'OrdersController@copyDoc']);

        // Documents routes
        Route::post('documents/enableSelected', ['as' => 'admin.documents.enable-selected',  'uses' => 'DocumentController@enableSelected']);
        Route::post('documents/disableSelected', ['as' => 'admin.documents.disable-selected', 'uses' => 'DocumentController@disableSelected']);
        Route::get('documents/search', ['as' => 'admin.documents.search',           'uses' => 'DocumentController@searchByName']);
        Route::post('documents/getInfo', ['as' => 'admin.documents.get-info',         'uses' => 'DocumentController@getInfo']);
        Route::post('documents', ['as' => 'admin.documents.store',            'uses' => 'DocumentController@store']);

        Route::get('documents', ['as' => 'admin.documents.index',            'uses' => 'DocumentController@index']);

        Route::get('documents/create', ['as' => 'admin.documents.create',           'uses' => 'DocumentController@create']);
        Route::get('documents/{documentId}', ['as' => 'admin.documents.show',             'uses' => 'DocumentController@show']);
        Route::patch('document/{documentId}', ['as' => 'admin.documents.patch',            'uses' => 'DocumentController@update']);
        Route::put('documents/{documentId}', ['as' => 'admin.documents.update',           'uses' => 'DocumentController@update']);
        Route::delete('document/{documentId}', ['as' => 'admin.documents.destroy',          'uses' => 'DocumentController@destroy']);
        Route::get('documents/{documentId}/edit', ['as' => 'admin.documents.edit',             'uses' => 'DocumentController@edit']);
        Route::get('documents/{documentId}/confirm-delete', ['as' => 'admin.documents.confirm-delete',   'uses' => 'DocumentController@getModalDelete']);
        Route::get('documents/{documentId}/delete', ['as' => 'admin.documents.delete',           'uses' => 'DocumentController@destroy']);
        Route::get('documents/{documentId}/enable', ['as' => 'admin.documents.enable',           'uses' => 'DocumentController@enable']);
        Route::get('documents/{documentId}/disable', ['as' => 'admin.documents.disable',          'uses' => 'DocumentController@disable']);

        // Bugs routes
        Route::post('bugs/enableSelected', ['as' => 'admin.bugs.enable-selected',  'uses' => 'BugsController@enableSelected']);
        Route::post('bugs/disableSelected', ['as' => 'admin.bugs.disable-selected', 'uses' => 'BugsController@disableSelected']);
        Route::get('bugs/search', ['as' => 'admin.bugs.search',           'uses' => 'BugsController@searchByName']);
        Route::post('bugs/getInfo', ['as' => 'admin.bugs.get-info',         'uses' => 'BugsController@getInfo']);
        Route::post('bugs', ['as' => 'admin.bugs.store',            'uses' => 'BugsController@store']);

        Route::get('bugs', ['as' => 'admin.bugs.index',            'uses' => 'BugsController@index']);

        Route::get('bugs/create', ['as' => 'admin.bugs.create',           'uses' => 'BugsController@create']);
        Route::get('bugs/{bugId}', ['as' => 'admin.bugs.show',             'uses' => 'BugsController@show']);
        Route::patch('bug/{bugId}', ['as' => 'admin.bugs.patch',            'uses' => 'BugsController@update']);
        Route::put('bugs/{bugId}', ['as' => 'admin.bugs.update',           'uses' => 'BugsController@update']);
        Route::delete('bug/{bugId}', ['as' => 'admin.bugs.destroy',          'uses' => 'BugsController@destroy']);
        Route::get('bugs/{bugId}/edit', ['as' => 'admin.bugs.edit',             'uses' => 'BugsController@edit']);
        Route::get('bugs/{bugId}/confirm-delete', ['as' => 'admin.bugs.confirm-delete',   'uses' => 'BugsController@getModalDelete']);
        Route::get('bugs/{bugId}/delete', ['as' => 'admin.bugs.delete',           'uses' => 'BugsController@destroy']);
        Route::get('bugs/{bugId}/enable', ['as' => 'admin.bugs.enable',           'uses' => 'BugsController@enable']);
        Route::get('bugs/{caseId}/disable', ['as' => 'admin.bugs.disable',          'uses' => 'BugsController@disable']);

        // Sales Accounting Routes

        Route::get('sales/list', ['as' => 'admin.salesaccount.index',            'uses' => 'SalesAccountController@invoiceindex']);
        Route::get('payment/list', ['as' => 'admin.paymentaccount.index',          'uses' => 'SalesAccountController@paymentindex']);
        Route::get('order/list', ['as' => 'admin.quotationaccount.index',            'uses' => 'SalesAccountController@quotationindex']);

        Route::get('sales/create', ['as' => 'admin.salesaccount.create',           'uses' => 'SalesAccountController@invoicecreate']);
        Route::get('payment/create/{orderid}/{invoiceid}', ['as' => 'admin.paymentaccount.create',           'uses' => 'SalesAccountController@paymentcreate']);
        Route::get('order/create', ['as' => 'admin.quotationaccount.create',           'uses' => 'SalesAccountController@quotationcreate']);

        Route::get('sales/edit/{orderId}', ['as' => 'admin.salesaccount.edit',             'uses' => 'SalesAccountController@invoiceedit']);
        Route::get('payment/edit/{orderId}', ['as' => 'admin.paymentaccount.edit',             'uses' => 'SalesAccountController@paymentedit']);
        Route::get('order/edit/{orderId}', ['as' => 'admin.quotationaccount.edit',              'uses' => 'SalesAccountController@quotationedit']);

        Route::post('sales', ['as' => 'admin.salesaccount.store',            'uses' => 'SalesAccountController@invoicestore']);
        Route::post('payment', ['as' => 'admin.paymentaccount.store',            'uses' => 'SalesAccountController@paymentstore']);
        Route::post('order', ['as' => 'admin.quotationaccount.store',            'uses' => 'SalesAccountController@quotationstore']);

        Route::post('sales/{orderId}', ['as' => 'admin.salesaccount.update',           'uses' => 'SalesAccountController@invoiceUpdate']);
        Route::post('payment/{orderId}', ['as' => 'admin.paymentaccount.update',           'uses' => 'SalesAccountController@paymentUpdate']);
        Route::post('order/{orderId}', ['as' => 'admin.quotationaccount.update',           'uses' => 'SalesAccountController@quotationUpdate']);

        Route::get('invoice/show/{orderid}/{invoiceid}', ['as' => 'admin.salesaccount.show',           'uses' => 'SalesAccountController@invoiceShow']);

        Route::get('payment/show/{orderid}/{paymentid}', ['as' => 'admin.paymentaccount.show',           'uses' => 'SalesAccountController@paymentShow']);
        Route::get('quotation/show/{orderid}', ['as' => 'admin.quotationaccount.show',           'uses' => 'SalesAccountController@quotationShow']);

        Route::get('sales/print/{orderid}/{invoiceid}', ['as' => 'admin.salesaccount.print',  'uses' => 'SalesAccountController@printInvoice']);
        Route::get('payment/print/{id}', ['as' => 'admin.paymentaccount.print',  'uses' => 'SalesAccountController@printPayment']);
        Route::get('quotation/print/{id}', ['as' => 'admin.quotationaccount.print',  'uses' => 'SalesAccountController@printQuotation']);

        Route::get('sales/generatePDF/{orderid}/{invoiceid}', ['as' => 'admin.salesaccount.generatePDF',   'uses' => 'SalesAccountController@generatePDFInvoice']);
        Route::get('payment/generatePDF/{id}', ['as' => 'admin.paymentaccount.generatePDF',   'uses' => 'SalesAccountController@generatePDFPayment']);
        Route::get('quotation/generatePDF/{id}', ['as' => 'admin.quotationaccount.generatePDF',   'uses' => 'SalesAccountController@generatePDFQuotation']);

        // Orders routes
        // Orders routes
        Route::get('merge_order/{id}',['as'=>'admin.orders.merge_order','uses'=>'OrdersController@mergeTable']);

        Route::post('merge_order/{id}',['as'=>'admin.orders.merge_orderpo','uses'=>'OrdersController@mergeTablePost']);

        Route::get('split_bill/{id}',['as'=>'admin.orders.split_bill','uses'=>'OrdersController@splitBill']);
        Route::post('split_bill/{id}',['as'=>'admin.orders.split_bill','uses'=>'OrdersController@splitBillSave']);

        Route::get('merge_order_room/{id}',['as'=>'admin.orders.move_to_room','uses'=>'OrdersController@moveToRoom']);

        Route::post('merge_order_room/{id}',['as'=>'admin.orders.move_to_roompos','uses'=>'OrdersController@moveToRoomPost']);

        Route::get('merge_order_folio/{id}',['as'=>'admin.hotel.merge_folio','uses'=>'Hotel\FolioController@merge']);

        Route::post('merge_order_folio/{id}',['as'=>'admin.hotel.merge_foliopo','uses'=>'Hotel\FolioController@mergePost']);

        Route::get('split_order_folio/{id}',['as'=>'admin.hotel.split_folio','uses'=>'Hotel\FolioController@split']);

        Route::post('split_order_folio/{id}',['as'=>'admin.hotel.split_folio','uses'=>'Hotel\FolioController@splitPost']);


        Route::post('orders/enableSelected',          ['as' => 'admin.orders.enable-selected',  'uses' => 'OrdersController@enableSelected']);
        Route::post('orders/disableSelected',         ['as' => 'admin.orders.disable-selected', 'uses' => 'OrdersController@disableSelected']);
        Route::get('orders/search',                  ['as' => 'admin.orders.search',           'uses' => 'OrdersController@searchByName']);
        Route::post('orders/getInfo',                 ['as' => 'admin.orders.get-info',         'uses' => 'OrdersController@getInfo']);
        Route::post('orders',                         ['as' => 'admin.orders.store',            'uses' => 'OrdersController@store']);

        Route::get('orders/outlet/{id}',                         ['as' => 'admin.orders.index',            'uses' => 'OrdersController@index']);


        Route::get('orders/alloutlet',                         ['as' => 'admin.orders.alloutlet',            'uses' => 'OrdersController@allPOSSales']);

        Route::get('orders/abbr',                         ['as' => 'admin.orders.alloutletabbr',            'uses' => 'OrdersController@allPOSSalesAbbr']);

        Route::get('orders/returnpos',                         ['as' => 'admin.orders.returnpos',            'uses' => 'OrdersController@returnpos']);

        Route::get('orders/returnpos/list',                         ['as' => 'admin.orders.returnpos.list',            'uses' => 'OrdersController@returnposlist']);


        Route::get('orders/create',                  ['as' => 'admin.orders.create',           'uses' => 'OrdersController@create']);
        Route::get('orders/{orderId}',                ['as' => 'admin.orders.show',             'uses' => 'OrdersController@show']);
        Route::patch('order/{orderId}',                ['as' => 'admin.orders.patch',            'uses' => 'OrdersController@update']);
        Route::put('orders/{orderId}',                ['as' => 'admin.orders.update',           'uses' => 'OrdersController@update']);
        Route::delete('order/{orderId}',                ['as' => 'admin.orders.destroy',          'uses' => 'OrdersController@destroy']);
        Route::get('orders/{orderId}/edit',           ['as' => 'admin.orders.edit',             'uses' => 'OrdersController@edit']);
        Route::get('orders/{orderId}/confirm-delete', ['as' => 'admin.orders.confirm-delete',   'uses' => 'OrdersController@getModalDelete']);
        Route::get('orders/{orderId}/delete',         ['as' => 'admin.orders.delete',           'uses' => 'OrdersController@destroy']);



        Route::post('orders/makevoid/{id}', ['as' => 'admin.orders.makevoid', 'uses' => 'OrdersController@voidfromedit']);
        Route::get('orders/void/confirm/{id}', ['as' => 'admin.orders.void.confirm', 'uses' => 'OrdersController@voidConfirm']);



        // For Orders

        Route::post('products/GetProductDetailAjax/{productId}', ['uses' => 'OrdersController@getProductDetailAjax', 'as' => 'admin.products.GetProductDetailAjax']);

        Route::post('multiple_orders', ['uses' => 'OrdersController@store', 'as' => 'admin.multiple_orders.store']);
        Route::get('order/print/{id}', ['as' => 'admin.orders.print',  'uses' => 'OrdersController@printInvoice']);
        Route::get('order/generatePDF/{id}',   ['as' => 'admin.order.generatePDF',   'uses' => 'OrdersController@generatePDF']);

        Route::post('ajax_order_status', ['as' => 'admin.ajax_order_status', 'uses' => 'PurchaseSalePaymentController@ajaxSaleStatus']);



        Route::get('order/thermalprint/{id}',   ['as' => 'admin.order.thermalprint',   'uses' => 'OrdersController@thermalprint']);

        Route::get('order/botprint/{id}',   ['as' => 'admin.order.botprint',   'uses' => 'OrdersController@botprint']);

        Route::get('order/kitchenprint/{id}',   ['as' => 'admin.order.kitchenprint',   'uses' => 'OrdersController@kitchenprint']);

        Route::get('order/estimateprint/{id}',   ['as' => 'admin.order.estimateprint',   'uses' => 'OrdersController@estimateprint']);


        Route::get('users/ajax/getProductcategory',      ['as' => 'admin.users.ajaxgetProductcategory',      'uses' => 'OrdersController@ajaxGetProductcategory']);
        Route::get('outlet/ajax/getMenu',      ['as' => 'admin.outlet.ajax.getMenu',      'uses' => 'ProductController@ajaxGetMenu']);

        Route::get('users/ajax/getProduct',      ['as' => 'admin.users.getProduct',      'uses' => 'OrdersController@getProduct']);


        /// Pos list

        Route::get('orders/allpos/invoice', ['as' => 'admin.orders.allpos.invoice',  'uses' => 'OrdersController@allPosInvoice']);
        Route::get('orders/todaypos/invoice', ['as' => 'admin.orders.todaypos.invoice',  'uses' => 'OrdersController@todayPosInvoice']);


        Route::get('orders/allpos/payments', ['as' => 'admin.orders.allpos.payments',  'uses' => 'OrdersController@allPosPayment']);
        Route::get('orders/hotel/payments', ['as' => 'admin.orders.hotel.payments',  'uses' => 'Hotel\PaymentFolioController@allFolioPayment']);

        Route::get('orders/todaypos/payments', ['as' => 'admin.orders.todaypos.payments',  'uses' => 'OrdersController@todayPosPayment']);

        Route::get('/hotel/nightaudit-settlement/resturantbills/listing', ['as' => 'admin.hotel.nightaudit-settlement.resturantbills.listing', 'uses' => 'Hotel\NightAuditController@settlementResturantBillsListing']);


        Route::get('/purchase-book', ['as' => 'admin.purchase-book', 'uses' => 'PurchaseController@PurchaseBook']);

        Route::get('/purchase-book-bymonth/{month}', ['as' => 'admin.purchase-book-bymonth', 'uses' => 'PurchaseController@PurchaseBookByMonths']);
        Route::get('/sales-book-bymonth/{month}', ['as' => 'admin.sales-book-bymonth', 'uses' => 'Hotel\FolioController@SalesBookByMonths']);
        Route::get('purchase/sendmail/{id}', ['as' => 'purchase.sendmail', 'uses' => 'PurchaseController@sendMail']);
        //

        Route::get('pos/orders/selectoutlet/forstatus', ['as' => 'admin.pos.orders.selectoutlet.forstatus', 'uses' => 'OrdersController@OutletForStatus']);
        Route::get('pos/orders/byoutlet/status', ['as' => 'admin.pos.orders.byoutlet.status', 'uses' => 'OrdersController@ordersByStatus']);



        Route::get('edm_print_post/{id}',['as'=>'admin.order.edm_print_post','uses'=>'OrdersController@edmPrint']);

        Route::get('edm/order',['as'=>'admin.order.edm_list','uses'=>'OrdersController@edmlist']);

        //for sales payment

        Route::get('payment/orders/{id}', ['as' => 'admin.payment.orders.list',     'uses' => 'PurchaseSalePaymentController@SalePaymentlist']);

        Route::get('payment/orders/{id}/create', ['as' => 'admin.payment.orders.create',    'uses' => 'PurchaseSalePaymentController@SalePaymentcreate']);

        Route::post('payment/orders/{id}', ['as' => 'admin.payment.orders.store',    'uses' => 'PurchaseSalePaymentController@SalePaymentpost']);

        Route::post('postabbrtotax', ['as' => 'admin.payment.orders.postabbrtotax',    'uses' => 'PurchaseSalePaymentController@postAbbrToTax']);

        Route::get('payment/orders/{id}/edit/{paymentid}', ['as' => 'admin.payment.orders.edit',    'uses' => 'PurchaseSalePaymentController@SalePaymentedit']);

        Route::post('payment/orders/{id}/{payment_id}', ['as' => 'admin.payment.orders.update',    'uses' => 'PurchaseSalePaymentController@SalePaymentUpdate']);

        Route::get('payment/orders/{id}/confirm-delete', ['as' => 'admin.payment.orders.confirm-delete',   'uses' => 'PurchaseSalePaymentController@getModalDeleteSalePayment']);

        Route::get('payment/orders/{id}/delete', ['as' => 'admin.payment.orders.delete',           'uses' => 'PurchaseSalePaymentController@destroySalePayment']);


        //  pos reports

        Route::get('posreport/sales', ['as' => 'admin.pos.report', 'uses' => 'Hotel\PosReportController@daily']);

        Route::post('posreport/sales', ['as' => 'admin.pos.reportdasy', 'uses' => 'Hotel\PosReportController@dailyreport']);



        Route::get('posreport/products', ['as' => 'admin.pos.products.report', 'uses' => 'Hotel\PosReportController@products']);

        Route::post('posreport/products', ['as' => 'admin.pos.products.reportpro', 'uses' => 'Hotel\PosReportController@productsreport']);



        // pos sales return

        Route::get('possales/return', ['as' => 'admin.possales.return', 'uses' => 'Hotel\PosReportController@possalesreturn']);
        Route::post('possales/return', ['as' => 'admin.possales.return.post', 'uses' => 'Hotel\PosReportController@possalesreturnpost']);

        Route::get('possales/getbillinfo',['as'=>'possales.getbillinfo','uses'=>'OrdersController@getbillinfo']);
        Route::get('possales/getbillinfowithcn',['as'=>'possales.getbillinfowithcn','uses'=>'OrdersController@getbillinfoWithCN']);

        Route::get('credit_note/orders/show/{ordId}', ['as' => 'admin.credit_note.show', 'uses' => 'OrdersController@showcreditnote']);
        Route::get('credit_note/orders/print/{ordId}/{type}', ['as' => 'admin.credit_note.print', 'uses' => 'OrdersController@printcreditnote']);
        // materalized view

        Route::get('posreport/materalize', ['as' => 'admin.pos.materalize', 'uses' => 'Hotel\PosReportController@materializeview']);
        Route::get('posreport/materalize/result', ['as' => 'admin.pos.materalize.result', 'uses' => 'Hotel\PosReportController@materializeviewresult']);


        // ird posting index of pos

        Route::get('pos/irdposting/index', ['as' => 'admin.pos.irdposting.index', 'uses' => 'Hotel\PosReportController@posIrdIndex']);

        // ird posting index of hotel
        Route::get('hotel/irdposting/index', ['as' => 'admin.hotel.irdposting.indexin', 'uses' => 'Hotel\PosReportController@hotelIrdIndex']);


        //Hotel IRD Sync

        Route::get('hotel/nepalirdsync/hotelModal/{id}', ['as' => 'admin.nepal.ird.hotel.sync.modal', 'uses' => 'Hotel\NightAuditController@nepalIrdHotelSyncModal']);

        Route::get('hotel/nepalirdsync/hotel/{id}', ['as' => 'admin.nepal.ird.hotel.sync', 'uses' => 'Hotel\NightAuditController@nepalIrdHotelSync']);


        Route::get('hotelsales/return', ['as' => 'admin.hotelsales.return', 'uses' => 'Hotel\PosReportController@hotelsalesreturn']);

        Route::post('hotelsales/return', ['as' => 'admin.hotelsales.return.post', 'uses' => 'Hotel\PosReportController@hotelsalesreturnpost']);


        //hotel sales return book

        Route::get('hotelsales/return/book',  ['as' => 'admin.hotel.return.invoice', 'uses' => 'Hotel\FolioController@returnInvoiceBook']);

        Route::post('hotelsales/return/booklist',  ['as' => 'admin.hotel.return.invoice.list', 'uses' => 'Hotel\FolioController@returnInvoiceBookList']);

        Route::get('invoicereport/materalize', ['as' => 'admin.invoice.materalize', 'uses' => 'Hotel\PosReportController@hotelmaterializeview']);

        Route::post('invoicereport/materalize', ['as' => 'admin.invoice.materalizerez', 'uses' => 'Hotel\PosReportController@hotelmaterializeviewresult']);

        Route::get('hotelsales/credit_note/show/{invId}', ['as' => 'admin.hotel.sales.credit_note', 'uses' => 'Hotel\FolioController@hotelcreditnote']);


        // pos bill return history

        Route::get('orders/history/{orderId}', ['as' => 'admin.orders.ordershistroy', 'uses' => 'OrdersController@record_histroy_index']);

        Route::get('orders/history/show/{orderId}', ['as' => 'admin.orders.show.ordershistroy', 'uses' => 'OrdersController@record_histroy_show']);

        // For Invoice

        Route::get('invoice1', ['as' => 'admin.invoice.index', 'uses' => 'InvoiceController@index']);
        Route::get('invoice/create', ['as' => 'admin.invoice.create', 'uses' => 'InvoiceController@create']);

        Route::post('invoice/create', ['as' => 'admin.invoice.store', 'uses' => 'InvoiceController@store']);

        Route::get('invoice1/{id}', ['as' => 'admin.invoice.show', 'uses' => 'InvoiceController@show']);

        Route::get('invoice/void/{id}', ['as' => 'admin.salesaccount.void', 'uses' => 'InvoiceController@invoiceVoid']);

        Route::post('invoice/void/{id}', ['as' => 'admin.salesaccount.mkvoid', 'uses' => 'InvoiceController@MakeVoid']);
        Route::get('invoice/{orderId}/confirm-invoice', ['as' => 'admin.invoice.confirm-invoice',   'uses' => 'InvoiceController@getModalConverttoInvoice']);

        Route::post('invoice/{id}', ['as' => 'admin.invoice.storetoin',            'uses' => 'InvoiceController@postOrdertoInvoice']);

        Route::get('invoice/{id}/change', ['as' => 'admin.invoice.change', 'uses' => 'InvoiceController@postOrdertoInvoice']);
        Route::get('invoice/print/{id}', ['as' => 'admin.invoice.print',  'uses' => 'InvoiceController@printInvoice']);
        Route::get('invoice/generatePDF/{id}', ['as' => 'admin.invoice.generatePDF',   'uses' => 'InvoiceController@generatePDF']);
        Route::get('invoice/payment/{id}', ['as' => 'admin.invoice.payment',   'uses' => 'InvoiceController@makepayment']);
        Route::get('payment/invoice/{id}/create', ['as' => 'admin.payment.invoice.create',    'uses' => 'InvoiceController@invoicePaymentcreate']);
        Route::post('payment/invoice/{id}/create', ['as' => 'admin.payment.invoice.createpo',    'uses' => 'InvoiceController@InvoicePaymentPost']);
        Route::get('payment/invoice/{id}/show', ['as' => 'admin.payment.invoice.show',    'uses' => 'InvoiceController@invoicePaymentshow']);

        Route::get('taxinvoice/renewals', ['as' => 'admin.tax-invoice.renewals',           'uses' => 'InvoiceController@renewals']);

        // For Purchase

        Route::get('purchase', ['as' => 'admin.purchase.index',            'uses' => 'PurchaseController@index']);
        Route::get('purchase/create', ['as' => 'admin.purchase.create',           'uses' => 'PurchaseController@create']);
        Route::post('purchase', ['as' => 'admin.purchase.store',            'uses' => 'PurchaseController@store']);
        Route::get('purchase/{orderId}/edit', ['as' => 'admin.purchase.edit',             'uses' => 'PurchaseController@edit']);
        Route::post('purchase/reference-validation', 'PurchaseController@referenceValidation');

        Route::post('ajax_purchase_status', ['as' => 'admin.ajax_purchase_status', 'uses' => 'PurchaseSalePaymentController@ajaxPurchaseStatus']);

        Route::get('purchase/paymentslist', ['as' => 'admin.sales.paymentslist',  'uses' => 'PurchaseController@PurchasePayment']);

        //for purchase payment





        Route::get('cash_in_out',['as'=>'admin.cash_in_out','uses'=>'CashInOutController@cash']);

        Route::get('payment/purchase/{id}', ['as' => 'admin.payment.purchase.list',     'uses' => 'PurchaseSalePaymentController@PurchasePaymentlist']);
        Route::get('payment/purchase/{id}/create', ['as' => 'admin.payment.purchase.create',    'uses' => 'PurchaseSalePaymentController@PurchasePaymentcreate']);
        Route::post('payment/purchase/{id}', ['as' => 'admin.payment.purchase.store',    'uses' => 'PurchaseSalePaymentController@PurchasePaymentpost']);
        Route::get('payment/purchase/{id}/edit/{paymentid}', ['as' => 'admin.payment.purchase.edit',    'uses' => 'PurchaseSalePaymentController@PurchasePaymentedit']);
        Route::post('payment/purchase/{id}/{payment_id}', ['as' => 'admin.payment.purchase.update',    'uses' => 'PurchaseSalePaymentController@PurchasePaymentUpdate']);
        Route::get('payment/purchase/{id}/confirm-delete', ['as' => 'admin.payment.purchase.confirm-delete',   'uses' => 'PurchaseSalePaymentController@getModalDelete']);
        Route::get('payment/purchase/{id}/delete', ['as' => 'admin.payment.purchase.orders.delete',           'uses' => 'PurchaseSalePaymentController@destroy']);


        // Folio
        Route::get('folio/index',  ['as' => 'admin.hotel.todayfolio.index', 'uses' => 'Hotel\FolioController@billingindex']);

        Route::get('reservation/folio/{id}/index',  ['as' => 'admin.hotel.folio.index', 'uses' => 'Hotel\FolioController@index']);

        Route::get('folio/{id}/create',  ['as' => 'admin.hotel.folio.create', 'uses' => 'Hotel\FolioController@create']);
        Route::post('folio/{id}',  ['as' => 'admin.hotel.folio.store', 'uses' => 'Hotel\FolioController@store']);
        Route::get('folio/{id}/edit/{folio_id}',  ['as' => 'admin.hotel.folio.edit', 'uses' => 'Hotel\FolioController@edit']);
        Route::post('folio/{id}/update/{folio_id}',  ['as' => 'admin.hotel.folio.update', 'uses' => 'Hotel\FolioController@update']);
        Route::get('folio/{id}/confirm-delete',  ['as' => 'admin.hotel.folio.confirm-delete',   'uses' =>
        'Hotel\FolioController@getModalDelete']);
        Route::get('folio/{id}/delete',      ['as' => 'admin.hotel.folio.orders.delete',           'uses' => 'Hotel\FolioController@destroy']);

        Route::get('folio/allfoliosales',  ['as' => 'admin.hotel.allfoliosales', 'uses' => 'Hotel\FolioController@allFolioSales']);



        Route::post('folio/makevoid/{id}', ['as' => 'admin.folio.makevoid', 'uses' => 'Hotel\FolioController@voidfromedit']);
        Route::get('folio/void/confirm/{id}', ['as' => 'admin.folio.void.confirm', 'uses' => 'Hotel\FolioController@voidConfirm']);

        Route::post('hotel/makevoid/groups/{id}', ['as' => 'admin.folio.makevoid.groups', 'uses' => 'Hotel\GroupReservationController@GroupVoidFolio']);
        Route::get('hotel/void/groups/confirm/{id}', ['as' => 'admin.folio.void..groupsconfirm', 'uses' => 'Hotel\GroupReservationController@GroupVoidFolioConfirm']);

        Route::get('hotel/invoice/type/select/{id}', ['as' => 'admin.invoice.type.select', 'uses' => 'Hotel\FolioController@InvoiceTypeModal']);

        Route::post('hotel/invoice/type/select/{id}', ['as' => 'admin.invoice.type.select.post', 'uses' => 'Hotel\FolioController@InvoiceTypePost']);

        Route::get('hotel/invoice/print_estimate/{id}', ['as' => 'admin.invoice.print_estimate', 'uses' => 'Hotel\FolioController@estimatePrint']);


        Route::get('hotel/invoice/print/book', ['as' => 'admin.hotel.invoice.print.book', 'uses' => 'InvoiceController@HotelInvoicePrintBook']);



        // folio Print And Pdf


        Route::get('folio/print/{id}', ['as' => 'admin.folio.print',  'uses' => 'Hotel\FolioController@printInvoice']);

        Route::get('folio/taxprint/{id}', ['as' => 'admin.folio.taxprint',  'uses' => 'Hotel\FolioController@printTaxInvoice']);

        Route::get('folio/generatePDF/{id}',   ['as' => 'admin.folio.generatePDF',   'uses' => 'Hotel\FolioController@generatePDF']);



        // Folio Payment

        Route::get('payment/folio/{id}/index',  ['as' => 'admin.hotel.payment.folio.index', 'uses' => 'Hotel\PaymentFolioController@index']);

        Route::get('payment/folio/{id}/create',  ['as' => 'admin.hotel.payment.folio.create', 'uses' => 'Hotel\PaymentFolioController@create']);

        Route::post('payment/folio/{id}',  ['as' => 'admin.hotel.payment.folio.store', 'uses' => 'Hotel\PaymentFolioController@store']);

        Route::get('payment/folio/{id}/edit/{payment_id}',  ['as' => 'admin.hotel.payment.folio.edit', 'uses' => 'Hotel\PaymentFolioController@edit']);

        Route::post('payment/folio/{id}/update/{folio_id}', ['as' => 'admin.hotel.payment.folio.update', 'uses' => 'Hotel\PaymentFolioController@update']);

        Route::get('payment/folio/{id}/confirm-delete',  ['as' => 'admin.hotel.payment.folio.confirm-delete',   'uses' =>
        'Hotel\PaymentFolioController@getModalDelete']);

        Route::get('payment/folio/{id}/delete',      ['as' => 'admin.hotel.payment.folio.delete', 'uses' => 'Hotel\PaymentFolioController@destroy']);

        //Payment Reservation Deposit


        Route::get('payment/reservation/{id}/deposit',  ['as' => 'admin.hotel.payment.reservation.deposit', 'uses' => 'Hotel\PaymentFolioController@depositCreate']);
        Route::post('payment/reservation/{id}/deposit',  ['as' => 'admin.hotel.payment.reservation.deposit.store', 'uses' => 'Hotel\PaymentFolioController@depositStore']);

        Route::get('reservation/deposit/receipt/{payment_id}',['as'=>'admin.hotel.receipt.print','uses'=>'Hotel\PaymentFolioController@printReceipt']);

        // payment list reservation

        Route::get('payment/reservation/{id}/index',  ['as' => 'admin.hotel.payment.reservation.index', 'uses' => 'Hotel\PaymentFolioController@reservationList']);

        // generate invoice folio

        Route::get('hotel/reservation/folio/{id}/generate/invoice', ['as' => 'admin.reservation.folio.generate.invoice', 'uses' => 'InvoiceController@ConvertFoliotoInvoice']);

        Route::get('hotel/reservation/folio/{id}/generate/final/invoice', ['as' => 'admin.reservation.folio.generate.final.invoice', 'uses' => 'OrdersController@ConvertFoliotoFinalInvoice']);

        // Knowledge routes
        Route::post('knowledge/enableSelected', ['as' => 'admin.knowledge.enable-selected',  'uses' => 'KnowledgeController@enableSelected']);
        Route::post('knowledge/disableSelected', ['as' => 'admin.knowledge.disable-selected', 'uses' => 'KnowledgeController@disableSelected']);
        Route::get('knowledge/search', ['as' => 'admin.knowledge.search',           'uses' => 'KnowledgeController@searchByName']);
        Route::post('knowledge/getInfo', ['as' => 'admin.knowledge.get-info',         'uses' => 'KnowledgeController@getInfo']);
        Route::post('knowledge', ['as' => 'admin.knowledge.store',            'uses' => 'KnowledgeController@store']);

        Route::get('knowledge', ['as' => 'admin.knowledge.index',            'uses' => 'KnowledgeController@index']);
        Route::get('knowledge/cat/{cat_id}', ['as' => 'admin.knowledge.category',            'uses' => 'KnowledgeController@category']);

        Route::get('knowledge/create', ['as' => 'admin.knowledge.create',           'uses' => 'KnowledgeController@create']);
        Route::get('knowledge/{knowledgeId}', ['as' => 'admin.knowledge.show',             'uses' => 'KnowledgeController@show']);
        Route::patch('knowledge/{knowledgeId}', ['as' => 'admin.knowledge.patch',            'uses' => 'KnowledgeController@update']);
        Route::put('knowledge/{knowledgeId}', ['as' => 'admin.knowledge.update',           'uses' => 'KnowledgeController@update']);
        Route::delete('knowledge/{knowledgeId}', ['as' => 'admin.knowledge.destroy',          'uses' => 'KnowledgeController@destroy']);
        Route::get('knowledge/{knowledgeId}/edit', ['as' => 'admin.knowledge.edit',             'uses' => 'KnowledgeController@edit']);
        Route::get('knowledge/{knowledgeId}/confirm-delete', ['as' => 'admin.knowledge.confirm-delete',   'uses' => 'KnowledgeController@getModalDelete']);
        Route::get('knowledge/{knowledgeId}/delete', ['as' => 'admin.knowledge.delete',           'uses' => 'KnowledgeController@destroy']);
        Route::get('knowledge/{knowledgeId}/enable', ['as' => 'admin.knowledge.enable',           'uses' => 'KnowledgeController@enable']);
        Route::get('knowledge/{knowledgeId}/disable', ['as' => 'admin.knowledge.disable',          'uses' => 'KnowledgeController@disable']);

        //Transfer Leads
        Route::get('transfer_lead/{lead_id}', ['as' => 'admin.lead.transfer',           'uses' => 'LeadsController@transferLead']);
        Route::post('transfer_lead/post-transfer', ['as' => 'admin.lead.transfer-update',         'uses' => 'LeadsController@postTransferLead']);

        //Convert Leads
        Route::get('convert_lead/{lead_id}', ['as' => 'admin.lead.convert',           'uses' => 'LeadsController@convertLead']);
        Route::post('convert_lead/post-transfer', ['as' => 'admin.lead.convert-update',         'uses' => 'LeadsController@postConvertLead']);

        Route::get('convert_lead_clients/{lead_id}', ['as' => 'admin.lead.convert_lead_clients', 'uses' => 'LeadsController@convertToClients']);
        Route::get('convert_lead_clients_confirm/{lead_id}', ['as' => 'admin.lead.convert_lead_clients-confirm', 'uses' => 'LeadsController@ConfirmconvertToClients']);

        //Query Leads
        Route::get('lead_query_list/{lead_id}', ['as' => 'admin.lead.query_list',           'uses' => 'LeadsController@listingquery']);

        Route::get('lead_query/{lead_id}', ['as' => 'admin.lead.query',           'uses' => 'LeadsController@queryLead']);

        Route::post('lead_query/create/{lead_id}', ['as' => 'admin.lead.query-create',         'uses' => 'LeadsController@postQueryLead']);

        Route::get('lead_query/edit/{query_id}', ['as' => 'admin.lead.query_edit',           'uses' => 'LeadsController@queryLeadEdit']);

        Route::post('lead_query/update/{query_id}', ['as' => 'admin.lead.query-update',         'uses' => 'LeadsController@updateQueryLead']);

        Route::get('lead_query/confirm-delete/{query_id}', ['as' => 'admin.lead.query-confirm-delete',   'uses' => 'LeadsController@getModalQueryDelete']);

        Route::delete('lead_query/{query_id}', ['as' => 'admin.lead.query-destroy',          'uses' => 'LeadsController@querydestroy']);
        Route::get('lead_query/{query_id}/delete', ['as' => 'admin.lead.query-delete',           'uses' => 'LeadsController@querydestroy']);

        // Knowledge Category routes
        Route::post('knowledgecat/enableSelected', ['as' => 'admin.knowledgecat.enable-selected',  'uses' => 'KnowledgecatController@enableSelected']);
        Route::post('knowledgecat/disableSelected', ['as' => 'admin.knowledgecat.disable-selected', 'uses' => 'KnowledgecatController@disableSelected']);
        Route::get('knowledgecat/search', ['as' => 'admin.knowledgecat.search',           'uses' => 'KnowledgecatController@searchByName']);
        Route::post('knowledgecat/getInfo', ['as' => 'admin.knowledgecat.get-info',         'uses' => 'KnowledgecatController@getInfo']);
        Route::post('knowledgecat', ['as' => 'admin.knowledgecat.store',            'uses' => 'KnowledgecatController@store']);

        Route::get('knowledgecat', ['as' => 'admin.knowledgecat.index',            'uses' => 'KnowledgecatController@index']);

        Route::get('knowledgecat/create', ['as' => 'admin.knowledgecat.create',           'uses' => 'KnowledgecatController@create']);
        Route::get('knowledgecat/{knowledgeCatId}', ['as' => 'admin.knowledgecat.show',             'uses' => 'KnowledgecatController@show']);
        Route::patch('knowledgecat/{knowledgeCatId}', ['as' => 'admin.knowledgecat.patch',            'uses' => 'KnowledgecatController@update']);
        Route::put('knowledgecat/{knowledgeCatId}', ['as' => 'admin.knowledgecat.update',           'uses' => 'KnowledgecatController@update']);
        Route::delete('knowledgecat/{knowledgeCatId}', ['as' => 'admin.knowledgecat.destroy',          'uses' => 'KnowledgecatController@destroy']);
        Route::get('knowledgecat/{knowledgeCatId}/edit', ['as' => 'admin.knowledgecat.edit',             'uses' => 'KnowledgecatController@edit']);
        Route::get('knowledgecat/{knowledgeCatId}/confirm-delete', ['as' => 'admin.knowledgecat.confirm-delete',   'uses' => 'KnowledgecatController@getModalDelete']);
        Route::get('knowledgecat/{knowledgeCatId}/delete', ['as' => 'admin.knowledgecat.delete',           'uses' => 'KnowledgecatController@destroy']);
        Route::get('knowledgecat/{knowledgeCatId}/enable', ['as' => 'admin.knowledgecat.enable',           'uses' => 'KnowledgecatController@enable']);
        Route::get('knowledgecat/{knowledgeCatId}/disable', ['as' => 'admin.knowledgecat.disable',          'uses' => 'KnowledgecatController@disable']);

        // communication routes
        Route::post('communication/enableSelected', ['as' => 'admin.communication.enable-selected',  'uses' => 'CommunicationController@enableSelected']);
        Route::post('communication/disableSelected', ['as' => 'admin.communication.disable-selected', 'uses' => 'CommunicationController@disableSelected']);
        Route::get('communication/search', ['as' => 'admin.communication.search',           'uses' => 'CommunicationController@searchByName']);
        Route::post('communication/getInfo', ['as' => 'admin.communication.get-info',         'uses' => 'CommunicationController@getInfo']);
        Route::post('communication', ['as' => 'admin.communication.store',            'uses' => 'CommunicationController@store']);
        Route::get('communication', ['as' => 'admin.communication.index',            'uses' => 'CommunicationController@index']);
        Route::get('communication/create', ['as' => 'admin.communication.create',           'uses' => 'CommunicationController@create']);
        Route::get('communication/{communicationId}', ['as' => 'admin.communication.show',             'uses' => 'CommunicationController@show']);
        Route::patch('communication/{communicationId}', ['as' => 'admin.communication.patch',            'uses' => 'CommunicationController@update']);
        Route::put('communication/{communicationId}', ['as' => 'admin.communication.update',           'uses' => 'CommunicationController@update']);
        Route::delete('communication/{communicationId}', ['as' => 'admin.communication.destroy',          'uses' => 'CommunicationController@destroy']);
        Route::get('communication/{communicationId}/edit', ['as' => 'admin.communication.edit',             'uses' => 'CommunicationController@edit']);
        Route::get('communication/{communicationId}/confirm-delete', ['as' => 'admin.communication.confirm-delete',   'uses' => 'CommunicationController@getModalDelete']);
        Route::get('communication/{communicationId}/delete', ['as' => 'admin.communication.delete',           'uses' => 'CommunicationController@destroy']);
        Route::get('communication/{communicationId}/enable', ['as' => 'admin.communication.enable',           'uses' => 'CommunicationController@enable']);
        Route::get('communication/{communicationId}/disable', ['as' => 'admin.communication.disable',          'uses' => 'CommunicationController@disable']);

        // organization routes
        Route::post('organization/enableSelected', ['as' => 'admin.organization.enable-selected',  'uses' => 'OrganizationController@enableSelected']);
        Route::post('organization/disableSelected', ['as' => 'admin.organization.disable-selected', 'uses' => 'OrganizationController@disableSelected']);
        Route::get('organization/search', ['as' => 'admin.organization.search',           'uses' => 'OrganizationController@searchByName']);
        Route::post('organization/getInfo', ['as' => 'admin.organization.get-info',         'uses' => 'OrganizationController@getInfo']);
        Route::post('organization', ['as' => 'admin.organization.store',            'uses' => 'OrganizationController@store']);
        Route::get('organization', ['as' => 'admin.organization.index',            'uses' => 'OrganizationController@index']);
        Route::get('organization/create', ['as' => 'admin.organization.create',           'uses' => 'OrganizationController@create']);
        Route::get('organization/{organizationId}', ['as' => 'admin.organization.show',             'uses' => 'OrganizationController@show']);
        Route::patch('organization/{organizationId}', ['as' => 'admin.organization.patch',            'uses' => 'OrganizationController@update']);
        Route::put('organization/{organizationId}', ['as' => 'admin.organization.update',           'uses' => 'OrganizationController@update']);
        Route::delete('organization/{organizationId}', ['as' => 'admin.organization.destroy',          'uses' => 'OrganizationController@destroy']);
        Route::get('organization/{organizationId}/edit', ['as' => 'admin.organization.edit',             'uses' => 'OrganizationController@edit']);
        Route::get('organization/{organizationId}/confirm-delete', ['as' => 'admin.organization.confirm-delete',   'uses' => 'OrganizationController@getModalDelete']);
        Route::get('organization/{organizationId}/delete', ['as' => 'admin.organization.delete',           'uses' => 'OrganizationController@destroy']);
        Route::get('organization/{organizationId}/enable', ['as' => 'admin.organization.enable',           'uses' => 'OrganizationController@enable']);
        Route::get('organization/{organizationId}/disable', ['as' => 'admin.organization.disable',          'uses' => 'OrganizationController@disable']);

        Route::post('/ajax_org', ['as' => 'admin.organization.ajaxorg',   'uses' => 'OrganizationController@ajaxUpdateOrgDropdown']);

        // LeadTypes routes
        Route::post('leadtypes/enableSelected', ['as' => 'admin.leadtypes.enable-selected',  'uses' => 'LeadTypesController@enableSelected']);
        Route::post('leadtypes/disableSelected', ['as' => 'admin.leadtypes.disable-selected', 'uses' => 'LeadTypesController@disableSelected']);
        Route::get('leadtypes/search', ['as' => 'admin.leadtypes.search',           'uses' => 'LeadTypesController@searchByName']);
        Route::post('leadtypes/getInfo', ['as' => 'admin.leadtypes.get-info',         'uses' => 'LeadTypesController@getInfo']);
        Route::post('leadtypes', ['as' => 'admin.leadtypes.store',            'uses' => 'LeadTypesController@store']);
        Route::get('leadtypes', ['as' => 'admin.leadtypes.index',            'uses' => 'LeadTypesController@index']);
        Route::get('leadtypes/create', ['as' => 'admin.leadtypes.create',           'uses' => 'LeadTypesController@create']);
        Route::get('leadtypes/{leadtypeId}', ['as' => 'admin.leadtypes.show',             'uses' => 'LeadTypesController@show']);
        Route::patch('leadtypes/{leadtypeId}', ['as' => 'admin.leadtypes.patch',            'uses' => 'LeadTypesController@update']);
        Route::put('leadtypes/{leadtypeId}', ['as' => 'admin.leadtypes.update',           'uses' => 'LeadTypesController@update']);
        Route::delete('leadtypes/{leadtypeId}', ['as' => 'admin.leadtypes.destroy',          'uses' => 'LeadTypesController@destroy']);
        Route::get('leadtypes/{leadtypeId}/edit', ['as' => 'admin.leadtypes.edit',             'uses' => 'LeadTypesController@edit']);
        Route::get('leadtypes/{leadtypeId}/confirm-delete', ['as' => 'admin.leadtypes.confirm-delete',   'uses' => 'LeadTypesController@getModalDelete']);
        Route::get('leadtypes/{leadtypeId}/delete', ['as' => 'admin.leadtypes.delete',           'uses' => 'LeadTypesController@destroy']);
        Route::get('leadtypes/{leadtypeId}/enable', ['as' => 'admin.leadtypes.enable',           'uses' => 'LeadTypesController@enable']);
        Route::get('leadtypes/{leadtypeId}/disable', ['as' => 'admin.leadtypes.disable',          'uses' => 'LeadTypesController@disable']);

        // Lead Notes Routes
        // Set the notes for the lead - Ajax
        Route::post('leadnotes', ['as' => 'admin.leadnotes.store',            'uses' => 'LeadNotesController@store']);
        Route::get('leadnotes/{leadnoteId}/confirm-delete', ['as' => 'admin.leadnotes.confirm-delete',   'uses' => 'LeadNotesController@getModalDelete']);
        Route::get('leadnotes/{leadnoteId}/delete', ['as' => 'admin.leadnotes.delete',           'uses' => 'LeadNotesController@destroy']);

        // Lead Files Routes
        // Set the file for the lead - Ajax
        Route::post('leadfiles', ['as' => 'admin.leadfiles.store',            'uses' => 'LeadFilesController@store']);
        Route::get('leadfiles/{leadfileId}/confirm-delete', ['as' => 'admin.leadfiles.confirm-delete',   'uses' => 'LeadFilesController@getModalDelete']);
        Route::get('leadfiles/{leadfileId}/delete', ['as' => 'admin.leadfiles.delete',           'uses' => 'LeadFilesController@destroy']);

        // LeadStatus routes
        Route::post('leadstatus/enableSelected', ['as' => 'admin.leadstatus.enable-selected',  'uses' => 'LeadStatusController@enableSelected']);
        Route::post('leadstatus/disableSelected', ['as' => 'admin.leadstatus.disable-selected', 'uses' => 'LeadStatusController@disableSelected']);
        Route::get('leadstatus/search', ['as' => 'admin.leadstatus.search',           'uses' => 'LeadStatusController@searchByName']);
        Route::post('leadstatus/getInfo', ['as' => 'admin.leadstatus.get-info',         'uses' => 'LeadStatusController@getInfo']);
        Route::post('leadstatus', ['as' => 'admin.leadstatus.store',            'uses' => 'LeadStatusController@store']);
        Route::get('leadstatus', ['as' => 'admin.leadstatus.index',            'uses' => 'LeadStatusController@index']);
        Route::get('leadstatus/create', ['as' => 'admin.leadstatus.create',           'uses' => 'LeadStatusController@create']);
        Route::get('leadstatus/{leadstatusId}', ['as' => 'admin.leadstatus.show',             'uses' => 'LeadStatusController@show']);
        Route::patch('leadstatus/{leadstatusId}', ['as' => 'admin.leadstatus.patch',            'uses' => 'LeadStatusController@update']);
        Route::put('leadstatus/{leadstatusId}', ['as' => 'admin.leadstatus.update',           'uses' => 'LeadStatusController@update']);
        Route::delete('leadstatus/{leadstatusId}', ['as' => 'admin.leadstatus.destroy',          'uses' => 'LeadStatusController@destroy']);
        Route::get('leadstatus/{leadstatusId}/edit', ['as' => 'admin.leadstatus.edit',             'uses' => 'LeadStatusController@edit']);
        Route::get('leadstatus/{leadstatusId}/confirm-delete', ['as' => 'admin.leadstatus.confirm-delete',   'uses' => 'LeadStatusController@getModalDelete']);
        Route::get('leadstatus/{leadstatusId}/delete', ['as' => 'admin.leadstatus.delete',           'uses' => 'LeadStatusController@destroy']);
        Route::get('leadstatus/{leadstatusId}/enable', ['as' => 'admin.leadstatus.enable',           'uses' => 'LeadStatusController@enable']);
        Route::get('leadstatus/{leadstatusId}/disable', ['as' => 'admin.leadstatus.disable',          'uses' => 'LeadStatusController@disable']);

        // Fiscalyear routes
        Route::post('fiscalyear/enableSelected', ['as' => 'admin.fiscalyear.enable-selected',  'uses' => 'FiscalyearController@enableSelected']);
        Route::post('fiscalyear/disableSelected', ['as' => 'admin.fiscalyear.disable-selected', 'uses' => 'FiscalyearController@disableSelected']);
        Route::get('fiscalyear/search', ['as' => 'admin.fiscalyear.search',           'uses' => 'FiscalyearController@searchByName']);
        Route::post('fiscalyear/getInfo', ['as' => 'admin.fiscalyear.get-info',         'uses' => 'FiscalyearController@getInfo']);
        Route::post('fiscalyear', ['as' => 'admin.fiscalyear.store',            'uses' => 'FiscalyearController@store']);
        Route::get('fiscalyear', ['as' => 'admin.fiscalyear.index',            'uses' => 'FiscalyearController@index']);
        Route::get('fiscalyear/create', ['as' => 'admin.fiscalyear.create',           'uses' => 'FiscalyearController@create']);
        Route::get('fiscalyear/{fiscalyearId}', ['as' => 'admin.fiscalyear.show',             'uses' => 'FiscalyearController@show']);
        Route::patch('fiscalyear/{fiscalyearId}', ['as' => 'admin.fiscalyear.patch',            'uses' => 'FiscalyearController@update']);
        Route::put('fiscalyear/{fiscalyearId}', ['as' => 'admin.fiscalyear.update',           'uses' => 'FiscalyearController@update']);
        Route::delete('fiscalyear/{fiscalyearId}', ['as' => 'admin.fiscalyear.destroy',          'uses' => 'FiscalyearController@destroy']);
        Route::get('fiscalyear/{fiscalyearId}/edit', ['as' => 'admin.fiscalyear.edit',             'uses' => 'FiscalyearController@edit']);
        Route::get('fiscalyear/{fiscalyearId}/confirm-delete', ['as' => 'admin.fiscalyear.confirm-delete',   'uses' => 'FiscalyearController@getModalDelete']);
        Route::get('fiscalyear/{fiscalyearId}/delete', ['as' => 'admin.fiscalyear.delete',           'uses' => 'FiscalyearController@destroy']);
        Route::get('fiscalyear/{fiscalyearId}/enable', ['as' => 'admin.fiscalyear.enable',           'uses' => 'FiscalyearController@enable']);
        Route::get('fiscalyear/{fiscalyearId}/disable', ['as' => 'admin.fiscalyear.disable',          'uses' => 'FiscalyearController@disable']);

        // Product Category routes
        Route::post('productcats/enableSelected', ['as' => 'admin.productcats.enable-selected',  'uses' => 'ProductCatsController@enableSelected']);
        Route::post('productcats/disableSelected', ['as' => 'admin.productcats.disable-selected', 'uses' => 'ProductCatsController@disableSelected']);
        Route::get('productcats/search', ['as' => 'admin.productcats.search',           'uses' => 'ProductCatsController@searchByName']);
        Route::post('productcats/getInfo', ['as' => 'admin.productcats.get-info',         'uses' => 'ProductCatsController@getInfo']);
        Route::post('productcats', ['as' => 'admin.productcats.store',            'uses' => 'ProductCatsController@store']);
        Route::get('productcats', ['as' => 'admin.productcats.index',            'uses' => 'ProductCatsController@index']);
        Route::get('productcats/create', ['as' => 'admin.productcats.create',           'uses' => 'ProductCatsController@create']);
        Route::get('productcats/{productcatsId}', ['as' => 'admin.productcats.show',             'uses' => 'ProductCatsController@show']);
        Route::patch('productcats/{productcatsId}', ['as' => 'admin.productcats.patch',            'uses' => 'ProductCatsController@update']);
        Route::put('productcats/{productcatsId}', ['as' => 'admin.productcats.update',           'uses' => 'ProductCatsController@update']);
        Route::delete('productcats/{productcatsId}', ['as' => 'admin.productcats.destroy',          'uses' => 'ProductCatsController@destroy']);
        Route::get('productcats/{productcatsId}/edit', ['as' => 'admin.productcats.edit',             'uses' => 'ProductCatsController@edit']);
        Route::get('productcats/{productcatsId}/confirm-delete', ['as' => 'admin.productcats.confirm-delete',   'uses' => 'ProductCatsController@getModalDelete']);
        Route::get('productcats/{productcatsId}/delete', ['as' => 'admin.productcats.delete',           'uses' => 'ProductCatsController@destroy']);
        Route::get('productcats/{productcatsId}/enable', ['as' => 'admin.productcats.enable',           'uses' => 'ProductCatsController@enable']);
        Route::get('productcats/{productcatsId}/disable', ['as' => 'admin.productcats.disable',          'uses' => 'ProductCatsController@disable']);

        // Product Type Master

        Route::post('producttypemaster/enableSelected',          ['as' => 'admin.producttypemaster.enable-selected',  'uses' => 'ProductTypeMasterController@enableSelected']);
        Route::post('producttypemaster/disableSelected',         ['as' => 'admin.producttypemaster.disable-selected', 'uses' => 'ProductTypeMasterController@disableSelected']);
        Route::get('producttypemaster/search',                  ['as' => 'admin.producttypemaster.search',           'uses' => 'ProductTypeMasterController@searchByName']);
        Route::post('producttypemaster/getInfo',                 ['as' => 'admin.producttypemaster.get-info',         'uses' => 'ProductTypeMasterController@getInfo']);
        Route::post('producttypemaster',                         ['as' => 'admin.producttypemaster.store',            'uses' => 'ProductTypeMasterController@store']);
        Route::get('producttypemaster',                         ['as' => 'admin.producttypemaster.index',            'uses' => 'ProductTypeMasterController@index']);
        Route::get('producttypemaster/create',                  ['as' => 'admin.producttypemaster.create',           'uses' => 'ProductTypeMasterController@create']);
        Route::get('producttypemaster/{Id}',                ['as' => 'admin.producttypemaster.show',             'uses' => 'ProductTypeMasterController@show']);
        Route::patch('producttypemaster/{Id}',                ['as' => 'admin.producttypemaster.patch',            'uses' => 'ProductTypeMasterController@update']);
        Route::put('producttypemaster/{Id}',                ['as' => 'admin.producttypemaster.update',           'uses' => 'ProductTypeMasterController@update']);
        Route::delete('producttypemaster/{Id}',                ['as' => 'admin.producttypemaster.destroy',          'uses' => 'ProductTypeMasterController@destroy']);
        Route::get('producttypemaster/{Id}/edit',           ['as' => 'admin.producttypemaster.edit',             'uses' => 'ProductTypeMasterController@edit']);
        Route::get('producttypemaster/{Id}/confirm-delete', ['as' => 'admin.producttypemaster.confirm-delete',   'uses' => 'ProductTypeMasterController@getModalDelete']);
        Route::get('producttypemaster/{Id}/delete',         ['as' => 'admin.producttypemaster.delete',           'uses' => 'ProductTypeMasterController@destroy']);
        Route::get('producttypemaster/{Id}/enable',         ['as' => 'admin.producttypemaster.enable',           'uses' => 'ProductTypeMasterController@enable']);
        Route::get('producttypemaster/{Id}/disable',        ['as' => 'admin.producttypemaster.disable',          'uses' => 'ProductTypeMasterController@disable']);




        // LeadEnquiryMode routes
        Route::post('leadenquirymode/enableSelected', ['as' => 'admin.leadenquirymode.enable-selected',  'uses' => 'LeadEnquiryModeController@enableSelected']);
        Route::post('leadenquirymode/disableSelected', ['as' => 'admin.leadenquirymode.disable-selected', 'uses' => 'LeadEnquiryModeController@disableSelected']);
        Route::get('leadenquirymode/search', ['as' => 'admin.leadenquirymode.search',           'uses' => 'LeadEnquiryModeController@searchByName']);
        Route::post('leadenquirymode/getInfo', ['as' => 'admin.leadenquirymode.get-info',         'uses' => 'LeadEnquiryModeController@getInfo']);
        Route::post('leadenquirymode', ['as' => 'admin.leadenquirymode.store',            'uses' => 'LeadEnquiryModeController@store']);
        Route::get('leadenquirymode', ['as' => 'admin.leadenquirymode.index',            'uses' => 'LeadEnquiryModeController@index']);
        Route::get('leadenquirymode/create', ['as' => 'admin.leadenquirymode.create',           'uses' => 'LeadEnquiryModeController@create']);
        Route::get('leadenquirymode/{leadenquirymodeId}', ['as' => 'admin.leadenquirymode.show',             'uses' => 'LeadEnquiryModeController@show']);
        Route::patch('leadenquirymode/{leadenquirymodeId}', ['as' => 'admin.leadenquirymode.patch',            'uses' => 'LeadEnquiryModeController@update']);
        Route::put('leadenquirymode/{leadenquirymodeId}', ['as' => 'admin.leadenquirymode.update',           'uses' => 'LeadEnquiryModeController@update']);
        Route::delete('leadenquirymode/{leadenquirymodeId}', ['as' => 'admin.leadenquirymode.destroy',          'uses' => 'LeadEnquiryModeController@destroy']);
        Route::get('leadenquirymode/{leadenquirymodeId}/edit', ['as' => 'admin.leadenquirymode.edit',             'uses' => 'LeadEnquiryModeController@edit']);
        Route::get('leadenquirymode/{leadenquirymodeId}/confirm-delete', ['as' => 'admin.leadenquirymode.confirm-delete',   'uses' => 'LeadEnquiryModeController@getModalDelete']);
        Route::get('leadenquirymode/{leadenquirymodeId}/delete', ['as' => 'admin.leadenquirymode.delete',           'uses' => 'LeadEnquiryModeController@destroy']);
        Route::get('leadenquirymode/{leadenquirymodeId}/enable', ['as' => 'admin.leadenquirymode.enable',           'uses' => 'LeadEnquiryModeController@enable']);
        Route::get('leadenquirymode/{leadenquirymodeId}/disable', ['as' => 'admin.leadenquirymode.disable',          'uses' => 'LeadEnquiryModeController@disable']);

        //purchase routes
        Route::get('expenses', ['as' => 'admin.expenses.index',            'uses' => 'ExpensesController@index']);
        Route::get('expenses/create', ['as' => 'admin.expenses.create',           'uses' => 'ExpensesController@create']);
        Route::get('expenses/{courseId}', ['as' => 'admin.expenses.show',             'uses' => 'ExpensesController@show']);
        Route::post('expenses', ['as' => 'admin.expenses.store',            'uses' => 'ExpensesController@store']);
        Route::get('expenses/{courseId}/edit', ['as' => 'admin.expenses.edit',             'uses' => 'ExpensesController@edit']);
        Route::patch('expenses/{courseId}', ['as' => 'admin.expenses.patch',            'uses' => 'ExpensesController@update']);
        Route::put('expenses/{courseId}', ['as' => 'admin.expenses.update',           'uses' => 'ExpensesController@update']);
        Route::get('expenses/{courseId}/confirm-delete', ['as' => 'admin.expenses.confirm-delete',   'uses' => 'ExpensesController@getModalDelete']);
        Route::get('expenses/{courseId}/delete', ['as' => 'admin.expenses.delete',           'uses' => 'ExpensesController@destroy']);
        Route::get('expenses/showModal/{courseId}', ['as' => 'admin.expenses.showModal',             'uses' => 'ExpensesController@showModal']);

        Route::get('purchase', ['as' => 'admin.purchase.index',            'uses' => 'PurchaseController@index']);
        Route::get('purchase/create', ['as' => 'admin.purchase.create',           'uses' => 'PurchaseController@create']);
        Route::post('purchase', ['as' => 'admin.purchase.store',            'uses' => 'PurchaseController@store']);
        Route::get('purchase/{orderId}', ['as' => 'admin.purchase.show',             'uses' => 'PurchaseController@show']);
        Route::get('purchase/{orderId}/edit', ['as' => 'admin.purchase.edit',             'uses' => 'PurchaseController@edit']);
        Route::patch('purchase/{orderId}', ['as' => 'admin.purchase.patch',            'uses' => 'PurchaseController@update']);
        Route::put('purchase/{orderId}', ['as' => 'admin.purchase.update',           'uses' => 'PurchaseController@update']);
        Route::get('purchase/{orderId}/confirm-delete', ['as' => 'admin.purchase.confirm-delete',   'uses' => 'PurchaseController@getModalDelete']);
        Route::get('purchase/{orderId}/delete', ['as' => 'admin.purchase.delete',           'uses' => 'PurchaseController@destroy']);
        Route::post('purchase/reference-validation', 'PurchaseController@referenceValidation');
        Route::get('purchase/print/{id}', ['as' => 'admin.purchase.print',  'uses' => 'PurchaseController@printInvoice']);
        Route::get('purchase/generatePDF/{id}', ['as' => 'admin.purchase.generatePDF',   'uses' => 'PurchaseController@generatePDF']);

        Route::get('event/{id}/pdf',['as'=>'admin.event.pdf','uses'=>'EventController@eventPDF']);
        // Attendance
        Route::get('attendance_report', ['as' => 'admin.attendance_report', 'uses' =>
        'AttendanceController@index']);
        Route::post('attendance_report', ['as' => 'admin.attendance_report_show', 'uses' => 'AttendanceController@show']);

        Route::get('attendance_report/print/{department}/{date_in}', ['as' => 'admin.attendance_report.print',  'uses' => 'AttendanceController@printAttendance']);
        Route::get('attendance_report/generatePDF/{department}/{date_in}', ['as' => 'admin.attendance_report.generatePDF',   'uses' => 'AttendanceController@generatePDF']);

        Route::get('time_history', ['as' => 'admin.time_history', 'uses' => 'AttendanceController@timeHistory']);
        Route::post('time_history', ['as' => 'admin.time_history_post', 'uses' => 'AttendanceController@timeHistoryPost']);
        Route::get('time_history/print/{user_id}/{date_in}', ['as' => 'admin.time_history.print',  'uses' => 'AttendanceController@printTimeHistory']);
        Route::get('time_history/generatePDF/{user_id}/{date_in}', ['as' => 'admin.time_history.generatePDFTimeHistory',   'uses' => 'AttendanceController@generatePDFTimeHistory']);

        Route::get('time_history/edit_time/{clock_id}', ['as' => 'admin.time_history.edit_time',   'uses' => 'AttendanceController@editTime']);
        Route::post('time_history/update_time/{clock_id}', ['as' => 'admin.time_history.updateTime',   'uses' => 'AttendanceController@updateTime']);

        Route::get('timechange_request', ['as' => 'admin.timechange_request',   'uses' => 'AttendanceController@timeChangeRequest']);

        Route::get('timechange_request_modal/{clock_history_id}', ['as' => 'admin.timechange_request_modal',   'uses' => 'AttendanceController@timeRequestModal']);
        Route::post('timechange_request/{clock_history_id}', ['as' => 'admin.timechange_request.update',   'uses' => 'AttendanceController@updateTimeChangeRequest']);

        Route::get('importexport/attendance_report', ['as' => 'admin.importexport.attendance_report', 'uses' => 'AttendanceController@importexport']);
        Route::post('importexport/attendance_report', ['as' => 'admin.importexport.attendance_reportsto', 'uses' => 'AttendanceController@importexportstore']);
        Route::get('downloadExcel/attendance/{type}', ['as' => 'admin.downloadExcel.attendance', 'uses' => 'AttendanceController@downloadexcel']);

        Route::get('all_attendance_report', ['as' => 'admin.all_attendance_report', 'uses' => 'AttendanceController@allattendanceindex']);
        Route::post('all_attendance_report', ['as' => 'admin.all_attendance_report_show', 'uses' => 'AttendanceController@allattendanceshow']);

        Route::get('mark_attendance', ['as' => 'admin.mark_attendance', 'uses' => 'AttendanceController@markAttendance']);

        Route::get('mark_attendance/checkstatus/{emp_id}', 'AttendanceController@view_status');
        Route::post('mark_attendance', ['as' => 'admin.mark_attendance-st', 'uses' => 'AttendanceController@storeMarkAttendance']);


        Route::get('bulk/mark_attendance', ['as' => 'admin.bulk.mark_attendance', 'uses' => 'AttendanceController@bulkAttendance']);
        Route::post('bulk/mark_attendance', ['as' => 'admin.bulk.mark_attendancenxt', 'uses' => 'AttendanceController@bulkAttendanceNext']);
        Route::post('bulk/mark_attendance/save', ['as' => 'admin.bulk.mark_attendance.save', 'uses' => 'AttendanceController@bulkAttendanceSave']);
        Route::get('markattendence/{user_id}/clockin', ['as' => 'markattendance.clockin',   'uses' => 'ClockController@markAttendanceClockin']);

        Route::get('markattendence/{user_id}/clockout', ['as' => 'markattendance.clockout',   'uses' => 'ClockController@markAttendanceClickout']);

        // Leave Management
        Route::get('leave_management', ['as' => 'admin.leave_management', 'uses' => 'LeaveManagementController@index']);
        Route::post('leave_management', ['as' => 'admin.saveLeaveManagement', 'uses' => 'LeaveManagementController@store']);
        Route::get('leave_management/detail/{id}', ['as' => 'admin.editLeaveManagement', 'uses' => 'LeaveManagementController@edit']);
        Route::get('leave_management/change_status/{applId}/{status}', ['as' => 'admin.editLeaveManagementChangeStatus', 'uses' => 'LeaveManagementController@changeStatus']);
        Route::post('leave_management/set_action/{applId}/{status}', ['as' => 'admin.setStatusLeaveManagement', 'uses' => 'LeaveManagementController@setStatus']);
        Route::get('leave_management_delete/{id}', ['as' => 'admin.deleteLeaveManagement', 'uses' => 'LeaveManagementController@destroy']);

        Route::get('allpendingleaves', ['as' => 'admin.allpendingleaves', 'uses' => 'LeaveManagementController@allpendingleave']);

        Route::get('leavereport', ['as' => 'admin.leavereport', 'uses' => 'LeaveManagementController@leavereport']);



        Route::get('bulk/leave_management', ['as' => 'admin.bulk.leave_management', 'uses' => 'LeaveManagementController@bulkleave']);

        Route::post('bulk/leave_management', ['as' => 'admin.bulk.leave_managementgt', 'uses' => 'LeaveManagementController@getbulkleave']);
        Route::post('bulk/leave_management/store', ['as' => 'admin.bulk.leave_management-store', 'uses' => 'LeaveManagementController@storebulkleave']);
        // leave category
        Route::get('leavecategory', ['as' => 'admin.leavecategory', 'uses' => 'LeaveCategoryController@index']);
        Route::get('leavecategory/create', ['as' => 'admin.leavecategory.create',            'uses' => 'LeaveCategoryController@create']);
        Route::post('leavecategory/store', ['as' => 'admin.leavecategory.store',            'uses' => 'LeaveCategoryController@store']);
        Route::get('leavecategory/{id}/edit', ['as' => 'admin.leavecategory.edit',            'uses' => 'LeaveCategoryController@edit']);
        Route::post('leavecategory/{id}/update', ['as' => 'admin.leavecategory.update',            'uses' => 'LeaveCategoryController@update']);
        Route::get('leavecategory/{caseId}/confirm-delete', ['as' => 'admin.leavecategory.confirm-delete',   'uses' => 'LeaveCategoryController@getModalDelete']);
        Route::get('leavecategory/{caseId}/delete', ['as' => 'admin.leavecategory.delete',           'uses' => 'LeaveCategoryController@destroy']);

        // Ajax User Leave Check
        Route::get('check_user_available_leave', ['as' => 'admin.checkAvailableLeave', 'uses' => 'LeaveManagementController@checkAvailableLeave']);

        // Department and Designation
        Route::get('departments', ['as' => 'admin.departments.index',   'uses' => 'DepartmentController@index']);
        Route::post('departments/store', ['as' => 'admin.departments.store',   'uses' => 'DepartmentController@store']);
        Route::get('departments/{departments_id}/edit', ['as' => 'admin.departments.edit',   'uses' => 'DepartmentController@edit']);
        Route::post('departments/update/{departments_id}', ['as' => 'admin.departments.updateDepartment',   'uses' => 'DepartmentController@updateDepartment']);
        Route::get('departments/edit/{departments_id}/{designations_id}', ['as' => 'admin.departments.editDesignation',   'uses' => 'DepartmentController@editDesignation']);

        Route::get('departments/delete/{departments_id}', ['as' => 'admin.departments.deleteDepartment',   'uses' => 'DepartmentController@deleteDepartment']);
        Route::get('departments/delete_designation/{designations_id}', ['as' => 'admin.departments.deleteDesignation',   'uses' => 'DepartmentController@deleteDesignation']);

        //Project Calendar
        Route::get('/project/calendar', ['as' => 'admin.project.calendar', 'uses' => 'ProjectCalendarController@index']);

        // Holiday
        Route::get('holiday', ['as' => 'admin.holiday', 'uses' => 'HolidayController@index']);
        Route::post('holiday', ['as' => 'admin.saveHoliday', 'uses' => 'HolidayController@store']);
        Route::post('holiday/{id}', ['as' => 'admin.editHoliday', 'uses' => 'HolidayController@edit']);
        Route::post('holiday_update/{id}', ['as' => 'admin.updateHoliday', 'uses' => 'HolidayController@update']);
        Route::get('holiday_delete/{id}', ['as' => 'admin.deleteHoliday', 'uses' => 'HolidayController@destroy']);
        Route::get('holiday/exportpdf/{year}', ['as' => 'admin.holidays.exportpdf', 'uses' => 'HolidayController@DownloadPdf']);
        Route::get('holiday/exportexcel/{year}', ['as' => 'admin.holidays.exportexcel', 'uses' => 'HolidayController@DownloadExcel']);

        //travel request
        Route::get('tarvelrequest', ['as' => 'admin.tarvelrequest.index', 'uses' => 'TarvelRequestController@index']);
        Route::get('tarvelrequest/create', ['as' => 'admin.tarvelrequest.create', 'uses' => 'TarvelRequestController@create']);
        Route::post('tarvelrequest/create', ['as' => 'admin.tarvelrequest.store', 'uses' => 'TarvelRequestController@store']);
        Route::get('tarvelrequest/edit/{id}', ['as' => 'admin.tarvelrequest.edit', 'uses' => 'TarvelRequestController@edit']);
        Route::post('tarvelrequest/edit/{id}', ['as' => 'admin.tarvelrequest.update', 'uses' => 'TarvelRequestController@update']);
        Route::get('tarvelrequest/show/{id}', ['as' => 'admin.tarvelrequest.show', 'uses' => 'TarvelRequestController@show']);
        Route::get('tarvelrequest/confirm-delete/{id}', ['as' => 'admin.tarvelrequest.confirm-delete', 'uses' => 'TarvelRequestController@getModalDelete']);
        Route::get('tarvelrequest/delete/{id}', ['as' => 'admin.tarvelrequest.delete', 'uses' => 'TarvelRequestController@destroy']);

        Route::get('getClientinfo', ['as' => 'admin.getclient.info', 'uses' => 'ClientsController@get_client_info']);

        // Routes for User Targets
        Route::get('userTarget', ['as' => 'admin.userTarget.index',            'uses' => 'UserTargetController@index']);
        Route::get('userTarget/create', ['as' => 'admin.userTarget.create',           'uses' => 'UserTargetController@create']);
        Route::post('userTarget', ['as' => 'admin.userTarget.store',            'uses' => 'UserTargetController@store']);
        Route::get('userTarget/{leadId}/edit', ['as' => 'admin.userTarget.edit',             'uses' => 'UserTargetController@edit']);
        Route::patch('userTarget/{leadId}', ['as' => 'admin.userTarget.update',           'uses' => 'UserTargetController@update']);
        Route::get('userTarget/{leadId}/confirm-delete', ['as' => 'admin.userTarget.confirm-delete',   'uses' => 'UserTargetController@getModalDelete']);
        Route::get('userTarget/{leadId}/delete', ['as' => 'admin.userTarget.delete',           'uses' => 'UserTargetController@destroy']);
        Route::get('userTarget/summary/{year}', ['as' => 'admin.userTarget.summary', 'uses' => 'UserTargetController@summary']);

        // Salary Template
        Route::get('payroll/salary_template', ['as' => 'admin.payroll.salary_template',   'uses' => 'PayrollController@salaryTemplate']);

        Route::post('payroll/salary_template', ['as' => 'admin.payroll.store_salary_template',   'uses' => 'PayrollController@storeSalaryTemplate']);

        Route::get('payroll/salary_template/{salary_template_id}', ['as' => 'admin.payroll.edit_salary_template',   'uses' => 'PayrollController@editSalaryTemplate']);

        Route::get('payroll/salary_template_details/{salary_template_id}', ['as' => 'admin.payroll.show_salary_template',   'uses' => 'PayrollController@showSalaryTemplate']);

        Route::get('payroll/salary_template_delete/{salary_template_id}', ['as' => 'admin.payroll.destroy_salary_template',   'uses' => 'PayrollController@destroySalaryTemplate']);

        Route::get('payroll/generatePdfSalarytemplate/{training_id}', ['as' => 'admin.payroll.generatePdfSalarytemplate',   'uses' => 'PayrollController@generatePdfSalarytemplate']);

        Route::get('payroll/generatePrintSalarytemplate/{training_id}', ['as' => 'admin.payroll.generatePrintSalarytemplate',   'uses' => 'PayrollController@generatePrintSalarytemplate']);

        // Manage Salary Details
        Route::get('payroll/manage_salary_details', ['as' => 'admin.payroll.manage_salary_details',   'uses' => 'PayrollController@manageSalaryDetails']);
        Route::post('payroll/manage_salary_details', ['as' => 'admin.payroll.list_manage_salary_details',   'uses' => 'PayrollController@listSalaryDetails']);
        Route::get('payroll/manage_salary_details/{departments_id}', ['as' => 'admin.payroll.list_department_salary_details',   'uses' => 'PayrollController@listDepartmentSalaryDetails']);
        Route::post('payroll/store_salary_details', ['as' => 'admin.payroll.store_salary_details',   'uses' => 'PayrollController@storeSalaryDetails']);

        // Employee Salary List
        Route::get('payroll/employee_salary_list', ['as' => 'admin.payroll.employee_salary_list',   'uses' => 'PayrollController@employeeSalaryList']);

        Route::get('payroll/salary_delete/{payroll_id}', ['as' => 'admin.payroll.salary_delete',   'uses' => 'PayrollController@destroySalary']);

        Route::get('payroll/salary_details/{payroll_id}', ['as' => 'admin.payroll.show_salary_details',   'uses' => 'PayrollController@showSalaryDetail']);

        Route::get('payroll/generatePdfSalaryDetail/{payroll_id}', ['as' => 'admin.payroll.generatePdfSalaryDetail',   'uses' => 'PayrollController@generatePdfSalaryDetail']);


        //accounting prefix

        Route::get('accountingPrefix', ['as' => 'admin.accountingPrefix.index', 'uses' => 'AccountingPrefixController@index']);

        Route::get('accountingPrefix/create', ['as' => 'admin.accountingPrefix.create', 'uses' => 'AccountingPrefixController@create']);

        Route::post('accountingPrefix/create', ['as' => 'admin.accountingPrefix.store', 'uses' => 'AccountingPrefixController@store']);

        Route::get('accountingPrefix/edit/{id}', ['as' => 'admin.accountingPrefix.edit', 'uses' => 'AccountingPrefixController@edit']);

        Route::post('accountingPrefix/edit/{id}', ['as' => 'admin.accountingPrefix.update', 'uses' => 'AccountingPrefixController@update']);

        Route::get('accountingPrefix/confirm-delete{id}', ['as' => 'admin.accountingPrefix.confirm-delete', 'uses' => 'AccountingPrefixController@getModalDelete']);

        Route::get('accountingPrefix/delete/{id}', ['as' => 'admin.accountingPrefix.delete', 'uses' => 'AccountingPrefixController@destroy']);




        // Advance Salary
        Route::get('payroll/advance_salary', ['as' => 'admin.advance_salary', 'uses' => 'AdvanceSalaryController@index']);

        Route::post('payroll/advance_salary', ['as' => 'admin.saveAdvanceSalary', 'uses' => 'AdvanceSalaryController@store']);

        Route::post('payroll/advance_salary/{id}', ['as' => 'admin.editAdvanceSalary', 'uses' => 'AdvanceSalaryController@edit']);

        Route::post('payroll/advance_salary_update/{id}', ['as' => 'admin.updateAdvanceSalary', 'uses' => 'AdvanceSalaryController@update']);

        Route::get('payroll/advance_salary_delete/{id}', ['as' => 'admin.deleteAdvanceSalary', 'uses' => 'AdvanceSalaryController@destroy']);

        // Provident Fund
        Route::get('payroll/provident_fund', ['as' => 'admin.provident_fund', 'uses' => 'ProvidentFundController@index']);

        // Over Time
        Route::get('payroll/over_time', ['as' => 'admin.over_time', 'uses' => 'OverTimeController@index']);
        Route::post('payroll/over_time', ['as' => 'admin.saveOverTime', 'uses' => 'OverTimeController@store']);
        Route::post('payroll/over_time/{id}', ['as' => 'admin.editOverTime', 'uses' => 'OverTimeController@edit']);

        Route::post('payroll/over_time_update/{id}', ['as' => 'admin.updateOverTime', 'uses' => 'OverTimeController@update']);
        Route::get('payroll/over_time_delete/{id}', ['as' => 'admin.deleteOverTime', 'uses' => 'OverTimeController@destroy']);



        // new payroll system
        Route::get('payroll/list_payroll', ['as' => 'admin.payrolllist.index',   'uses' => 'PayrollControllerNew@index']);
        Route::get('payroll/create_payroll', ['as' => 'admin.payrolllist.create',   'uses' => 'PayrollControllerNew@create']);

        Route::post('payroll/store_payroll', ['as' => 'admin.payrolllist.store',   'uses' => 'PayrollControllerNew@store']);
        Route::get('payroll/show-detail/{id}', ['as' => 'admin.payrolllist.show',   'uses' => 'PayrollControllerNew@show']);
        Route::get('/payroll/download-payroll', ['as' => 'admin.payrolllist.download',   'uses' => 'PayrollControllerNew@payrollDownload']);



        // Make Payment
        Route::get('payroll/make_payment', ['as' => 'admin.payroll.make_payment.index',   'uses' => 'PaymentController@index']);
        Route::post('payroll/make_payment', ['as' => 'admin.payroll.list_payment',   'uses' => 'PaymentController@listPayment']);
        Route::get('payroll/show_payment_detail/{user_id}/{payment_month}', ['as' => 'admin.payroll.show_payment_detail',   'uses' => 'PaymentController@showPaymentDetail']);
        Route::get('payroll/salary_payment_detail/{salary_payment_id}', ['as' => 'admin.payroll.salary_payment_detail',   'uses' => 'PaymentController@salaryPaymentDetail']);
        Route::get('payroll/make_payment/{user_id}/{departments_id}/{payment_month}', ['as' => 'admin.payroll.make_payment',   'uses' => 'PaymentController@makePayment']);
        Route::post('payroll/make_payment/submit_new_payment', ['as' => 'admin.payroll.submit_new_payment',   'uses' => 'PaymentController@submitNewPayment']);

        // Generate Payslip
        Route::get('payroll/generate_payslip', ['as' => 'admin.payroll.generate_payslip.index',   'uses' => 'PaymentController@generatePaySlip']);
        Route::post('payroll/generate_payslip', ['as' => 'admin.payroll.generate_payslip',   'uses' => 'PaymentController@listPayslip']);
        Route::get('payroll/show_payslip_detail/{user_id}/{payment_month}', ['as' => 'admin.payroll.show_payslip_detail',   'uses' => 'PayslipController@showPaymentDetail']);

        //Run Payroll
        Route::get('payroll/run/step1', ['as' => 'admin.payroll.run_step1',   'uses' => 'PayrollController@run_payroll_step1']);
        Route::post('payroll/run/timecard_review', ['as' => 'admin.payroll.timecard_review',   'uses' => 'PayrollController@run_payroll_timecard_review']);
        Route::post('payroll/run/enter_payroll', ['as' => 'admin.payroll.enter_payroll',   'uses' => 'PayrollController@run_payroll_enter_payroll']);
        Route::post('payroll/run/payroll_summary', ['as' => 'admin.payroll.payroll_summary',   'uses' => 'PayrollController@run_payroll_summary']);

        //timekeeting setup
        Route::get('timekeep_setup', ['as' => 'admin.attendance.timekeep_setup',   'uses' => 'AttendanceController@timekeep_setup']);
        Route::post('timekeep_setup', ['as' => 'admin.attendance.timekeep_setup-post',   'uses' => 'AttendanceController@timekeep_setup_post']);

        //Employee Award

        Route::get('payroll/employee_award', ['as' => 'admin.payroll.employee_award.index',   'uses' => 'PaymentController@employeeAward']);
        Route::post('payroll/employee_award', ['as' => 'admin.employeeeaward.store', 'uses' => 'PaymentController@employeeAwardStore']);
        Route::get('payroll/employee_award_delete/{id}', ['as' => 'admin.deleteemployeeaward', 'uses' => 'PaymentController@destroyEmployeeAward']);
        Route::post('payroll/employee_award/{id}', ['as' => 'admin.editEmployeeAward', 'uses' => 'PaymentController@editEmployeeAward']);
        Route::post('payroll/employee_award_update/{id}', ['as' => 'admin.updateEmployeeAward', 'uses' => 'PaymentController@updateEmployeeAward']);

        Route::get('payroll/salary_summary', ['as' => 'admin.payroll.salary_summary', 'uses' => 'PayrollController@salary_summary']);
        Route::get('payroll/salary_summary_view', ['as' => 'admin.payroll.salary_summary_view', 'uses' => 'PayrollController@salary_summary_details']);
        Route::get('payroll/salary_summary/{query}/{filter}/{print_type}', ['as' => 'admin.payroll.salary_summary.print', 'uses' => 'PayrollController@salary_summary_print']);

        //BillPrintTemplate
        Route::get('billprinttemplate', ['as' => 'admin.billprinttemplate.index', 'uses' => 'BillPrintTemplateController@index']);
        Route::get('billprinttemplate/create', ['as' => 'admin.billprinttemplate.create', 'uses' => 'BillPrintTemplateController@create']);
        Route::post('billprinttemplate/create', ['as' => 'admin.billprinttemplate.store', 'uses' => 'BillPrintTemplateController@store']);
        Route::get('billprinttemplate/edit/{id}', ['as' => 'admin.billprinttemplate.edit', 'uses' => 'BillPrintTemplateController@edit']);
        Route::post('billprinttemplate/edit/{id}', ['as' => 'admin.billprinttemplate.update', 'uses' => 'BillPrintTemplateController@update']);
        Route::get('billprinttemplate/confirm-delete{id}', ['as' => 'admin.billprinttemplate.confirm-delete', 'uses' => 'BillPrintTemplateController@getModalDelete']);
        Route::get('billprinttemplate/delete/{id}', ['as' => 'admin.billprinttemplate.delete', 'uses' => 'BillPrintTemplateController@destroy']);

        // Job Application
        Route::get('job_applied', ['as' => 'admin.jobApplication.index',   'uses' => 'JobApplicationController@index']);
        Route::get('job_applied/status/{job_appliactions_id}', ['as' => 'admin.jobApplication.status',   'uses' => 'JobApplicationController@status']);
        Route::post('job_applied/updateStatus/{job_appliactions_id}', ['as' => 'admin.jobApplication.updateStatus',   'uses' => 'JobApplicationController@updateStatus']);
        Route::get('job_applied/show/{job_appliactions_id}', ['as' => 'admin.jobApplication.show',   'uses' => 'JobApplicationController@show']);
        Route::get('job_applied/delete/{job_appliactions_id}', ['as' => 'admin.jobApplication.delete',   'uses' => 'JobApplicationController@delete']);

        // Recruitment Job Posted / Circular
        Route::get('job_posted', ['as' => 'admin.recruitment.index',   'uses' => 'RecruitmentController@index']);
        Route::post('job_posted/save', ['as' => 'admin.recruitment.save',   'uses' => 'RecruitmentController@save']);
        Route::get('job_posted/edit/{job_circular_id}', ['as' => 'admin.recruitment.edit',   'uses' => 'RecruitmentController@edit']);
        Route::get('job_posted/show/{job_circular_id}', ['as' => 'admin.recruitment.show',   'uses' => 'RecruitmentController@show']);
        Route::get('job_posted/delete/{job_circular_id}', ['as' => 'admin.recruitment.delete',   'uses' => 'RecruitmentController@delete']);

        // Announcement
        Route::get('announcements', ['as' => 'admin.announcements.index',   'uses' => 'AnnouncementController@index']);
        Route::post('announcements/save', ['as' => 'admin.announcements.save',   'uses' => 'AnnouncementController@save']);
        Route::get('announcements/edit/{announcements_id}', ['as' => 'admin.announcements.edit',   'uses' => 'AnnouncementController@edit']);
        Route::get('announcements/show/{announcements_id}', ['as' => 'admin.announcements.show',   'uses' => 'AnnouncementController@show']);
        Route::get('announcements/delete/{announcements_id}', ['as' => 'admin.announcements.delete',   'uses' => 'AnnouncementController@delete']);
        Route::get('announcements/print', ['as' => 'admin.announcements.print',  'uses' => 'AnnouncementController@printNow']);
        Route::get('announcements/generatePdf', ['as' => 'admin.announcements.generatePdf',   'uses' => 'AnnouncementController@generatePdf']);

        // read Announcement

        Route::get('close/announcements/{id}', ['as' => 'admin.closeannouncements.index',   'uses' => 'AnnouncementController@ReadAnnouncement']);

        // Projects Category routes
        Route::post('projectscat/enableSelected', ['as' => 'admin.projectscat.enable-selected',  'uses' => 'ProjectscatController@enableSelected']);
        Route::post('projectscat/disableSelected', ['as' => 'admin.projectscat.disable-selected', 'uses' => 'ProjectscatController@disableSelected']);
        Route::get('projectscat/search', ['as' => 'admin.projectscat.search',           'uses' => 'ProjectscatController@searchByName']);
        Route::post('projectscat/getInfo', ['as' => 'admin.projectscat.get-info',         'uses' => 'ProjectscatController@getInfo']);
        Route::post('projectscat', ['as' => 'admin.projectscat.store',            'uses' => 'ProjectscatController@store']);

        Route::get('projectscat', ['as' => 'admin.projectscat.index',            'uses' => 'ProjectscatController@index']);

        Route::get('projectscat/create', ['as' => 'admin.projectscat.create',           'uses' => 'ProjectscatController@create']);
        Route::get('projectscat/{projectsCatId}', ['as' => 'admin.projectscat.show',             'uses' => 'ProjectscatController@show']);
        Route::patch('projectscat/{projectsCatId}', ['as' => 'admin.projectscat.patch',            'uses' => 'ProjectscatController@update']);
        Route::put('projectscat/{projectsCatId}', ['as' => 'admin.projectscat.update',           'uses' => 'ProjectscatController@update']);
        Route::delete('projectscat/{projectsCatId}', ['as' => 'admin.projectscat.destroy',          'uses' => 'ProjectscatController@destroy']);
        Route::get('projectscat/{projectsCatId}/edit', ['as' => 'admin.projectscat.edit',             'uses' => 'ProjectscatController@edit']);
        Route::get('projectscat/{projectsCatId}/confirm-delete', ['as' => 'admin.projectscat.confirm-delete',   'uses' => 'ProjectscatController@getModalDelete']);
        Route::get('projectscat/{projectsCatId}/delete', ['as' => 'admin.projectscat.delete',           'uses' => 'ProjectscatController@destroy']);
        Route::get('projectscat/{projectsCatId}/enable', ['as' => 'admin.projectscat.enable',           'uses' => 'ProjectscatController@enable']);
        Route::get('projectscat/{projectsCatId}/disable', ['as' => 'admin.projectscat.disable',          'uses' => 'ProjectscatController@disable']);

        // For Project Task
        Route::get('project_tasks', ['as' => 'admin.project_task.index',           'uses' => 'ProjectTaskController@index']);

        // Task Category routes
        Route::post('taskscat/enableSelected', ['as' => 'admin.taskscat.enable-selected',  'uses' => 'TaskscatController@enableSelected']);
        Route::post('taskscat/disableSelected', ['as' => 'admin.taskscat.disable-selected', 'uses' => 'TaskscatController@disableSelected']);
        Route::get('taskscat/search', ['as' => 'admin.taskscat.search',           'uses' => 'TaskscatController@searchByName']);
        Route::post('taskscat/getInfo', ['as' => 'admin.taskscat.get-info',         'uses' => 'TaskscatController@getInfo']);
        Route::post('taskscat', ['as' => 'admin.taskscat.store',            'uses' => 'TaskscatController@store']);

        Route::get('taskscat', ['as' => 'admin.taskscat.index',            'uses' => 'TaskscatController@index']);

        Route::get('taskscat/create', ['as' => 'admin.taskscat.create',           'uses' => 'TaskscatController@create']);
        Route::get('taskscat/{tasksCatId}', ['as' => 'admin.taskscat.show',             'uses' => 'TaskscatController@show']);
        Route::patch('taskscat/{tasksCatId}', ['as' => 'admin.taskscat.patch',            'uses' => 'TaskscatController@update']);
        Route::put('taskscat/{tasksCatId}', ['as' => 'admin.taskscat.update',           'uses' => 'TaskscatController@update']);
        Route::delete('taskscat/{tasksCatId}', ['as' => 'admin.taskscat.destroy',          'uses' => 'TaskscatController@destroy']);
        Route::get('taskscat/{tasksCatId}/edit', ['as' => 'admin.taskscat.edit',             'uses' => 'TaskscatController@edit']);
        Route::get('taskscat/{tasksCatId}/confirm-delete', ['as' => 'admin.taskscat.confirm-delete',   'uses' => 'TaskscatController@getModalDelete']);
        Route::get('taskscat/{tasksCatId}/delete', ['as' => 'admin.taskscat.delete',           'uses' => 'TaskscatController@destroy']);
        Route::get('taskscat/{tasksCatId}/enable', ['as' => 'admin.taskscat.enable',           'uses' => 'TaskscatController@enable']);
        Route::get('taskscat/{tasksCatId}/disable', ['as' => 'admin.taskscat.disable',          'uses' => 'TaskscatController@disable']);

        Route::get('projects/search/tasks', ['as' => 'admin.projects.searchprojecttask', 'uses' => 'ProjectTaskController@searchprojecttask']);

        // Client routes
        Route::post('clients/enableSelected', ['as' => 'admin.clients.enable-selected',  'uses' => 'ClientsController@enableSelected']);
        Route::post('clients/disableSelected', ['as' => 'admin.clients.disable-selected', 'uses' => 'ClientsController@disableSelected']);



        Route::get('clients', ['as' => 'admin.clients.index', 'uses' => 'ClientsController@index']);

        Route::get('customer', ['as' => 'admin.customer.index', 'uses' => 'ClientsController@customers']);
        Route::get('supplier', ['as' => 'admin.supplier.index', 'uses' => 'ClientsController@suppliers']);

        //modal routes



        //clients import export
        Route::get('clients/importExport', ['as' => 'admin.import-export-clients.index',            'uses' => 'ExcelController@indexClients']);
        Route::post('clients/importExcelClients', ['as' => 'admin.import-export-clients.store', 'uses' => 'ExcelController@importExcelClients']);
        Route::get('downloadExcelclients/{type}', ['as' => 'admin.downloadExcelclients', 'uses' => 'ExcelController@downloadExcelClients']);
        //customergroups
        Route::get('customergroup', ['as' => 'admin.customergroup.index', 'uses' => 'CustomerGroupController@index']);
        Route::get('customergroup/create', ['as' => 'admin.customergroup.create', 'uses' => 'CustomerGroupController@create']);
        Route::post('customergroup/create', ['as' => 'admin.customergroup.store', 'uses' => 'CustomerGroupController@store']);
        Route::get('customergroup/edit/{id}', ['as' => 'admin.customergroup.edit', 'uses' => 'CustomerGroupController@edit']);
        Route::post('customergroup/edit/{id}', ['as' => 'admin.customergroup.update', 'uses' => 'CustomerGroupController@update']);
        Route::get('customergroup/confirm-delete{id}', ['as' => 'admin.customergroup.confirm-delete', 'uses' => 'CustomerGroupController@getModalDelete']);
        Route::get('customergroup/delete/{id}', ['as' => 'admin.customergroup.delete', 'uses' => 'CustomerGroupController@destroy']);



        Route::get('poscustomer', ['as' => 'admin.clients.poscustomer', 'uses' => 'ClientsController@posCustomer']);
        Route::get('hotelcustomer', ['as' => 'admin.clients.hotelcustomer', 'uses' => 'ClientsController@hotelCustomer']);
        Route::get('supplier', ['as' => 'admin.clients.supplier', 'uses' => 'ClientsController@suppliers']);
        Route::get('agent', ['as' => 'admin.clients.agent', 'uses' => 'ClientsController@agent']);

        Route::resource('datatables', 'ClientsController', [
            'anyData'  => 'datatables.data',
            'index' => 'datatables',
        ]);

        // Contact routes
        Route::post('contacts/enableSelected', ['as' => 'admin.contacts.enable-selecteded',  'uses' => 'ContactsController@enableSelected']);
        Route::post('contacts/disableSelected', ['as' => 'admin.contacts.disable-selected', 'uses' => 'ContactsController@disableSelected']);

        Route::get('contacts', ['as' => 'admin.contacts.index',            'uses' => 'ContactsController@index']);

        Route::resource('datatables', 'ContactsController', [
            'anyData'  => 'datatables.data',
            'index' => 'datatables',
        ]);

        Route::get('contacts/create', ['as' => 'admin.contacts.create',           'uses' => 'ContactsController@create']);
        Route::post('contacts', ['as' => 'admin.contacts.store',            'uses' => 'ContactsController@store']);
        Route::get('contacts/{leadId}', ['as' => 'admin.contacts.show',             'uses' => 'ContactsController@show']);
        Route::patch('contacts/{leadId}', ['as' => 'admin.contacts.patch',            'uses' => 'ContactsController@update']);
        Route::put('contacts/{leadId}', ['as' => 'admin.contacts.update',           'uses' => 'ContactsController@update']);
        Route::delete('contacts/{leadId}', ['as' => 'admin.contacts.destroy',          'uses' => 'ContactsController@destroy']);
        Route::get('contacts/{leadId}/edit', ['as' => 'admin.contacts.edit',             'uses' => 'ContactsController@edit']);
        Route::get('contacts/{leadId}/confirm-delete', ['as' => 'admin.contacts.confirm-delete',   'uses' => 'ContactsController@getModalDelete']);
        Route::get('contacts/{leadId}/delete', ['as' => 'admin.contacts.delete',           'uses' => 'ContactsController@destroy']);
        Route::get('contacts/{leadId}/enable', ['as' => 'admin.contacts.enable',           'uses' => 'ContactsController@enable']);
        Route::get('contacts/{leadId}/disable', ['as' => 'admin.contacts.disable',          'uses' => 'ContactsController@disable']);

        Route::get('contacts/create/modals', ['as' => 'admin.contacts.create.modals',          'uses' => 'ContactsController@createModal']);
        Route::post('contacts/create/modals', ['as' => 'admin.contacts.create.store',          'uses' => 'ContactsController@store']);

        // Import Export Excel Files - Contacts
        Route::get('importExport', ['as' => 'admin.import-export.index',            'uses' => 'ExcelController@index']);
        Route::get('downloadExcel/{type}', ['as' => 'admin.import-export.downloadExcel', 'uses' => 'ExcelController@downloadExcel']);
        Route::post('importExcel', ['as' => 'admin.import-export.importExcel', 'uses' => 'ExcelController@importExcel']);

        // Import Export Excel Files - Leads
        Route::get('importExportLeads', ['as' => 'admin.import-export.leads',            'uses' => 'ExcelController@leads']);
        Route::get('downloadExcelLeads/{type}', ['as' => 'admin.import-export.downloadExcelLeads', 'uses' => 'ExcelController@downloadExcelLeads']);
        Route::post('importExcelLeads', ['as' => 'admin.import-export.importExcelLeads', 'uses' => 'ExcelController@importExcelLeads']);

        Route::get('downloadexcelfilter', ['as' => 'admin.import-export.downloadexcelfilter', 'uses' => 'LeadsController@DownloadExcelFilter']);

        // Training
        Route::get('trainings', ['as' => 'admin.trainings.index',   'uses' => 'TrainingController@index']);
        Route::post('trainings/save', ['as' => 'admin.trainings.save',   'uses' => 'TrainingController@save']);
        Route::get('trainings/edit/{training_id}', ['as' => 'admin.trainings.edit',   'uses' => 'TrainingController@edit']);
        Route::get('trainings/show/{training_id}', ['as' => 'admin.trainings.show',   'uses' => 'TrainingController@show']);
        Route::get('trainings/delete/{training_id}', ['as' => 'admin.trainings.delete',   'uses' => 'TrainingController@delete']);
        Route::get('trainings/print', ['as' => 'admin.trainings.print',  'uses' => 'TrainingController@printNow']);
        Route::get('trainings/generatePdf', ['as' => 'admin.trainings.generatePdf',   'uses' => 'TrainingController@generatePdf']);
        Route::get('trainings/generatePdf/{training_id}', ['as' => 'admin.trainings.generateSinglePdf',   'uses' => 'TrainingController@generateSinglePdf']);

        // Stock
        Route::get('stock/category', ['as' => 'admin.stock.category',   'uses' => 'StockController@category']);
        Route::post('stock/save_category', ['as' => 'admin.stock.save_category',   'uses' => 'StockController@saveCategory']);
        Route::get('stock/category/{cat}/{subCat}', ['as' => 'admin.stock.editSubCategory',   'uses' => 'StockController@editSubCategory']);
        Route::get('stock/category/{stock_category_id}', ['as' => 'admin.stock.editCategory',   'uses' => 'StockController@editCategory']);
        Route::post('stock/category/{stock_category_id}', ['as' => 'admin.stock.updateCategory',   'uses' => 'StockController@updateCategory']);

        Route::get('stock/delete_stock_category/{stock_category_id}', ['as' => 'admin.stock.deleteCategory',   'uses' => 'StockController@deleteCategory']);
        Route::get('stock/delete_stock_sub_category/{subCat}', ['as' => 'admin.stock.deleteSubCategory',   'uses' => 'StockController@deleteSubCategory']);

        Route::get('stock/list', ['as' => 'admin.stock.list',   'uses' => 'StockController@lists']);
        Route::get('stock/list/{stock_id}', ['as' => 'admin.stock.editStock',   'uses' => 'StockController@editStock']);
        Route::post('stock/save_stock', ['as' => 'admin.stock.save_stock',   'uses' => 'StockController@saveStock']);
        Route::get('stock/delete_stock/{stock_id}', ['as' => 'admin.stock.delete_stock',   'uses' => 'StockController@deleteStock']);

        Route::get('stock/history', ['as' => 'admin.stock.history',   'uses' => 'StockController@history']);
        Route::post('stock/history', ['as' => 'admin.stock.history_list',   'uses' => 'StockController@history']);

        Route::get('stock/assign', ['as' => 'admin.stock.assign',   'uses' => 'StockController@assign']);
        //Ajax
        Route::get('stock/getStockBySubCategory', ['as' => 'admin.stock.getStockBySubCategory',   'uses' => 'StockController@getStockBySubCategory']);
        Route::get('stock/getStockQuantity', ['as' => 'admin.stock.getStockQuantity',   'uses' => 'StockController@getStockQuantity']);

        Route::post('stock/save_assign', ['as' => 'admin.stock.save_assign',   'uses' => 'StockController@saveAssign']);
        Route::get('stock/delete_assign_stock/{assign_item_id}', ['as' => 'admin.stock.assign_item_id',   'uses' => 'StockController@deleteAssign']);
        Route::get('stock/printAssign', ['as' => 'admin.stock.printAssign',  'uses' => 'StockController@printAssign']);
        Route::get('stock/generateAssignPdf', ['as' => 'admin.stock.generateAssignPdf',   'uses' => 'StockController@generateAssignPdf']);

        Route::get('stock/assign_report', ['as' => 'admin.stock.assign_report',   'uses' => 'StockController@assignReport']);
        Route::post('stock/assign_report', ['as' => 'admin.stock.assign_report_list',   'uses' => 'StockController@assignReport']);
        Route::get('stock/printAssign/{user_id}', ['as' => 'admin.stock.printUserAssign',  'uses' => 'StockController@printUserAssign']);
        Route::get('stock/generateAssignPdf/{user_id}', ['as' => 'admin.stock.generateUserAssignPdf',   'uses' => 'StockController@generateUserAssignPdf']);

        Route::get('stock/report', ['as' => 'admin.stock.report',   'uses' => 'StockController@report']);
        Route::post('stock/report', ['as' => 'admin.stock.report_list',   'uses' => 'StockController@reportList']);
        Route::get('stock/printAssign/{start_date}/{end_date}', ['as' => 'admin.stock.printUserAssignByDate',  'uses' => 'StockController@printUserAssignBydate']);
        Route::get('stock/generateAssignPdf/{start_date}/{end_date}', ['as' => 'admin.stock.generateUserAssignPdfByDate',   'uses' => 'StockController@generateUserAssignPdfByDate']);


        // assets return

        Route::get('stock/return', ['as' => 'admin.stock.return',   'uses' => 'StockController@return']);
        Route::post('stock/save_return', ['as' => 'admin.stock.save_return',   'uses' => 'StockController@saveReturn']);
        Route::get('stock/delete_return_stock/{return_item_id}', ['as' => 'admin.stock.return_item_id',   'uses' => 'StockController@deleteReturn']);
        Route::get('stock/printReturn', ['as' => 'admin.stock.printReturn',  'uses' => 'StockController@printReturn']);
        Route::get('stock/generateReturnPdf', ['as' => 'admin.stock.generateReturnPdf',   'uses' => 'StockController@generateReturnPdf']);

        // For Project Task
        Route::get('project_tasks', ['as' => 'admin.project_task.index',           'uses' => 'ProjectTaskController@index']);
        // For Project Task
        //    Route::post(  'store_project_task',                           ['as' => 'admin.project_task.store',            'uses' => 'ProjectTaskController@store']);

        Route::get('project_task/create/{projectId}', ['as' => 'admin.project_task.create',             'uses' => 'ProjectTaskController@create']);
        Route::get('task/create/global', ['as' => 'admin.project_task.createglobal', 'uses' => 'ProjectTaskController@createGlobal']);
        Route::post('task/create/store', ['as' => 'admin.project_task.storeglobal', 'uses' => 'ProjectTaskController@storeGlobal']);

        Route::post('project_task/store/{projectId}', ['as' => 'admin.project_task.store',             'uses' => 'ProjectTaskController@store']);
        Route::get('project_task/{taskId}', ['as' => 'admin.project_task.show',             'uses' => 'ProjectTaskController@show']);
        Route::get('project_task/{taskId}/edit', ['as' => 'admin.project_task.edit',             'uses' => 'ProjectTaskController@edit']);
        Route::patch('project_task/{taskId}', ['as' => 'admin.project_task.patch',            'uses' => 'ProjectTaskController@update']);
        Route::put('project_task/{taskId}', ['as' => 'admin.project_task.update',           'uses' => 'ProjectTaskController@update']);

        Route::delete('project_task/{taskId}', ['as' => 'admin.project_task.destroy',          'uses' => 'ProjectTaskController@destroy']);

        Route::get('project_task/{taskId}/confirm-delete', ['as' => 'admin.project_task.confirm-delete',   'uses' => 'ProjectTaskController@getModalDelete']);
        Route::get('project_task/{taskId}/delete', ['as' => 'admin.project_task.delete',           'uses' => 'ProjectTaskController@destroy']);
        Route::get('getUserTagsJson', ['as' => 'admin.getUserTagsJson.store',            'uses' => 'ProjectTaskController@getUserTagsJson']);

        Route::post('post_comment', ['as' => 'admin.post_comment',            'uses' => 'ProjectTaskController@postComment']);

        Route::post('ajax_proj_task_status', ['as' => 'admin.ajax_proj_task_status', 'uses' => 'ProjectTaskController@ajaxProjectTaskStatus']);

        Route::post('ajax_proj_task_order', ['as' => 'admin.ajax_proj_task_order', 'uses' => 'ProjectTaskController@ajaxProjectTaskOrder']);

        Route::get('debtors_lists/ageing',['as'=>'admin.debtors_lists.ageing','uses'=>'PosAnalysisController@ageingView']);

        Route::get('debtors_lists',['as'=>'admin.debtors_lists','uses'=>'PosAnalysisController@debtors_lists']);

        Route::get('debtors_pay/{id}',['as'=>'admin.debtors_pay','uses'=>'PosAnalysisController@debtorsPay']);
        Route::get('creditors_pay/{id}',['as'=>'admin.creditors_pay','uses'=>'PosAnalysisController@creditorsPay']);

        Route::get('creditors_lists/ageing',['as'=>'admin.creditors_lists.ageing','uses'=>'PosAnalysisController@creditorageingView']);

        Route::get('creditors_lists',['as'=>'admin.creditors_lists','uses'=>'PosAnalysisController@creditors_lists']);


        Route::get('invoice_print/{id}','OrdersController@getInvoicePrint');

        Route::post('debtors_pay',['as'=>'admin.debtors_pay-subm','uses'=>'PosAnalysisController@debtorsPaySubmit']);
        Route::post('creditors_pay',['as'=>'admin.creditors_pay-subm','uses'=>'PosAnalysisController@creditorsPaySubmit']);

        Route::get('project_task_activities/{projectid}', ['as' => 'admin.project_task_activities.modals',           'uses' => 'ProjectsController@openactivities']);

        //COA Group && Leadgers
        Route::get('chartofaccounts', ['as' => 'admin.chartofaccounts', 'uses' => 'COAController@index']);

        Route::get('chartofaccounts/create/groups', ['as' => 'admin.chartofaccounts.create.groups', 'uses' => 'COAController@CreateGroups']);

        Route::post('chartofaccounts/store/groups', ['as' => 'admin.chartofaccounts.store.groups', 'uses' => 'COAController@PostGroups']);
        Route::get('chartofaccounts/create/ledgers', ['as' => 'admin.chartofaccounts.create.ledgers', 'uses' => 'COAController@CreateLedgers']);
        Route::post('chartofaccounts/store/ledgers', ['as' => 'admin.chartofaccounts.store.ledgers', 'uses' => 'COAController@PostLedgers']);
        Route::get('chartofaccounts/edit/{id}/groups', ['as' => 'admin.chartofaccounts.edit.groups', 'uses' => 'COAController@EditGroups']);
        Route::post('chartofaccounts/update/{id}/groups', ['as' => 'admin.chartofaccounts.update.groups', 'uses' => 'COAController@UpdateGroups']);
        Route::get('chartofaccounts/edit/{id}/ledgers', ['as' => 'admin.chartofaccounts.edit.ledgers', 'uses' => 'COAController@EditLedgers']);
        Route::post('chartofaccounts/update/{id}/ledgers', ['as' => 'admin.chartofaccounts.update.ledgers', 'uses' => 'COAController@UpdateLedgers']);
        Route::post('chartofaccounts/groups/getNextCode', ['as' => 'admin.chartofaccounts.create.groups.getnextcode', 'uses' => 'COAController@GetNextCode']);
        Route::post('chartofaccounts/ledgers/getNextCode', ['as' => 'admin.chartofaccounts.create.ledgers.getnextcode', 'uses' => 'COAController@getNextCodeLedgers']);
        Route::get('chartofaccounts/{orderId}/groups/confirm-delete', ['as' => 'admin.chartofaccounts.groups.confirm-delete',   'uses' => 'COAController@getModalDeleteGroups']);
        Route::get('chartofaccounts/{orderId}/groups/delete', ['as' => 'admin.chartofaccounts.groups.delete',           'uses' => 'COAController@destroyGroups']);
        Route::get('chartofaccounts/{orderId}/ledgers/confirm-delete', ['as' => 'admin.chartofaccounts.ledgers.confirm-delete',   'uses' => 'COAController@getModalDeleteLedgers']);
        Route::get('chartofaccounts/{orderId}/ledgers/delete', ['as' => 'admin.chartofaccounts.ledgers.delete',           'uses' => 'COAController@destroyLedgers']);

        Route::get('chartofaccounts/detail/{id}/ledgers', ['as' => 'admin.chartofaccounts.detail.ledgers', 'uses' => 'COAController@DetailLedgers']);
        Route::get('chartofaccounts/detail/{id}/groups', ['as' => 'admin.chartofaccounts.detail.groups', 'uses' => 'COAController@DetailGroups']);
        Route::get('chartofaccounts/list/{search}/groups', ['as' => 'admin.chartofaccounts.list.groups', 'uses' => 'COAController@DetailGroups']);

        Route::get('chartofaccounts/pdf/{id}', ['as' => 'admin.chartofaccounts.pdf', 'uses' => 'COAController@DownloadPdf']);
        Route::get('chartofaccounts/print/{id}', ['as' => 'admin.chartofaccounts.print', 'uses' => 'COAController@PrintLedgers']);
        Route::get('chartofaccounts/getLedgersAjax', ['as' => 'admin.chartofaccounts.getLedgersAjax', 'uses' => 'COAController@getLedgersAjax']);

        //Ledger import export
        Route::get('downloadExcelLedger', ['as' => 'admin.ExcelLedger.index', 'uses' => 'COAController@excelLedger']);
        Route::get('downloadExcelLedger/{type}', ['as' => 'admin.ExcelLedger.export', 'uses' => 'COAController@exportLedger']);
        Route::post('downloadExcelLedger/', ['as' => 'admin.ExcelLedger.store', 'uses' => 'COAController@importLedger']);

        Route::get('coa/filterbygroups', ['as' => 'admin.filter.coa.groups', 'uses' => 'COAController@filterByGroups']);
        Route::post('coa/filterbygroups', ['as' => 'admin.filter.coa.groups.detail', 'uses' => 'COAController@filterByGroupPost']);

        //ledgerSetting
        Route::get('ledgers/settings', ['as' => 'admin.ledgers.setting', 'uses' => 'LedgerSettingController@index']);
        Route::get('ledgers/settings/create', ['as' => 'admin.ledgers.setting.create', 'uses' => 'LedgerSettingController@create']);
        Route::post('ledgers/settings/store', ['as' => 'admin.ledgers.setting.store', 'uses' => 'LedgerSettingController@store']);

        Route::get('ledgers/settings/edit/{id}', ['as' => 'admin.ledgers.setting.edit', 'uses' => 'LedgerSettingController@edit']);
        Route::post('ledgers/settings/update/{id}', ['as' => 'admin.ledgers.setting.update', 'uses' => 'LedgerSettingController@update']);

        Route::get('ledgers/settings/destroy/{id}', ['as' => 'admin.ledgers.setting.destroy', 'uses' => 'LedgerSettingController@destroy']);

        //currency

        Route::get('currencies',['as'=>'admin.currency.index','uses'=>'CurrencyController@index']);
        Route::get('currency/create',['as'=>'admin.currency.create','uses'=>'CurrencyController@create']);
        Route::post('currency/create',['as'=>'admin.currency.store','uses'=>'CurrencyController@store']);
        Route::get('currency/edit/{id}',['as'=>'admin.currency.edit','uses'=>'CurrencyController@edit']);
        Route::post('currency/edit/{id}',['as'=>'admin.currency.up','uses'=>'CurrencyController@update']);
        Route::get('currency/confirm-delete/{id}',['as'=>'admin.currency.confirm-delete','uses'=>'CurrencyController@deleteModal']);
        Route::get('currency/delete/{id}',['as'=>'admin.currency.delete','uses'=>'CurrencyController@destroy']);

        //Manual currency
        Route::get('manual-currency',['as'=>'admin.manual-currency.index','uses'=>'ManualCurrencyController@index']);
        Route::get('manual-currency/create',['as'=>'admin.manual-currency.create','uses'=>'ManualCurrencyController@create']);
        Route::post('manual-currency/create',['as'=>'admin.manual-currency.store','uses'=>'ManualCurrencyController@store']);
        Route::get('manual-currency/edit/{id}',['as'=>'admin.manual-currency.edit','uses'=>'ManualCurrencyController@edit']);
        Route::post('manual-currency/edit/{id}',['as'=>'admin.manual-currency.up','uses'=>'ManualCurrencyController@update']);
        Route::get('manual-currency/confirm-delete/{id}',['as'=>'admin.manual-currency.confirm-delete','uses'=>'ManualCurrencyController@deleteModal']);
        Route::get('manual-currency/delete/{id}',['as'=>'admin.manual-currency.delete','uses'=>'ManualCurrencyController@destroy']);

        //Requisition

         Route::get('requisition', ['as' => 'admin.requisition.index',            'uses' => 'RequisitionController@index']);
        Route::get('requisition/create', ['as' => 'admin.requisition.create',            'uses' => 'RequisitionController@create']);
        Route::post('requisition/', ['as' => 'admin.requisition.store',            'uses' => 'RequisitionController@store']);
        Route::get('requisition/{Id}', ['as' => 'admin.requisition.show',             'uses' => 'RequisitionController@show']);
        Route::get('requisition/approve/{Id}', ['as' => 'admin.requisition.approve',             'uses' => 'RequisitionController@approve']);
        Route::get('requisition/{Id}/edit', ['as' => 'admin.requisition.edit',             'uses' => 'RequisitionController@edit']);
        Route::post('requisition/{Id}', ['as' => 'admin.requisition.update',             'uses' => 'RequisitionController@update']);
        Route::get('requisition/{id}/confirm-delete', ['as' => 'admin.requisition.confirm-delete',   'uses' => 'RequisitionController@getModalDelete']);
        Route::get('requisition/{id}/delete', ['as' => 'admin.requisition.delete',           'uses' => 'RequisitionController@destroy']);
        Route::get('getStockUnit', ['as' => 'admin.getStockUnit',            'uses' => 'RequisitionController@getStockUnit']);




        //Ledger Group import export
        Route::get('downloadExcelLedgergroups', ['as' => 'admin.ExcelLedgergroups.index', 'uses' => 'COAController@excelLedgergroups']);
        Route::get('downloadExcelLedgergroups/{type}', ['as' => 'admin.ExcelLedgergroups.export', 'uses' => 'COAController@exportLedgergroups']);
        Route::post('downloadExcelLedgergroups/', ['as' => 'admin.ExcelLedgergroups.store', 'uses' => 'COAController@importLedgergroups']);

        // Entries

        Route::get('entries', ['as' => 'admin.entries.index', 'uses' => 'EntryController@index']);
        Route::get('entries/add/{label}', ['as' => 'admin.entries.add', 'uses' => 'EntryController@Create']);
        Route::get('entries/show/{label}/{id}', ['as' => 'admin.entries.show', 'uses' => 'EntryController@show']);
        Route::post('entries/store', ['as' => 'admin.entries.store.', 'uses' => 'EntryController@store']);
        Route::post('entries/ajaxaddentry', ['as' => 'admin.entries.ajaxAddEntry', 'uses' => 'EntryController@ajaxAddEntry']);
        Route::post('entries/ajaxcl', ['as' => 'admin.entries.ajaxcl', 'uses' => 'EntryController@Ajaxcl']);
        Route::get('entries/edit/{label}/{id}', ['as' => 'admin.entries.edit', 'uses' => 'EntryController@Edit']);
        Route::post('entries/update/{id}', ['as' => 'admin.entries.update', 'uses' => 'EntryController@Update']);
        Route::get('entries/{id}/confirm-delete', ['as' => 'admin.entries.confirm-delete',   'uses' => 'EntryController@getModalEntry']);
        Route::get('entries/{id}/ledgers/delete', ['as' => 'admin.entries.delete', 'uses' => 'EntryController@destroyEntry']);

        Route::get('entries/pdf/{label}/{id}', ['as' => 'admin.entries.pdf', 'uses' => 'EntryController@DownloadPdf']);
        Route::get('entries/print/{label}/{id}', ['as' => 'admin.entries.print', 'uses' => 'EntryController@PrintEntry']);
        Route::get('entries/filter/excel', ['as' => 'admin.entries.filter.excel', 'uses' => 'ExcelController@exportFilterEntry']);
        Route::get('entries/excel/{label}/{id}', ['as' => 'admin.entries.excel', 'uses' => 'ExcelController@exportEntry']);

        //Account Reports

        //ledger Statement
        Route::get('accounts/reports/ledgerstatement', ['as' => 'admin.accounts.reports.ledgerstatement', 'uses' => 'AccountReportController@ViewLedgers']);
        Route::get('accounts/reports/ledger_statement', ['as' => 'admin.accounts.reports.ledgerdetail', 'uses' => 'AccountReportController@DetailLedgers']);
        Route::get('get-fiscal-year', ['as' => 'admin.accounts.reports.getFiscalYear', 'uses' => 'AccountReportController@getFiscalYear']);
        Route::get('chartofaccounts/excel/', ['as' => 'admin.chartofaccounts.excel', 'uses' => 'COAController@downloadExcel']);
        Route::get('chartofaccounts/pdf/', ['as' => 'admin.chartofaccounts.pdf', 'uses' => 'COAController@DownloadPdf']);
        Route::get('chartofaccounts/print/', ['as' => 'admin.chartofaccounts.print', 'uses' => 'COAController@PrintLedgers']);

        //group ledger
        Route::get('accounts/reports/group-ledger-statement', ['as' => 'admin.accounts.reports.groupledgerstatement', 'uses' => 'COAController@groupLedgerStatement']);


        Route::get('accounts/reports/group-ledger-bulk', ['as' => 'admin.accounts.reports.groupledgerBulk', 'uses' => 'COAController@groupLedgerBulk']);

        //ledger Entries
        Route::get('accounts/reports/ledgerentries', ['as' => 'admin.accounts.reports.ledgerentries', 'uses' => 'AccountReportController@listLedgersEntries']);
        Route::post('accounts/reports/ledgerentries', ['as' => 'admin.accounts.reports.detledgerentries', 'uses' => 'AccountReportController@detailLedgersEntries']);

        // Trial Balance
        Route::get('accounts/reports/trialbalance', ['as' => 'admin.accounts.reports.trialbalance.index', 'uses' => 'AccountReportController@trialbalanceindex']);
        Route::get('accounts/reports/trialbalance/pdf', ['as' => 'admin.accounts.reports.trialbalance.pdf', 'uses' => 'AccountReportController@trialbalancepdf']);
        Route::get('accounts/reports/trialbalance/excel', ['as' => 'admin.accounts.reports.trialbalance.excel', 'uses' => 'AccountReportController@trialbalanceexcel']);

        //Balance Sheet
        Route::get('accounts/reports/balancesheet', ['as' => 'admin.accounts.reports.balancesheet.index', 'uses' => 'AccountReportController@balancesheetindex']);

        Route::get('accounts/reports/balancesheet/pdf', ['as' => 'admin.accounts.reports.balancesheet.pdf', 'uses' => 'AccountReportController@balancesheetpdf']);

        Route::get('accounts/reports/balancesheet/excel', ['as' => 'admin.accounts.reports.balancesheet.excel', 'uses' => 'AccountReportController@balancesheetexcel']);
        Route::get('chartofaccounts/detail/{id}/groups', ['as' => 'admin.chartofaccounts.detail.groups', 'uses' => 'COAController@DetailGroups']);

        //profit loss
        Route::get('accounts/reports/profitloss', ['as' => 'admin.accounts.reports.profitloss.index', 'uses' => 'AccountReportController@profitlossindex']);
        Route::get('accounts/reports/profitloss/pdf', ['as' => 'admin.accounts.reports.profitloss.pdf', 'uses' => 'AccountReportController@profitlosspdf']);

        Route::get('accounts/reports/profitloss/excel', ['as' => 'admin.accounts.reports.profitloss.excel', 'uses' => 'AccountReportController@profitlossexcel']);

        // Tag

        // reprint bills

        Route::get('hotel/reprintbills/openoutlets', ['as' => 'admin.hotel.reprintbills.openoutlets', 'uses' => 'Hotel\POSDashboardController@ReprintBillOpenOutlet']);
        Route::get('hotel/orders/outlet/{id}/reprintbills', ['as' => 'admin.hotel.reprintbills.openoutlets.lists', 'uses' => 'Hotel\POSDashboardController@ReprintBillsOutletsLists']);


        //Settlement
        Route::get('hotel/settlement/openoutlets', ['as' => 'admin.hotel.settlement.openoutlets', 'uses' => 'Hotel\POSDashboardController@SettlementOutlets']);
        Route::get('hotel/orders/outlet/{id}/settlement', ['as' => 'admin.hotel.settlement.openoutlets.lists', 'uses' => 'Hotel\POSDashboardController@SettlementOutletsLists']);


        Route::get('tags', ['as' => 'admin.tags.index', 'uses' => 'TagController@index']);
        Route::get('tags/create', ['as' => 'admin.tags.create', 'uses' => 'TagController@create']);
        Route::post('tags/store', ['as' => 'admin.tags.store', 'uses' => 'TagController@store']);
        Route::get('tags/{id}/edit', ['as' => 'admin.tags.edit', 'uses' => 'TagController@edit']);
        Route::post('tags/update/{id}', ['as' => 'admin.tags.update', 'uses' => 'TagController@update']);
        Route::get('tags/{id}/confirm-delete', ['as' => 'admin.tags.confirm-delete',   'uses' => 'TagController@getModalDelete']);
        Route::get('tags/{id}/delete', ['as' => 'admin.tags.delete',           'uses' => 'TagController@destroy']);


        //entrytypes

        Route::get('entrytype', ['as' => 'admin.entrytype.index', 'uses' => 'EntryTypeController@index']);
        Route::get('entrytype/create', ['as' => 'admin.entrytype.create', 'uses' => 'EntryTypeController@create']);
        Route::post('entrytype/store', ['as' => 'admin.entrytype.store', 'uses' => 'EntryTypeController@store']);
        Route::get('entrytype/{id}/edit', ['as' => 'admin.entrytype.edit', 'uses' => 'EntryTypeController@edit']);
        Route::post('entrytype/update/{id}', ['as' => 'admin.entrytype.update', 'uses' => 'EntryTypeController@update']);
        Route::get('entrytype/{id}/confirm-delete', ['as' => 'admin.entrytype.confirm-delete', 'uses' => 'EntryTypeController@getModalDelete']);
        Route::get('entrytype/{id}/delete',         ['as' => 'admin.entrytype.delete', 'uses' => 'EntryTypeController@destroy']);

        //Talk Controller
        Route::get('talktests', 'TalkController@tests');
        Route::get('/talk', 'TalkController@index');
        Route::get('talk/{id}', 'TalkController@chatHistory')->name('message.read');
        Route::get('talk/typing/{receiver_id}', 'TalkController@typing');

        // HR Letters routes
        Route::post('hrletter/enableSelected', ['as' => 'admin.hrletter.enable-selected',  'uses' => 'HRLettersController@enableSelected']);
        Route::post('hrletter/disableSelected', ['as' => 'admin.hrletter.disable-selected', 'uses' => 'HRLettersController@disableSelected']);
        Route::get('hrletter/search', ['as' => 'admin.hrletter.search',           'uses' => 'HRLettersController@searchByName']);
        Route::post('hrletter/getInfo', ['as' => 'admin.hrletter.get-info',         'uses' => 'HRLettersController@getInfo']);
        Route::post('hrletter', ['as' => 'admin.hrletter.store',            'uses' => 'HRLettersController@store']);

        Route::get('hrletter', ['as' => 'admin.hrletter.index',            'uses' => 'HRLettersController@index']);

        Route::get('hrletter/create', ['as' => 'admin.hrletter.create',           'uses' => 'HRLettersController@create']);
        Route::get('hrletter/{proposalId}', ['as' => 'admin.hrletter.show',             'uses' => 'HRLettersController@show']);
        Route::patch('hrletter/{proposalId}', ['as' => 'admin.hrletter.patch',            'uses' => 'HRLettersController@update']);
        Route::put('hrletter/{proposalId}', ['as' => 'admin.hrletter.update',           'uses' => 'HRLettersController@update']);
        Route::delete('hrletter/{proposalId}', ['as' => 'admin.hrletter.destroy',          'uses' => 'HRLettersController@destroy']);
        Route::get('hrletter/{proposalId}/edit', ['as' => 'admin.hrletter.edit',             'uses' => 'HRLettersController@edit']);
        Route::get('hrletter/{proposalId}/confirm-delete', ['as' => 'admin.hrletter.confirm-delete',   'uses' => 'HRLettersController@getModalDelete']);
        Route::get('hrletter/{proposalId}/delete', ['as' => 'admin.hrletter.delete',           'uses' => 'HRLettersController@destroy']);
        Route::get('hrletter/{proposalId}/enable', ['as' => 'admin.hrletter.enable',           'uses' => 'HRLettersController@enable']);
        Route::get('hrletter/{proposalId}/disable', ['as' => 'admin.hrletter.disable',          'uses' => 'HRLettersController@disable']);

        Route::get('hrletter/print/{id}', ['as' => 'admin.hrletter.print',  'uses' => 'HRLettersController@printLetter']);
        Route::get('hrletter/copy/{id}', ['as' => 'admin.hrletter.copy',  'uses' => 'HRLettersController@copyDoc']);
        Route::get('hrletter/generatePDF/{id}', ['as' => 'admin.hrletter.generatePDF',   'uses' => 'HRLettersController@generatePDF']);
        // Send Email of Proposal from Detail Page
        Route::get('hrletter/{proposalId}/show-mailmodal', ['as' => 'admin.hrletter.show-mailmodal', 'uses' => 'HRLettersController@getModalMail']);

        Route::post('hrletter/{proposalId}/send-mail-modal', ['as' => 'admin.hrletter.send-mail-modal', 'uses' => 'HRLettersController@postModalMail']);
        // Ajax upload the template
        Route::post('hrletter/loadTemplate', ['as' => 'admin.loadTemplate.hr', 'uses' => 'HRLettersController@loadTemplate']);

        //backup
        Route::get('backup/list', 'SettingsController@backupList');
        Route::post('backup/delete/{id}', 'SettingsController@destroyBackup');
        Route::get('backup', 'SettingsController@backupDB');

        Route::get('download/backup/{file_name}', ['as' => 'admin.download.backup', 'uses' => 'SettingsController@downloadbackup']);

        /// Notification

        Route::get('not_viewed_cases', ['as' => 'admin.not_viewed_cases.index', 'uses' => 'HomeController@notViewedCases']);

        Route::get('not_viewed_leads', ['as' => 'admin.not_viewed_leads.index', 'uses' => 'HomeController@NotViewedLeads']);

        Route::get('due_marketing_tasks', ['as' => 'admin.due_marketing_tasks.index', 'uses' => 'HomeController@NotViewedMarketingTasks']);

        //Profile routes
        Route::get('myprofile/show', ['as' => 'admin.myprofile.show', 'uses' => 'ProfilesController@show']);
        Route::get('profile/show/{user_id}', ['as' => 'admin.profile.show', 'uses' => 'ProfilesController@publicProfile']);
        Route::get('myprofile/edit', ['as' => 'admin.myprofile.edit',             'uses' => 'ProfilesController@edit']);
        Route::post('myprofile/update', ['as' => 'admin.myprofile.update',           'uses' => 'ProfilesController@update']);
        Route::get('myprofile/imap', ['as' => 'admin.myprofile.showimap',             'uses' => 'ProfilesController@showimap']);
        Route::get('myprofile/editimap', ['as' => 'admin.myprofile.editimap',             'uses' => 'ProfilesController@editimap']);
        Route::post('myprofile/updateimap', ['as' => 'admin.myprofile.updateimap',       'uses' => 'ProfilesController@updateimap']);

        //GeoLocation
        Route::get('geolocations', 'GeoLocationController@index');
        Route::get('geolocations/filter', ['as' => 'admin.geolocations.filter', 'uses' => 'GeoLocationController@filter']);
        Route::get('geolocations/monitor', 'GeoLocationController@monitorlocation');

        Route::get('finance/dashboard', ['as' => 'admin.finance.dashboard', 'uses' => 'FinanceBoardController@index']);

        Route::get('bank', ['as' => 'admin.bank.index', 'uses' => 'BankController@index']);
        Route::get('bank/create', ['as' => 'admin.bank.create', 'uses' => 'BankController@create']);
        Route::post('bank/store', ['as' => 'admin.bank.store', 'uses' => 'BankController@store']);
        Route::get('bank/{id}/edit', ['as' => 'admin.bank.edit', 'uses' => 'BankController@edit']);
        Route::get('bank/{id}/show', ['as' => 'admin.bank.show', 'uses' => 'BankController@show']);
        Route::post('bank/{id}/update', ['as' => 'admin.bank.update', 'uses' => 'BankController@update']);
        Route::get('bank/{id}/confirmdelete', ['as' => 'admin.bank.confirmdelete', 'uses' => 'BankController@getModalDelete']);
        Route::get('bank/{id}/delete', ['as' => 'admin.bank.delete', 'uses' => 'BankController@destroy']);

        Route::get('bank/{id}/income', ['as' => 'admin.bank.income', 'uses' => 'BankController@createIncome']);
        Route::post('bank/{id}/income', ['as' => 'admin.bank.income.save', 'uses' => 'BankController@saveIncome']);
        Route::get('bank/{id}/income/edit', ['as' => 'admin.bank.income.edit', 'uses' => 'BankController@editIncome']);
        Route::post('bank/{id}/income/edit', ['as' => 'admin.bank.income.update', 'uses' => 'BankController@updateIncome']);

        Route::post('rating', ['as' => 'admin.rating.store', 'uses' => 'RatingController@store']);
        Route::get('rating', ['as' => 'admin.rating.index', 'uses' => 'RatingController@index']);
        Route::get('rating/create', ['as' => 'admin.rating.create', 'uses' => 'RatingController@create']);
        Route::post('rating/{id}', ['as' => 'admin.rating.update', 'uses' => 'RatingController@update']);
        Route::get('rating/{id}/edit', ['as' => 'admin.rating.edit', 'uses' => 'RatingController@edit']);
        Route::get('rating/delete-confirm/{id}', ['as' => 'admin.rating.delete-confirm', 'uses' => 'RatingController@getModalDelete']);
        Route::get('rating/delete{id}', ['as' => 'admin.rating.delete', 'uses' => 'RatingController@destroy']);

        // task stages routes

        Route::post('task/stages', ['as' => 'admin.task.stages.store', 'uses' => 'TaskStageController@store']);
        Route::get('task/stages', ['as' => 'admin.task.stages.index', 'uses' => 'TaskStageController@index']);
        Route::get('task/stages/create', ['as' => 'admin.task.stages.create', 'uses' => 'TaskStageController@create']);
        Route::post('task/stages/{id}', ['as' => 'admin.task.stages.update', 'uses' => 'TaskStageController@update']);
        Route::get('task/stages/{id}/edit', ['as' => 'admin.task.stages.edit', 'uses' => 'TaskStageController@edit']);
        Route::get('task/stages/delete-confirm/{id}', ['as' => 'admin.task.stages.delete-confirm', 'uses' => 'TaskStageController@getModalDelete']);
        Route::get('task/stages/delete{id}', ['as' => 'admin.task.stages.delete', 'uses' => 'TaskStageController@destroy']);

        Route::post('lost-and-founds', ['as' => 'admin.lostAndFound.store', 'uses' => 'LostAndFoundController@store']);
        Route::get('lost-and-founds', ['as' => 'admin.lostAndFound.index', 'uses' => 'LostAndFoundController@index']);
        Route::get('lost-and-founds/create', ['as' => 'admin.lostAndFound.create', 'uses' => 'LostAndFoundController@create']);
        Route::post('lost-and-founds/{id}', ['as' => 'admin.lostAndFound.update', 'uses' => 'LostAndFoundController@update']);
        Route::get('lost-and-founds/{id}/edit', ['as' => 'admin.lostAndFound.edit', 'uses' => 'LostAndFoundController@edit']);
        Route::get('lost-and-founds/delete-confirm/{id}', ['as' => 'admin.lostAndFound.confirm-delete', 'uses' => 'LostAndFoundController@getModalDelete']);
        Route::get('lost-and-founds/delete{id}', ['as' => 'admin.lostAndFound.delete', 'uses' => 'LostAndFoundController@destroy']);

        Route::get('getReservation', ['as' => 'admin.getReservation', 'uses' => 'LostAndFoundController@getReservation']);

        Route::post('room-cleanings', ['as' => 'admin.roomCleanings.store', 'uses' => 'RoomCleaningController@store']);
        Route::get('room-cleanings', ['as' => 'admin.roomCleanings.index', 'uses' => 'RoomCleaningController@index']);
        Route::get('room-cleanings/create', ['as' => 'admin.roomCleanings.create', 'uses' => 'RoomCleaningController@create']);
        Route::post('room-cleanings/{id}', ['as' => 'admin.roomCleanings.update', 'uses' => 'RoomCleaningController@update']);
        Route::get('room-cleanings/{id}/edit', ['as' => 'admin.roomCleanings.edit', 'uses' => 'RoomCleaningController@edit']);
        Route::get('room-cleanings/delete-confirm/{id}', ['as' => 'admin.roomCleanings.confirm-delete', 'uses' => 'RoomCleaningController@getModalDelete']);
        Route::get('room-cleanings/delete{id}', ['as' => 'admin.roomCleanings.delete', 'uses' => 'RoomCleaningController@destroy']);


        // shifts routes

        Route::post('shift', ['as' => 'admin.shift.store', 'uses' => 'ShiftController@store']);
        Route::get('shift', ['as' => 'admin.shift.index', 'uses' => 'ShiftController@index']);
        Route::get('shift/create', ['as' => 'admin.shift.create', 'uses' => 'ShiftController@create']);
        Route::post('shift/{id}', ['as' => 'admin.shift.update', 'uses' => 'ShiftController@update']);
        Route::get('shift/{id}/edit', ['as' => 'admin.shift.edit', 'uses' => 'ShiftController@edit']);
        Route::get('shift/delete-confirm/{id}', ['as' => 'admin.shift.delete-confirm', 'uses' => 'ShiftController@getModalDelete']);
        Route::get('shift/delete{id}', ['as' => 'admin.shift.delete', 'uses' => 'ShiftController@destroy']);

        Route::post('ajaxLeadUpdate', 'LeadsController@ajaxLeadUpdate');
        Route::post('/ajaxTaskDescription', 'ProjectTaskController@taskdescription');

        Route::get('shiftcalender', ['as' => 'admin.shift.calender', 'uses' => 'ShiftController@shiftCalender']);
        Route::post('shiftcalender', ['as' => 'admin.shift.calenderpo', 'uses' => 'CalendarController@shiftCalender']);

        // shift maps

        Route::post('shifts/maps', ['as' => 'admin.shift.maps.store', 'uses' => 'ShiftMapController@store']);
        Route::get('shifts/maps', ['as' => 'admin.shift.maps.index', 'uses' => 'ShiftMapController@index']);
        Route::get('shifts/maps/create', ['as' => 'admin.shift.maps.create', 'uses' => 'ShiftMapController@create']);
        Route::post('shifts/maps/{id}', ['as' => 'admin.shift.maps.update', 'uses' => 'ShiftMapController@update']);
        Route::get('shifts/maps/{id}/edit', ['as' => 'admin.shift.maps.edit', 'uses' => 'ShiftMapController@edit']);
        Route::get('shifts/maps/delete-confirm/{id}', ['as' => 'admin.shift.maps.delete-confirm', 'uses' => 'ShiftMapController@getModalDelete']);
        Route::get('shifts/maps/delete{id}', ['as' => 'admin.shift.maps.delete', 'uses' => 'ShiftMapController@destroy']);

        //shift breaks

        Route::get('shiftsBreaks', ['as' => 'admin.shiftsBreaks.index', 'uses' => 'ShiftBreakController@index']);

        Route::get('shiftsBreaks/create', ['as' => 'admin.shiftsBreaks.create', 'uses' => 'ShiftBreakController@create']);
        Route::post('shiftsBreaks/create', ['as' => 'admin.shiftsBreaks.store', 'uses' => 'ShiftBreakController@store']);

        Route::get('shiftsBreaks/{id}/edit', ['as' => 'admin.shiftsBreaks.edit', 'uses' => 'ShiftBreakController@edit']);
        Route::post('shiftsBreaks/{id}/edit', ['as' => 'admin.shiftsBreaks.update', 'uses' => 'ShiftBreakController@update']);
        Route::get('shiftsBreaks/delete-confirm/{id}', ['as' => 'admin.shiftsBreaks.delete-confirm', 'uses' => 'ShiftBreakController@getModalDelete']);
        Route::get('shifts/maps/delete{id}', ['as' => 'admin.shiftsBreaks.delete', 'uses' => 'ShiftBreakController@destroy']);

        // projects groups

        Route::get('projectsgroups/{projectId}', ['as' => 'admin.projects.group', 'uses' => 'ProjectGroupController@index']);
        Route::get('projectsgroups/create/{projectId}', ['as' => 'admin.projects.group.create', 'uses' => 'ProjectGroupController@create']);
        Route::post('projectsgroups/create/{projectId}', ['as' => 'admin.projects.group.store', 'uses' => 'ProjectGroupController@store']);
        Route::get('projectsgroups/edit/{projectId}/{id}', ['as' => 'admin.projects.group.edit', 'uses' => 'ProjectGroupController@edit']);
        Route::post('projectsgroups/edit/{id}', ['as' => 'admin.projects.group.update', 'uses' => 'ProjectGroupController@update']);

        Route::get('projectsgroups/enable/{id}', ['as' => 'admin.projects.group.enabledisable', 'uses' => 'ProjectGroupController@enabledisable']);

        Route::get('shifts/bulk/', ['as' => 'admin.shift.bulk.index', 'uses' => 'ShiftMapController@bulkindex']);
        Route::post('shifts/bulk/create', ['as' => 'admin.shift.bulk.create', 'uses' => 'ShiftMapController@bulkcreate']);
        Route::post('shifts/bulk/store', ['as' => 'admin.shift.bulk.store', 'uses' => 'ShiftMapController@bulkstore']);

        Route::get('bank/{id}/income/pdf', ['as' => 'admin.bank.income.pdf', 'uses' => 'BankController@printIncome']);
        Route::get('bank/{id}/income/mail', ['as' => 'admin.bank.income.mail', 'uses' => 'BankController@sendmail']);

        Route::get('mytimehistory', ['as' => 'admin.mytime.history', 'uses' => 'AttendanceController@myTimeHistory']);
        Route::post('mytimehistory', ['as' => 'admin.mytime.history.post', 'uses' => 'AttendanceController@myTimeHistoryUpdate']);
        Route::get('talk/sync/message/{id}', 'TalkController@ajaxSyncMessage');
        Route::get('talk/popover/message', 'TalkController@getPopoverMessage');

        Route::get('all/attendance_report/print/{date_in}', ['as' => 'admin.all.attendance_report.print',  'uses' => 'AttendanceController@printAllAttendance']);

        Route::get('all/attendance_report/generatePDF/{date_in}', ['as' => 'admin.all.attendance_report.generatePDF',   'uses' => 'AttendanceController@generateAllPDF']);

        Route::get('country', 'LeadsController@getcountry');
        Route::get('cities/{country}', 'LeadsController@getcity');

        Route::get('product/stocks_by_location', ['as' => 'admin.products.stocks_by_location', 'uses' => 'ProductController@stocks_by_location']);

        Route::post('product/stocks_by_location', ['as' => 'admin.products.stocks_by_location.post', 'uses' => 'ProductController@stocks_by_location_post']);

        Route::get('product/stock_adjustment', ['as' => 'admin.products.stock_adjustment', 'uses' => 'ProductController@stock_adjustment']);
        Route::post('product/stock_adjustment', ['as' => 'admin.products.stock_adjustment.store', 'uses' => 'ProductController@stock_adjustment_store']);

        Route::get('product/stock_adjustment/create', ['as' => 'admin.products.stock_adjustment.create', 'uses' => 'ProductController@stock_adjustment_create']);
        Route::get('product/stock_adjustment/{id}/edit', ['as' => 'admin.products.stock_adjustment.edit', 'uses' => 'ProductController@stock_adjustment_edit']);

        Route::post('product/stock_adjustment/{id}', ['as' => 'admin.products.stock_adjustment.update', 'uses' => 'ProductController@stock_adjustment_update']);

        Route::get('product/stock_adjustment/{courseId}/confirm-delete', ['as' => 'admin.products.stock_adjustment.confirm-delete',   'uses' => 'ProductController@stock_adjustment_getModalDelete']);
        Route::get('product/stock_adjustment/{courseId}/delete', ['as' => 'admin.products.stock_adjustment.delete',           'uses' => 'ProductController@stock_adjustment_destroy']);

        //Credit Note

        Route::get('creditnote', ['as' => 'admin.creditnote.index', 'uses' => 'CreditNoteController@index']);
        Route::get('creditnote/create', ['as' => 'admin.creditnote.create', 'uses' => 'CreditNoteController@create']);
        Route::post('creditnote', ['as' => 'admin.creditnote.store', 'uses' => 'CreditNoteController@store']);
        Route::get('creditnote/{id}', ['as' => 'admin.creditnote.show', 'uses' => 'CreditNoteController@show']);

        Route::get('creditnote/{id}/edit', ['as' => 'admin.creditnote.edit',    'uses' => 'CreditNoteController@edit']);

        Route::post('creditnote/{id}', ['as' => 'admin.creditnote.update',    'uses' => 'CreditNoteController@update']);

        Route::get('creditnote/{id}/confirm-delete', ['as' => 'admin.creditnote.confirm-delete',   'uses' => 'CreditNoteController@getModalDelete']);
        Route::get('creditnote/{id}/delete', ['as' => 'admin.creditnote.delete',           'uses' => 'CreditNoteController@destroy']);

        Route::get('creditnote/print/{id}', ['as' => 'admin.creditnote.print',  'uses' => 'CreditNoteController@printCreditNote']);
        Route::get('creditnote/generatePDF/{id}', ['as' => 'admin.creditnote.generatePDF',   'uses' => 'CreditNoteController@generatePDF']);

        Route::get('getInvoiceId', ['as' => 'admin.getInvoiceId', 'uses' => 'CreditNoteController@getInvoiceId']);

        Route::get('getInvoiceInfo', ['as' => 'admin.getInvoiceInfo', 'uses' => 'CreditNoteController@getInvoiceInfo']);

        // WareHouse Stock Transfer

        Route::post('location/stocktransfer', ['as' => 'admin.location.stocktransfer.store',            'uses' => 'LocationStockTransferController@store']);
        Route::get('location/stocktransfer', ['as' => 'admin.location.stocktransfer.index',            'uses' => 'LocationStockTransferController@index']);
        Route::get('location/stocktransfer/create', ['as' => 'admin.location.stocktransfer.create',            'uses' => 'LocationStockTransferController@create']);

        Route::get('location/stocktransfer/{Id}', ['as' => 'admin.location.stocktransfer.show',             'uses' => 'LocationStockTransferController@show']);

        Route::get('location/stocktransfer/{Id}/edit', ['as' => 'admin.location.stocktransfer.edit',             'uses' => 'LocationStockTransferController@edit']);
        Route::post('location/stocktransfer/{Id}', ['as' => 'admin.location.stocktransfer.update',             'uses' => 'LocationStockTransferController@update']);

        Route::get('location/stocktransfer/{id}/confirm-delete', ['as' => 'admin.location.stocktransfer.confirm-delete',   'uses' => 'LocationStockTransferController@getModalDelete']);
        Route::get('location/stocktransfer/{id}/delete', ['as' => 'admin.location.stocktransfer.delete',           'uses' => 'LocationStockTransferController@destroy']);

        Route::get('getStockAvailability', ['as' => 'admin.getStockAvailability',            'uses' => 'LocationStockTransferController@getStockAvailability']);

        Route::get('location/stocktransfer/pdf/{id}', ['as' => 'admin.location.stocktransfer.pdf',            'uses' => 'LocationStockTransferController@pdf']);
        Route::get('location/stocktransfer/print/{id}', ['as' => 'admin.location.stocktransfer.print',            'uses' => 'LocationStockTransferController@print']);

        Route::get('email/downloadExcel', ['as' => 'admin.email.downloadExcel',           'uses' => 'MailController@downloadExcel']);

        Route::get('campaigns/bulk-mail/{id}', ['as' => 'admin.campaigns.bulk-mail', 'uses' => 'CampaignsController@getbulkmail']);
        Route::post('campaigns/bulk-mail/{id}', ['as' => 'admin.campaigns.postbulk-mail', 'uses' => 'CampaignsController@postbulkmail']);
        Route::get('invoice/edit/{id}', ['as' => 'admin.invoice.edit', 'uses' => 'InvoiceController@edit']);
        Route::post('invoice/edit/{id}', ['as' => 'admin.invoice.update', 'uses' => 'InvoiceController@update']);

        // Supplier return
        Route::get('supplierreturn', ['as' => 'admin.supplierreturn.index', 'uses' => 'SupplierReturnController@index']);
        Route::get('supplierreturn/create', ['as' => 'admin.supplierreturn.create', 'uses' => 'SupplierReturnController@create']);
        Route::post('supplierreturn', ['as' => 'admin.supplierreturn.store', 'uses' => 'SupplierReturnController@store']);
        Route::get('supplierreturn/{id}', ['as' => 'admin.supplierreturn.show', 'uses' => 'SupplierReturnController@show']);

        Route::get('supplierreturn/{id}/edit', ['as' => 'admin.supplierreturn.edit',    'uses' => 'SupplierReturnController@edit']);

        Route::post('supplierreturn/{id}', ['as' => 'admin.supplierreturn.update',    'uses' => 'SupplierReturnController@update']);

        Route::get('supplierreturn/{id}/confirm-delete', ['as' => 'admin.supplierreturn.confirm-delete',   'uses' => 'SupplierReturnController@getModalDelete']);
        Route::get('supplierreturn/{id}/delete', ['as' => 'admin.supplierreturn.delete',           'uses' => 'SupplierReturnController@destroy']);

        Route::get('supplierreturn/print/{id}', ['as' => 'admin.supplierreturn.print',  'uses' => 'SupplierReturnController@print']);
        Route::get('supplierreturn/pdf/{id}', ['as' => 'admin.supplierreturn.generatePDF',   'uses' => 'SupplierReturnController@pdf']);

        Route::get('getPurchaseBillId', ['as' => 'admin.getPurchaseBillId', 'uses' => 'SupplierReturnController@getPurchaseBillId']);

        Route::get('getPurchaseBillInfo', ['as' => 'admin.getPurchaseBillInfo', 'uses' => 'SupplierReturnController@getPurchaseBillInfo']);

        // download index
        Route::get('download/orders/pdf/index', ['as' => 'admin.download.orders.pdf.index', 'uses' => 'DownloadIndexController@orderpdf']);

        Route::get('download/orders/excel/index', ['as' => 'admin.download.orders.excel.index', 'uses' => 'DownloadIndexController@orderexcel']);

        Route::get('download/purchase/pdf/index', ['as' => 'admin.download.purchase.pdf.index', 'uses' => 'DownloadIndexController@purchasepdf']);

        Route::get('download/purchase/excel/index', ['as' => 'admin.download.purchase.excel.index', 'uses' => 'DownloadIndexController@purchaseexcel']);

        Route::get('expenses/{courseId}/confirm-delete', ['as' => 'admin.expenses.confirm-delete', 'uses' => 'ExpensesController@getModalDelete']);

        Route::get('expenses/{courseId}/delete', ['as' => 'admin.expenses.delete', 'uses' => 'ExpensesController@destroy']);

        Route::get('getProductName', ['as' => 'admin.getProdcutName', 'uses' => 'ProductController@getProductName']);

        Route::get('getComponentProduct', ['as' => 'admin.getComponentProduct', 'uses' => 'ProductController@getComponentProduct']);


        // bill of materials

        Route::get('billofmaterials', ['as' => 'admin.billofmaterials.index', 'uses' => 'BOMController@index']);
        Route::get('billofmaterials/create', ['as' => 'admin.billofmaterials.create', 'uses' => 'BOMController@create']);
        Route::post('billofmaterials', ['as' => 'admin.billofmaterials.store', 'uses' => 'BOMController@store']);
        Route::get('billofmaterials/{id}', ['as' => 'admin.billofmaterials.show', 'uses' => 'BOMController@show']);

        Route::get('billofmaterials/{id}/edit', ['as' => 'admin.billofmaterials.edit',    'uses' => 'BOMController@edit']);

        Route::post('billofmaterials/{id}', ['as' => 'admin.billofmaterials.update',    'uses' => 'BOMController@update']);

        Route::get('billofmaterials/{id}/confirm-delete', ['as' => 'admin.billofmaterials.confirm-delete',   'uses' => 'BOMController@getModalDelete']);
        Route::get('billofmaterials/{id}/delete', ['as' => 'admin.billofmaterials.delete',           'uses' => 'BOMController@destroy']);

        Route::get('getComponentProductInfo', ['as' => 'admin.getComponentProductInfo',           'uses' => 'BOMController@getComponentProductInfo']);

        Route::get('billofmaterials/print/{id}', ['as' => 'admin.billofmaterials.print',  'uses' => 'BOMController@print']);
        Route::get('billofmaterials/pdf/{id}', ['as' => 'admin.billofmaterials.generatePDF',   'uses' => 'BOMController@pdf']);

        Route::post('product/{courseId}/model', ['as' => 'admin.product.model', 'uses' => 'ProductModelsController@addProdModel']);

        Route::get('product/confirm-delete-prod-model/{modelId}', ['as' => 'admin.confirm-delete-prod-model', 'uses' => 'ProductModelsController@confirmdeleteProdmodel']);

        Route::get('product/delete-prod-model/{modelId}', ['as' => 'admin.delete-prod-model', 'uses' => 'ProductModelsController@deleteProdmodel']);

        Route::post('product/{courseId}/serial_num', ['as' => 'admin.product.serial_num', 'uses' => 'ProductModelsController@addProdserialnum']);

        Route::get('product/confirm-delete-prod-serial_num/{modelId}', ['as' => 'admin.confirm-delete-prod-serial_num', 'uses' => 'ProductModelsController@confirmdeleteProdserialnum']);

        Route::get('product/delete-prod-serial_num/{modelId}', ['as' => 'admin.delete-prod-serial_num', 'uses' => 'ProductModelsController@deleteProdserialnum']);

        Route::post('cases/product_info/{id}', ['as' => 'admin.cases.product_info', 'uses' => 'CasesController@productinfo']);
        Route::post('cases/model_serial_no/{id}', ['as' => 'admin.cases.model_serial_no', 'uses' => 'CasesController@productserialnum']);

        Route::get('dealer', ['as' => 'admin.dealer.index', 'uses' => 'ClientsController@dealer']);


        //assembly

        Route::get('assembly', ['as' => 'admin.assembly.index', 'uses' => 'AssemblyController@index']);
        Route::get('assembly/create', ['as' => 'admin.assembly.create', 'uses' => 'AssemblyController@create']);
        Route::post('assembly', ['as' => 'admin.assembly.store', 'uses' => 'AssemblyController@store']);
        Route::get('assembly/{id}', ['as' => 'admin.assembly.show', 'uses' => 'AssemblyController@show']);

        Route::get('assembly/{id}/edit',  ['as' => 'admin.assembly.edit',    'uses' => 'AssemblyController@edit']);

        Route::post('assembly/{id}',  ['as' => 'admin.assembly.update',    'uses' => 'AssemblyController@update']);

        Route::get('assembly/{id}/confirm-delete', ['as' => 'admin.assembly.confirm-delete',   'uses' => 'AssemblyController@getModalDelete']);
        Route::get('assembly/{id}/delete',         ['as' => 'admin.assembly.delete',           'uses' => 'AssemblyController@destroy']);

        Route::get('/getComponentProductInfo/assembly',         ['as' => 'admin.assembly.getComponentProductInfo',           'uses' => 'AssemblyController@getComponentProductInfo']);

        Route::get('/getProductName/assembly', ['as' => 'admin.assembly.getProdcutName', 'uses' => 'AssemblyController@getProductName']);
        Route::get('/getComponentProduct/assembly', ['as' => 'admin.assembly.getComponentProduct', 'uses' => 'AssemblyController@getComponentProduct']);

        Route::get('assembly/print/{id}', ['as' => 'admin.assembly.print',  'uses' => 'AssemblyController@print']);
        Route::get('assembly/pdf/{id}',   ['as' => 'admin.assembly.generatePDF',   'uses' => 'AssemblyController@pdf']);



        //google sync

        Route::get('googlesync/oauth', ['as' => 'oauthCallback', 'uses' => 'GoogleCalendarController@oauth']);
        Route::get('googlesync/lead', ['as' => 'lead.oauthCallback', 'uses' => 'GoogleCalendarController@syncLeadTaskWithGoogle']);
        Route::get('googlesync/projecttask', ['as' => 'projecttask.oauthCallback', 'uses' => 'GoogleCalendarController@syncProjectTaskWithGoogle']);

        Route::get('invoice/posttoird/{id}', ['as' => 'admin.invoice.posttoird', 'uses' => 'InvoiceController@postInvoicetoIRD']);
        Route::get('invoice/return/fromird', ['as' => 'admin.invoice.returnfromird', 'uses' => 'InvoiceController@returnfromird']);
        Route::post('invoice/return/fromird', ['as' => 'admin.invoice.returnfromird.post', 'uses' => 'InvoiceController@returnfromirdpost']);
        Route::get('invoice/return/sales', ['as' => 'admin.invoice.return.sales', 'uses' => 'InvoiceController@returnsales']);
        Route::post('invoice/return/sales', ['as' => 'admin.invoice.return.sales.list', 'uses' => 'InvoiceController@returnsaleslist']);

        Route::get('documents/userlist/{id}', ['as' => 'admin.documents.userlist', 'uses' => 'DocumentController@userlist']);
        Route::post('documents/share/{id}', ['as' => 'admin.documents.share', 'uses' => 'DocumentController@docsharepost']);


        //
        Route::get('/select/outlet/table/transfer', ['as' => 'admin.select.outlet.table.transfer', 'uses' => 'Hotel\POSDashboardController@selectOutletTableTransfer']);
        Route::get('/hotel/orders/outlet/{id}/tabletransfer', ['as' => 'admin.outlet.table.transfer.index', 'uses' => 'Hotel\POSDashboardController@TableTransferIndex']);
        Route::post('/hotel/orders/outlet/{id}/tabletransfer', ['as' => 'admin.outlet.table.transfer.post', 'uses' => 'Hotel\POSDashboardController@TableTransferPost']);

                // Issue from warehouse
            Route::get('production/issue-warehouse',           ['as'=>'admin.production.issue-warehouse-index',     'uses'=>'ProductionController@warehouseIndex']);

            Route::get('production/statistics',           ['as'=>'admin.production.statistics',     'uses'=>'ProductionController@statistics']);


            Route::post('production/issue-warehouse-updateProd/{id}',          ['as'=>'admin.production.issue-warehouse-updateProd',          'uses'=>'ProductionController@warehouseUpdateProd']);


            Route::get('production/issue-warehouse-create',    ['as'=>'admin.production.issue-warehouse-create',    'uses'=>'ProductionController@warehouseCreate']);
            Route::post('production/issue-warehouse-create',   ['as'=>'admin.production.issue-warehouse-store',    'uses'=>'ProductionController@warehouseStore']);

            Route::get('production/issue-warehouse-edit/{id}',          ['as'=>'admin.production.issue-warehouse-edit',          'uses'=>'ProductionController@warehouseEdit']);
            Route::post('production/issue-warehouse-edit/{id}',         ['as'=>'admin.production.issue-warehouse-update',          'uses'=>'ProductionController@warehouseUpdate']);
            Route::get('production/issue-warehouse-confirm-delete/{id}',['as'=>'admin.production.issue-warehouse-confirm-delete','uses'=>'ProductionController@deletewarehouseModal']);
            Route::get('production/issue-warehouse-delete/{id}',        ['as'=>'admin.production.issue-warehouse-delete',        'uses'=>'ProductionController@deleteWarehouse']);

            Route::get('production/issue-warehouse-return/{id}',['as'=>'admin.production.issue-warehouse-returnwh','uses'=>'ProductionController@retrunWarehouseShow']);
            Route::post('production/issue-warehouse-return/{id}',['as'=>'admin.production.issue-warehouse-return','uses'=>'ProductionController@returnWarehouse']);

            Route::get('production/issue-warehouse-show/{id}',['as'=>'admin.production.issue-warehouse-show','uses'=>'ProductionController@showWarehouse']);

            Route::get('production/lot_print/{issue_id}',
                ['as'=>'admin.production.lot_print',     'uses'=>'ProductionController@lot_print']);
            Route::get('production/target_lot_print/{issue_id}',
                ['as'=>'admin.production.target_lot_print',     'uses'=>'ProductionController@target_lot_print']);
            Route::post('production/start_production',['as'=>'admin.start_production','uses'=>'ProductionController@startProduction']);
            Route::post('production/pause_production',['as'=>'admin.pause_production','uses'=>'ProductionController@pauseProduction']);
            Route::post('production/stop_production',['as'=>'admin.stop_production','uses'=>'ProductionController@stopProduction']);

            Route::post('production/finish_production/{id}',['as'=>'admin.finish_production','uses'=>'ProductionController@finishProduction']);


            Route::get('production/myplan/{id}',['as'=>'admin.production.myplan','uses'=>'ProductionController@production_plan']);

            Route::get('production/issue-warehouse-print/{id}',['as'=>'admin.production.issue-warehouse-print','uses'=>'ProductionController@showWarehouse']);

            Route::get('production/filter-warehouse',   ['as'=>'admin.production.filter-warehouse', 'uses'=>'ProductionController@warehouseFilter']);
            Route::get('production/search-warehouse',   ['as'=>'admin.production.search-warehouse', 'uses'=>'ProductionController@warehouseSearch']);
            //BOM

             Route::get('production/fact_bom/',                      ['as'=>'admin.production.fact_bom',             'uses'=>'FactBOMController@index']);
        Route::get('production/fact_bom/create/',               ['as'=>'admin.production.fact_bom.create',      'uses'=>'FactBOMController@create']);
        Route::post('production/fact_bom/create/',              ['as'=>'admin.production.fact_bom.store',      'uses'=>'FactBOMController@store']);
        Route::get('production/fact_bom/edit/{id}',             ['as'=>'admin.production.fact_bom.edit',        'uses'=>'FactBOMController@edit']);
        Route::put('production/fact_bom/update/{id}',            ['as'=>'admin.production.fact_bom.update',        'uses'=>'FactBOMController@update']);
        Route::get('production/fact_bom/confirm-delete/{id}',   ['as'=>'admin.production.fact_bom.confirm-delete','uses'=>'FactBOMController@deleteModal']);
        Route::get('production/fact_bom/delete/{id}',           ['as'=>'admin.production.fact_bom.delete',      'uses'=>'FactBOMController@destroy']);
        Route::get('production/fact_bom/show/{id}',             ['as'=>'admin.production.fact_bom.show',        'uses'=>'FactBOMController@show']);
        Route::get('production/fact_bom/print/{id}',            ['as'=>'admin.production.fact_bom.print',         'uses'=>'FactBOMController@show']);
        Route::get('production/fact_bom/search',                ['as'=>'admin.production.fact_bom.search',      'uses'=>'FactBOMController@search']);
        Route::get('production/fact_bom/filter',                ['as'=>'admin.production.fact_bom.filter',      'uses'=>'FactBOMController@filter']);


 // ---------- fact Routing -------------- //

        Route::get('production/fact_routing/',                      ['as'=>'admin.production.fact_routing',             'uses'=>'FactRoutingController@index']);
        Route::get('production/fact_routing/create/',               ['as'=>'admin.production.fact_routing.create',      'uses'=>'FactRoutingController@create']);
        Route::post('production/fact_routing/create/',              ['as'=>'admin.production.fact_routing.store',      'uses'=>'FactRoutingController@store']);
        Route::get('production/fact_routing/edit/{id}',             ['as'=>'admin.production.fact_routing.edit',        'uses'=>'FactRoutingController@edit']);
        Route::put('production/fact_routing/update/{id}',            ['as'=>'admin.production.fact_routing.update',        'uses'=>'FactRoutingController@update']);
        Route::get('production/fact_routing/confirm-delete/{id}',   ['as'=>'admin.production.fact_routing.confirm-delete','uses'=>'FactRoutingController@deleteModal']);
        Route::get('production/fact_routing/delete/{id}',           ['as'=>'admin.production.fact_routing.delete',      'uses'=>'FactRoutingController@destroy']);
        Route::get('production/fact_routing/show/{id}',             ['as'=>'admin.production.fact_routing.show',        'uses'=>'FactRoutingController@show']);
        Route::get('production/fact_routing/print/{id}',            ['as'=>'admin.production.fact_routing.print',         'uses'=>'FactRoutingController@show']);
        Route::get('production/fact_routing/search',                ['as'=>'admin.production.fact_routing.search',      'uses'=>'FactRoutingController@search']);
        Route::get('production/fact_routing/filter',                ['as'=>'admin.production.fact_routing.filter',      'uses'=>'FactRoutingController@filter']);


        Route::get('production/product-section-index',['as'=>'admin.production.product-section-index','uses'=>'ProductSectionController@productSectionIndex']);
        Route::get('production/product-section-create',['as'=>'admin.production.product-section-create','uses'=>'ProductSectionController@productSection']);
        Route::post('production/product-section-create',['as'=>'admin.production.product-section-store','uses'=>'ProductSectionController@productSectionStore']);
        Route::get('production/confirm-delete-prodsection/{id}',['as'=>'admin.production.confirm-delete-prodsection','uses'=>'ProductSectionController@deleteproductSectionModal']);
        Route::get('production/delete-prodsection/{id}',['as'=>'admin.productsection.delete-prodsection','uses'=>'ProductSectionController@deleteproductSection']);
        Route::get('production/edit-prodsection/{id}',['as'=>'admin.production.edit-prodsection','uses'=>'ProductSectionController@editproductSection']);
        Route::post('production/edit-prodsection/{id}',['as'=>'admin.production.update-prodsection','uses'=>'ProductSectionController@updateproductSection']);

        Route::get('migrate-data/{table}',['as'=>'admin.migrateData','uses'=>'FiscalyearController@migrateData']);

// Recipes
        Route::get('recipe',['as'=>'admin.recipe.index','uses'=>'RecipeController@Index']);
        Route::get('recipe/create',['as'=>'admin.recipe.create','uses'=>'RecipeController@Create']);
        Route::post('recipe/create',['as'=>'admin.recipe.store','uses'=>'RecipeController@Store']);
        Route::get('recipe/edit/{id}',['as'=>'admin.recipe.edit','uses'=>'RecipeController@Edit']);
        Route::post('recipe/edit/{id}',['as'=>'admin.recipe.up','uses'=>'RecipeController@Update']);
        Route::get('recipe/confirm-delete/{id}',['as'=>'admin.recipe.confirm-delete','uses'=>'RecipeController@deleteModal']);
        Route::get('recipe/delete/{id}',['as'=>'admin.recipe.delete','uses'=>'RecipeController@destroy']);
        Route::get('recipe/show/{id}',['as'=>'admin.recipe.show','uses'=>'RecipeController@show']);
        Route::get('recipe/search',['as'=>'admin.recipe.search','uses'=>'RecipeController@Search']);

        Route::get('set-session',['as'=>'admin.set-session','uses'=>'FiscalYearController@setSession']);

        Route::get('business-date',['as'=>'admin.getBusinessDate','uses'=>'BusinessDateController@getBusinessDate']);
        Route::post('business-date',['as'=>'admin.setBusinessDate','uses'=>'BusinessDateController@setBusinessDate']);
        Route::post('day-end/business-date',['as'=>'admin.setBusinessDate','uses'=>'Hotel\NightAuditController@completeDayEnd']);

    }); // End of ADMIN group

    //Talk Ajax
    Route::group(['prefix' => 'ajax', 'as' => 'ajax::'], function () {

        Route::post('message/send', 'TalkController@ajaxSendMessage')->name('message.new');
        Route::post('message/seen/{cid}', 'TalkController@ajaxSeenMessage')->name('message.seen');
        Route::post('message/more/{id}/{start}/{end}', 'TalkController@moreMessage')->name('message.start');
        Route::delete('message/delete/{id}', 'TalkController@ajaxDeleteMessage')->name('message.delete');
    });

    // Uncomment to enable Rapyd datagrid.
    // require __DIR__.'/rapyd.php';
}); // end of AUTHORIZE middleware group
